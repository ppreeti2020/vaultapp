<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dao.DataDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>CSR Administration</title>

  <!-- calendar stylesheet -->
  <link rel="stylesheet" type="text/css" media="all" href="styles/calendar-win2k-cold-1.css" title="win2k-cold-1" />

  <!-- main calendar program -->
  <script type="text/javascript" src="js/calendar.js"></script>

  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar-setup.js"></script>

<script type="text/javascript">
function moveoutid()
{
	var sda = document.getElementsByName("csrGrps");
	//alert(document.csrGrps);
	var len = sda.length;
	var sda1 = document.getElementsByName("selcsrGrps");
	
	for(var j=0; j<len; j++)
	{
		alert( sda.options[j].text);
		if(sda[j].selected)
		{
			var tmp = sda.options[j].text;
			var tmp1 = sda.options[j].value;
			sda.remove(j);
			j--;
			var y=document.createElement('option');
			y.text=tmp;
			try
			{sda1.add(y,null);
			}
			catch(ex)
			{
			sda1.add(y);
			}
		}
	}
}


function moveinid()
{
	var sda = document.getElementById('csrGrps');
	var sda1 = document.getElementById('selcsrGrps');
	var len = sda1.length;
	for(var j=0; j<len; j++)
	{
		if(sda1[j].selected)
		{
			var tmp = sda1.options[j].text;
			var tmp1 = sda1.options[j].value;
			sda1.remove(j);
			j--;
			var y=document.createElement('option');
			y.text=tmp;
			try
			{
			sda.add(y,null);}
			catch(ex){
			sda.add(y);	
			}

		}
	}	
}
/*window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"inputField",
			dateFormat:"%d-%m-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1
		});
	};
	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
	*/

</script>

</head>
<s:include value="adminHeader.jsp" />
<body>
<table width="100%"><tr><td>
<s:include value="userrepCommonTabs.jsp" />
<td></tr><tr><td>
<table width="100%" border="1px solid #778" height="500" align="top">
	
	<tr><td>
	<s:form action="AdminApplyAction">
	<table class="dataTable" >
	
		<tr>
			<td>
				<b>User Groups</b>
			</td>
			<td>
				<b>&nbsp;</b>
			</td>
			<td>
				<b>Selected User Groups</b>
			</td>
			
			<td>
				<b>CSR Roles</b>
			</td>
			<td>
				<b>CSR Databases</b>
			</td>
			<td>
				<b>Active</b>
			</td>
		</tr>
		<tr><td>
			<%
		List<String> csrGroups = (ArrayList<String>)request.getAttribute("csrGroups");			
		Iterator<String> itr = csrGroups.iterator();
		%>
				<select name="csrGrps" size="6" multiple="multiple"> 
				<%
		while (itr.hasNext()) {
			String groupName = itr.next();
	%>
			
			
			 <option value="<%= groupName %>"><%= groupName %></option>	
				
	<%} %>
			 </select>
			 </td>
			 <td><table><tr><td>
<input type="button" value=">>" onclick="moveoutid()"></td>
<td>
<input type="button" value="<<" onclick="moveinid()">
</td></tr>
</table>
</td>
			 <td>
					
	
		
	<select name="selcsrGrps" size="6" multiple="multiple" > 
		
	<option></option>
				</select>
				</td>
				
			 <td>
					
	<%
		List csrRoles = request.getAttribute("csrRoles")!= null?(ArrayList)request.getAttribute("csrRoles"):new ArrayList();			
		Iterator itrer = csrRoles.iterator();
		%>
		
	<div id="div1" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		<%
		while (itrer.hasNext()) {
			DataDAO cDao = (DataDAO)itrer.next();
	%>
			
			
			 <tr><td><input type="checkbox" name="roleId" value="<%= cDao.getRoleId() %>"><%= cDao.getRoleName() %></input>	
				</td>
			</tr>
	<%} %>
	
				</table></div>
				</td>
				<td>
					
	<%
		List csrDatabases = request.getAttribute("csrDatabases")!= null?(ArrayList)request.getAttribute("csrDatabases"):new ArrayList();			
		Iterator dbItrer = csrDatabases.iterator();
		%>
		
	<div id="div1" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		<%
		while (dbItrer.hasNext()) {
			UserDTO uDto = (UserDTO)dbItrer.next();
	%>
			
			
			 <tr><td><input type="checkbox" name="dbId" value="<%= uDto.getDbId() %>"/><%= uDto.getDbName() %>	
				</td>
			</tr>
	<%} %>
	
				</table></div>
				</td>
				 <td>
					
	
		
	<div id="div2" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		
			
			
			 <tr><td><input type="checkbox" name="active" value="active">Active</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="active" value="active">InActive</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="active" value="active">Disabled</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="active" value="active">Auto Disabled</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="active" value="active">Mark for Deletion</input>	
				
	</td></tr>
				</table></div>
				</td>
			</tr>
			<tr>
			<td colspan="5">
					
	
		<table align="center">
	
			
			
			 <tr><td>Validity Dates From:</td><td><input type="text" name="validdateFrom" id="validdateFrom"/>
			
				
	</td><td>To:
			
				
	</td><td><input type="text" name="validdateTo" id="validdateTo" />
			
				
	</td></tr>
	
	<tr><td>Validity Days From:</td><td><select name="validfromDays">
	<option value="monday">Monday</option>
	<option value="tueday">Tuesday</option>
	<option value="wednesday">Wednesday</option>
	<option value="thursday">Thursday</option>
	<option value="friday">Firday</option>
	
	</select>
				
	</td><td>to:</td>
	<td><select name="validtoDays">
	<option value="monday">Monday</option>
	<option value="tueday">Tuesday</option>
	<option value="wednesday">Wednesday</option>
	<option value="thursday">Thursday</option>
	<option value="friday">Firday</option>
	
	</select>
				
	</td></tr>
	<tr><td>Current Year:</td><td><select name="currYear">
	<option value="2011">2011</option>
	<option value="2012">2012</option>
	<option value="2013">2013</option>
	<option value="2014">2014</option>
	<option value="2015">2015</option>
	<option value="2016">2016</option>
	<option value="2017">2017</option>
	</select></input>	
				
	</td></tr>
	
				
			
			
			 <tr><td>Access Date From:</td><td><input type="text" name="accessdateFrom" id="accessdateFrom"/>
			
				
	</td><td>To:
			
				
	</td><td><input type="text" name="accessdateTo" id="accessdateTo" />
			
				
	</td></tr>
	
	<tr><td>Access Days From:</td><td><select name="accessfromDays">
	<option value="monday">Monday</option>
	<option value="tueday">Tuesday</option>
	<option value="wednesday">Wednesday</option>
	<option value="thursday">Thursday</option>
	<option value="friday">Firday</option>
	
	</select>
				
	</td><td>to:</td>
	<td><select name="accesstoDays">
	<option value="monday">Monday</option>
	<option value="tueday">Tuesday</option>
	<option value="wednesday">Wednesday</option>
	<option value="thursday">Thursday</option>
	<option value="friday">Firday</option>
	
	</select>
				
	</td></tr>
	
	<tr><td>Weekends:</td><td><input type="radio" name="weekend" value="yes">Yes <input type="radio" name="weekend" value="no">No</input>	
				
	</td></tr>
				</table>
				</td>
			</tr>

	<tr>
		<td><input type="Submit" value="Apply"></td>
	</tr>	
	</table>
	
	</s:form>
		</td></tr></table>
	
		</td></tr></table>
		<script type="text/javascript">
    Calendar.setup({
        inputField     :    "validdateFrom",      // id of the input field
        ifFormat       :    "%m/%d/%Y %I:%M %p",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	 Calendar.setup({
        inputField     :    "validdateTo",      // id of the input field
        ifFormat       :    "%m/%d/%Y %I:%M %p",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	 Calendar.setup({
        inputField     :    "accessdateFrom",      // id of the input field
        ifFormat       :    "%m/%d/%Y %I:%M %p",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	 Calendar.setup({
        inputField     :    "accessdateTo",      // id of the input field
        ifFormat       :    "%m/%d/%Y %I:%M %p",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
</script>
</body>
</html>