<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    
<s:include value="adminHeader.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxtabs.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<title>Insert title here</title>
<%
String serverHost =  request.getAttribute("serverHost")!= null? request.getAttribute("serverHost").toString():"";
//String serverIP =  request.getAttribute("serverIP")!= null? request.getAttribute("serverIP").toString():""; 
String serverPort =  request.getAttribute("serverPort")!= null? request.getAttribute("serverPort").toString():""; 
String softwarePath =  request.getAttribute("softwarePath")!= null? request.getAttribute("softwarePath").toString():""; 
String vaultPath =  request.getAttribute("vaultPath")!= null? request.getAttribute("vaultPath").toString():"";
String serverInstanceName= (String)(request.getAttribute("serverInstanceName")!= null? request.getAttribute("serverInstanceName").toString():"");


int archivalServerPort =  request.getAttribute("archivalServerPort")!= null? Integer.parseInt(request.getAttribute("archivalServerPort").toString()):0; 
int renderServerPort =  request.getAttribute("renderServerPort")!= null? Integer.parseInt(request.getAttribute("renderServerPort").toString()):0; 
int dataRouterServerPort =  request.getAttribute("dataRouterPort")!= null? Integer.parseInt(request.getAttribute("dataRouterPort").toString()):0; 
%>
<script language="javascript" type="text/javascript">
function cancelForm(button)
{
	//action="ActiveDirectoryAction"
	//alert(document.pressed);
	
		
	document.getElementById("archsubmitform").action = "ProfilesListAction.action";
	document.getElementById("archsubmitform").submit;
	return true;
			
	}
function submitForm(button)
{
	//action="ActiveDirectoryAction"
	//alert(document.pressed);
	var b = false,b1 = false;
	if(document.getElementById("serverHost").value == "" )
	{
	b = true;
	
	alert("Please Enter Server Host or Server IP");
	document.getElementById("serverHost").focus();
	return false;
	}
	
	if(document.getElementById("archivalServerPort").value == 0)
	{
	alert("Please Enter Archival Server Port");
	document.getElementById("archivalServerPort").focus();
	return false;
	}
	else
	{
		if(isNaN(document.getElementById("archivalServerPort").value)){
alert ("Please Enter Numeric Vaules for Archival Server Port ");
return false;
}
if(document.getElementById("archivalServerPort").value.length < 2)
		{
	alert ("Archival Server Port Should not be one number ");
return false;
		}
	}
	if(document.getElementById("renderServerPort").value == 0)
	{
	alert("Please Enter Render Server Port");
	document.getElementById("renderServerPort").focus();
	return false;
	}
	else
	{
		if(isNaN(document.getElementById("renderServerPort").value)){
alert ("Please Enter Numeric Vaules for Render Server Port ");
return false;
}

if(document.getElementById("renderServerPort").value.length < 2)
		{
	alert ("Render Server Port Should not be one number ");
return false;
		}
	}
	if(document.getElementById("dataRouterPort").value == 0)
	{
	alert("Please Enter Data Router Server Port");
	document.getElementById("dataRouterPort").focus();
	return false;
	}
	else
	{
		if(isNaN(document.getElementById("dataRouterPort").value)){
alert ("Please Enter Numeric Vaules for Data Router Server Port ");
return false;
}
if(document.getElementById("dataRouterPort").value.length < 2)
		{
	alert ("Data Server Port Should not be one number ");
return false;
		}
	}
	if(document.getElementById("softwarePath").value == "")
	{
	alert("Please Enter Server2 Vault Data Path");
	document.getElementById("softwarePath").focus();
	return false;
	}
	if(document.getElementById("vaultPath").value == "")
	{
	alert("Please Enter Server2 Vault Server Path");
	document.getElementById("vaultPath").focus();
	return false;
	}
	if(button.value != null || button.value != "")
			{
		if(button.value == "Test Connection")
			{
			document.getElementById("act").value = "test";
			//alert(document.getElementById("act").value);
			}
		document.getElementById("server").value = "server2";
	document.getElementById("archsubmitform").action = "VaultServer2Action.action";
	document.getElementById("archsubmitform").submit;
	return true;
			}
	else
		{
		return false;
		}
	}
	function submitHost(val)
{
	
	if(val.value != "")
	{
	document.getElementById("serverIP").disabled = true;
	}
	else
	{
		document.getElementById("serverIP").disabled = false;
	}
}
function submitIP(val)
{
	
	if(val.value != "")
	{
	document.getElementById("serverHost").disabled = true;
	}
	else
	{
		document.getElementById("serverHost").disabled = false;
	}
}
</script>
</head>
<body>
<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">


<jsp:include page="SystemCommonTabs.jsp" />

<div id="countrydivcontainer1" style="border:1px solid gray; width:1500px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">

<jsp:include page="ServerCommonTabs.jsp" />

<div id="countrydivcontainer2" style="border:1px solid gray; width:1460px; height:520px; margin-bottom: 1em; padding: 10px; overflow:auto">

<form id="archsubmitform" name="archsubmitform">	
<table width="100%" height="500" valign="top" align="center">
<tr><td valign="top">
<table width="100%" valign="top">

<tr >
<td align="left" colspan=2>
<b>e2 Vault Server2 Configuration:</b>
</td>
</tr>
<tr>
<td align="right" width="50%">
Server2 Host Name/ IP Address:
</td>
<td align="left">
<input type="text" name="serverHost" id="serverHost" value="<%=serverHost%>" onchange="return submitHost(this);"/>
</td>
</tr>

<tr>
<td align="right" width="50%">
Server2 Instance Name:
</td>
<td align="left">
<input type="text" name="serverInstanceName" id="serverInstanceName" value="<%=serverInstanceName%>" onchange="return submitHost(this);"/>
</td>
</tr>

<tr>
<td align="right">
Server2 Vault Data Path:
</td>
<td align="left">
<input type="text" name="softwarePath" id="softwarePath" value="<%=softwarePath%>" onchange="return submitIP(this);"/>

</td>
</tr>
<tr>
<td align="right">
Server2 Vault Server Path:
</td>
<td align="left">
<input type="text" name="vaultPath" id="vaultPath" value="<%=vaultPath%>"/>

</td>
</tr>
<tr>
<td align="right">
Archival Server2 Port Number:
</td>
<td align="left">
<input type="text" name="archivalServerPort" id="archivalServerPort" value="<%=archivalServerPort%>"/>

</td>
</tr>


<tr>
<td align="right">
Rendering Server2 Port Number:
</td>
<td align="left">
<input type="text" name="renderServerPort" id="renderServerPort" value="<%=renderServerPort%>"/>

</td>
</tr>
<tr>
<td align="right">
Data Router Server2 Port Number:
</td>
<td align="left">
<input type="text" name="dataRouterPort" id="dataRouterPort" value="<%=dataRouterServerPort%>"/>

</td>
</tr>
<tr align="center">
<td align="center" colspan="2">
<s:property value="message" />
</td>
</tr>
<tr>
<td align="center" colspan="2">
<input type="hidden" name="act" id="act"/>
<input type="hidden" name="server" id="server"/>
</td>

</tr>
<tr>
<td align="center" colspan="2">

<input type="submit" value="Test Connection"   onclick="return submitForm(this);"/> <input type="submit" value="Save"   onclick="return submitForm(this);"/> 
<input type="submit" value="Cancel"  onclick="return cancelForm();"/>
</td>

</tr>
</table>
</td>

</tr>
</table>
</form>
</div>
</div>
</div>

</body>
</html>