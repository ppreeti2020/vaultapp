<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dao.DataDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>e2 Vault Administration</title>
</head>
<%
String roleName =  request.getAttribute("roleName")!= null? request.getAttribute("roleName").toString():"";
String roleDesc =  request.getAttribute("roleDesc")!= null? request.getAttribute("roleDesc").toString():""; 
%>
<script language="javascript" type="text/javascript">

function submitForm(button)
{
	if(document.getElementById("roleName").value == "")
	{
	alert("Please Enter Role Name.");
	document.getElementById("roleName").focus();
	return false;
	}
	if(document.getElementById("roleDesc").value == "")
	{
	alert("Please Enter Role Description.");
	document.getElementById("roleDesc").focus();
	return false;
	}
	document.getElementById("roleForm").action = "ActiveSubmitAction.action";
	document.getElementById("roleForm").submit;
	return true;
			
	
	}
</script>

<body>
<table width="100%"><tr><td>
<td></tr><tr><td >
<table align="center"  width="100%">
<tr>
<td>
<s:include value="userrepCommonTabs.jsp" />
</td>
</tr>
<tr>
<td>
	
<table width="100%" border="1px solid #778" height="500" valign="top">

<tr><td valign="top">
<form name="roleForm"  id="roleForm" action="CreateRole">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
<table width="100%">
<tr>
<td valign="top" align="left" colspan=2>

</td>
</tr>
<tr>
<td valign="top" align="left" colspan=2><B>
New Role:</B>
</td>
</tr>
<tr>
<td width="50%" align="right">
<B>Role Name:</B>
</td>
<td width="50%"  align="left">
<input type="text" name="roleName" id="roleName" value="<%=roleName%>"/>
</td>
</tr>
<tr>
<td align="right">
<B>Role Description:</B>
</td>
<td align="left">
<input type="text" name="roleDesc" id="roleDesc" value="<%=roleDesc%>"/>

</td>
				</tr>
<tr><td align="center" colspan=2>											
						<input type="submit" value="Save"    onclick="return submitForm(this);"/>&nbsp;<input type="submit" value="Cancel"    onclick="return cancelForm(this);"/>
					
			

</td></tr></table>
</form>
</td></tr></table>
</td></tr></table>
		</td></tr></table>
</body>
</html>