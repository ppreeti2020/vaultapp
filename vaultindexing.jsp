<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dto.UserDTO" %>

  <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	
	<script src="js/jquery_treeview.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/demo.js"></script>
	
	<title>e2 Vault Administration</title>
	<s:include value="adminHeader.jsp" />
	<%	String host = (String)request.getAttribute("host");
	List<String> dbList = (ArrayList<String>)request.getAttribute("dbList");
	Long padmin = (Long)request.getAttribute("padmin");
	List<String> userProfileList = (ArrayList<String>)request.getAttribute("userProfileList");
	List<String> prePopulateList = (ArrayList<String>)request.getAttribute("prePopulateList");
	
	List indexResults = request.getAttribute("indexResults")!= null?(ArrayList)request.getAttribute("indexResults"): new ArrayList();
	//List indexResults = new ArrayList();
	Map<String,Long> detailIndex = (HashMap<String,Long>)request.getAttribute("detailIndex");
	List csrUsers = request.getAttribute("csrUsers")!= null?(ArrayList)request.getAttribute("csrUsers"):new ArrayList();		
	System.out.println("csrUsers"+csrUsers);
	String dbName = request.getAttribute("dbName")!= null?request.getAttribute("dbName").toString():"";
	  String userProfileName = request.getAttribute("userProfileName")!= null?request.getAttribute("userProfileName").toString():"";
	  String changeValue = request.getAttribute("changeValue")!= null?request.getAttribute("changeValue").toString():"";
	  Long userProfileId = request.getAttribute("userProfileId")!= null?(Long)request.getAttribute("userProfileId"):0L;
	  System.out.println("changeValue::"+changeValue);
	  System.out.println("User Profile Name::"+userProfileName);
	  System.out.println("database Name::"+dbName);
	  
	%>
	<script>
	function toggle(source) {
		var aInputs = document.getElementsByTagName('input');
		if(source.className == <%=padmin%> && source.checked==true){
			var r=confirm("Changing ProfileAdmin indexes will change all the profile Users\n Do you want to continue!!");
			if(r==true){
				for (var i=0;i<aInputs.length;i++) {
			    			    	
			    	 if (aInputs[i] != source && aInputs[i].className != source.className) {
			        	aInputs[i].disabled = true;
			        		        		
			        }
			    }
			}
		}
	    for (var i=0;i<aInputs.length;i++) {
	    		    	
	    	 if (aInputs[i] != source && aInputs[i].className == source.className) {
	        	if(source.checked == true){
	        		aInputs[i].disabled = false;
	        	}
	        	else
	        		aInputs[i].disabled = true;
	        		
	        }
	    }
	}
	
	function cancelForm(button)
	{
		//action="ActiveDirectoryAction"
		//alert(document.pressed);
		
			
		document.getElementById("indexsearchform").action = "DBIndexAction.action";
		document.getElementById("indexsearchform").submit;
		return true;
				
		}
	
	function ValidateForm(){
		if(document.getElementById("changeValue").value=="ProfileIndexes"){
		var r=confirm("Changes will update all the profile Users \n Do you want to continue!!");
			if(r==true){
				return true;
			}
			else
				return false;
			}
		else
			return true;
		
	}
	
	function getResults(val,url,userId,dbId,index)
	  {
		 window.open(url+"?userId="+userId+"&dbId="+dbId+"&index="+index+"&res="+val.value,'IndexResults','width=300,height=250,toolbar=no,resizable=yes,menubar=yes,scrollbars=yes')
	  }
	
	function Ajax(popUrl,val,userId,dbId,index) {
		  var xmlHttpReq = false;
		  var self1 = this;
		  var stdin='';
		  // Mozilla/Safari
		  var url = "AjaxIndexAction.action?userId="+userId+"&dbId="+dbId+"&index="+index;
		 // alert(val.checked);
		 if(val.checked == true)
			{
		  if (window.XMLHttpRequest) {
		    xmlHttpReq = new XMLHttpRequest();
		  }
		  // IE
		  else if (window.ActiveXObject) {
		    xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  if (!xmlHttpReq) {
		    alert('ERROR AJAX:( Cannot create an XMLHTTP instance');
		    return false;
		  }   
		  xmlHttpReq.open('POST', url, true);
		  //xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		  xmlHttpReq.onreadystatechange = function() {
			 if (xmlHttpReq.readyState == 4 )
			  {
				if(xmlHttpReq.status == 200)
				{
					var list = xmlHttpReq.responseXML.getElementsByTagName("IndexResults")[0].childNodes[0].nodeValue;
				    var grid="";
			 		if(list != null)
					  {
				if(list == "More than 100 Results!!")
						  {
					//alert(list);
						  }
						  if(list != "More than 100 Results!!")
						  {
							  //alert("hi"+"res_"+userId+"_"+dbId+"_"+index);
			
			  grid += "<select name='sel' id='sel'  onchange=getResults(this,'<s:url value='"+popUrl+"'/>','"+userId+"','"+dbId+"','"+index+"') style='width:100px;'><option value='all'>Select</option><option value='"+list+"'>"+list+"</option></select>";
			  document.getElementById("res_"+userId+"_"+dbId+"_"+index).innerHTML = grid;
					  }
					  }
				  }  
			  }
			  }
		  }
		  
		  xmlHttpReq.send(stdin);
		}
	
  	</script>
  	
  	<title>tables</title>
  	<style type="text/css">
  		#playlist tr.header {
      		background-color:#0072a8;
    	}
    	#playlist tr.even td {
      		background-color: #eee;
    	}
    	#playlist tr.odd  td {
      		background-color: #fff;
    	}
    	#navigation a.active,a.visited, a.hover {
			color: black;
			background-color: transparent;
		}
	</style>
</head>

<body>
	<jsp:include page="CommonTabs.jsp" />
	<%--Nithu Alexander: 17 Aug 2015, Changing width from 1200px to 100%, height from 610px to 100% --%>
	<div id="countrydivcontainer" style="border:1px solid gray; width:100%; height:100%; margin-bottom: 1em; padding: 10px; overflow:auto">


	<jsp:include page="userprofadminCommonTabs.jsp" />
	<div id="countrydivsubcontainer" style="border:1px solid gray; width:1150px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">

							<table width="100%" border="1" valign="top" height="450">
								<tr><td width="20%"  valign="top">
									<%
  									  if(host != null)
						              { %>
										<ul id="browser" class="filetree treeview">
										<li><%= host%>
										<ul>
										<%if(changeValue != ""){
											if(changeValue != "" && changeValue.equalsIgnoreCase("ProfileIndexes") )
										  {%>
											<li>
											<a href="DBIndexAction.action?changeValue=ProfileIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">ProfileIndexes</font></a>
											<ul>
											<%for(Iterator iter = userProfileList.iterator();iter.hasNext();)
										  	{   UserDTO uDTO1 = (UserDTO)iter.next();
											    System.out.println("userProfileName...."+uDTO1.getUserProfileName());%>
											 	<%if(userProfileName != "" && userProfileName.equals(uDTO1.getUserProfileName())) 
											  { %>
												<li>
													<a href="DBIndexAction.action?userProfileName=<%=uDTO1.getUserProfileName()%>&changeValue=ProfileIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300"><%=uDTO1.getUserProfileName()%></font></a>
													<ul>
													<% for(Iterator iter1 = dbList.iterator();iter1.hasNext();)
														{ UserDTO uDTO = (UserDTO)iter1.next();
														  System.out.println("dbname...."+uDTO.getDbName());%>
						                                  <li><% if(dbName != "" && dbName.equals(uDTO.getDbName()))
						                                      	 {%><a href="DBIndexAction.action?dbName=<%=uDTO.getDbName()%>&changeValue=ProfileIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>&userProfileName=<%=uDTO1.getUserProfileName()%>" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300"><%=uDTO.getDbDesc()%></font></a>
																<%}else 
																  {%> <a href="DBIndexAction.action?dbName=<%=uDTO.getDbName()%>&changeValue=ProfileIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>&userProfileName=<%=uDTO1.getUserProfileName()%>"><%=uDTO.getDbDesc()%></a>
																<%}%>
														  </li>
				    							  	  <%}%>
				   									</ul>
											<%}else 
											  {%>
												<li>
													<a href="DBIndexAction.action?userProfileName=<%=uDTO1.getUserProfileName()%>&changeValue=ProfileIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>"><%=uDTO1.getUserProfileName()%></a>
			  								<%}%>
			  									</li>
			  									</ul>
			  									</li>
										<% } 
										} else{%>
											<li>
											<a href="DBIndexAction.action?changeValue=ProfileIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">ProfileIndexes</font></a>
											</li>
										<%} if(changeValue != "" && changeValue.equalsIgnoreCase("UserIndexes"))
											
											{ %><li>
											<a href="DBIndexAction.action?changeValue=UserIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">UserIndexes</font></a>
											<ul>
											<%for(Iterator iter = userProfileList.iterator();iter.hasNext();)
										  	{   UserDTO uDTO1 = (UserDTO)iter.next();
											    System.out.println("userProfileName...."+uDTO1.getUserProfileName());%>
											 	<%if(userProfileName != "" && userProfileName.equals(uDTO1.getUserProfileName())) 
											  { %>
												<li>
													<a href="DBIndexAction.action?userProfileName=<%=uDTO1.getUserProfileName()%>&changeValue=UserIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300"><%=uDTO1.getUserProfileName()%></font></a>
													<ul>
													<% for(Iterator iter1 = dbList.iterator();iter1.hasNext();)
														{ UserDTO uDTO = (UserDTO)iter1.next();
														  System.out.println("dbname...."+uDTO.getDbName());%>
						                                  <li><% if(dbName != "" && dbName.equals(uDTO.getDbName()))
						                                      	 {%><a href="DBIndexAction.action?dbName=<%=uDTO.getDbName()%>&changeValue=UserIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>&userProfileName=<%=uDTO1.getUserProfileName()%>" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300"><%=uDTO.getDbDesc()%></font></a>
																<%}else 
																  {%> <a href="DBIndexAction.action?dbName=<%=uDTO.getDbName()%>&changeValue=UserIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>&userProfileName=<%=uDTO1.getUserProfileName()%>"><%=uDTO.getDbDesc()%></a>
																<%}%>
														  </li>
				    							  	  <%}%>
				   									</ul>
											<%}else 
											  {%>
												<li>
													<a href="DBIndexAction.action?userProfileName=<%=uDTO1.getUserProfileName()%>&changeValue=UserIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>"><%=uDTO1.getUserProfileName()%></a>
			  								<%}%>
			  									</li>
			  									</ul></li>
										<% } 
										}else{%>
											<li>
											<a href="DBIndexAction.action?changeValue=UserIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">UserIndexes</font></a>
											</li>
										<%}%>
										
									<%}else 
										{ %>
										<li>
											<a href="DBIndexAction.action?changeValue=ProfileIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">ProfileIndexes</font></a>
										</li>
										<li>
											<a href="DBIndexAction.action?changeValue=UserIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">UserIndexes</font></a>
										</li>
										<%}%>
			  							</ul></li></ul>
									<%}%>
									</td>
									<td width="80%"  valign="top">
									<s:form action="UserIndexAction" id="indexsearchform" class="indexsearchform">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
									<table border="0" align="center" width="100%">	
										<tr><td align="left"><b>Search Index Control:</b></td></tr>
										<tr><td><input type="hidden" name="dbName" id="dbName" value="<%=dbName%>"/></td>
															<td><input type="hidden" name="userProfileName" id="userProfileName" value="<%=userProfileName%>"/></td>
															<td><input type="hidden" name="dbName" id="dbName" value="<%=dbName%>"/></td>
															<td><input type="hidden" name="userProfileId" id="userProfileId" value="<%=userProfileId%>"/></td>
															<td><input type="hidden" name="changeValue" id="changeValue" value="<%=changeValue%>"/></td>
															
														</tr>
										<tr><td>
										<center><s:if test="hasActionErrors()"> <s:actionerror /></s:if></center>
													
													<table border="0" align="center" width="100%">	
														<%	
												UserDTO dto = new UserDTO();
												
												System.out.println("pop"+request.getAttribute("prePopulateList"));
												System.out.println("indexResult"+indexResults);
												int i=1;
												if (dbName != "" && csrUsers.size() != 0 && detailIndex.size()==0) {%>
												<table align="left">
												<tr>
													<td align="left"><font color="red">No Indexes configured for selected Database!!</font>
													</td>
												</tr>

											 </table>
											<%}
												
												else if(dbName != "" && csrUsers.size()!=0)
												{	
													System.out.println("csrUsers:if:"+csrUsers.size());
													String noRes = "None";
													String nor = "";
													String nor1 = "";
													
												%>
												
														<%--Nithu Alexander: 24 Aug 2015, Changing Style parameter in dataTable from "width:100px;position:static" to "position:static" --%>
														
														<tr><td>
															<display:table name="csrUsers" id="Accs" class="dataTable" style="position:static" pagesize="15" requestURI="/DBIndexAction.action">
															<display:setProperty name="paging.banner.onepage" value=" " />
															<display:column sortable="true" title="ID"><%=(i++)%></display:column>
															<display:column property="userName" sortable="true" title="Users"></display:column>
															<!-- Nithu Alexander: 22/09/2014
															Change to Display the FullName of every UserId -->
															<display:column property="userDisplayName" sortable="true" title="User Name"></display:column>
															
															<% if(pageContext.getAttribute("Accs")!= null)
		   													   {dto = ((UserDTO) pageContext.getAttribute("Accs")); 
		   														
																System.out.println("XXXXAccs id"+dto.getUserId());%>
																<%for(String key:detailIndex.keySet()) 
																{ System.out.println("Prasanna>>>>>>>>>>>>>");
		 														  nor1 = "";
		 														 
																  if (prePopulateList != null && prePopulateList.contains("Index_"+dto.getUserId()+"_"+dto.getDbId()+"_"+key)) 
																  {	System.out.println(dto.getUserId());%>
																  	
																	
																	<display:column sortable="true" title='<%=key%>'>  
																	<input type="checkbox" name="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" class="<%=dto.getUserId()%>" id="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" value="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" onclick='Ajax("<s:url value="ResultIndexAction.action"/>",this,"<%=dto.getUserId()%>","<%=dto.getDbId()%>","<%=key%>")'  checked/>
																	<input type="hidden" name="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" id="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" value="unchecked" />
																	</display:column>
																	
																	 <div id="dis"><display:column sortable="true"   title='Filter'><div id="res_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>">
																	
																	<%for(Iterator iter=indexResults.iterator();iter.hasNext();){
																		UserDTO dtoRes = (UserDTO) iter.next();
																		//nor1 = "";
																		
																		System.out.println("^^^^^^^^^^^^^^^^^^^^^^ index result: " + dtoRes.getIndexResults());
																		System.out.println(key+"dtoRes:"+dtoRes.getIndexName()+"--dtoRes.getIndexResults()"+dtoRes.getIndexResults());
																		if(dto.getUserId() == dtoRes.getUserId() && dto.getDbId() == dtoRes.getDbId() && key == dtoRes.getIndexName() && dtoRes.getIndexResults() != ""){
																			nor1 = "yes";%>
																			<%= dtoRes.getIndexResults()%>
																		<% }
																		else if(dto.getUserId() == dtoRes.getUserId() && dto.getDbId() == dtoRes.getDbId() && key == dtoRes.getIndexName() && dtoRes.getIndexResults() == "")
																		{	nor = "All";
																		}
																	}
																	if(nor.equals("All") &&  !nor1.equals("yes") )
																	{%>
																		<%=nor%>
																	<%}%>
																	</div>
																	</display:column></div>
																<%} else { %>
		 															<display:column sortable="true" title='<%=key%>'>
   				 													<input type="checkbox" name="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" class="<%=dto.getUserId()%>" id="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" value="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" onclick='Ajax("<s:url value="ResultIndexAction.action"/>",this,"<%=dto.getUserId()%>","<%=dto.getDbId()%>","<%=key%>")' />
																	<input type="hidden" name="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" id="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" value="unchecked" />
																	</display:column>
																	 <div id="dis">
																	<display:column  sortable="true" title='Filter'>
																	<div id="res_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>"><%=noRes%></div>
																	</display:column></div> 
																								
																
																<%}
		  														}
															   }%>
															</display:table>
															</td>
														</tr>
														<tr><td align="left">
															<div id="mes"></div>
															<%if(request.getAttribute("message")!= null)
															  { String message = (String)request.getAttribute("message"); %>
															   <font color="red"><%= message%></font>
															<%}%>
															</td>
														</tr>
														<tr><td colspan="<%=i%>" align="left" valign="center"><input type="Submit" value="Apply" onclick="return ValidateForm()"/>&nbsp;&nbsp;<input type="Submit" value="Cancel" onclick="return cancelForm(this);"></td></tr>	
													
											  <%}
												else
												{%>
												<table align="left"><tr ><td align="left"><font color="red">No Users Found</font></td></tr></table>
											  <%}%>
											  </table>
												
											</td>
										</tr>
								 </table>
								 </s:form>
								 </td>
							</tr>
						</table>
						</div></div>

</body>
</html>