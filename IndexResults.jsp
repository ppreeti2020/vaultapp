<%@page import="java.util.ArrayList"%>
<%@page import="com.g1.e2.vault.VaultClient.Account"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dto.UserDTO" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<s:include value="adminHeader.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Index Results</title>
<%
String message = request.getAttribute("message")!=null?(String) request.getAttribute("message"):"";
List indexResults = (ArrayList)session.getAttribute("indexResults");
List<String> selectedIndexList = new ArrayList<String>();
UserDTO dto=null;
if(indexResults!=null)
{
System.out.println("@@@@@@@@@@@@@@@@@@@@@ indexResults size in IndexResults.jsp : " + indexResults.size());	
//session.removeAttribute("indexResults");

for(int i=0; i<indexResults.size(); i++)
{
	 dto = (UserDTO)indexResults.get(i);
	
	 System.out.println(":::::::::::::: " + dto.getIndexName());
	 System.out.println(":::::::::::::: " + dto.getUserId());
	 System.out.println(":::::::::::::: " + dto.getDbId());
	 System.out.println(":::::::::::::: " + dto.getIndexResults());
	 System.out.println(":::::::::::::: userIndexId : " + dto.getUserIndexId());
	 
}




}
%>
<script language="javascript">


function save()
{
	var checked = false;
	var index = document.getElementsByName("indexMatch");
	var update = "updated";
	
	for(var v=0;v<index.length;v++)
	{
		if(index[v].checked == true)
		{
			checked = true;
		}
	}
	if(checked)
	{
			
		document.getElementById("submitform").action = "ResIndexAction.action";
		document.getElementById("submitform").submit;
	
		return true;
	}
	if(!checked)
	{
		alert("Please select atleast one Search Results");
		return false;
	}
	
	//window.opener.location.reload();
	//window.location.reload(); 
	//opener.document.getElementById("mes").innerHTML="<font color='red'>Successfully Updated</font>";
	//window.close();
	
}
function func()
{
var update = "updated";
if(update == '<%=message%>')
	{
	 var grid = "<font color='red'>Successfully Updated</font>";
		window.opener.location.reload(); 
		window.location.reload();
		//opener.document.getElementById("mes").innerHTML=grid;
		window.close();
	}
}

function defaultCheck()
{
var res	
}

function checkAll(val)
{
	var index = document.getElementsByName("indexMatch");
	for(var v=0;v<index.length;v++)
	{
		
		if(index[v].checked != true)
		{
			index[v].checked = true;
		}
		/*
		//Changes done as checked == true is making the functionality as toggle the checked box.
		else
		if(index[v].checked == true)
		{
		index[v].checked = false;
		}
		*/
	}
	return false;
}
</script>
</head>
<body onload="func()">
<form id="submitform" name="submitform">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
<table>
<% List indexres = request.getAttribute("indexResults")!=null?(ArrayList)request.getAttribute("indexResults"):new ArrayList();
System.out.println("userIndexId"+request.getAttribute("userIndexId"));
Long userIndexId = request.getAttribute("userIndexId") != null? Long.parseLong(request.getAttribute("userIndexId").toString()):0L;
System.out.println("::::::::::::: UserIndexId : " + userIndexId);
//Nithu Alexander: 15/09/2014 (Retrieving request parameters from url)
String changeValue = request.getAttribute("changeValue")!= null?request.getAttribute("changeValue").toString():"";
 
%>
<tr>
<td><input type="hidden" name="userIndexId" id="userIndexId" value="<%=userIndexId%>"/></td></tr>
<%
for(int i=0; i<indexResults.size(); i++)
{
	 dto = (UserDTO)indexResults.get(i);
	 System.out.println("**** dto.getIndexId() : " + dto.getUserIndexId());
	 System.out.println("**** userIndexId : " + userIndexId);
if(dto.getUserIndexId().equals(userIndexId))
{
	System.out.println(":::::::::::::: main if");
	
	String[] selectedIndexes=dto.getIndexResults().split(",");
	for(int j=0; j<selectedIndexes.length; j++)
	{
		System.out.println("------ before trim:"+selectedIndexes[j]);
		System.out.println("------ after trim:"+selectedIndexes[j].trim());
		/* Nithu Alexander : 04/09/2014
		 * Addition of Code to avoid EMPTY STRING to appear in IndexResults
		 * Popup of Search Index Control.
		 * Putting condition to avoid adding EMPTY STRING in selectedIndexList List.
		 * Checking for EMPTY STRING in selectedIndexes array [Data for from USER_INDEX_RESULTS and stored in session]
		 * Previous Code Snippet: selectedIndexList.add(selectedIndexes[j].trim());
		 */
		 
		 if(selectedIndexes[j] != null && !(selectedIndexes[j].trim().equals(""))){
			selectedIndexList.add(selectedIndexes[j].trim());
		 }

	}
	
for(Iterator iter=indexres.iterator();iter.hasNext();)
{
	String acc = (String) iter.next();
	%>
	
	<%
	if(selectedIndexList.contains(acc)){
		System.out.println(":::::::::::::: if");
	%>
<tr>

	<td><input type="checkbox" name="indexMatch" id="indexMatch" checked="checked" value="<%=acc%>"/><%=acc%></td>

	
</tr>
<%
	}else{
		System.out.println(":::::::::::::: else");
%>

<tr>

	<td><input type="checkbox" name="indexMatch" id="indexMatch" value="<%=acc%>"/><%=acc%></td>

	
</tr>


<%
	}
%>


<%}}}%>

<tr><td><input type="submit" name="selectAll" value="selectAll" onclick="return checkAll(this)"/><input type="submit" name="selectAll" value="Apply" onclick="return save();"/></td></tr> 
</table>
</form>
</body>
</html>