<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dto.DataDTO" %>
<%@page import="com.pb.common.CommonConstants" %>
<%@page import="com.pb.common.PropertyUtils" %>

<s:include value="adminHeader.jsp" />
  <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />
<% String searchReport = "";%>
<script type="text/javascript" src="js/ajaxtabs.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<style>
.AccordionTitle, .AccordionContent, .AccordionContainer
{
  position:relative;
  
}

.AccordionTitle
{
  height:20px;
  overflow:hidden;
  cursor:pointer;
  font-family:Arial;
  font-size:8pt;
  font-weight:bold;
  vertical-align:top;
  text-align:center;
  background-repeat:repeat-x;
  display:table-cell;
  -moz-user-select:none;
}

.AccordionContent
{
  height:0px;
  
  overflow:auto;
  display:none; 
  
}

.AccordionContainer
{
  border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;
}
</style>
<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	
	<script src="js/jquery_treeview.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="js/demo.js"></script>
<title>e2 Vault Administration</title>

  <title>tables</title>
  <style type="text/css">
  #playlist tr.header {
      background-color:#0072a8;
    }
    #playlist tr.even td {
      background-color: #eee;
    }
    #playlist tr.odd  td {
      background-color: #fff;
    }
  </style>
<script language="javascript" type="text/javascript">

var TimeToSlide = 250.0;
var ContentHeight = 100;
var openAccordion = '';

function Ajax(id,stdin,stdout,type,size) {
  var xmlHttpReq = false;
  var self1 = this;
  // Mozilla/Safari
  var url = "ResourceReportAction.action?type="+type+"&logFileId="+id;
// alert(stdout);
  if (window.XMLHttpRequest) {
    xmlHttpReq = new XMLHttpRequest();
  }
  // IE
  else if (window.ActiveXObject) {
    xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
  }
  if (!xmlHttpReq) {
    alert('ERROR AJAX:( Cannot create an XMLHTTP instance');
    return false;
  }   
  xmlHttpReq.open('POST', url, true);
  //xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xmlHttpReq.onreadystatechange = function() {
	  //var table = document.getElementById("searchList");
		/* if(table != null)
			  {
    var rows = table.getElementsByTagName("tr");
    for (i = 1; i < rows.length; i++) {
		document.getElementById("AccordionContent"+i).style.display = 'none';
	}
	document.getElementById("AccordionContent"+stdout).style.display = 'block';
	
	document.getElementById("AccordionContent"+stdout).innerHTML = "loading..";*/
	//alert(xmlHttpReq.readyState);
    if (xmlHttpReq.readyState == 4 )
	  {
		//xmlHttpReq.responseText
		if(xmlHttpReq.status == 200)
		  {
			alert(size);
		for(var v=1;v<=15;v++)
			  {
			//alert(v+" "+stdout);
			 if(v != stdout)
				  {
document.getElementById("uparrow"+v).src = 'images/iconDownArrow_20x20.gif';
				  }

			if(v == stdout)
				  {
			document.getElementById("uparrow"+stdout).src = 'images/ArrowUp.jpg';
				  }
				 
			  }
			var list = xmlHttpReq.responseXML.getElementsByTagName("Resources")[0].childNodes[0].nodeValue;
		// alert(list);
	document.getElementById("AccordionContent"+stdout).style.display = 'block'
		document.getElementById("AccordionContent"+stdout).innerHTML=list;
		
	


var nID = "AccordionContent"+stdout ;
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  if(openAccordion != "")
	document.getElementById("uparrow"+stdout).src = 'images/iconDownArrow_20x20.gif';
  openAccordion = nID;
		 
	  }
	  }
  }
  xmlHttpReq.send(stdin);
}
function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate(" + curTick + "," + timeLeft + ",'" 
      + closingId + "','" + openingId + "')", 33);
}
</script>
	
</head>
<%String host = (String)request.getAttribute("host");
List populateLogList = request.getAttribute("populateLogList")!= null?(ArrayList)request.getAttribute("populateLogList"):new ArrayList();
String host = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.HOST_NAME);
int size = populateLogList.size();%>
<body>
<table width="100%"><tr><td>
<s:include value="CommonTabs.jsp" />
<td></tr><tr><td >
<table align="center"  width="100%">

<tr>
<td valign="top">
<table width="100%">
<tr><td valign="top">
<table width="100%" border="1" valign="top" height="450">
<tr>
<td width="20%"  valign="top">
<%Long logFileId = request.getAttribute("logFileId")!= null?Long.parseLong(request.getAttribute("logFileId").toString()):0L;
%>
<ul id="browser" class="filetree">
	<li><span class="folder"><%= host%></span>
		<ul>
			<li><span class="folder">Archival</span>
				<ul>
					<li><span class="file">
					<a href="ResourceReportAction.action?name=Indexing">Indexing</a>
					</span></li>
					<li><span class="file">
					<a href="ResourceReportAction.action?name=Reindexing">Reindexing</a>
					</span></li>
					<li><span class="file">
					<a href="ResourceReportAction.action?name=Purging">Purging</a>
					</span></li>
					<li><span class="file">
					<a href="ResourceReportAction.action?name=Kill">Kill Document</a>
					</span></li>
					<li><span class="file">
					<a href="ResourceReportAction.action?name=Replication">Replication</a>
					</span></li>
				</ul>
			</li>
		</ul>
		<ul>
			<li><span class="folder">Renderer</span>
				<ul>
					<li><span class="file">
					<a href="ResourceReportAction.action?name=Renderer">Renderer</a>
					</span></li>
					
				</ul>
			</li>
		</ul>
		<ul>
			<li><span class="folder">Profiles</span>
				<ul>
					<li><span class="file">
					<a href="ResourceReportAction.action?name=Profile1">Profile1 </a>
					</span></li>
					
				</ul>
			</li>
		</ul>
	</li>
</ul>

</td>
<td width="80%"  valign="top">
<ul id="countrytabs" class="shadetabs">

<li><a href="AuditAction.action" rel="countrycontainer">Log</a></li>

<li><a href="ResourceReportAction.action" rel="#default" class="selected">Resources</a></li>
</ul>
<div id="countrydivcontainer" style="border:1px solid gray; width:1300px; height:550px; margin-bottom: 1em; padding: 10px; overflow:auto">
<table border="0" align="center" width="100%">	
<tr ><td align="left">
		<b>Search Index Control:</b>
		</td>
</tr>
<tr><td>
<%
			
			
				int i=1;
			int num = 1;
				DataDTO cd = new DataDTO();
		%>
			<s:form action="UserIndexAction">	
<table border="0" align="center" width="100%">	
<tr><td><input type="hidden" name="logFileId" id="logFileId" value="<%=logFileId%>"/></td></tr>
		<tr>
			<td>
			<display:table name="populateLogList" class="dataTable" id="Accs" pagesize="15" requestURI="/ResourceReportAction.action">
			<display:column sortable="true" title="ID"><%=(i++)%></display:column>
			<display:column property="logDate" sortable="true" title="Date Of Archival/Injestion"></display:column>
			<display:column property="drpFile" sortable="true" title="Drp File Name"></display:column>

			
			<display:column sortable="true" style="width:150px" title="Search Results"><%  cd = ((DataDTO) pageContext.getAttribute("Accs")); %>
	
	<div onclick="Ajax('<%= cd.getLogFileId()%>','',<%=num%>,'resources',<%=size%>);" class="AccordionTitle" onselectstart="return false;">    
    <%= cd.getResourceCount()%><img src="images/iconDownArrow_20x20.gif" width="15" height="15" id="uparrow<%=num%>"/>
   </div>
  <div id="AccordionContent<%=num%>" class="AccordionContent"></div>
	<% num++; %>
	
	</display:column>
		
	
		</display:table>
			</td>
			</tr>
	
	</table>
	</s:form>
	</td>
	</tr>
	<% System.out.println("num:"+num);%>
</table>
	</div>
	</td>
</tr>

</table>
</td>
</tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</body>
</html>