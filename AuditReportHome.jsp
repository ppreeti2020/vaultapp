<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dto.DataDTO"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="adminHeader.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	
	<script src="js/jquery_treeview.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>

	
</head>

<%
String serverHost =  request.getAttribute("serverHost")!= null? request.getAttribute("serverHost").toString():"";
String serverIP =  request.getAttribute("serverIP")!= null? request.getAttribute("serverIP").toString():""; 
String serverPort =  request.getAttribute("serverPort")!= null? request.getAttribute("serverPort").toString():""; 
String softwarePath =  request.getAttribute("softwarePath")!= null? request.getAttribute("softwarePath").toString():""; 
String vaultPath =  request.getAttribute("vaultPath")!= null? request.getAttribute("vaultPath").toString():"";
int archivalServerPort =  request.getAttribute("archivalServerPort")!= null? Integer.parseInt(request.getAttribute("archivalServerPort").toString()):0; 
int renderServerPort =  request.getAttribute("renderServerPort")!= null? Integer.parseInt(request.getAttribute("renderServerPort").toString()):0; 
int dataRouterServerPort =  request.getAttribute("dataRouterPort")!= null? Integer.parseInt(request.getAttribute("dataRouterPort").toString()):0;

%>
<body>
<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">
<table width="100%" height="600" valign="top" align="center" border="1">
<tr><td width="20%" valign="top">
<jsp:include page="AuditCheckLeftPanel.jsp" />
</td>

<td valign="top" width="80%" >
<table width="100%" valign="center" class="dataTable">

<tr>
<td align="left" colspan="2">
<b>Details of e2 Vault Server Running On:</b>
</td>
</tr>
<tr>
<td align="left" colspan="2">

</td>
</tr>
<tr>
<td width="20%">
</td>

<td valign="center" width="80%">
<table valign="center" width="600px" border="1">
<tr>
<td align="right" width="50%">
Server1 Host Name/IP Address:
</td>
<td align="left">
<%=serverHost%>
</td>
</tr>

<tr>
<td align="right">
Server1 Vault Data Path:
</td>
<td align="left">
<%=softwarePath%>

</td>
</tr>
<tr>
<td align="right">
Server1 Vault Server Path:
</td>
<td align="left">
<%=vaultPath%>

</td>
</tr>
<tr>
<td align="right">
Archival Server1 Port Number:
</td>
<td align="left">
<%=archivalServerPort%>

</td>
</tr>


<tr>
<td align="right">
Rendering Server1 Port Number:
</td>
<td align="left">
<%=renderServerPort%>

</td>
</tr>
<tr>
<td align="right">
Data Router Server1 Port Number:
</td>
<td align="left">
<%=dataRouterServerPort%>

</td>
</tr>
</table>
</td></tr>

</table>
</td>

</tr>
</table>

</div>

</body>
</html>