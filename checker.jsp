<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>CSR Administration</title>

</head>
<body>
<div id="page">
	<div id="header" class="clearfix">
		<div id="branding">
			<img id="logo" src="images/header/logo.JPG"/>
		</div>
		<div id="applicationDetail">
			<ul id="productName">
				<li>EngageOne Customer Service Representative - Administration</li>
			</ul>
			<ul id="userDetail">
			</ul>
			<table border="0" align="right">
			<tr>
			<td>
			<form name="Logout" action="<%=request.getContextPath()%>/Logout.action" >
        		<%
        		out.println("<font color='white'><input type='button' onClick=\"window.location.href='Interface';\" value='Home'/></font>");	
        		if(session.getValue("GROUP_MAKER").equals("true"))
        		{
        			out.println("<font color='white'><input type='button' onClick=\"window.location.href='MakerSubmitAction.action';\" value='Setup Permission'/></font>");	
        		}
        		if(session.getValue("GROUP_CHECKER").equals("true"))
    			{
    				out.println("<font color='white'><input type='button' onClick=\"window.location.href='CheckerSubmitAction.action';\"  value='Approve Permission'/></font>");	
    			}
        	%>
        	<input type="submit" value="Logout" />
        	</form>
        	</ul>
	</td>
	</tr>
	</table>
		</div>
		<div id="curve">
			<img id="curve" src="styles/themes/purple/images/curve.gif" />
		</div>
		<hr />
	</div>
	
	<h3>
		<s:property value="message" />		
	</h3>
	
	<s:form action="CheckerSubmitAction">
	<% 
	int status = Integer.parseInt(request.getAttribute("status").toString());
	String searchIndexValues = (String)request.getAttribute("searchIndexValues");				
	String[] searchIndexValuesArr = searchIndexValues.split(",");
	Iterator<String> itr = null;
	
	List<String> csrGroups = (ArrayList<String>)request.getAttribute("csrGroups");			
	if(status==1){ %>
	<center><h3>Submit for Approval Permission</h3></center>
	<table border="0" align="center">		
		<tr>
			<td>
				<b>CSR Group</b>
			</td>
			
			<%
				List<String> prePopulateList = (ArrayList<String>)request.getAttribute("prePopulateList");
			
				
				for (String sidx : searchIndexValuesArr) {
			%>
					<td><B><%=sidx%></B></td>
					<td>&nbsp;</td>					
			<%		
				}
			%>
			
		</tr>
	<%
		
		itr = csrGroups.iterator();
		while (itr.hasNext()) {
			String groupName = itr.next();
	%>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>	
					<%=groupName%>
				</td>
				
				<%
				for (String sidx : searchIndexValuesArr) {
				%>
				<%if (prePopulateList != null && prePopulateList.contains("LDAP_"+groupName+"_"+sidx)) {%>
					<td><input type="checkbox" name="LDAP_<%=groupName%>_<%=sidx%>" id="LDAP_<%=groupName%>_<%=sidx%>" value="LDAP_<%=groupName%>_<%=sidx%>" checked disabled='true'/></td>
					<td>&nbsp;</td>
				<%} else { %>
					<td><input type="checkbox" name="LDAP_<%=groupName%>_<%=sidx%>" id="LDAP_<%=groupName%>_<%=sidx%>" value="LDAP_<%=groupName%>_<%=sidx%>" disabled='true'/></td>
					<td>&nbsp;</td>				
				<%} %>
				<%} %>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>		
	<%} %>
	<tr>
		<td></td>
	</tr>	
	</table>
	<input type="hidden" value="true" name="approve_permission">
	<input type="Submit" name="post_submit"  value="Apply">
	<% } %>
	</form>
	<form method="post">
	<% if(status==1){ %>
	<input type="Submit" name="post_submit" value="Reject">
	<% } %>
	<center><h3>Current</h3></center>
	<table border="0" align="center">		
		<tr>
			<td>
				<b>CSR Group</b>
			</td>
			
			<%
				List<String> nowPopulateList = (ArrayList<String>)request.getAttribute("nowPopulateList");
				for (String sidx : searchIndexValuesArr) {
			%>
					<td><B><%=sidx%></B></td>
					<td>&nbsp;</td>					
			<%		
				}
			%>
			
		</tr>
	<%
		itr = csrGroups.iterator();
		while (itr.hasNext()) {
			String groupName = itr.next();
	%>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>	
					<%=groupName%>
				</td>
				
				<%
				for (String sidx : searchIndexValuesArr) {
				%>
				<%if (nowPopulateList != null && nowPopulateList.contains("LDAP_"+groupName+"_"+sidx)) {%>
					<td><input type="checkbox" name="LDAP_<%=groupName%>_<%=sidx%>" id="LDAP_<%=groupName%>_<%=sidx%>" value="LDAP_<%=groupName%>_<%=sidx%>" checked disabled='true'/></td>
					<td>&nbsp;</td>
				<%} else { %>
					<td><input type="checkbox" name="LDAP_<%=groupName%>_<%=sidx%>" id="LDAP_<%=groupName%>_<%=sidx%>" value="LDAP_<%=groupName%>_<%=sidx%>" disabled='true'/></td>
					<td>&nbsp;</td>				
				<%} %>
				<%} %>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>		
	<%} %>
	</table>
	</s:form>
		
	</div>
</body>
</html>