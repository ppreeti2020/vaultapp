<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dao.DataDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="com.pb.common.CompareDate" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>e2 Vault Administration</title>
<!-- calendar stylesheet -->
  <link rel="stylesheet" type="text/css" media="all" href="styles/calendar-win2k-cold-1.css" title="win2k-cold-1" />

  <!-- main calendar program -->
  <script type="text/javascript" src="js/calendar.js"></script>

  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar-setup.js"></script>
 <%
String userName =  request.getAttribute("userName")!= null? request.getAttribute("userName").toString():"";
int currYear =  request.getAttribute("currYear")!= null? Integer.parseInt(request.getAttribute("currYear").toString()):0; 
String userGrp =  request.getAttribute("userGrp")!= null? request.getAttribute("userGrp").toString():""; 
String accessdateFrom =  request.getAttribute("accessdateFrom")!= null? request.getAttribute("accessdateFrom").toString():""; 
String accessdateTo =  request.getAttribute("accessdateTo")!= null? request.getAttribute("accessdateTo").toString():""; 
int accessfromDays =  request.getAttribute("accessfromDays")!= null?  Integer.parseInt(request.getAttribute("accessfromDays").toString()):0; 
int accesstoDays =  request.getAttribute("accesstoDays")!= null?  Integer.parseInt(request.getAttribute("accesstoDays").toString()):0; 
String active =  request.getAttribute("active")!= null? request.getAttribute("active").toString():""; 
System.out.println("active..."+active);
Long userId =  request.getAttribute("userId")!= null?  Long.parseLong(request.getAttribute("userId").toString()):0L; 
System.out.println("userId..."+userId);
//Long dbId =  request.getAttribute("dbId")!= null?  Long.parseLong(request.getAttribute("dbId").toString()):0L; 
Long[] roleIds = null;
Long[] actionIds = null;
Long[] dbIds = null;
int i=0,j=0,k=0;
if(request.getAttribute("roleId")!= null)
{	System.out.println("roleids..."+request.getAttribute("roleId"));
	roleIds = (Long[])request.getAttribute("roleId");
}
else
{	roleIds = new Long[0];
}
int roleLen = roleIds.length;
if(request.getAttribute("actionId")!= null)
{	 actionIds = (Long[])request.getAttribute("actionId");
	 System.out.println("actionIds..."+actionIds.length);
}
else
{	actionIds = new Long[0];
}
int actLen = actionIds.length;
if(request.getAttribute("dbId")!= null)
{	dbIds =  (Long[])request.getAttribute("dbId");
	System.out.println("dbIds..."+dbIds.length);
	for(int h=0;h<dbIds.length;h++)
	{	System.out.println("dbIds[]..."+dbIds[h]);
	}
}
else
{	dbIds = new Long[0];
}
int dbLen = dbIds.length;
System.out.println("dbLen..."+dbLen);
 %>
 
 
<script type="text/javascript">
window.onload = function(){
	var dbid = document.getElementsByName("dbId");
	var roleid = document.getElementsByName("roleId");
	var actionId = document.getElementsByName("actionId");
	var active = document.getElementsByName("active");
	var curryear = document.getElementById("currYear");
	var accessfromDays = document.getElementById("accessfromDays");
	var accesstoDays = document.getElementById("accesstoDays");
	if(dbid != null)
	{ for(var v=0;v<dbid.length;v++)
	  {	<% while(k<dbLen)
		{%>
			if(dbid[v].value == '<%=dbIds[k]%>')
			{	dbid[v].checked = true;
			}
			<%k++;}%>
		}
	  }
	 for(var v=0;v<roleid.length;v++)
	 { <% while(i<roleLen)
		{%>
			if(roleid[v].value == '<%=roleIds[i]%>')
			{ 	roleid[v].checked = true;
			}
			<%i++;}%>
	 }
	 for(var v=0;v<actionId.length;v++)
	 {	<% while(j<actLen)
		{%>
		   if(actionId[v].value == '<%=actionIds[j]%>')
		   {	actionId[v].checked = true;
		   }
		   <%j++;}%>
	 }
	for(var v=0;v<active.length;v++)
	{	if(active[v].value == '<%=active%>')
		{  active[v].checked = true;
		}
	}
	for(var v=0;v<accessfromDays.length;v++)
	{	if(accessfromDays[v].value == '<%=accessfromDays%>')
		{  accessfromDays[v].selected = true;
		}
	}
	for(var v=0;v<accesstoDays.length;v++)
	{	if(accesstoDays[v].value == '<%=accesstoDays%>')
		{  accesstoDays[v].selected = true;
		}
	}
	if(curryear.value == '<%=currYear%>')
	{	curryear.selected = true;
	}
}

function CancelForm(button)
{
	document.getElementById("updateUsers").action = "UsersListAction.action";
	document.getElementById("updateUsers").submit;
	return true;
			
}

function ValidateForm(){
	
	var roleid = document.getElementsByName("roleId");
	var rib = false;
	for(var v=0;v<roleid.length;v++)
	{ <% i =0; 
		while(i<roleLen)
		{%>
			var ri = roleid[v].checked;
			if(ri != false)
			{ 	rib = true;
			}
			<%i++;
		}%>
	 }
	if(rib != true){
		alert("Please select any one of the Role!!!");
		return false;
	}
	var actionId = document.getElementsByName("actionId");
	var aib = false;
	for(var v=0;v<actionId.length;v++)
	 { <% i=0;
		while(i<actLen)
		{%>
			var ai = actionId[v].checked;
			if(ai != false)
			{ 	aib = true;
			}
			<%i++;
		}%>
	 }
	if(aib != true){
		alert("Please select any one of the Document Permissions!!!");
		return false;
	}
	var active = document.getElementsByName("active");
	var acib = false;
	for(var v=0;v<active.length;v++)
	 { 	var aci = active[v].value;
	 	if(aci.length != 0)
		{ 	acib = true;
		}
		<%i++;%>
	 }
	if(acib != true){
		alert("Please select any one of the User Actions!!!");
		return false;
	}
			
	var accessdateFrom = document.getElementById("accessdateFrom").value;
	if(accessdateFrom.length==0){
		alert("Please select the Access Date From!!!");
		return false;
	}
	var accessdateTo = document.getElementById("accessdateTo").value;
	if(accessdateTo.length==0){
		alert("Please select the Access Date To!!!");
		return false;
	}
	
	var accessfromDays = document.getElementById("accessfromDays");
	var fromday = accessfromDays.options[accessfromDays.selectedIndex].value;
	if(fromday<=0){
		alert("Please select the Access Days From!!!");
		return false;
	}
	var accesstoDays = document.getElementById("accesstoDays");
	var today = accesstoDays.options[accesstoDays.selectedIndex].value;
	if(today<=0){
		alert("Please select the Access Days To!!!");
		return false;
	}
		
	var datefromDate = accessdateFrom.split(' ')[0];
	var timefromDate = accessdateFrom.split(' ')[1];
	
	var fromDate = new Date(datefromDate.split('-')[0],datefromDate.split('-')[1]-1,datefromDate.split('-')[2]);
	fromDate.setHours(timefromDate.split(':')[0],timefromDate.split(':')[1],timefromDate.split(':')[2]);
	
	var datetoDate = accessdateTo.split(' ')[0];
	var timetoDate = accessdateTo.split(' ')[1];
	
	var toDate = new Date(datetoDate.split('-')[0],datetoDate.split('-')[1]-1,datetoDate.split('-')[2]);
	toDate.setHours(timetoDate.split(':')[0],timetoDate.split(':')[1],timetoDate.split(':')[2]);
	
	var currDate = new Date();
	
	/* if(fromDate<currDate){
		alert("Access From Date cannot be less than today's date!!!");
		return false;
	} */
	if(toDate<currDate){
		alert("Access To Date cannot be less than today's date!!!");
		return false;
	}
	
	if(fromDate>toDate)
	{
	alert("Access From Date is greater than Access To Date");
	return false;
	}
	
/* 	if(fromday>today){
		alert("Access from Day should be before Access to Day");
		return false;
	} */
	document.getElementById("updateUsers").action = "UpdateUserAction.action";
	document.getElementById("updateUsers").submit;
	return true;
	
}
</script>
</head>
<body>
<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1500px; height:620px; margin-bottom: 1em; padding: 10px; overflow:auto">


<jsp:include page="useradminCommonTabs.jsp" />

<div id="countrydivsubcontainer" style="border:1px solid gray; width:1400px; height:560px; margin-bottom: 1em; padding: 10px; overflow:auto">


<table width="100%" border="1px solid #778" height="500" valign="top">

	<tr><td valign="top" align="left">
	<form id="updateUsers">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
	<table align="left" >
	<tr><td colspan="4" align="left"><table><tr><td>
				<b>Group:</b></td><td><%= userGrp%></td><td><b>User:</b>
			</td><td><%= userName%></td><td><input type="hidden" name="seluserId" id="seluserId" value="<%=userId%>"/></td>
			</tr></table>
			</td></tr>
		<tr>
			
			
			<td>
				<b>Roles</b>
			</td>
			<td>
				<b>Repositories</b>
			</td>			
			<td>
				<b>Document Permission</b>
			</td>
			<td>
				<b>User Actions</b>
			</td>
		</tr>
	
		<tr> 
			 <td>
			 <%
		List csrRoles = request.getAttribute("csrRoles")!= null?(ArrayList)request.getAttribute("csrRoles"):new ArrayList();			
		Iterator itrer = csrRoles.iterator();
		%>
		
	<div id="div1" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		<%
		while (itrer.hasNext()) {
			UserDTO cDao = (UserDTO)itrer.next();
			System.out.println("role"+cDao.getRoleId());
	%>
			
			
			 <tr><td><input type="checkbox" name="roleId" id="roleId" value="<%= cDao.getRoleId() %>" ><%= cDao.getRoleName() %></input>	
				</td>
			</tr>
	<%} %>
	
				</table></div>
</td>
<td>
			<%
		List csrDatabases = request.getAttribute("csrDatabases")!= null?(ArrayList)request.getAttribute("csrDatabases"):new ArrayList();			
		Iterator dbItrer = csrDatabases.iterator();
		%>
		
	<div id="div1" style="overflow:auto; width:450px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		<%
		while (dbItrer.hasNext()) {
			UserDTO uDto = (UserDTO)dbItrer.next();
	%>
			
			
			 <tr><td><input type="checkbox" name="dbId" id="dbId" value="<%= uDto.getDbId() %>"  onclick="checkGroup(this);"/><%= uDto.getDbDesc() %>	
				</td>
			</tr>
	<%} %>
	
				</table></div>
				</td>
			 
				<td>
				<%
		List csrActions = request.getAttribute("csrActions")!= null?(ArrayList)request.getAttribute("csrActions"):new ArrayList();			
		Iterator actIter = csrActions.iterator();
		%>
		
	<div id="div1" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		<%
		while (actIter.hasNext()) {
			UserDTO uDto = (UserDTO)actIter.next();
			System.out.println("actions"+uDto.getActionName());
	%>
			
			
			 <tr><td><input type="checkbox" name="actionId" id="actionId" value="<%= uDto.getActionId() %>" /><%= uDto.getActionName() %>	
				</td>
			</tr>
	<%} %>
	
				</table></div>
				</td>
				<td>
				<div id="div2" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		
			
			
			 <tr><td><input type="radio" name="active" id="active" value="Active" />Active	
				
	</td></tr>
	<tr><td><input type="radio" name="active" id="active" value="Inactive"  />Inactive	
				
	</td></tr>
	<tr><td><input type="radio" name="active" id="active" value="Disabled"  />Disabled	
				
	</td></tr>
	<tr><td><input type="radio" name="active" id="active" value="Auto Disabled" />Auto Disabled	
				
	</td></tr>
	<tr><td><input type="radio" name="active" id="active" value="Mark for Deletion" />Mark for Deletion	
				
	</td></tr>
				</table></div>
				</td>
			</tr>
			<tr>
			<td colspan="5">
					
	
		<table align="center">
	
			
			
			
			
			
			
			 <tr><td>Access Date From:</td><td><input type="text" name="accessdateFrom" id="accessdateFrom" value="<%=accessdateFrom %>" />
			
				
	</td><td>To:
			
				
	</td><td><input type="text" name="accessdateTo" id="accessdateTo" value="<%=accessdateTo %>"/>
			
				
	</td></tr>
	
	<tr><td>Access Days From:</td><td><select name="accessfromDays" id="accessfromDays" >
	<option value="1">Monday</option>
	<option value="2">Tuesday</option>
	<option value="3">Wednesday</option>
	<option value="4">Thursday</option>
	<option value="5">Friday</option>
	<option value="6">Saturday</option>
	<option value="7">Sunday</option>
	</select>
				
	</td><td>To:</td>
	<td><select name="accesstoDays" id="accesstoDays" >
	<option value="1">Monday</option>
	<option value="2">Tuesday</option>
	<option value="3">Wednesday</option>
	<option value="4">Thursday</option>
	<option value="5">Friday</option>
	<option value="6">Saturday</option>
	<option value="7">Sunday</option>
	</select>
				
	</td></tr>
	
	
				</table>
				</td>
			</tr>

	<tr>
		<td colspan="5"><input type="Submit" value="Apply" onclick="return ValidateForm()"/>&nbsp;&nbsp;<input type="Submit" value="Cancel" onclick="return CancelForm()"></td>
	</tr>	
			
	</table>
	
	</form>
		</td></tr></table>
	
		
		<script type="text/javascript">
  
	 Calendar.setup({
        inputField     :    "accessdateFrom",      // id of the input field
        ifFormat       :    "%Y-%m-%d %H:%M:%S",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	 Calendar.setup({
        inputField     :    "accessdateTo",      // id of the input field
        ifFormat       :    "%Y-%m-%d %H:%M:%S",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	

</script>
</div></div></div>
</body>
</html>