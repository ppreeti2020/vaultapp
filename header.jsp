<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.SQLException" %>
<%@page import="com.pb.dao.DBConnectionDAO" %>
<%@page import="com.pb.dao.CommonDAO" %>
<!DOCTYPE html PUBLIC 
	"-//W3C//DTD XHTML 1.1 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="en" lang="en">
<head>
	<title>Vault Application
</title>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/black/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<style> html{display : none ; } </style>
<script>
   if( self == top ) {
       document.documentElement.style.display = 'block' ; 
   } else {
       top.location = self.location ; 
   }
</script>
</head>
<body >
<div id="page">
	<!-- Footer -->
	<div id="footer" class="clearfix"></div>
<div id="header" class="clearfix">
	<div id="branding">
		<img id="logo" src="images/logonew.jpg" />
	</div>
	<div id="applicationDetail">
		<ul id="productName">
			<li>Vault App</li>
		</ul>
		<ul id="userDetail">
		<li>	
		<% String userName = "";
		   String LastLoginDate = "";
		if(session.getAttribute("UserName") != null)
		{
			userName = session.getAttribute("UserName").toString();
			DBConnectionDAO dao = new DBConnectionDAO();
			PreparedStatement ps = null;
			CommonDAO comDao = new CommonDAO();
			Connection conn = dao.getDBConnection();
			try {
				System.out.println("getting Last login details:::");

				String success = comDao.CheckEncryptData("Y");
				System.out.println("Success2is"+success);
				// ,SUCCESS_FLAG,HASH_ID
				String query = "select max(Login_Date) from LOGIN_DETAILS where SUCCESS_FLAG = ? and Login_Name = ?";
				ps = conn.prepareStatement(query);
				ps.setString(1, success);
				ps.setString(2, userName);
				System.out.println("selecting record");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					LastLoginDate = comDao.CheckDecryptData(rs.getString(1));
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				conn.close();

			}
		%>
		Welcome <%= userName%>
		Last Login: <%= LastLoginDate%>
		<%} %>
		</li>
		</ul>
		<ul id="mainLinks">
			<li><a href="DocManager.action">Home</a></li>	
			<li><a href="Logout.action">Logout</a></li>	
		</ul>
	</div>
	<div id="curve">
		<img id="curve" src="styles/themes/black/images/curve.gif" />
	</div>
	<hr />
</div>
	<!-- Main Content -->
	<div id="content">
		<div id="main"> 

	