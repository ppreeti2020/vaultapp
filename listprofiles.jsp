<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dao.DataDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>e2 Vault Administration</title>
<script type="text/javascript">

<% List<UserDTO> profileGroupList = request.getAttribute("profileGroupList") != null?(List<UserDTO>)request.getAttribute("profileGroupList"): new ArrayList<UserDTO>();

List<String> profiles = (ArrayList<String>)request.getAttribute("profiles");			
		%>


function populateGroupUsers()
{
	
	var csrGroups = document.getElementById('profiles');
	var csrgroupsInProfile = document.getElementById('groupsInProfile');
	var len=csrGroups.length;
	
	for(var i=0; i<csrgroupsInProfile.length; i++)
	{
		csrgroupsInProfile.remove(i);
		i--;
		
	}
	
	for(var j=0; j<len; j++)
	{
	if(csrGroups[j].selected)
	{
		var tmp = csrGroups.options[j].text;
		
		var tmp1 =csrGroups.options[j].value;
	var userArray=tmp1.split(",");
	for(var i=0;i<userArray.length-1;i++)
		{
		var y=document.createElement('option');
		y.text=userArray[i];
		
		y.value=userArray[i];
		try
		{
			csrgroupsInProfile.add(y,null);}
		catch(ex){
			csrgroupsInProfile.add(y);	
		}
		}
	
	}
	}
}

function checkUsersAndCreateAdmin(button)
{
	
	var users = document.getElementById('groupsInProfile');

	
	
	var len=users.length;
	var selectedUsers="";
	var count=0;
	for(var j=0; j<len; j++)
	{
	if(users[j].selected)
	{count++;
		var tmp = users.options[j].text;
		var tmp1 =users.options[j].value;
	selectedUsers=(tmp1+",");
	}
	}
	//alert("selected Yse"+selectedUsers)
	if(count == 0)
	{
	alert("Please Select a User");
	
		return false;
	}
	
	var csrGroups = document.getElementById('profiles');
	var tmp3=null;
	for(var k=0; k<csrGroups.length; k++)
	{
	if(csrGroups[k].selected)
	{
		tmp3 = csrGroups.options[k].text;
	
		
	}
	}
	//alert(tmp3);
	document.getElementById("selectedGroup").value=tmp3;
	document.getElementById("createAdminForm").action = "CreateAdminAction.action";
	document.getElementById("createAdminForm").submit;
	return true;
}
</script>
</head>
<body>

<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">


<jsp:include page="userpadminCommonTabs.jsp" />

<div id="countrydivsubcontainer" style="border:1px solid gray; width:1500px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">


<center><s:if test="hasActionErrors()">

      
  <s:actionerror />
 
 
</s:if><font color="#ff0000"><s:property value="message" /></font></center>

<form name="createAdminForm" id="createAdminForm">
<table width="100%" border="1px solid #778" height="500" align="top">
	
	<tr><td valign="top" align="center">
<table  valign="left">
	
		<tr>
			<td>
				<b>Profiles</b>
			</td>
			<td>
				<b>Groups</b>
			</td>
		</tr>
		<tr><td>
			<%
		
		Iterator<String> itr = profiles.iterator();
		%>
				<select id="profiles" name="profiles" size="6" onclick="populateGroupUsers()" > 
				<%
		while (itr.hasNext()) {
			String profile = itr.next();
			
			String groupNames="";
			Iterator<UserDTO> users=profileGroupList.iterator();
		 while(users.hasNext())
		 {
			 UserDTO u=users.next();
			 if(u.getUserProfileName().equalsIgnoreCase(profile))
			 {
				 groupNames+=u.getProfileGroup()+",";
			 }
		 }
		
	%>
			
			
			 <option value="<%= groupNames %>"><%= profile %></option>	
				
	<%} %>
			 </select>
			 </td>
			 <td>

<input type="hidden" id="selectedGroup" name="selectedGroup" value="tmp"/>
			
	<select id="groupsInProfile" name="groupsInProfile" size="6"  > 
		
	
				</select>
</td>
<td></td>
</tr>
<tr>
<td></td><td></td><td><!-- <input type="Submit" value="Create Admin/s" onclick="return checkUsersAndCreateAdmin(this);">--></td>

</tr>
</table>
</td></tr></table>
</form>
</div>
</div>	
		
</body>

</html>
