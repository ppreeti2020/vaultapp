<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dao.DataDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>e2 Vault Administration</title>
<script type="text/javascript">
function moveoutid()
{
	var sda = document.getElementById("printerNames");
	//alert(document.csrGrps);
	var len = sda.length;
	var sda1 = document.getElementById("selprinterNames");
	var i=0;
	for(var j=0; j<len; j++)
	{
		//alert( sda[j].text);
		if(sda[j] != null)
		{
		if(sda[j].selected)
		{
			var tmp = sda.options[j].text;
			var tmp1 = sda.options[j].value;
			sda.remove(j);
			j--;
			var y=document.createElement('option');
			y.text=tmp;
			y.value=tmp1;
			try
			{
				sda1.add(y,null);
				
				
			}
			catch(ex)
			{
			sda1.add(y);
			}
		
		
		sda1[i].selected = true;
		i++;
		}
		}
	}
	
	
}


function moveinid()
{
	var sda = document.getElementById('printerNames');
	var sda1 = document.getElementById('selprinterNames');
	var len = sda1.length;
	for(var j=0; j<len; j++)
	{
		if(sda1[j].selected)
		{
			var tmp = sda1.options[j].text;
			var tmp1 = sda1.options[j].value;
			sda1.remove(j);
			j--;
			var y=document.createElement('option');
			y.text=tmp;
			y.value=tmp1;
			try
			{
			sda.add(y,null);}
			catch(ex){
			sda.add(y);	
			}

		}
	}	
}
/*window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"inputField",
			dateFormat:"%d-%m-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1
		});
	};
	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
	*/
function checkValues()
{
	
}
</script>
</head>
<body>
<jsp:include page="CommonTabs.jsp" />

	<div id="countrydivcontainer"
		style="border: 1px solid gray; width: 1550px; height: 610px; margin-bottom: 1em; padding: 10px; overflow: auto">


		<jsp:include page="SystemCommonTabs.jsp" />

		<div id="countrydivcontainer1"
			style="border: 1px solid gray; width: 1500px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">

			<jsp:include page="PrinterCommonTabs.jsp" />

			<div id="countrydivcontainer1"
				style="border: 1px solid gray; width: 1500px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">
<center><s:if test="hasActionErrors()">

      
  <s:actionerror />
 
 
</s:if><font color="#ff0000"><s:property value="message" /></font></center>
<table width="100%" border="1px solid #778" height="500" align="top">
	
	<tr><td valign="top" align="center">
	<s:form action="AddPrinterAction">
	
	
	<table  valign="top">
		<tr>
			<td>
				<b>Printers</b>
			</td>
			<td>
				<b>&nbsp;</b>
			</td>
			<td>
				<b>Selected Printers</b>
			</td>
		</tr>
		<tr><td>
			<%
		List<String> printerNames = (ArrayList<String>)request.getAttribute("printerNames");			
		Iterator<String> itr = printerNames.iterator();
		%>
				<select id="printerNames" name="printerNames" size="6" multiple="multiple"> 
				<%
		while (itr.hasNext()) {
			String printerName = itr.next();
	%>
			
			
			 <option value="<%= printerName %>"><%= printerName %></option>	
				
	<%} %>
			 </select>
			 </td>
			 <td><table><tr><td>
<input type="button" value=">>" onclick="moveoutid()"></td>
<td>
<input type="button" value="<<" onclick="moveinid()">
</td></tr>
</table>
</td>
			 <td>
					
	
		
	<select id="selprinterNames" name="selprinterNames" size="6" multiple="multiple" > 
		
	
				</select>
				</td>
				
			</tr>
			<tr>
		<td colspan="3" align="right"><input type="Submit" value="Add" name="print"></td>
	</tr>	
	</table>
	
	</s:form>
		</td></tr></table>
	</div></div></div>
</body>
</html>