<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
String subAccessTabId = request.getAttribute("subAccessTabId") != null?request.getAttribute("subAccessTabId").toString():"";
System.out.println("subAccessTabId:"+subAccessTabId);

String subTabId = request.getAttribute("subTabId") != null?request.getAttribute("subTabId").toString():"";
System.out.println("subTabId:"+subTabId);

%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxtabs.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<title>Insert title here</title>
</head>
<body >
<ul id="countrytabs2" class="shadetabs">
	<%if(subAccessTabId.equals("DB Config"))
	{%>
			<li id="ser1"><a href="DBAction.action" rel="#default" class="selected">Test Database Connection</a></li>
			<li id="ser1"><a href="CreateTableName.action" rel="countrycontainer2">Create Data Table</a></li>
	<%}
 	else if(subAccessTabId.equals("Create Table"))
 	{%>
			<li id="ser1"><a href="DBAction.action" rel="countrycontainer2">Test Database Connection</a></li>
			<li id="ser1"><a href="CreateTableName.action" rel="#default" class="selected">Create Data Table</a></li>
	<%}
	else if(subAccessTabId.equals("Create CrossReftable"))
 	{%>
			<li id="ser1"><a href="DBAction.action" rel="countrycontainer2">Test Database Connection</a></li>
			<li id="ser1"><a href="CreateTableName.action" rel="countrycontainer2">Create Data Table</a></li>
	<%}
	else if(subAccessTabId.equals("CrossRef Config"))
 	{%>
			<li id="ser1"><a href="DBAction.action" rel="countrycontainer2">Test Database Connection</a></li>
			<li id="ser1"><a href="CreateTableName.action" rel="countrycontainer2">Create Data Table</a></li>
	<%}%>
	
</ul>
<script type="text/javascript">

var countries=new ddajaxtabs("countrytabs2", "countrydivcontainer2");
countries.setpersist(false);
countries.setselectedClassTarget("link"); //"link" or "linkparent"
countries.init();

</script>
</body>
</html>
