<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec"%>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet"
	type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css"
	type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css"
	media="all" />
<title>e2 Vault Administration</title>
<%
List<String> csrGroups = request.getAttribute("csrGroups")!=null?(ArrayList<String>)request.getAttribute("csrGroups"): new ArrayList<String>();
List<String> preSelCsrGroups = request.getAttribute("preSelCsrGroups")!=null?(ArrayList<String>)request.getAttribute("preSelCsrGroups"): new ArrayList<String>();
List<String> preSelAllCsrGroups = request.getAttribute("preSelAllCsrGroups")!=null?(ArrayList<String>)request.getAttribute("preSelAllCsrGroups"): new ArrayList<String>();
System.out.println("preSelAllCsrGroups::"+preSelAllCsrGroups);
System.out.println("preSelCsrGroups::"+preSelCsrGroups);
System.out.println("csrGroups::"+csrGroups);
String profileName = request.getAttribute("profileName")!=null?request.getAttribute("profileName").toString():"";
String profileAction = request.getAttribute("profileAction")!=null?request.getAttribute("profileAction").toString():"";
System.out.println("profileAction:::"+profileAction);

List selectedDB = (ArrayList)session.getAttribute("seldb");
Map<String,List<String>> profilePrePopList = session.getAttribute("profilePrePopulateList")!=null?(Map<String,List<String>>)session.getAttribute("profilePrePopulateList"): new HashMap<String,List<String>>();
Map<String,Map<String,Long>> profileDetailIndexMap = session.getAttribute("profileDetailIndex")!=null?(Map<String,Map<String,Long>>)session.getAttribute("profileDetailIndex"):new HashMap<String,Map<String,Long>>();
%>

<script type="text/javascript">
	function moveoutid() {
		var sda = document.getElementById("csrGrps");
		//alert(document.csrGrps);
		var len = sda.length;
		var sda1 = document.getElementById("selcsrGrps");
		var i = 0;
		for ( var j = 0; j < len; j++) {
			//alert( sda[j].text);
			if (sda[j] != null) {
				if (sda[j].selected) {
					var tmp = sda.options[j].text;
					var tmp1 = sda.options[j].value;
					sda.remove(j);
					j--;
					var y = document.createElement('option');
					y.text = tmp;
					y.value = tmp1;
					try {
						sda1.add(y, null);

					} catch (ex) {
						sda1.add(y);
					}

					sda1[i].selected = true;
					i++;
				}
			}
		}

	}

	function moveinid() {
		var sda = document.getElementById('csrGrps');
		var sda1 = document.getElementById('selcsrGrps');
		var len = sda1.length;
		for ( var j = 0; j < len; j++) {
			if (sda1[j].selected) {
				var tmp = sda1.options[j].text;
				var tmp1 = sda1.options[j].value;
				sda1.remove(j);
				j--;
				var y = document.createElement('option');
				y.text = tmp;
				y.value = tmp1;
				try {
					sda.add(y, null);
				} catch (ex) {
					sda.add(y);
				}

			}
		}
	}
	
	function CancelcreateForm()
	{
		document.getElementById("profileGroups").action = "ShowCreateProfileAction.action";
		document.getElementById("profileGroups").submit;
		return true;
	}

	function CanceleditForm()
	{
		document.getElementById("profileGroups").action = "ProfilesListAction.action";
		document.getElementById("profileGroups").submit;
		return true;
	}
	
	function NextcreateForm()
	{
		var sda1 = document.getElementById('selcsrGrps');
		var len = sda1.length;
		if(len<1)

		{
			window.alert("At least one group has to be selected");
			return false;
		}
		for ( var j = 0; j < len; j++) {
			sda1[j].selected = true;
		}
		document.getElementById("profileGroups").action = "ListProfileUsersAction.action";
		document.getElementById("profileGroups").submit;
		return true;
	}

	function NexteditForm()
	{
		var sda1 = document.getElementById('selcsrGrps');
		var len = sda1.length;
		for ( var j = 0; j < len; j++) {
			sda1[j].selected = true;
		}
		document.getElementById("profileGroups").action = "ApplyProfileSettings.action";
		document.getElementById("profileGroups").submit;
		return true;
	}
	
	function EditDBGroupBackSubmit(){
		document.getElementById("profileGroups").action="SaveProfileDbSettingsAction.action?backSubmitFlag=editBackSubmit";
	    document.getElementById("profileGroups").submit();
		return true;
	}
	
	function CreateDBGroupBackSubmit(){
		document.getElementById("profileGroups").action="SaveProfileDbSettingsAction.action?backSubmitFlag=createBackSubmit";
	    document.getElementById("profileGroups").submit();
		return true;
	}
			
</script>
</head>
<body>
	<jsp:include page="CommonTabs.jsp" />

	<div id="countrydivcontainer"
		style="border: 1px solid gray; width: 1230px; height: 610px; margin-bottom: 1em; padding: 10px; overflow: auto">


		<jsp:include page="userpadminCommonTabs.jsp" />

		<div id="countrydivsubcontainer"
			style="border: 1px solid gray; width: 1180px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">
			<!-- /Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1 and SCT (VaultApp) 
			Below change is to Display Invalid Parameters message if Selected Group is incorrect-->
			<s:if test="hasActionErrors()">
				<s:actionerror />
			</s:if>
			<font color="#ff0000"><s:property value="message" />
			</font>
			
			<s:form id="profileGroups">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
				<table width="100%" border="1px solid #778" height="500" >

					<tr>
						<td valign="top" align="left">

							<table>
								<tr>
									<td><b>Profile Name:</b>&nbsp;<%=profileName%></td>
								</tr>
								<tr>
									<td><b>Add/Remove Groups</b></td>
								</tr>
								
								<tr><td></td></tr>
								<tr>
									<td><b>User Groups</b></td>
									<td><b>&nbsp;</b></td>
									<td><b>Selected User Groups</b></td>
								</tr>
								<tr>
									<td>
										<%
											
												Iterator<String> itr = csrGroups.iterator();%> 
												<select id="csrGrps" name="csrGrps" size="6">
											<%
												while (itr.hasNext()) {
														String groupName = itr.next();
											%>


											<option value="<%=groupName%>"><%=groupName%></option>
											<%
												}
											%>

									</select></td>
									<td><table>
											<tr>
												<td><input type="button" value=">>"
													onclick="moveoutid()"/>
												</td>
												<td><input type="button" value="<<" onclick="moveinid()"/>
												</td>
											</tr>
										</table></td>
										
										<%if(preSelAllCsrGroups.size()!=0){ 
											Iterator<String> itr1 = preSelAllCsrGroups.iterator();%>
											<td><select id="selcsrGrps" name="selcsrGrps" size="6"	multiple="multiple">
												<%while (itr1.hasNext()) {
													String groupName = itr1.next();	
													if(preSelCsrGroups.contains(groupName)){%>
														<option value="<%=groupName%>" disabled selected><%=groupName%></option>
													<%}else{ %>
														<option value="<%=groupName%>"><%=groupName%></option>
													<%} %>
												<%}	%>
												</select>
											</td>
										<%}else{ %>
											<td><select id="selcsrGrps" name="selcsrGrps" size="6"	multiple="multiple">
												</select>
											</td>
										<%}%>
								</tr>
								<tr>
									<td colspan="3" align="right">
										<!--Nithu Alexander: 24/10/2014. Added CreateDBGroupBackSubmit() and  EditDBGroupBackSubmit() 
										as part of BACK Button issue fix-->
										<!-- <input type="Submit" value="Back" onclick="history.go(-1);return false;"/>&nbsp;&nbsp; -->
										<%if(profileAction.equalsIgnoreCase("create")){ %>
											<input type="button" value="Back" onclick="return CreateDBGroupBackSubmit()"/>&nbsp;&nbsp;
										<%}else if(profileAction.equalsIgnoreCase("edit")){ %>
											<input type="button" value="Back" onclick="return EditDBGroupBackSubmit()"/>&nbsp;&nbsp;
										<%} %>
										<%if(profileAction.equalsIgnoreCase("create")){ %>
											<input type="Submit" value="Next" onclick="return NextcreateForm()">
											<input type="Submit" value="Cancel" onclick="return CancelcreateForm()">
										<%}else if(profileAction.equalsIgnoreCase("edit")){ %>
											<input type="Submit" value="Update" onclick="return NexteditForm()">
											<input type="Submit" value="Cancel" onclick="return CanceleditForm()">
										<%} %>
									</td>
								</tr>
							</table></td>
					</tr>
				</table>
			</s:form>
		</div>
	</div>
</body>
</html>