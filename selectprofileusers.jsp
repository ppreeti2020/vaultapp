<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dao.DataDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>e2 Vault Administration</title>
	<%
		//List<String> csrAllUsers = (ArrayList<String>)request.getAttribute("csrAllUsers");			
		//Iterator<String> itr = csrAllUsers.iterator();
		Map<String,Object> userGroups = (HashMap<String,Object>)request.getAttribute("userGroups");
		%>
<script type="text/javascript">
function moveoutid()
{
	var userTab = document.getElementById("userTable");
	//alert(document.csrGrps);
	var userRowCount = userTab.rows.length;
	var seluserTab = document.getElementById("selUserTable");
	var seluserRowCount = seluserTab.rows.length;
	 for(var i=0; i<userRowCount; i++) {
                var row1 = userTab.rows[i];
                var chkbox = row1.cells[0].childNodes[0];
				 var text = row1.cells[1].innerHTML;
                if(null != chkbox && true == chkbox.checked) {
				
					var row = seluserTab.insertRow(seluserRowCount);
					var cell1 = row.insertCell(0);
					
					var element1 = document.createElement("input");
					element1.type = "checkbox";
					element1.value = chkbox.value;
					element1.id = "selCsrUsers";
					cell1.appendChild(element1);
					var cell2 = row.insertCell(1);
					cell2.innerHTML = text;
				}
	 }
	
	
}

function CancelForm(button)
{
	document.getElementById("createProfile").action = "ShowCreateProfileAction.action";
	document.getElementById("createProfile").submit;
	return true;
			
}

function selAllUsers(group)
{
	
	//alert(group.value);
	var grid = "";
	grid += "<table id='selUserTable'><%for(String key: userGroups.keySet())	{ 	%>";
	
if('<%=key%>' == group.value)
	{
	grid += "<%List users = (ArrayList)userGroups.get(key);
	for(Iterator iter=users.iterator();iter.hasNext();){ String userdto = (String)iter.next();%><tr><td><input type='radio' checked id='csrUsers' name='csrUsers' value='<%=key%>,<%= userdto %>' /></td><td><%= userdto %></td></tr><%}%>";
	}
	grid += "<%} %></table>";
//alert(grid);
	document.getElementById("div2").innerHTML = grid;
}
function moveinid()
{
	var sda = document.getElementById('userTable');
	
	var len = sda1.length;
	for(var j=0; j<len; j++)
	{
		if(sda1[j].selected)
		{
			var tmp = sda1.options[j].text;
			var tmp1 = sda1.options[j].value;
			sda1.remove(j);
			j--;
			var y=document.createElement('option');
			y.text=tmp;
			y.value=tmp1;
			try
			{
			sda.add(y,null);}
			catch(ex){
			sda.add(y);	
			}

		}
	}	
}

function checkValues()
{
	
}

function createProfileBackSubmit(){
	document.getElementById("createProfileProjectAdmin").action="SaveProfileGroupSettingsAction.action";
    document.getElementById("createProfileProjectAdmin").submit();
	return true;
}
</script>
</head>
<body>
<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1230px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">


<jsp:include page="userpadminCommonTabs.jsp" />

<div id="countrydivsubcontainer" style="border:1px solid gray; width:1180px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">


<table width="100%" border="1px solid #778" height="500" valign="top">
	<!-- 2em margin here  -->
		<h1><center><s:if test="hasActionErrors()">

      
  <s:actionerror />
 
 
</s:if><font color="#ff0000"><s:property value="message" /></font>
	
	<tr><td valign="top" align="left">
	<s:form id="createProfileProjectAdmin" action="CreateProfileProjectAdminAction">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
	<table align="left" >
	
		<tr>
			<td>
				<b>Selected Groups</b>
			</td>
			
			<td>
				<b>Selected Users </b>
			</td>
		</tr>
		<tr><td>

		
				
			
			 <div id="div1" style="overflow:auto; width:250px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:120px;"><table id="userTable"> 
			<%			
			for(String key: userGroups.keySet())
			{
	%>
		 <tr><td><input type="radio" id="csrGroups" name="csrGroups" value="<%= key %>" onclick="selAllUsers(this);"/></td><td><%= key %></td></tr>
			
			<%} %>
				</table></div>
			 </td>
			 
			 <td>
			<div id="div2" style="overflow:auto; width:250px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:120px;"> 		
	
	</div>
				</td>
				
			</tr>
			<tr>
		<td colspan="3" align="right"><input type="Submit" onclick="" value="Create Profile And Profile Admin"/>&nbsp;&nbsp;
		<!-- <input type="Submit" value="Back" onclick="history.go(-1);return false;"/></td> -->
		<input type="button" value="Back" onclick="return createProfileBackSubmit()"/>
	</tr>	
	</table>
	
	</s:form>
		</td></tr></table>
	
		</div></div>
</body>
</html>