<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dto.DataDTO" %>
<%@page import="com.pb.dao.UserDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>

<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />

<title>e2 Vault Administration</title>
<s:include value="adminHeader.jsp" />
<%

String message = request.getAttribute("message")!= null?(String)request.getAttribute("message"):"";
System.out.println("message::"+message);
List<String> profileList = request.getAttribute("profileList") != null?(ArrayList<String>)request.getAttribute("profileList"): new ArrayList<String>();
System.out.println("profileList::"+profileList);
%>
<script type="text/javascript">
function checkAll(checklist)
{
	var list = document.getElementsByName("newProfileId");
	if(list != null){
		for(var v=0;v<list.length;v++){
		
			if(list[v].checked)
			{
			list[v].checked = false;
			}
			else
			if(!list[v].checked)
			{
		list[v].checked = true;
			}
		}
	}
}
function deleteUser(button)
{
	
	var list = document.getElementsByName("newProfileId");
	var ProfileNamelist = document.getElementsByName("newProfileName");
	var delusers = "";
	for(var v=0;v<list.length;v++)
	{
		if(list[v].checked){
			var userVal = ProfileNamelist[v].value;
			if(delusers == "")
				delusers = userVal+",";
			else
				delusers = delusers + userVal+",";
		}
			
	}
	
	var r=confirm("do you want to delete "+delusers+"!!");
	
	if (r==true)
	 {
	var flag = 0;
	if(list != null)
	{
		var users = new Array(list.length);
	for(var v=0;v<list.length;v++)
		{
		if(!list[v].checked)
			{
			flag = 0;
			
			}
			else
		if(list[v].checked)
			users[v] = list[v].value;
			flag = 1;
		}
		
	}
if(flag == 0)
		 {
	alert("Please select User");
			return false;
}
	if(flag == 1)
		 {
	document.getElementById("profilelistform").action = "DeleteProfileAction.action";
	document.getElementById("profilelistform").submit;
	return true;
		 }
	
	}
	else
  {
	return false;
  }
		
}
</script>
</head>
<body>

<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">


<%-- <jsp:include page="userpadminCommonTabs.jsp" />
 --%>
<div id="countrydivsubcontainer" style="border:1px solid gray; width:1500px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">

<B><CENTER>Welcome to Vault App!</CENTER></B>

<!-- <form name="profilelistform" id="profilelistform">
<table width="100%"  height="500" valign="top">
<tr><td valign="top">
<table width="100%">
<tr>
<td align="left">
												<div id="mes"><font color="red">
													<%if(message != null && !message.equals(""))
												  	{%>
														<%=message%>
													<%}
												 	 else if(session.getAttribute("message") != null)
												  	{%>
														<%= session.getAttribute("message")%>
														<%session.setAttribute("message",null);
												  	}%>
												  	</font>
												</div>
											 </td>
											 </tr>
											 <tr>
<% 
if(profileList.size() != 0)
{%>
<td align="left" valign="top"><input type="submit" name="delete" value="Delete" onclick="return deleteUser(this);"/>
</td></tr>
	<tr><td  align="left">
	<% int i=1; %>
	<display:table name="profileList" class="dataTable" id="profiles" pagesize="15" requestURI="/UsersListAction.action">
	<display:setProperty name="paging.banner.onepage" value=" " />
	<display:column sortable="true" title='<input type="checkbox" name="allbox" id="allbox" onclick="checkAll(this);"  />'>
    <input type="checkbox" name="newProfileId" id="newProfileId" value="${profiles.profileId}"/>
    <input type="hidden" name="newProfileName" id="newProfileName" value="${profiles.profileName}"/>
    </display:column>
   		<display:column property="profileName" sortable="true" title="Profile Name"/>
		 <display:column property="profileDb" sortable="true" title="Databases"/>
		 <display:column property="profileGroup" sortable="true" title="Groups"/>
		 <display:column property="profileAdmin"  sortable="true"  href="EditProfileAdminAction.action" paramId="profileId" paramProperty="profileId" title="Profile Admin"/>
		 <display:column property="profileUsers" sortable="true" title="Users"/>
		 <display:column href="EditProfileAction.action?editflag=editProfile" paramId="selprofileId" paramProperty="profileId" sortable="true" title="Action">Edit</display:column>
		
		</display:table></td>
<%}
		else
		{%>
<td align="center">No Profiles Found</td>
<%		}%>	</tr></table>
	
		
		</td></tr></table>
		</form>-->
		</div>
		</div>
	
</body>
</html>