<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.*"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="java.text.*"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="com.pb.evault.DocumentHandle"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
	
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Document Management Results</title>
<s:include value="adminHeader.jsp" />
<%
String host = (String)request.getAttribute("host");
Map<Long,DataDAO> dbMapList = request.getAttribute("dbMapList")!=null?(HashMap<Long,DataDAO>)request.getAttribute("dbMapList"):new HashMap<Long,DataDAO>();
System.out.println("dbMapList size::"+dbMapList.size());
String message = request.getAttribute("message")!= null?(String)request.getAttribute("message"):"";
Long prdbID =  request.getAttribute("prdbID")!=null?Long.parseLong(request.getAttribute("prdbID").toString()):0L;
System.out.println("prdbID:::"+prdbID);
String prName =  request.getAttribute("prName")!=null?(String)request.getAttribute("prName"):"";

Map<Long,String> searchIdxList = request.getAttribute("searchIdxList")!=null?(HashMap<Long,String>)request.getAttribute("searchIdxList"):new HashMap<Long,String>();
ArrayList<String> resultIdxList = request.getAttribute("resultIdxList")!=null?(ArrayList<String>)request.getAttribute("resultIdxList"):new ArrayList<String>();
Map<Integer, Map<String,String>> docList = request.getAttribute("docList")!=null?(HashMap<Integer, Map<String,String>>)request.getAttribute("docList"):new HashMap<Integer, Map<String,String>>();
String accountNumber = request.getAttribute("accountNumber")!=null?(String)request.getAttribute("accountNumber"):"";
System.out.println("account Number::"+accountNumber);
String databaseName = request.getAttribute("databaseName")!=null?(String)request.getAttribute("databaseName"):"";
DocumentHandle docHandle = request.getAttribute("docHandle")!=null?(DocumentHandle)request.getAttribute("docHandle"):new DocumentHandle();
String actType = request.getAttribute("actType")!= null?(String)request.getAttribute("actType"):"";
List<String> actionList = request.getAttribute("actionList")!= null?(ArrayList<String>)request.getAttribute("actionList"):new ArrayList<String>();
Map<String,String> idxList = request.getAttribute("idxList")!= null?(Map<String,String>)request.getAttribute("idxList"):new HashMap<String,String>();
System.out.println("Cross ref idxs::"+idxList);
String primaryidx = request.getAttribute("primaryidx")!= null?(String)request.getAttribute("primaryidx"):"";
String crossSearch = request.getAttribute("crossSearch")!= null?(String)request.getAttribute("crossSearch"):"no";
System.out.println("Primary idx::"+primaryidx);
int pageSize = request.getAttribute("pageSize")!=null?Integer.parseInt(request.getAttribute("pageSize").toString()):10;
if(pageSize == 0 && docList.size() != 0  )
{
	if( docList.size() < 10)
		pageSize = docList.size();
	else
pageSize = 10;
}
String view = "";
String print = "";
String reprint = "";
String download = "";
String viewed = "";

String vaultIndexDesc = request.getAttribute("vaultIndexDesc")!=null?(String)request.getAttribute("vaultIndexDesc"):"";
System.out.println("vaultIndexDesc>>"+vaultIndexDesc);
String vaultIndex = request.getAttribute("vaultIndex")!=null?(String)request.getAttribute("vaultIndex"):"";


%>
<script>
function ValidateForm(){
	var pr = 0;
	var sec = 0;
	var aInputs = document.getElementsByTagName('input');
	for (var i=0;i<aInputs.length;i++) {
		var id = aInputs[i].id;
		if (id.match("Pri")){
			if(aInputs[i].value.length==0){
				pr = 0;
				var prIdx = id.split('_');
				alert("'"+prIdx[3]+"' cannot be empty!!");
				return false;
			}
			else
				pr = 1;
		}
		else if(id.match("Sec")){
			if(aInputs[i].value.length!=0){
				sec++;				
			}
		}
	}
/*	if(pr == 1 && sec == 0){
		alert("Please enter any other search value for cross reference!!");
		return false;
	}*/
	document.getElementById("search").value = "true";
	document.getElementById("Display").action="DocManagerNew1.action";
	document.getElementById("Display").submit();
	return true;
}

</script>
</head>
<!-- <body> -->

<body topmargin=1 marginheight=16 >
<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1500px; height:630px; margin-bottom: 1em; padding: 10px; overflow:auto">

	<table width="100%" height="500">
		<tr><td>
			<table align="center"  width="100%" height="500">
				<tr><td valign="top">
					<table width="100%">
					<tr><td valign="top">
						<table width="100%" border="1" height="500">
						<tr><td id="table1" width="15%"  valign="top">
							<table>
								<tr><td>
									<ul id="repository" class="profMap">
										<%
										   for(Long dbId: dbMapList.keySet())
										   { DataDAO ddao = (DataDAO)dbMapList.get(dbId);
										   System.out.println("dbname::"+ddao.getDbName());
										    %> 
										    <li id=<%= ddao.getDbName()%>><span><a href='DocManagerNew1.action?prName=<%= ddao.getDbName()%>&prdbID=<%=ddao.getDbId()%>'><%= ddao.getDbDesc()%></a></span></li>
										<% }%>
									</ul>
								</td></tr>
							</table>
							</td>
							<td width="80%"  valign="top">
							
							<table border="0" align="center" width="100%">	
								<tr><td align="left" width="100%">
									<form id="Display" name="Display" method="post">
									<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
									<table width="100%">
									<tr>
									<td align="left">
												<div id="mes"><font color="red">
													<%if(message != null && !message.equals(""))
												  	{%>
														<%=message%>
													<%}
												 	 else if(session.getAttribute("message") != null)
												  	{%>
														<%= session.getAttribute("message")%>
														<%session.setAttribute("message",null);
												  	}%>
												  	</font>
												</div>
											 </td>
											 </tr>
									<%if(accountNumber.equals("")){ %>
										<tr>
											 <td align="left">
												<input type="hidden" name="prdbID" id="prdbID" value="<%=prdbID%>"/>
												<input type="hidden" name="prName" id="prName" value="<%=prName%>"/>
												<input type="hidden" name="search" id="search"/>
											</td>
										</tr>
										<tr><td>
										<table><tr>
										<% if(searchIdxList.size() != 0)
										   { for(Long idxID:searchIdxList.keySet())
										     { String tempstr[] = searchIdxList.get(idxID).split("_");
										       if(tempstr[0].equalsIgnoreCase("Primary")){
										      %><td><B><font color = red>*<%=tempstr[2]%>::</font></B>
										     	<input type="text" name="textSearchs_Pri_<%=idxID%>_<%=tempstr[1]%>_<%=prdbID%>" 
										     	                     id="textSearchs_Pri_<%=idxID%>_<%=tempstr[1]%>_<%=prdbID%>" 
										     	                  value="" /></td>
										     <%}
										     }
										     for(Long idxID:searchIdxList.keySet())
										     { String tempstr[] = searchIdxList.get(idxID).split("_");
										   	   if(tempstr[0].equalsIgnoreCase("Secondary")){
										      %><td><B><%=tempstr[2]%>::</B>
										     	<input type="text" name="textSearchs_Sec_<%=idxID%>_<%=tempstr[1]%>_<%=prdbID%>" 
										     	                     id="textSearchs_Sec_<%=idxID%>_<%=tempstr[1]%>_<%=prdbID%>" 
										     	                  value="" /></td>
									         <%}
									         }%>
									         <td><input type="Submit" id="searchbutton" value="search" onclick="return ValidateForm()"/></td></tr>
									     <%}%>
										   
										</table></td></tr>
										<tr><td  align="left">
									<table width=100%><tr><td>
										
										<% if(docList.size() != 0)
									  	 { System.out.println("Inside docList"+docList.size());
											 int i =1; Map<String,String> idxvals; String vaultIdx = "";String searchString ="";
											  %>
											</td>
											<tr><td>
											<display:table name="docList" class="dataTable" id="dispID" export="true" pagesize="<%=pageSize%>" requestURI="/DocManagerNew1.action">
												<display:setProperty name="export.excel.filename" value="SearchDetails.xls"/>
            									<display:setProperty name="export.xml.filename" value="SearchDetails.xml"/>
			  									<display:setProperty name="export.csv.filename" value="SearchDetails.csv"/>
			  									<display:setProperty name="paging.banner.onepage" value=" " />
              									<display:column sortable="true" title="ID"><%=i++%></display:column>
												<% idxvals = (Map<String, String>)pageContext.getAttribute("dispID"); 
												   if(idxList.size()!=0 && crossSearch.equalsIgnoreCase("no")){
													   searchString = "";
													   for(String idx:idxList.keySet()){
														   System.out.println("idx:"+idx+">primaryidx::"+primaryidx+">>");
														   searchString +="&"+idx+"="+idxvals.get(idxList.get(idx));
													   }
													   %>
													  <display:column sortable="true" title='<%=primaryidx%>'>
													  <a href="DocManagerNew1.action?crossSearch=yes&prdbID=<%=prdbID%>&prName=<%=prName%><%=searchString%>"><b><%=idxvals.get(primaryidx)%></b></a>
													  </display:column>
												  <%}else{  %>
												  	  <display:column sortable="true" title='<%=primaryidx%>'> <%=idxvals.get(primaryidx)%> </display:column>
												  <%}
												    for(String col:idxvals.keySet())
													{ if(col.equalsIgnoreCase(vaultIndexDesc)){
														vaultIdx = idxvals.get(col);
													  } if(!col.equalsIgnoreCase(primaryidx)){%>
													  <display:column sortable="true" title='<%=col%>'> <%=idxvals.get(col)%> </display:column>
												  <%    }
													}%>
												 	 <display:column sortable="true" title="Action">
												 	 <% for(Iterator iter=actionList.iterator();iter.hasNext();)
                                      					{ UserDTO dto = (UserDTO) iter.next();
                            			 	 			  if(dto.getActionName().equalsIgnoreCase("View"))
								    	     			  	view = "true";
								    	  				  if(dto.getActionName().equalsIgnoreCase("Download"))
								    	  	 				download = "true";
								    	  				  if(dto.getActionName().equalsIgnoreCase("Print")) 
								    	  	 				print="true";
								     	  				  if(dto.getActionName().equalsIgnoreCase("RePrint")) 
								     	  	 				reprint="true";
								      					} %>
								      					<%if(view.equals("true"))
									  					  {%>	<a href="DocManagerNew1.action?vaultIdx=<%=vaultIdx%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=view"><b>View </b></a>
														<%}if(download.equals("true"))
									  					  {%>|<a href="DocManagerNew1.action?vaultIdx=<%=vaultIdx%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=download"><b>Download </b></a>
														<%}if(print.equals("true"))
									  					  {%>|<a href="DocManagerNew1.action?vaultIdx=<%=vaultIdx%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=print"><b>Print</b></a>
														<%}%>
												  	
												  	</display:column>
											</display:table></td></tr>
									  	<%}else{ %>
											<tr><td>No Records Found </td></tr>
											<%}%>
											</table>
										<%	}
											if(!accountNumber.equals("") && !actType.equals("download") && !actType.equals("print")&& !actType.equals("reprint"))
											{
												System.out.println("acct:"+accountNumber);
												System.out.println("db:"+databaseName);
												System.out.println("doc:"+docHandle.toString());
												String url = "Interface";%>
			 
												<jsp:include page="<%= url %>" flush="true" >
													<jsp:param name="acct" value="<%= accountNumber %>" />
													<jsp:param name="db" value="<%= databaseName %>" />
													<jsp:param name="doc" value="<%= docHandle.toString() %>" />
													<jsp:param name="op" value="view" />
												</jsp:include>
											</td></tr></table>
										<%} %>
										</form></td>
							</table>
							</td>
						</tr>
						</table>
					</td></tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	</table>
<s:include value="footer.jsp" />
</div>
</body>
</html>