<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dto.DataDTO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="com.pb.common.CommonConstants" %>
<%@page import="com.pb.common.PropertyUtils" %>

<s:include value="adminHeader.jsp" />
  <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxtabs.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
	<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	
	<script src="js/jquery_treeview.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="js/demo.js"></script>
<title>e2 Vault Administration</title>
<%
String host = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.HOST_NAME);
String name = request.getAttribute("name")!=null?(String)request.getAttribute("name"):"";
int pageSize = request.getAttribute("pageSize")!=null?Integer.parseInt(request.getAttribute("pageSize").toString()):10;

System.out.println("pageSize::"+pageSize);
Map<Long,DataDTO>  treeMapList = request.getAttribute("treeMapList")!=null?(Map<Long,DataDTO>)request.getAttribute("treeMapList"):new HashMap<Long,DataDTO>();
Map<Integer,DataDTO>  treeProfile = request.getAttribute("treeProfile")!=null?(Map<Integer,DataDTO>)request.getAttribute("treeProfile"):new HashMap<Integer,DataDTO>();

System.out.println("treeProfile::"+treeProfile);
List populateLogList = request.getAttribute("populateLogList")!=null?(ArrayList)request.getAttribute("populateLogList"):new ArrayList();
if(pageSize == 0 && populateLogList.size() != 0  )
{
	if( populateLogList.size() < 10)
		pageSize = populateLogList.size();
	else
pageSize = 10;
}
String hostName = request.getAttribute("hostName")!=null?(String)request.getAttribute("hostName"):"";
String profile = request.getAttribute("profile")!=null?(String)request.getAttribute("profile"):"";%>
<script>
  // this function is needed to work around 
  // a bug in IE related to element attributes
  function submitPage(button)
  {
	  
			document.getElementById("auditPage").action="AuditAction.action";
		
            document.getElementById("auditPage").submit();
			return true;  
  }
  function dropdownlist()
{
	
 var x = document.getElementById("pageSize");
 
 var len = x.length;

 for(var i=0;i<len;i++)
	 {
	 
	 if(x[i].value == '<%= pageSize %>')
		 {
	 x[i].selected = true;
	 x[i].value = '<%= pageSize %>';
		 }
	 }

}
if(navigator.appName == 'Microsoft Internet Explorer')

{

document.write('<link rel="stylesheet" type="text/css" href="/Scroll for IE.css" />');

}

else

{

document.write('<link rel="stylesheet" type="text/css" href="/Scroll for Other.css" />');

}
  function getRes(url,val,userId,indexId)
  {
	  //alert(userId);
	 // alert(url);
	  var grid="";
	 // var u="<s:url value='"+url+"?Index_"userId"_"indexId"'/>";
	 var u = "Index_"+userId+"_"+indexId;
	//  alert(u);
	  grid += "<select name='sel' id='sel'  onchange=getResults(this,'<s:url value='"+url+"'/>',"+userId+","+indexId+") style='width:50px;'><option value='all'>All</option><option value='100'>100</option><option value='200'>200</option></select>";
	  document.getElementById("res_"+userId+"_"+indexId).innerHTML = grid;
  }
  function getResults(val,url,userId,indexId)
  {
	// alert(val.value);

	  window.open(url+"?userId="+userId+"&indexId="+indexId+"&res="+val.value,'IndexResults','width=300,height=250,toolbar=no,resizable=yes,menubar=yes,scrollbars=yes')
  }
function Ajax(popUrl,val,userId,indexId) {
  var xmlHttpReq = false;
  var self1 = this;
  var stdin='';
  // Mozilla/Safari
  var url = "AjaxIndexAction.action?userId="+userId+"&indexId="+indexId;
 // alert(val.checked);
 if(val.checked == true)
	{
  if (window.XMLHttpRequest) {
    xmlHttpReq = new XMLHttpRequest();
  }
  // IE
  else if (window.ActiveXObject) {
    xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
  }
  if (!xmlHttpReq) {
    alert('ERROR AJAX:( Cannot create an XMLHTTP instance');
    return false;
  }   
  xmlHttpReq.open('POST', url, true);
  //xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xmlHttpReq.onreadystatechange = function() {
	 
		/* if(table != null)
			  {
    var rows = table.getElementsByTagName("tr");
    for (i = 1; i < rows.length; i++) {
		document.getElementById("AccordionContent"+i).style.display = 'none';
	}
	document.getElementById("AccordionContent"+stdout).style.display = 'block';
	
	document.getElementById("AccordionContent"+stdout).innerHTML = "loading..";*/
	
    if (xmlHttpReq.readyState == 4 )
	  {
		
		//xmlHttpReq.responseText
		if(xmlHttpReq.status == 200)
		  {
			//alert(xmlHttpReq.responseXML.getElementsByTagName("IndexResults")[0]);
			var list = xmlHttpReq.responseXML.getElementsByTagName("IndexResults")[0].childNodes[0].nodeValue;
		 
			
	 var grid="";
	 // var u="<s:url value='"+url+"?Index_"userId"_"indexId"'/>";
	
 
	if(list != null)
			  {
		if(list == "Morethan 100 Results!!")
				  {
			alert(list);
				  }
				  if(list != "Morethan 100 Results!!")
				  {
	  grid += "<select name='sel' id='sel'  onchange=getResults(this,'<s:url value='"+popUrl+"'/>',"+userId+","+indexId+") style='width:50px;'><option value='all'>All</option><option value='"+list+"'>"+list+"</option></select>";
	  document.getElementById("res_"+userId+"_"+indexId).innerHTML = grid;
			  }
			  }
	

		

		  }  
	  }
	  }
  }
  xmlHttpReq.send(stdin);
}
  </script>
  <title>tables</title>
  <style type="text/css">
  #playlist tr.header {
      background-color:#0072a8;
    }
    #playlist tr.even td {
      background-color: #eee;
    }
    #playlist tr.odd  td {
      background-color: #fff;
    }
  </style>
</head>

<body >

<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">


<form name="auditPage" id="auditPage">
<table width="100%" border="1" valign="top" height="550">
<tr>
<td width="20%"  valign="top">

<jsp:include page="AuditLeftPanel.jsp" />
</td>
<td width="80%"  valign="top" >
<jsp:include page="AuditCommonTabs.jsp" />
<div id="countrydivcontainer1" style="border:1px solid gray; width:1230px; height:550px; margin-bottom: 1em; padding: 10px; overflow:auto">

<table border="0" align="center" width="100%">	
<tr ><td align="left">
		<b>Audit Log: <%= name %></b>
		</td>
</tr>
<tr><td align="left"><table><tr>
<% if(populateLogList.size()>10){%>
<td>
<select name="pageSize" id="pageSize" style="width:50px;" onchange="return submitPage(this);">
<option value="10">10</option>
<option value="20">20</option>
<option value="30">30</option>

</select>
</td>
<%}%>
<td align="right"><%=pageSize%> records of <%= populateLogList.size()%></td>
</tr>
</table>
</td>
</tr>
<tr><td>
<% DataDTO cd = new DataDTO();
int i=1;%>
<div id="div2" style="overflow:auto; width:1200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:500px;">

<display:table name="populateLogList" class="dataTable" id="logList" export="true"  pagesize="<%=pageSize%>" style="width:300;overflow: scroll" requestURI="/AuditAction.action">
<display:setProperty name="export.excel.filename" value="LogDetails.xls"/>
             <display:setProperty name="export.xml.filename" value="LogDetails.xml"/>
			  <display:setProperty name="export.csv.filename" value="LogDetails.csv"/>
           <display:setProperty name="paging.banner.onepage" value=" " />
	<display:column sortable="true" title="Index"><%= i++%>
	<%  cd = ((DataDTO) pageContext.getAttribute("logList")); %>
	</display:column>
	<display:column property="indexName" sortable="true" title="Index Name"/>
	<display:column property="indexDesc" sortable="true" title="Index Desc"/>
		</display:table>
		</div>
	</td>
</tr>

</table>
</div>
</td>
</tr>

</table>
</td>
</tr>
</table>
</form>
</div>

</body>
</html>