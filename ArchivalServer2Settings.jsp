
<%@page import="com.g1.e2.vault.VaultClient.Document"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.g1.e2.vault.VaultClient.Account"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    
<s:include value="header.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%
String serverHost =  request.getAttribute("serverHost")!= null? request.getAttribute("serverHost").toString():"";
String serverIP =  request.getAttribute("serverIP")!= null? request.getAttribute("serverIP").toString():""; 
String serverPort =  request.getAttribute("serverPort")!= null? request.getAttribute("serverPort").toString():""; 
String softwarePath =  request.getAttribute("softwarePath")!= null? request.getAttribute("softwarePath").toString():""; 
%>
<script language="javascript" type="text/javascript">

function submitForm(button)
{
	//action="ActiveDirectoryAction"
	//alert(document.pressed);
	if(document.getElementById("serverHost").value == "")
	{
	alert("Please Enter Server Host");
	document.getElementById("serverHost").focus();
	return false;
	}
	if(document.getElementById("serverIP").value == "")
	{
	alert("Please Enter Server IP");
	document.getElementById("serverIP").focus();
	return false;
	}
	if(document.getElementById("serverPort").value == "")
	{
	alert("Please Enter Server Port");
	document.getElementById("serverPort").focus();
	return false;
	}
	if(document.getElementById("softwarePath").value == "")
	{
	alert("Please Enter Software Path");
	document.getElementById("softwarePath").focus();
	return false;
	}
	if(button.value != null || button.value != "")
			{
		if(button.value == "Test Connection")
			{
			document.getElementById("act").value = "test";
			//alert(document.getElementById("act").value);
			}
/*Nithu Alexander : 08/09/2014
Changed as part of code merging between SIT and eclipse 
*/
	document.getElementById("archsubmitform").action = "	.action?server=server2";
	document.getElementById("archsubmitform").submit;
	return true;
			}
	else
		{
		return false;
		}
	}
</script>
</head>
<body>
<table width="100%"><tr><td>
<s:include value="CommonTabs.jsp" />
<td></tr><tr><td>
<table align="center"  width="100%">
<tr>
<td>
<s:include value="SystemCommonTabs.jsp" />
</td>
</tr>
<tr><td>
<table align="center"  width="100%">
<tr>
<td>
<s:include value="e2vaultCommonTabs.jsp" />
</td>
</tr>
<tr>
<td >
<form id="archsubmitform" name="archsubmitform">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>	
<table width="100%" border="1px solid #778" height="500" valign="top" align="center">
<tr><td valign="top">
<table width="100%" valign="top">

<tr >
<td align="left" colspan=2>
<b>Archival Server Configuration:</b>
</td>
</tr>
<tr>
<td align="right">
Server Host Name:
</td>
<td align="left">
<input type="text" name="serverHost" id="serverHost" value="<%=serverHost%>"/>
</td>
</tr>
<tr>
<td align="right">
Server IP Address:
</td>
<td align="left">
<input type="text" name="serverIP" id="serverIP" value="<%=serverIP%>"/>

</td>
</tr>
<tr>
<td align="right">
Server Port Number:
</td>
<td align="left">
<input type="text" name="serverPort" id="serverPort" value="<%=serverPort%>"/>

</td>
</tr>
<tr>
<td align="right">
Software Path:
</td>
<td align="left">
<input type="text" name="softwarePath" id="softwarePath" value="<%=softwarePath%>"/>

</td>
</tr>
<tr>
<td align="center" colspan=2>
<input type="hidden" name="act" id="act"/>
</td>

</tr>
<tr align="center">
<td align="center" colspan=2>
<s:property value="message" />
</td>
</tr>
<tr>
<td align="center" colspan=2>

<input type="submit" value="Test Connection"   onclick="return submitForm(this);"/> <input type="submit" value="Save"   onclick="return submitForm(this);"/> <input type="submit" value="Cancel"  onclick="return cancelForm();"/>
</td>

</tr>
</table>
</td>

</tr>
</table>
</form>
</td>
</tr>

</table>
</td>
</tr>

</table>
</td>
</tr>

</table>
</body>
</html>