<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.g1.e2.vault.VaultClient.Account"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    
<s:include value="adminHeader.jsp" />
<%
	Account account = request.getAttribute("selectedAccount")!=null?(Account)request.getAttribute("selectedAccount"):null;
	Map<String,Integer> detailProduct = (Map<String,Integer>)request.getAttribute("detailProduct");
	if(account!=null){
%>
<script language="javascript" type="text/javascript">

function submitForm()
{
	//action="ActiveDirectoryAction"
	//alert(document.pressed);
	
	document.getElementById("accForm").action = "ResultAccount.action";
	document.getElementById("accForm").submit;
	return true;
		
	}
</script>
<form name="accForm" id="accForm">
<div align="right"><input type="submit" onclick="return submitForm();" value="Back" /></div>
</form>


	<table width="200px">
		<tr class='odd'>
			<td class='selector'>Account No</td>
			<td class='selector'>:</td>
			<td class='selector'><b><%=account.getAccountNumber()%></b></td>
		</tr>
	</table>
	<b>Detail Product</b>
	<table class="dataTable">
		<thead>
			<tr>
				<th class="sortable wi_ctrl selector">Product Name</th>
				<th class="sortable wi_ctrl selector">Total Documents</th>
				<th class="sortable wi_ctrl selector">Action</th>
			</tr>
		</thead>
		<%
		int no = 0;
			for(String key:detailProduct.keySet())
			{
		%>
			<tr class="<%=(no%2==0?"odd":"even")%>">
				<td class='selector'><%=key%></td>
				<td class='selector'><%=detailProduct.get(key)%></td>
				<td class='selector'><a href='ListDocument.action?acc=<%=account.getAccountNumber()%>&p=<%=key%>'><b>Select</b></a></td>
			</tr>
		<%
		no++;
			}
		%>
	</table>
<%
	}
%>
<s:include value="footer.jsp" />