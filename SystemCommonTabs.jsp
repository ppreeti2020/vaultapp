<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<i></i>
<html>
<head>
<%
String subSystemTabId = request.getAttribute("subSystemTabId") != null?request.getAttribute("subSystemTabId").toString():"";
System.out.println("subSystemTabId:"+subSystemTabId);
%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxsubtabs.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script>

</script>
<title>Insert title here</title>
</head>
<body >
<ul id="countrysubtabs" class="shadetabs">
<!--<li id="ldapTab"><a href="ActiveDirectoryAction.action" rel="#default" class="selected">Test LDAP Connection</a></li>
<li id="vaultTab"><a href="VaultAction.action?server=server1" rel="countrysubcontainer1">Vault Configuration</a></li>
<li id="dbTab"><a href="DBAction.action" rel="countrysubcontainer1">Database Configuration</a></li>-->
<%if(subSystemTabId.equals("DB Config"))
		{%>
			<li><a href="DBAction.action" rel="#default" class="selected">Database Configuration</a></li>
			<li><a href="ActiveDirectoryAction.action" rel="countrysubcontainer1">Test LDAP Connection</a></li>
			<li><a href="VaultAction.action?server=server1" rel="countrysubcontainer1">Vault Configuration</a></li>
		<!--  	<li><a href="PrintersListAction.action" rel="countrysubcontainer1">Printer Configuration</a></li>-->
		<%}
	else if(subSystemTabId.equals("Vault Config"))
		{%>
			<li><a href="DBAction.action" rel="countrysubcontainer1">Database Configuration</a></li>
			<li><a href="ActiveDirectoryAction.action" rel="countrysubcontainer1">Test LDAP Connection</a></li>
			<li><a href="VaultAction.action?server=server1" rel="#default" class="selected">Vault Configuration</a></li>
		<!--	<li><a href="PrintersListAction.action" rel="countrysubcontainer1">Printer Configuration</a></li> -->
			
		<%}
		else if(subSystemTabId.equals("LDAP Config"))
		{%>
			<li><a href="DBAction.action" rel="countrysubcontainer1">Database Configuration</a></li>
			<li><a href="ActiveDirectoryAction.action" rel="#default" class="selected">Test LDAP Connection</a></li>
			<li><a href="VaultAction.action?server=server1" rel="countrysubcontainer1">Vault Configuration</a></li>
		<!--	<li><a href="PrintersListAction.action" rel="countrysubcontainer1">Printer Configuration</a></li> -->
		<%}
		else if(subSystemTabId.equals("Printer Config"))
		{%>
			<li><a href="DBAction.action" rel="countrysubcontainer1">Database Configuration</a></li>
			<li><a href="ActiveDirectoryAction.action" rel="countrysubcontainer1">Test LDAP Connection</a></li>
			<li><a href="VaultAction.action?server=server1" rel="countrysubcontainer1">Vault Configuration</a></li>
		<!--	<li><a href="PrintersListAction.action" rel="#default" class="selected">Printer Configuration</a></li> -->
		<%}
%></ul>
<script type="text/javascript">

var countries=new ddajaxtabs("countrysubtabs", "countrydivsubcontainer");

countries.init();

</script>
</body>
</html>
