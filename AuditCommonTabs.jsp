<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
String auditLogTabId = request.getAttribute("auditLogTabId") != null?request.getAttribute("auditLogTabId").toString():"";
boolean archivalTabs=request.getAttribute("archivalTabs") != null?(Boolean)request.getAttribute("archivalTabs"):false;
System.out.println("auditSubTabId:"+auditLogTabId);
%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxsubtabs.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

<%
//String s=(String)request.getAttribute("hostId");
String hostId=(String)request.getParameter("hostId");
//System.out.println("xxxxx"+s);
//System.out.println("yyyyyy"+s1);

%>

</script>
<script>

</script>
<title>Insert title here</title>
</head>
<body >
<ul id="countrytabs1" class="shadetabs">
<% if(!auditLogTabId.equals("Index1"))
{ if(auditLogTabId.equals("Log"))
	{%>
<li id="aud1"><a href="AuditAction.action?server=server1" rel="#default" class="selected">Log</a></li>

<%

	
	}

else if(auditLogTabId.equals("indexLog"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Indexing&hostId=<%=hostId%>&tab=indexLog" rel="#default"  class="selected">Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Indexing&hostId=<%=hostId%>&tab=index" rel="countrydivcontainer1" >Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Indexing&hostId=<%=hostId%>&tab=indexResource" rel="countrydivcontainer1">Resources</a></li>

<%
	
}
else if(auditLogTabId.equals("index"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Indexing&hostId=<%=hostId%>&tab=indexLog" rel="countrydivcontainer1"  >Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Indexing&hostId=<%=hostId%>&tab=index" rel="#default" class="selected">Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Indexing&hostId=<%=hostId%>&tab=indexResource" rel="countrydivcontainer1">Resources</a></li>

<%
	
}

else if(auditLogTabId.equals("indexResource"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Indexing&hostId=<%=hostId%>&tab=indexLog" rel="countrydivcontainer1"  >Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Indexing&hostId=<%=hostId%>&tab=index" rel="countrydivcontainer1">Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Indexing&hostId=<%=hostId%>&tab=indexResource" rel="#default"  class="selected">Resources</a></li>

<%
	
}




else if(auditLogTabId.equals("reIndexLog"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Reindexing&hostId=<%=hostId%>&tab=reIndexLog" rel="#default"  class="selected">Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Reindexing&hostId=<%=hostId%>&tab=reIndex" rel="countrydivcontainer1" >Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Reindexing&hostId=<%=hostId%>&tab=reIndexResource" rel="countrydivcontainer1">Resources</a></li>

<%
	
}
else if(auditLogTabId.equals("reIndex"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Reindexing&hostId=<%=hostId%>&tab=reIndexLog" rel="countrydivcontainer1"  >Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Reindexing&hostId=<%=hostId%>&tab=reIndex" rel="#default" class="selected">Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Reindexing&hostId=<%=hostId%>&tab=reIndexResource" rel="countrydivcontainer1">Resources</a></li>

<%
	
}

else if(auditLogTabId.equals("reIndexResource"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Reindexing&hostId=<%=hostId%>&tab=reIndexLog" rel="countrydivcontainer1"  >Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Reindexing&hostId=<%=hostId%>&tab=reIndex" rel="countrydivcontainer1">Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Reindexing&hostId=<%=hostId%>&tab=reIndexResource" rel="#default"  class="selected">Resources</a></li>

<%
	
}


else if(auditLogTabId.equals("purgingLog"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Purging&hostId=<%=hostId%>&tab=purgingLog" rel="#default"  class="selected">Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Purging&hostId=<%=hostId%>&tab=purge" rel="countrydivcontainer1" >Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Purging&hostId=<%=hostId%>&tab=purgeResource" rel="countrydivcontainer1">Resources</a></li>

<%
	
}
else if(auditLogTabId.equals("purge"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Purging&hostId=<%=hostId%>&tab=purgingLog" rel="countrydivcontainer1"  >Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Purging&hostId=<%=hostId%>&tab=purge" rel="#default" class="selected">Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Purging&hostId=<%=hostId%>&tab=purgeResource" rel="countrydivcontainer1">Resources</a></li>

<%
	
}

else if(auditLogTabId.equals("purgeResource"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Purging&hostId=<%=hostId%>&tab=purgingLog" rel="countrydivcontainer1"  >Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Purging&hostId=<%=hostId%>&tab=purge" rel="countrydivcontainer1">Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Purging&hostId=<%=hostId%>&tab=purgeResource" rel="#default"  class="selected">Resources</a></li>

<%
	
}



else if(auditLogTabId.equals("removeLog"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Remove&hostId=<%=hostId%>&tab=removeLog" rel="#default"  class="selected">Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Remove&hostId=<%=hostId%>&tab=remove" rel="countrydivcontainer1" >Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Remove&hostId=<%=hostId%>&tab=removeResource" rel="countrydivcontainer1">Resources</a></li>

<%
	
}
else if(auditLogTabId.equals("remove"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Remove&hostId=<%=hostId%>&tab=removeLog" rel="countrydivcontainer1"  >Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Remove&hostId=<%=hostId%>&tab=remove" rel="#default" class="selected">Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Remove&hostId=<%=hostId%>&tab=removeResource" rel="countrydivcontainer1">Resources</a></li>

<%
	
}

else if(auditLogTabId.equals("removeResource"))
{%>
<li id="aud1"><a href="AuditAction.action?server=server1&name=Remove&hostId=<%=hostId%>&tab=removeLog" rel="#countrydivcontainer1"  >Log</a></li>
<li id="aud2"><a href="ShowIndexesAction.action?server=server1&name=Remove&hostId=<%=hostId%>&tab=remove" rel="countrydivcontainer1">Index</a></li>
<li id="aud3"><a href="ResourceReportAction.action?server=server1&name=Remove&hostId=<%=hostId%>&tab=removeResource" rel="#default"  class="selected">Resources</a></li>

<%
	
}




else
	if(auditLogTabId.equals("Resources")){%>

<li id="aud1"><a href="ResourceReportAction.action?server=server1" rel="#default" class="selected">Resources</a></li>
	<%}
}
else {%>
<li id="aud2"><a href="AuditIndexAction.action?server=server1" rel="countrycontainer1">Index</a></li>
	<%}%>
</ul>
<script type="text/javascript">

var countries=new ddajaxtabs("countrytabs1", "countrydivcontainer1");
countries.setpersist(true);
countries.setselectedClassTarget("link"); //"link" or "linkparent"
countries.init();

</script>
</body>
</html>
