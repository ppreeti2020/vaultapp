<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="org.apache.commons.lang.StringEscapeUtils" %>

<%@page import="com.pb.dao.DataDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<!-- calendar stylesheet -->
  <link rel="stylesheet" type="text/css" media="all" href="styles/calendar-win2k-cold-1.css" title="win2k-cold-1" />

  <!-- main calendar program -->
  <script type="text/javascript" src="js/calendar.js"></script>

  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
 	adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar-setup.js"></script>
<title>e2 Vault Administration</title>
	<%	
		Map<String,Object> userGroups = (HashMap<String,Object>)request.getAttribute("userGroups");
		Map<String,Object> userProfiles = (HashMap<String,Object>)request.getAttribute("userProfiles");
		String accessdateFrom =  request.getAttribute("accessdateFrom")!= null? request.getAttribute("accessdateFrom").toString():""; 
		String accessdateTo =  request.getAttribute("accessdateTo")!= null? request.getAttribute("accessdateTo").toString():""; 
		List csrActions = request.getAttribute("csrActions")!= null?(ArrayList)request.getAttribute("csrActions"):new ArrayList();
		int i=0;
		%>
<script type="text/javascript">

function moveoutid()
{
	alert("Inside moveoutid");
	var userTab = document.getElementById("userTable");
	//alert(document.csrGrps);
	var userRowCount = userTab.rows.length;
	var seluserTab = document.getElementById("selGroupTable");
	var seluserRowCount = seluserTab.rows.length;
	 for(var i=0; i<userRowCount; i++) {
                var row1 = userTab.rows[i];
                var chkbox = row1.cells[0].childNodes[0];
				 var text = row1.cells[1].innerHTML;
                if(null != chkbox && true == chkbox.checked) {
				
					var row = seluserTab.insertRow(seluserRowCount);
					var cell1 = row.insertCell(0);
					
					var element1 = document.createElement("input");
					element1.type = "checkbox";
					element1.value = chkbox.value;
					element1.id = "selCsrGroups";
					cell1.appendChild(element1);
					var cell2 = row.insertCell(1);
					cell2.innerHTML = text;
				}
	 }
	
	
}

function selAllGroups(profile)
{
	
	//alert(profile.value);
	var grid = "";
	grid += "<table id='selGroupTable'><%for(String key: userProfiles.keySet())	{%>";
				if('<%=key%>' == profile.value)
				{ grid += "<%List groups = (ArrayList)userProfiles.get(key);System.out.println(groups.size());for(Iterator iter=groups.iterator();iter.hasNext();){ String groupdto = (String)iter.next();%><tr><td><input type='radio' id='csrGroups' name='csrGroups' value='<%= groupdto %>' onclick='selAllUsers(this);'/></td><td><%= groupdto %></td></tr><%}%>";
				 
			}
	grid += "<%} %></table>";
	document.getElementById("div2").innerHTML = grid;
}
function selAllUsers(group)
{
	
	//alert(group.value);
	var grid = "";
	grid += "<table id='selUserTable'><%for(String key: userGroups.keySet())	{ 	%>";
	if('<%=key%>' == group.value)
	{
		/* Nithu Alexander: 02 Oct 2015. Added replaceAll.("'","&#x27;"); for value of "csrUsers" entity. 
		As part of bug fix in AssignUsersToProfile() due to single quotes in user display name */
		grid += "<%List users = (ArrayList)userGroups.get(key);for(Iterator iter=users.iterator();iter.hasNext();){ String userdto = (String)iter.next();%><tr><td><input type='checkbox' id='csrUsers' name='csrUsers' value='<%=key.replaceAll("'","&#x27;")%>,<%=userdto.replaceAll("'","&#x27;")%>' /></td><td><%= userdto %></td></tr><%}%>";
	}
	grid += "<%} %></table>";
	document.getElementById("div3").innerHTML = grid;
}
function moveinid()
{
	alert("Inside moveinid");
	var sda = document.getElementById('userTable');
	
	var len = sda1.length;
	for(var j=0; j<len; j++)
	{
		if(sda1[j].selected)
		{
			var tmp = sda1.options[j].text;
			var tmp1 = sda1.options[j].value;
			sda1.remove(j);
			j--;
			var y=document.createElement('option');
			y.text=tmp;
			y.value=tmp1;
			try
			{
			sda.add(y,null);}
			catch(ex){
			sda.add(y);	
			}

		}
	}	
}

function CancelForm(button)
{
	//document.getElementById("assignUser").action = "ProfileListAction.action";
	//UsersListAction
	document.getElementById("assignUser").action = "UsersListAction.action";
	
	document.getElementById("assignUser").submit;
	return true;
			
}
		
	function ValidateForm(){
		
		var csrProfiles = document.getElementsByName("csrProfiles");
		var csrProfilesb = false;
		
		for(var v=0;v<csrProfiles.length;v++)
		{ 	 if(csrProfiles[v].checked){
			csrProfilesb = true;
		 	}
		 }
		if(csrProfilesb != true){
			alert("Please select the profile!!!");
			return false;
		}
		
		var csrGroups = document.getElementsByName("csrGroups");
		var csrGroupsb = false;
		
		for(var v=0;v<csrGroups.length;v++)
		{ 	 if(csrGroups[v].checked){
			csrGroupsb = true;
		 	}
		 }
		if(csrGroupsb != true){
			alert("Please select the group!!!");
			return false;
		}
		
		var csrUsers = document.getElementsByName("csrUsers");
		var csrUsersb = false;
		for(var v=0;v<csrUsers.length;v++)
		{ 	 if(csrUsers[v].checked){
			csrUsersb = true;
		 	}
		 }
		if(csrUsersb != true){
			alert("Please select the User!!!");
			return false;
		}
		
		var actionId = document.getElementsByName("actionId");
		var aib = false;
		for(var v=0;v<actionId.length;v++)
		 { <% i=0;
			while(i<csrActions.size())
			{%>
				var ai = actionId[v].checked;
				if(ai != false)
				{ 	aib = true;
				}
				<%i++;
			}%>
		 }
		if(aib != true){
			alert("Please select atleast one Document Permissions!!!");
			return false;
		}
		
		var active = document.getElementsByName("active");
		var acib = false;
		for(var v=0;v<active.length;v++)
		{ 	 if(active[v].checked){
			 acib = true;
		 	}
		 }
		if(acib != true){
			alert("Please select the User Actions!!!");
			return false;
		}
				
		var accessdateFrom = document.getElementById("accessdateFrom").value;
		if(accessdateFrom.length==0){
			alert("Please select the Access Date From!!!");
			return false;
		}
		var accessdateTo = document.getElementById("accessdateTo").value;
		if(accessdateTo.length==0){
			alert("Please select the Access Date To!!!");
			return false;
		}
		
		var accessfromDays = document.getElementById("accessfromDays");
		var fromday = accessfromDays.options[accessfromDays.selectedIndex].value;
		if(fromday<=0){
			alert("Please select the Access Days From!!!");
			return false;
		}
		var accesstoDays = document.getElementById("accesstoDays");
		var today = accesstoDays.options[accesstoDays.selectedIndex].value;
		if(today<=0){
			alert("Please select the Access Days To!!!");
			return false;
		}
		
		var datefromDate = accessdateFrom.split(' ')[0];
		var timefromDate = accessdateFrom.split(' ')[1];
		
		var fromDate = new Date(datefromDate.split('-')[0],datefromDate.split('-')[1]-1,datefromDate.split('-')[2]);
		fromDate.setHours(timefromDate.split(':')[0],timefromDate.split(':')[1],timefromDate.split(':')[2]);
		
		var datetoDate = accessdateTo.split(' ')[0];
		var timetoDate = accessdateTo.split(' ')[1];
		
		var toDate = new Date(datetoDate.split('-')[0],datetoDate.split('-')[1]-1,datetoDate.split('-')[2]);
		toDate.setHours(timetoDate.split(':')[0],timetoDate.split(':')[1],timetoDate.split(':')[2]);
		
		var currDate = new Date();
		/* if(fromDate<currDate){
			alert("Access From Date cannot be less than today's date::"+currDate+"!!!");
			return false;
		} */
		if(toDate<currDate){
			alert("Access To Date cannot be less than today's date!!!");
			return false;
		}
		
		if(fromDate>toDate)
		{
		alert("Access From Date is greater than Access To Date");
		return false;
		}
		
		if(fromday>today){
			alert("Access from Day should be before Access to Day");
			return false;
		}
		
		return true;
		
	}
</script>
</head>
<body>
	<jsp:include page="CommonTabs.jsp" />
	
	<%--Nithu Alexander: 17 Aug 2015, Changing width from 1230px to 100%, height from 610px to 100% --%>
	<div id="countrydivcontainer" style="border:1px solid gray; width:100%; height:100%; margin-bottom: 1em; padding: 10px; overflow:auto">
	
	<%--Nithu Alexander: 12 Aug 2015
	Including  useradminCommonTabs.jsp and countrydivsubcontainer as part of Tabs changes for VaultApp Profile Admin
	--%>
	<jsp:include page="useradminCommonTabs.jsp" />
	<div id="countrydivsubcontainer" style="border:1px solid gray; width:1180px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">

			<table align="center"  width="100%">
			<tr><td></td></tr>
			<tr><td>
				<!-- <table width="100%" border="1px solid #778" height="500" valign="top"> -->
				<table width="100%" border="1" height="500" valign="top">
				<tr><td valign="top" align="left">
					<s:form id="assignUser" action="AssignUsersToProfileAction">
					<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
					<table align="left" >
					<tr><td><b>Selected Profiles</b></td>
						<td><b>Selected Groups</b></td>
						<td><b>Selected Users </b></td>
					</tr>
					<tr><td>			
			 			<div id="div1" style="overflow:auto; width:250px;border-top: solid 1px #C1C1C1;border-bottom: solid 1px #C1C1C1;
  							 border-left: solid 2px #C1C1C1; border-right: solid 2px #C1C1C1;height:120px;">
  							<table id="userTable"> 
							<%for(String key: userProfiles.keySet())
						  	{%>
		 						<tr><td><input type="radio" id="csrProfiles" name="csrProfiles" value="<%= key %>" onclick="selAllGroups(this);"/></td><td><%= key %></td></tr>
							<%}%>
							</table></div>
			 			</td>
			 			<td>
						<div id="div2" style="overflow:auto; width:250px;border-top: solid 1px #C1C1C1; border-bottom: solid 1px #C1C1C1;
  							 border-left: solid 2px #C1C1C1; border-right: solid 2px #C1C1C1;height:120px;"> 		
				    	</div>
						</td>
			            <td>
						<div id="div3" style="overflow:auto; width:250px;border-top: solid 1px #C1C1C1; border-bottom: solid 1px #C1C1C1;
  							 border-left: solid 2px #C1C1C1; border-right: solid 2px #C1C1C1;height:120px;"> 		
						</div>
						</td>
					</tr>
					<tr><td><b>Document Permissions</b></td>
						<td><b>User Actions</b></td>
					</tr>
					<tr>
					<td>
						<%
							Iterator actIter = csrActions.iterator();
						%>
						<div id="div1" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;border-bottom: solid 1px #C1C1C1;border-left: solid 2px #C1C1C1;
  						 border-right: solid 2px #C1C1C1;height:90px;">
  					 	<table> 
					 	<%
						while (actIter.hasNext()) {
							UserDTO uDto = (UserDTO)actIter.next();
							System.out.println("actions"+uDto.getActionName());
					 	%>
					 		<tr><td><input type="checkbox" name="actionId" id="actionId" value="<%= uDto.getActionId() %>"  onclick="checkGroup(this);"/><%= uDto.getActionName() %></td>
					 		</tr>
						<%} %>
					 	</table>
						</div>
					</td>
					<td>
						<div id="div2" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;border-bottom: solid 1px #C1C1C1;border-left: solid 2px #C1C1C1;
  						border-right: solid 2px #C1C1C1;height:90px;">
  						<table> 
						<tr><td><input type="radio" name="active" id="active" value="Active"  onclick="checkGroup(this);"/>Active</td></tr>
						<tr><td><input type="radio" name="active" id="active" value="Inactive"  onclick="checkGroup(this);"/>InActive</td></tr>
						<tr><td><input type="radio" name="active" id="active" value="Disabled"  onclick="checkGroup(this);"/>Disabled</td></tr>
						<tr><td><input type="radio" name="active" id="active" value="Auto Disabled"  onclick="checkGroup(this);"/>Auto Disabled</td></tr>
						<tr><td><input type="radio" name="active" id="active" value="Mark for Deletion"  onclick="checkGroup(this);"/>Mark for Deletion</td></tr>
						</table>
						</div>
					</td>
					<td colspan="5">
							<table align="top">
								 <tr><td>Access Date From:</td><td><input type="text" name="accessdateFrom" id="accessdateFrom" value="<%=accessdateFrom %>" onclick="checkGroup(this);"/>
								 	 </td>
								 	 <td>To:</td>
								 	 <td><input type="text" name="accessdateTo" id="accessdateTo" value="<%=accessdateTo %>"/></td>
								 </tr>
								 <tr><td>Access Days From:</td>
								 	 <td><select name="accessfromDays" id="accessfromDays" onclick="checkGroup(this);">
												<option value="1">Monday</option>
												<option value="2">Tuesday</option>
												<option value="3">Wednesday</option>
												<option value="4">Thursday</option>
												<option value="5">Friday</option>
												<option value="6">Saturday</option>
												<option value="7">Sunday</option>
										 </select></td>
									<td>To:</td>
									<td><select name="accesstoDays" id="accesstoDays" onclick="checkGroup(this);">
												<option value="1">Monday</option>
												<option value="2">Tuesday</option>
												<option value="3">Wednesday</option>
												<option value="4">Thursday</option>
												<option value="5">Friday</option>
												<option value="6">Saturday</option>
												<option value="7">Sunday</option>
										</select>
									</td></tr>
									<tr><td colspan="3" align="left">
									<input type="Submit" value="Assign" onclick="return ValidateForm()"/>&nbsp;&nbsp;<input type="Submit" value="Cancel" onclick="return CancelForm()"></td>
								</tr>
							</table>
					</td>
					</tr>	
				</table>
				</s:form></td>
				</tr>
				</table>
		</td></tr>
		</table>
		</div>
		</div>
		<script type="text/javascript">
  
	 Calendar.setup({
        inputField     :    "accessdateFrom",      // id of the input field
        ifFormat       :    "%Y-%m-%d %H:%M:%S",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	 Calendar.setup({
        inputField     :    "accessdateTo",      // id of the input field
        ifFormat       :    "%Y-%m-%d %H:%M:%S",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	</script>		
</body>
</html>