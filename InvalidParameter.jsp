<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.*"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="java.text.*"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="com.pb.evault.DocumentHandle"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<title>Document Management Results</title>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<s:include value="adminHeader.jsp" />

<%
String prodDBId = ((String)session.getAttribute("ProdDBId")) != null?((String)session.getAttribute("ProdDBId")):"";
%>
<script type="text/javascript">

function backToSearch(prodDBId){
	document.getElementById("invalidParameterForm").action="SearchAccountNew.action?prdbID="+prodDBId;
	document.getElementById("invalidParameterForm").submit();
	return true;
}
</script>
</head>

<body>

<div id="countrydivcontainer" style="border:1px solid gray; width:1500px; height:630px; margin-bottom: 1em; padding: 10px; overflow:auto">

<div>
<B>Invalid Search Parameters. Please Contact Administrator.</B>

<form id="invalidParameterForm" name="invalidParamterForm" method="post">
		<table>
		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr>
        <td align="left"><input type="button" id="BackToSearch" value="Back to Search Page" onclick='return backToSearch("<%=prodDBId%>")'/>
        </td></tr>
        </table>		
</form>

</div>
<s:include value="footer.jsp" />



</div>

</body>
</html>