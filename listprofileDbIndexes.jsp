<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dto.UserDTO" %>
<s:include value="adminHeader.jsp" />
  <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="styles/jquery.treeview.css" />
<link rel="stylesheet" href="styles/screen.css" />
<script src="js/jquery_treeview.js" type="text/javascript"></script>
<script src="js/jquery.cookie.js" type="text/javascript"></script>
<script src="js/jquery.treeview.js" type="text/javascript"></script>
<script type="text/javascript" src="js/demo.js"></script>
<title>e2 Vault Administration</title>
<%
String profileName = (String)request.getAttribute("profileName");
System.out.println("profileName"+profileName);
String profileAction = request.getAttribute("profileAction")!=null?request.getAttribute("profileAction").toString():"";
System.out.println("profileAction:::"+profileAction);
List<UserDTO> seldb = request.getAttribute("seldb")!=null?(ArrayList<UserDTO>)request.getAttribute("seldb"): new ArrayList<UserDTO>();
System.out.println("seldb"+seldb);
Map<String,List<String>> profilePrePopulateList = request.getAttribute("profilePrePopulateList")!=null?(Map<String,List<String>>)request.getAttribute("profilePrePopulateList"): new HashMap<String,List<String>>();
UserDTO dto = new UserDTO();
Map<String,Map<String,Long>> profileDetailIndex = request.getAttribute("profileDetailIndex")!=null?(Map<String,Map<String,Long>>)request.getAttribute("profileDetailIndex"):new HashMap<String,Map<String,Long>>();

/* Nithu Alexander: 23/10/2014. Retrieving SelectedProfileId from session as part of BACK Button change.
This is passed to JS EditDBIndexBackSubmit()*/
String selprofileID = ((Long)session.getAttribute("profileId")) != null?((Long)session.getAttribute("profileId")).toString():"";
System.out.println("selprofileID:::"+selprofileID);

%>
<script>
function CancelcreateForm(button)
{
	document.getElementById("SaveProfileGroupSettingsAction").action = "ShowCreateProfileAction.action";
	document.getElementById("SaveProfileGroupSettingsAction").submit;
	return true;
			
}
function CanceleditForm()
{
	document.getElementById("SaveProfileGroupSettingsAction").action = "ProfilesListAction.action";
	document.getElementById("SaveProfileGroupSettingsAction").submit;
	return true;
}

function ValidateForm(){
	var aInputs = document.getElementsByTagName('input');
	var count = 0;
	for (var i=0;i<aInputs.length;i++) {
		var type = aInputs[i].type;
		var id = aInputs[i].id;
		if(type != "hidden"){
			if(aInputs[i].checked){
				count++;
			}
		}
	}
	if(count==0){
		alert("Please select atleast one Index for the profile!!!");
		return false;
	}
	
	else{
		return true;
	}
	
}

function EditDBIndexBackSubmit(selectedProfileId){
	document.getElementById("SaveProfileGroupSettingsAction").action="EditProfileAction.action?editflag=editProfile&selprofileId="+selectedProfileId;
    document.getElementById("SaveProfileGroupSettingsAction").submit();
	return true;
}

function createDBIndexBackSubmit(profileName,profileAction){
	document.getElementById("SaveProfileGroupSettingsAction").action="PopulateProfileAction.action?profileName="+profileName+"&profileAction="+profileAction;
    document.getElementById("SaveProfileGroupSettingsAction").submit();
	return true;
}
</script>
  <title>tables</title>
  <style type="text/css">
  #playlist tr.header {
      background-color:#0072a8;
    }
    #playlist tr.even td {
      background-color: #eee;
    }
    #playlist tr.odd  td {
      background-color: #fff;
    }
    #navigation a.active,a.visited, a.hover {
	color: black;
	background-color: transparent;
}
  </style>
</head>

<body>
<jsp:include page="CommonTabs.jsp" />
	<div id="countrydivcontainer" style="border:1px solid gray; width:1210px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">
	<jsp:include page="userpadminCommonTabs.jsp" />
		<div id="countrydivsubcontainer"
			style="border: 1px solid gray; width: 1160px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">
			<s:form id="SaveProfileGroupSettingsAction" action="SaveProfileGroupSettingsAction">	
			<table>
			<tr><td align="left"><b>Profile Name:</b>&nbsp;<%=profileName%></td></tr>
			<tr><td align="left"><b>Database Index settings</b></td></tr>
			<tr><td>
			<table width="100%" border="1" height="450">
			<tr>
			<%	Iterator<UserDTO> dbItrer = seldb.iterator();
						while (dbItrer.hasNext()) {
							UserDTO uDto = (UserDTO) dbItrer.next();
							Map<String,Long>detailIndex = new HashMap<String,Long>();
							List<String> prePopulateList = new ArrayList<String>();%>
							<td align="left"><b>Database Name:<%=uDto.getDbName() %></b>
							<table>
							<%for(String dbName:profileDetailIndex.keySet()){
								if(dbName.equalsIgnoreCase(uDto.getDbName())){
									detailIndex = profileDetailIndex.get(dbName);
									for(String dbName1:profilePrePopulateList.keySet()){
										if(dbName1.equalsIgnoreCase(uDto.getDbName())){
											prePopulateList = profilePrePopulateList.get(dbName1);
										}
									}
								}
							  }
							  if(detailIndex.size()==0){ %>
							  <tr><td>
    							  <table align="center">
       								<tr ><td align="center"><font color="red">No Indexes found for the selected database in VaultApp</font></td></tr>
								  </table>
							  </td></tr>
							  <%}else{ %>
								  <tr><td>
									  <table>
										<tr><td><b>Index</b></td></tr>
											<%
											String databaseName = uDto.getDbName();
											for(String key:detailIndex.keySet()) { 
												if (prePopulateList != null && prePopulateList.contains("Index_"+detailIndex.get(key))) {%>
												<tr><td>
					    							<input type="checkbox" name="Index_<%=databaseName%>_<%=detailIndex.get(key)%>_<%=key%>" id="Index_<%=databaseName%>_<%=detailIndex.get(key)%>_<%=key%>" value="Index_<%=databaseName%>_<%=detailIndex.get(key)%>_<%=key%>" class="<%=detailIndex.get(key)%>" checked/><%=key%>
													<input type="hidden" name="Index_<%=databaseName%>_<%=detailIndex.get(key)%>_<%=key%>" id="Index_<%=databaseName%>_<%=detailIndex.get(key)%>_<%=key%>" value="unchecked" />
												</td></tr>
												<%}else{%>
												<tr><td>
													<input type="checkbox" name="Index_<%=databaseName%>_<%=detailIndex.get(key)%>_<%=key%>" id="Index_<%=databaseName%>_<%=detailIndex.get(key)%>_<%=key%>" value="Index_<%=databaseName%>_<%=detailIndex.get(key)%>_<%=key%>" class="<%=detailIndex.get(key)%>" /><%=key%>
													<input type="hidden" name="Index_<%=databaseName%>_<%=detailIndex.get(key)%>_<%=key%>" id="Index_<%=databaseName%>_<%=detailIndex.get(key)%>_<%=key%>" value="unchecked" />
												</td></tr>
												<%}
											}%>
										</table></td></tr>
									
							<%}%>
							</table>
							</td>
						<%}%>
					</tr></table></td></tr>
					<tr><td colspan="5">
						<!-- Nithu Alexander: 24/10/2014. Added EditDBIndexBackSubmit() and createDBIndexBackSubmit()
						as part of BACK Button issue fix -->
						<!-- <input type="Submit" value="Back" onclick="history.go(-1);return false;"/>&nbsp;&nbsp; -->
						<%if(profileAction.equalsIgnoreCase("edit")){ %>
							<input type="button" value="Back" onclick='EditDBIndexBackSubmit("<%=selprofileID%>")'/>&nbsp;&nbsp;
						<%}else if(profileAction.equalsIgnoreCase("create")){ %>
							<input type="button" value="Back" onclick='createDBIndexBackSubmit("<%=profileName%>","<%=profileAction%>")'/>&nbsp;&nbsp; 
						<%} %>
						<input type="Submit" value="Next" onclick="return ValidateForm()"/>&nbsp;&nbsp;
						<%if(profileAction.equalsIgnoreCase("create")){ %>
							<input type="Submit" value="Cancel" onclick="return CancelcreateForm()">
						<%}else if(profileAction.equalsIgnoreCase("edit")){ %>
							<input type="Submit" value="Cancel" onclick="return CanceleditForm()">
						<%} %>
					</td></tr>
					</table>
		</s:form>
		</div></div>
</body>
</html>