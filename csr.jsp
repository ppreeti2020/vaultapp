<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Set" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.g1.e2.vault.SearchIndex" %>
<%@page import="com.pb.common.CommonConstants" %>
<%@page import="com.pb.common.PropertyUtils" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CSR Console</title>
        <link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
		<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
		<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
		<meta name="helpHref" content="GettingStarted" />
		<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
		<link rel="stylesheet" href="styles/login.css" />
		<link rel="stylesheet" href="styles/common/messages.css" />
		<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body>
    	<div id="page">
    	<div id="header" class="clearfix">
		<div id="branding">
			<img id="logo" src="images/header/logo.JPG"/>
		</div>
		<div id="applicationDetail">
			<ul id="productName">
				<li>EngageOne Customer Service Correspondent</li>
			</ul>
			<ul id="userDetail">
			</ul>

			<form name="Logout" action="<%=request.getContextPath()%>/Logout.action" >
        		<%
        		out.println("<font color='white'><input type='button' onClick=\"window.location.href='Interface';\" value='Home'/></font>");	
        		if(session.getValue("GROUP_MAKER").equals("true"))
        		{
        			out.println("<font color='white'><input type='button' onClick=\"window.location.href='MakerSubmitAction.action';\" value='Setup Permission'/></font>");	
        		}
        		if(session.getValue("GROUP_CHECKER").equals("true"))
    			{
    				out.println("<font color='white'><input type='button' onClick=\"window.location.href='CheckerSubmitAction.action';\"  value='Approve Permission'/></font>");	
    			}
        	%>
        	<input type="submit" value="Logout" />
        	</form>
        	</ul>
		</div>
		<div id="curve">
			<img id="curve" src="styles/themes/purple/images/curve.gif" />
		</div>
		<hr />
	</div>
        <h4><s:property value="message" /></h4>
        <br>
        <form name="searchVaultForm" action="<%=request.getContextPath()%>/Interface" method="POST">
        <select name="selectedIndex" id="selectedIndex">
        <%
        	Set<SearchIndex> searchIndexes = (Set<SearchIndex>)request.getAttribute("searchIndexes");
        	Iterator<SearchIndex> itr = searchIndexes.iterator();
        	while (itr.hasNext()) {
        		SearchIndex idx = itr.next();
        		if (!idx.getName().equals(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.SEARCH_INDEX_NAME))) {
        %>
        		<option value="<%=idx.getName()%>"><%=idx.getDescription()%></option>
        		
        <%		
        		}
        	}
        %>
        </select>
         for
        <input type="text" name="searchKey" value="" size="32" />
        <input type="image" alt="Search" src="images/cgo.gif" border="0" align="absMiddle">
        </form>
        <hr>
        
</div>
    </body>
</html>
