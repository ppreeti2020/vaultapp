<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<%@page import="java.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec"%>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet"
	type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css"
	type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css"
	media="all" />
<title>e2 Vault Administration</title>
<%
	String tableName = request.getAttribute("tableName") != null ? request
			.getAttribute("tableName").toString() : "";
	String tableName1 = request.getAttribute("tableName1") != null ? request
			.getAttribute("tableName1").toString() : "";
	System.out.println("TableName:::" + tableName);
	System.out.println("TableName1:::" + tableName1);
	String message = request.getAttribute("message") != null ? request
			.getAttribute("message").toString() : "";
	List csrDatabase = request.getAttribute("csrDatabases") != null ? (ArrayList) request
			.getAttribute("csrDatabases") : new ArrayList();
	List<String> dbindexList = request.getAttribute("dbindexList") != null ? (ArrayList<String>) request
			.getAttribute("dbindexList") : new ArrayList<String>();
	System.out.println("dbindexList size:::" + dbindexList.size());
%>
<script type="text/javascript">
  function selectDatabase(sel)
 {
	document.getElementById("createCrossRefTableForm").action="CreateCrossRefTable.action";
    document.getElementById("createCrossRefTableForm").submit();
	return true;
 }
 
 
 </script>
</head>
<body>
	<jsp:include page="CommonTabs.jsp" />

	<div id="countrydivcontainer"
		style="border: 1px solid gray; width: 1550px; height: 610px; margin-bottom: 1em; padding: 10px; overflow: auto">


		<jsp:include page="SystemCommonTabs.jsp" />

		<div id="countrydivcontainer1"
			style="border: 1px solid gray; width: 1500px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">

			<jsp:include page="DatabaseCommonTabs.jsp" />

			<div id="countrydivcontainer1"
				style="border: 1px solid gray; width: 1500px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">
				<s:if test="hasActionErrors()">
					<s:actionerror />
				</s:if>
				<font color="#ff0000"><s:property value="message" /> </font>
				<table width="100%" border="1px solid #778" height="500"
					valign="top">
					<tr>
						<td valign="top" align="center">
							<form id="createCrossRefTableForm" name="createCrossRefTableForm"
								method="post">
								<s:hidden id="tableName1" name="tableName1" />
								<s:hidden id="dbindexList" name="dbindexList" />
								<%
									if (csrDatabase.size() != 0) {
								%>
								<table align="center">
									<tr>
										<td align="right"><B>Table Name:</B></td>
										<td><select name="tableName" id="tableName"
											style="width: 450px;" onchange="return selectDatabase(this);">
												<option value="">Select Database to create table
													for</option>
												<%
													for (Iterator iter = csrDatabase.iterator(); iter.hasNext();) {
															UserDTO dto = (UserDTO) iter.next();
															System.out.println("dto.getDbId()" + dto.getDbId());
												%>
												<option value="<%=dto.getDbId()%>_<%=dto.getDbName()%>"><%=dto.getDbDesc()%></option>
												<%
													}
												%>
										</select>
										</td>
									</tr>
								</table>
								<%
									} else {
										message = "No Databases!!";
									}
								%>
							</form></td>
					</tr>
				</table>
			</div>
		</div>
		</div>
</body>
</html>