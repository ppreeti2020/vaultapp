<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
String tabId = request.getAttribute("tabId") != null? request.getAttribute("tabId").toString():"";
System.out.println("tabId"+tabId);

//String act= request.getAttribute("act") != null?request.getAttribute("act").toString():"";
String act= request.getParameter("actType") != null?request.getParameter("actType").toString():"";
System.out.println("%%%%%%%%%%%%%%%%%%%%%%% act : " + act);
%>
<% String reports = session.getAttribute("reports")!=null?session.getAttribute("reports").toString():"";
String useraccesscontrol = session.getAttribute("useraccesscontrol")!=null?session.getAttribute("useraccesscontrol").toString():"";
String sysaccesscontrol = session.getAttribute("sysaccesscontrol")!=null?session.getAttribute("sysaccesscontrol").toString():"";
String search = session.getAttribute("search")!=null?session.getAttribute("search").toString():"";
String rep = session.getAttribute("rep")!=null?session.getAttribute("rep").toString():"";
String audit = session.getAttribute("audit")!=null?session.getAttribute("audit").toString():"";
String userindexcontrol = session.getAttribute("userindexcontrol")!=null?session.getAttribute("userindexcontrol").toString():"";
String pradmincreation = session.getAttribute("pradmincreation")!=null?session.getAttribute("pradmincreation").toString():"";
String profileuserassign = session.getAttribute("profileuserassign")!=null?session.getAttribute("profileuserassign").toString():"";
String repository = session.getAttribute("repository")!=null?session.getAttribute("repository").toString():"";
System.out.println("access"+search);
%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxtabshome.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script>

</script>
<title>Insert title here</title>
</head>

<!-- Nithu Alexander: 06 Jul 2015
AVA-10: Fix for Cross-site framing  --> 
<%response.addHeader("X-Frame-Options","SAMEORIGIN");%>

<body>
<ul id="countrytabs" class="shadetabs">
<%
	if(useraccesscontrol.equals("true"))
	{ 
		if(tabId.equals("User Access Control")){%>
			<li id="tab_admin1"><a href="UsersListAction.action" rel="#default" class="selected">User Administration</a></li>
		<%}
		else { %>
			<li id="tab_admin1"><a href="UsersListAction.action" rel="countrycontainer">User Administration</a></li>
		<%}
	}
	
	if(pradmincreation.equals("true"))
	{
		if(tabId.equals("User Profile Control")){%>
			<li id="tab_pradmin"><a href="ProfilesListAction.action" rel="#default" class="selected">Profile Administration</a></li>
		<%}
		else { %>
			<li id="tab_pradmin"><a href="ProfilesListAction.action" rel="countrycontainer">Profile Administration</a></li>
		<%}
		
	}
	if(sysaccesscontrol.equals("true"))
	{
		if(tabId.equals("System Admin")){%>
			<li id="tab_sysadmin"><a href="ActiveDirectoryAction.action" rel="#default" class="selected">System Administration</a></li>
		<%}
		else { %>
			<li id="tab_sysadmin"><a href="ActiveDirectoryAction.action" rel="countrycontainer">System Administration</a></li>
		<%}
	}
	/* Nithu Alexander: 12 Aug 2015
	Below changes included for changing tabs & subtabs in Profile Admin screen
	1. Change "Profile - User Assignment" to "User Management"
	2. Change Tab Id condition from "Prof Assign" to "User Access Control"
	3. Change action name in href from AssignCreateGroupsForProfilesAction.action to UsersListAction.action
	*/
	
	if(profileuserassign.equals("true"))
	{
		if(tabId.equals("User Access Control")){%>
			<li id="tab_prasgn"><a href="UsersListAction.action" rel="#default" class="selected">User Management</a></li>
		<%}
		else {%>
			<li id="tab_prasgn"><a href="UsersListAction.action" rel="countrycontainer">User Management</a></li>
		<%}	
	}
	if(userindexcontrol.equals("true"))
	{
		if(tabId.equals("Ind Cntrl")){%>
			<li id="tab_idxctrl"><a href="DBIndexAction.action" rel="#default" class="selected">User Index Control</a></li>
		<%}
		else { %>
			<li id="tab_idxctrl"><a href="DBIndexAction.action" rel="countrycontainer">User Index Control</a></li>
		<%}
	}
	if(search.equals("true") && !(act.equalsIgnoreCase("view")) && !(act.equalsIgnoreCase("download")) && !(act.equalsIgnoreCase("print")) &&
			!(act.equalsIgnoreCase("reprint"))){ 
		if(tabId.equals("CSR-search")){%>
			<li id="tab_search"><a href="SearchAccountNew.action" rel="#default" class="selected">Search</a></li>
		<%}
		else { %>
			<li id="tab_search"><a href="SearchAccountNew.action" rel="countrycontainer">Search8</a></li>
		<%}
	}
	if(reports.equals("true1"))
	{ 	if(tabId.equals("Reports")){%>
			<li id="tab_reports"><a href="AdminReportAction.action" rel="#default" class="selected">Reports</a></li>
		<%}
		else { %>
			<li id="tab_reports"><a href="AdminReportAction.action" rel="countrycontainer">Reports</a></li>
		<%}
	}
	if(repository.equals("true"))
	{
		if(tabId.equals("Repository")){%>
			<li id="tab_rep"><a href="DocManagerNew1.action" rel="#default" class="selected">Repository</a></li>
		<%}
		else { %>
			<li id="tab_rep"><a href="DocManagerNew1.action" rel="countrycontainer">Repository</a></li>
		<%}
	}
	if(audit.equals("true"))
	{
		if(tabId.equals("Audit")){%>
			<li id="tab_audit"><a href="AuditServerAction.action?server=server1" rel="#default" class="selected">Process Reports</a></li>
			<li id="tab_auditdata"><a href="AuditProfileAction.action?server=server1" rel="countrycontainer">Audit Reports</a></li>
		<%}
		else if(tabId.equals("AuditCheck")){ %>
			<li id="tab_audit"><a href="AuditServerAction.action?server=server1" rel="countrycontainer">Process Reports</a></li>
			<li id="tab_auditdata"><a href="AuditProfileAction.action?server=server1" rel="#default" class="selected">Audit Reports</a></li>
		<%}
		else { %>
			<li id="tab_audit"><a href="AuditServerAction.action?server=server1" rel="countrycontainer">Process Reports</a></li>
			<li id="tab_auditdata"><a href="AuditProfileAction.action?server=server1" rel="countrycontainer">Audit Reports</a></li>
		<%}
		
	} %>
</ul>
<script type="text/javascript">
var countries=new ddajaxtabs("countrytabs", "countrydivcontainer");
countries.setpersist(true);
countries.setselectedClassTarget("link"); //"link" or "linkparent"
countries.init();

</script>
</body>
</html>