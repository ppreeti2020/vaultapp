<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dto.DataDTO" %>
<%@page import="com.pb.dao.UserDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>

<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />

<title>e2 Vault Administration</title>
<s:include value="adminHeader.jsp" />
<%

String message = request.getAttribute("message")!= null?(String)request.getAttribute("message"):"";
System.out.println("message::"+message);
%>
<script type="text/javascript">
function checkAll(checklist)
{
	var list = document.getElementsByName("newUserId");
	if(list != null)
	{	for(var v=0;v<list.length;v++){
			if(list[v].checked){
				list[v].checked = false;
			}
			else if(!list[v].checked){
				list[v].checked = true;
			}
		}
	}
}
function deleteUser(button)
{
	
	var list = document.getElementsByName("newUserId");
	var UserNamelist = document.getElementsByName("newUserName");
	var delusers = "";
	for(var v=0;v<list.length;v++)
	{
		if(list[v].checked){
			var userVal = UserNamelist[v].value;
			if(delusers == "")
				delusers = userVal+",";
			else
				delusers = delusers + userVal+",";
		}
			
	}
	
	var r=confirm("do you want to delete "+delusers+"!!");
	
	if (r==true)
	 {
	var flag = 0;
	if(list != null)
	{
		var users = new Array(list.length);
	for(var v=0;v<list.length;v++)
		{
		if(!list[v].checked)
			{
			flag = 0;
			
			}
			else
		if(list[v].checked)
			users[v] = list[v].value;
			flag = 1;
		}
		
	}
if(flag == 0)
		 {
	alert("Please select User");
			return false;
}
	if(flag == 1)
		 {
	document.getElementById("userlistform").action = "DeleteUserAction.action";
	document.getElementById("userlistform").submit;
	return true;
		 }
	
	}
	else
  {
	return false;
  }
	
	
	//document.getElementById("newUserId").value = users;
	
}
</script>
</head>
<body>

<jsp:include page="CommonTabs.jsp" />

<%--Nithu Alexander: 17 Aug 2015, Changing width from 1200px to 100%, height from 610px to 100% --%>
<div id="countrydivcontainer" style="border:1px solid gray; width:100%; height:100%; margin-bottom: 1em; padding: 10px; overflow:auto">

<jsp:include page="useradminCommonTabs.jsp" />

<div id="countrydivsubcontainer" style="border:1px solid gray; width:1150px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">


<form name="userlistform" id="userlistform">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
<table width="100%"  height="500" valign="top">
<tr><td valign="top">
<table width="100%">
<tr>
<td align="left">
												<div id="mes"><font color="red">
													<%if(message != null && !message.equals(""))
												  	{%>
														<%=message%>
													<%}
												 	 else if(session.getAttribute("message") != null)
												  	{%>
														<%= session.getAttribute("message")%>
														<%session.setAttribute("message",null);
												  	}%>
												  	</font>
												</div>
											 </td>
											 </tr>
											 <tr>
<% List<String> csrAllUsers = request.getAttribute("csrAllUsers") != null?(ArrayList<String>)request.getAttribute("csrAllUsers"): new ArrayList<String>();
System.out.println("users"+csrAllUsers);
if(csrAllUsers.size() != 0)
{%>
<td align="left" valign="top"><input type="submit" name="delete" value="Delete" onclick="return deleteUser(this);"/>
</td></tr>
	<tr><td  align="left">
	<% int i=1; %>
	<%--Nithu Alexander: 21-Aug-2015, Including style parameter for DataTable --%>
	<display:table name="csrAllUsers" class="dataTable" style="position:static" id="users" pagesize="15" requestURI="/UsersListAction.action">
	<display:setProperty name="paging.banner.onepage" value=" " />
	<display:column sortable="true" title='<input type="checkbox" name="allbox" id="allbox" onclick="checkAll(this);"  />'>
    <input type="checkbox" name="newUserId" id="newUserId" value="${users.userId}"/>
    <input type="hidden" name="newUserName" id="newUserName" value="${users.userName}"/>
    </display:column>
   		<display:column property="userName" sortable="true" title="Users"/>
   		<!-- Nithu Alexander: 22/09/2014
   		Change to Display the FullName of every UserId -->
   		<display:column property="userDisplayName" sortable="true" title="User Name"/>
		 <display:column property="userGrp" sortable="true" title="Group Name"/>
		 <display:column property="accessdateFrom" sortable="true" title="Access Date From"/>
		 <display:column property="accessdateTo" sortable="true" title="Access Date To"/>
		 <display:column property="accessfromDay" sortable="true" title="Access From Days">
	</display:column>
	 <display:column property="accesstoDay" sortable="true" title="Access To Days">
		</display:column>
		<display:column property="active" sortable="true" title="User Actions">
		</display:column>
		  <display:column property="roleName" sortable="true" title="Roles"></display:column>
		 <display:column property="dbName" sortable="true" title="Repository"></display:column>
		<display:column href="EditUserAction.action?editflag=editUser" paramId="seluserId" paramProperty="userId" sortable="true" title="Action">Edit</display:column>
		
		</display:table></td>
<%}
		else
		{%>
<td align="center"><B>No Users Found</B></td>
<%		}%>	</tr>
			</table>
	
		
		</td></tr></table>
		</form>
		</div>
		</div>
	
</body>
</html>