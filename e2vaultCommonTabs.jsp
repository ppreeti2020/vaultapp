<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script type="text/javascript" src="tabber.js"></script>
<script type="text/javascript">
function archivalmouseOver()
{
document.getElementById("archival").src ="images/archival1.jpg";
}
function archivalmouseOut()
{
document.getElementById("archival").src ="images/archival.jpg";
}
function rendermouseOver()
{
document.getElementById("render").src ="images/renderingserver1.jpg";
}
function rendermouseOut()
{
document.getElementById("render").src ="images/renderingserver.jpg";
}
function dataroutermouseOver()
{
document.getElementById("datarouter").src ="images/datarouterconfig1.jpg";
}
function dataroutermouseOut()
{
document.getElementById("datarouter").src ="images/datarouterconfig.jpg";
}


</script>
<link rel="stylesheet" href="styles/example.css" TYPE="text/css" MEDIA="screen">
<link rel="stylesheet" href="styles/example-print.css" TYPE="text/css" MEDIA="print">
</head>
<body>
<table align="left">
<tr align="left">
	 	<td> <a href="VaultAction.action?pg=archival"><img id="archival" src="images/archival.jpg" onmouseover="archivalmouseOver()" onmouseout="archivalmouseOut()"/></a></td>
	  	<td> <a href="VaultAction.action?pg=render"><img id="render" src="images/renderingserver.jpg" onmouseover="rendermouseOver()" onmouseout="rendermouseOut()"/></a></td>
		<td> <a href="VaultAction.action?pg=router"><img id="datarouter" src="images/datarouterconfig.jpg" onmouseover="dataroutermouseOver()" onmouseout="dataroutermouseOut()"/></a></td>
	
	</tr>
	</table>
</body>
</html>