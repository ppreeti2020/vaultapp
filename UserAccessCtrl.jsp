<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table width="100%">
	
	<tr><td>
	<s:form action="AdminSubmitAction">
	<table class="dataTable">
	
		<tr>
			<td>
				<b>User Groups</b>
			</td>
			<td>
				<b>&nbsp;</b>
			</td>
			<td>
				<b>Selected User Groups</b>
			</td>
			
			<td>
				<b>CSR User</b>
			</td>
		</tr>
		<tr><td>
			<%
		List<String> csrGroups = (ArrayList<String>)request.getAttribute("csrGroups");			
		Iterator<String> itr = csrGroups.iterator();
		%>
				<select name="csrGrps" size="6" multiple="multiple"> 
				<%
		while (itr.hasNext()) {
			String groupName = itr.next();
	%>
			
			
			 <option value="<%= groupName %>"><%= groupName %></option>	
				
	<%} %>
			 </select>
			 </td>
			 <td><table><tr><td>
<input type="button" value=">>" onclick="moveoutid()"></td>
<td>
<input type="button" value="<<" onclick="moveinid()">
</td></tr>
</table>
</td>
			 <td>
					
	
		
	<select name="selcsrGrps" size="6" multiple="multiple" > 
		
	
				</select>
				</td>
				
			 <td>
					
	<%
		List csrRoles = request.getAttribute("csrRoles")!= null?(ArrayList)request.getAttribute("csrRoles"):new ArrayList();			
		Iterator itrer = csrRoles.iterator();
		%>
		
	<div id="div1" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		<%
		while (itrer.hasNext()) {
			DataDAO cDao = (DataDAO)itrer.next();
	%>
			
			
			 <tr><td><input type="checkbox" name="roleId" value="<%= cDao.getRoleId() %>"><%= cDao.getRoleName() %></input>	
				</td>
			</tr>
	<%} %>
	
				</table></div>
				</td>
			</tr>
			<tr>
			 <td>
					
	
		
	<div id="div2" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		
			
			
			 <tr><td><input type="checkbox" name="roleId" value="active">Active</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="roleId" value="active">Inactive</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="roleId" value="active">Disabled</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="roleId" value="active">Auto Disabled</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="roleId" value="active">Mark for Deletion</input>	
				
	</td></tr>
				</table></div>
				</td><td>&nbsp;</td>
				<td>
					
	
		
	<div id="div3" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		
			
			
			 <tr><td><input type="checkbox" name="roleId" value="active" >Validity Dates From</input>
			
				
	</td></tr>
	<tr><td><input type="checkbox" name="roleId" value="active">Validity Times From</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="roleId" value="active">Validity Days From</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="roleId" value="active">Current Year</input>	
				
	</td></tr>
	
				</table></div>
				</td>
				<td>
					
	
		
	<div id="div4" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		
			
			
			 <tr><td><input type="checkbox" name="roleId" value="active">Access Time From</input>	
				
	</td></tr>
	
	<tr><td><input type="checkbox" name="roleId" value="active">Access Days From</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="roleId" value="active">Public Holidays</input>	
				
	</td></tr>
	<tr><td><input type="checkbox" name="roleId" value="active">Weekends</input>	
				
	</td></tr>
				</table></div>
				</td>
			</tr>

	<tr>
		<td><input type="Submit" value="Apply"></td>
	</tr>	
	</table>
	
	</s:form>
		</td></tr></table>
</body>
</html>