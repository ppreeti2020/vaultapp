<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dto.DataDTO"%>
<%@page import="com.pb.common.ClientSideEncryptDecrypt"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="calendar/d3.js"></script>
<script type="text/javascript" src="calendar/d3.csv.js"></script>
<script type="text/javascript" src="calendar/d3.time.js"></script>
<link type="text/css" rel="stylesheet" href="calendar/colorbrewer.css" />
<link type="text/css" rel="stylesheet" href="calendar/calendar.css" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/common/messages.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet"
	type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css"
	type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css"
	media="all" />

<title>Insert title here</title>

<s:include value="adminHeader.jsp" />
<%
	String searchReport = request.getAttribute("searchReport") != null ? request.getAttribute("searchReport").toString() : "";
	String linkType = request.getAttribute("linkType") != null ? request.getAttribute("linkType").toString() : "";
	int i = 1;
	String logCount = request.getAttribute("logCount") != null ? request.getAttribute("logCount").toString() : "";
%>
<style>
.AccordionTitle,.AccordionContent,.AccordionContainer {
	position: relative;
}

.AccordionTitle {
	height: 20px;
	overflow: hidden;
	cursor: pointer;
	font-family: Arial;
	font-size: 8pt;
	font-weight: bold;
	vertical-align: top;
	text-align: center;
	background-repeat: repeat-x;
	display: table-cell;
	-moz-user-select: none;
}

.AccordionContent {
	height: 0px;
	overflow: auto;
	display: none;
}

.AccordionContainer {
	border-top: solid 1px #C1C1C1;
	border-bottom: solid 1px #C1C1C1;
	border-left: solid 2px #C1C1C1;
	border-right: solid 2px #C1C1C1;
}
</style>
<script language="javascript" type="text/javascript">

 function dropdownlist()
{
	
 var x = document.getElementById("searchReport");
 
 var len = x.length;

 for(var i=0;i<len;i++)
	 {
	 
	 if(x[i].value == '<%=searchReport%>')
		 {
	 x[i].selected = true;
	 x[i].value = '<%=searchReport%>';
		 }
	 }

}
var TimeToSlide = 250.0;
var ContentHeight = 100;
var openAccordion = '';
	function Ajax(id,stdin,stdout) {
  var xmlHttpReq = false;
  var self1 = this;
  // Mozilla/Safari
  var url = "AdminSearchAction.action?searchReport=search&serId="+id;
 // alert(url);
  if (window.XMLHttpRequest) {
    xmlHttpReq = new XMLHttpRequest();
  }
  // IE
  else if (window.ActiveXObject) {
    xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
  }
  if (!xmlHttpReq) {
    alert('ERROR AJAX:( Cannot create an XMLHTTP instance');
    return false;
  }   
  xmlHttpReq.open('POST', url, true);
  //xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xmlHttpReq.onreadystatechange = function() {
	  var table = document.getElementById("searchList");
		/* if(table != null)
			  {
    var rows = table.getElementsByTagName("tr");
    for (i = 1; i < rows.length; i++) {
		document.getElementById("AccordionContent"+i).style.display = 'none';
	}
	document.getElementById("AccordionContent"+stdout).style.display = 'block';
	
	document.getElementById("AccordionContent"+stdout).innerHTML = "loading..";*/
    if (xmlHttpReq.readyState == 4 )
	  {
		//xmlHttpReq.responseText
		if(xmlHttpReq.status == 200)
		  {
			//alert("hi");
			var list = xmlHttpReq.responseXML.getElementsByTagName("SearchList")[0].childNodes[0].nodeValue;
		 
	document.getElementById("AccordionContent"+stdout).style.display = 'block'
		document.getElementById("AccordionContent"+stdout).innerHTML=list;
		
	


var nID = "AccordionContent"+stdout ;
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
		  
	  }
	  }
  }
  xmlHttpReq.send(stdin);
}
function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate(" + curTick + "," + timeLeft + ",'" 
      + closingId + "','" + openingId + "')", 33);
}
function getResources(logFileId, desc)
{
	var x = document.getElementById("searchReport");
 
 var len = x.length;

 for(var i=0;i<len;i++)
	 {

	 
	 if(x[i].value == "logcheck")
		 {
	alert(x[i].value);
	 x[i].selected = true;
	x[i].value = "logcheck";
		 }
	 }

	document.getElementById("searchReport").value = "logcheck";

	document.getElementById("linkType").value = desc;
	document.getElementById("reportAction").action="AdminSearchAction.action";
	document.getElementById("reportAction").submit();
			return true;
}
function getResources(logFileId, desc)
{
	

	
document.getElementById("searchReport").value = "logcheck";
	document.getElementById("linkType").value = desc;
	document.getElementById("reportAction").action="AdminSearchAction.action?logFileId="+logFileId;
	document.getElementById("reportAction").submit();
			return true;
}
function submitReport(button)
{
	
	
		document.getElementById("reportAction").action="AdminSearchAction.action";
		
            document.getElementById("reportAction").submit();
			return true;
}
</script>

</head>


<body onLoad="dropdownlist();">
	<jsp:include page="CommonTabs.jsp" />

	<div id="countrydivcontainer"
		style="border: 1px solid gray; width: 1550px; height: 610px; margin-bottom: 1em; padding: 10px; overflow: auto">

		<form method="post" name="reportAction" id="reportAction"
			action="AdminSearchAction.action">

			<table width="100%" style="border-width: 1px 1px 1px 1px; border-style: solid;"	height="500">
				<tr>
					<td valign="top">
						<table width="100%" valign="top">
							<tr>
								<td valign="top">
									<table valign="top">
										<tr>
											<td><b>Reports</b></td>

											<td align="center"><SELECT NAME="searchReport"
												id="searchReport" style="width: 350px;">
													<OPTION SELECTED="SELECTED" VALUE=" ">Select
														Report!</OPTION>
													<OPTION VALUE="login">Login Report</OPTION>
													<OPTION VALUE="unsuccess">UnSuccessful Login
														Report</OPTION>
													<OPTION VALUE="logout">Logout Report</OPTION>
													<OPTION VALUE="doc">Document Report</OPTION>
													<OPTION VALUE="freq">Frequently Searched Account
														Report</OPTION>
													<OPTION VALUE="useraccess">User Previliges Report</OPTION>
													<!-- <OPTION VALUE="search">Search Report</OPTION> 
													<OPTION VALUE="loginintegcheck">Login Table
														Integrity Check Report</OPTION>
													<OPTION VALUE="logoutintegcheck">Logout Table
														Integrity Check Report</OPTION>
													<OPTION VALUE="searchdetintegcheck">Search Details
														Integrity Check Report</OPTION>
													<OPTION VALUE="searchresintegcheck">Search Results
														Integrity Check Report</OPTION>
													<OPTION VALUE="accountIntegCheck">Account Details
														Integrity Check Report</OPTION>
													<OPTION VALUE="docDetIntegCheck">Document Details
														Integrity Check Report</OPTION>
													<OPTION VALUE="docActIntegCheck">Document Action
														Details Integrity Check Report</OPTION>
													<OPTION VALUE="docRendIntegCheck">Document Render
														Details Integrity Check Report</OPTION>
													<OPTION VALUE="userMgmtIntegCheck">User Access
														Details Integrity Check Report</OPTION>
													<OPTION VALUE="userRoleIntegCheck">User Role
														Details Integrity Check Report</OPTION>
													<OPTION VALUE="tablesIntegCheck">All Tables
														Integrity Check Report</OPTION>
													<OPTION VALUE="fileCompression">Archived File
														Statistics Report</OPTION>-->
													<!--<OPTION VALUE="logcheck">Log File Statistics
														Report</OPTION>-->
											</SELECT> &nbsp;&nbsp;<input type="submit" name="search_criteria"
												value="Search" class="actionButton2"
												onclick="return submitReport(this);" /></td>
										</tr>

									</table>
								</td>
							</tr>
							<tr>
								<td><input type="hidden" name="linkType" id="linkType" />
								</td>
							</tr>
							<tr>
								<td>
									<%
										ClientSideEncryptDecrypt data = new ClientSideEncryptDecrypt();
										if (searchReport != null) {

											DataDTO cd = new DataDTO();
											DataDTO cd1 = new DataDTO();
											int num = 1;
											if (searchReport.equals("login")) {
									%>
									<table width="100%">
										<tr>
											<td><b>Login Report</b></td>
										</tr>
										<tr>
										<td>
											<display:table class="dataTable" id="loginList" name="populateLoginList"
												 pagesize="10"
												requestURI="/AdminSearchAction.action">
												<display:setProperty name="paging.banner.onepage" value=" " />
												<display:setProperty name="paging.banner.placement" value="left" />
												<display:column property="loginGroup" sortable="true"
													title="Group Name">
													<%
														cd = ((DataDTO) pageContext
																				.getAttribute("loginList"));
													%>
												</display:column>
												<display:column property="loginName" sortable="true"
													title="User Name">
												</display:column>
												<display:column property="loginDate" sortable="true"
													title="Login Date">

												</display:column>
												<display:column property="IP4" sortable="true"
													title="IPAddress">

												</display:column>
												<display:column property="browser" sortable="true"
													title="Browser">

												</display:column>
											</display:table>
											</td></tr></table>
											<%
												}
													if (searchReport.equals("unsuccess")) {
											%>
											<table width="100%">
												<tr>
													<td><b>Unsuccessful Login Details:<b>
													</td>
												</tr>
												<tr>
													<td><display:table class="dataTable" id="loginList"
															name="populateUnsuccessLoginList" pagesize="10"
															requestURI="/AdminSearchAction.action">
															<display:setProperty name="paging.banner.onepage"
																value=" " />
															<display:column property="loginName" sortable="true"
																title="User Name">
															</display:column>
															<display:column property="loginDate" sortable="true"
																title="Login Date">

															</display:column>
															<display:column property="IP4" sortable="true"
																title="IP Address">

															</display:column>
															<display:column property="browser" sortable="true"
																title="Browser">

															</display:column>
														</display:table>
														</td></tr></table> <%
 	}
 		if (searchReport.equals("search")) {
 %>
														<table width="100%">
															<tr>
																<td><b>User Search Report</b></td>
															</tr>
															<tr>
																<td><display:table class="dataTable" name="populateSearchList"
																		 id="searchList" pagesize="10"
																		requestURI="/AdminSearchAction.action">
																		<display:setProperty name="paging.banner.onepage"
																			value=" " />
																		<display:column sortable="true" title="Group Name">
																			<%
																				cd = ((DataDTO) pageContext
																										.getAttribute("searchList"));
																			%>
																			<%=cd.getLoginGroup()%></display:column>
																		<display:column sortable="true" title="User Name"><%=cd.getLoginName()%>
																		</display:column>
																		<display:column sortable="true" title="Search"><%=cd.getSearchValue()%>
																		</display:column>
																		<display:column sortable="true" title="No.of Results"><%=cd.getResCount()%></display:column>
																		<display:column sortable="true" style="width:300px"
																			title="Search Results">
																			<div
																				onclick="Ajax('<%=cd.getSerId()%>','',<%=num%>);"
																				class="AccordionTitle" onselectstart="return false;">
																				Select</div>
																			<div id="AccordionContent<%=num%>"
																				class="AccordionContent"></div>
																			<%
																				num++;
																			%>

																		</display:column>

																		<display:column sortable="true" title="Search Date"><%=cd.getSearchDate()%></display:column>
																	</display:table>
																	</td></tr></table> <%
 	}
 		if (searchReport.equals("logout")) {
 %>
																	<table width="100%">
																		<tr>
																			<td><b>User Logout Report</b></td>
																		</tr>
																		<tr>
																			<td><display:table name="populateLogoutList" class="dataTable"
																					 id="logoutList" pagesize="10"
																					requestURI="/AdminSearchAction.action" >
																					<display:setProperty name="paging.banner.onepage" value=" " />
																					<display:column property="loginGroup"
																						sortable="true" title="Group Name">
																					</display:column>
																					<display:column property="loginName"
																						sortable="true" title="User Name">
																					</display:column>

																					<display:column property="logout" sortable="true"
																						title="Logout">

																					</display:column>
																					<display:column property="logoutDate"
																						sortable="true" title="Logout Date">


																					</display:column>
																				</display:table>
																				</td></tr></table> <%
 	}
 		if (searchReport.equals("doc")) {
 %>
																				<table width="100%">
																					<tr>
																						<td><b>User Document Report</b></td>
																					</tr>
																					<tr>
																						<td><display:table class="dataTable" name="populateDocList"
																								 id="docList" pagesize="10"
																								requestURI="/AdminSearchAction.action">
																								<display:setProperty
																									name="paging.banner.onepage" value=" " />
																								<display:column property="loginName"
																									sortable="true" title="User Name">
																								</display:column>
																								<display:column property="dbName"
																									sortable="true" title="Database Name"></display:column>
																								<display:column property="nricNo"
																									sortable="true" title="Account Number"></display:column>
																								<display:column property="docgenDate"
																									sortable="true" title="Document Date"></display:column>
																								<display:column property="contentType"
																									sortable="true" title="Document Type"></display:column>

																								<display:column property="docAccessDate"
																									sortable="true" title="Document Access Date"></display:column>

																								<display:column property="docAction"
																									sortable="true" title="Action"></display:column>

																							</display:table>
																							</td></tr></table> <%
 	}
 		if (searchReport.equals("freq")) {
 %>
																							<table width="100%">
																								<tr>
																									<td><b>Frequently viewed Account
																											Report</b></td>
																								</tr>
																								<tr>
																									<td><display:table class="dataTable" name="populateFreqList"
																											 id="freqList" pagesize="10"
																											requestURI="/AdminSearchAction.action">
																											<display:setProperty
																												name="paging.banner.onepage" value=" " />
																											<display:column property="nricNo"
																												sortable="true" title="Account Number">
																											</display:column>
																											<display:column property="count"
																												sortable="true" title="Frequently Viewed">

																											</display:column>
																										</display:table>
																										</td></tr></table> <%
 	}
 		if (searchReport.equals("useraccess")) {
 %>
																										<table width="100%">
																											<tr>
																												<td><b>User Previliges Report</b></td>
																											</tr>
																											<tr>
																												<td><display:table class="dataTable"
																														name="populateUserList" 
																														id="userList" pagesize="10"
																														requestURI="/AdminSearchAction.action">
																														<display:setProperty
																															name="paging.banner.onepage" value=" " />
																														<display:column property="userGrp"
																															sortable="true" title="User Group">
																														</display:column>
																														<display:column property="active"
																															sortable="true" title="Active">

																														</display:column>
																														<display:column property="roleName"
																															sortable="true" title="Roles">
																														</display:column>
																														<display:column property="dbName"
																															sortable="true" title="Product">

																														</display:column>
																														<display:column property="accessdateFrom"
																															sortable="true" title="Access Date From">

																														</display:column>
																														<display:column property="accessdateTo"
																															sortable="true" title="Access Date To">

																														</display:column>
																														<display:column property="accessfromDay"
																															sortable="true" title="Access From Days">

																														</display:column>
																														<display:column property="accesstoDay"
																															sortable="true" title="Access To Days">

																														</display:column>

																													</display:table>
																													</td></tr></table> <%
 	}
 		if (searchReport.equals("loginintegcheck")
 				|| searchReport.equals("logoutintegcheck")
 				|| searchReport.equals("searchdetintegcheck")
 				|| searchReport.equals("searchresintegcheck")
 				|| searchReport.equals("accountIntegCheck")
 				|| searchReport.equals("docDetIntegCheck")
 				|| searchReport.equals("docRendIntegCheck")
 				|| searchReport.equals("userMgmtIntegCheck")
 				|| searchReport.equals("userRoleIntegCheck")
 				|| searchReport.equals("userDBIntegCheck")
 				|| searchReport.equals("tablesIntegCheck")) {
 %>
																													<table width="100%">
																														<tr>
																															<td><b>Table Integrity Reports</b></td>
																														</tr>
																														<tr>
																															<td><display:table class="dataTable"
																																	name="populateFreqList"
																																	 id="logList"
																																	pagesize="10"
																																	requestURI="/AdminSearchAction.action">
																																	<display:setProperty
																																		name="paging.banner.onepage" value=" " />
																																	<display:column property="tableName"
																																		sortable="true" title="Table Name">
																																	</display:column>
																																	<display:column property="newHashId"
																																		sortable="true" title="New Hash Value">
																																	</display:column>
																																	<display:column property="oldHashId"
																																		sortable="true" title="Old Hash Value">

																																	</display:column>
																																	<display:column
																																		property="hashCheckDate"
																																		sortable="true" title="Check Date">
																																	</display:column>
																																	<display:column
																																		property="hashCheckFlag"
																																		sortable="true" title="Validation">

																																	</display:column>

																																</display:table>
																																</td></tr></table> <%
 	}
 		if (searchReport.equals("fileCompression")) {
 %>
																																<table width="100%">
																																	<tr>
																																		<td><b>Archived Statistics
																																				Report</b></td>
																																	</tr>
																																	<tr>
																																		<td><display:table class="dataTable"
																																				name="populateFreqList"
																																				 id="userList"
																																				pagesize="10"
																																				requestURI="/AdminSearchAction.action">
																																				<display:setProperty
																																					name="paging.banner.onepage"
																																					value=" " />
																																				<display:column property="fileName"
																																					sortable="true" title="File Name">
																																				</display:column>
																																				<display:column property="fileDate"
																																					sortable="true" title="File Date">

																																				</display:column>
																																				<display:column property="profile"
																																					sortable="true"
																																					title="Profile Name">
																																				</display:column>
																																				<display:column
																																					property="unCompress"
																																					sortable="true"
																																					title="Uncompress Size">

																																				</display:column>
																																				<display:column property="compress"
																																					sortable="true"
																																					title="Compress Size">

																																				</display:column>
																																				<display:column property="pages"
																																					sortable="true" title="Pages">

																																				</display:column>
																																				<display:column property="ipAddr"
																																					sortable="true" title="IP Address">

																																				</display:column>
																																				<display:column property="hostName"
																																					sortable="true" title="Host Name">

																																				</display:column>
																																			</display:table>
																																			</td></tr></table> <%
 	}
 		if (searchReport.equals("logcheck")) {
 			//System.out.println("logccheck::"+request.getAttribute("populateFreqList"));
 %>

																																			<table width="100%">
																																				<tr>
																																					<td><b>Log File Statistics
																																							Report</b></td>
																																				</tr>
																																				<tr>
																																					<td><display:table class="dataTable"
																																							name="populateFreqList"
																																							 id="logList"
																																							pagesize="10"
																																							requestURI="/AdminSearchAction.action">
																																							<display:setProperty
																																								name="paging.banner.onepage"
																																								value=" " />
																																							<display:column
																																								property="logDate"
																																								sortable="true"
																																								title="Log Started Date">
																																								<%
																																									cd = ((DataDTO) pageContext.getAttribute("logList"));
																																								%>
																																							</display:column>
																																							<display:column
																																								property="logTime"
																																								sortable="true"
																																								title="Log Started Time">

																																							</display:column>
																																							<display:column
																																								property="journalFile"
																																								sortable="true"
																																								title="Journal File">

																																							</display:column>
																																							<display:column
																																								property="profile"
																																								sortable="true"
																																								title="Profile Name">
																																							</display:column>
																																							<display:column
																																								property="drdFile"
																																								sortable="true"
																																								title="DRD File Name">
																																							</display:column>
																																							<display:column
																																								property="drpFile"
																																								sortable="true"
																																								title="DRP File Name">
																																							</display:column>

																																							<display:column property="format"
																																								sortable="true" title="Format">

																																							</display:column>
																																							<display:column
																																								property="totalRead"
																																								sortable="true"
																																								title="Total Read">

																																							</display:column>
																																							<display:column
																																								property="totalWrite"
																																								sortable="true"
																																								title="Total Written">

																																							</display:column>
																																							<display:column
																																								property="finalRatio"
																																								sortable="true"
																																								title="Final Ratio">

																																							</display:column>
																																							<display:column property="method"
																																								sortable="true" title="Method">

																																							</display:column>

																																							<display:column
																																								property="documents"
																																								sortable="true"
																																								title="Documents">

																																							</display:column>
																																							<display:column
																																								property="documentPages"
																																								sortable="true"
																																								title="Document Pages">

																																							</display:column>
																																							<display:column
																																								property="ignoredPages"
																																								sortable="true"
																																								title="Ignored Pages">

																																							</display:column>
																																							<display:column sortable="true"
																																								title="Errors">
																																								<%
																																									String showErrors = "";
																																													if (cd.getErrorQccurs().equals("false")) {
																																														showErrors = "No Errors";
																																								%>
																																								<%=showErrors%>
																																								<%
																																									} else {
																																								%>
																																								<a
																																									href="javascript:getResources('<%=cd.getLogFileId()%>','errors');">
																																									Show Errors </a>
																																								<%
																																									}
																																								%>


																																							</display:column>
																																							<display:column sortable="true"
																																								title="Resources">
																																								<%
																																									if (cd.getResourceCount() == 0) {
																																								%>
																																								<%=cd.getResourceCount()%>
																																								<%
																																									} else {
																																								%>
																																								<a
																																									href="javascript:getResources('<%=cd.getLogFileId()%>','resources');">
																																									<%=cd.getResourceCount()%> </a>
																																								<%
																																									}
																																								%>

																																							</display:column>
																																							<display:column
																																								property="logEndDate"
																																								sortable="true"
																																								title="Log End Date">

																																							</display:column>
																																							<display:column
																																								property="logEndTime"
																																								sortable="true"
																																								title="Log End Time">

																																							</display:column>
																																						</display:table>
																																						</td></tr></table> <%
 	}
 		if (searchReport.equals("calLog")) {
 %>
																																						<table width="100%">
																																							<tr>
																																								<td></b>Frequently viewed
																																									Details</b></td>
																																							</tr>
																																							<tr>
																																								<td>
																																									<%
																																										String str = "Date" + "," + "Count" + ";";
																																												List populateLogList = request
																																														.getAttribute("populateLogList") != null ? (ArrayList) request
																																														.getAttribute("populateLogList") : new ArrayList();
																																												Iterator iter = populateLogList.iterator();
																																												String nric = "";
																																												while (iter.hasNext()) {
																																													DataDTO cDao = (DataDTO) iter.next();
																																													if (iter.hasNext()) {
																																														str += cDao.getLogDate() + "," + cDao.getCountLog()
																																																+ ";";
																																													} else {
																																														str += cDao.getLogDate() + "," + cDao.getCountLog();
																																													}

																																												}
																																												System.out.println("<<<>>>>>>" + str);
																																									%>

																																									<div id="chart"></div> <script
																																										type="text/javascript">
		// replaceAll('1,000,000', ',', '') 
		function replaceAll(txt, replace, with_this) {
  return txt.replace(new RegExp(replace, 'g'),with_this);
}
		//alert('<%=str%>');
		var m = [19, 20, 20, 19], // top right bottom left margin
    w = 960 - m[1] - m[3], // width
    h = 136 - m[0] - m[2], // height
    z = 17; // cell size
var text1 = '<%=str%>';
var text2 = replaceAll(text1, ";","\n");
var text = text2;
var day = d3.time.format("%w"),
    week = d3.time.format("%U"),
    percent = d3.format(".1%"),
    format = d3.time.format("%Y-%m-%d");

var color = d3.scale.quantize()
    .domain([-.05, .05])
    .range(d3.range(9));

var svg = d3.select("#chart").selectAll("svg")
    .data(d3.range(2010, 2012))
  .enter().append("svg:svg")
    .attr("width", w + m[1] + m[3])
    .attr("height", h + m[0] + m[2])
    .attr("class", "RdYlGn")
  .append("svg:g")
    .attr("transform", "translate(" + (m[3] + (w - z * 53) / 2) + "," + (m[0] + (h - z * 7) / 2) + ")");

svg.append("svg:text")
    .attr("transform", "translate(-6," + z * 3.5 + ")rotate(-90)")
    .attr("text-anchor", "middle")
    .text(String);

var rect = svg.selectAll("rect.day")
    .data(function(d) { return d3.time.days(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
  .enter().append("svg:rect")
    .attr("class", "day")
    .attr("width", z)
    .attr("height", z)
    .attr("x", function(d) { return week(d) * z; })
    .attr("y", function(d) { return day(d) * z; });

svg.selectAll("path.month")
    .data(function(d) { return d3.time.months(new Date(d, 0, 1), new Date(d + 1, 0, 1)); })
  .enter().append("svg:path")
    .attr("class", "month")
    .attr("d", monthPath);
	

d3.csv(text, function(csv) {
	alert(text);
  var data = d3.nest()
    .key(function(d) { return d.Date; })
    .rollup(function(d) { return (d[0].Count) ; })
    .map(csv);

  rect
      .attr("class", function(d) {return "day q" + color(data[format(d)]) + "-9"; })
    .append("svg:title")
      .text(function(d) { return format(d) + ": " + data[format(d)]; });
 
});

function monthPath(t0) {
  var t1 = new Date(t0.getUTCFullYear(), t0.getUTCMonth() + 1, 0),
      d0 = +day(t0), w0 = +week(t0),
      d1 = +day(t1), w1 = +week(t1);
  return "M" + (w0 + 1) * z + "," + d0 * z
      + "H" + w0 * z + "V" + 7 * z
      + "H" + w1 * z + "V" + (d1 + 1) * z
      + "H" + (w1 + 1) * z + "V" + 0
      + "H" + (w0 + 1) * z + "Z";
}

	</script>
																																								<%
 	}
 	}
 %> <%
 	if (searchReport.equals("logcheck") && linkType != null) {
 %>
																																					<table>
																																					<tr><td>
																																							<%
																																								System.out.println("coming here::" + linkType);
																																									List populateLogList = request.getAttribute("populateLogList") != null ? (ArrayList) request
																																											.getAttribute("populateLogList") : new ArrayList();
																																									System.out.println("populateLogList here::" + populateLogList);
																																									if (linkType.equalsIgnoreCase("resources")) {
																																										System.out.println("populateLogList here::"
																																												+ populateLogList);
																																							%>
																																							<tr>
																																								<td>Reource Name:</td>
																																								<td>Reource Size:</td>
																																								<td>Reource Date:</td>
																																								<td>Reource Time:</td>
																																							</tr>
																																							<%
																																								for (Iterator iter = populateLogList.iterator(); iter
																																												.hasNext();) {
																																											DataDTO dDao = (DataDTO) iter.next();
																																											System.out.println("wrote:::" + dDao.getResWrote());
																																											if (dDao.getResWrote() != null) {
																																							%>
																																							<tr>
																																								<td><%=dDao.getResWrote()%>
																																								</td>
																																								<td><%=dDao.getResSize()%>
																																								</td>
																																								<td><%=dDao.getResStartDate()%>
																																								</td>
																																								<td><%=dDao.getResStartTime()%></td>
																																							</tr>
																																							<%
																																								}
																																										}
																																									} else if (linkType.equalsIgnoreCase("errors")) {
																																							%>
																																							<tr>
																																								<td>Error Code:</td>
																																								<td>Error Description:</td>
																																							</tr>
																																							<%
																																								for (Iterator iter = populateLogList.iterator(); iter
																																												.hasNext();) {
																																											DataDTO dDao = (DataDTO) iter.next();
																																											if (dDao.getErrCode() != null) {
																																							%>
																																							<tr>
																																								<td><%=dDao.getErrCode()%></td>
																																								<td><%=dDao.getErrDesc()%></td>
																																							</tr>

																																							<%
																																								}
																																										}
																																									}
																																							%>
																																							 <%
 	}
 %>
																																					
																																		
																																</table></td></tr></table></td></tr></table></td></tr></table>
																																</form>
																																</div>
																																
</body>
</html>

