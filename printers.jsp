<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Select printer Preference</title>
<%@page import="com.pb.dao.DBConnectionDAO" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="java.util.Iterator"%>
</head>
<body>
<%DBConnectionDAO daocon = new DBConnectionDAO();
List<DataDAO> listPrinters = new ArrayList<DataDAO>();
PreparedStatement ps = null;
Connection conn = daocon.getDBConnection();
String query="SELECT NAME from PRINTER";
ps = conn.prepareStatement(query);
ResultSet rs = ps.executeQuery();
while (rs.next()) {
	DataDAO dao = new DataDAO();
	dao.setPrinterName(rs.getString(1).toString());
	listPrinters.add(dao);
}
request.setAttribute("listPrinters",listPrinters); %>

<tr><td align="left">
<B>Printer Name :</B><select name="listPrinters" id="listPrinters" style="width:125px;">

<%	Iterator listItername =  listPrinters.iterator();
if(listItername.hasNext())
{
while(listItername.hasNext())
{
	DataDAO nameDAO = (DataDAO) listItername.next();%>
	<option value="<%=nameDAO.getPrinterName()%>"><%=nameDAO.getPrinterName() %></option>
<%}
}%>
</select>
</td>
</tr>

<a href="#" onclick="self.close();return false;">Print Document</a> 
</body>
</html>