<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.*"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="java.text.*"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="com.pb.evault.DocumentHandle"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
	<s:include value="adminHeader.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Document Management Results</title>
<%
String host = (String)request.getAttribute("host");
Map<Long,DataDAO> profMapList = request.getAttribute("profMapList")!=null?(HashMap<Long,DataDAO>)request.getAttribute("profMapList"):new HashMap<Long,DataDAO>();
String message = request.getAttribute("message")!= null?(String)request.getAttribute("message"):"";
Long prdbID =  request.getAttribute("prdbID")!=null?Long.parseLong(request.getAttribute("prdbID").toString()):0L;
String prName =  request.getAttribute("prName")!=null?(String)request.getAttribute("prName"):"";

List indexList = request.getAttribute("indexList")!=null?(ArrayList)request.getAttribute("indexList"):new ArrayList();
Map<String,String> textValues = request.getAttribute("textValues")!=null?(HashMap<String,String>)request.getAttribute("textValues"):new HashMap<String,String>();

ArrayList<String> resultIdxList = request.getAttribute("resultIdxList")!=null?(ArrayList<String>)request.getAttribute("resultIdxList"):new ArrayList<String>();
Map<Integer, Map<String,String>> docList = request.getAttribute("docList")!=null?(HashMap<Integer, Map<String,String>>)request.getAttribute("docList"):new HashMap<Integer, Map<String,String>>();
String accountNumber = request.getAttribute("accountNumber")!=null?(String)request.getAttribute("accountNumber"):"";
String databaseName = request.getAttribute("databaseName")!=null?(String)request.getAttribute("databaseName"):"";
DocumentHandle docHandle = request.getAttribute("docHandle")!=null?(DocumentHandle)request.getAttribute("docHandle"):new DocumentHandle();
String actType = request.getAttribute("actType")!= null?(String)request.getAttribute("actType"):"";
int pageSize = request.getAttribute("pageSize")!=null?Integer.parseInt(request.getAttribute("pageSize").toString()):10;
if(pageSize == 0 && docList.size() != 0  )
{
	if( docList.size() < 10)
		pageSize = docList.size();
	else
pageSize = 10;
}
%>
<script>
function ValidateForm(){
	var pr = 0;
	var sec = 0;
	var aInputs = document.getElementsByTagName('input');
	for (var i=0;i<aInputs.length;i++) {
		var id = aInputs[i].id;
		if (id.match("Pri")){
			if(aInputs[i].value.length==0){
				pr = 0;
				var prIdx = id.split('_');
				alert("'"+prIdx[3]+"' cannot not be empty!!");
				return false;
			}
			else
				pr = 1;
		}
		else if(id.match("Sec")){
			if(aInputs[i].value.length!=0){
				sec++;				
			}
		}
	}
	if(pr == 1 && sec == 0){
		alert("Please enter any other search value for cross reference!!");
		return false;
	}
	document.getElementById("Display").action="DocManagerGeneral.action";
	
    document.getElementById("Display").submit();
	return true;
}

</script>
</head>
<!-- <body> -->

<body topmargin=16 marginheight=16 >
	<table width="100" height="500">
		<tr><td>
			<s:include value="userrepCommonTabs.jsp" />
		<td></tr>
		<tr><td>
			<table align="center"  width="100%" height="500">
				<tr><td valign="top">
					<table width="100%">
					<tr><td valign="top">
						<table width="100%" border="1" height="500">
						<tr><td id="table1" width="15%"  valign="top">
							<table>
								<tr><td>
									<ul id="repository" class="profMap">
										<% List<Long> dbid = new ArrayList<Long>();
										   for(Long profId: profMapList.keySet())
										   { DataDAO ddao = (DataDAO)profMapList.get(profId);
										     if(!dbid.contains(ddao.getPdbID()))
										     {
										    	dbid.add(ddao.getPdbID());
						 	            %>	   <li id=<%= ddao.getPdbName()%>><span><a ><%= ddao.getPdbName()%></a></span>
						 	            <%	  	Long tempdbId = ddao.getPdbID();%>
						 	            	 	<ul>
												<% for(Long profId1: profMapList.keySet())
										   		   { DataDAO ddao1 = (DataDAO)profMapList.get(profId1);
										   		     if(ddao1.getPdbID() == tempdbId){
										   		%>	   <li id=<%= ddao1.getDbProfileName()%>><span><a href='DocManagerGeneral.action?prdbID=<%=ddao1.getPdbID()%>&prName=<%= ddao1.getDbProfileName()%>'><%= ddao1.getDbProfileName()%></a></span></li>
												<% 	  }
										   		   	}
										   	     %>
												</ul>
										    <% }%>
											</li>
										<% }%>
									</ul>
								</td></tr>
							</table>
							</td>
							<td width="80%"  valign="top">
							
							<table border="0" align="center" width="100%">	
								<tr><td align="left" width="100%">
									<form id="Display" name="Display" method="post">
									<%if(accountNumber.equals("")){ %>
										<table width="100%">
										 <tr><td align="left">
												<div id="mes"><font color="red">
													<%if(message != null && !message.equals(""))
												  	{%>
														<%=message%>
													<%}
												 	 else if(session.getAttribute("message") != null)
												  	{%>
														<%= session.getAttribute("message")%>
														<%session.setAttribute("message",null);
												  	}%>
												  	</font>
												</div>
											 </td>
											 <td align="left">
												<input type="hidden" name="prdbID" id="prdbID" value="<%=prdbID%>"/>
												<input type="hidden" name="prName" id="prName" value="<%=prName%>"/>
											 </td>
										</tr>
										<tr><td>
										<table><tr>
										<%if(indexList.size() !=0)
				  						{
		    								for(Iterator iter=indexList.iterator();iter.hasNext();)
											{
												UserDTO dto = (UserDTO) iter.next();
												System.out.println("dto"+dto.getIndexName());%>
												<td><%=dto.getIndexDesc()%>:</td>
												<td><%String in = "textSearchs_"+dto.getIndexName()+"_"+dto.getDbId();
							  						System.out.println("textValues.keySet()"+textValues.keySet());
							  						if(textValues.keySet().size() != 0)
							  						{	int co = 0;
														for(String str:textValues.keySet())
														{ System.out.println("textValues.keySet()"+textValues.keySet());
														  if(in.equals(str))
														  {	co = 1;	%>
															<input type="text" name="textSearchs_<%=dto.getIndexId()%>_<%=dto.getDbId()%>" id="textSearchs_<%=dto.getIndexId()%>_<%=dto.getDbId()%>" value="<%=textValues.get(str)%>" />
								  						<%}
														}
														if(co == 0)
														{%>
															<input type="text" name="textSearchs_<%=dto.getIndexId()%>_<%=dto.getDbId()%>" id="textSearchs_<%=dto.getIndexId()%>_<%=dto.getDbId()%>" value="" />
				              							<%}
													}else
													{%>
														<input type="text" name="textSearchs_<%=dto.getIndexId()%>_<%=dto.getDbId()%>" id="textSearchs_<%=dto.getIndexId()%>_<%=dto.getDbId()%>" value="" />
						  							<%}%>
					  							 </td>
											<%}%>
		    								<td><input type="Submit" value="search" onclick="return ValidateForm()"/></td></tr>
		    							<%}%>
										   
										</table>
										</td></tr>
										<tr><td  align="left">
									<table width=100%><tr><td>
										<table><tr>
										<% if(docList.size() != 0)
									  	 { System.out.println("Inside docList"+docList.size());
											 int i =1; Map<String,String> idxvals; String docRefNo ="";
											 %>
											<td align="right"><%=pageSize%> records of <%= docList.size()%></td>
											</tr></table></td>
											<tr><td>
											<display:table name="docList" class="dataTable" id="dispID" export="true" pagesize="<%=pageSize%>" requestURI="/DocManagerGeneral.action">
												<display:setProperty name="export.excel.filename" value="SearchDetails.xls"/>
            									<display:setProperty name="export.pdf.filename" value="SearchDetails.pdf"/>
			 									<display:setProperty name="export.xml.filename" value="SearchDetails.xml"/>
			  									<display:setProperty name="export.csv.filename" value="SearchDetails.csv"/>
              									<display:setProperty name="export.pdf" value="true" />
												<display:column sortable="true" title="ID"><%=i++%></display:column>
												<% idxvals = (Map<String, String>)pageContext.getAttribute("dispID"); 
												    for(String col:idxvals.keySet())
													{ if(col.equalsIgnoreCase("Document Reference Number")){
														docRefNo = idxvals.get(col);
													  }%>
													  <display:column sortable="true" title='<%=col%>'> <%=idxvals.get(col)%> </display:column>
												  <%}%>
												 	 <display:column sortable="true" title="Action">
												  	<a href="DocManagerGeneral.action?docRefNo=<%=docRefNo%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=view">view</a>||<a href="DocManagerGeneral.action?docRefNo=<%=docRefNo%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=download">download</a>||<a href="DocManagerGeneral.action?docRefNo=<%=docRefNo%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=print">print</a>||<a href="DocManagerGeneral.action?docRefNo=<%=docRefNo%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=reprint">reprint</a>
												  	</display:column>
											</display:table></td></tr>
									  	<%}else{ %>
											<tr><td>No Records Found </td></tr>
											<%}%>
											</table>
										<%	}else if(!accountNumber.equals("") && !actType.equals("download"))
											{
												System.out.println("acct:"+accountNumber);
												System.out.println("db:"+databaseName);
												System.out.println("doc:"+docHandle.toString());
												String url = "Interface";%>
			 
												<jsp:include page="<%= url %>" flush="true" >
													<jsp:param name="acct" value="<%= accountNumber %>" />
													<jsp:param name="db" value="<%= databaseName %>" />
													<jsp:param name="doc" value="<%= docHandle.toString() %>" />
													<jsp:param name="op" value="view" />
												</jsp:include>
											</td></tr></table>
										<%} %>
										</form></td>
							</table>
							</td>
						</tr>
						</table>
					</td></tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	</table>
<s:include value="footer.jsp" />
</body>
</html>