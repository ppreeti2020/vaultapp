<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script type="text/javascript" src="tabber.js"></script>
<link rel="stylesheet" href="styles/example.css" TYPE="text/css" MEDIA="screen">
<link rel="stylesheet" href="styles/example-print.css" TYPE="text/css" MEDIA="print">
<script type="text/javascript">
function logmouseOver()
{
document.getElementById("log").src ="images/Log1.jpg";
}
function logmouseOut()
{
document.getElementById("log").src ="images/Log.jpg";
}
function resmouseOver()
{
document.getElementById("res").src ="images/resources1.jpg";
}
function resmouseOut()
{
document.getElementById("res").src ="images/resources.jpg";
}
</script>
<body>
<table align="left">
<% String tabId = request.getAttribute("tabId")!=null?request.getAttribute("tabId").toString():"";
String reports = session.getAttribute("reports")!=null?session.getAttribute("reports").toString():"";
String useraccesscontrol = session.getAttribute("useraccesscontrol")!=null?session.getAttribute("useraccesscontrol").toString():"";

String access = session.getAttribute("access")!=null?session.getAttribute("access").toString():"";

System.out.println("access"+access);
%>
<tr align="left">
	  <td> <a href="AuditAction.action"><img id="log" src="images/Log.jpg" onmouseover="logmouseOver()" onmouseout="logmouseOut()"/></a></td>
	 
	 <td> <a href="ResourceReportAction.action"><img id="res" src="images/resources.jpg" onmouseover="resmouseOver()" onmouseout="resmouseOut()"/></a></td>
	
	</tr>
	</table>
</body>
</html>