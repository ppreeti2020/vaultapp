<%@page import="java.util.ArrayList"%>
<%@page import="com.g1.e2.vault.VaultClient.Account"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
	 <s:include value="adminHeader.jsp" />
	 
	<html>
	<head>
	<!-- calendar stylesheet -->
  <link rel="stylesheet" type="text/css" media="all" href="styles/calendar-win2k-cold-1.css" title="win2k-cold-1" />

  <!-- main calendar program -->
  <script type="text/javascript" src="js/calendar.js"></script>

  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar-setup.js"></script>
	<%
	String textSearch = request.getAttribute("textSearch")!=null?request.getAttribute("textSearch").toString():"";
	Map<String,String> textValues = request.getAttribute("textValues")!=null?(HashMap<String,String>)request.getAttribute("textValues"):new HashMap<String,String>();
	//System.out.println(textValues.size());
	
	String message = request.getAttribute("message")!=null?request.getAttribute("message").toString():"";
	List indexList = request.getAttribute("indexList")!=null?(ArrayList)request.getAttribute("indexList"):new ArrayList();
	
	System.out.println("indexList"+indexList);
		int i=1;
	
		List csrDatabase = request.getAttribute("csrDatabase")!=null?(ArrayList)request.getAttribute("csrDatabase"):new ArrayList();
		System.out.println("csrDatabase:::"+csrDatabase);
		Long dbId = request.getAttribute("dbId")!=null?Long.parseLong(request.getAttribute("dbId").toString()):0L;
		System.out.println("dbId"+dbId);
		boolean showDate = request.getAttribute("showDate") != null? (Boolean)request.getAttribute("showDate"):false;
		String fromDate = request.getAttribute("fromDate") != null? (String)request.getAttribute("fromDate"):"";
		
		String toDate = request.getAttribute("toDate") != null? (String)request.getAttribute("toDate"):"";
%>
	<script language="javascript" type="text/javascript">
		var illegalChars= /[\(\)\<\>\,\;\:\\\/\"\@\#\$\%\*\!\^\{\}\~\`\&\-\_\+\=\|\[\]]/; 
		function fun()
		{ 
			var value=document.getElementById("textSearch").value;//.search("[A-Za-z0-9-_]");
 			if(value == "" || value == "")
			{
				document.getElementById("mesage").innerHTML="Please Enter Account Number";
				document.getElementById("textSearch").focus();
				return false;
			}
			else
			{
				if((illegalChars.test(value))) {
					document.getElementById("mesage").innerHTML="Please Enter Valid Account Number";
					document.getElementById("textSearch").value = "";
					document.getElementById("textSearch").focus();
					return false;
				}
				else{
					document.getElementById("searchAction").action="ResultAccount.action";
					document.getElementById("searchAction").submit();
				}
			}
	 	}
		
		function func()
		{
			var list = document.getElementById("dbId");
			var length = list.length;
			for (var i=0 ;i < length ; i++) {
				if(list[i].value == '<%=dbId%>') {
					list[i].selected = true;	
				}
			}
		}
		
		function selectDatabase(sel)
		{
			document.getElementById("searchAction").action="SearchAccount.action";
		    document.getElementById("searchAction").submit();
			return true;
		}
		
		function submitSearch(button)
		{
			var fromDate = document.getElementById("fromDate").value;
			var fromDateArr =fromDate.split('-');
			var fromDateObj = new Date(fromDateArr[2],fromDateArr[1]-1,fromDateArr[0]);
			var toDate = document.getElementById("toDate").value;
			var toDateArr =toDate.split('-');
			var toDateObj = new Date(toDateArr[2],toDateArr[1]-1,toDateArr[0]);
			
			if(fromDate.length != 0){
				if(toDate.length == 0){
					var today = new Date();
					toDate = today;
					alert("toDate::"+toDate);
				}
			}else if(toDate.length!=0){
				alert("Please enter from Date!!!");
				return false;
			}
			if(fromDate>toDate){
				alert("FromDate is after ToDate");
				return false;
			}
			var textsearchidx = 0;
			var aInputs = document.getElementsByTagName('input');
			for (var i=0;i<aInputs.length;i++) {
				var id = aInputs[i].id;
				if (id.match("textSearchs_")){
					if(aInputs[i].value.length!=0){
						textsearchidx++;
					}
				}
			}
			if (textsearchidx == 0 && fromDate.length == 0 && toDate.length ==0){
				alert("Please enter any one of the search indexes!!!");
				return false;
			}
											
			document.getElementById("searchAction").action="ResultAccount.action";
			document.getElementById("searchAction").submit();
			return true;
		}
		
</script>
</head>
 
<body onload="func();">
	<table width="100%">
	<tr><td>
		<s:include value="userrepCommonTabs.jsp" />
	<td></tr>
	<tr><td>
		<table width="100%" border="1px solid #778" height="500">
		<tr><td valign="top">
			<table valign="top"  width="100%">
			<tr><td valign="top">
				<form id="searchAction" name="searchAction" method="post"  >
				<table  valign="top">
				<tr><td><font color="red" id="mesage"><%=message%></font></td></tr>
				<tr>
				<% if(csrDatabase.size() != 0)
				   { if(csrDatabase.size() > 1)
				   	 {%>
					<td>
					<table><tr><td>Database:</td>
							   <td><select name="dbId" id="dbId" style="width:450px;" onchange="return selectDatabase(this);">
							   		<option value="">Select Database</option>
								<%
									for(Iterator iter=csrDatabase.iterator();iter.hasNext();)
									{
										UserDTO dto = (UserDTO) iter.next();
										System.out.println("dto.getDbId()"+dto.getDbId());
								%>
										<option value="<%=dto.getDbId()%>"><%=dto.getDbDesc()%></option>
								<%	}%>
									</select>
								</td>
								
						   </tr>
				   </table>
				   </td>
				   <%} %>
			   </tr>
			   <tr><td>
			   	   <table>
			   	   <tr>
			   	<%if(indexList.size() !=0)
				  {
		    		for(Iterator iter=indexList.iterator();iter.hasNext();)
					{
						UserDTO dto = (UserDTO) iter.next();
						System.out.println("dto"+dto.getIndexName());%>
						<td align="justify"><%=dto.getIndexDesc()%>:</td>
						<td><%String in = "textSearchs_"+dto.getIndexName()+"_"+dto.getDbId();
							  System.out.println("textValues.keySet()"+textValues.keySet());
							  if(textValues.keySet().size() != 0)
							  {
				             	int co = 0;
								for(String str:textValues.keySet())
								{
									System.out.println("textValues.keySet()>>>>"+textValues.keySet());
									if(in.equals(str))
									{
										co = 1;	%>
										<input type="text" name="textSearchs_<%=dto.getIndexName()%>_<%=dto.getDbId()%>" id="textSearchs_<%=dto.getIndexName()%>_<%=dto.getDbId()%>" value="<%=textValues.get(str)%>" />
								  <%}
								}
								if(co == 0)
								{%>
									<input type="text" name="textSearchs_<%=dto.getIndexName()%>_<%=dto.getDbId()%>" id="textSearchs_<%=dto.getIndexName()%>_<%=dto.getDbId()%>" value="" />
				              <%}
							}
							else
							{%>
								<input type="text" name="textSearchs_<%=dto.getIndexName()%>_<%=dto.getDbId()%>" id="textSearchs_<%=dto.getIndexName()%>_<%=dto.getDbId()%>" value="" />
						  <%}%>
					  </td>
				<%}
		    	}
				if(showDate)
				{%>
					<td>From Date:</td><td><input type="text" name="fromDate" id="fromDate" value="<%=fromDate%>"/></td>
					<td>To Date:</td><td><input type="text" name="toDate" id="toDate" value="<%= toDate %>"/></td>
					<td><input type="Submit" name="search_criteria" value="Search" class="actionButton2" onclick="return submitSearch(this);"/></td>
			   <%}%>
				</tr>
				</table>
				</td>
			</tr>
			</table>
			</form>
			</td>
		</tr>
		<tr><td>
			<display:table name="foundAccs" class="dataTable" id="Accs" pagesize="15" requestURI="/SearchAccount.action">
			<display:column sortable="true" title="ID"><%=(i++)%></display:column>
			<display:column property="accountNumber" sortable="true" title="Account Number"/>
			<display:column property="name" sortable="true" title="Customer Name"/>
		 	<display:column href="ListDocument.action?dbId=<%=dbId%>" paramId="acc" paramProperty="accountNumber" sortable="true" title="Action">Select</display:column>
			</display:table></td>
	  <%}
		else
		{%>
		<td align="center"><br><center><b>User has no previliges</b></center></td>
	  <%}%>
		</tr>
		</table>
		</td>
	</tr>
  </table>
	<script type="text/javascript">
  	if(document.getElementById("fromDate") != null)
	{
	 Calendar.setup({
        inputField     :    "fromDate",      // id of the input field
        ifFormat       :    "%d-%m-%Y",       // format of the input field
        showsTime      :    false,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	}
	if(document.getElementById("toDate") != null)
	{
	 Calendar.setup({
        inputField     :    "toDate",      // id of the input field
        ifFormat       :    "%d-%m-%Y",       // format of the input field
        showsTime      :    false,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	}
	</script>
	<s:include value="footer.jsp" />
</body>
</html>

