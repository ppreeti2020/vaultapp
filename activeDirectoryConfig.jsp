

<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    
<s:include value="adminHeader.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%
String ldapHost =  request.getAttribute("ldapHost")!= null? request.getAttribute("ldapHost").toString():"";
int ldapPort =  request.getAttribute("ldapPort")!= null? Integer.parseInt(request.getAttribute("ldapPort").toString()):0; 
String ldapNamingCtx =  request.getAttribute("ldapNamingCtx")!= null? request.getAttribute("ldapNamingCtx").toString():""; 
String userName =  request.getAttribute("userName")!= null? request.getAttribute("userName").toString():""; 
String password =  request.getAttribute("password")!= null? request.getAttribute("password").toString():""; 
String ldapUserDn =  request.getAttribute("ldapUserDn")!= null? request.getAttribute("ldapUserDn").toString():""; 
String ldapUserFilter =  request.getAttribute("ldapUserFilter")!= null? request.getAttribute("ldapUserFilter").toString():""; 
String ldapUserName =  request.getAttribute("ldapUserName")!= null? request.getAttribute("ldapUserName").toString():""; 
String ldapGrpFilter =  request.getAttribute("ldapGrpFilter")!= null? request.getAttribute("ldapGrpFilter").toString():""; 
String ldapGrpName =  request.getAttribute("ldapGrpName")!= null? request.getAttribute("ldapGrpName").toString():""; 
String ldapGrpMember =  request.getAttribute("ldapGrpMember")!= null? request.getAttribute("ldapGrpMember").toString():""; 
String sslTrustPath =  request.getAttribute("sslTrustPath")!= null? request.getAttribute("sslTrustPath").toString():""; 
String sslTrustPass =  request.getAttribute("sslTrustPass")!= null? request.getAttribute("sslTrustPass").toString():"";
String secureLdap =  request.getAttribute("secureLdap")!= null? request.getAttribute("secureLdap").toString():"";
%>
<script language="javascript" type="text/javascript">
function getSSL(secure)
{
var sec = secure.value;


if(sec == 'T')
	{
	document.getElementById("secureSettings").style.display="inline";
	}
	if(sec == 'F')
	{
	document.getElementById("secureSettings").style.display="none";
	}
}

function cancelForm(button)
{
	//action="ActiveDirectoryAction"
	//alert(document.pressed);
	
		
	document.getElementById("submitform").action = "ProfilesListAction.action";
	document.getElementById("submitform").submit;
	return true;
			
	}

function submitForm(button)
{
	//action="ActiveDirectoryAction"
	//alert(document.pressed);
	var rad = document.getElementsByName("secureLdap");
	
	var checked = false;
	var radVal = "";
	for (var i=0 ;i < rad.length ; i++) {
		if(rad[i].checked == true) {
				checked = true;	
				radVal = rad[i].value;
		}
	}	

	if(!checked) {
		alert("Please select Secure LDAP");
		return false;
	}
	if(checked)
	{
		if(radVal == "T")
		{
if(document.getElementById("sslTrustPath").value == "")
	{
	alert("Please Enter Ldap SSL Truststore Path");
	document.getElementById("sslTrustPath").focus();
	return false;
	}
	if(document.getElementById("sslTrustPass").value == "")
	{
	alert("Please Enter Ldap SSL Truststore Password");
	document.getElementById("sslTrustPass").focus();
	return false;
	}
		}
	}
	if(document.getElementById("ldapHost").value == "")
	{
	alert("Please Enter Ldap Host");
	document.getElementById("ldapHost").focus();
	return false;
	}
	if(document.getElementById("ldapPort").value == "")
	{
	alert("Please Enter Ldap Port");
	document.getElementById("ldapPort").focus();
	return false;
	}
	else
	{
		if(isNaN(document.getElementById("ldapPort").value)){
alert ("Please Enter Numeric Vaules for  Ldap Port ");
return false;
}
	}
	if(document.getElementById("ldapNamingCtx").value == "")
	{
	alert("Please Enter Ldap Naming Context");
	document.getElementById("ldapNamingCtx").focus();
	return false;
	}
	if(document.getElementById("userName").value == "")
	{
	alert("Please Enter User Name");
	document.getElementById("userName").focus();
	return false;
	}
	if(document.getElementById("password").value == "")
	{
	alert("Please Enter Password");
	document.getElementById("password").focus();
	return false;
	}
	if(document.getElementById("ldapUserDn").value == "")
	{
	alert("Please Enter Ldap User DN");
	document.getElementById("ldapUserDn").focus();
	return false;
	}
	if(document.getElementById("ldapUserFilter").value == "")
	{
	alert("Please Enter Ldap User Filter");
	document.getElementById("ldapUserFilter").focus();
	return false;
	}
	if(document.getElementById("ldapUserName").value == "")
	{
	alert("Please Enter Ldap User Name");
	document.getElementById("ldapUserName").focus();
	return false;
	}
	if(document.getElementById("ldapGrpFilter").value == "")
	{
	alert("Please Enter Ldap Group Filter");
	document.getElementById("ldapGrpFilter").focus();
	return false;
	}
	if(document.getElementById("ldapGrpName").value == "")
	{
	alert("Please Enter Ldap Group Name");
	document.getElementById("ldapGrpName").focus();
	return false;
	}
	if(document.getElementById("ldapGrpMember").value == "")
	{
	alert("Please Enter Ldap Group Member");
	document.getElementById("ldapGrpMember").focus();
	return false;
	}
	if(button.value != null || button.value != "")
			{
		if(button.value == "Test Connection")
			{
			document.getElementById("act").value = "testldap";
			//alert(document.getElementById("act").value);
			}
		if(button.value == "Save")
		{
		document.getElementById("act").value = "saveldap";
		//alert(document.getElementById("act").value);
		}
	document.getElementById("submitform").action = "ActiveSubmitAction.action";
	document.getElementById("submitform").submit;
	return true;
			}
	else
		{
		return false;
		}
	}
</script>
</head>
<body>

<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1230px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">


<jsp:include page="SystemCommonTabs.jsp" />

<div id="countrydivcontainer1" style="border:1px solid gray; width:1180px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">
<!-- Nithu Alexander: Added as part of "XSS" and AVA-1,AVA-2 fixes*/  -->
	<font color="#ff0000"><s:property value="message" /> </font>

<form id="submitform" name="submitform">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
<table width="100%" height="500" valign="top" align="center">
<tr>
<td valign="top" align="center">
<table width="100%" valign="top">

<tr >
<td align="left" colspan=2>
<b>LDAP Server Configuration:</b>
</td>
</tr>
<tr>
<td width="50%" align="right">
Secure LDAP:
</td>
<td width="50%"  align="left">
<table><tr><td align="justify"><input type="radio"  name="secureLdap" id="secureLdap" value="T" onclick="getSSL(this)"/>Yes
</td>
<td><input type="radio"  name="secureLdap" id="secureLdap" value="F" onclick="getSSL(this)"/>No
</td>
</tr></table>

</td>
</tr>
<tr><td colspan="2" align="center">
<div style="display:none;" id="secureSettings" >
<table>
<tr>
<td width="50%" align="right">
SSL Truststore Path:
</td>
<td width="50%"  align="left">
<input type="text" name="sslTrustPath" id="sslTrustPath" value="<%=sslTrustPath%>"/>
</td>
</tr>
<tr>
<td width="50%" align="right">
SSL Truststore Password:
</td>
<td width="50%"  align="left">
<input type="password" name="sslTrustPass" id="sslTrustPass" value="<%=sslTrustPass%>"/>
</td>
</tr>
</table>
</div>
</td></tr>
<tr>
<td width="50%" align="right">
LDAP Host:
</td>
<td width="50%"  align="left">
<input type="text" name="ldapHost" id="ldapHost" value="<%=ldapHost%>"/>
</td>
</tr>
<tr>
<td align="right">
LDAP Port:
</td>
<td align="left">
<input type="text" name="ldapPort" id="ldapPort" value="<%=ldapPort%>"/>

</td>
</tr>
<tr>
<td align="right">
LDAP Naming Context:
</td>

<td align="left">
<input type="text" name="ldapNamingCtx" id="ldapNamingCtx" value="<%=ldapNamingCtx%>"/>

</td>
</tr>
<tr>
<td align="right">
User Name:
</td>

<td align="left">
<input type="text" name="userName" id="userName" value="<%=userName%>"/>

</td>
</tr>
<tr><td align="right">
Password:
</td>

<td align="left">
<input type="password" name="password" id="password" value="<%=password%>"/>

</td>
</tr>
<tr>
<td align="right">
Ldap User DN:
</td>

<td align="left">
<input type="text" name="ldapUserDn" id="ldapUserDn" value="<%=ldapUserDn%>"/>

</td>
</tr>
<tr>
<td align="right">
LDAP User Filter:
</td>

<td align="left">
<input type="text" name="ldapUserFilter" id="ldapUserFilter" value="<%=ldapUserFilter%>"/>

</td>
</tr>
<tr>
<td align="right">
LDAP User Name Attribute:
</td>

<td align="left">
<input type="text" name="ldapUserName" id="ldapUserName" value="<%=ldapUserName%>"/>

</td>
</tr>
<tr>
<td align="right">
LDAP Group Filter:
</td>

<td align="left">
<input type="text" name="ldapGrpFilter" id="ldapGrpFilter" value="<%=ldapGrpFilter%>"/>

</td>
</tr>
<tr>
<td align="right">
LDAP Group Name Attribute:
</td>

<td align="left">
<input type="text" name="ldapGrpName" id="ldapGrpName" value="<%=ldapGrpName%>"/>

</td>
</tr>

<tr>
<td align="right">LDAP Group Member Attribute:</td>

<td align="left">
<input type="text" name="ldapGrpMember" id="ldapGrpMember" value="<%=ldapGrpMember%>"/>

</td>

</tr>
<tr>
<td align="center" colspan=2>
<input type="hidden" name="act" id="act"/>

</td>

</tr>
<tr>
<td align="center" colspan=2>
<!-- Nithu Alexander: Commented as part of "XSS" and AVA-1,AVA-2 fixes*/  -->
<!--<s:property value="message" />-->
</td>
</tr>
<tr>
<td align="center" colspan=2>

<input type="submit" value="Test Connection"   onclick="return submitForm(this);"/> <input type="submit" value="Save"    onclick="return submitForm(this);"/> <input type="submit" value="Cancel"    onclick="return cancelForm(this);"/>
</td>

</tr>
</table>
</td>
</tr>
<script>

	
var radi = document.getElementsByName("secureLdap");
	

	for (var i=0 ;i < radi.length ; i++) {
		if(radi[i].value == '<%=secureLdap%>') {
				radi[i].checked = true;	
		}
	}	
	if('<%=secureLdap%>' == "T")
		{
		document.getElementById("secureSettings").style.display="inline";
		}
	</script>
</table>
</form>
</div>
</div>

</body>
</html>