<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.*"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="java.text.*"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="com.pb.evault.DocumentHandle"%>
<%@page import="com.g1.e2.vault.OutputFormat"%>
<%@page import="com.g1.e2.vault.VaultClient.Document"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Document Management Results</title>

<%

String act=request.getParameter("actType");
 if(act!=null&& act.equalsIgnoreCase("view11"))
{
}else{
%>
<s:include value="adminHeader.jsp" />

<%
}
String host = (String)request.getAttribute("host");
System.out.println("host  at searchaccountjsp is"+host);
List<UserDTO> csrDatabase = request.getAttribute("csrDatabase")!=null?(ArrayList<UserDTO>)request.getAttribute("csrDatabase"):new ArrayList<UserDTO>();
System.out.println("Allowed Databases for the user::"+csrDatabase.size());

boolean showDate = request.getAttribute("showDate") != null? (Boolean)request.getAttribute("showDate"):false;
String fromDate = request.getAttribute("fromDate") != null? (String)request.getAttribute("fromDate"):"";
String toDate = request.getAttribute("toDate") != null? (String)request.getAttribute("toDate"):"";

String message = request.getAttribute("message")!= null?(String)request.getAttribute("message"):"";
String prdbID =  request.getAttribute("prdbID")!=null?(String)request.getAttribute("prdbID"):"";
String prName =  request.getAttribute("prName")!=null?(String)request.getAttribute("prName"):"";
String strprdbID =  request.getAttribute("strprdbID")!=null?(String)request.getAttribute("strprdbID"):"";
Map<Long,String> searchIdxList = request.getAttribute("searchIdxList")!=null?(HashMap<Long,String>)request.getAttribute("searchIdxList"):new HashMap<Long,String>();
ArrayList<String> resultIdxList = request.getAttribute("resultIdxList")!=null?(ArrayList<String>)request.getAttribute("resultIdxList"):new ArrayList<String>();
Map<Integer, Map<String,String>> docList = request.getAttribute("docList")!=null?(HashMap<Integer, Map<String,String>>)request.getAttribute("docList"):new HashMap<Integer, Map<String,String>>();
String accountNumber = request.getAttribute("accountNumber")!=null?(String)request.getAttribute("accountNumber"):"";
System.out.println("account Number::"+accountNumber);
String databaseName = request.getAttribute("databaseName")!=null?(String)request.getAttribute("databaseName"):"";
DocumentHandle docHandle = request.getAttribute("docHandle")!=null?(DocumentHandle)request.getAttribute("docHandle"):new DocumentHandle();
String actType = request.getAttribute("actType")!= null?(String)request.getAttribute("actType"):"";
List<UserDTO> actionList = request.getAttribute("actionList")!= null?(ArrayList<UserDTO>)request.getAttribute("actionList"):new ArrayList<UserDTO>();
Map<String,String> idxList = request.getAttribute("idxList")!= null?(Map<String,String>)request.getAttribute("idxList"):new HashMap<String,String>();
String primaryidx = request.getAttribute("primaryidx")!= null?(String)request.getAttribute("primaryidx"):"";
Boolean search = request.getAttribute("search")!= null?Boolean.parseBoolean(request.getAttribute("search").toString()):false;

//Nithu testing document view
String docFormat = request.getAttribute("docFormat")!= null?(String)request.getAttribute("docFormat"):"";
int docPageCount = request.getAttribute("docPageCount")!=null?Integer.parseInt(request.getAttribute("docPageCount").toString()):0;
String outputFormats = request.getAttribute("outputFormats")!=null?(String)request.getAttribute("outputFormats"):"";
String docInfoType =  request.getAttribute("docInfoType")!= null?(String)request.getAttribute("docInfoType"):"";
boolean imageOutput = request.getAttribute("imageOutput") != null? (Boolean)request.getAttribute("imageOutput"):false;
boolean pdfOutput = request.getAttribute("pdfOutput") != null? (Boolean)request.getAttribute("pdfOutput"):false;
boolean textOutput = request.getAttribute("textOutput") != null? (Boolean)request.getAttribute("textOutput"):false;
boolean tiffOutput = request.getAttribute("tiffOutput") != null? (Boolean)request.getAttribute("tiffOutput"):false;




//int pageSize = request.getAttribute("pageSize")!=null?Integer.parseInt(request.getAttribute("pageSize").toString()):10;
/* Nithu Alexander: 16/10/2014
Changing Page Display Count from 10 to 15
as part of Secondary Issues fix*/
int pageSize = request.getAttribute("pageSize")!=null?Integer.parseInt(request.getAttribute("pageSize").toString()):15;
if(pageSize == 0 && docList.size() != 0  )
{
	if( docList.size() < 10)
		pageSize = docList.size();
	else
/*Nithu Alexander: 16/10/2014
Changing Page Display Count from 10 to 15
as part of Secondary Issues fix
Previous Code Snippet:pageSize = 10 */
pageSize = 15;
}
String view = "";
String print = "";
String reprint = "";
String download = "";
String viewed = "";

String vaultIndexDesc = request.getAttribute("vaultIndexDesc")!=null?(String)request.getAttribute("vaultIndexDesc"):"";
String vaultIndex = request.getAttribute("vaultIndex")!=null?(String)request.getAttribute("vaultIndex"):"";

String attachmentPath = request.getAttribute("attachmentPath")!=null?(String)request.getAttribute("attachmentPath"):"";
System.out.println("attachmentPath::"+attachmentPath);
String attachment1 = request.getAttribute("attachment1")!=null?(String)request.getAttribute("attachment1"):"";
String attachment2 = request.getAttribute("attachment2")!=null?(String)request.getAttribute("attachment2"):"";
String attachment3 = request.getAttribute("attachment3")!=null?(String)request.getAttribute("attachment3"):"";
String attachment4 = request.getAttribute("attachment4")!=null?(String)request.getAttribute("attachment4"):"";
String attachURL1 ="\\\\"+host+attachmentPath+"\\"+attachment1;

String url1=attachmentPath+"\\"+attachment1;

String url2=attachmentPath+"\\"+attachment2;

String url3=attachmentPath+"\\"+attachment3;

String url4=attachmentPath+"\\"+attachment4;


%>
<c:url value="Attachment.do" var="ViewAttachOneURL">
	<c:param name="host" value="<%=host%>"/>
	<c:param name="attachmentPath" value="<%=attachmentPath%>"/>
	<c:param name="attachment" value="<%=attachment1%>"/>
</c:url>

<c:url value="Attachment.do" var="ViewAttachTwoURL">
	<c:param name="host" value="<%=host%>"/>
	<c:param name="attachmentPath" value="<%=attachmentPath%>"/>
	<c:param name="attachment" value="<%=attachment2%>"/>
</c:url>

<c:url value="Attachment.do" var="ViewAttachThreeURL">
	<c:param name="host" value="<%=host%>"/>
	<c:param name="attachmentPath" value="<%=attachmentPath%>"/>
	<c:param name="attachment" value="<%=attachment3%>"/>
</c:url>

<c:url value="Attachment.do" var="ViewAttachFourURL">
	<c:param name="host" value="<%=host%>"/>
	<c:param name="attachmentPath" value="<%=attachmentPath%>"/>
	<c:param name="attachment" value="<%=attachment4%>"/>
</c:url>


<script type="text/javascript">
var childwindows = new Array(); 
window.onbeforeunload=closewindows;

function openwindow(url)
{
var win=window.open(url);
childwindows[childwindows.length] = win;
}


function closewindows()
{
for(var i=0; i<childwindows.length; i++){   
	try{     
		childwindows[i].close()  
	}catch(e){
	
	} 
 } 

}


if(!counter)
var counter=0;

function ReadCookie()
{
var allcookies = document.cookie;
   // Get all the cookies pairs in an array
	cookiearray  = allcookies.split(';');

   // Now take key value pair out of this array
	for(var i=0; i<cookiearray.length; i++){
		var   name = cookiearray[i].split('=')[0];
		var  value = cookiearray[i].split('=')[1];
		if(name.valueOf() == " counter".valueOf()){
			return value;

		}
     
   }
}


function WriteCookie(x)
{
   document.cookie="counter=" + parseInt(x);
   
}




var allfields;
function submitForm(){
	//added code for loading icon
	ShowLoading();
	WriteCookie(0);
	var pr = 0;
	var aInputs = document.getElementsByTagName('input');
	for (var i=0;i<aInputs.length;i++) {
	allfields=aInputs;		
	var id = aInputs[i].id;
		if (id.match("Idx")){

			if(aInputs[i].value.length!=0){
				pr = 1;
			}
		}
	}
	if(pr != 1){
		alert("Please enter any one search value!!");
		return false;
	}
	document.getElementById("search").value = "true";
	document.getElementById("searchAction").action="SearchAccountNew.action";
	document.getElementById("searchAction").submit();
	return true;
}

function ShowLoading(e) {
    var div = document.createElement('div');
    var img = document.createElement('img');
    img.src = 'loading_1.gif';
    div.innerHTML = "Loading...<br />";
    div.style.cssText = 'position: fixed; top: 5%; left: 40%; z-index: 5000; width: 422px; text-align: center; background: #EDDBB0; border: 1px solid #000';
    div.appendChild(img);
    document.body.appendChild(div);
    return true;
    
}

/*Nithu Alexander: 03/09/2014
 * Inserting BACK Button
 */
function backsubmit(){
	document.getElementById("backButtonForm").action="SearchAccountNew.action";
    document.getElementById("backButtonForm").submit();
	return true;
}


function backsubmit1()
{   

var notsearch=true;

while(notsearch)
{
var url=window.location;

var i=url.indexOf("VaultApp");

var req=url.substr(i,url.length);

if(req.length>120)
	window.history.go(-1);
else
notsearch=false;

}

}


function selectDatabase(sel)
{   
	if(document.getElementById("sprdbID").value == ""){
		return false;
	}
	
	document.getElementById("prdbID").value = document.getElementById("sprdbID").value;
	document.getElementById("searchAction").action="SearchAccountNew.action";
    document.getElementById("searchAction").submit();
	return true;
}




function printPage(iFid)
{
	iFid.focus();
	iFid.print();
	
}

function setIframe(url)
{
document.write("<iframe name='iframe1' id='iframe1' width='1' height='1' src='"+url+"'></iframe>");
}

function printwindow(url)
{
mywindow=window.open(url,'print','left=200,top=200,toolbar=yes,width=950,height=500');
}

function printframe(printframeelement)
{
var myframe=document.getElementById(printframeelement);
document.execCommand("print",false,null);
}



</script>

<style>
#iframe1{
	visibility:hidden;
}
</style>
</head>

<!-- Nithu Alexander: 06 Jul 2015
AVA-10: Fix for Cross-site framing -->
<% response.addHeader("X-Frame-Options","SAMEORIGIN");%>

<!-- <body> -->

<body>
<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1230px; height:630px; margin-bottom: 1em; padding: 10px; overflow:auto">
	
	<table>
		<tr><td>
			<table>
				<tr><td valign="top">
					<table>
					<tr><td valign="top">
					<%--Nithu Alexander: 03/09/2014
					Inserting Back Button  --%>
					<form id="backButtonForm" name="backButtonForm" method="post">
					
					</form>
					<form id="searchAction" name="searchAction" method="post">
									<input type="hidden" name="strprdbID" id="strprdbID" value="<%=prdbID%>"/>
									<input type="hidden" name="prdbID" id="prdbID" value="<%=prdbID%>"/>
									<input type="hidden" name="prName" id="prName" value="<%=prName%>"/>
									<input type="hidden" name="search" id="search"/>
					<%if(strprdbID != ""||(!accountNumber.equals("")))
			   	 	{ %>
						<div align="left">
						<!--<input type="button" value="Back" onclick="backsubmit();return false;" />
						<a href="print.html" onClick="window.print();return false">print</a>-->
						</div>
				   <%}else{ %>
				   		<div align="left">
				   		<script></script>
						<!--<input type="button" value="Back" disabled />-->
						</div>
				   <%} %>
						<table>
						<tr><% if(csrDatabase.size() != 0 || message.equals(""))
				   		{ %>
						<td id="table1" width="15%"  valign="top">
							<table>
								<tr><%if(csrDatabase.size() > 1 && prdbID =="")
								
				   	 					{
										%>
									<td><table><tr><td align="left">Database:</td>
							  					   <td><select name="sprdbID" id="sprdbID" style="width:450px;" >
							   							<option value="" selected="selected">Select Database</option>
														<%
														for(Iterator<UserDTO> iter=csrDatabase.iterator();iter.hasNext();)
														{
															UserDTO dto = (UserDTO) iter.next();
															System.out.println("dto.getDbId()"+dto.getDbId());%>
															<option value="<%=dto.getDbId()%>"><%=dto.getDbDesc()%></option>
													<%	}%>
													  </select>&nbsp;&nbsp;
													  <input type="submit" name="search_db" value="Search" class="actionButton2" onclick="return selectDatabase(this);" />
										</td></tr></table></td>
								     </tr>
				   					<tr>
				   				<%} if(prdbID != "")
			   	 				{ %>
								 <td>
								    
									<table><tr><td align="left">
												<B>Database Name:</B></td>
												<td><%
													for(Iterator<UserDTO> iter=csrDatabase.iterator();iter.hasNext();)
													{
														UserDTO dto = (UserDTO) iter.next();
														if(dto.getDbId().toString().equalsIgnoreCase(prdbID)){
														System.out.println("dto.getDbId()"+dto.getDbId());%>
														<%=dto.getDbDesc()%>
														<%}%>
												<%	}%>
												</td></tr></table>
			   					</td>
			   				<%} %>
				   			</tr>
							<tr>
							<%-- Inceasing table width from 80% to 100% to fix image-truncate issue --%>
							<td width="100%"  valign="top">
							
							<table>	
								<tr>
									<%if(accountNumber.trim().equals("")){ %>
										<td align="left" width="auto">
										<table>
										 <tr><td>
										<table><tr>
										<% if(searchIdxList.size() != 0)
										   { 
										     for(Long idxID:searchIdxList.keySet())
										     { String tempstr[] = searchIdxList.get(idxID).split("_");
										   	   if(tempstr[0].equalsIgnoreCase("Idx")){
										      %><td width="210">
										      <%--Nithu Alexander: 03/09/2014
										      Fix to remove Double Colon
										      Previous Code Snippet: <table><tr><td><B><%=tempstr[2]%>::</B></td></tr>
										      --%>
										      	<table><tr><td><B><%=tempstr[2]%>:</B></td></tr>
										     	<tr><td><input type="text" name="textSearchs_Idx_<%=idxID%>_<%=tempstr[1]%>_<%=prdbID%>" 
										     	                     id="textSearchs_Idx_<%=idxID%>_<%=tempstr[1]%>_<%=prdbID%>" 
										     	                  value="" /></td></tr></table></td>
									         <%}
									         }%>
									         </tr>
									         <tr>
									         <td align="left"><input type="Submit" id="searchbutton" value="Search" onclick="return submitForm()"/>
									         <%--Nithu Alexander : 03/09/2014
									         Inserting BACK Button --%>
									         <input type="button" id="backbutton" value="Back" onclick="return backsubmit()"/>
									         </td></tr>
									    <%}else if(!(csrDatabase.size() > 1)){ %>
											<tr><td>Please configure Search indexes for the user!!! </td></tr>
											<%}%>
																					   
										</table></td></tr>
										<tr><td  align="left">
									<table><tr><td>
										
										<% if(docList.size() != 0)
									  	 { System.out.println("Inside docList"+docList.size());
											 int i =1; 
											 Map<String,String> idxvals; 
											 String vaultIdx = "";
											 String searchString ="";
											  %>
											</td>
											<tr><td>
											<%-- Nithu Alexander: 16/10/2014. Changing Page size from 10 to 15 --%>
											<display:table class="dataTable" name="docList" id="dispID1" export="false" pagesize="15" requestURI="/SearchAccountNew.action">
												<display:setProperty name="paging.banner.onepage" value=" " />
												<%--<display:setProperty name="export.excel.filename" value="SearchDetails.xls"/>
            									<display:setProperty name="export.xml.filename" value="SearchDetails.xml"/>
			  									<display:setProperty name="export.csv.filename" value="SearchDetails.csv"/>--%>
              									<%  idxvals = (Map<String, String>)pageContext.getAttribute("dispID1");
              										for(String col:idxvals.keySet())
													{ if(col.equalsIgnoreCase(vaultIndexDesc)){
														vaultIdx = idxvals.get(col);
													  } 
													  col = col.trim();%>
													  <display:column sortable="true" title='<%=col%>'> <%=idxvals.get(col)%> </display:column>
												  <%}%>
												 	 <display:column sortable="true" title="Action" style="width:200px">
												 	 <% for(Iterator<UserDTO> iter=actionList.iterator();iter.hasNext();)
                                      					{ UserDTO dto = (UserDTO) iter.next();
                            			 	 			  if(dto.getActionName().equalsIgnoreCase("View"))
								    	     			  	view = "true";
															if(databaseName==null)databaseName="";
								    	  				  if(dto.getActionName().equalsIgnoreCase("Download") && !prName.equalsIgnoreCase("communications1"))
								    	  	 				download = "true";
								    	  				  if(dto.getActionName().equalsIgnoreCase("Print") && !prName.equalsIgnoreCase("communications1"))
								    	  	 				print="true";
								     	  				  if(dto.getActionName().equalsIgnoreCase("RePrint") && !prName.equalsIgnoreCase("communications")) 
								     	  	 				reprint="true";
								      					} %>
								      					<%if(view.equals("true"))
									  					  {%>
															<a href="#" onClick="javascript:openwindow('SearchAccountNew.action?vaultIdx=<%=vaultIdx%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=view','','width=900,height=700')"><b>View </b></a>
														<%}if(download.equals("true"))
									  					  {%>|<a href="SearchAccountNew.action?vaultIdx=<%=vaultIdx%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=download"><b>Download </b></a>
														<%}if(print.equals("true"))
									  					  {%>|<a href="#" onClick="javascript:printwindow('SearchAccountNew.action?vaultIdx=<%=vaultIdx%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=print');return false;"><b>Print</b></a>
														<%}
														if(reprint.equals("true"))
									  					  {%>|<a href="#" onClick="javascript:window.open('SearchAccountNew.action?vaultIdx=<%=vaultIdx%>&prdbID=<%=prdbID%>&prName=<%=prName%>&actType=reprint','Reprint','left=200,top=200,toolbar=yes,width=100,height=100');return false;"><b>Reprint</b></a>
														<%}

														%>
																									  	
												  	</display:column>
											</display:table></td></tr>
									  	<%}else if(!(search==false)){ %>
											<tr><td>No Records Found </td></tr>
											<%}%>
											</table>
											</td></tr></table>
										<%	}
											//Nithu: Changed below condition as part of download action test
										if(!accountNumber.equals("") && actType.equals("view")){
												if(message != null && !message.equals(""))
												{%>
												<td align="left" width="auto">
												<table><tr><td>
												<div align="left" id="mes">
												<font color="red"><%=message%></font></div>
												<%}
												else if(session.getAttribute("message") != null)
												{%><div align="left" id="mes">
													<font color="red">	<%= session.getAttribute("message")%></font></div>
													<%session.setAttribute("message",null);
													
												}
												System.out.println("acct:"+accountNumber);
												System.out.println("db:"+databaseName);
												System.out.println("doc:"+docHandle.toString());
												String url = "Interface";
												System.out.println("Value of Doc handle before sending"+docHandle.toString());
												Boolean b=(Boolean)session.getAttribute("documentfound");
												if(b==true){
												%>
												<%--Nithu testing: 28 Aug 2015 --%>
												<jsp:include page="<%= url %>" flush="false" >
													<jsp:param name="acct" value="<%= accountNumber %>" />
													<jsp:param name="db" value="<%= databaseName %>" />
													<jsp:param name="selDocHandle" value="<%= docHandle.toString() %>" />
													<jsp:param name="selDocFormat" value="<%= docFormat %>" />
													<jsp:param name="selDocPageCount" value="<%= String.valueOf(docPageCount) %>" />
													<jsp:param name="selDocOutputFrmts" value="<%= outputFormats %>" />
													<jsp:param name="selImageOpFlag" value="<%= String.valueOf(imageOutput) %>" />
													<jsp:param name="selPdfOpFlag" value="<%= String.valueOf(pdfOutput) %>" />
													<jsp:param name="selTextOpFlag" value="<%= String.valueOf(textOutput) %>" />
													<jsp:param name="selTiffOpFlag" value="<%= String.valueOf(tiffOutput) %>" />
													<jsp:param name="selDocInfoType" value="<%= docInfoType %>" />
													<jsp:param name="op" value="view" />
													
												</jsp:include><%}%>
												
												<%
												System.out.println("include document done");												%>
											</td>
											<td>&nbsp&nbsp</td>
											<td valign="top">
											<table>
											<tr></tr><tr></tr>
											<tr><td>
											<%--Nithu Alexander : 29/08/2014
											Fix for Letter Attachment Issue
											Including NULL check and trim() method to check Empty String
											for attachments. Display 'Attachment' heading only if atleast one attachment 
											is available for document
											Previous Code Snippet:  <%if(b==true){%><table><tr><td><B>Attachments:</B></td></tr></table><%}%> --%>
												<%if(attachment1 != null && attachment2 != null && attachment3 != null && attachment4 != null && 
												(!(attachment1.trim().equals("")) || !(attachment2.trim().equals("")) || !(attachment3.trim().equals("")) || !(attachment4.trim().equals("")))) %>
												<table><tr><td><B>Attachments:</B></td></tr></table>
											</td></tr>
											<%--Nithu Alexander: 29/08/2014
											Fix for Letter Attachment 
											Removing b==true condition to display
											Attachment dispay table
											Previous Code Snippet: <tr><td><%if(b==true){%><table>--%>
											<tr><td><table>
											
											<%--Nithu Alexander : 28/08/2014
											Fix for Letter Attachment Issue
											Including NULL check and trim() method to check Empty String
											Previous Code Snippet: <%if(attachment1!=""){%>
											--%>
											<%--Nithu Alexander: 20th May 2015
											Adding condition for Hold indicator as part of ECMS R2 requirement
											 --%>
											<%if(attachment1 != null && !(attachment1.trim().equals("")) && !(attachment1.trim().equalsIgnoreCase("Y")) && !(attachment1.trim().equalsIgnoreCase("N"))){%>
											<tr>
											<td>
											<%-- Nithu Alexander: 19th May 2015
											Commenting below code snippet to remove hyperlink for attachments
											Adding attachment just as display information
											--%>
											<img width="35" height="33" src="images/attachment.png"/><b><%=attachment1%></b>
											<%-- <a href="#" onclick="window.open('<%=url1%>','Attachment1','menubar=1,resizable=1,width=350,height=250')" ><%=attachment1%></a> --%>
	
	
											<%--Nithu Alexander: 29/08/2014
												Fix For Letter Attachment Issue
												Commenting below lines completely
												Previous Code Snippet:<!--<img width="35" height="33" src="images/attachment.png"/><a href="#" onclick="window.open('<c:out value='${ViewAttachOneURL}'/>&act=savepdf','Attachment1','menubar=1,resizable=1,width=350,height=250')" ><%=attachment1%></a>
												<img width="35" height="33" src="images/attachment.png"/><a href="#" onclick="window.open('<%=url2%>','menubar=1,resizable=1,width=350,height=250')" ><%=attachment1%></a>-->
											--%>
												
											<%--<img width="35" height="33" src="images/attachment.png"/><a href="#" onclick="window.open('<c:out value='${ViewAttachOneURL}'/>&act=savepdf','Attachment1','menubar=1,resizable=1,width=350,height=250')" ><%=attachment1%></a> --%>
											<%--<img width="35" height="33" src="images/attachment.png"/><a href="#" onclick="window.open('<%=url2%>','menubar=1,resizable=1,width=350,height=250')" ><%=attachment1%></a> --%>
	
											</td>
											</tr>
	                           				<%} %>
	                           				
	                           				<%--Nithu Alexander: 28/08/2014
											Fix for Letter Attachment Issue
											Including NULL check and trim() method to check Empty String
											Previous Code Snippet: <%if(attachment2!=""){%>
											--%>
											<%--Nithu Alexander: 20th May 2015
											Adding condition for Hold indicator as part of ECMS R2 requirement
											 --%>
	                           				<%if(attachment2 != null && !(attachment2.trim().equals("")) && !(attachment2.trim().equalsIgnoreCase("Y")) && !(attachment2.trim().equalsIgnoreCase("N"))){%>
	                           				<tr>
											<td>
												<%-- Nithu Alexander: 19th May 2015
												Commenting below code snippet to remove hyperlink for attachments
												Adding attachment just as display information
												--%>
												<img width="35" height="33" src="images/attachment.png"/><b><%=attachment2%></b>
												<%--<a href="#" onclick="window.open('<%=url2%>','Attachment2','menubar=1,resizable=1,width=350,height=250')"><%=attachment2%></a> --%>
											</td>
											</tr>
	                           				<%} %>
	                           				
	                           				<%--Nithu Alexander : 28/08/2014 
											Fix for Letter Attachment Issue
											Including NULL check and trim() method to check Empty String
											Previous Code Snippet: <%if(attachment3!=""){%>
											--%>
											<%--Nithu Alexander: 20th May 2015
											Adding condition for Hold indicator as part of ECMS R2 requirement
											 --%>
	                           				<%if(attachment3 != null && !(attachment3.trim().equals("")) && !(attachment3.trim().equalsIgnoreCase("Y")) && !(attachment3.trim().equalsIgnoreCase("N"))){%>
	                           				<tr>
											<td>
												<%-- Nithu Alexander: 19th May 2015
												Commenting below code snippet to remove hyperlink for attachments
												Adding attachment just as display information
												--%>
												<img width="35" height="33" src="images/attachment.png"/><b><%=attachment3%></b>
												<%--<a href="#" onclick="window.open('<%=url3%>','Attachment3','menubar=1,resizable=1,width=350,height=250')"><%=attachment3%></a> --%>
											</td>
											</tr>
	                           				<%} %>
	                           				
	                           				<%--Nithu Alexander : 28/08/2014
											Fix for Letter Attachment Issue
											Including NULL check and trim() method to check Empty String
											Previous Code Snippet: <%if(attachment4!=""){%>
											--%>
											<%--Nithu Alexander: 20th May 2015
											Adding condition for Hold indicator as part of ECMS R2 requirement
											 --%>
	                           				<%if(attachment4 != null && !(attachment4.trim().equals("")) && !(attachment4.trim().equalsIgnoreCase("Y")) && !(attachment4.trim().equalsIgnoreCase("N"))){%>
	                           				<tr>
											<td>
												<%-- Nithu Alexander: 19th May 2015
												Commenting below code snippet to remove hyperlink for attachments
												Adding attachment just as display information
												--%>
												<img width="35" height="33" src="images/attachment.png"><b><%=attachment4%></b>
												<%--<a href="#" onclick="window.open('<%=url4%>','Attachment4','menubar=1,resizable=1,width=350,height=250')"><%=attachment4%></a> --%>
											</td>
											</tr>
	                           				<%} 
	                           				
	                           					System.out.println("reached attachment done");
	                           				
	                           				%>
	                           				</table>
	                           				<%--Nithu Alexander: 29/08/2014
	                           				Fix for Letter Attachment Issue 
	                           				Removing b==true condition to display
	                           				Attachment display table. Removing ending braces for b==true condition
	                           				Previous Code Snippet: </tr></table><%}%></td></tr></table>--%>
											</tr></table></td></tr></table>
											
										<%}else if(!accountNumber.equals("") && ((actType.equals("print")) || (actType.equals("reprint")))){
											System.out.println("reached attachment done4");
											if(message != null && !message.equals("")){
												System.out.println("reached attachment done5");%>
												<td align="left" width="auto">
												<table><tr><td>
												<div align="left" id="mes">
												<font color="red"><%=message%></font></div>
											<%}else if(session.getAttribute("message") != null){
												System.out.println("reached attachement done6");%><div align="left" id="mes">
												<font color="red">	<%= session.getAttribute("message")%></font></div>
												<%session.setAttribute("message",null);
											}%>
										</td></tr></table>
									<%} System.out.println("reached attachment done7");%>
									</td></tr>
						</table>
						</td>
										<%System.out.println("reached attachment done8");}
											else{
												System.out.println("reached attachment done9");%>
												<td align="center"><br><b>User has no database access</b></td>
				  								<%}
									System.out.println("reached attachment done10");%>
						</tr>
						</table></td></tr></table>
					</form>
				<%System.out.println("reached attachment done11");%>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	</table>
	</td></tr></table>

	<script type="text/javascript">
  	if(document.getElementById("fromDate") != null)
	{
	 Calendar.setup({
        inputField     :    "fromDate",      // id of the input field
        ifFormat       :    "%d-%m-%Y",       // format of the input field
        showsTime      :    false,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	}
	if(document.getElementById("toDate") != null)
	{
	 Calendar.setup({
        inputField     :    "toDate",      // id of the input field
        ifFormat       :    "%d-%m-%Y",       // format of the input field
        showsTime      :    false,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	}
	</script>

<s:include value="footer.jsp" />
</div>
</body>
</html>