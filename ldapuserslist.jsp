<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dto.DataDTO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>e2 Vault Administration</title>
<script type="text/javascript">

<% List<UserDTO> ldapUsersList = request.getAttribute("ldapUsersList") != null?(List<UserDTO>)request.getAttribute("ldapUsersList"): new ArrayList<UserDTO>();

List<String> csrGroups = (ArrayList<String>)request.getAttribute("csrGroups");			
		%>

function populateGroupUsers()
{
	
	var csrGroups = document.getElementById('csrGrps');
	var csrUsersIngroup = document.getElementById('usersInGroup');
	var len=csrGroups.length;
	
	for(var i=0; i<csrUsersIngroup.length; i++)
	{
		csrUsersIngroup.remove(i);
		i--;
		
	}
	
	for(var j=0; j<len; j++)
	{
	if(csrGroups[j].selected)
	{
		var tmp = csrGroups.options[j].text;
		
		var tmp1 =csrGroups.options[j].value;
	var userArray=tmp1.split(",");
	for(var i=0;i<userArray.length;i++)
		{
		var y=document.createElement('option');
		y.text=userArray[i];
		
		y.value=userArray[i];
		try
		{
			csrUsersIngroup.add(y,null);}
		catch(ex){
			csrUsersIngroup.add(y);	
		}
		}
	
	}
	}
}

function checkUsersAndCreateAdmin(button)
{
	
	var users = document.getElementById('usersInGroup');

	
	
	var len=users.length;
	var selectedUsers="";
	var count=0;
	for(var j=0; j<len; j++)
	{
	if(users[j].selected)
	{count++;
		var tmp = users.options[j].text;
		var tmp1 =users.options[j].value;
	selectedUsers=(tmp1+",");
	}
	}
	//alert("selected Yse"+selectedUsers)
	if(count == 0)
	{
	alert("Please Select a User");
	
		return false;
	}
	
	var csrGroups = document.getElementById('csrGrps');
	var tmp3=null;
	for(var k=0; k<csrGroups.length; k++)
	{
	if(csrGroups[k].selected)
	{
		tmp3 = csrGroups.options[k].text;
	
		
	}
	}
	//alert(tmp3);
	document.getElementById("selectedGroup").value=tmp3;
	document.getElementById("createAdminForm").action = "CreateAdminAction.action";
	document.getElementById("createAdminForm").submit;
	return true;
}


</script>
</head>
<body>

<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1500px; height:630px; margin-bottom: 1em; padding: 10px; overflow:auto">


<jsp:include page="useradminCommonTabs.jsp" />

<div id="countrydivsubcontainer" style="border:1px solid gray; width:1480px; height:590px; margin-bottom: 1em; padding: 10px; overflow:auto">

<h1><center><s:if test="hasActionErrors()">

      
  <s:actionerror />
 
 
</s:if><font color="#ff0000"><s:property value="message" /></font></center></h2><br>




<div id="countrydivsubcontainer2" style="border:1px solid gray; width:1300px; height:530px; margin-bottom: 1em; padding: 10px; overflow:auto">

<table width="100%" height="500" align="top">
	
	<tr><td valign="top" align="left">
	<form  id="createAdminForm" name="createAdminForm">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
	<table  valign="top">
	
		<tr>
			<td>
				<b>Ldap User Groups</b>
			</td>
			<td>
				<b>&nbsp;</b>
			</td>
			<td>
				<b>Ldap Users </b>
			</td>
		</tr>
		<tr><td>
			<%
		
		Iterator<String> itr = csrGroups.iterator();
		%>
				<select id="csrGrps" name="csrGrps" size="6" onclick="populateGroupUsers()" > 
				<%
		while (itr.hasNext()) {
			String groupName = itr.next();
			
			String userNames="";
			Iterator<UserDTO> users=ldapUsersList.iterator();
		 while(users.hasNext())
		 {
			 UserDTO u=users.next();
			 if(u.getUserGrp().equalsIgnoreCase(groupName))
			 {
				 userNames+=u.getUserName()+",";
			 }
		 }
		
	%>
			
			
			 <option value="<%= userNames %>"><%= groupName %></option>	
				
	<%} %>
			 </select>
			 </td>
			 <td><table>
</table>
</td>
			 <td>
					
	<input type="hidden" id="selectedGroup" name="selectedGroup" value="tmp"/>
			
	<select id="usersInGroup" name="usersInGroup" size="6"  > 
		
	
				</select>
				</td>
				
			</tr>
			<tr>
		<td colspan="3" align="right"><input type="Submit" value="Create Admin/s" onclick="return checkUsersAndCreateAdmin(this);"></td>
	</tr>	
	</table>

	</form>
		</td></tr></table>
	</div>
		</div>
		</div>
	
</body>
</html>