<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dao.DataDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>e2 Vault Administration</title>
<script type="text/javascript">
function moveoutid()
{
	var sda = document.getElementById("csrGrps");
	//alert(document.csrGrps);
	var len = sda.length;
	var sda1 = document.getElementById("selcsrGrps");
	
	for(var j=0; j<len; j++)
	{
		//alert( sda[j].text);
		if(sda[j].selected)
		{
			var tmp = sda.options[j].text;
			var tmp1 = sda.options[j].value;
			sda.remove(j);
			j--;
			var y=document.createElement('option');
			y.text=tmp;
			y.value=tmp1;
			try
			{
				sda1.add(y,null);
			
			}
			catch(ex)
			{
			sda1.add(y);
			}
		}
	}
}


function moveinid()
{
	var sda = document.getElementById('csrGrps');
	var sda1 = document.getElementById('selcsrGrps');
	var len = sda1.length;
	for(var j=0; j<len; j++)
	{
		if(sda1[j].selected)
		{
			var tmp = sda1.options[j].text;
			var tmp1 = sda1.options[j].value;
			sda1.remove(j);
			j--;
			var y=document.createElement('option');
			y.text=tmp;
			y.value=tmp1;
			try
			{
			sda.add(y,null);}
			catch(ex){
			sda.add(y);	
			}

		}
	}	
}
/*window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"inputField",
			dateFormat:"%d-%m-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1
		});
	};
	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
	*/
function checkValues()
{
	
}
</script>
</head>
<body>
<table width="100%"><tr><td>
<s:include value="userrepCommonTabs.jsp" />
<td></tr><tr><td>
<table width="100%" border="1px solid #778" height="500" align="top">
	
	<tr><td>
	<s:form action="AdminApplyAction">
	<table class="dataTable" >
	
		<tr>
			<td>
				<b>User Groups</b>
			</td>
			<td>
				<b>&nbsp;</b>
			</td>
			<td>
				<b>Selected User Groups</b>
			</td>
		</tr>
		<tr><td>
			<%
		List<String> csrGroups = (ArrayList<String>)request.getAttribute("csrGroups");			
		Iterator<String> itr = csrGroups.iterator();
		%>
				<select id="csrGrps" name="csrGrps" size="6" multiple="multiple"> 
				<%
		while (itr.hasNext()) {
			String groupName = itr.next();
	%>
			
			
			 <option value="<%= groupName %>"><%= groupName %></option>	
				
	<%} %>
			 </select>
			 </td>
			 <td><table><tr><td>
<input type="button" value=">>" onclick="moveoutid()"></td>
<td>
<input type="button" value="<<" onclick="moveinid()">
</td></tr>
</table>
</td>
			 <td>
					
	
		
	<select id="selcsrGrps" name="selcsrGrps" size="6" multiple="multiple" > 
		
	
				</select>
				</td>
				
			</tr>
			<tr>
		<td><input type="Submit" value="Next"></td>
	</tr>	
	</table>
	
	</s:form>
		</td></tr></table>
	
		</td></tr></table>
</body>
</html>