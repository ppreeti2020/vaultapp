<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dto.DataDTO" %>
<%@page import="com.pb.common.CommonConstants" %>
<%@page import="com.pb.common.PropertyUtils" %>

  <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
  <s:include value="adminHeader.jsp" />
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

  <link rel="stylesheet" type="text/css" media="all" href="styles/calendar-win2k-cold-1.css" title="win2k-cold-1" />

  <!-- main calendar program -->
  <script type="text/javascript" src="js/calendar.js"></script>

  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar-setup.js"></script>
	<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	
	<script src="js/jquery_treeview.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="js/demo.js"></script>
<title>e2 Vault Administration</title>
<%
String host = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.HOST_NAME);
String name = request.getAttribute("name")!=null?(String)request.getAttribute("name"):"";
int pageSize = request.getAttribute("pageSize")!=null?Integer.parseInt(request.getAttribute("pageSize").toString()):10;

System.out.println("pageSize::"+pageSize);

Map<Integer,DataDTO>  treeProfile = request.getAttribute("treeProfile")!=null?(Map<Integer,DataDTO>)request.getAttribute("treeProfile"):new HashMap<Integer,DataDTO>();

//System.out.println("treeProfile::"+treeProfile);
List populateLogList = request.getAttribute("populateLogList")!=null?(ArrayList)request.getAttribute("populateLogList"):new ArrayList();
if(pageSize == 0 && populateLogList.size() != 0  )
{
	if( populateLogList.size() < 10)
		pageSize = populateLogList.size();
	else
pageSize = 10;
}
String hostName = request.getAttribute("hostName")!=null?(String)request.getAttribute("hostName"):"";
String profile = request.getAttribute("profile")!=null?(String)request.getAttribute("profile"):"";
%>
<script>
  // this function is needed to work around 
  // a bug in IE related to element attributes
  function submitPage(button)
  {
	  
			document.getElementById("auditPage").action="AuditProfileAction.action";
		
            document.getElementById("auditPage").submit();
			return true;  
  }
onLoad=function()
{
	alert("hi");
 var x = document.getElementById("pageSize");
 alert(x);
 var len = x.length;

 for(var i=0;i<len;i++)
	 {
	 
	 if(x[i].value == '<%= pageSize %>')
		 {
	 x[i].selected = true;
	 x[i].value = '<%= pageSize %>';
		 }
	 }

}
if(navigator.appName == 'Microsoft Internet Explorer')

{

document.write('<link rel="stylesheet" type="text/css" href="/Scroll for IE.css" />');

}

else

{

document.write('<link rel="stylesheet" type="text/css" href="/Scroll for Other.css" />');

}
 
  </script>
  <title>tables</title>
  <style type="text/css">
  #playlist tr.header {
      background-color:#0072a8;
    }
    #playlist tr.even td {
      background-color: #eee;
    }
    #playlist tr.odd  td {
      background-color: #fff;
    }
  </style>
</head>

<body>
<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">

<table align="center"  width="100%">
<td valign="top">
<table width="100%">
<tr><td valign="top">
<form name="auditPage" id="auditPage">
<table width="100%" border="1" valign="top" height="450">
<tr>
<td width="20%"  valign="top">

<jsp:include page="AuditCheckLeftPanel.jsp" />
</td>
<td width="80%"  valign="top" >
<jsp:include page="AuditCheckCommonTabs.jsp" />

<div id="countrydivcontainer1" style="border:1px solid gray; width:1230px; height:550px; margin-bottom: 1em; padding: 10px; overflow:auto">

<table border="0" align="center" width="100%">	
<tr ><td align="left">
		<b>Audit Log: Profiles</b>
		</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr><td align="left"><table><tr>
<% if( populateLogList.size() !=0)
	{if( populateLogList.size()>10){%>
<td>

<select name="pageSize" id="pageSize"  style="width:50px;" onchange="return submitPage(this);">
<option value="10">10</option>
<%if( populateLogList.size()>20){%>
<option value="20">20</option>
<%}if( populateLogList.size()>30){%>
<option value="30">30</option>

<%}if( populateLogList.size()>40){%>
<option value="50">50</option>
<%}%>
</select>
</td>
<%} %>
<td align="right"><%=pageSize%> records of <%= populateLogList.size()%></td>
</tr>
</table>
</td>
</tr>
<tr><td>
<% DataDTO cd = new DataDTO();



%>
<div id="div2" style="overflow:auto; width:1200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:500px;">

<display:table name="populateLogList"  id="logList" export="true" pagesize="<%=pageSize%>" requestURI="/AuditProfileAction.action">
<display:setProperty name="paging.banner.onepage" value=" " />
<display:column sortable="true" title="ID"><% cd = ((DataDTO) pageContext.getAttribute("logList")); %><%=cd.getProfileInteId()%></display:column>
<display:column property="profile" sortable="true" title="Profile Name"/>
<display:column property="inteCheckDate" sortable="true" title="Integrity Check Date"/>
<display:column sortable="true" title="Exists">
<%  cd = ((DataDTO) pageContext.getAttribute("logList"));
String action=(cd.getAction()==null)?"":cd.getAction();
cd.setAction(action);
System.out.println("Action is"+action);
String fileStatus=(cd.getFileStatus()==null)?"":cd.getFileStatus();
cd.setFileStatus(fileStatus);
System.out.println("File status is"+fileStatus);
if(cd.getFileStatus().equals("T"))
		{
	%>
	<div align="center"><img src="images/right-icon1.gif" width="15" height="15" /></div>
	<%} else if(cd.getFileStatus().equals("F")){%>
	
	<div align="center"><img src="images/wrong-icon.png" width="15" height="15" /></div>
<%} %>
</display:column>

		</display:table>
		</div>
	</td>
</tr>
<%}else{%>
<tr><td>No Records Found </td></tr>
<%}%>
</table>
</div>
</td>
</tr>

</table>
</div>
</td>
</tr>
</table>
</form>
</td>
</tr>
</table>
</div>


</body>
</html>