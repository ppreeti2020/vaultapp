<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<i></i>
<html>
<head>
<%
String subAccessTabId = request.getAttribute("subAccessTabId") != null?request.getAttribute("subAccessTabId").toString():"";
System.out.println("subAccessTabId:"+subAccessTabId);
%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxsubtabs.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script>

</script>
<title>Insert title here</title>
</head>
<body >
<ul id="countrysubtabs" class="shadetabs">
<!--<li id="ldapTab"><a href="ActiveDirectoryAction.action" rel="#default" class="selected">LDAP Configuration</a></li>
<li id="vaultTab"><a href="VaultAction.action?server=server1" rel="countrysubcontainer1">Vault Configuration</a></li>
<li id="dbTab"><a href="DBAction.action" rel="countrysubcontainer1">Database Configuration</a></li>-->
<%if(subAccessTabId.equals("Add Printer"))
		{%>
			<li><a href="PrintersListAction.action" rel="#default" class="selected">Add Printer</a></li>
			<li><a href="AddedPrintersListAction.action" rel="countrysubcontainer1">Remove Printer</a></li>
		<%}
  else if(subAccessTabId.equals("Remove Printer"))
  		{%>
			<li><a href="PrintersListAction.action" rel="countrysubcontainer1">Add Printer</a></li>
			<li><a href="AddedPrintersListAction.action" rel="#default" class="selected">Remove Printer</a></li>
		<%}
		
%></ul>
<script type="text/javascript">

var countries=new ddajaxtabs("countrysubtabs", "countrydivsubcontainer");

countries.init();

</script>
</body>
</html>
