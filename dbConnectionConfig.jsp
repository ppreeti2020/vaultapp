<%@page import="com.pb.evault.DocumentHandle"%>
<%@page import="com.g1.e2.vault.VaultClient.Document"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.g1.e2.vault.VaultClient.Account"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    
<s:include value="adminHeader.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<% 
String serverType =  request.getAttribute("serverType")!= null? request.getAttribute("serverType").toString():"";
String serverHost =  request.getAttribute("serverHost")!= null? request.getAttribute("serverHost").toString():""; 
String serverIP =  request.getAttribute("serverIP")!= null? request.getAttribute("serverIP").toString():""; 
String serverPort =  request.getAttribute("serverPort")!= null? request.getAttribute("serverPort").toString():""; 
String dbUsername =  request.getAttribute("dbUsername")!= null? request.getAttribute("dbUsername").toString():""; 

String password =  request.getAttribute("password")!= null? request.getAttribute("password").toString():""; 
String dbName =  request.getAttribute("dbName")!= null? request.getAttribute("dbName").toString():""; 
System.out.println("dbName"+dbName);
%>
<script language="javascript" type="text/javascript">

function submitForm1()
{
	//action="ActiveDirectoryAction"
	//alert(document.pressed);
	
if(document.getElementById("serverType").checked == false)
	{
	alert("Please select ServerType");
	document.getElementById("serverType").focus();
	return false;
	}
	if(document.pressed != null || document.pressed != "")
			{
		if(document.pressed == "Test Connection")
			{
			document.getElementById("act").value = "test";
			//alert(document.getElementById("act").value);
			}
		if(document.pressed == "go")
		{
		document.getElementById("act").value = "dblist";
		//alert(document.getElementById("act").value);
		}
	document.getElementById("dbsubmitform").action = "DBSubmitAction.action";
	document.getElementById("dbsubmitform").submit;
	return true;
			}
	else
		{
		return false;
		}
	}
	function submitForm(button)
{
	//action="ActiveDirectoryAction"
	//alert(button.value);
	var rad = document.getElementsByName("serverType");
	
	var checked = false;

	for (var i=0 ;i < rad.length ; i++) {
		if(rad[i].checked == true) {
				checked = true;	
		}
	}	
	
	if(!checked) {
		alert("Please select Server Type");
		return false;
	}		
	if(document.getElementById("serverHost").value == "")
	{
	alert("Please Enter Server Host");
	document.getElementById("serverHost").focus();
	return false;
	}
	
	if(document.getElementById("serverIP").value == "")
	{
	alert("Please Enter Server IP");
	document.getElementById("serverIP").focus();
	return false;
	}
	
	if(document.getElementById("serverPort").value == "")
	{
	alert("Please Enter Server Port");
	document.getElementById("serverPort").focus();
	return false;
	}
	
	if(document.getElementById("dbUsername").value == "")
	{
	alert("Please Enter User Name");
	document.getElementById("dbUsername").focus();
	return false;
	}
	
	if(document.getElementById("password").value == "")
	{
	alert("Please Enter Password");
	document.getElementById("password").focus();
	return false;
	}
	
	if(button.value != "go")
	{
		var list = document.getElementById('dbName');
		var dbselected = false;
		
	
	for (var i=0 ;i < list.options.length ; i++) {
		if(list.options[i].selected == true) {
			dbselected = true;	
		}
	}
	
		if(!dbselected)
		{
	alert("Please select Database Name");
	document.getElementById("dbName").focus();
	return false;
		}
	
	}	
	
	if(button.value != null || button.value != "")
			{
		if(button.value == "Test Connection")
			{
			document.getElementById("act").value = "test";
			//alert(document.getElementById("act").value);
			}
		if(button.value == "Save")
		{
		document.getElementById("act").value = "save";
		//alert(document.getElementById("act").value);
		}
		if(button.value == "go")
		{
		document.getElementById("act").value = "dblist";
		//alert(document.getElementById("act").value);
		}
	document.getElementById("dbsubmitform").action = "DBSubmitAction.action";
	document.getElementById("dbsubmitform").submit;
	return true;
			}
	else
		{
		return false;
		}
	}

	function cancelForm(button)
{
	//action="ActiveDirectoryAction"
	//alert(document.pressed);
	
		
	document.getElementById("dbsubmitform").action = "ProfilesListAction.action";
	document.getElementById("dbsubmitform").submit;
	return true;
			
	}


function loadFun()
{
	
var radi = document.getElementsByName("serverType");
	var sel = document.getElementById("dbName");
	
	for (var i=0 ;i < radi.length ; i++) {
		if(radi[i].value == '<%=serverType%>') {
				radi[i].checked = true;	
		}
	}	
	
	for (var i=0 ;i < sel.length ; i++) {
		
		if(sel[i].value == '<%=dbName%>') {
				sel[i].selected = true;	
		}
	}
	
	if(document.getElementById("serverPort").value == 0)
	{
		document.getElementById("serverPort").value="";
	}
}
</script>
</head>
<body onload="loadFun()">
<jsp:include page="CommonTabs.jsp" />

	<div id="countrydivcontainer"
		style="border: 1px solid gray; width: 1230px; height: 610px; margin-bottom: 1em; padding: 10px; overflow: auto">


		<jsp:include page="SystemCommonTabs.jsp" />

		<div id="countrydivcontainer1"
			style="border: 1px solid gray; width: 1180px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">

			<jsp:include page="DatabaseCommonTabs.jsp" />

			<div id="countrydivcontainer1"
				style="border: 1px solid gray; width: 1180px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">
				
				<!-- Nithu Alexander: Added as part of "XSS" and AVA-1,AVA-2 fixes*/  -->
				<font color="#ff0000"><s:property value="message" /> </font>
			

<form id="dbsubmitform" name="dbsubmitform" >	
<table width="100%" border="1px solid #778" height="500" valign="top" align="center">
<tr>
<td valign="top">
<table width="100%" valign="top">
<tr>
<td valign="top" align="center">
<table width="100%" valign="top">
<tr >
<td align="left" colspan=2>
<b>Database Server Configuration:</b>
</td>
</tr>
<tr>
<td align="right" colspan=2>
Server Type:
</td>
<td width="50%" align="left" colspan=2>
<% 
List servers = request.getAttribute("servers")!= null?(ArrayList) request.getAttribute("servers"):new ArrayList(); 
Iterator iter=servers.iterator();
while(iter.hasNext()){
	String str = (String) iter.next();%>

<input type="radio"  name="serverType" id="serverType" value="<%=str%>"/><%=str%>
<%}%>
</td>
</tr>
<tr>
<td width="50%" align="right" colspan=2>
Server Host:
</td>
<td width="50%" align="left" colspan=2>
<input type="text" name="serverHost" id="serverHost" value="<%=serverHost%>"/>
</td>
</tr>
<tr>
<td width="50%" align="right" colspan=2>
Server IP Address:
</td>
<td width="50%" align="left" colspan=2>
<input type="text" name="serverIP" id="serverIP" value="<%=serverIP%>"/>
</td>
</tr>
<tr>
<td width="50%" align="right" colspan=2>
Server Port Number:
</td>
<td align="left"colspan=2 >
<input type="text" name="serverPort" id="serverPort" value="<%=serverPort%>"/>
</td>
</tr>
<tr>
<td align="right" colspan=2>
User Name:

</td>
<td align="left" colspan=2>
<input type="text" name="dbUsername" id="dbUsername" value="<%=dbUsername%>"/>

</td>
</tr>

<tr>
<td align="right" colspan=2>Password:</td>
<td align="left" colspan=2>

<input type="password" name="password" id="password" value="<%=password%>" />&nbsp;
</td>
</tr>
<tr>
<td align="right" colspan=2>Database Name:</td>
<td align="left" colspan=2>
<% List dbList = request.getAttribute("dbList")!= null?(ArrayList) request.getAttribute("dbList"):new ArrayList(); 
Iterator iters=dbList.iterator();
%>
<select  name="dbName" id="dbName" style="width:150px">
 <%while(iters.hasNext()){
	 String names = (String)iters.next();%>
<option value="<%=names %>"> <%= names%></option>
 <%}%>

 </select>
<input type="submit" value="Refresh Databases" style="padding:0em; margin:0em;" onclick="document.pressed=this.value"/>
</td>
</tr>
<tr>
<td width=50% align="center" colspan=2>
<input type="hidden" name="act" id="act"/>

</td>

</tr>

<tr>
<td width=50% align="center" colspan=4>
<!-- Nithu Alexander: Commented as part of "XSS" and AVA-1,AVA-2 fixes*/  -->
<!--<s:property value="message" />-->
</td>
</tr>
<tr>
<td width=50% align="center" colspan=4>
<input type="submit" value="Test Connection" onclick="return submitForm(this);"/> <input type="submit" value="Save"  onclick="return submitForm(this);"/> <input type="submit" value="Cancel" onclick="return cancelForm(this);"/>
</td>

</tr>
</table>
</td></tr></table>
</td></tr></table>
</form>

</div></div></div>

</body>
</html>