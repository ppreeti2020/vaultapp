<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dto.DataDTO" %>
<%@page import="com.pb.common.CommonConstants" %>
<%@page import="com.pb.common.PropertyUtils" %>


  <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
  <s:include value="adminHeader.jsp" />
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

  <link rel="stylesheet" type="text/css" media="all" href="styles/calendar-win2k-cold-1.css" title="win2k-cold-1" />

  <!-- main calendar program -->
  <script type="text/javascript" src="js/calendar.js"></script>

  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar-setup.js"></script>
	<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	
	<script src="js/jquery_treeview.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="js/demo.js"></script>
<title>e2 Vault Administration</title>
<% 
String host = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.HOST_NAME);
String name = request.getAttribute("name")!=null?(String)request.getAttribute("name"):"";
int pageSize = request.getAttribute("pageSize")!=null?Integer.parseInt(request.getAttribute("pageSize").toString()):10;
String hostId=(String)request.getParameter("hostId");
System.out.println("pageSize::"+pageSize);

Map<Integer,DataDTO>  treeProfile = request.getAttribute("treeProfile")!=null?(Map<Integer,DataDTO>)request.getAttribute("treeProfile"):new HashMap<Integer,DataDTO>();

System.out.println("treeProfile::"+treeProfile);
List populateLogList = request.getAttribute("populateLogList")!=null?(ArrayList)request.getAttribute("populateLogList"):new ArrayList();
if(pageSize == 0 && populateLogList.size() != 0  )
{
	if( populateLogList.size() < 10)
		pageSize = populateLogList.size();
	else
pageSize = 10;
}
String hostName = request.getAttribute("hostName")!=null?(String)request.getAttribute("hostName"):"";
String auditLogTabId=request.getAttribute("auditLogTabId")!=null?(String)request.getAttribute("auditLogTabId"):"";
String profile = request.getAttribute("profile")!=null?(String)request.getAttribute("profile"):"";
String searchProfile = request.getAttribute("searchProfile")!=null?(String)request.getAttribute("searchProfile"):"";
String logDate = request.getAttribute("logDate")!=null?(String)request.getAttribute("logDate"):"";

//String name = request.getAttribute("name")!=null?(String)request.getAttribute("name"):"";
System.out.println("compName"+name);
String tab="";
if(auditLogTabId.equalsIgnoreCase("indexLog"))
{
	tab="indexLog";
}

if (auditLogTabId.equalsIgnoreCase("index")) {
	tab="index";
}

if (auditLogTabId.equalsIgnoreCase("indexResource")) {
	tab="indexResource";
}

if (auditLogTabId.equalsIgnoreCase("reIndexLog")) {
	tab="reIndexLog";
}

if (auditLogTabId.equalsIgnoreCase("reIndex")) {
	tab="reIndex";
}


if (auditLogTabId.equalsIgnoreCase("reIndexResource")) {
	tab="reIndexResource";
}




if (auditLogTabId.equalsIgnoreCase("purgingLog")) {
	tab="purgingLog";
}


if (auditLogTabId.equalsIgnoreCase("purge")) {
	tab="purge";
}

if (auditLogTabId.equalsIgnoreCase("purgeResource")) {
	tab="purgeResource";
}



if (auditLogTabId.equalsIgnoreCase("removeLog")) {
	tab="removeLog";
}


if (auditLogTabId.equalsIgnoreCase("remove")) {
	tab="remove";
}

if (auditLogTabId.equalsIgnoreCase("removeResource")) {
	tab="removeResource";
}





%>
<script>
  // this function is needed to work around 
  // a bug in IE related to element attributes
  function submitPage(button)
  {
	  
			document.getElementById("auditPage").action="AuditAction.action";
		
            document.getElementById("auditPage").submit();
			return true;  
  }
onLoad=function()
{
	alert("hi");
 var x = document.getElementById("pageSize");
 alert(x);
 var len = x.length;

 for(var i=0;i<len;i++)
	 {
	 
	 if(x[i].value == '<%= pageSize %>')
		 {
	 x[i].selected = true;
	 x[i].value = '<%= pageSize %>';
		 }
	 }

}





 /* function getRes(url,val,userId,indexId)
  {
	  //alert(userId);
	 // alert(url);
	  var grid="";
	 // var u="<s:url value='"+url+"?Index_"userId"_"indexId"'/>";
	 var u = "Index_"+userId+"_"+indexId;
	//  alert(u);
	  grid += "<select name='sel' id='sel'  onchange=getResults(this,'<s:url value='"+url+"'/>',"+userId+","+indexId+") style='width:50px;'><option value='all'>All</option><option value='100'>100</option><option value='200'>200</option></select>";
	  document.getElementById("res_"+userId+"_"+indexId).innerHTML = grid;
  }
  function getResults(val,url,userId,indexId)
  {
	// alert(val.value);

	  window.open(url+"?userId="+userId+"&indexId="+indexId+"&res="+val.value,'IndexResults','width=300,height=250,toolbar=no,resizable=yes,menubar=yes,scrollbars=yes')
  }
function Ajax(popUrl,val,userId,indexId) {
  var xmlHttpReq = false;
  var self1 = this;
  var stdin='';
  // Mozilla/Safari
  var url = "AjaxIndexAction.action?userId="+userId+"&indexId="+indexId;
 // alert(val.checked);
 if(val.checked == true)
	{
  if (window.XMLHttpRequest) {
    xmlHttpReq = new XMLHttpRequest();
  }
  // IE
  else if (window.ActiveXObject) {
    xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
  }
  if (!xmlHttpReq) {
    alert('ERROR AJAX:( Cannot create an XMLHTTP instance');
    return false;
  }   
  xmlHttpReq.open('POST', url, true);
  //xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xmlHttpReq.onreadystatechange = function() {
	 
		/* if(table != null)
			  {
    var rows = table.getElementsByTagName("tr");
    for (i = 1; i < rows.length; i++) {
		document.getElementById("AccordionContent"+i).style.display = 'none';
	}
	document.getElementById("AccordionContent"+stdout).style.display = 'block';
	
	document.getElementById("AccordionContent"+stdout).innerHTML = "loading..";*/
	
    /*if (xmlHttpReq.readyState == 4 )
	  {
		
		//xmlHttpReq.responseText
		if(xmlHttpReq.status == 200)
		  {
			//alert(xmlHttpReq.responseXML.getElementsByTagName("IndexResults")[0]);
			var list = xmlHttpReq.responseXML.getElementsByTagName("IndexResults")[0].childNodes[0].nodeValue;
		 
			
	 var grid="";
	 // var u="<s:url value='"+url+"?Index_"userId"_"indexId"'/>";
	
 
	if(list != null)
			  {
		if(list == "Morethan 100 Results!!")
				  {
			alert(list);
				  }
				  if(list != "Morethan 100 Results!!")
				  {
	  grid += "<select name='sel' id='sel'  onchange=getResults(this,'<s:url value='"+popUrl+"'/>',"+userId+","+indexId+") style='width:50px;'><option value='all'>All</option><option value='"+list+"'>"+list+"</option></select>";
	  document.getElementById("res_"+userId+"_"+indexId).innerHTML = grid;
			  }
			  }
	

		

		  }  
	  }
	  }
  }
  xmlHttpReq.send(stdin);
}*/

	  function update(value){
		  var url="ShowLogErrorsAction.action?logFileId="+value;
		  window.open(url,"_blank","directories=no, status=no,width=900, height=500,top=100,left=100");
		}




  </script>
  <title>tables</title>
  <style type="text/css">
  #playlist tr.header {
      background-color:#0072a8;
    }
    #playlist tr.even td {
      background-color: #eee;
    }
    #playlist tr.odd  td {
      background-color: #fff;
    }
  </style>
</head>

<body>

<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">


<form name="auditPage" id="auditPage">

<table width="100%" border="1" valign="top" height="600">
<tr>
<td width="20%"  valign="top">

<jsp:include page="AuditLeftPanel.jsp" />
</td>
<td width="80%"  valign="top" >
<jsp:include page="AuditCommonTabs.jsp" />
<div id="countrydivcontainer1" style="border:1px solid gray; width:1230px; height:550px; margin-bottom: 1em; padding: 10px; overflow:auto">

<table border="0" align="center" width="100%">	
<tr ><td align="left">
		<b>Audit Log: <%= name %></b>
		</td>
</tr>
<tr>
<td>
<table>
<tr>
<td>
<b>Filter By:&nbsp;&nbsp;</b>
</td>
<td>
Profile Name:&nbsp;
</td>
<td>
<select name="searchProfile" id="searchProfile">
<option value=""></option>
<% for(Integer rootId1: treeProfile.keySet()){
		
		
			DataDTO ddao1 = (DataDTO)treeProfile.get(rootId1);
			
			System.out.println(""+ddao1.getProfile());
		%> 
<option value="<%= ddao1.getProfile()%>"><%= ddao1.getProfile()%></option>
<%} %>
</select>
</td>
<td>&nbsp;&nbsp;&nbsp;Date:</td>
<td><input type="text" name="logDate" id="logDate" value="<%=logDate%>" onclick="checkGroup(this);"></td>

<input type="hidden" name="name" value="<%= name %>"/>
<input type="hidden" name="hostId" value="<%=hostId%>"/>
<input type="hidden" name="server" value="server1"/>
<input type="hidden" name="tab" value="<%= tab %>"/>

<td>&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="search"  onclick="submitPage(this);"></td>
</tr>
</table>
</td>
</tr>
<tr><td align="left"><table><tr>
<% if( populateLogList.size() !=0)
	{if( populateLogList.size()>10){%>
<td>

<select name="pageSize" id="pageSize" style="width:50px;" onchange="return submitPage(this);">
<option value="10">10</option>
<%if( populateLogList.size()>20){%>
<option value="20">20</option>
<%}if( populateLogList.size()>30){%>
<option value="30">30</option>

<%}if( populateLogList.size()>40){%>
<option value="50">50</option>
<%}%>
</select>
</td>
<%} %>
<td align="right"><%=pageSize%> records of <%= populateLogList.size()%></td>
</tr>
</table>
</td>
</tr>
<tr><td>
<% DataDTO cd = new DataDTO();
%>

<%if(name.equalsIgnoreCase("Indexing") || name.equals(""))
		{
%>
<display:table name="populateLogList"  id="logList" export="true"  style="width:300px;" pagesize="<%=pageSize%>"  requestURI="/AuditAction.action">
<display:setProperty name="paging.banner.onepage" value=" " />
<display:setProperty name="export.excel.filename" value="AuditLog.xls"/>
            
			 <display:setProperty name="export.xml.filename" value="AuditLog.xml"/>
			  <display:setProperty name="export.csv.filename" value="AuditLog.csv"/>
            
	<display:column property="logDate" sortable="true" title="Log Started Date">
	<%  cd = ((DataDTO) pageContext.getAttribute("logList")); %>
	</display:column>
	<display:column property="logTime" sortable="true" style="width:20px" title="Log Started Time">
	
	</display:column>
	<display:column property="compressingType" sortable="true" title="Archival">
	
	</display:column>
	<display:column property="profile" sortable="true" title="Profile Name">
  </display:column>
	<display:column property="journalFile" sortable="true" title="Journal File">
	
	</display:column>
	
   <display:column property="drdFile" sortable="true" title="DRD File Name">
  </display:column>
    
	
	 <display:column property="drpFile" sortable="true" title="DRP File Name">
  </display:column>
    <display:column property="format" sortable="true" title="Format">
	
	</display:column>
	 <display:column property="totalRead" sortable="true" title="Total Read">
	
	</display:column>
	 <display:column property="totalWrite" sortable="true" title="Total Written">
	
	</display:column>
	<display:column property="finalRatio" sortable="true" title="Final Ratio">
	
	</display:column>
	<display:column property="method" sortable="true" title="Method">
	
	</display:column>
	
	<display:column property="documents" sortable="true" title="Documents">
	
	</display:column>
	<display:column property="documentPages" sortable="true" title="Document Pages">
	
	</display:column>
	<display:column property="ignoredPages" sortable="true" title="Ignored Pages">
	
	</display:column>
	
	<display:column sortable="true" title="Errors">
	<%
		String showErrors = "";
		if(cd.getErrorQccurs().equals("false"))
			{
			showErrors = "No Errors";
			%>
			<%= showErrors%>
			<%
			}
		else
			{%>
	  		
     
 <a href="" onclick="update('<%=cd.getLogFileId()%>')"> Errors Occurs</a>
			<%
			}
		%>
		
	
	</display:column>
	<display:column sortable="true" title="Resources">
	<%
		
		if(cd.getResourceCount() == 0)
			{
			
			%>
			<%= cd.getResourceCount()%>
			<%
			}
		else
			{%>
			   
     <%= cd.getResourceCount()%>
  
			<%
			}
		%>
		
	</display:column>
	 
	<display:column property="logEndDate" sortable="true" title="Log End Date">
	
	</display:column>
	<display:column property="logEndTime" sortable="true" title="Log End Time"/>
	
	
		</display:table>
		<%}else if(name.equals("Reindexing") || name.equals("Purging") || name.equals("Remove")) { %>
<display:table name="populateLogList"  id="logList" export="true"  pagesize="<%=pageSize%>"  requestURI="/AuditAction.action">
<display:setProperty name="export.excel.filename" value="AuditLog.xls"/>
             <display:setProperty name="export.xml.filename" value="AuditLog.xml"/>
			  <display:setProperty name="export.csv.filename" value="AuditLog.csv"/>
            <display:setProperty name="paging.banner.onepage" value=" " />
	<display:column property="logDate" sortable="true" title="Log Started Date">
	<%  cd = ((DataDTO) pageContext.getAttribute("logList")); %>
	</display:column>
	<display:column property="logTime" sortable="true" title="Log Started Time">
	
	</display:column>
	<display:column property="compressingType" sortable="true" title="Archival">
	
	</display:column>
	<display:column property="profile" sortable="true" title="Profile Name">
  </display:column>
	<display:column property="journalFile" sortable="true" title="Journal File">
	
	</display:column>
	
   <display:column property="drdFile" sortable="true" title="DRD File Name">
  </display:column>
  
	
	<display:column sortable="true" title="Errors">
	<%
		String showErrors = "";
		if(cd.getErrorQccurs().equals("false"))
			{
			showErrors = "No Errors";
			%>
			<%= showErrors%>
			<%
			}
		else
			{%>
			
       <a href="" onclick="update('<%=cd.getLogFileId()%>')"> Errors Occurs</a>

			<%
			}
		%>
		
	
	</display:column>
	<display:column sortable="true" title="Resources">
	<%
		
		if(cd.getResourceCount() == 0)
			{
			
			%>
			<%= cd.getResourceCount()%>
			<%
			}
		else
			{%>
			   
     <%= cd.getResourceCount()%>
  
			<%
			}
		%>
		
	</display:column>
	 
	
	<display:column property="logEndTime" sortable="true" title="Log End Time">
	
	</display:column>
	
		
	
	
	
		</display:table>
<%}%>
	
	</td>
</tr>
<%}else{%>
<tr><td>No Records Found </td></tr>
<%}%>
</table>
</div>
</td>
</tr>
<script type="text/javascript">
 
	 Calendar.setup({
        inputField     :    "logDate",      // id of the input field
        ifFormat       :    "%Y/%m/%d",       // format of the input field
        showsTime      :    false,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
var profile = document.getElementById("searchProfile");
for(var p = 0;p<profile.length;p++)
{
	
	if(profile[p].value  == '<%= searchProfile%>')
	{
		profile[p].selected = true;
	}
}


</script>
</table>
</form>
</div>



</body>
</html>