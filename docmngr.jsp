<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.*"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="java.text.*"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.evault.DocumentHandle"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
	
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Document Management Results</title>
<s:include value="adminHeader.jsp" />
<%List docList = request.getAttribute("docList")!=null?(ArrayList)request.getAttribute("docList"):new ArrayList();%>
<%List treeList = request.getAttribute("treeList")!=null?(ArrayList)request.getAttribute("treeList"):new ArrayList();%>
<%List treeAccList = request.getAttribute("treeAccList")!=null?(ArrayList)request.getAttribute("treeAccList"):new ArrayList();%>
<%List treeNameList = request.getAttribute("treeNameList")!=null?(ArrayList)request.getAttribute("treeNameList"):new ArrayList();%>
<%List docRList = request.getAttribute("docListProfile")!=null?(ArrayList)request.getAttribute("docListProfile"):new ArrayList();
Map<Long,DataDAO> docMapList = request.getAttribute("treeMapList")!=null?(HashMap<Long,DataDAO>)request.getAttribute("treeMapList"):new HashMap<Long,DataDAO>();
System.out.println("testing:treeList:"+request.getAttribute("treeList"));
%>
<%int pageSize = request.getAttribute("pageSize")!=null?Integer.parseInt(request.getAttribute("pageSize").toString()):10;
String tempacc="test";
String tempname="test";
System.out.println("pageSize::"+pageSize);
if(pageSize == 0 && treeList.size() != 0  )
{
	if( treeList.size() < 10)
		pageSize = treeList.size();
	else
pageSize = 10;
}%>
<%/**int pageSize1 = request.getAttribute("pageSize1")!=null?Integer.parseInt(request.getAttribute("pageSize1").toString()):10;
System.out.println("pageSize1::"+pageSize1);
if(pageSize1 == 0 && docRList.size() != 0  )
{
	if( docRList.size() < 10)
		pageSize1 = docRList.size();
	else
pageSize1 = 10;
}**/%>
<%List indexList = request.getAttribute("searchBoxes")!=null?(ArrayList)request.getAttribute("searchBoxes"):new ArrayList();; %>
<%//System.out.println("indexList::"+request.getAttribute("docList"));
String accountNumber = request.getAttribute("accountNumber")!=null?(String)request.getAttribute("accountNumber"):"";
String databaseName = request.getAttribute("databaseName")!=null?(String)request.getAttribute("databaseName"):"";
//String textSearch = request.getAttribute("textSearch")!=null?(String)request.getAttribute("textSearch"):"";

DocumentHandle docHandle = request.getAttribute("docHandle")!=null?(DocumentHandle)request.getAttribute("docHandle"):new DocumentHandle();
String textSearch =  request.getAttribute("textSearch")!=null?(String)request.getAttribute("textSearch"):"";
String Account =  request.getAttribute("Account")!=null?(String)request.getAttribute("Account"):"";
Long profileId =  request.getAttribute("profileId")!=null?Long.parseLong(request.getAttribute("profileId").toString()):0L;
%>

<%String accountNum = request.getParameter("AccountText")!= null?request.getParameter("AccountText"):"";%>
<%String date = request.getParameter("date")!= null?request.getParameter("date"):"";%>
<%String address = request.getParameter("Address")!=null?request.getParameter("Address"):"";%>

<%String name = request.getParameter("Name")!= null?request.getParameter("Name"):"";%>
<%String invlink = request.getParameter("Invlink") != null?request.getParameter("Invlink"):"";%>
<%String selAcc = request.getParameter("selAcc")!=null?request.getParameter("selAcc"):"";%>
<%String selAdd = request.getParameter("selAdd")!= null?request.getParameter("selAdd"):"";%>

<%String selFN = request.getParameter("selFN")!= null?request.getParameter("selFN"):"";
String message = request.getAttribute("message")!= null?(String)request.getAttribute("message"):"";
String searchReport = request.getAttribute("searchReport")!= null?(String)request.getAttribute("searchReport"):"";
String actType = request.getAttribute("actType")!= null?(String)request.getAttribute("actType"):"";%>
<%System.out.println("accountNumber = "+accountNumber);%>
<% if(!message.equals("")){session.setAttribute("message",message);}%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" media="all" href="styles/calendar-win2k-cold-1.css" title="win2k-cold-1" />

  <!-- main calendar program -->
  <script type="text/javascript" src="js/calendar.js"></script>

  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar-setup.js"></script>

	<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	<link rel="stylesheet" type="text/css" href="js/datepicker.css"/>
<script type="text/javascript" src="js/datepicker.js"></script>
	
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>

	
	<script type="text/javascript" src="js/demo1.js"></script>
	
	<script>
	$(document).ready(function(){
	
	// first example
	$("#browser").treeview();
	
	// second example
	$("#navigation").treeview({
		persist: "location",
		collapsed: true,
		unique: true
	});
	
	// third example
	$("#red").treeview({
		animated: "fast",
		collapsed: true,
		unique: true,
		persist: "cookie",
		toggle: function() {
			window.console && console.log("%o was toggled", this);
		}
	});
	
	// fourth example
	$("#black, #gray").treeview({
		control: "#treecontrol",
		persist: "cookie",
		cookieId: "treeview-black"
	});

});
	function hideTable(){
	var myTD=document.getElementById('table1');
	myTD.parentNode.removeChild(myTD);
	}
</script>
	<script>
	function submitSearch(button)
	{
				document.getElementById("Display").action="DocManager.action";
			
	            document.getElementById("Display").submit();
				return true;
	}
	function submitDateSearch(datesel,user,button)
	{
				
				document.getElementById("Display").action="DocManager.action";
			
	            document.getElementById("Display").submit();
				return true;
	}
	
	onLoad=function()
	{
		alert("hi");
	 var x = document.getElementById("pageSize");
	// var y = document.getElementById("pageSize1");
	 alert(x);
	// alert(y);
	 var len = x.length;
	// var len2 = y.length;

	 for(var i=0;i<len;i++)
		 {
		 
		 if(x[i].value == '<%= pageSize %>')
			 {
		 x[i].selected = true;
		 x[i].value = '<%= pageSize %>';
			 }
		 }
	 	}
	
	function func()
{
	//alert('<%=message%>');
 var update = "Successfully added to favourites";

 if('<%=message%>' != null && update=='<%=message%>')
	{
	 var grid = "<font color='red'>Successfully Updated</font>";
		//window.opener.location.reload();
		//window.location.reload(); 
		//opener.document.getElementById("mes").innerHTML=grid;
		
		window.close();
	}
}
	
function checkAll(checklist)
{
	var list = document.getElementsByName("docIds");
	if(list != null)
	{
	for(var v=0;v<list.length;v++)
		{
		
			if(list[v].checked)
			{
		list[v].checked = false;
			}
			else
			if(!list[v].checked)
			{
		list[v].checked = true;
			}
		}
		return false;
	}
}
function downloadDoc(actType)
{
	document.getElementById("Display").action="DocManager.action?actType="+actType;
	
    document.getElementById("Display").submit();
	return true;

}
function validate()
{
	 var  sdate = document.frm.date.value;
	 var sdateAr=sdate.split("-");
	if(document.frm.date.value=="")
	{
		
		alert("Please enter the date.");
		document.frm.date.focus();
		return false;
	}
	if(sdateAr.length!=3)
	{
		alert("Please enter valid Date in mm-dd-yyyy format.");
		document.frm.date.value="";
		document.frm.date.focus();
		return false;
	}
	if(document.frm.event_description.value=="")
	{
		alert("Please enter the event description.");
		document.frm.event_description.focus();
		return false;
	}
	return true;
	
}

</script>

</head>
<!-- <body> -->

<body topmargin=16 marginheight=16 onload="func()">

<table width="300" height="500"><tr><td>
<s:include value="userrepCommonTabs.jsp" />
<td></tr><tr><td >
<table align="center"  width="100%" height="500">

<tr>
<td valign="top">
<table width="100%">
<tr><td valign="top">
<table width="100%" border="1" valign="top" height="500">
<tr>
<td id="table1" width="15%"  valign="top">
<table>
<tr><td>
<form name="disForm" id="disForm" >
<input type="text" name="searchReport" id="searchReport" value="<%= searchReport %>"/><input type="Submit" value="Go" onclick="submitSearch(this)"/>
</form>
</td></tr>
<tr><td>
		<ul id="browser" class="filetree treeview"><%String temp="";String temp1="";int flag = 0;String temproot="";
		// docMapList.keySet()
				      for(Long rootId: docMapList.keySet())
						{DataDAO ddao = (DataDAO)docMapList.get(rootId);
						
							//if(!(ddao.getRootName().equals(temp)))
							//{
								System.out.println(rootId+"rootName:"+ddao.getRootName()+"-profileName:"+ddao.getRootID());
								flag=1;
								//temp = temp+ddao.getRootID();
								
								if(session.getAttribute("root") != null)
								{
									System.out.println("1234:"+session.getAttribute("root"));
									if(!ddao.getRootName().equals(session.getAttribute("root").toString()) && !ddao.getRootName().equals(temproot))
									{
										
										//session.setAttribute("root",ddao.getRootName());
										%>
										<li id="li<%= ddao.getRootID()%>"><span class="folder"><a href="#"><%=ddao.getRootName()%></a></span>
										<%
										temproot=ddao.getRootName();
									}
										else
										{
											temp1 = "true";
										}
								}
								else
							{System.out.println(rootId+"coming here:"+ddao.getRootName()+"-profileName:"+ddao.getProfileName());
								
							%>
							
							<li id="li<%= ddao.getRootID()%>"><span class="folder"><a href="#"><%=ddao.getRootName()%></a></span>
							<%  session.setAttribute("root",ddao.getRootName()); } if(!temp1.equals("true")){%>
							<ul><li><span class="file"><a href="DocManager.action?prID=<%=ddao.getProfileID()%>"><%=ddao.getProfileName()%></a></li></span></ul>
					<% }
							else
							{%>
							<script>


var inhtml = document.getElementById("li<%= ddao.getRootID()%>").innerHTML;
document.getElementById("li<%= ddao.getRootID()%>").innerHTML = inhtml+"<ul><li id=li<%= ddao.getRootID()%>><span><a href='DocManager.action?prID=<%=ddao.getProfileID()%>'><%=ddao.getProfileName()%></a></span></li></ul>";
//alert(document.getElementById("li<%= ddao.getRootID()%>").innerHTML);
								</script>
							<%}
							if (flag ==1){
								flag=0;
								%>
					</li>
					<%}%>
					<%} session.setAttribute("root",null);%>
		</ul>
</td></tr>
</table>

</td>
<td width="80%"  valign="top">

<table border="0" align="center" width="100%">	
<tr ><td align="left" width="100%">
<form id="Display" name="Display" method="post">
	<table width="100%"><tr><td align="left">
		<div id="mes"><font color="red"><%if(message != null && !message.equals("")){%>
	<%=message%>
	<%}else if(session.getAttribute("message") != null){%>
	<%= session.getAttribute("message")%>
	<%session.setAttribute("message",null);}%></font></div>
</td><td align="left">
		<input type="hidden" name="prID" id="prID" value="<%=profileId%>"/>
</td></tr>
<tr><td>
<%DataDAO dao1 = null; int i1=1;int size1=25; if(profileId == 0)
{%>
<table id="table2" width="100%">
<tr><td align="left"><table><tr><td>
		<B>Search :</B><input type="text" name="textSearch" id="textSearch" value="<%=textSearch%>"/>
		</td>
		<td>
		<input type="Submit" value="Search" onclick="submitSearch(this)"/>
		</td>
		</tr></table>
	</td></tr>

	<tr><td align="left"><table><tr>
<% if( treeList.size() !=0)
	{if( treeList.size()>10){%>
<td>

<select name="pageSize" id="pageSize" style="width:50px;" onchange="submitSearch(this);">
<option value="10">10</option>
<%if( treeList.size()>20){%>
<option value="20">20</option>
<%}if( treeList.size()>30){%>
<option value="30">30</option>

<%}if( treeList.size()>40){%>
<option value="50">50</option>
<%}%>
</select>
</td>
<%} %>
<td align="right"><%=pageSize%> records of <%= treeList.size()%></td>
</tr>
</table>
</td>
</tr>
<tr><td align="right"><a href="#"><B>Prev Date</B></a> | <b>Date:</b><input  type="text" name="datesel" id="cdate">
	<input type=button value="SelectDate" onclick="displayDatePicker('datesel', this)" onchange="submitDateSearch('datesel','<%=request.getSession().getAttribute("UserID") %>' ,this);"> | <a href="#"><B>Next Date</B></a>
	</td></tr>
	<tr><td>
	<display:table name="treeList" class="dataTable" id="dispID" export="true" pagesize="<%=pageSize%>" requestURI="/DocManager.action">
	
	<display:setProperty name="export.excel.filename" value="SearchDetails.xls"/>
            <display:setProperty name="export.pdf.filename" value="SearchDetails.pdf"/>
			 <display:setProperty name="export.xml.filename" value="SearchDetails.xml"/>
			  <display:setProperty name="export.csv.filename" value="SearchDetails.csv"/>
              <display:setProperty name="export.pdf" value="true" />
    <display:column sortable="true" title="ID"><%  
			dao1 = ((DataDAO) pageContext.getAttribute("dispID")); %><%=i1++%></display:column>
		
	<display:column property="profileName" sortable="true" title="Repository Name"/>
	
	<display:column property="indx1" sortable="true" title="Account"/>
		<display:column property="indx2" sortable="true" title="Address"/>
		<display:column property="indx3" sortable="true" title="Name"/>
		<display:column property="indx4" sortable="true" title="Date"/>
		
		<display:column sortable="true" title="Action">
		<a href="DocManager.action?docId=<%=dao1.getDocId() %>&prID=<%= dao1.getProfileID()%>&guid=<%= dao1.getGuid()%>&actType=view">view</a>|<a href="DocManager.action?docId=<%=dao1.getDocId() %>&prID=<%= dao1.getProfileID()%>&guid=<%= dao1.getGuid()%>&actType=download">download</a>|<a href="printers.jsp" target="name" onclick="window.open('printers.jsp','name','height=255,continued from previous line width=250,toolbar=no,directories=no,status=no,continued from previous line menubar=no,scrollbars=no,resizable=no'); return false;">print</a>
		</display:column>
	
	</display:table>
	</td></tr>
	<%}else{%>
<tr><td>No Records Found </td></tr>
<%}%>
	</table>
	<%}%>
	</td></tr>
<%System.out.println("accountNumber{} "+accountNumber);
System.out.println("accountNumber"+accountNumber);	
if(accountNumber.equals(""))
{
int counter=0;
int countdisp=indexList.size();
session.setAttribute("countdisppass",indexList.size());
if(indexList.size()!=0 && profileId !=0){
for(Iterator iter = indexList.iterator();iter.hasNext();)
{
	DataDAO indDAO = (DataDAO)iter.next();
	System.out.println("INDEX NAME : "+indDAO.getIndexName());
	counter++;
	if(indDAO.getIndexName().toString().equals("account") && counter<countdisp){%>
	<tr><td align="left">
		<B><%= indDAO.getIndexDesc()%> :</B><input type="text" name="AccountText" value="<%=accountNum%>"/>
		<select name="selAcc">
		<option value="OR">OR</option>
			<option value="AND">AND</option>
			
		</select>
	</td></tr>
<%}%>

<%if(indDAO.getIndexName().toString().equals("account") && counter>=countdisp){%>
	<tr><td align="left">
		<B><%= indDAO.getIndexDesc()%> :</B><input type="text" name="AccountText" value="<%=accountNum%>"/>
	</td></tr>
<%}%>

<%if(indDAO.getIndexName().toString().equals("address") && counter<countdisp){%>
	<tr><td align="left">
		<B><%= indDAO.getIndexDesc()%> :</B><input type="text" name="Address" value="<%=address%>"/>
		<select name="selAdd">
		<option value="OR">OR</option>
			<option value="AND">AND</option>
			
		</select>
	</td></tr>
<%}%>
<%if(indDAO.getIndexName().toString().equals("address") && counter>=countdisp){%>
	<tr><td align="left">
		<B><%= indDAO.getIndexDesc()%> :</B><input type="text" name="Address" value="<%=address%>"/>
	</td></tr>
<%}%>
<%if(indDAO.getIndexName().toString().equals("name") && counter<countdisp){%>
	<tr><td align="left">
		<B><%= indDAO.getIndexDesc()%> :</B><input type="text" name="Name" value="<%=name%>"/>
		<select name="selFN">
		<option value="OR">OR</option>
			<option value="AND">AND</option>
			
		</select>
	</td></tr>
<%}%>
<%if(indDAO.getIndexName().toString().equals("name") && counter>=countdisp){%>
	<tr><td align="left">
		<B><%= indDAO.getIndexDesc()%> :</B><input type="text" name="Name" value="<%=name%>"/>
	</td></tr>
<%}%>
<%if(indDAO.getIndexName().toString().equals("invlink")){%>
	<tr><td align="left">
		<B><%= indDAO.getIndexDesc()%> :</B><input type="text" name="Invlink" value="<%=invlink%>"/>
	</td></tr>
<%}%>
<%}%>
<tr><td align="left" width="100%">
	<input type="Submit" value="Search" onclick="submitSearch(this)"/>
</td></tr>
</tr>
<tr>
<td width="100%">

<%
if((request.getParameter("AccountText")!=null) || (request.getParameter("Address")!=null) || (request.getParameter("Name")!=null) || (request.getParameter("Invlink")!=null)){
%>
<s:include value="listDisplay.jsp"/>
<%}
else
{DataDAO dao = null;int i=1;int size=20;%>
<table width="100%"><tr><td><a href="javascript: downloadDoc(download);">download</a> || <a href="javascript: downloadDoc(print);">print</a>
</td></tr>
<tr><td>
	<display:table name="docListProfile" class="dataTable" id="dispID" pagesize="10" requestURI="/DocManager.action">
<display:column sortable="true" title='<input type="checkbox" name="allbox" id="allbox" onclick="return checkAll(this);"  />'>
    <input type="checkbox" name="docIds" id="docIds" value="${dispID.docId}"/>
    <input type="hidden" name="format" id="format" value="${dispID.format}"/>
  	</display:column>
	<display:column sortable="true" title="ID"><%  
			dao = ((DataDAO) pageContext.getAttribute("dispID")); %><%=i++%></display:column>
	<display:column property="indx1" sortable="true" title="Account"/>
	<display:column property="indx2" sortable="true" title="Address"/>
	<display:column property="indx3" sortable="true" title="Name"/>
	<display:column property="indx4" sortable="true" title="Date"/>
	
	<display:column property="profileName" sortable="true" title="Repository Name"/>
	<display:column sortable="true" title="Action">
	<a href="DocManager.action?docId=<%=dao.getDocId() %>&prID=<%= dao.getProfileID()%>&guid=<%= dao.getGuid()%>&actType=view&format=<%= dao.getFormat()%>">view</a>|<a href="DocManager.action?docId=<%=dao.getDocId() %>&prID=<%= dao.getProfileID()%>&guid=<%= dao.getGuid()%>&actType=download&format=<%= dao.getFormat()%>">download</a>
	</display:column>
	</display:table>
	</td></tr>
	</table>
<%}
}}
else if(!accountNumber.equals("") && !actType.equals("download"))
	{
	System.out.println("accountNumnber:"+accountNumber);
			System.out.println("databaseName:"+databaseName);
		String url = "Interface";%>
			 
		<jsp:include page="<%= url %>" flush="true" >
<jsp:param name="acct" value="<%= accountNumber %>" />
<jsp:param name="db" value="<%= databaseName %>" />
<jsp:param name="doc" value="<%= docHandle.toString() %>" />

<jsp:param name="op" value="view" />
</jsp:include>

	</td>
</tr>
</table>	

		<%} %>
</form>
<form id="DisplayNew" name="DisplayNew" method="post">
<table width="100%"><tr><td align="left">
<div id="mes"><font color="red">
	
<%System.out.println("treeAccList "+treeAccList);
if(treeAccList.size()!=0) {%>	
<tr><td align="left"><tr>
<td>
<B>Account :</B><select name="Accountlist" id="Accountlist" style="width:125px;">

<%	Iterator listIteracc =  treeAccList.iterator();
	
if(listIteracc.hasNext())
{
while(listIteracc.hasNext())
{
	DataDAO accDAO = (DataDAO) listIteracc.next();
	if(!tempacc.equals(accDAO.getIndx1().toString())){ %>
	<option value="<%=accDAO.getIndx1()%>"><%=accDAO.getIndx1()%></option>
<%	
tempacc=accDAO.getIndx1().toString();
}
}
}
%>
</select>
</td>
</tr>
<%} %>

<%System.out.println("treeNameList "+treeAccList);
if(treeNameList.size()!=0) {%>	
<tr><td align="left"><tr>
<td>
<B>Name :</B><select name="Namelist" id="Namelist" style="width:125px;">

<%	Iterator listItername =  treeNameList.iterator();
if(listItername.hasNext())
{
while(listItername.hasNext())
{
	DataDAO nameDAO = (DataDAO) listItername.next();
	if((nameDAO.getIndx3()!=null) && (!tempname.equals(nameDAO.getIndx3().toString()))){%>
	<option value="<%=nameDAO.getIndx3()%>"><%=nameDAO.getIndx3()%></option>
<%	
tempname=nameDAO.getIndx3().toString();
}
}
}
%>
</select>
</td>
</tr>
<tr><td><b>Date:</b><input  type="text" name="date" id="cdate">
<input type=button value="SelectDate" onclick="displayDatePicker('date', this)">
</td></tr>
<tr>
<td>
		<input type="Submit" value="Search" onclick="submitSearch(this)"/>
		</td></tr>
<%} %>	
</font>
</table>
</form>	

</table>
</td>



</tr>
</table>
</td>

</tr>
</table>
</td>
</tr>
</table>
<s:include value="footer.jsp" />
</body>
</html>