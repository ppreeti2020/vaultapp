<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec"%>

<s:include value="adminHeader.jsp" />
<html>
<head>
	<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
	<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
	<meta name="helpHref" content="GettingStarted" />
	<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
	<link rel="stylesheet" href="styles/login.css" />
	<link rel="stylesheet" href="styles/common/messages.css" />
	<link href="styles/common/header.css" rel="stylesheet" type="text/css"	media="all" />
	
	<title>Vault Administration</title>

<!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
<script type="text/javascript" src="js/calendar-setup.js"></script>
<%	List<UserDTO> csrDatabases = request.getAttribute("csrDatabases") != null? (ArrayList<UserDTO>) request.getAttribute("csrDatabases"): new ArrayList<UserDTO>();
	List<Long> csrUserDatabases = request.getAttribute("selCsrDatabases") != null? (ArrayList<Long>) request.getAttribute("selCsrDatabases"): new ArrayList<Long>();
	List<Long> csrProfileDatabases = request.getAttribute("selProfileDatabases") != null? (ArrayList<Long>) request.getAttribute("selProfileDatabases"): new ArrayList<Long>();
	System.out.println("csrUserDatabases:::"+csrUserDatabases);
	System.out.println("csrProfileDatabases:::"+csrProfileDatabases);
	System.out.println("csrDatabases:::"+csrDatabases);
	int dbLen = csrDatabases.size();
	int i=0;
	String profileName = request.getAttribute("profileName")!=null?request.getAttribute("profileName").toString():"";
	String profileAction = request.getAttribute("profileAction")!=null?request.getAttribute("profileAction").toString():"";
	System.out.println("profileAction:::"+profileAction);
	
%>
<script type="text/javascript">
	
function CancelcreateForm()
{
	document.getElementById("createProfile").action = "ShowCreateProfileAction.action";
	document.getElementById("createProfile").submit;
	return true;
}

function CanceleditForm()
{
	document.getElementById("createProfile").action = "ProfilesListAction.action";
	document.getElementById("createProfile").submit;
	return true;
}
		
function ValidateForm()
{
		var dbsel = false;
		var aInputs = document.getElementsByTagName('input');
		var count = 0;
		for (var i=0;i<aInputs.length;i++) {
			var type = aInputs[i].type;
			var name = aInputs[i].name;
			if(type != "hidden"){
				if (name.match("dbId")){
					if(aInputs[i].checked){
						dbsel= true;
					}
				}
				
			}
			
		}
		if(dbsel != true){
			alert("Please select any one of the Repositories!!!");
			return false;
		}
		
		return true;
		
	}
	
function EditDBBackSubmit(){
	document.getElementById("createProfile").action="ProfilesListAction.action";
    document.getElementById("createProfile").submit();
	return true;
}

function createDBBackSubmit(){
	document.getElementById("createProfile").action="ShowCreateProfileAction.action";
    document.getElementById("createProfile").submit();
	return true;
}
</script>
</head>
<body>
	<jsp:include page="CommonTabs.jsp" />

	<div id="countrydivcontainer"
		style="border: 1px solid gray; width: 1230px; height: 610px; margin-bottom: 1em; padding: 10px; overflow: auto">

		<jsp:include page="userpadminCommonTabs.jsp" />

		<div id="countrydivsubcontainer"
			style="border: 1px solid gray; width: 1180px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">

			<table width="100%" border="1px solid #778" height="500">
				<tr>
					<td valign="top" align="left"><s:form id="createProfile"
							action="SaveProfileDbSettingsAction">
							<table align="left">
								<tr>
									<td><b>Profile Name:</b>&nbsp;<%=profileName%></td>
								</tr>
								<tr>
									<td><b>Database Settings</b></td>
								</tr>
								
								<tr><td></td></tr>

								<tr>
									<td>
										<%Iterator<UserDTO> dbItrer = csrDatabases.iterator();%>

										<div id="div1"
											style="overflow: auto; width: 450px; border-top: solid 1px #C1C1C1; border-bottom: solid 1px #C1C1C1; border-left: solid 2px #C1C1C1; border-right: solid 2px #C1C1C1; height: 90px;">
											<table>
												<%	while (dbItrer.hasNext()) {
															UserDTO uDto = (UserDTO) dbItrer.next();
															if (csrProfileDatabases != null && csrProfileDatabases.contains(uDto.getDbId())) {
																if(csrUserDatabases!=null && csrUserDatabases.contains(uDto.getDbId())){%>
																	<tr><td>
																		<input type="checkbox" name="dbId_<%=uDto.getDbId()%>" value="<%=uDto.getDbId()%>" checked disabled/><%=uDto.getDbDesc()%>
																		<input type="hidden" name="dbId_<%=uDto.getDbId()%>" value="<%=uDto.getDbId()%>" />
																	</td></tr>
																<%}else{ %>
																	<tr><td>
																		<input type="checkbox" name="dbId_<%=uDto.getDbId()%>" value="<%=uDto.getDbId()%>" checked/><%=uDto.getDbDesc()%>
																		<input type="hidden" name="dbId_<%=uDto.getDbId()%>" value="unchecked" />
																	</td></tr>
																<%} %>
															<%}else{%>
																<tr><td>
																	<input type="checkbox" name="dbId_<%=uDto.getDbId()%>" value="<%=uDto.getDbId()%>" /><%=uDto.getDbDesc()%>
																	<input type="hidden" name="dbId_<%=uDto.getDbId()%>" value="unchecked" />
																</td></tr>
															<%} %>
												<%}	%>

											</table>
										</div></td>
									
								</tr>
								<tr>
									<td colspan="5">
									<!-- Nithu Alexander: 23/10/2014. New JS methods createDBBackSubmit() and EditDBBackSubmit() as part of
									BACK Button issue fix-->
									<!-- <input type="Submit" value="Back" onclick="history.go(-1);return false;"/>&nbsp;&nbsp; -->
									<%if(profileAction.equalsIgnoreCase("create")){ %>
										<input type="button" value="Back" onclick="return createDBBackSubmit()"/>&nbsp;&nbsp;
									<%}else if(profileAction.equalsIgnoreCase("edit")){ %>
										<input type="button" value="Back" onclick="return EditDBBackSubmit()"/>&nbsp;&nbsp;
									<%} %>
									<input type="Submit" value="Next" onclick="return ValidateForm()"/>&nbsp;&nbsp;
									<%if(profileAction.equalsIgnoreCase("create")){ %>
										<input type="Submit" value="Cancel" onclick="return CancelcreateForm()">
									<%}else if(profileAction.equalsIgnoreCase("edit")){ %>
										<input type="Submit" value="Cancel" onclick="return CanceleditForm()">
									<%} %>
									</td>
								</tr>

							</table>

						</s:form></td>
				</tr>
			</table>
			</div></div>
			
</body>
</html>