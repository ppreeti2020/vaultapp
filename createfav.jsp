<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Favourite Settings</title>
<%Long prId = request.getParameter("prID")!= null?Long.parseLong(request.getParameter("prID")):0L;
String query = request.getParameter("query")!= null?(request.getParameter("query")):"";
System.out.println("query:create Fav:"+query);
int listSize = request.getParameter("listSize")!= null?Integer.parseInt(request.getParameter("listSize")):0;
String message = request.getParameter("favName")!= null?request.getParameter("favName"):"";
String guid = request.getParameter("guid")!= null?request.getParameter("guid"):"";
String docId = request.getParameter("docId")!= null?request.getParameter("docId"):"";
System.out.println("%%%%%%%%create Fav:guid:"+guid);
Long favId = request.getParameter("favId")!= null?Long.parseLong(request.getParameter("favId")):0L;
Long favDocId = request.getParameter("favDocId")!= null?Long.parseLong(request.getParameter("favDocId")):0L;
String favName = request.getParameter("favName")!= null?request.getParameter("favName"):"";
System.out.println("%%%%%%%%create Fav:favName:"+favName);
String favDesc = request.getParameter("favDesc")!= null?request.getParameter("favDesc"):"";
String type = request.getParameter("type")!= null?request.getParameter("type"):"";
System.out.println("%%%%%%%%create Fav:favDesc:"+favDesc);
%>
<script>
function submitFav(button)
{
	//alert('<%= prId%>');
	
	document.getElementById("DisplayForm").action="DocManager.action";
			//document.getElementById("Display").action="\createfav.jsp";
			// window.open("\createfav.jsp",'IndexResults','width=300,height=250,toolbar=no,resizable=yes,menubar=no,scrollbars=yes')
	            document.getElementById("DisplayForm").submit();

				window.close();
				
				
}
function submitEditFav(button)
{
	//alert('<%= prId%>');
	
	document.getElementById("DisplayForm").action="DocManagerFav.action";
			//document.getElementById("Display").action="\createfav.jsp";
			// window.open("\createfav.jsp",'IndexResults','width=300,height=250,toolbar=no,resizable=yes,menubar=no,scrollbars=yes')
	            document.getElementById("DisplayForm").submit();

				window.close();
				
				
}
function func()
{
 var update = "Successfully added to favourites";

 if('<%=message%>' != null && '<%=message%>'!="")
	{
	 var grid = "<font color='red'>Successfully Updated</font>";
		//window.opener.location.reload();
		//window.location.reload(); 
		//opener.document.getElementById("mes").innerHTML=grid;
		window.close();
	}
}

</script>
</head>
<body>
<form name="DisplayForm" id="DisplayForm">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
<table  align="center">
<tr>
	<td >Favourites:
	</td>
	</tr>
<tr>
	<td>
<table  align="center">
<tr>
	<td>Name:</td>
	<td><input type="text" name="favName" id="favName" value="<%=favName%>"/></td>
</tr>
<tr>
	<td>Description:</td>
	<td><input type="text" name="favDesc" id="favDesc" value="<%=favDesc%>"/></td>
</tr>
<tr>
	<td><%if(prId != 0){%><input type="hidden" name="prID" id="prID" value="<%= prId%>"/><%}if(favId!=0){%><input type="hidden" name="favouriteId" id="favouriteId" value="<%= favId%>"/><input type="hidden" name="action" id="action" value="edit"/><%}if(favDocId!=0){%><input type="hidden" name="favDocId" id="favDocId" value="<%= favDocId%>"/><input type="hidden" name="action" id="action" value="edit"/><%}%>
	<%if(!guid.equals("")){%><input type="hidden" name="guid" id="guid" value="<%= guid%>"/><%}%><%if(!type.equals("")){%><input type="hidden" name="type" id="type" value="<%= type%>"/><%}%><%if(!docId.equals("")){%><input type="hidden" name="docId" id="docId" value="<%= docId%>"/><%}if(listSize!=0){%><input type="hidden" name="listSize" id="listSize" value="<%= listSize%>"/><%}if(!query.equals("")){ %><input type="hidden" name="query" id="query" value="<%=query%>"/><%}%></td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td><%if(prId != 0 && (favId==0 && favDocId==0)){%><input type="submit" name="submit" id="submit" value="Add" onclick="submitFav(this)";/><%}else if(favId != 0 || favDocId !=0){%> <input type="submit" name="submit" id="submit" value="Update" onclick="submitEditFav(this)";/><%} %></td>
</tr>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>