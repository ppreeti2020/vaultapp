<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<%@page import="java.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec"%>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet"
	type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css"
	type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css"
	media="all" />

<script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.mobile-1.0a1.min.js"></script>

<title>e2 Vault Administration</title>
<%
	int columnnum = request.getAttribute("columnnum") != null ? Integer
			.parseInt(request.getAttribute("columnnum").toString()) : 0;
	Long seldbId = request.getAttribute("seldbId") != null ? Long
					.parseLong(request.getAttribute("seldbId").toString()) : 0L;
	String tableName = request.getAttribute("tableName") != null ? request
			.getAttribute("tableName").toString() : "";
	String tableName1 = request.getAttribute("tableName1") != null ? request
			.getAttribute("tableName1").toString() : "";
	System.out.println("TableName:::" + tableName);
	System.out.println("TableName1:::" + tableName1);
	String message = request.getAttribute("message") != null ? request
			.getAttribute("message").toString() : "";
	List csrDatabase = request.getAttribute("csrDatabases") != null ? (ArrayList) request
			.getAttribute("csrDatabases") : new ArrayList();
	List<String> dbindexList = request.getAttribute("dbindexList") != null ? (ArrayList<String>) request
			.getAttribute("dbindexList") : new ArrayList<String>();
	Map<String, Integer> dbdataTypeMap = request
			.getAttribute("dbdataTypeMap") != null ? (Map<String, Integer>) request
			.getAttribute("dbdataTypeMap")
			: new HashMap<String, Integer>();
	System.out.println("dbdataTypeMap size:::" + dbdataTypeMap.size());
	System.out.println("dbindexList size:::" + dbindexList.size());
%>
<script type="text/javascript">
 var i = 0;
 function addColumn(){
	 var temp1=document.getElementById("columns").innerHTML;
	 if(i==0){
		 i=<%=dbindexList.size()%>;
	 }
	 i++;
	 temp1+="<div id='column"+i+"'><td width='200'><input style='width:200px' name='columnName"+i+"' id='columnName"+i+"' type='text'/></td>";
	 temp1+="<td width='200'><input width='200' name='columnDesc"+i+"' id='columnDesc"+i+"' type='text'/></td>";
	 temp1+="<td width='200'><select name='datatype"+i+"' id='datatype"+i+"'><option value=''></option>";
	 <%for (String type : dbdataTypeMap.keySet()) {%>
	 var temptype = '<%=type%>';
	 temp1+="<option value='"+temptype+"'>"+temptype+"</option>";
	<%}%>
	 temp1+="</select></td>";
	 temp1+="<td width='200'><input width='200'	name='width"+i+"' id='width"+i+"' type='text' /></td>";
	 temp1+="<td width='200'><input width='200'	name='constraint"+i+"' id='constraint"+i+"' type='checkbox' /></td>";
	 temp1+="<td width='200'><input type='button' value='Delete Column'	onclick='deleteColumn();'/></td></div>";
	 document.getElementById("columns").innerHTML = temp1;

 }
 
  
 $(document).ready(function (){
	 $("#InsertColumn").click( function(){
 		var temp1="";
 		 if(i==0){
 			 i=<%=dbindexList.size()%>;
 		 }
 		 i++;
 		 temp1+="<tr class='"+i+"'><td width='200'><input style='width:200px' name='columnName"+i+"' id='columnName"+i+"' type='text'/></td>";
 		 temp1+="<td width='200'><input width='200' name='columnDesc"+i+"' id='columnDesc"+i+"' type='text'/></td>";
 		 temp1+="<td width='200'><select name='datatype"+i+"' id='datatype"+i+"'><option value=''></option>";
 		 <%for (String type : dbdataTypeMap.keySet()) {%>
 		 var temptype = '<%=type%>';
 		 temp1+="<option value='"+temptype+"'>"+temptype+"</option>";
 		<%}%>
 		 temp1+="</select></td>";
 		 temp1+="<td width='200'><input width='200'	name='width"+i+"' id='width"+i+"' type='text' /></td>";
 		 temp1+="<td width='200'><input width='200'	name='constraint"+i+"' id='constraint"+i+"' type='checkbox' /></td>";
 		 temp1+="<td width='200'><input type='button' id='"+i+"' class='DeleteColumn"+i+"' value='Delete Column'/></td></tr>";
 		  		 
 		 $("#columns").append(temp1);
 		 var deletecolumn = ".DeleteColumn"+i;
 		$(deletecolumn).click( function(){
 			var id = "."+$(deletecolumn).attr('id');
			 $(this).closest('tr').fadeOut(300, function() { 
                 $(this).remove(); 
             });

		 });
 				
	 });
 });
	 	 
 function selectDatabase(sel)
 {
	document.getElementById("createTableForm").action="PopulateColumnsAction.action";
    document.getElementById("createTableForm").submit();
	return true;
 }
 
 function checkValues(){
	 
	 if(i < '<%=dbindexList.size()%>'){
		 i=<%=dbindexList.size()%>;
	 }
	 var columnnum = i;
	 var idxcolsnum = '<%=dbindexList.size()%>';
	// alert("Column num::"+columnnum+"tableName::"+'<%=tableName%>'+"idxcols size::"+idxcolsnum);
	 var divtext = '<%=tableName%>';
	 if(divtext!="")
	 {
		 var columnNames = new Array();
		 var columnDescs = new Array();
	 	 var widths = new Array();
	 	 var dataTypes = new Array();
	 	 var constraints = new Array();
	 	 var k = 0;
	 	 for(var j=1; j<=columnnum; j++)
	 	 {	
	 		var tempcolumnname = "columnName"+j;
	 		var column = document.getElementById(tempcolumnname);
	 		if(column){
	 			if( j<= idxcolsnum){
			 		var tempcolumnname = "columnName"+j;
			 	 	var Selected = document.getElementById(tempcolumnname);
			 	 	var columnName = Selected.options[Selected.selectedIndex].text;
			 	 	if(columnName.length == 0){
			 	 	 alert("Please select column name!!!");
			 	 	 return false;
			 	 	}
			 	 	else
			 	 	 columnNames[k]=columnName;
			 	}
			 	else{
			 		var tempcolumnname = "columnName"+j;
			 		var column = document.getElementById(tempcolumnname);
			 		if(column != null){
			 			var columnName = document.getElementById(tempcolumnname).value;
			 			if(columnName.length == 0){
					 	 	 alert("Please enter column name!!!");
					 	 	 return false;
					 	 	}
					 	else
					 	 	 columnNames[k]=columnName;
			 		}
			 		
				 }
		 		
		 		var tempcolumndesc = "columnDesc"+j;
		 		var columnDesc = document.getElementById(tempcolumndesc).value;
		 		if(columnDesc.length == 0){
			 	 	 alert("Please enter column Desc!!!");
			 	 	 return false;
			 	 	}
			 	else
			 	 	 columnDescs[k]=columnDesc;
		 		
			 	var tempdatatype = "datatype"+j;
		 	 	var Selected = document.getElementById(tempdatatype);
		     	var dataType = Selected.options[Selected.selectedIndex].text;
		 	 	if(dataType.length == 0){
		 	 	 alert("Please select dataType for the column!!!");
		 	 	 return false;
		 	 	}
		 	 	else
		 	 		dataTypes[k]=dataType;
		     	
		     	var tempwidth = "width"+j;
			 	widths[k] = document.getElementById(tempwidth).value;
			 	var width = document.getElementById(tempwidth).value;
			 	if(width.length == 0){
			 		widths[k] = "default";
			 	 	}
			 	else
			 		widths[k] = width;
		     	
		     	var tempconstraint = "constraint"+j;
		     	if(document.getElementById(tempconstraint).checked){
		     		constraints[k]="null";
		     	}
		     	else{
		     		constraints[k]="not null";
		     	}
		     	k++;
	 		}
	  		
	 	 }
	     document.getElementById("columnNames").value = columnNames;
	     document.getElementById("columnDescs").value = columnDescs;
	 	 document.getElementById("dataTypes").value = dataTypes;
	 	 document.getElementById("widths").value = widths;
	 	 document.getElementById("constraints").value = constraints;
	 	 var columnnum1 = k;
	 	 document.getElementById("columnnum1").value = columnnum1;
	}
	 return true;
 }

 
 </script>
</head>
<body>
	<jsp:include page="CommonTabs.jsp" />

	<div id="countrydivcontainer"
		style="border: 1px solid gray; width: 1230px; height: 610px; margin-bottom: 1em; padding: 10px; overflow: auto">


		<jsp:include page="SystemCommonTabs.jsp" />

		<div id="countrydivcontainer1"
			style="border: 1px solid gray; width: 1180px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">
			
			<jsp:include page="DatabaseCommonTabs.jsp" />

			<div id="countrydivcontainer1"
				style="border: 1px solid gray; width: 1180px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">
				<s:if test="hasActionErrors()">
					<s:actionerror />
				</s:if>
				<font color="#ff0000"><s:property value="message" /> </font>
				<table width="100%" border="1px solid #778" height="500"
					valign="top">
					<tr>
						<td valign="top" align="center">
							<form id="createTableForm" name="createTableForm" method="post"
								onsubmit="return checkValues();">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
								<s:hidden id="columnNames" name="columnNames" />
								<s:hidden id="columnDescs" name="columnDescs" />
								<s:hidden id="dataTypes" name="dataTypes" />
								<s:hidden id="widths" name="widths" />
								<s:hidden id="constraints" name="constraints" />
								<s:hidden id="columnnum1" name="columnnum1" />
								<s:hidden id="tableName1" name="tableName1" />
								<s:hidden id="seldbId" name="seldbId" />
								<%
									if (csrDatabase.size() != 0) {
								%>
								<table align="center">
									<tr>
										<td align="right"><B>Table Name:</B></td>
										<td><select name="tableName" id="tableName"
											style="width: 450px;" onchange="return selectDatabase(this);">
												<option value="">Select Database to create table
													for</option>
												<%
													for (Iterator iter = csrDatabase.iterator(); iter.hasNext();) {
															UserDTO dto = (UserDTO) iter.next();
															System.out.println("dto.getDbId()" + dto.getDbId());
												%>
												<option value="<%=dto.getDbId()%>_<%=dto.getDbName()%>"><%=dto.getDbDesc()%></option>
												<%
													}
												%>
												
										</select>
										</td>
									</tr>
								</table>
								<%
									} else if (dbindexList.size() != 0) {
								%>
								<table>
									<tr>
										<td>
											<table>
												<tr>
													<td width="100%"><B>Table Name : <%=tableName1%></B></td>
												</tr>
												<tr>
													<td><input type="button" value="Insert Column" id="InsertColumn" /></td>
													<!-- 	onclick="addColumn();" /></td> -->
												</tr>
											</table>
											<table>
												<tr>
													<td width="200"><B>Column Name</B></td>
													<td width="200"><B>Column Desc</B></td>
													<td width="200"><B>Data Type </B></td>
													<td width="200"><B>Width </B></td>
													<td width="200"><B>Allow Nulls</B></td>
												</tr>
												<%
													int i = 1;
														for (String idxName : dbindexList) {
												%>
												<tr>
													<td width="200"><select name='columnName<%=i%>'
														id='columnName<%=i%>'>
															<option value=""></option>
															<%
																for (String idxName1 : dbindexList) {
															%>
															<option value="<%=idxName1%>"><%=idxName1%></option>
															<%
																}
															%>
													</select>
													</td>
													<td width="200"><input width='300' name='columnDesc<%=i%>'
														id='columnDesc<%=i%>' type='text' /></td>
													<td width="200"><select name='datatype<%=i%>'
														id='datatype<%=i%>'>
															<option value=""></option>
															<%
																for (String type : dbdataTypeMap.keySet()) {
															%>
															<option value="<%=type%>"><%=type%></option>
															<%
																}
															%>
													</select>
													</td>
													<td width="200"><input width='200' name='width<%=i%>'
														id='width<%=i%>' type='text' /></td>
													<td width="200"><input width='200'
														name='constraint<%=i%>' id='constraint<%=i%>'
														type="checkbox" /></td>
													<td width="200"><p width='200'>   </p></td>
												</tr>
												<%
													i++;
														}
												%>
												</table>
												<table id="columns"></table>
																																	
										</td>
									</tr>
									<tr>
									</tr>
									<tr>
										<td width="200"><input type="submit" value="Create Table" />
										</td>
									</tr>
								</table>
								<%
									} else {
										message = "Not valid Database!!No Indexes!!";
									}
								%>
							</form></td>
					</tr>
				</table>
			</div>
		</div>
		</div>
</body>
</html>