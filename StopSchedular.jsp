<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    
<s:include value="adminHeader.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxtabs.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<title>Insert title here</title>

<script language="javascript" type="text/javascript">
function submitForm(button)
{
	document.getElementById("archsubmitform").action = "StartMonitorAction.action";
	document.getElementById("archsubmitform").submit;

}
function submitstopForm(button)
{
	document.getElementById("archsubmitform").action = "StopMonitorAction.action";
	document.getElementById("archsubmitform").submit;

}
</script>
</head>
<body>
<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">


<jsp:include page="SystemCommonTabs.jsp" />

<div id="countrydivcontainer1" style="border:1px solid gray; width:1500px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">

<jsp:include page="ServerCommonTabs.jsp" />

<div id="countrydivcontainer2" style="border:1px solid gray; width:1460px; height:520px; margin-bottom: 1em; padding: 10px; overflow:auto">

<form id="archsubmitform" name="archsubmitform">	
<table width="100%" height="500" valign="top" align="center">
<tr><td valign="top">
<table width="100%" valign="top">

<tr >
<td align="left" colspan=2>
<b>Start Monitor</b>
</td>
</tr>



<tr align="center">
<td align="center" colspan="2">
<s:property value="message" />
</td>
</tr>

<tr>
<td align="center" colspan="2">

<input type="submit" name="button" value="Start Monitor"   onclick="return submitForm(this);"/>
<input type="submit" name="button" value="Stop Monitor"   onclick="return submitstopForm(this);"/>

</td>

</tr>
</table>
</td>

</tr>
</table>
</form>
</div>
</div>
</div>

</body>
</html>