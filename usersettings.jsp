<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dto.DataDTO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>e2 Vault Administration</title>
<!-- calendar stylesheet -->
  <link rel="stylesheet" type="text/css" media="all" href="styles/calendar-win2k-cold-1.css" title="win2k-cold-1" />

  <!-- main calendar program -->
  <script type="text/javascript" src="js/calendar.js"></script>

  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar-setup.js"></script>
<script type="text/javascript">
function moveoutid()
{
	var userTab = document.getElementById("userTable");
	//alert(document.csrGrps);
	var userRowCount = userTab.rows.length;
	var seluserTab = document.getElementById("selUserTable");
	var seluserRowCount = seluserTab.rows.length;
	 for(var i=0; i<userRowCount; i++) {
                var row1 = userTab.rows[i];
                var chkbox = row1.cells[0].childNodes[0];
				 var text = row1.cells[1].innerHTML;
                if(null != chkbox && true == chkbox.checked) {
				
					var row = seluserTab.insertRow(seluserRowCount);
					var cell1 = row.insertCell(0);
					
					var element1 = document.createElement("input");
					element1.type = "checkbox";
					element1.value = chkbox.value;
					element1.id = "selCsrUsers";
					cell1.appendChild(element1);
					var cell2 = row.insertCell(1);
					cell2.innerHTML = text;
				}
	 }
	
	
}
function checkValues(button)
{
	var roleId = document.getElementsByName("roleId");

var active = document.getElementsByName("active");

var accessfromDays = document.getElementById("accessfromDays");
var accesstoDays = document.getElementById("accesstoDays");
var selected = false;
for(var v=0;v<roleId.length;v++)
	{
	
		if(roleId[v].checked == true)
		{
			
		selected = true;
			
		}
		if(roleId[v].checked == "undefined")
		{
		selected = true;
		}
	}
	if(!selected)
	{
		alert("Please select atleast one Role");
		return false;
	}
	
	if(document.getElementById("accessdateFrom").value=="")
	{
alert("Please select Access Date From");
		return false;
	}
	if(document.getElementById("accessdateTo").value =="")
	{
alert("Please select Access Date To");
		return false;
	}
	selected = false;
	for(var v=0;v<active.length;v++)
	{
	
		if(active[v].checked == true)
		{
		
			selected = true;
		}
		
	}
	if(!selected)
	{
		alert("Please select User Actions");
		return false;
	}
	document.getElementById("dbsubmitform").action = "AdminApplyAction.action";
	document.getElementById("dbsubmitform").submit;
	return true;
}
function selAllUsers(group)
{
	var users = document.getElementsByName("csrUsers");
	
	for(var i=0;i<users.length;i++)
	{
		var user = users[i].value;
		var arrays = user.split(",");
		if(arrays[0] == group.value)
		{
			users[i].checked = true;
		}
	}
}
function moveinid()
{
	var sda = document.getElementById('userTable');
	
	var len = sda1.length;
	for(var j=0; j<len; j++)
	{
		if(sda1[j].selected)
		{
			var tmp = sda1.options[j].text;
			var tmp1 = sda1.options[j].value;
			sda1.remove(j);
			j--;
			var y=document.createElement('option');
			y.text=tmp;
			y.value=tmp1;
			try
			{
			sda.add(y,null);}
			catch(ex){
			sda.add(y);	
			}

		}
	}	
}
/*window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"inputField",
			dateFormat:"%d-%m-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1
		});
	};
	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
	*/

</script>
</head>
<body>
<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">


<jsp:include page="useradminCommonTabs.jsp" />

<div id="countrydivsubcontainer1" style="border:1px solid gray; width:1500px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">
<jsp:include page="accesscontrolCommonTabs.jsp" />

<div id="countrydivsubcontainer2" style="border:1px solid gray; width:1460px; height:520px; margin-bottom: 1em; padding: 10px; overflow:auto">

	<s:form action="AdminApplyAction">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
	<table align="left" >
	<tr><td colspan="4">

				<%
		List<String> csrAllUsers = (ArrayList<String>)request.getAttribute("csrAllUsers");
		
		Iterator<String> itr = csrAllUsers.iterator();
		%>
				
			
			 <table id="userTable">  <tr>
			<%
				String grpName = "";
				String grpName1 = "";
				String userName = "";
				int it = csrAllUsers.size();
				int f = 0;
				Long userids[] = new Long[csrAllUsers.size()];
		while (itr.hasNext()) {
			String users = itr.next();
			String[] splitUsers = users.split(",");
			grpName = splitUsers[0];
			String[] full = splitUsers[1].split(";");
			userName = full[0];
			userids[f] = Long.parseLong(full[1]);
			
	%><input type="hidden" name="newUserId" id="newUserId" value="<%=userids[f]%>"/>
		<%if(!grpName.equalsIgnoreCase(grpName1))
			{%><td><b>Group:</b><%= grpName %>&nbsp;<b>User Name:</b></td>
			<% }%>
	
		<td><%=userName%>&nbsp;</td>			
			
	<% f++; grpName1 = grpName; } %>
	</tr>
				</table>
			 </td>
			 </tr>
		<tr>
			<td>
			
				</td>
			<td>
				<b>Roles</b>
			</td>
			
			<td>
				
			</td>
			<td>
				<b>User Actions</b>
			</td>
		</tr>
	
		<tr><td>
		<input type="hidden" name="csrAllUsers" id="csrAllUsers" value="<%= csrAllUsers %>"/>
		</td>
		</tr>
		
			 <tr>
			 <td>
			
				</td>
			 <td>
			 <%
		List csrRoles = request.getAttribute("csrRoles")!= null?(ArrayList)request.getAttribute("csrRoles"):new ArrayList();			
		Iterator itrer = csrRoles.iterator();
		%>
		
	<div id="div1" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		<%
		while (itrer.hasNext()) {
			UserDTO cDao = (UserDTO)itrer.next();
			System.out.println("role"+cDao.getRoleId());
	%>
			
			
			 <tr><td><input type="radio" name="roleId" value="<%= cDao.getRoleId() %>" onclick="checkGroup(this);"><%= cDao.getRoleName() %></input>	
				</td>
			</tr>
	<%} %>
	
				</table></div>
</td>
			 
				<td></td>
				<td>
				<div id="div2" style="overflow:auto; width:200px;border-top: solid 1px #C1C1C1;
  border-bottom: solid 1px #C1C1C1;
  border-left: solid 2px #C1C1C1;
  border-right: solid 2px #C1C1C1;height:90px;"><table> 
		
			
			
			 <tr><td><input type="radio" name="active" id="active" value="Active"  onclick="checkGroup(this);"/>Active	
				
	</td></tr>
	<tr><td><input type="radio" name="active" id="active" value="Inactive"  onclick="checkGroup(this);"/>InActive	
				
	</td></tr>
	<tr><td><input type="radio" name="active" id="active" value="Disabled"  onclick="checkGroup(this);"/>Disabled	
				
	</td></tr>
	<tr><td><input type="radio" name="active" id="active" value="Auto Disabled"  onclick="checkGroup(this);"/>Auto Disabled	
				
	</td></tr>
	<tr><td><input type="radio" name="active" id="active" value="Mark for Deletion"  onclick="checkGroup(this);"/>Mark for Deletion	
				
	</td></tr>
				</table></div>
				</td>
			</tr>
			<tr>
			<td colspan="5">
					
	
		<table align="center">
		 <tr><td>Access Date From:</td><td><input type="text" name="accessdateFrom" id="accessdateFrom"  onclick="checkGroup(this);"/>
			
				
	</td><td>To:
			
				
	</td><td><input type="text" name="accessdateTo" id="accessdateTo" />
			
				
	</td></tr>
	
	<tr><td>Access Days From:</td><td><select name="accessfromDays" id="accessfromDays" onclick="checkGroup(this);">
	<option value="1">Monday</option>
	<option value="2">Tuesday</option>
	<option value="3">Wednesday</option>
	<option value="4">Thursday</option>
	<option value="5">Friday</option>
	<option value="6">Saturday</option>
	<option value="7">Sunday</option>
	</select>
				
	</td><td>to:</td>
	<td><select name="accesstoDays" id="accesstoDays" onclick="checkGroup(this);">
	<option value="1">Monday</option>
	<option value="2">Tuesday</option>
	<option value="3">Wednesday</option>
	<option value="4">Thursday</option>
	<option value="5">Friday</option>
	<option value="6">Saturday</option>
	<option value="7">Sunday</option>
	</select>
				
	</td></tr>
	
	
				</table>
				</td>
			</tr>

	<tr>
		<td colspan="5"><input type="Submit" value="Apply" onclick="return checkValues(this);"></td>
	</tr>	
			
	</table>
	
	</s:form>
	<script type="text/javascript">
  /*  Calendar.setup({
        inputField     :    "validdateFrom",      // id of the input field
        ifFormat       :    "%m/%d/%Y %I:%M %p",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	 Calendar.setup({
        inputField     :    "validdateTo",      // id of the input field
        ifFormat       :    "%m/%d/%Y %I:%M %p",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });*/
	 Calendar.setup({
        inputField     :    "accessdateFrom",      // id of the input field
        ifFormat       :    "%d-%m-%Y %H:%M:%S",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	 Calendar.setup({
        inputField     :    "accessdateTo",      // id of the input field
        ifFormat       :    "%d-%m-%Y %H:%M:%S",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
	

</script>
		</div>
		</div>
		</div>
		
		
</body>
</html>