<script type="text/javascript">
	refreshData = function() {
		// define a callback function for row data
		var rowsCallback = function(bean) {
			var yourTable = dojo.widget.byId('yourTableId');
			yourTable.store.setData(bean);
		};
		var yourTable = dojo.widget.byId('yourTableId');
		// construct filter based on current FilteringTable sort information
		var sortInfo = yourTable.sortInformation[0];
		var filter = {field:yourTable.columns[sortInfo.index].field,direction:sortInfo.direction,page:currentPage};
		// add criteria to filter, dynamically construct your own
		filter['criteria'] = {firstName:document.getElementById('filterFirstName').value,secondName:document.getElementById('filterSecondName').value};
		var deferred = service.getRows(filter);
		// start the RPC process
		deferred.addCallback(rowsCallback);

		var rowNumCallback = function(rowNumber) {
			var totalPages = Math.ceil(rowNumber / 10);
			// call your fillPagination function rendering pagination info
			fillPagination(totalPages);
		}
		// we don't need sort and page info to count rows, just filtering criteria
		filter = {field:'',direction:1,page:0};
		filter['criteria'] = {firstName:document.getElementById('filterFirstName').value,secondName:document.getElementById('filterSecondName').value};
		var deferred = service.getRowCount(filter);
		// start the RPC process
		deferred.addCallback(rowNumCallback);
	}

	fillPagination = function(totalPages) {
		var el = document.getElementById('testTablePagination');
		el.innerHTML = 'Pages:&nbsp;';
		for (var i = 0; i < totalPages; i++) {
			if (currentPage == i) {
				el.innerHTML += '<b>' + (i + 1) + '</b>&nbsp;';
			} else {
				el.innerHTML += '<a href="#" onclick="currentPage=' + i + '; refreshData();">' + (i + 1) + '</a>&nbsp;';
			}
		}
	}
</script>