<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dto.UserDTO" %>
<s:include value="adminHeader.jsp" />
  <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	
	<script src="js/jquery_treeview.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="js/demo.js"></script>
<title>e2 Vault Administration</title>
<%
String host = (String)request.getAttribute("host");
List<String> dbList = (ArrayList<String>)request.getAttribute("dbList");
String dbName = request.getAttribute("dbName")!= null?request.getAttribute("dbName").toString():"";
List<String> prePopulateList = request.getAttribute("prePopulateList")!=null?(ArrayList<String>)request.getAttribute("prePopulateList"): new ArrayList<String>();
UserDTO dto = new UserDTO();
Map<String,Long> detailIndex = request.getAttribute("detailIndex")!=null?(HashMap<String,Long>)request.getAttribute("detailIndex"):new HashMap<String,Long>();
if(request.getAttribute("detailIndex")!=null){
	Long[] idxids= new Long[detailIndex.size()];
	int j=0;
	for(String key:detailIndex.keySet()){
		idxids[j] = detailIndex.get(key);
		j++;
	}
}

%>
<script>
function ValidateForm(){
	var aInputs = document.getElementsByTagName('input');
	var count = 0;
	for (var i=0;i<aInputs.length;i++) {
		var type = aInputs[i].type;
		var id = aInputs[i].id;
		if(type != "hidden"){
			if (id.match("Primary")){
				if(aInputs[i].checked){
					count++;
				}
			}
			
		}
		
	}
	if(count>1){
		alert("There can be only one Primary Index!!!");
		return false;
	}else if(count==0){
		alert("Please select Primary Index!!!");
		return false;
	}
	
	else
		return true;
	
}
function toggle(source) {
	var aInputs = document.getElementsByTagName('input');
    for (var i=0;i<aInputs.length;i++) {
    	 if (aInputs[i] != source && aInputs[i].className == source.className) {
        	if(source.checked == true){
        		aInputs[i].disabled = false;
        	}
        	else
        		aInputs[i].disabled = true;
        		aInputs[i].checked = false;
        }
    }
}

 
  </script>
  <title>tables</title>
  <style type="text/css">
  #playlist tr.header {
      background-color:#0072a8;
    }
    #playlist tr.even td {
      background-color: #eee;
    }
    #playlist tr.odd  td {
      background-color: #fff;
    }
    #navigation a.active,a.visited, a.hover {
	color: black;
	background-color: transparent;
}
  </style>
</head>

<body>
<jsp:include page="CommonTabs.jsp" />

<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">


<jsp:include page="SystemCommonTabs.jsp" />

<div id="countrydivcontainer1" style="border:1px solid gray; width:1500px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">

<jsp:include page="DatabaseCommonTabs.jsp" />

<div id="countrydivcontainer1" style="border:1px solid gray; width:1500px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">
<table width="100%" border="1" valign="top" height="450">
<tr>
<td width="20%"  valign="top">
<%
if(host != null)

{ %>
<ul id="browser" class="filetree treeview"><li><%= host%><ul>

			<%
			for(Iterator iter = dbList.iterator();iter.hasNext();)
			{
			UserDTO uDTO = (UserDTO)iter.next();
				System.out.println("dbname...."+uDTO.getDbName());%>
			<li><% if(dbName != "" && dbName.equals(uDTO.getDbName())) {%>
			<a href="CreateCrossRef.action?dbName=<%=uDTO.getDbName()%>" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300"><%=uDTO.getDbDesc()%></font></a>
			<%} 
				else 
				{%>
				<a href="CreateCrossRef.action?dbName=<%=uDTO.getDbName()%>"><%=uDTO.getDbDesc()%></a>
				<%}%></li>
			<% } %>
			</ul></li></ul>
<%}%>
</td>
<td width="80%"  valign="top">
<table border="0" align="center" width="100%">	
<tr ><td align="left"><b>Cross Ref Index Control:</b></td>
</tr>
<%
int i=1;	
if(dbName != "")
{ %>
		<tr ><td align="left"><b>Database Name:<%=dbName %></b>	</td></tr>
		<tr><td>
		<center><s:if test="hasActionErrors()">

      
  <s:actionerror />
 
 
</s:if></center>
			<s:form action="dbProfileIndexAction">	
			
			
<table border="0" align="center" width="100%">	
<tr><td><input type="hidden" name="dbName" id="dbName" value="<%=dbName%>"/></td>
    </tr>
    <%if(detailIndex.size()==0){%>
       	<table align="center"><tr ><td align="center">
		<font color="red">No Indexes configured for the selected database in VaultApp</font>
		</td></tr></table>
	<%}else{%>
    
    
		<tr>
			<td>
			<table>
			<tr><td><b>ID</b></td><td></td><td><b>Index</b></td><td><b>Primary</b></td><td></td><td><b>Secondary</b></td>
			<%for(String key:detailIndex.keySet()) { 
				if (prePopulateList != null && prePopulateList.contains("Index_"+detailIndex.get(key))) {%>
					<tr><td><%=(i++)%></td><td></td>
					    <td><input type="checkbox" name="Index_<%=detailIndex.get(key)%>" id="Index_<%=detailIndex.get(key)%>" value="Index_<%=detailIndex.get(key)%>" class="<%=detailIndex.get(key)%>" onclick="toggle(this)" checked/><%=key%>
							<input type="hidden" name="Index_<%=detailIndex.get(key)%>" id="Index_<%=detailIndex.get(key)%>" value="unchecked" />
						</td>
					<%if (prePopulateList != null && prePopulateList.contains("Primary_"+detailIndex.get(key))) {%>	
						<td><input type="checkbox" name="Primary_<%=detailIndex.get(key)%>" id="Primary_<%=detailIndex.get(key)%>" value="Primary_<%=detailIndex.get(key)%>" class="<%=detailIndex.get(key)%>" checked/>
							<input type="hidden" name="Primary_<%=detailIndex.get(key)%>" id="Primary_<%=detailIndex.get(key)%>" value="unchecked" />
						</td>
					<%}else{%>
						<td><input type="checkbox" name="Primary_<%=detailIndex.get(key)%>" id="Primary_<%=detailIndex.get(key)%>" value="Primary_<%=detailIndex.get(key)%>" class="<%=detailIndex.get(key)%>" />
							<input type="hidden" name="Primary_<%=detailIndex.get(key)%>" id="Primary_<%=detailIndex.get(key)%>" value="unchecked" />
						</td>
					<%}%>
					<td></td>
					<%if (prePopulateList != null && prePopulateList.contains("Secondary_"+detailIndex.get(key))) {%>	
						<td><input type="checkbox" name="Secondary_<%=detailIndex.get(key)%>" id="Secondary_<%=detailIndex.get(key)%>" value="Secondary_<%=detailIndex.get(key)%>" class="<%=detailIndex.get(key)%>" checked/>
							<input type="hidden" name="Secondary_<%=detailIndex.get(key)%>" id="Secondary_<%=detailIndex.get(key)%>" value="unchecked" />
						</td>
					<%}else{%>
						<td><input type="checkbox" name="Secondary_<%=detailIndex.get(key)%>" id="Secondary_<%=detailIndex.get(key)%>" value="Secondary_<%=detailIndex.get(key)%>" class="<%=detailIndex.get(key)%>" />
							<input type="hidden" name="Secondary_<%=detailIndex.get(key)%>" id="Secondary_<%=detailIndex.get(key)%>" value="unchecked" />
						</td>
					<%}%>
					</tr>
				<%}else{%>
					<tr><td><%=(i++)%></td><td></td>
					    <td><input type="checkbox" name="Index_<%=detailIndex.get(key)%>" id="Index_<%=detailIndex.get(key)%>" value="Index_<%=detailIndex.get(key)%>" class="<%=detailIndex.get(key)%>" onclick="toggle(this)"/><%=key%>
							<input type="hidden" name="Index_<%=detailIndex.get(key)%>" id="Index_<%=detailIndex.get(key)%>" value="unchecked" />
						</td>
						<td><input type="checkbox" name="Primary_<%=detailIndex.get(key)%>" id="Primary_<%=detailIndex.get(key)%>" value="Primary_<%=detailIndex.get(key)%>" class ="<%=detailIndex.get(key)%>" disabled />
							<input type="hidden" name="Primary_<%=detailIndex.get(key)%>" id="Primary_<%=detailIndex.get(key)%>" value="unchecked" />
						</td>
						<td></td>
						<td><input type="checkbox" name="Secondary_<%=detailIndex.get(key)%>" id="Secondary_<%=detailIndex.get(key)%>" value="Secondary_<%=detailIndex.get(key)%>" class="<%=detailIndex.get(key)%>" disabled/>
							<input type="hidden" name="Secondary_<%=detailIndex.get(key)%>" id="Secondary_<%=detailIndex.get(key)%>" value="unchecked" />
						</td>
					</tr>
					
				<%}
			}%>
			</table>
			<tr>
			<td align="left">
			<div id="mes"></div>
			<%if(request.getAttribute("message")!= null)
				{ String message = (String)request.getAttribute("message"); %><font color="red"><%= message%></font><%}%></td>
				
				</tr>
			<tr>
					<td colspan="<%=i%>" align="left" valign="center"><input type="Submit" value="Apply" onclick="return ValidateForm()"/>&nbsp;&nbsp;<input type="Submit" value="Cancel"></td>
					</tr>
			<%} %>
			
			</td>
			</tr>
	
		
	</table>
	</s:form>
	<%}
	else
	{%>
	<table align="center"><tr ><td align="center">
		<font color="red">Select the Database</font>
		</td>
</tr>

</table>
	<%}%>
	</td>
</tr>

</table>
</td>
</tr>

</table>
</div></div></div>

</body>
</html>