<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
		<display:table name="populateReportList" class="dataTable" id="Accs" pagesize="10" requestURI="/AdminSearchAction.action">
		<display:setProperty name="paging.banner.onepage" value=" " />
			<display:column property="loginGroup" sortable="true" title="Group Name"/>
		 <display:column property="loginName" sortable="true" title="User Name"/>
		  <display:column property="loginDate" sortable="true" title="Login Date"/>
		   <display:column property="searchValue" sortable="true" title="Search"/>
		    <display:column property="resCount" sortable="true" title="No.of Results"/>
			 <display:column property="searchDate" sortable="true" title="Search Date"/>
				</display:table>
</body>
</html>