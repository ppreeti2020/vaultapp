<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.pb.common.CommonConstants"%>
<%@page import="com.pb.common.PropertyUtils" %>
<%@page import="java.util.Properties" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
String server = request.getAttribute("server") != null?request.getAttribute("server").toString():"";
System.out.println("server:"+server);

String filePath1 = CommonConstants.CSR_PROPERTY_FILE_NAME;
Properties properties1 = PropertyUtils.getProperties(filePath1);
String vault_Server2 = properties1.getProperty(CommonConstants.VAULT_SERVER2).toString();
%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxtabs.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script>
/*onload = function ()
{
	//alert('<%= server%>');
	if('<%= server%>' == "server2")
		{
			document.getElementById("ser1").innerHTML = '<a href="VaultAction.action?server=server1" rel="countrycontainer2">Vault Server1 Settings</a>';
			document.getElementById("ser2").innerHTML = '<a href="VaultAction.action?server=server2" rel="#default" class="selected">Vault Server2 Settings</a>';
		}
	else
		{
		document.getElementById("ser1").innerHTML = '<a href="VaultAction.action?server=server1" rel="#default" class="selected">Vault Server1 Settings</a>';
		document.getElementById("ser2").innerHTML = '<a href="VaultAction.action?server=server2" rel="countrycontainer2">Vault Server2 Settings</a>';

		}
}*/
</script>
<title>Insert title here</title>
</head>
<body >
<ul id="countrytabs2" class="shadetabs">

<%if(server.equals("server2"))
		{%>
			<li id="ser1"><a href="VaultAction.action?server=server1" rel="countrycontainer2">Vault Server Settings</a></li>
			<% if(vault_Server2.equalsIgnoreCase("yes")){%>
			<li id="ser1"><a href="VaultAction.action?server=server2" rel="#default" class="selected">Vault Server2 Settings</a></li>
			<%} %>
				<li id="ser1"><a href="VaultAction.action?server=schedular" rel="countrycontainer2">Scheduler Monitoring</a></li>
		<%}
	else if(server.equals("server1"))
		{%>
		<li id="ser1"><a href="VaultAction.action?server=server1" rel="#default" class="selected">Vault Server Settings</a></li>
		<% if(vault_Server2.equalsIgnoreCase("yes")){%>
		<li id="ser1"><a href="VaultAction.action?server=server2" rel="countrycontainer2">Vault Server2 Settings</a></li>
		<%} %>
			<li id="ser1"><a href="VaultAction.action?server=schedular" rel="countrycontainer2">Scheduler Monitoring</a></li>

		<%}
	else if(server.equals("schedular"))
	{%>
	<li id="ser1"><a href="VaultAction.action?server=server1"  rel="countrycontainer2">Vault Server Settings</a></li>
	<% if(vault_Server2.equalsIgnoreCase("yes")){%>
	<li id="ser1"><a href="VaultAction.action?server=server2" rel="countrycontainer2">Vault Server2 Settings</a></li>
	<%} %>
	<li id="ser1"><a href="VaultAction.action?server=schedular" rel="#default" class="selected">Scheduler Monitoring</a></li>

	<%}
	
		
		%>

</ul>
<script type="text/javascript">

var countries=new ddajaxtabs("countrytabs2", "countrydivcontainer2");
countries.setpersist(false);
countries.setselectedClassTarget("link"); //"link" or "linkparent"
countries.init();

</script>
</body>
</html>
