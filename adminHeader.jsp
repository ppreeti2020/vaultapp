<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.SQLException" %>
<%@page import="com.pb.dao.DBConnectionDAO" %>
<%@page import="com.pb.dao.CommonDAO" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Collections" %>
<!DOCTYPE html PUBLIC 
	"-//W3C//DTD XHTML 1.1 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="en" lang="en">
<head>
	<title>Vault  Application
</title>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/table-layout.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />


<script language='JavaScript'>
window.onerror=function(){window.status='';return true;};
</script>


</head>
<!-- Nithu Alexander: 06 Jul 2015
AVA-10: Fix for Cross-site framing  --> 
<%response.addHeader("X-Frame-Options","SAMEORIGIN");%>

<body  onunload="">
<% String tabId = request.getAttribute("tabId")!=null?request.getAttribute("tabId").toString():"";
String reports = session.getAttribute("reports")!=null?session.getAttribute("reports").toString():"";
String useraccesscontrol = session.getAttribute("useraccesscontrol")!=null?session.getAttribute("useraccesscontrol").toString():"";
String assignUsersToProfile = session.getAttribute("assignUsersToProfile")!=null?session.getAttribute("assignUsersToProfile").toString():"";
String search = session.getAttribute("search")!=null?session.getAttribute("search").toString():"";
String repository = session.getAttribute("repository")!=null?session.getAttribute("repository").toString():"";
System.out.println("access"+useraccesscontrol);
String act=request.getParameter("actType");
%>
<div id="page" >
	<!-- Footer -->
	<div id="footer" class="clearfix"></div>
<div id="header" class="clearfix">
	<div id="branding">
		<img id="logo" src="images/logonew.jpg" />
	</div>
	<div id="applicationDetail">
	<%
		String userName = "";
		   String LastLoginDate = "";
		   ArrayList<String> DateList = new ArrayList<String>();
			if(session.getAttribute("UserName") != null)
			{
		userName = session.getAttribute("UserName").toString();
		DBConnectionDAO dao = new DBConnectionDAO();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = dao.getDBConnection();
		try {
			System.out.println("getting Last login details:::");

			String success = comDao.CheckEncryptData("Y");
			System.out.println("successis"+success);
			String userName1 = comDao.CheckEncryptData(userName);
			System.out.println("Username1 is"+userName1+"=="+conn);
			// ,SUCCESS_FLAG,HASH_ID
			String query = "select Login_Date from LOGIN_DETAILS where SUCCESS_FLAG = ? and Login_Name = ?";
			System.out.println("query is"+query);
			ps = conn.prepareStatement(query);
			ps.setString(1, success);
			ps.setString(2, userName1);
			System.out.println("selecting record");
			ResultSet rs = ps.executeQuery();
			if (!rs.next() ) {
			    System.out.println("no data");
			    LastLoginDate = "";
			} else {
					while (rs.next()) {
						String s = rs.getString(1);
						//System.out.println("Before encryption"+s);
						String e = comDao.CheckDecryptData(s);
						//System.out.println("After encryption"+e);
						DateList.add(e == null ? "" : e);
					}
					System.out.println("Login Dtls Size:::" + DateList.size() + "Last Login Date:::"
							+ DateList.get(DateList.size() - 1));
					//Collections.sort(DateList);
					LastLoginDate = DateList.get(DateList.size() - 1);
				}
				
				System.out.println("LastLoginDate:::" + LastLoginDate);

			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			} finally {
				if(ps != null){
				ps.close();
				}
				if(conn != null){
				conn.close();
				}

			}
			if (act != null && ((act.equalsIgnoreCase("view")) || (act.equalsIgnoreCase("download"))
					|| (act.equalsIgnoreCase("print")) || (act.equalsIgnoreCase("reprint")))) {

			} else {
	%>
	
		<ul id="productName">
			<li>Vault App
			<% if(useraccesscontrol.equals("true"))
			{%>
				<a style="color:white; background-color: transparent;" id="mainLinks" href="UsersListAction.action">Home</a>
			<%}
			else if(assignUsersToProfile.equals("true"))
			{%>
				<a style="color:white; background-color: transparent;" id="mainLinks" href="ProfileListAction.action">Home</a>
			<%}
			else if(search.equals("true"))
			{%>
				<a style="color:white; background-color: transparent;" id="mainLinks" href="SearchAccountNew.action">Home</a>
			<%}
			else if(repository.equals("true"))
			{%>
				<a style="color:white; background-color: transparent;" id="mainLinks" href="DocManagerNew1.action">Home</a>
			<%} if(act!=null&&act.equalsIgnoreCase("view")){}else {%>
			<a id="mainLinks" style="color:white; background-color: transparent;" href="Logout.action">Logout</a>
			
<%}%>
</li>
		</ul>

<%}  if(act!=null&&act.equalsIgnoreCase("view")){} else{%>
		<ul id="userDetail">	
		<li>Welcome <%= userName%> </li>
		</ul>
		<ul id="userDetail">
		 <li>Last Login: <%= LastLoginDate%></li>
		</ul><%}%>
				   
		<%}else{
		%>
		  out.println("<script>parent.location.href='Logout.action'</script>");
				
		<%} %>
		
	</div>
<%if(act!=null&&act.equalsIgnoreCase("view")){}else{%>
	<div id="curve">
		<img id="curve" src="styles/themes/purple/images/curve.gif" />
	</div>
<%}%>
	<hr />
</div>
	<!-- Main Content -->
	<div id="content">
		<div id="main"> 

	