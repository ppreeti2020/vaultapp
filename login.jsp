<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">


<head>

<title>User Login |  Vault Application
</title>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
</head>
<%response.addHeader("X-Frame-Options","SAMEORIGIN");%>

<script language="javascript" type="text/javascript">
window.history.forward();
function noBack()
{
	window.history.forward();
}

</script>

<body onload="noBack();" onpageshow="if(event.persisted)noBack();" onunload="">
	<div id="page">
	<!-- Footer -->
		<div id="footer" class="clearfix"></div>
	<!-- HEADER -->
		<div id="header" class="clearfix">
		<div id="branding" >
			<img id="logo" src="images/logonew.jpg" />
		</div>
		<div id="applicationDetail">
			<ul id="productName">
				<li></li><li></li><li></li><li></li>
				<li>     Vault App   </li>
			</ul>
			<ul id="userDetail"></ul>
			<ul id="mainLinks">
				<!--<li><a style="color:white; background-color: transparent;" href="#">Help</a></li>-->
			</ul>
		</div>
		<div id="curve">
			<img id="curve" src="styles/themes/purple/images/curve.gif" />
		</div>
		<hr />
	</div>
	<!-- 2em margin here  -->
	<center>
	<h1>
	 <s:if test="hasActionErrors()"> <s:actionerror /></s:if>
	</h1>
	<font color="#ff0000"><s:property value="message" /></font></center><br>
	<!-- Main Content -->	
	<center>									 
		<s:form action="Login" autocomplete="off" >
				
				<table >
					<tr><td></td><td >User Name:

					<s:textfield name="userName" />	</td></tr>											
			
					<tr><td></td><td >Password:
					
					<!-- Nithu Alexander: 13 Oct 2015 Adding class="passwordreveal" for AVA-8.5 bug fix -->
					<s:password name="userPwd" class="passwordreveal"/>																											
					</td></tr>
						<tr><td></td><td>
	  						<s:submit style="margin-right:40px"/></td><td></td></tr>
				</table>
		</s:form>
	</center>
	</div>
	
</body>
</html>