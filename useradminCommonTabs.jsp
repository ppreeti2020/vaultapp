<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
String subTabId = request.getAttribute("subTabId") != null?request.getAttribute("subTabId").toString():"";
System.out.println("subTabId:"+subTabId);
%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxsubtabs.js"></script>

<title>Insert title here</title>
</head>
<body >
<ul id="countrysubtabs" class="shadetabs">

	<%if(subTabId.equals("Access Control"))
			{%>
				<li><a href="UsersListAction.action" rel="#default" class="selected">Users List</a></li>
				<li><a href="AssignCreateGroupsForProfilesAction.action" rel="countrysubcontainer1">Profile - User Assignment</a></li>
			<%}
	else if(subTabId.equals("User Creation"))
			{%>
			<li><a href="UsersListAction.action" rel="countrysubcontainer1">Users List</a></li>
			<li><a href="AssignCreateGroupsForProfilesAction.action" rel="#default" class="selected">Profile - User Assignment</a></li>
			
	
		<%}%>

</ul>
<script type="text/javascript">

var countries=new ddajaxtabs("countrysubtabs", "countrydivsubcontainer");
countries.setpersist(true);
countries.setselectedClassTarget("link"); //"link" or "linkparent"
countries.init();

</script>
</body>
</html>
