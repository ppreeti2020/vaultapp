<%-- 
    Document   : DocumentView
    Created on : Mar 9, 2009, 12:40:01 PM
    Author     : ssahmed
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList"%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.pb.dto.UserDTO"%>
<%-- URL to render this current document but with some different
     parameters:

     - either at a different resolution
     - either at a different rotation
     - either a different page within the document
     - or with a "multiple pages" setting

     and so on. --%>
<%
	int pageN = request.getAttribute("pagenum") != null ? Integer
			.parseInt(request.getAttribute("pagenum").toString()) : 1;
	System.out.println("pageNo:" + pageN);

	String product = session.getAttribute("SELECT_DB").toString();
	String dbDesc = session.getAttribute("SELECT_DB_DESC").toString();
	String rootName = session.getAttribute("SELECT_ROOT").toString();
	// System.out.println("product:"+product);
	List actionList = request.getAttribute("actionList") != null
			? (ArrayList) request.getAttribute("actionList")
			: new ArrayList();
	String flagFav = request.getAttribute("flag") != null
			? (String) request.getAttribute("flag")
			: "";
	System.out.println("flagFav:" + flagFav);
	//String view = "";
	String print = "";
	String download = "";
	String search = session.getAttribute("search") != null ? session
			.getAttribute("search").toString() : "";
	String rep = session.getAttribute("rep") != null ? session
			.getAttribute("rep").toString() : "";
	String pageName = "search";
	String pageName1 = "fav";
	String pageType = session.getAttribute("pageType") != null
			? session.getAttribute("pageType").toString()
			: "";
	System.out.println("pageType:" + pageType);
	Long UserId = session.getAttribute("UserId") != null ? Long
			.parseLong(session.getAttribute("UserId").toString()) : 0L;
	System.out.println("userId:" + UserId);

	Enumeration params = request.getParameterNames();
	System.out.println("paramName:" + params);
	while (params.hasMoreElements()) {
		String paramName = params.nextElement().toString();
		System.out.println("paramName:" + paramName);
		System.out.println("paramName:value:"
				+ request.getParameter(paramName));

	}
%>
<%
	for (Iterator iter = actionList.iterator(); iter.hasNext();) {
		UserDTO dto = (UserDTO) iter.next();
%>
<%
	if (dto.getActionName().equalsIgnoreCase("Download")) {
			download = "true";
%>
<%
	}
		if (dto.getActionName().equalsIgnoreCase("Print")) {
			print = "true";
%>
<%
	}
%>
<%
	}
%>
<%
	if (pageType.equalsIgnoreCase(pageName + UserId)) {
%>

<c:url value="DocManager.action" var="ViewDocURL">
	<c:param name="guid" value="${param.guid}" />
	<c:param name="prID" value="${param.prID}" />
	<c:param name="docId" value="${param.docId}" />
	<c:param name="actType" value="view" />
	<c:param name="acct" value="${param.acct}" />
	<c:param name="db" value="${param.db}" />
	<c:param name="doc" value="${doc}" />
	<c:param name="pg" value="${pagenum}" />
	<c:param name="rot" value="${rot}" />
	<c:param name="res" value="${res}" />
	<c:param name="mult1" value="${mult1}" />
	<c:if test="${not empty param.par}">
		<c:param name="par" value='${param.par}' />
	</c:if>
	<c:param name="op" value="view" />
</c:url>
<%
	} else if (pageType.equalsIgnoreCase(pageName1 + UserId)) {
%>
<c:url value="DocManagerFav.action" var="ViewDocURL">
	<c:param name="guid" value="${param.guid}" />
	<c:param name="prID" value="${param.prID}" />
	<c:param name="docId" value="${param.docId}" />
	<c:param name="actType" value="view" />
	<c:param name="acct" value="${param.acct}" />
	<c:param name="db" value="${param.db}" />
	<c:param name="doc" value="${doc}" />
	<c:param name="pg" value="${pagenum}" />
	<c:param name="rot" value="${rot}" />
	<c:param name="res" value="${res}" />
	<c:param name="mult1" value="${mult1}" />
	<c:if test="${not empty param.par}">
		<c:param name="par" value='${param.par}' />
	</c:if>
	<c:param name="op" value="view" />
</c:url>
<%
	}
%>
<%-- URL for the Document rendering servlet that actually does the work
     of rendering the document and returning back the appropriate
     rendered image output. --%>
<c:url value="Render.do" var="renderURL">
	<c:param name="acct" value="${param.acct}" />
	<c:param name="db" value="${param.db}" />
	<c:param name="dh" value="${doc}" />
	<c:param name="pg" value="${pagenum}" />
	<c:param name="pagecnt" value="${pagecnt}" />
	<c:param name="par" value='${par}' />
	<c:param name="rot" value="${rot}" />
	<c:param name="res" value="${res}" />
	<c:if test="${not empty param.of}">
		<c:param name="of" value='${param.of}' />
	</c:if>
	<c:param name="docformat" value="${requestScope.docformat}" />
	<c:param name="doctype" value="${requestScope.doctype}" />
	<c:param name="imageoutput" value="${requestScope.imageoutput}" />
	<c:param name="pdfoutput" value="${requestScope.pdfoutput}" />
</c:url>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Database <c:out value="${param.db}" />: Account # <c:out
		value="${param.acct}" />, Document <c:out value="${doc.date}" />,
	Page <c:out value="${pagenum}" />
</title>
<link href="styles/common/main.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/common/messages.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet"
	type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css"
	type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css"
	media="all" />
<script src="js/demo1.js"></script>
<link href="styles/shCore.css" rel="stylesheet" type="text/css" />
<link href="styles/shThemeDefault.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="styles/demo.css" type="text/css"
	media="screen" />
<link rel="stylesheet" href="styles/flexslider.css" type="text/css"
	media="screen" />
<script src="js/modernizr.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery-1.7.1.min.js"></script>
<script defer src="js/jquery.flexslider.js"></script>

<script type="text/javascript" src="js/shCore.js"></script>
<script type="text/javascript" src="js/shBrushXml.js"></script>
<script type="text/javascript" src="js/shBrushJScript.js"></script>


<!-- Optional FlexSlider Additions -->

<script src="js/jquery.easing.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<!-- <script defer src="js/demo.js"></script> -->
<script type="text/javascript" src="js/jquery.rotel.js"></script>
<script language="javascript" type="text/javascript">
        
        $(function(){
           SyntaxHighlighter.all();
          });
        
        <c:if test="${!(requestScope.singlepagedoc) || !(requestScope.docformat == 'collection')}">
           alert("Hello 123!!!");
           $(window).load(function(){
        	   //var customSlider = new useCurSlideHere();     
        	   
        		<c:forEach var="i" begin="1" end="${pagecnt}">
        			<c:url value='${renderURL}' var="ViewDocMultiplePage">
        			<c:param name="pg" value="${i}"/></c:url>
        			$('#mainslides').append('<li><a href="<c:out value='${ViewDocMultiplePage}'/>"><img src="<c:out value='${ViewDocMultiplePage}'/>" alt="Document rendering" border="1"></a></li>');
        			<c:if test="${pagecnt > 1}">
        				$('#thumbslides').append('<li><img src="<c:out value='${ViewDocMultiplePage}'/>" alt="Document rendering" border="1"><p class="flex-caption">Page <c:out value='${i}'/></p></li>');
        			</c:if>
        		</c:forEach>
        	        	  
            $('#carousel').flexslider({
              animation: "slide",
              controlNav: false,
              animationLoop: false,
              slideshow: false,
              itemWidth: 100,
              itemMargin: 2,
              asNavFor: '#slider',
              maxItems: 3
              
            });
            
            $('#slider').flexslider({
              animation: "slide",
              controlNav: false,
              animationLoop: false,
              slideshow: false,
              sync: "#carousel",
              start: function(slider){
                  $('body').removeClass('loading');
                },
                after: function(slider) {
                	var customSlider = slider.currentSlide;
          	    }
            });
                   	
            
          });
           
        </c:if>
        function useCurSlideHere() {
        	alert("inside method");
        	alert("cur sld::"+$('#slider').data('flexslider.currentSlide'));
        	//alert("cur sld123::"+customSlider);
        	$('#slider').data('flexslider.currentSlide').rotBy(90);
        	/*if ( $('#slider').data('flexslider.currentSlide').getRotationDegrees()<90) {
        		$('#slider').data('flexslider.currentSlide').rotTo(90);
    		     //rotate the elements with class .element to 90 degrees
    		    } else {
    		    	$('#slider').data('flexslider.currentSlide').rotBy(90);
    		     //rotate the elements with class .element by 10 degrees
    		    } 
        	
        	
        	
        	
      	  this.curSlide = 0;

      	  this.setCurSlide = function(curSlide) {
      		alert("inside set method");
      	    this.curSlide = curSlide;
      	  }

      	  this.getCurSlide= function() {
      		alert("inside get method");
      		if ( $(this.curSlide).getRotationDegrees()<90) {
      			
      			
      		      $(this.curSlide).rotTo(90);
      		     //rotate the elements with class .element to 90 degrees
      		    } else {
      		     $(this.curSlide).find("img").rotBy(10);
      		     //rotate the elements with class .element by 10 degrees
      		    }    
      	    //alert(this.curSlide);
      	  }*/
      	}
             <%-- This function is invoked when a document date is selected
                 from the "Date" drop-down list. --%>
                     function changeDocument() {
                         document.forms["docDateForm"].submit();
                     }
            <%-- This function is invoked when a section is selected from
                 the "Section" drop-down list. --%>
                     function gotoSection() {
                         document.forms["docSectionForm"].submit();
                     }
                     function resizeIframe() {
                         if (document.getElementById("frameimage")) {
                            var height = document.documentElement.clientHeight;
                            height -= document.getElementById('frameimage').offsetTop;

                            // not sure how to get this dynamically
                            height -= 20; /* whatever you set your body bottom margin/padding to be */

                            document.getElementById('frameimage').style.height = height +"px";
                         }
                     }
                     if (document.getElementById("frameimage")) {
                        document.getElementById('frameimage').onload = resizeIframe;
                        window.onresize = resizeIframe;
                        window.onload = resizeIframe;
                     }
					 function submitFav(button)
{
	
var guids=document.getElementById("guid").value;
var prId=document.getElementById("prID").value;
var docId=document.getElementById("docId").value;
//alert(guids);
			 window.open('\createfav.jsp?prID='+prId+'&guid='+guids+'&docId='+docId+'&type=doc','IndexResults','width=300,height=250,toolbar=no,resizable=yes,menubar=no,scrollbars=yes')
	          //  document.getElementById("Display").submit();
				//return false;
}
        </script>
<script>
        function back()
        {
       // alert('${param.acct}');		
			//document.getElementById("backForm").action="${backURL}";
			//alert(document.getElementById("backForm").action);
			document.getElementById("backForm").submit;
			return true;
			
        }
        function submitpage(id)
		{
			//alert(document.pressed);
			if(document.pressed != null || document.pressed != "")
			{//gotoPage
				if(document.pressed != "gotoPage")
				{
					if(document.pressed == "Next")
					{
					document.getElementById("pg").value =id+1;
					}
					else
						if(document.pressed == "Previous")
					{
					document.getElementById("pg").value = id-1;
					}
				}
				//alert(document.getElementById("urlType").value);
					document.getElementById("urlType").value="interface";

var url = "<c:out value='${ViewDocURL}'/>";
alert(url);

			document.getElementById("docPageForm").action="DocManager.action";
			//alert(document.getElementById("docPageForm").action);
			document.getElementById("docPageForm").submit;
			return true;
			}
			else
			{
				return false;
			}
		}
        </script>
<script>
</script>
</head>

<body 	bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0"
	marginheight="0" link="#000000" vlink="#000000" alink="#000000">

	<input type='button' onClick='javascript:hideTable();'
		value='fullscreen'>
	<table width="60%" height="70%" cellpadding="0" cellspacing="0"
		valign="top"
		<tr bgcolor="#ffffff" valign="middle">
                <td align="center">
              
                            	<%if (download.equalsIgnoreCase("true")) {%>
                    <c:choose>
                        <c:when test="${requestScope.pdfoutput}">
                            <%-- If this document supports output to PDF format, then enable the two PDF output buttons --%>
                            <c:url value="${renderURL}" var="ViewDocAsPdfURL">
                                <c:param name="of"  value="PDF"/>
                            </c:url>
                            <c:url value="${renderURL}" var="ViewDocAsPdfNoBgURL">
                                <c:param name="of"  value="PDF"/>
                                <c:param name="par" value="NoBG"/>
                            </c:url>
                            <img width="35" height="33" src="images/cpdf.gif" onclick="window.open('<c:out value='${ViewDocAsPdfURL}'/>&act=savepdf','900','650')"     alt="View as PDF">
                            <img width="35" height="33" src="images/cpdfnobg.gif" onclick="window.open('<c:out value='${ViewDocAsPdfNoBgURL}'/>&act=savepdf','900','650')" alt="View as PDF without background">
                        </c:when>
                        <c:otherwise>
                            <img width="35" height="33" src="images/cpdf.gif"   alt="View as PDF" onclick="window.open('<c:out value='${renderURL}'/>','900','650')"     >
                            <img width="35" height="33" src="images/gpdfnobg.gif" alt="View as PDF without background">
                        </c:otherwise>
                    </c:choose>
					<%} else if (print.equalsIgnoreCase("true")) {%>
                    <c:choose>
                        <c:when test="${requestScope.textoutput}">
                            <%-- If this document supports output to TEXT format, then enable the "View as Text" button --%>
							
                            <c:url value="${renderURL}" var="ViewDocAsTextURL">
							<c:choose>
							
                        <c:when test="${(requestScope.doctype == 'djdeline')}">

                               <c:param name="of"  value="TEXT"/>
							   </c:when>
							    <c:otherwise>
                           <c:param name="of"  value="PDF"/>
                        </c:otherwise>
                    </c:choose>
                                <c:param name="par" value="NoBG"/>
                            </c:url>
                            <img width="35" height="33" src="images/cprint.gif" onclick="window.open('<c:out value='${ViewDocAsTextURL}'/>&akhmad=print','900','650')" alt="View as Text">
                        </c:when>
                        <c:otherwise>
                            <img width="35" height="33" src="images/cprint.gif" onclick="window.open('<c:out value='${renderURL}'/>&akhmad=print','900','650')" alt="View as Text">
                        </c:otherwise>
                    </c:choose>
					
					<%}
			if (download.equalsIgnoreCase("true")) {%>
					 <c:choose>
                        <c:when test="${requestScope.textoutput}">
                            <%-- If this document supports output to TEXT format, then enable the "View as Text" button --%>
							
                            <c:url value="${renderURL}" var="ViewDocAsTextURL">
							<c:choose>
							
                        <c:when test="${(requestScope.doctype == 'djdeline')}">

                               <c:param name="of"  value="TEXT"/>
							   </c:when>
							    <c:otherwise>
                           <c:param name="of"  value="PDF"/>
                        </c:otherwise>
                    </c:choose>
                                <c:param name="par" value="NoBG"/>
                            </c:url>
                            <img width="35" height="33" src="images/textimage.jpg" onclick="window.open('<c:out value='${ViewDocAsTextURL}'/>&act=saveText','900','650')" alt="View as Text1">
                        </c:when>
                        <c:otherwise>
                            <img width="35" height="33" src="images/textimage.jpg" onclick="window.open('<c:out value='${renderURL}'/>&act=saveText','900','650')" alt="View as Text">
                        </c:otherwise>
                    </c:choose>
					
                    <c:choose>
                        <c:when test="${requestScope.tiffoutput}">
                            <%-- If this document supports output to TIFF format, then enable the "View as Tiff" button --%>
                            <c:url value="${renderURL}" var="ViewDocAsTiffURL">
                                <c:param name="of" value="TIFF"/>
                                <c:param name="pg" value="${requestScope.pagenum}"/>
                            </c:url>
                            <img width="35" height="33" src="images/ctiff.gif" onclick="location.href='<c:out value='${ViewDocAsTiffURL}'/>&act=saveTiff'" alt="View as TIFF">
                        </c:when>
                        <c:otherwise>
                            <img width="35" height="33" src="images/gtiff.gif" alt="View as TIFF">
                        </c:otherwise>
                    </c:choose>
                    <%}%>
                    <c:choose>
                        <c:when test="${requestScope.singlepagedoc || (requestScope.docformat == 'collection')}">
                            <%-- If this is a single-page document or if this is a Collection format document,
                                 disable the page navigation buttons. Collection-format documents do not have
                                 the concept of pages and hence are not page addressable or viewable. --%>
                            <img width="35" height="33" src="images/gfirstpage.gif" alt="First Page">
                            <img width="35" height="33" src="images/grewpage.gif"   alt="Backward 5 Pages">
                            <img width="35" height="33" src="images/gprevpage.gif"  alt="Previous Page">
                            <img width="35" height="33" src="images/gnextpage.gif"  alt="Next Page">
                            <img width="35" height="33" src="images/gfastpage.gif"  alt="Forward 5 Pages">
                            <img width="35" height="33" src="images/glastpage.gif"  alt="Last Page">
                        </c:when>
                        <c:otherwise>
                            <c:url value="${ViewDocURL}" var="ViewDocFirstPageURL">
                                <c:param name="par" value="first"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocMinusFiveURL">
                                <c:param name="par" value="m5"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocMinusOneURL">
                                <c:param name="par" value="m1"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocPlusOneURL">
                                <c:param name="par" value="1"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocPlusFiveURL">
                                <c:param name="par" value="5"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocLastPageURL">
                                <c:param name="par" value="last"/>
                            </c:url>
                            <img width="35" height="33" src="images/cfirstpage.gif" onclick="location.href='<c:out value='${ViewDocFirstPageURL}'/>'" alt="First Page">
                            <img width="35" height="33" src="images/crewpage.gif"   onclick="location.href='<c:out value='${ViewDocMinusFiveURL}'/>'" alt="Backward 5 Pages">
                            <img width="35" height="33" src="images/cprevpage.gif"  onclick="location.href='<c:out value='${ViewDocMinusOneURL}'/>'"  alt="Previous Page">
                            <img width="35" height="33" src="images/cnextpage.gif"  onclick="location.href='<c:out value='${ViewDocPlusOneURL}'/>'"   alt="Next Page">
                            <img width="35" height="33" src="images/cfastpage.gif"  onclick="location.href='<c:out value='${ViewDocPlusFiveURL}'/>'"  alt="Forward 5 Pages">
                            <img width="35" height="33" src="images/clastpage.gif"  onclick="location.href='<c:out value='${ViewDocLastPageURL}'/>'"  alt="Last Page">
                        </c:otherwise>
                    </c:choose>
                    <c:url value="${ViewDocURL}" var="ViewDocZoomIn">
                        <c:param name="par" value="zoomin"/>
                    </c:url>
                    <img width="35" height="33" src="images/czoomin.gif"     onclick="location.href='<c:out value='${ViewDocZoomIn}'/>'"         alt="Zoom In">
                    <c:url value="${ViewDocURL}" var="ViewDocZoomNorm">
                        <c:param name="par" value="zoomnorm"/>
                    </c:url>
                    <img width="35" height="33" src="images/czoomnorm.gif"   onclick="location.href='<c:out value='${ViewDocZoomNorm}'/>'"         alt="Normal Zoom">
                    <c:url value="${ViewDocURL}" var="ViewDocZoomOut">
                        <c:param name="par" value="zoomout"/>
                    </c:url>
                    <img width="35" height="33" src="images/czoomout.gif"    onclick="location.href='<c:out value='${ViewDocZoomOut}'/>'"         alt="Zoom Out">
                    <c:url value="${ViewDocURL}" var="ViewDocRotateLeft">
                        <c:param name="par" value="rotleft"/>
                    </c:url>
                    <img width="35" height="33" src="images/crotleft.gif"   onclick="location.href='<c:out value='${ViewDocRotateLeft}'/>'"          alt="Rotate Left">
                    <c:url value="${ViewDocURL}" var="ViewDocRotateNorm">
                        <c:param name="par" value="rotnorm"/>
                    </c:url>
                    <img width="35" height="33" src="images/crotnorm.gif"  onclick="location.href='<c:out value='${ViewDocRotateNorm}'/>'"       alt="Normal Rotation">
                    <c:url value="${ViewDocURL}" var="ViewDocRotateRight">
                        <c:param name="par" value="rotright"/>
                    </c:url>
                     <img width="35" height="33" src="images/crotright.gif"  onclick="useCurSlideHere().getCurSlide();"       alt="Rotate Right"> 
                   <%-- <img width="35" height="33" src="images/crotright.gif"  onclick="location.href='${slider.currentSlide}'"  $(thisSlide).find("img")     alt="Rotate Right"> --%>
                    <c:url value="${ViewDocURL}" var="ViewDocMultiplePages">
                        <c:param name="par" value="multiplepages"/>
                    </c:url>
                    <img width="35" height="33" src="images/cface.gif"   onclick="location.href='<c:out value='${ViewDocMultiplePages}'/>'"          alt="Multiple Pages">
              		 <c:if test="${!(requestScope.docformat == 'collection')}">
                                <%-- Don't display the "Goto Page #" form for Collection format documents,
                                     as they don't have the concept of pages and page navigation. --%>
                                <form name="docPageForm" id="docPageForm" method="GET" onclick="return submitpage(<%=pageN%>);" >
								<input type="hidden" name="urlType" value="">
								      
                                	 <input type="hidden" name="acct" value="${param.acct}">
                                        <input type="hidden" name="db"   value="${param.db}">
                                        <input type="hidden" name="doc"  value="${doc}">
                                        <input type="hidden" name="rot"  value="${rot}">
                                        <input type="hidden" name="op"   value="view">
										 <input type="hidden" name="res"   value="${res}">
										  <input type="hidden" name="mult1"   value="view">
										  
                                    <%-- Page M of N of the Document --%>
                                   <table width="100%"><tr><td align="center">
                                        Page
                                        <c:choose>
                                            <c:when test="${requestScope.singlepagedoc}">
                                                <%-- For single page documents, only show "Page 1 of 1" --%>
                                                <input disabled="true" type="text" name="pg" id="pg" value="${pagenum}" size="8" maxlength="8"/>
                                                / <c:out value="${pagecnt}" />
                                            </c:when>
                                            <c:otherwise>
                                                <input type="text" name="pg" id="pg" value="${pagenum}" size="8" maxlength="8"/>
                                                / <c:out value="${pagecnt}" />
                                                <input type=image alt="Go to Page" value="gotoPage" src="images/cgo.gif" border=0 name="image" align=absMiddle onclick="document.pressed=this.value">
                                            </c:otherwise>
                                        </c:choose>
                                       
                                       
                              </td></tr></table>
	</form>
	</c:if>
	</td>
	<td></td>
	</tr>


	<tr bgcolor="#ffffff">
		<c:choose>
			<c:when test="${mult1 == '2' || mult1 == '4'}">
				<td colspan="0">
			</c:when>
			<c:otherwise>
				<td colspan="2">
			</c:otherwise>
		</c:choose>
		<div align=center>
			<c:choose>
				<%-- When rendering a Collection format document,
                                 or a native PDF format document,
                                 or if the forcepdf app config param is set
                                 display the document rendering in an IFrame.
                            --%>
				<c:when
					test="${(requestScope.docformat == 'collection') || (requestScope.docformat == 'pdf') || initParam.forcepdf}">
					<c:choose>
						<c:when test="${fn:contains(requestScope.doctype, 'mht')}">
							<%-- Remove "%2f" chars embedded in the rendering URL otherwise certain Windows OSes
                                             will prevent the images in the MHT document from being viewed in the browser. --%>
							<IFRAME id="frameimage" test='111'
								src="<c:out value='${fn:replace(renderURL,"%2f","")}'/>"
								onload="resizeIframe();" width="100%" scrolling="auto "
								frameborder="1">ERROR: your browser does not support
								the iframe tag</IFRAME>
						</c:when>
						<c:otherwise>
							<IFRAME id="frameimage" test='222'
								src="<c:out value='${renderURL}'/>" onload="resizeIframe();"
								width="100%" scrolling="auto " frameborder="1">ERROR:
								your browser does not support the iframe tag</IFRAME>
						</c:otherwise>
					</c:choose>
				</c:when>

				<c:otherwise>
					<%-- <div id="container" class="cf">--%>
						<div id="main" role="main">
							<section class="slider">
							<div id="slider" class="flexslider">
								<ul class="slides" id="mainslides">
								</ul>
							</div>
							<div id="carousel" class="flexslider">
								<ul class="slides" id="thumbslides">
								</ul>
							</div>
							</section>
						</div>
					<%--</div>--%>
				</c:otherwise>
			</c:choose>
			<%-- <table><tr><td><a href="<c:out value='${renderURL}'/>"><img src="<c:out value='${renderURL}'/>" alt="Document rendering" border="1"></a></td></tr></table> --%>
			<%-- <IFRAME id="frameimage" src="<c:out value='${fn:replace(renderURL,"%2f","")}'/>" height="100%" width="100%" scrolling="auto "frameborder="1">ERROR: your browser does not support the iframe tag</IFRAME> --%>
		</div>
		</td>

		<c:choose>
			<c:when test="${mult1 == '2' || mult1 == '4'}">
				<c:choose>
					<c:when test="${mult2 > pagecnt}"></c:when>
					<c:otherwise>
						<td colspan="0">
							<div align=center>
								<table>
									<tr>
										<td><c:url value="${renderURL}"
												var="ViewDocMultiplePage2">
												<c:param name="pg" value="${mult2}" />
											</c:url> <a href="<c:out value='${ViewDocMultiplePage2}'/>"><img
												src="<c:out value='${ViewDocMultiplePage2}' />"
												alt="Document rendering" border="1">
										</a></td>
									</tr>
								</table>
							</div></td>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise></c:otherwise>
		</c:choose>
	</tr>
	<c:choose>
		<c:when test="${mult1 == '4'}">
			<tr bgcolor="#ffffff">
				<c:choose>
					<c:when test="${mult3 > pagecnt}"></c:when>
					<c:otherwise>
						<td colspan="0">
							<div align=center target="thumbnail">
								<table>
									<tr>
										<td><c:url value="${renderURL}"
												var="ViewDocMultiplePage3">
												<c:param name="pg" value="${mult3}" />
											</c:url> <a href="<c:out value='${ViewDocMultiplePage3}'/>"><img
												src="<c:out value='${ViewDocMultiplePage3}'/>"
												alt="Document rendering" border="1">
										</a></td>
									</tr>
								</table>
							</div></td>
						<c:choose>
							<c:when test="${mult4 > pagecnt}"></c:when>
							<c:otherwise>
								<td colspan="0">
									<div align=center>
										<table>
											<tr>
												<td><c:url value="${renderURL}"
														var="ViewDocMultiplePage4">
														<c:param name="pg" value="${mult4}" />
													</c:url> <a href="<c:out value='${ViewDocMultiplePage4}'/>"><img
														src="<c:out value='${ViewDocMultiplePage4}' />"
														alt="Document rendering" border="1">
												</a></td>
											</tr>
										</table>
									</div></td>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:when>
		<c:otherwise></c:otherwise>
	</c:choose>
	<tr bgcolor="#ffffff">
		<td colspan="2" height="10"></td>
	</tr>
	</table>

</body>
</html>
