
<%-- URL for the Document rendering servlet that actually does the work
     of rendering the document and returning back the appropriate
     rendered image output. --%>
<c:url value="Render.do" var="renderURL">
	<c:param name="acct" value="${param.acct}" />
	<c:param name="db" value="${param.db}" />
	<c:param name="dh" value="${doc}" />
	<c:param name="pg" value="${pagenum}" />
	<c:param name="pagecnt" value="${pagecnt}" />
	<c:param name="par" value='${par}' />
	<c:param name="rot" value="${rot}" />
	<c:param name="res" value="${res}" />
	<c:if test="${not empty param.of}">
		<c:param name="of" value='${param.of}' />
	</c:if>
	<c:param name="docformat" value="${requestScope.docformat}" />
	<c:param name="doctype" value="${requestScope.doctype}" />
	<c:param name="imageoutput" value="${requestScope.imageoutput}" />
	<c:param name="pdfoutput" value="${requestScope.pdfoutput}" />
</c:url>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<title>Database <c:out value="${param.db}" />: Account # <c:out
		value="${param.acct}" />, Document <c:out value="${doc.date}" />,
	Page <c:out value="${pagenum}" />
</title>
<body>
	<div style="width: 68%; height:100%; float:left; border:1px solid black" class="mainclass">
	    <img src="<c:out value='${renderURL}'/>"></img>
	</div>
	
</body>
</html>
