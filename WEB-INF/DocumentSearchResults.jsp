<%-- 
    Document   : DocumentSearchResults
    Created on : May 5, 2010, 10:53:42 AM
    Author     : ssahmed
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Account search results</title>
        <link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
		<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
		<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
		<meta name="helpHref" content="GettingStarted" />
		<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
		<link rel="stylesheet" href="styles/login.css" />
		<link rel="stylesheet" href="styles/common/messages.css" />
		<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#000000" vlink="#000000" alink="#000000">
        <div id="page">
        <div id="header" class="clearfix">
		<div id="branding">
			<img id="logo" src="images/header/logo.JPG"/>
		</div>
		<div id="applicationDetail">
			<ul id="productName">
				<li>EngageOne Customer Service Representative</li>
			</ul>
			<ul id="userDetail">
			</ul>

			 <form name="Logout" action="<%=request.getContextPath()%>/Logout.action" >
        		<%
        		out.println("<font color='white'><input type='button' onClick=\"window.location.href='Interface';\" value='Home'/></font>");	
        		if(session.getValue("GROUP_MAKER").equals("true"))
        		{
        			out.println("<font color='white'><input type='button' onClick=\"window.location.href='MakerSubmitAction.action';\" value='Setup Permission'/></font>");	
        		}
        		if(session.getValue("GROUP_CHECKER").equals("true"))
    			{
    				out.println("<font color='white'><input type='button' onClick=\"window.location.href='CheckerSubmitAction.action';\"  value='Approve Permission'/></font>");	
    			}
        	%>
        	<input type="submit" value="Logout" />
        	</form>
        	</ul>
		</div>
		<div id="curve">
			<img id="curve" src="styles/themes/purple/images/curve.gif" />
		</div>
		<hr />
	</div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr bgcolor="#0060B0">
                <td colspan="2" height="4"></td>
            </tr>           
            <tr>
            <td>              
                  	 
        	</td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="5"></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10"></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="5">
                    <table>
                        <tr>
                            <td>
                                Searched:
                                <label>${param.selectedIndex}</label>
                            </td>
                            <td width="10"></td>                            
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td colspan="5" height="10">
                            </td>
                        </tr>
                        <tr>
                            <td>                                
                                <label>for:   ${param.searchKey}</label>
                            </td>
                            <td width="10"></td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="5">
                </td>
            </tr>
            <tr bgcolor="#D0E0F0">
                <td colspan="2" height="5">
                </td>
            </tr>
            <tr bgcolor="#D0E0F0" valign="middle">
                <td colspan="2" align="center">
                    <img width="35" height="33" src="images/csearch.gif" onclick="location.href='<c:out value='${MainSearchPageURL}'/>'" alt="Search">
                    <img width="35" height="33" src="images/gpdf.gif" alt="View as PDF">
                    <img width="35" height="33" src="images/gpdfnobg.gif" alt="View as PDF with no background">
                    <img width="35" height="33" src="images/gprint.gif" alt="View as Text">
                    <img width="35" height="33" src="images/gtiff.gif" alt="View as TIFF">
                    <img width="35" height="33" src="images/gfirstpage.gif" alt="First Page">
                    <img width="35" height="33" src="images/grewpage.gif" alt="Backward 5 Pages">
                    <img width="35" height="33" src="images/gprevpage.gif" alt="Previous Page">
                    <img width="35" height="33" src="images/gnextpage.gif" alt="Next Page">
                    <img width="35" height="33" src="images/gfastpage.gif" alt="Forward 5 Pages">
                    <img width="35" height="33" src="images/glastpage.gif" alt="Last Page">
                    <img width="35" height="33" src="images/gzoomin.gif" alt="Zoom In">
                    <img width="35" height="33" src="images/gzoomnorm.gif" alt="Normal Zoom">
                    <img width="35" height="33" src="images/gzoomout.gif" alt="Zoom Out">
                    <img width="35" height="33" src="images/grotleft.gif" alt="Rotate Left">
                    <img width="35" height="33" src="images/grotnorm.gif" alt="Normal Rotation">
                    <img width="35" height="33" src="images/grotright.gif" alt="Rotate Right">
                    <img width="35" height="33" src="images/gface.gif" alt="Multiple Pages">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="5">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2">
                    <center>
                        <table cellpadding="3" class="outline">                        	
                            <tr>
                                <c:forEach items="${requestScope.columnNames}" var="colName">
                                    <td class="headers"><b><c:out value="${colName}"/></b></td>
                                </c:forEach>
                            </tr>
                            <c:forEach items="${requestScope.documents}" var="doc">
                                <tr>
                                    <c:forEach items="${requestScope.columnNames}" var="colName" varStatus="status">
                                        <%-- Create an instance of the DocumentHandle JavaBean so that the
                                             URL for rendering each document can be created. --%>
                                        <jsp:useBean id="docHandle" class="com.pb.evault.DocumentHandle" scope="page" />
                                        <jsp:setProperty name="docHandle" property="date" value="${doc.date}" />
                                        <c:choose>
                                            <%-- For Collection format documents, set the type property to the document's type
                                                 which is the file extension of the underlying file/document. For non-Collection
                                                 format documents, however, set the type property to the document's format.
                                                 This behaviour has to match the com.g1.e2.vault.serviceweb.DocumentHandle class
                                                 which does the same thing in one of its constructors.
                                            --%>
                                            <c:when test="${doc.format == 'collection'}">
                                                <jsp:setProperty name="docHandle" property="type" value="${doc.type}" />
                                            </c:when>
                                            <c:otherwise>
                                                <jsp:setProperty name="docHandle" property="type" value="${doc.format}" />
                                            </c:otherwise>
                                        </c:choose>
                                        <jsp:setProperty name="docHandle" property="file" value="${doc.file}" />
                                        <jsp:setProperty name="docHandle" property="pointer" value="${doc.pointer}" />

                                        <c:if test="${status.first}">
                                            <%-- The URL to render/view this specific document --%>
                                            <c:url value="Interface" var="RenderURL">
                                                <c:param name="acct" value="${doc.accountNumber}"/>
                                                <c:param name="db"   value="${param.db}"/>
                                                <c:param name="doc"  value="${docHandle}"/>
                                                <c:param name="op"   value="view"/>
                                            </c:url>
                                            <td class="cells"><a href='<c:out value="${RenderURL}"/>'><c:out value="${doc.customFields[colName]}"/></a></td>
                                        </c:if>
                                        <c:if test="${!status.first}">
                                            <td class="cells"><c:out value="${doc.customFields[colName]}"/></td>
                                        </c:if>
                                    </c:forEach>
                                </tr>
                            </c:forEach>
                        </table>
                    </center>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2">
                    <center>                    	
                    	<c:choose>
                        	<c:when test="${requestScope.NoPermissions eq 'NoPermissions'}">
                        		<font color="red"><strong>No permissions have been set!</strong></font>                        		
                        	</c:when>
	                        <c:otherwise>                        
		                        <c:choose>
		                            <c:when test="${not empty requestScope.documents}">
		                                Found: <c:out value="${fn:length(requestScope.documents)}" />
		                                <c:choose>
		                                    <c:when test="${fn:length(requestScope.documents) > 1}">
		                                        Documents
		                                    </c:when>
		                                    <c:otherwise>
		                                        Document
		                                    </c:otherwise>
		                                </c:choose>
		                            </c:when>
		                            <c:otherwise>
		                                <font color="red"><c:out value="No records match this search criteria"/></font>
		                            </c:otherwise>
		                        </c:choose>
			               </c:otherwise>
			             </c:choose>  
                    </center>
                </td>
            </tr>
            <c:if test="${requestScope.hasMore}">
                <c:url value="Interface" var="MoreSearchResultsURL">                    
                    <c:param name="selectedIndex" value="${param.selectedIndex}"/>
                    <c:param name="searchKey" value="${param.searchKey}"/>
                    <c:param name="cont"      value="${requestScope.documents[fn:length(requestScope.documents) - 1].match}"/>
                </c:url>
                <tr bgcolor="#ffffff">
                    <td colspan="2">
                        <center>
                            <a href="<c:out value='${MoreSearchResultsURL}'/>"><c:out value="More" /></a>
                        </center>
                    </td>
                </tr>
            </c:if>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
        </table>
        </div>
    </body>
</html>
