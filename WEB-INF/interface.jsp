<%-- 
    Document   : interface
    Created on : Feb 3, 2009, 11:31:37 AM
    Author     : ssahmed
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e2 Vault Web search</title>
        <link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
		<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
		<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
		<meta name="helpHref" content="GettingStarted" />
		<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
		<link rel="stylesheet" href="styles/login.css" />
		<link rel="stylesheet" href="styles/common/messages.css" />
		<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
        <script language="javascript" type="text/javascript">
            function refreshSearchIndexes() {
                var dropdownList = document.getElementById("index");
                if (dropdownList) dropdownList.selectedIndex = -1;
                //document.searchInterface.searchKey.value = "";
                document.searchInterface.acct.value = "";
                document.forms["searchInterface"].submit();
            }
        </script>
    </head>
    <body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#000000" vlink="#000000" alink="#000000">
        <div id="page">
        <div id="header" class="clearfix">
		<div id="branding">
			<img id="logo" src="images/header/logo.JPG"/>
		</div>
		<div id="applicationDetail">
			<ul id="productName">
				<li>EngageOne Customer Service Representative</li>
			</ul>
			<ul id="userDetail">
			</ul>

			  <form name="Logout" action="<%=request.getContextPath()%>/Logout.action" >
        		<%
        		out.println("<font color='white'><input type='button' onClick=\"window.location.href='Interface';\" value='Home'/></font>");	
        		if(session.getValue("GROUP_MAKER").equals("true"))
        		{
        			out.println("<font color='white'><input type='button' onClick=\"window.location.href='MakerSubmitAction.action';\" value='Setup Permission'/></font>");	
        		}
        		if(session.getValue("GROUP_CHECKER").equals("true"))
    			{
    				out.println("<font color='white'><input type='button' onClick=\"window.location.href='CheckerSubmitAction.action';\"  value='Approve Permission'/></font>");	
    			}
        	%>
        	<input type="submit" value="Logout" />
        	</form>

			</ul>
		</div>
		<div id="curve">
			<img id="curve" src="styles/themes/purple/images/curve.gif" />
		</div>
		<hr />
	</div>
        
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr bgcolor="#0060B0">
                <td colspan="2" height="4"></td>
            </tr>
            
            <tr bgcolor="#ffffff">
                <td colspan="2" height="5"></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10"></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="5">
                    <table>
                        <form id="searchInterface" name="searchInterface" method="POST" action="<%=request.getContextPath()%>/Interface" >
                            <tr>
                                <td>
                                    Search
                                    <select id="index" name="index" onchange="document.searchInterface.searchKey.focus()">
                                        <c:forEach items="${searchIndexes}" var="searchIndex">                                            
                                            <option value='<c:out value="${searchIndex.name}" />'>
                                                <c:out value="${searchIndex.description}" />
                                            </option>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td width="10">
                                </td>
                                <td>
                                    for
                                    <input type="text" name="searchKey" value="" size="32" />
                                </td>
                                <td width="10">
                                </td>	
                                <td>
                                    <input type="image" alt="Search" src="images/cgo.gif" border="0" align="absMiddle">
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td colspan="5" height="10">
                                </td>
                            </tr>
                            <tr>                                                               
                                <td colspan="4">
                                    Selected Account <input maxlength="20" size="20" value="${param.acct}" name="acct">
                                </td>
                            </tr>
                        </form>
                    </table>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="5">
                </td>
            </tr>
            <tr bgcolor="#D0E0F0">
                <td colspan="2" height="5">
                </td>
            </tr>
            <tr bgcolor="#D0E0F0" valign="middle">
                <td colspan="2" align="center">
                    <img width="35" height="33" src="images/csearch.gif"     onclick="location.href='Interface'"   alt="Search">
                    <img width="35" height="33" src="images/gpdf.gif"                                                          alt="View as PDF">
                    <img width="35" height="33" src="images/gpdfnobg.gif"                                                      alt="View as PDF with no background">
                    <img width="35" height="33" src="images/gprint.gif"                                                        alt="View as Text">
                    <img width="35" height="33" src="images/gtiff.gif"                                                         alt="View as TIFF">
                    <img width="35" height="33" src="images/gfirstpage.gif"                                                    alt="First Page">
                    <img width="35" height="33" src="images/grewpage.gif"                                                      alt="Backward 5 Pages">
                    <img width="35" height="33" src="images/gprevpage.gif"                                                     alt="Previous Page">
                    <img width="35" height="33" src="images/gnextpage.gif"                                                     alt="Next Page">
                    <img width="35" height="33" src="images/gfastpage.gif"                                                     alt="Forward 5 Pages">
                    <img width="35" height="33" src="images/glastpage.gif"                                                     alt="Last Page">
                    <img width="35" height="33" src="images/gzoomin.gif"                                                       alt="Zoom In">
                    <img width="35" height="33" src="images/gzoomnorm.gif"                                                     alt="Normal Zoom">
                    <img width="35" height="33" src="images/gzoomout.gif"                                                      alt="Zoom Out">
                    <img width="35" height="33" src="images/grotleft.gif"                                                      alt="Rotate Left">
                    <img width="35" height="33" src="images/grotnorm.gif"                                                      alt="Normal Rotation">
                    <img width="35" height="33" src="images/grotright.gif"                                                     alt="Rotate Right">
                    <img width="35" height="33" src="images/gface.gif"                                                         alt="Multiple Pages">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="5">
                </td>
            </tr>
        </table>
        </div>
    </body>
</html>
