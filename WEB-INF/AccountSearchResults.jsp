<%-- 
    Document   : AccountSearchResults
    Created on : Mar 6, 2009, 12:08:42 PM
    Author     : ssahmed
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<c:url value="Interface" var="MainSearchPageURL">
    <c:param name="db" value="${param.db}"/>
</c:url>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Account search results</title>
        <link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
		<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
		<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
		<meta name="helpHref" content="GettingStarted" />
		<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
		<link rel="stylesheet" href="styles/login.css" />
		<link rel="stylesheet" href="styles/common/messages.css" />
		<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#000000" vlink="#000000" alink="#000000">
        <div id="page">
        <div id="header" class="clearfix">
		<div id="branding">
			<img id="logo" src="images/header/logo.JPG"/>
		</div>
		<div id="applicationDetail">
			<ul id="productName">
				<li>EngageOne Customer Service Representative</li>
			</ul>
			<ul id="userDetail">
			</ul>

			<form name="Logout" action="<%=request.getContextPath()%>/Logout.action" >
        		<%
        		out.println("<font color='white'><input type='button' onClick=\"window.location.href='Interface';\" value='Home'/></font>");	
        		if(session.getValue("GROUP_MAKER").equals("true"))
        		{
        			out.println("<font color='white'><input type='button' onClick=\"window.location.href='MakerSubmitAction.action';\" value='Setup Permission'/></font>");	
        		}
        		if(session.getValue("GROUP_CHECKER").equals("true"))
    			{
    				out.println("<font color='white'><input type='button' onClick=\"window.location.href='CheckerSubmitAction.action';\"  value='Approve Permission'/></font>");	
    			}
        	%>
        	<input type="submit" value="Logout" />
        	</form>
        	</ul>
		</div>
		<div id="curve">
			<img id="curve" src="styles/themes/purple/images/curve.gif" />
		</div>
		<hr />
	</div>
        
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr bgcolor="#0060B0">
                <td colspan="2" height="4">
                	
                </td>
            </tr>            
            <tr bgcolor="#ffffff">
                <td colspan="2" height="5"></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10"></td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="5">
                <a href="javascript: history.go(-1)">Back</a>
                    <table>
                        <tr>
                            <td>
                                Searched : ${requestScope.dict}
                            </td>                
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td colspan="5" height="10">
                            for : ${param.searchKey}
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Selected Account : ${requestScope.accNo}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="5">
                </td>
            </tr>
            <tr bgcolor="#D0E0F0">
                <td colspan="2" height="5">
                </td>
            </tr>
            <tr bgcolor="#D0E0F0" valign="middle">
                <td colspan="2" align="center">
                    <img width="35" height="33" src="images/csearch.gif" onclick="location.href='<c:out value='${MainSearchPageURL}'/>'" alt="Search">
                    <img width="35" height="33" src="images/gpdf.gif"                                                          alt="View as PDF">
                    <img width="35" height="33" src="images/gpdfnobg.gif"                                                      alt="View as PDF with no background">
                    <img width="35" height="33" src="images/gprint.gif"                                                        alt="View as Text">
                    <img width="35" height="33" src="images/gtiff.gif"                                                         alt="View as TIFF">
                    <img width="35" height="33" src="images/gfirstpage.gif"                                                    alt="First Page">
                    <img width="35" height="33" src="images/grewpage.gif"                                                      alt="Backward 5 Pages">
                    <img width="35" height="33" src="images/gprevpage.gif"                                                     alt="Previous Page">
                    <img width="35" height="33" src="images/gnextpage.gif"                                                     alt="Next Page">
                    <img width="35" height="33" src="images/gfastpage.gif"                                                     alt="Forward 5 Pages">
                    <img width="35" height="33" src="images/glastpage.gif"                                                     alt="Last Page">
                    <img width="35" height="33" src="images/gzoomin.gif"                                                       alt="Zoom In">
                    <img width="35" height="33" src="images/gzoomnorm.gif"                                                     alt="Normal Zoom">
                    <img width="35" height="33" src="images/gzoomout.gif"                                                      alt="Zoom Out">
                    <img width="35" height="33" src="images/grotleft.gif"                                                      alt="Rotate Left">
                    <img width="35" height="33" src="images/grotnorm.gif"                                                      alt="Normal Rotation">
                    <img width="35" height="33" src="images/grotright.gif"                                                     alt="Rotate Right">
                    <img width="35" height="33" src="images/gface.gif"                                                         alt="Multiple Pages">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="5">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2">
                    <center>
                        <table bgcolor="#D0E0F0" cellpadding="3" class="outline">
                            <tr>
                                <c:forEach items="${requestScope.columnNames}" var="colName">
                                    <td class="headers"><b><c:out value="${colName}"/></b></td>
                                </c:forEach>
                            </tr>
                            <c:forEach items="${requestScope.accounts}" var="account">
                                <tr>
                                    <c:forEach items="${requestScope.columnNames}" var="colName" varStatus="status">
                                        <c:if test="${status.first}">
                                            <%-- first The URL to render/view this specific account --%>
                                            <c:url value="Interface" var="RenderURL">
                                                <c:param name="acct" value="${account.accountNumber}"/>                                                
                                                <c:param name="op"   value="view"/>
                                            </c:url>
                                            <td class="cells"><a href='<c:out value="${RenderURL}"/>'><c:out value="${account.customFields[colName]}"/></a></td>
                                        </c:if>
                                        <c:if test="${!status.first}">
                                        	<%-- second The URL to render/view this specific account --%>
                                            <td class="cells"><c:out value="${account.customFields[colName]}"/></td>
                                        </c:if>
                                    </c:forEach>
                                </tr>
                            </c:forEach>
                        </table>
                    </center>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2">
                    <center>
                    	<c:choose>
                        	<c:when test="${requestScope.NoPermissions eq 'NoPermissions'}">
                        		<font color="red"><strong>No permissions have been set!</strong></font>                        		
                        	</c:when>
	                        <c:otherwise> 
		                        <c:choose>
		                            <c:when test="${not empty requestScope.accounts}">
		                                Found: <c:out value="${fn:length(requestScope.accounts)}" />
		                                <c:choose>
		                                    <c:when test="${fn:length(requestScope.accounts) > 1}">
		                                        Accounts
		                                    </c:when>
		                                    <c:otherwise>
		                                        Account
		                                    </c:otherwise>
		                                </c:choose>
		                            </c:when>
		                            <c:otherwise>
		                                <font color="red"><c:out value="No records match this search criteria"/></font>
		                            </c:otherwise>
		                        </c:choose>
		                        </c:otherwise>
		                        </c:choose>
                    </center>
                </td>
            </tr>
            <c:if test="${requestScope.hasMore}">
                <c:url value="Interface" var="MoreSearchResultsURL">                    
                    <c:param name="index"     value="${param.index}"/>
                    <c:param name="searchKey" value="${param.searchKey}"/>
                    <c:param name="cont"      value="${requestScope.accounts[fn:length(requestScope.accounts) - 1].match}"/>
                </c:url>
                <tr bgcolor="#ffffff">
                    <td colspan="2">
                        <center>
                            <a href="<c:out value='${MoreSearchResultsURL}'/>"><c:out value="More" /></a>
                        </center>
                    </td>
                </tr>
            </c:if>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
        </table>
        </div>
    </body>
</html>
