<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.pb.dto.UserDTO"%>
<%
	System.out.println("here1");
	int pageN = request.getAttribute("pagenum") != null ? Integer.parseInt(request.getAttribute("pagenum").toString()) : 1;
	System.out.println("here2");	
	System.out.println("here3");
	List actionList = request.getAttribute("actionList") != null? (ArrayList) request.getAttribute("actionList"): new ArrayList();
	//String view = "";
	String print = "";
	String download = "";
	String search = session.getAttribute("search") != null ? session.getAttribute("search").toString() : "";
	String rep = session.getAttribute("rep") != null ? session.getAttribute("rep").toString() : "";
	String pageName = "search";
	String pageName1 = "fav";
	String pageName2 = "search1";
	String pageType = session.getAttribute("pageType") != null? session.getAttribute("pageType").toString(): "";
	Long UserId = session.getAttribute("UserId") != null ? Long.parseLong(session.getAttribute("UserId").toString()) : 0L;
	Long prdbID = session.getAttribute("prdbID") != null ? Long.parseLong(session.getAttribute("prdbID").toString()) : 0L;
	System.out.println("prdbID in test.jsp:" + prdbID);
	
	Enumeration params = request.getParameterNames();
	String par = "";
	String rot = "";
	while (params.hasMoreElements()) {
		String paramName = params.nextElement().toString();
		if(paramName.equalsIgnoreCase("par")){
			par = request.getParameter(paramName);
		}
		if(paramName.equalsIgnoreCase("rot")){
			rot = request.getParameter(paramName);
		}
		System.out.println("paramName:" + paramName);
		System.out.println("paramName:value:"+ request.getParameter(paramName));

	}
%>
<%
	for (Iterator iter = actionList.iterator(); iter.hasNext();) {
		System.out.println("1");
		UserDTO dto = (UserDTO) iter.next();
%>
<%
	if (dto.getActionName().equalsIgnoreCase("Download")) {
			download = "true";
			System.out.println("download");
%>
<%
	}
		if (dto.getActionName().equalsIgnoreCase("Print")) {
			print = "true";
			System.out.println("print");
%>
<%
	}
%>
<%
	}
%>
<%
	if (pageType.equalsIgnoreCase(pageName + UserId)) {
		
		System.out.println("pagetype1"+pageName + UserId);
%>

<%-- URL to render this current document but with some different
     parameters:

     - either at a different resolution
     - either at a different rotation
     - either a different page within the document
     - or with a "multiple pages" setting

     and so on. --%>

<c:url value="DocManagerNew1.action" var="ViewDocURL">
	<c:param name="prdbID" value="${param.prdbID}" />
	<c:param name="vaultIdx" value="${param.vaultIdx}" />
	<c:param name="prName" value="${param.prName}" />
	<c:param name="actType" value="view" />
	<c:param name="acct" value="${param.acct}" />
	<c:param name="db" value="${param.db}" />
	<c:param name="doc" value="${doc}" />
	<c:param name="pg" value="${pagenum}" />
	<c:param name="rot" value="${rot}" />
	<c:param name="res" value="${res}" />
	<c:param name="mult1" value="${mult1}" />
	<c:if test="${not empty param.par}">
		<c:param name="par" value='${param.par}' />
	</c:if>
	<c:param name="op" value="view" />
</c:url>
<%
	} else if (pageType.equalsIgnoreCase(pageName1 + UserId)) {
		
		System.out.println("pagetype2"+pageName1 + UserId);
%>
<c:url value="DocManagerFav.action" var="ViewDocURL">
	<c:param name="prdbID" value="${param.prdbID}" />
	<c:param name="vaultIdx" value="${param.vaultIdx}" />
	<c:param name="prName" value="${param.prName}" />
	<c:param name="actType" value="view" />
	<c:param name="acct" value="${param.acct}" />
	<c:param name="db" value="${param.db}" />
	<c:param name="doc" value="${doc}" />
	<c:param name="pg" value="${pagenum}" />
	<c:param name="rot" value="${rot}" />
	<c:param name="res" value="${res}" />
	<c:param name="mult1" value="${mult1}" />
	<c:if test="${not empty param.par}">
		<c:param name="par" value='${param.par}' />
	</c:if>
	<c:param name="op" value="view" />
</c:url>
<%
	} else if (pageType.equalsIgnoreCase(pageName2 + UserId)) {
		System.out.println("pagetype2"+pageName2 + UserId);
%>
<c:url value="SearchAccountNew.action" var="ViewDocURL">
	<c:param name="prdbID" value="${param.prdbID}" />
	<c:param name="vaultIdx" value="${param.vaultIdx}" />
	<c:param name="prName" value="${param.prName}" />
	<c:param name="actType" value="view" />
	<c:param name="acct" value="${param.acct}" />
	<c:param name="db" value="${param.db}" />
	<c:param name="doc" value="${doc}" />
	<c:param name="pg" value="${pagenum}" />
	<c:param name="rot" value="${rot}" />
	<c:param name="res" value="${res}" />
	<c:param name="mult1" value="${mult1}" />
	<c:if test="${not empty param.par}">
		<c:param name="par" value='${param.par}' />
	</c:if>
	<c:param name="op" value="view" />
</c:url>
<%
	}
%>

<%
System.out.println("Before rendering");
%>
<%-- URL for the Document rendering servlet that actually does the work
     of rendering the document and returning back the appropriate
     rendered image output. --%>
<c:url value="Render.do" var="renderURL">
	<c:param name="acct" value="${param.acct}" />
	<c:param name="prID" value="${param.prID}" />
	<c:param name="db" value="${param.db}" />
	<c:param name="dh" value="${doc}" />
	<c:param name="pg" value="${pagenum}" />
	<c:param name="pagecnt" value="${pagecnt}" />
	<c:param name="par" value='${par}' />
	<c:param name="rot" value="${rot}" />
	<c:param name="res" value="${res}" />
	<c:if test="${not empty param.of}">
		<c:param name="of" value='${param.of}' />
	</c:if>
	<c:param name="docformat" value="${requestScope.docformat}" />
	<c:param name="doctype" value="${requestScope.doctype}" />
	<c:param name="imageoutput" value="${requestScope.imageoutput}" />
	<c:param name="pdfoutput" value="${requestScope.pdfoutput}" />
</c:url>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<title>Database <c:out value="${param.db}" />: Account # <c:out
		value="${param.acct}" />, Document <c:out value="${doc.date}" />,
	Page <c:out value="${pagenum}" />
</title>
<!--<script type="text/javascript" src="js/modernizr.js"></script>-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" defer src="js/jquery.flexslider.js"></script>

<script type="text/javascript" src="js/shCore.js"></script>
<script type="text/javascript" src="js/shBrushXml.js"></script>
<script type="text/javascript" src="js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="js/demo.js"></script>
<script src="js/jquery.easing.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script defer src="js/demo.js"></script>
<link href="styles/common/main.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/common/messages.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet"
	type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css"
	type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css"
	media="all" />

<!--  <link href="styles/shCore.css" rel="stylesheet" type="text/css" />
<link href="styles/shThemeDefault.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="styles/demo.css" type="text/css" media="screen" />
<link rel="stylesheet" href="styles/flexslider.css" type="text/css" media="screen" />-->

<script language="javascript" type="text/javascript">	
             <%-- This function is invoked when a document date is selected
                 from the "Date" drop-down list. --%>
                     function changeDocument() {
                         document.forms["docDateForm"].submit();
                     }
            <%-- This function is invoked when a section is selected from
                 the "Section" drop-down list. --%>
                     function gotoSection() {
                         document.forms["docSectionForm"].submit();
                     }
                     function resizeIframe() {
                         if (document.getElementById("frameimage")) {
                            var height = document.documentElement.clientHeight;
                            height -= document.getElementById('frameimage').offsetTop;

                            // not sure how to get this dynamically
                            height -= 20; /* whatever you set your body bottom margin/padding to be */

                            document.getElementById('frameimage').style.height = height +"px";
                         }
                     }
                     if (document.getElementById("frameimage")) {
                        document.getElementById('frameimage').onload = resizeIframe;
                        window.onresize = resizeIframe;
                        window.onload = resizeIframe;
                     }
					 function submitFav(button)
{
	
	var guids=document.getElementById("guid").value;
	var prId=document.getElementById("prID").value;
	var docId=document.getElementById("docId").value;

	window.open('\createfav.jsp?prID='+prId+'&guid='+guids+'&docId='+docId+'&type=doc','IndexResults','width=300,height=250,toolbar=no,resizable=yes,menubar=no,scrollbars=yes')
	          
				
}
        </script>
<script>
        function back()
        {
       // alert('${param.acct}');		
			//document.getElementById("backForm").action="${backURL}";
			//alert(document.getElementById("backForm").action);
			document.getElementById("backForm").submit;
			return true;
			
        }
        function submitpage(id)
		{
        	alert('Testing');
			alert(document.pressed);
			if(document.pressed != null || document.pressed != "")
			{//gotoPage
				if(document.pressed != "gotoPage")
				{
					if(document.pressed == "Next")
					{
					document.getElementById("pg").value =id+1;
					}
					else
						if(document.pressed == "Previous")
					{
					document.getElementById("pg").value = id-1;
					}
				}
				//alert(document.getElementById("urlType").value);
					document.getElementById("urlType").value="interface";

var url = "<c:out value='${ViewDocURL}'/>";
alert(url);

			document.getElementById("docPageForm").action="DocManagerNew1.action";
			//alert(document.getElementById("docPageForm").action);
			document.getElementById("docPageForm").submit;
			return true;
			}
			else
			{
				return false;
			}
		}
        </script>
<script>
</script>
</head>

<!-- Nithu Alexander: 06 Jul 2015
AVA-10: Fix for Cross-site framing  --> 
<%response.addHeader("X-Frame-Options","SAMEORIGIN");%>

<body>
	<table width="auto" height="auto" cellpadding="0" cellspacing="0" valign="top">
		<tr bgcolor="#ffffff">
                <td width="75%" align="left">
                    <%if (download.equalsIgnoreCase("true")) { %>
                    <c:choose>
                        <c:when test="${requestScope.pdfoutput}">
                            <%-- If this document supports output to PDF format, then enable the two PDF output buttons --%>
                            <c:url value="${renderURL}" var="ViewDocAsPdfURL">
                                <c:param name="of"  value="PDF"/>
                            </c:url>
                            <c:url value="${renderURL}" var="ViewDocAsPdfNoBgURL">
                                <c:param name="of"  value="PDF"/>
                                <c:param name="par" value="NoBG"/>
                            </c:url>
                            <%if (par.equalsIgnoreCase("rotleft")||par.equalsIgnoreCase("rotright")) {%>
                            	<img width="35" height="33" src="images/gpdf.gif" alt="View as PDF">
                           		<img width="35" height="33" src="images/gpdfnobg.gif" alt="View as PDF without background">
                           <%}else{%>
                           		<img width="35" height="33" src="images/cpdf.gif" onclick="window.open('<c:out value='${ViewDocAsPdfURL}'/>&act=savepdf','900','650')"     alt="View as PDF">
                            	<img width="35" height="33" src="images/cpdfnobg.gif" onclick="window.open('<c:out value='${ViewDocAsPdfNoBgURL}'/>&act=savepdf','900','650')" alt="View as PDF without background">
                           <%} %>
                             </c:when>
                        <c:otherwise>
                        	<%String df1=(String)request.getAttribute("docformat");
							  System.out.println("documentformat is"+df1);
								if (par.equalsIgnoreCase("rotleft")||par.equalsIgnoreCase("rotright")) {%>
                            	<img width="35" height="33" src="images/gpdf.gif"   alt="View as PDF"  >
                            	<img width="35" height="33" src="images/gpdfnobg.gif" alt="View as PDF without background">
                       
                           <%}else{ if(!df1.equalsIgnoreCase("collection")) { %>
                           		<img width="35" height="33" src="images/cpdf.gif"   alt="View as PDF" onclick="window.open('<c:out value='${renderURL}'/>','900','650')"     >
                           		<img width="35" height="33" src="images/gpdfnobg.gif" alt="View as PDF without background">
                           <%} }%>
                            
                        </c:otherwise>
                    </c:choose>
					<%} else if (print.equalsIgnoreCase("true")) {%>
                    <c:choose>
                        <c:when test="${requestScope.textoutput}">
                            <%-- If this document supports output to TEXT format, then enable the "View as Text" button --%>
							
                            <c:url value="${renderURL}" var="ViewDocAsTextURL">
							<c:choose>
							
                        <c:when test="${(requestScope.doctype == 'djdeline')}">

                               <c:param name="of"  value="TEXT"/>
							   </c:when>
							    <c:otherwise>
                           <c:param name="of"  value="PDF"/>
                        </c:otherwise>
                    </c:choose>
                                <c:param name="par" value="NoBG"/>
                            </c:url>
                            <img width="35" height="33" src="images/cprint.gif" onclick="window.open('<c:out value='${ViewDocAsTextURL}'/>&akhmad=print','900','650')" alt="View as Text">
                        </c:when>
                        <c:otherwise>
                            <img width="35" height="33" src="images/cprint.gif" onclick="window.open('<c:out value='${renderURL}'/>&akhmad=print','900','650')" alt="View as Text">
                        </c:otherwise>
                    </c:choose>
					
					<%}%>
					<%
					String df=request.getParameter("docformat");
					System.out.println("documentformat is"+df);
					
					String df1=(String)request.getAttribute("docformat");
					System.out.println("documentformat is"+df1);
					
					
					if(df1.equalsIgnoreCase("collection")) {
						System.out.println("equal here");
					
					%>
					
					<iframe src='${renderURL}' width="800" height="800"></iframe>
					
					<%
					}else{
					System.out.println("Not equal here");%>
			
                    <c:choose>
                        <c:when test="${requestScope.singlepagedoc || (requestScope.docformat == 'collection')}">
                        <%
                        System.out.println("Format is siglepagedoc");
                        %>
                        
                            <%-- If this is a single-page document or if this is a Collection format document,
                                 disable the page navigation buttons. Collection-format documents do not have
                                 the concept of pages and hence are not page addressable or viewable. --%>
                            <img width="35" height="33" src="images/gfirstpage.gif" alt="First Page">
                            <img width="35" height="33" src="images/grewpage.gif"   alt="Backward 5 Pages">
                            <img width="35" height="33" src="images/gprevpage.gif"  alt="Previous Page">
                            <img width="35" height="33" src="images/gnextpage.gif"  alt="Next Page">
                            <img width="35" height="33" src="images/gfastpage.gif"  alt="Forward 5 Pages">
                            <img width="35" height="33" src="images/glastpage.gif"  alt="Last Page">
                        </c:when>
                        <c:otherwise>
                            <c:url value="${ViewDocURL}" var="ViewDocFirstPageURL">
                                <c:param name="par" value="first"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocMinusFiveURL">
                                <c:param name="par" value="m5"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocMinusOneURL">
                                <c:param name="par" value="m1"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocPlusOneURL">
                                <c:param name="par" value="1"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocPlusFiveURL">
                                <c:param name="par" value="5"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocLastPageURL">
                                <c:param name="par" value="last"/>
                            </c:url>
                            <img width="35" height="33" src="images/cfirstpage.gif" onclick="location.href='<c:out value='${ViewDocFirstPageURL}'/>'" alt="First Page">
                            <img width="35" height="33" src="images/crewpage.gif"   onclick="location.href='<c:out value='${ViewDocMinusFiveURL}'/>'" alt="Backward 5 Pages">
                            <img width="35" height="33" src="images/cprevpage.gif"  onclick="location.href='<c:out value='${ViewDocMinusOneURL}'/>'"  alt="Previous Page">
                            <img width="35" height="33" src="images/cnextpage.gif"  onclick="location.href='<c:out value='${ViewDocPlusOneURL}'/>'"   alt="Next Page">
                            <img width="35" height="33" src="images/cfastpage.gif"  onclick="location.href='<c:out value='${ViewDocPlusFiveURL}'/>'"  alt="Forward 5 Pages">
                            <img width="35" height="33" src="images/clastpage.gif"  onclick="location.href='<c:out value='${ViewDocLastPageURL}'/>'"  alt="Last Page">
                        </c:otherwise>
                    </c:choose>
                    <c:url value="${ViewDocURL}" var="ViewDocZoomIn">
                    <%System.out.println("viewdocurl1"); %>
                        <c:param name="par" value="zoomin"/>
                    </c:url>
                    <img width="35" height="33" src="images/czoomin.gif"     onclick="location.href='<c:out value='${ViewDocZoomIn}'/>';WriteCookie(parseInt(ReadCookie())+parseInt(1));" alt="Zoom In">
                    <c:url value="${ViewDocURL}" var="ViewDocZoomNorm">
                        <c:param name="par" value="zoomnorm"/>
                    </c:url>
                    <img width="35" height="33" src="images/czoomnorm.gif"   onclick="location.href='<c:out value='${ViewDocZoomNorm}'/>';WriteCookie(parseInt(ReadCookie())+1);"         alt="Normal Zoom">
                    <c:url value="${ViewDocURL}" var="ViewDocZoomOut">
                        <c:param name="par" value="zoomout"/>
                    </c:url>
                    <img width="35" height="33" src="images/czoomout.gif"    onclick="location.href='<c:out value='${ViewDocZoomOut}'/>';WriteCookie(parseInt(ReadCookie())+1);"       alt="Zoom Out">
                    <c:url value="${ViewDocURL}" var="ViewDocRotateLeft">
                        <c:param name="par" value="rotleft"/>
                    </c:url>
                    <img width="35" height="33" src="images/crotleft.gif"   onclick="location.href='<c:out value='${ViewDocRotateLeft}'/>';WriteCookie(parseInt(ReadCookie())+1);"          alt="Rotate Left">
                                      
                    <c:url value="${ViewDocURL}" var="ViewDocRotateNorm">
                        <c:param name="par" value="rotnorm"/>
                    </c:url>
                        
                    <img width="35" height="33" src="images/crotnorm.gif"  onclick="location.href='<c:out value='${ViewDocRotateNorm}'/>';WriteCookie(parseInt(ReadCookie())+1);"       alt="Normal Rotation">
                    <c:url value="${ViewDocURL}" var="ViewDocRotateRight">
                        <c:param name="par" value="rotright"/>
                    </c:url>
                     <img width="35" height="33" src="images/crotright.gif"  onclick="location.href='<c:out value='${ViewDocRotateRight}'/>';WriteCookie(parseInt(ReadCookie())+1);"       alt="Rotate Right"> 
                    <c:url value="${ViewDocURL}" var="ViewDocMultiplePages">
                        <c:param name="par" value="multiplepages"/>
                    </c:url>
                   <%--  <img width="35" height="33" src="images/cface.gif"   onclick="location.href='<c:out value='${ViewDocMultiplePages}'/>'"          alt="Multiple Pages"> --%>
              		 <c:if test="${!(requestScope.docformat == 'collection')}">
                                <%-- Don't display the "Goto Page #" form for Collection format documents,
                                     as they don't have the concept of pages and page navigation. --%>
                                <%System.out.println("viewdocurl2"); %>
                                <form name="docPageForm" id="docPageForm" method="GET" onclick="return submitpage(<%=pageN%>);" align="top" >
								<input type="hidden" name="urlType" value="">
								      
                                	 <input type="hidden" name="acct" value="${param.acct}">
                                        <input type="hidden" name="db"   value="${param.db}">
                                        <input type="hidden" name="doc"  value="${doc}">
                                        <input type="hidden" name="rot"  value="${rot}">
                                        <input type="hidden" name="op"   value="view">
										 <input type="hidden" name="res"   value="${res}">
										  <input type="hidden" name="mult1"   value="view">
										  
                                    <%-- Page M of N of the Document --%>
                                   <table width="60%"><tr><td align="center">
                                        Page
                                        <c:choose>
                                            <c:when test="${requestScope.singlepagedoc}">
                                                <%-- For single page documents, only show "Page 1 of 1" --%>
                                                <input disabled="true" type="text" name="pg" id="pg" value="${pagenum}" size="8" maxlength="8"/>
                                                / <c:out value="${pagecnt}" />
                                            </c:when>
                                            <c:otherwise>
                                                <input type="text" name="pg" id="pg" value="${pagenum}" size="8" maxlength="8"/>
                                                / <c:out value="${pagecnt}" />
                                                 </c:otherwise>
                                        </c:choose>
                                        </td></tr></table>
                               
	</form>
	</c:if>
	</td>
	<td></td>
	</tr>


	<tr bgcolor="#ffffff">
		
			<td width = 100%>
				<div style="width: 100%; height:100%; float:left;  class="mainclass">
				  <%
				  System.out.println("viewdocurl3"); %>
				   <img src="<c:out value='${renderURL}'/>" style="border:1px solid black;"></img>
				<%System.out.println("viewdocur41"); %>
				</div>
			</td>
			<c:choose>
              <c:when test="${requestScope.par == null || requestScope.par == 'zoomnorm' ||requestScope.par == 'rotnorm'}">
                <%System.out.println("viewdocurl5"); %>
              <td align="top">
            <!--   	   <table width="100%" height="100%">
                 		<tr><td align="top" height="15%">
                 				<b> Current Document Pages </b>
								<div id="main" role="main" style="width: 27%;">
      								<section class="slider">
        								<div class="flexslider carousel" id="slider">
						 					<ul class="slides" id="mainslides">  					 
    										<!-- items mirrored twice, total of 12 -->
  						 				<!--	</ul>
										</div>
									</section>
				 				</div>
                 		</td></tr>
                 		<tr><td height="15%">
                 				<b> Attachments </b>
								<div id="main" role="main" >
      								<section class="slider">
        								<div class="flexslider carousel" id="slider1">
						 					<ul class="slides" id="attachments">  		--> 			 
    										<!-- items mirrored twice, total of 12 -->
  									<!-- 	</ul>
					   					</div>
					   				</section>
								</div>
                 		</td></tr>
                 		<tr></tr>
                 		<tr></tr>
                 	</table>--> 
                </td>
			 </c:when>
			 <c:otherwise></c:otherwise>
			 </c:choose>
			<%-- <table><tr><td><a href="<c:out value='${renderURL}'/>"><img src="<c:out value='${renderURL}'/>" alt="Document rendering" border="1"></a></td></tr></table> --%>
			<%-- <IFRAME id="frameimage" src="<c:out value='${fn:replace(renderURL,"%2f","")}'/>" height="100%" width="100%" scrolling="auto "frameborder="1">ERROR: your browser does not support the iframe tag</IFRAME> --%>
						
	<tr bgcolor="#ffffff"><%} %>
		<td colspan="2" height="10"></td>
	</tr>
	</table>

</body>
</html>
