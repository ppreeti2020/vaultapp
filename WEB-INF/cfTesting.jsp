<%-- 
    Document   : DocumentView
    Created on : Mar 9, 2009, 12:40:01 PM
    Author     : ssahmed
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%-- URL to render this current document but with some different
     parameters:

     - either at a different resolution
     - either at a different rotation
     - either a different page within the document
     - or with a "multiple pages" setting

     and so on. --%>
<c:url value="Interface" var="ViewDocURL">
    <c:param name="acct"     value="${param.acct}"/>
    <c:param name="db"       value="${param.db}"/>
    <c:param name="doc"      value="${doc}"/>
    <c:param name="pg"       value="${pagenum}"/>
    <c:param name="rot"      value="${rot}"/>
    <c:param name="res"      value="${res}"/>
    <c:param name="mult1"    value="${mult1}"/>
    <c:if test="${not empty param.par}">
        <c:param name="par"  value='${param.par}'/>
    </c:if>
    <c:param name="op"       value="view"/>
</c:url>

<%-- URL for the Document rendering servlet that actually does the work
     of rendering the document and returning back the appropriate
     rendered image output. --%>
<c:url value="Render.do" var="renderURL">
    <c:param name="acct"    value="${param.acct}"/>
    <c:param name="db"      value="${param.db}"/>
    <c:param name="dh"      value="${doc}"/>
    <c:param name="pg"      value="${pagenum}"/>
    <c:param name="pagecnt" value="${pagecnt}"/>
    <c:param name="par"     value='${par}'/>
    <c:param name="rot"     value="${rot}"/>
    <c:param name="res"     value="${res}"/>
    <c:param name="mult1"   value="${mult1}"/>
    <c:param name="mult2"   value="${mult2}"/>
    <c:param name="mult3"   value="${mult3}"/>
    <c:param name="mult4"   value="${mult4}"/>
    <c:if test="${not empty param.of}">
        <c:param name="of"  value='${param.of}'/>
    </c:if>
    <c:param name="docformat"   value="${requestScope.docformat}"/>
    <c:param name="doctype"     value="${requestScope.doctype}"/>
    <c:param name="imageoutput" value="${requestScope.imageoutput}"/>
    <c:param name="pdfoutput"   value="${requestScope.pdfoutput}"/>
</c:url>


<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Database <c:out value="${param.db}" />: Account # <c:out value="${param.acct}" />, Document <c:out value="${doc.date}" />, Page <c:out value="${pagenum}" /></title>
        <link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
		<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
		<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
		<meta name="helpHref" content="GettingStarted" />
		<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
		<link rel="stylesheet" href="styles/login.css" />
		<link rel="stylesheet" href="styles/common/messages.css" />
		<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
		
		<%-- Coverflow list --%>
        
        <script language="JavaScript" type="text/javascript" src="ContentFlow/contentflow_src.js" load="fancyScrollbar"></script>
        <link rel="stylesheet" title="Standard" href="styles.css" type="text/css" media="screen" />
        <script language="javascript" type="text/javascript">
           
           var cf = new ContentFlow('contentFlow', {reflectionHeight: 0, circularFlow: false});
        
            <%-- This function is invoked when a document date is selected
                 from the "Date" drop-down list. --%>
                     function changeDocument() {
                         document.forms["docDateForm"].submit();
                     }
            <%-- This function is invoked when a section is selected from
                 the "Section" drop-down list. --%>
                     function gotoSection() {
                         document.forms["docSectionForm"].submit();
                     }
                     function resizeIframe() {
                         if (document.getElementById("frameimage")) {
                            var height = document.documentElement.clientHeight;
                            height -= document.getElementById('frameimage').offsetTop;

                            // not sure how to get this dynamically
                            height -= 20; /* whatever you set your body bottom margin/padding to be */

                            document.getElementById('frameimage').style.height = height +"px";
                         }
                     }
                     if (document.getElementById("frameimage")) {
                        document.getElementById('frameimage').onload = resizeIframe;
                        window.onresize = resizeIframe;
                        window.onload = resizeIframe;
                     }
        </script>
        <script>
        function back()
        {
        	
        }
        
        </script>
    </head>
    <body onload="resizeIframe();" onresize="resizeIframe();" bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#000000" vlink="#000000" alink="#000000">
        <div id="page">
	<!-- Footer -->
	<div id="footer" class="clearfix"></div>
<div id="header" class="clearfix">
	<div id="branding">
		<img id="logo" src="images/header/logo.JPG" />
	</div>
	<div id="applicationDetail">
		<ul id="productName">
			<li>e2 Vault Application
</li>
		</ul>
		<ul id="userDetail">	
		</ul>
		<ul id="mainLinks">
		<li><a href="SearchAccount.action">Home</a></li>	
			<li><a href="Logout.action">Logout</a></li>	</ul>
	</div>
	<div id="curve">
		<img id="curve" src="styles/themes/purple/images/curve.gif" />
	</div>
	<hr />
</div>
	<!-- Main Content -->
	<div id="content">
		<div id="main"> 
		<form action="" name="">
<div align="right">
<input type="button" onclick="javascript: back()" value="Back" /></div>
</form>
	
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr bgcolor="#D0E0F0" valign="middle">
                <td colspan="2" align="center">
                    <c:choose>
                        <c:when test="${requestScope.pdfoutput}">
                            <%-- If this document supports output to PDF format, then enable the two PDF output buttons --%>
                            <c:url value="${renderURL}" var="ViewDocAsPdfURL">
                                <c:param name="of"  value="PDF"/>
                            </c:url>
                            <c:url value="${renderURL}" var="ViewDocAsPdfNoBgURL">
                                <c:param name="of"  value="PDF"/>
                                <c:param name="par" value="NoBG"/>
                            </c:url>
                            <img width="35" height="33" src="images/cpdf.gif" onclick="window.open('<c:out value='${ViewDocAsPdfURL}'/>&act=savepdf','900','650')"     alt="View as PDF">
                            <img width="35" height="33" src="images/cpdfnobg.gif" onclick="window.open('<c:out value='${ViewDocAsPdfNoBgURL}'/>&act=savepdf','900','650')" alt="View as PDF without background">
                        </c:when>
                        <c:otherwise>
                            <img width="35" height="33" src="images/cpdf.gif"   alt="View as PDF" onclick="window.open('<c:out value='${renderURL}'/>','900','650')"     >
                            <img width="35" height="33" src="images/gpdfnobg.gif" alt="View as PDF without background">
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${requestScope.textoutput}">
                            <%-- If this document supports output to TEXT format, then enable the "View as Text" button --%>
                            <c:url value="${renderURL}" var="ViewDocAsTextURL">
                               <c:param name="of"  value="PDF"/>
                                <c:param name="par" value="NoBG"/>
                            </c:url>
                            <img width="35" height="33" src="images/cprint.gif" onclick="window.open('<c:out value='${ViewDocAsTextURL}'/>&akhmad=print','900','650')" alt="View as Text">
                        </c:when>
                        <c:otherwise>
                            <img width="35" height="33" src="images/cprint.gif" onclick="window.open('<c:out value='${renderURL}'/>&akhmad=print','900','650')" alt="View as Text">
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${requestScope.tiffoutput}">
                            <%-- If this document supports output to TIFF format, then enable the "View as Tiff" button --%>
                            <c:url value="${renderURL}" var="ViewDocAsTiffURL">
                                <c:param name="of" value="TIFF"/>
                                <c:param name="pg" value="${requestScope.pagenum}"/>
                            </c:url>
                            <img width="35" height="33" src="images/ctiff.gif" onclick="location.href='<c:out value='${ViewDocAsTiffURL}'/>&act=saveTiff'" alt="View as TIFF">
                        </c:when>
                        <c:otherwise>
                            <img width="35" height="33" src="images/gtiff.gif" alt="View as TIFF">
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${requestScope.singlepagedoc || (requestScope.docformat == 'collection')}">
                            <%-- If this is a single-page document or if this is a Collection format document,
                                 disable the page navigation buttons. Collection-format documents do not have
                                 the concept of pages and hence are not page addressable or viewable. --%>
                            <img width="35" height="33" src="images/gfirstpage.gif" alt="First Page">
                            <img width="35" height="33" src="images/grewpage.gif"   alt="Backward 5 Pages">
                            <img width="35" height="33" src="images/gprevpage.gif"  alt="Previous Page">
                            <img width="35" height="33" src="images/gnextpage.gif"  alt="Next Page">
                            <img width="35" height="33" src="images/gfastpage.gif"  alt="Forward 5 Pages">
                            <img width="35" height="33" src="images/glastpage.gif"  alt="Last Page">
                        </c:when>
                        <c:otherwise>
                            <c:url value="${ViewDocURL}" var="ViewDocFirstPageURL">
                                <c:param name="par" value="first"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocMinusFiveURL">
                                <c:param name="par" value="m5"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocMinusOneURL">
                                <c:param name="par" value="m1"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocPlusOneURL">
                                <c:param name="par" value="1"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocPlusFiveURL">
                                <c:param name="par" value="5"/>
                            </c:url>
                            <c:url value="${ViewDocURL}" var="ViewDocLastPageURL">
                                <c:param name="par" value="last"/>
                            </c:url>
                            <img width="35" height="33" src="images/cfirstpage.gif" onclick="location.href='<c:out value='${ViewDocFirstPageURL}'/>'" alt="First Page">
                            <img width="35" height="33" src="images/crewpage.gif"   onclick="location.href='<c:out value='${ViewDocMinusFiveURL}'/>'" alt="Backward 5 Pages">
                            <img width="35" height="33" src="images/cprevpage.gif"  onclick="location.href='<c:out value='${ViewDocMinusOneURL}'/>'"  alt="Previous Page">
                            <img width="35" height="33" src="images/cnextpage.gif"  onclick="location.href='<c:out value='${ViewDocPlusOneURL}'/>'"   alt="Next Page">
                            <img width="35" height="33" src="images/cfastpage.gif"  onclick="location.href='<c:out value='${ViewDocPlusFiveURL}'/>'"  alt="Forward 5 Pages">
                            <img width="35" height="33" src="images/clastpage.gif"  onclick="location.href='<c:out value='${ViewDocLastPageURL}'/>'"  alt="Last Page">
                        </c:otherwise>
                    </c:choose>
                    <c:url value="${ViewDocURL}" var="ViewDocZoomIn">
                        <c:param name="par" value="zoomin"/>
                    </c:url>
                    <img width="35" height="33" src="images/czoomin.gif"     onclick="location.href='<c:out value='${ViewDocZoomIn}'/>'"         alt="Zoom In">
                    <c:url value="${ViewDocURL}" var="ViewDocZoomNorm">
                        <c:param name="par" value="zoomnorm"/>
                    </c:url>
                    <img width="35" height="33" src="images/czoomnorm.gif"   onclick="location.href='<c:out value='${ViewDocZoomNorm}'/>'"         alt="Normal Zoom">
                    <c:url value="${ViewDocURL}" var="ViewDocZoomOut">
                        <c:param name="par" value="zoomout"/>
                    </c:url>
                    <img width="35" height="33" src="images/czoomout.gif"    onclick="location.href='<c:out value='${ViewDocZoomOut}'/>'"         alt="Zoom Out">
                    <c:url value="${ViewDocURL}" var="ViewDocRotateLeft">
                        <c:param name="par" value="rotleft"/>
                    </c:url>
                    <img width="35" height="33" src="images/crotleft.gif"   onclick="location.href='<c:out value='${ViewDocRotateLeft}'/>'"          alt="Rotate Left">
                    <c:url value="${ViewDocURL}" var="ViewDocRotateNorm">
                        <c:param name="par" value="rotnorm"/>
                    </c:url>
                    <img width="35" height="33" src="images/crotnorm.gif"  onclick="location.href='<c:out value='${ViewDocRotateNorm}'/>'"       alt="Normal Rotation">
                    <c:url value="${ViewDocURL}" var="ViewDocRotateRight">
                        <c:param name="par" value="rotright"/>
                    </c:url>
                    <img width="35" height="33" src="images/crotright.gif"  onclick="location.href='<c:out value='${ViewDocRotateRight}'/>'"       alt="Rotate Right">
                    <c:url value="${ViewDocURL}" var="ViewDocMultiplePages">
                        <c:param name="par" value="multiplepages"/>
                    </c:url>
                    <img width="35" height="33" src="images/cface.gif"   onclick="location.href='<c:out value='${ViewDocMultiplePages}'/>'"          alt="Multiple Pages">
              		 <c:if test="${!(requestScope.docformat == 'collection')}">
                                <%-- Don't display the "Goto Page #" form for Collection format documents,
                                     as they don't have the concept of pages and page navigation. --%>
                                <form name="docPageForm" method="GET" action="Interface">
                                	 <input type="hidden" name="acct" value="${param.acct}">
                                        <input type="hidden" name="db"   value="${param.db}">
                                        <input type="hidden" name="doc"  value="${doc}">
                                        <input type="hidden" name="rot"  value="${rot}">
                                        <input type="hidden" name="op"   value="view">
                                    <%-- Page M of N of the Document --%>
                                   <table width="100%"><tr><td>
                                   <input type="submit" name="submit" value="Previous"/>
                                   </td><td>
                                        Page
                                        <c:choose>
                                            <c:when test="${requestScope.singlepagedoc}">
                                                <%-- For single page documents, only show "Page 1 of 1" --%>
                                                <input disabled="true" type="text" name="pg" value="${pagenum}" size="8" maxlength="8"/>
                                                / <c:out value="${pagecnt}" />
                                            </c:when>
                                            <c:otherwise>
                                                <input type="text" name="pg" value="${pagenum}" size="8" maxlength="8"/>
                                                / <c:out value="${pagecnt}" />
                                                <input type=image alt="Go to Page" src="images/cgo.gif" border=0 name="image" align=absMiddle>
                                            </c:otherwise>
                                        </c:choose>
                                       
                                        </td><td>
                                        <input type="submit" name="submit" value="Next"/>
                              </td></tr></table>
                                </form>
                            </c:if>
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="5">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
            <tr bgcolor="#ffffff">
            		<c:choose><c:when test="${mult1 == '2' || mult1 == '4'}">
                <td colspan="0">
                </c:when><c:otherwise>
                <td colspan="2" width="100" height= "100">
                </c:otherwise></c:choose>
                    <div align=center>
                        <c:choose>
                            <%-- When rendering a Collection format document,
                                 or a native PDF format document,
                                 or if the forcepdf app config param is set
                                 display the document rendering in an IFrame.
                            --%>
                            <c:when test="${(requestScope.docformat == 'collection') || (requestScope.docformat == 'pdf') || initParam.forcepdf}">
                                <c:choose>
                                    <c:when test="${fn:contains(requestScope.doctype, 'mht')}">
                                        <%-- Remove "%2f" chars embedded in the rendering URL otherwise certain Windows OSes
                                             will prevent the images in the MHT document from being viewed in the browser. --%>
                                        <IFRAME id="frameimage" test='111' src="<c:out value='${fn:replace(renderURL,"%2f","")}'/>" onload="resizeIframe();" width="100%" scrolling="auto "frameborder="1">ERROR: your browser does not support the iframe tag</IFRAME>
                                    </c:when>
                                    <c:otherwise>
                                        <IFRAME id="frameimage"  test='222' src="<c:out value='${renderURL}'/>" onload="resizeIframe();" width="100%" scrolling="auto "frameborder="1">ERROR: your browser does not support the iframe tag</IFRAME>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                               <div class="maincontent"></div>

     <!-- ===== FLOW ===== -->
    <div id="contentFlow" class="ContentFlow">
        <!-- should be place before flow so that contained images will be loaded first -->
        <div class="loadIndicator"><div class="indicator"></div></div>

        <div class="flow" id="flowitems">

            <div class="item">
                <img class="content" src="images/Access.jpg"/>
                <div class="caption">pic0: some stripes</div>
            </div>
            <div class="item">
                <img class="content" src="<c:out value='${renderURL}'/>" alt="Document rendering" border="3"/>
                <div class="caption">pic1: some stripes</div>
            </div>
            <c:forEach var="i" begin="1" end="${pagecnt}">
         			<c:url value='${renderURL}' var="ViewDocMultiplePage">
         			<c:param name="pg" value="${i}"/></c:url>
         			$('#flowitems').append('<div class="item"><img class="content" src="<c:out value='${ViewDocMultiplePage}'/>" alt="Document rendering" border="1" bordercolor="#ff0000" /><div class="caption">Page <c:out value='${i}'/></div></div>');
         		</c:forEach>

        </div>
        <div class="globalCaption"></div>
        <div class="scrollbar">
            <div class="slider"><div class="position"></div></div>
        </div>

    </div>
            <%--   <a href="<c:out value='${renderURL}'/>"><img src="<c:out value='${renderURL}'/>" alt="Document rendering" border="1"></a> --%>
                            </c:otherwise>
                        </c:choose>
                        <%-- <table><tr><td><a href="<c:out value='${renderURL}'/>"><img src="<c:out value='${renderURL}'/>" alt="Document rendering" border="1"></a></td></tr></table> --%>
                        <%-- <IFRAME id="frameimage" src="<c:out value='${fn:replace(renderURL,"%2f","")}'/>" height="100%" width="100%" scrolling="auto "frameborder="1">ERROR: your browser does not support the iframe tag</IFRAME> --%>
                    </div>
                </td>
                <c:choose><c:when test="${mult1 == '2' || mult1 == '4'}">
                <c:choose><c:when test="${mult2 > pagecnt}"></c:when><c:otherwise>
                <td colspan="0">
                    <div align=center>
                        <table><tr>
                        				<td>
                        					
                        					<c:url value="${renderURL}" var="ViewDocMultiplePage2">
                        						<c:param name="pg" value="${mult2}"/>
                    							</c:url>
                        					<a href="<c:out value='${ViewDocMultiplePage2}'/>"><img src="<c:out value='${ViewDocMultiplePage2}'/>" alt="Document rendering" border="1"></a>
                        				  
                        				</td>
                        			 </tr>
                        </table>
                    </div>						
                </td>
                </c:otherwise></c:choose>
                </c:when><c:otherwise></c:otherwise></c:choose>
            </tr>
            <c:choose><c:when test="${mult1 == '4'}">
            <tr bgcolor="#ffffff">
            	<c:choose><c:when test="${mult3 > pagecnt}"></c:when><c:otherwise>
                <td colspan="0">
                    <div align=center>
                        <table><tr>
                        				<td>
                        					
                        					<c:url value="${renderURL}" var="ViewDocMultiplePage3">
                        						<c:param name="pg" value="${mult3}"/>
                    							</c:url>
                        					<a href="<c:out value='${ViewDocMultiplePage3}'/>"><img src="<c:out value='${ViewDocMultiplePage3}'/>" alt="Document rendering" border="1"></a>
                        				  
                        				</td>
                        			 </tr>
                        </table>
                    </div>						
                </td>
                <c:choose><c:when test="${mult4 > pagecnt}"></c:when><c:otherwise>
                <td colspan="0">
                    <div align=center>
                        <table><tr>
                        				<td>
                        					
                        					<c:url value="${renderURL}" var="ViewDocMultiplePage4">
                        						<c:param name="pg" value="${mult4}"/>
                    							</c:url>
                        					<a href="<c:out value='${ViewDocMultiplePage4}'/>"><img src="<c:out value='${ViewDocMultiplePage4}'/>" alt="Document rendering" border="1"></a>
                        				  
                        				</td>
                        			 </tr>
                        </table>
                    </div>						
                </td>
              </c:otherwise></c:choose>
              </c:otherwise></c:choose>
            </tr>
            </c:when><c:otherwise></c:otherwise></c:choose>
            <tr bgcolor="#ffffff">
                <td colspan="2" height="10">
                </td>
            </tr>
        </table>
        </div>
    </body>
</html>
