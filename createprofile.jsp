<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec"%>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet"
	type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css"
	type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css"
	media="all" />
<title>e2 Vault Administration</title>
<script type="text/javascript">
	
</script>
</head>
<body>
	<jsp:include page="CommonTabs.jsp" />

	<div id="countrydivcontainer"
		style="border: 1px solid gray; width: 1230px; height: 610px; margin-bottom: 1em; padding: 10px; overflow: auto">


		<jsp:include page="userpadminCommonTabs.jsp" />

		<div id="countrydivsubcontainer"
			style="border: 1px solid gray; width: 1180px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">

			<s:if test="hasActionErrors()">
				<s:actionerror />
			</s:if>
			<font color="#ff0000"><s:property value="message" />
			</font>
			<s:form action="PopulateProfileAction">
				<table>
					<tr>
						<td valign="top" align="center"></td>
						<td></td>
					</tr>
					<tr>
						<td width="300"><B>Profile Name : </B><input name="profileName" type="text" />
															  <input name="profileAction" type="hidden" value = "create"/></td>
						<td width="200"><input type="submit" value="Setup Profile" />
						</td>
					</tr>
				</table>
				
			</s:form>
			</div>
			</div>
</body>
</html>