<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dto.DataDTO" %>
<html>
<head>
<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	
	<script src="js/jquery_treeview.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>
<script type="text/javascript" src="js/demo.js"></script>
</head>
<%
Map<Long,DataDTO>  treeMapList = request.getAttribute("treeMapList")!=null?(Map<Long,DataDTO>)request.getAttribute("treeMapList"):new HashMap<Long,DataDTO>();
Map<Integer,DataDTO>  treeProfile = request.getAttribute("treeProfile")!=null?(Map<Integer,DataDTO>)request.getAttribute("treeProfile"):new HashMap<Integer,DataDTO>();
System.out.println("map:"+treeMapList);
int j=1;
%>
<body>
<ul id="browser" class="filetree treeview">
	<% for(Long rootId: treeMapList.keySet()){
		DataDTO ddao = (DataDTO)treeMapList.get(rootId);
		%><li> <span class="folder"><a href="ServerAuditAction.action?server=server<%=j%>&pg=2"><%= ddao.getHostName()%></a></span>
		<ul>
			
					<li><span class="folder">
					<a href="AuditProfilesAction.action?name=Indexing&hostId=<%=rootId%>&server=server<%=j%>">Profiles</a>
					</span>
					<% for(Integer rootId1: treeProfile.keySet()){
		
		
			DataDTO ddao1 = (DataDTO)treeProfile.get(rootId1);
			if(ddao1.getHostId() == rootId){
			System.out.println(""+ddao1.getProfile());
		%> 
					<ul><li><span class="folder">
					<a href="AuditProfileAction.action?dbId=<%=ddao1.getDbId()%>&hostId=<%= ddao1.getHostId()%>&profile=<%= ddao1.getProfile()%>&server=server<%=j%>"><%= ddao1.getProfile()%> </a>
					</span>
						<ul>
							<li><span class="file">
							<a href="IndexAuditAction.action?dbId=<%=ddao1.getDbId()%>&hostId=<%=rootId%>&profile=<%= ddao1.getProfile()%>&server=server<%=j%>">Index</a>
							</span></li>
							
							<li><span class="file">
							<a href="AuditPageDataAction.action?dbId=<%=ddao1.getDbId()%>&hostId=<%=rootId%>&profile=<%= ddao1.getProfile()%>&server=server<%=j%>">Page Data</a>
							</span></li>
							<li><span class="file">
							<a href="AuditDocDataAction.action?dbId=<%=ddao1.getDbId()%>&hostId=<%=rootId%>&profile=<%= ddao1.getProfile()%>&server=server<%=j%>">DocData</a>
							</span></li>
						<li><span class="file">
							<a href="ResourcesAuditAction.action?dbId=<%=ddao1.getDbId()%>&hostId=<%=rootId%>&profile=<%= ddao1.getProfile()%>&server=server<%=j%>">Resources</a>
							</span></li>
						</ul>
					</li></ul>
					<%}}%>
					</li>
					
				</ul>
			</li>
		<%j++;}%>
	
</ul>
</body>
</html>