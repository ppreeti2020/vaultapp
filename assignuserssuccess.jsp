<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.pb.dao.DataDAO" %>
<%@page import="com.pb.dto.UserDTO" %>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec" %>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<title>e2 Vault Administration</title>
<script type="text/javascript">

	
</script>
</head>
<body>
<jsp:include page="CommonTabs.jsp" />

<%--Nithu Alexander: 17 Aug 2015, Changing width from 1200px to 100%, height from 610px to 100% --%>
<div id="countrydivcontainer" style="border:1px solid gray; width:100%; height:100%; margin-bottom: 1em; padding: 10px; overflow:auto">

<!-- Nithu Alexander: 13 Aug 2015 , Adding useradminCommonTabs.jsp and countrydivsubcontainer as part of Tabs changes in Profile Admin-->
<jsp:include page="useradminCommonTabs.jsp" />
<%--Nithu Alexander: 24 Aug 2015, Changing width from 1150px to 1200px --%>
<div id="countrydivsubcontainer" style="border:1px solid gray; width:1200px; height:565px; margin-bottom: 1em; padding: 10px; overflow:auto">

<table align="center"  width="100%">
<tr>
<td>
</td>
</tr>
<tr>
<td>

<s:if test="hasActionErrors()">

      
  <s:actionerror />
 
 
</s:if><B><font color="#ff0000"><s:property value="message" /></font></B>
<table width="100%" border="1px solid #778" height="500" align="top">
	
	<tr><td valign="top" align="left">
	
		</td></tr></table>
	</td></tr></table>
		</div>
		</div>
</body>
</html>