package com.pb.ocbc;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.g1.e2.vault.OutputFormat;
import com.g1.e2.vault.RenderOptions;
import com.g1.e2.vault.ReprintOptions;
import com.g1.e2.vault.ResolutionWidth;
import com.g1.e2.vault.Rotation;
import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.SearchMatches;
import com.g1.e2.vault.SearchMatchesIterator;
import com.g1.e2.vault.ServerErrorException;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Account;
import com.g1.e2.vault.VaultClient.Database;
import com.g1.e2.vault.VaultClient.Document;
import com.g1.e2.vault.VaultException;
import com.lowagie.tools.Executable;
import com.opensymphony.xwork2.ActionContext;
//import com.pb.dao.DataDAO;
import com.pb.admin.Common;
import com.pb.common.CommonConstants;
import com.pb.common.PrintTest;
import com.pb.common.PropertyUtils;
import com.pb.dao.DataDAO;
import com.pb.dao.DocMngrDAOOld;
import com.pb.dao.DocMngrDAO;
import com.pb.dao.ResultDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.UserDTO;
import com.pb.evault.DocumentHandle;

public class DocManagerNew1 extends BaseAction implements ServletRequestAware,
		ServletResponseAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static final Logger logger = Logger.getLogger(DocManagerNew1.class);
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private String host;
	private String message;
	private String prName;
	private Long prdbID;
	private Map<Long, DataDAO> dbMapList;
	private Map<Long, String> searchIdxList;
	private List<String> selectIdxList;
	private Map<Integer, Map<String, String>> docList;
	private String actType;
	private DocumentHandle docHandle;
	private int pageSize;
	private String accountNumber;
	private String databaseName;
	private boolean search;
	private String primarySearch;
	private List<String> secondarySearch;
	private String pageType;
	private List actionList;
	private List indexList;
	private String primaryidx;
	private String crossSearch;
	private Map<String, String> idxList;
	private String tabId;
	private String vaultIndex;
	private String vaultIndexDesc;
	private String pidxName;

	
	public String getPidxName() {
		return pidxName;
	}

	public void setPidxName(String pidxName) {
		this.pidxName = pidxName;
	}
	
	public String getVaultIndexDesc() {
		return vaultIndexDesc;
	}

	public void setVaultIndexDesc(String vaultIndexDesc) {
		this.vaultIndexDesc = vaultIndexDesc;
	}

	public String getVaultIndex() {
		return vaultIndex;
	}

	public void setVaultIndex(String vaultIndex) {
		this.vaultIndex = vaultIndex;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public String getCrossSearch() {
		return crossSearch;
	}

	public void setCrossSearch(String crossSearch) {
		this.crossSearch = crossSearch;
	}

	public Map<String, String> getIdxList() {
		return idxList;
	}

	public void setIdxList(Map<String, String> idxList) {
		this.idxList = idxList;
	}

	public String getPrimaryidx() {
		return primaryidx;
	}

	public void setPrimaryidx(String primaryidx) {
		this.primaryidx = primaryidx;
	}

	public List getIndexList() {
		return indexList;
	}

	public void setIndexList(ArrayList indexList) {
		this.indexList = indexList;
	}

	public List getActionList() {
		return actionList;
	}

	public void setActionList(ArrayList actList) {
		this.actionList = actList;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getPageType() {
		return pageType;
	}

	public void setSecondarySearch(List<String> secondarySearch) {
		this.secondarySearch = secondarySearch;
	}

	public List<String> getSecondarySearch() {
		return secondarySearch;
	}

	public void setPrimarySearch(String primarySearch) {
		this.primarySearch = primarySearch;
	}

	public String getPrimarySearch() {
		return primarySearch;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public boolean getSearch() {
		return search;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setDocHandle(DocumentHandle docHandle) {
		this.docHandle = docHandle;
	}

	public DocumentHandle getDocHandle() {
		return docHandle;
	}

	public void setActType(String actType) {
		this.actType = actType;
	}

	public String getActType() {
		return actType;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setDbMapList(Map<Long, DataDAO> dbMapList) {
		this.dbMapList = dbMapList;
	}

	public Map<Long, DataDAO> getDbMapList() {
		return dbMapList;
	}

	public Long getPrdbID() {
		return prdbID;
	}

	public void setPrdbID(Long prdbID) {
		this.prdbID = prdbID;
	}

	public String getPrName() {
		return prName;
	}

	public void setPrName(String prName) {
		this.prName = prName;
	}

	public Map<Long, String> getSearchIdxList() {
		return searchIdxList;
	}

	public void setSelectIdxList(List<String> selectIdxList) {
		this.selectIdxList = selectIdxList;
	}

	public List<String> getSelectIdxList() {
		return selectIdxList;
	}

	public void ResultIdxList(Map<Long, String> searchIdxList) {
		this.searchIdxList = searchIdxList;
	}

	public void setDocList(Map<Integer, Map<String, String>> docList) {
		this.docList = docList;
	}

	public Map<Integer, Map<String, String>> getDocList() {
		return docList;
	}

	public String execute() {
		setTabId("Repository");
		DocMngrDAO dao = new DocMngrDAO();
		Long UserId = 0L;
		Long DbID = 0L;
		UserDAO userDao = new UserDAO();
		long logId = 0L;
		host = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.HOST_NAME);
		vaultIndex = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.VAULT_IDX);
		System.out.println("hostName:::" + host);
		ResultDAO resDao = new ResultDAO();
		dbMapList = new HashMap<Long, DataDAO>();
		idxList = new HashMap<String, String>();
		indexList = new ArrayList();
		HttpSession sessionku = getServletRequest().getSession();
		logId = sessionku.getAttribute("logId") != null ? Long
				.parseLong(sessionku.getAttribute("logId").toString()) : 0L;
		long serId = getSession().get("SerId") != null ? Long
				.parseLong(getSession().get("SerId").toString()) : 0L;

		long sresId = getSession().get("sresId") != null ? Long
				.parseLong(getSession().get("sresId").toString()) : 0L;
		System.out.println("USER ID" + getSession().get("UserID") + " DBID : "
				+ servletRequest.getParameter("prdbID"));

		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		if (getSession().get("UserID") != null) {
			UserId = Long.parseLong(getSession().get("UserID").toString());
		}
		if ((UserId != null || !UserId.equals(0))) {
			try {
				dbMapList = dao.getdbResults();
				System.out.println("dbMapList size::" + dbMapList.size());
				System.out.println("ProfileDBID:::" + prdbID);
				if (servletRequest.getParameter("prdbID") != null
						&& !servletRequest.getParameter("prdbID").equals("0")) {
					search = false;
					String selectStr = "";
					actionList = userDao.getUserActions(UserId);
					
					indexList = userDao.getIndexUsersList(prdbID, UserId, "Y");
					System.out.println("IndexList Size in DocManagernewl:::"
							+ indexList.size());
					
					vaultIndexDesc = userDao.getIndexDescforName(prdbID,
							vaultIndex);
					if (indexList.size() != 0) {
											
						searchIdxList = new HashMap<Long, String>();
						Long idx = 0L;
						String idxName = "";
						for (Iterator iter = indexList.iterator(); iter
								.hasNext();) {
							UserDTO dto = (UserDTO) iter.next();
							idx = dto.getIndexId();
							idxName = "Idx_"+dto.getIndexName()+"_"+dto.getIndexDesc();
							searchIdxList.put(idx, idxName); 
						}

						List<String> selidxList = userDao.getUserColumnIndex(
								prdbID, UserId, "Y");
						System.out.println("selidxList Size:::"
								+ selidxList.size());
						selectIdxList = new ArrayList<String>();
						for (String idxstr : selidxList) {
							String[] tempidx = idxstr.split("_");
							Long idx1 = Long.parseLong(tempidx[1]);
							// String idxName1 =
							// userDao.getIndexName(prdbID,idx1);
							String idxName1 = tempidx[3];
							System.out.println("IndexID:::" + idx1);
							System.out.println("IndexName:::" + idxName1);
							selectIdxList.add(idxName1);
						}
						int j = 1;
						for (String idx2 : selectIdxList) {
							if (j == selectIdxList.size())
								selectStr = selectStr + idx2;
							else
								selectStr = selectStr + idx2 + ",";
							j++;
						}
						if (!selectIdxList.contains(vaultIndex)) {
							selectStr = selectStr + "," + vaultIndex;
						}
						if (!selectIdxList.contains("invlink")) {
							selectStr = selectStr + ",invlink";
						}
						System.out.println("search value::" + search);

						System.out.println("select Str:::" + selectStr + "--"
								+ selectIdxList.size());
						String tableName = servletRequest
								.getParameter("prName");
						docList = new HashMap<Integer, Map<String, String>>();
						secondarySearch = new ArrayList<String>();
						primarySearch = "";
						Enumeration params = servletRequest.getParameterNames();
						while (params.hasMoreElements()) {
							String paramName = params.nextElement().toString();
							String paramValue = servletRequest
									.getParameter(paramName);
							System.out.println("Parameter Names" + paramName);
							System.out.println("Parameter Values" + paramValue);
							if (paramName.startsWith("textSearchs_Pri")
									&& paramValue.length() != 0) {
								search = true;
								String temp[] = paramName.split("_");
								primarySearch = temp[2] + "_" + paramValue;
								System.out.println("PrimarySearch:::"
										+ primarySearch);
							} else if (paramName.startsWith("textSearchs_Sec")
									&& paramValue.length() != 0) {
								String temp[] = paramName.split("_");
								String secsrc = temp[2] + "_" + paramValue;
								System.out.println("SecondarySerach:::"
										+ secsrc);
								secondarySearch.add(secsrc);
							}
						}
						if (search == true
								&& !(servletRequest.getParameter("crossSearch") != null)) {
							System.out.println("Inside else of search:::");
							String conds = " where ";
							conds = conds + pidxName + " = '"
									+ primarySearch.split("_")[1] + "' and ";

							for (String str : secondarySearch) {
								Long sidx = Long.parseLong(str.split("_")[0]);
								String sIdxName = userDao.getIndexName(prdbID,
										sidx);
								conds = conds + sIdxName + " = '"
										+ str.split("_")[1] + "' and ";
							}
							System.out.println("conds::" + conds);
							int index = conds.lastIndexOf("and");
							if (index != -1) {
								conds = conds.substring(0, index);
							}
							System.out.println("conds::" + conds);
							docList = dao.getDocData(tableName, selectStr,
									conds);
						}

						System.out.println("docList Size in DocManager:::"
								+ docList.size());
					} else
						message = "Please configure search, result and cross reference Indexes!!!";
					System.out.println("Cross Search value::"
							+ servletRequest.getParameter("crossSearch"));
					if (servletRequest.getParameter("crossSearch") != null
							&& !servletRequest.getParameter("crossSearch")
									.equals(" ")) {
						setCrossSearch("yes");
						docList = new HashMap<Integer, Map<String, String>>();
						Map<String, String> xrefValues = new HashMap<String, String>();
						for (String idx : idxList.keySet()) {
							xrefValues.put(idx,
									servletRequest.getParameter(idx));
						}
						System.out.println("xrefValues>>" + xrefValues);
						String tableName = servletRequest
								.getParameter("prName");
						String xrefTableName = tableName + "_CROSSREF";
						docList = dao.getCrossRefData(xrefTableName, tableName,
								xrefValues, selectStr, pidxName);
						for (int i : docList.keySet()) {
							System.out.println("docList in DocManager:::"
									+ docList.get(i));
						}

					}

					if (servletRequest.getParameter("vaultIdx") != null
							&& servletRequest.getParameter("actType") != null) {
						System.out.println("vaultIdx:docManager-->"
								+ servletRequest.getParameter("vaultIdx"));
						SearchMatches<Document> sm = null;
						List<Document> documents = null;
						String dbName = userDao.getDBName(UserId, prdbID);
						System.out.println("Dbname:::" + dbName);
						List<DataDAO> list = dao.getDocInfo(vaultIndex,
								servletRequest.getParameter("vaultIdx"),
								servletRequest.getParameter("prName"));
						System.out.println("List Size:::" + list.size());
						if (servletRequest.getParameter("actType")
								.equalsIgnoreCase("view")) {
							actType = "view";
							pageType = "search" + UserId;
							System.out.println("pageType::" + pageType);
							sessionku.setAttribute("pageType", pageType);
							for (DataDAO ddao : list) {
								System.out.println("coming here:dbName::"
										+ dbName);
								Database db = Common.getDBConnection(dbName);
								System.out.println("coming here:1::" + db);
								System.out.println("coming here:2::account"
										+ ddao.getAccNumber());
								Account acct = db.getAccount(ddao
										.getAccNumber());
								System.out.println("coming here:accoutValut::"
										+ acct);
								System.out.println("coming here:date::"
										+ ddao.getCreated_dt());
								DocumentHandle selectedDocHandle = new DocumentHandle();
								Document selectedDoc = null;
								System.out.println("acct");
								if (acct != null) {
									System.out.println("Inside acct null loop");
									SearchMatchesIterator<Document> si = acct
											.searchForDocuments(
													ddao.getCreated_dt(),
													Integer.MAX_VALUE);
									int expectedResult = 500, i = 0;
									docHandle = new DocumentHandle();
									a: while (si.hasNext()) {
										List<Document> docs = si.next();
										for (Document d1 : docs) {
											System.out
													.println("Inside doc loop::"
															+ d1);
											System.out
													.println("DocumentInfo:::"
															+ d1.getDocumentInfo()
																	.getCustomAttributes());
											Map<String, List<String>> docInfo = new HashMap<String, List<String>>();
											docInfo = d1.getDocumentInfo()
													.getCustomAttributes();
											for (String s : docInfo.keySet()) {
												System.out
														.println("Vault cusotm attributes::"
																+ s);
												if (s.equalsIgnoreCase(vaultIndex)) {
													if (vaultIndex
															.equalsIgnoreCase("DRN")) {
														System.out
																.println("docInfo.get(s)>>"
																		+ docInfo
																				.get(s));
														System.out
																.println("ddao.getDocRefNo()>>"
																		+ ddao.getDocRefNo());
														System.out
																.println("docInfo.get(s).contains(ddao.getDocRefNo())>>>"
																		+ docInfo
																				.get(s)
																				.contains(
																						ddao.getDocRefNo()));
														if (docInfo
																.get(s)
																.contains(
																		ddao.getDocRefNo())) {
															System.out
																	.println("Inside 3");
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													} else if (vaultIndex
															.equalsIgnoreCase("Guid")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getGuid())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													} else if (vaultIndex
															.equalsIgnoreCase("DocId")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getDocId())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													}
												}
											}
											System.out.println(d1);
											if (i > expectedResult) {
												System.out
														.println("Database has more than "
																+ expectedResult
																+ " statements");
												break a;
											}
											i++;
										}
									}

									docHandle = selectedDocHandle;
									databaseName = db.getName();
									HashMap map = new HashMap();
									if (logId != 0) {
										map.put("DBName", databaseName);
										map.put("accNum", accountNumber);
										map.put("Name", acct.getName());
										map.put("count", 0);
										map.put("serId", serId);
										map.put("logId", logId);
										map.put("sresId", sresId);
										long accId = resDao
												.saveAccountDetails(map);
										// session.put("accId", accId);

										map.put("docName",
												selectedDoc.getName());
										map.put("Desc",
												selectedDoc.getDocumentInfo());
										map.put("docType",
												selectedDoc.getType());
										map.put("docDate",
												selectedDoc.getDate());
										map.put("accId", accId);
										map.put("logId", logId);
										// sessionku.setAttribute("logId",
										// logId);
										long docuId = resDao
												.saveDocumentDetails(map);
										if (docuId != 0) {
											map.put("logId", logId);
											map.put("docId", docuId);
											map.put("action", "view");
											resDao.saveDocumentActionDetails(map);
										}
									}
									sessionku.setAttribute("SELECT_DB",
											db.getName());
									sessionku.setAttribute("SELECT_DB_DESC",
											db.getDescription());
								}

							}
						} else if (servletRequest.getParameter("actType")
								.equalsIgnoreCase("download")) {
							actType = "download";
							System.out.println("actType::"
									+ servletRequest.getParameter("actType"));
							VaultClient vaultClient = Common
									.getVaultConnection();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							OutputStream output = getServletResponse()
									.getOutputStream();
							getServletResponse().setHeader(
									"Content-Disposition",
									"attachment; filename=\"download.pdf\"");
							getServletResponse().setContentType(
									"application/x-download");
							output = getServletResponse().getOutputStream();
							for (DataDAO ddao : list) {

								System.out.println("coming here:dbName::"
										+ dbName);
								Database db = Common.getDBConnection(dbName);
								System.out.println("coming here:1::" + db);
								System.out.println("coming here:2::account"
										+ ddao.getAccNumber());
								Account acct = db.getAccount(ddao
										.getAccNumber());
								System.out.println("coming here:accoutValut::"
										+ acct);
								System.out.println("coming here:date::"
										+ ddao.getCreated_dt());
								if (acct != null) {
									SearchMatchesIterator<Document> si = acct
											.searchForDocuments(
													ddao.getCreated_dt(),
													Integer.MAX_VALUE);
									int expectedResult = 500, i = 0;
									DocumentHandle selectedDocHandle = new DocumentHandle();
									Document selectedDoc = null;
									docHandle = new DocumentHandle();
									a: while (si.hasNext()) {
										List<Document> docs = si.next();
										for (Document d1 : docs) {
											System.out
													.println("DocumentInfo:::"
															+ d1.getDocumentInfo()
																	.getCustomAttributes());
											Map<String, List<String>> docInfo = new HashMap<String, List<String>>();
											docInfo = d1.getDocumentInfo()
													.getCustomAttributes();
											for (String s : docInfo.keySet()) {
												if (s.equalsIgnoreCase(vaultIndex)) {
													if (vaultIndex
															.equalsIgnoreCase("DRN")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getDocRefNo())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													} else if (vaultIndex
															.equalsIgnoreCase("Guid")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getGuid())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													} else if (vaultIndex
															.equalsIgnoreCase("DocId")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getDocId())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													}
												}
											}
											System.out.println(d1);
											if (i > expectedResult) {
												System.out
														.println("Database has more than "
																+ expectedResult
																+ " statements");
												break a;
											}
											i++;
										}
									}
									docHandle = selectedDocHandle;
									RenderOptions renderOptions = new RenderOptions();
									renderOptions.setEnableBackground(true);
									vaultClient.renderDirect(db.getName(),
											accountNumber, docHandle.getDate(),
											docHandle.getFile(),
											docHandle.getPointer(),
											OutputFormat.PDF, 1,
											selectedDoc.getPageCount(),
											ResolutionWidth.NONE,
											Rotation.NONE, renderOptions, baos);

									output.write(baos.toByteArray());

								}
							}
							output.close();
							return null;
						} else if (servletRequest.getParameter("actType")
								.equalsIgnoreCase("print")) {

							actType = "print";
							System.out.println("actType::"
									+ servletRequest.getParameter("actType"));
							VaultClient vaultClient = Common
									.getVaultConnection();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();

							for (DataDAO ddao : list) {

								System.out.println("coming here:dbName::"
										+ dbName);
								Database db = Common.getDBConnection(dbName);
								System.out.println("coming here:1::" + db);
								System.out.println("coming here:2::account"
										+ ddao.getAccNumber());
								Account acct = db.getAccount(ddao
										.getAccNumber());
								System.out.println("coming here:accoutValut::"
										+ acct);
								System.out.println("coming here:date::"
										+ ddao.getCreated_dt());
								if (acct != null) {
									SearchMatchesIterator<Document> si = acct
											.searchForDocuments(
													ddao.getCreated_dt(),
													Integer.MAX_VALUE);
									int expectedResult = 500, i = 0;
									DocumentHandle selectedDocHandle = new DocumentHandle();
									Document selectedDoc = null;
									docHandle = new DocumentHandle();
									a: while (si.hasNext()) {
										List<Document> docs = si.next();
										for (Document d1 : docs) {
											System.out
													.println("DocumentInfo:::"
															+ d1.getDocumentInfo()
																	.getCustomAttributes());
											Map<String, List<String>> docInfo = new HashMap<String, List<String>>();
											docInfo = d1.getDocumentInfo()
													.getCustomAttributes();
											for (String s : docInfo.keySet()) {
												if (s.equalsIgnoreCase(vaultIndex)) {
													if (vaultIndex
															.equalsIgnoreCase("DRN")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getDocRefNo())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													} else if (vaultIndex
															.equalsIgnoreCase("Guid")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getGuid())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													} else if (vaultIndex
															.equalsIgnoreCase("DocId")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getDocId())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													}
												}
											}
											System.out.println(d1);
											if (i > expectedResult) {
												System.out
														.println("Database has more than "
																+ expectedResult
																+ " statements");
												break a;
											}
											i++;
										}
									}
									docHandle = selectedDocHandle;
									RenderOptions renderOptions = new RenderOptions();
									renderOptions.setEnableBackground(false);

									vaultClient.renderDirect(db.getName(),
											accountNumber, docHandle.getDate(),
											docHandle.getFile(),
											docHandle.getPointer(),
											OutputFormat.PDF, 1,
											selectedDoc.getPageCount(),
											ResolutionWidth.NONE,
											Rotation.NONE, renderOptions, baos);
									String message = PrintTest.print(baos);
									setMessage(message);
									System.out.println("printed");
								}

							}

						}

						else if (servletRequest.getParameter("actType")
								.equalsIgnoreCase("reprint")) {

							actType = "reprint";
							System.out.println("actType::"
									+ servletRequest.getParameter("actType"));
							VaultClient vaultClient = Common
									.getVaultConnection();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();

							for (DataDAO ddao : list) {

								System.out.println("coming here:dbName::"
										+ dbName);
								Database db = Common.getDBConnection(dbName);
								System.out.println("coming here:1::" + db);
								System.out.println("coming here:2::account"
										+ ddao.getAccNumber());
								Account acct = db.getAccount(ddao
										.getAccNumber());
								System.out.println("coming here:accoutValut::"
										+ acct);
								System.out.println("coming here:date::"
										+ ddao.getCreated_dt());
								if (acct != null) {
									SearchMatchesIterator<Document> si = acct
											.searchForDocuments(
													ddao.getCreated_dt(),
													Integer.MAX_VALUE);
									int expectedResult = 500, i = 0;
									DocumentHandle selectedDocHandle = new DocumentHandle();
									Document selectedDoc = null;
									docHandle = new DocumentHandle();
									a: while (si.hasNext()) {
										List<Document> docs = si.next();
										for (Document d1 : docs) {
											System.out
													.println("DocumentInfo:::"
															+ d1.getDocumentInfo()
																	.getCustomAttributes());
											Map<String, List<String>> docInfo = new HashMap<String, List<String>>();
											docInfo = d1.getDocumentInfo()
													.getCustomAttributes();
											for (String s : docInfo.keySet()) {
												if (s.equalsIgnoreCase(vaultIndex)) {
													if (vaultIndex
															.equalsIgnoreCase("DRN")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getDocRefNo())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													} else if (vaultIndex
															.equalsIgnoreCase("Guid")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getGuid())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													} else if (vaultIndex
															.equalsIgnoreCase("DocId")) {
														if (docInfo
																.get(s)
																.contains(
																		ddao.getDocId())) {
															String type = d1
																	.getFormat()
																	.equals("collection") ? d1
																	.getType()
																	: d1.getFormat();
															System.out
																	.println(d1
																			.getDocID());
															System.out
																	.println("masterguid:"
																			+ d1.getMasterGUID());
															selectedDocHandle
																	.setDate(d1
																			.getDate());
															selectedDocHandle
																	.setType(type);
															selectedDocHandle
																	.setFile(d1
																			.getFile());
															selectedDocHandle
																	.setPointer(d1
																			.getPointer());
															selectedDoc = d1;
															accountNumber = d1
																	.getAccountNumber();
														}
													}
												}
											}
											System.out.println(d1);
											if (i > expectedResult) {
												System.out
														.println("Database has more than "
																+ expectedResult
																+ " statements");
												break a;
											}
											i++;
										}
									}
									docHandle = selectedDocHandle;
									// RenderOptions renderOptions = new
									// RenderOptions();

									ReprintOptions reprintOptions = new ReprintOptions();
									// renderOptions.setEnableBackground(true);
									System.out.println(db.getName() + "--"
											+ accountNumber + "--"
											+ docHandle.getDate() + "--"
											+ docHandle.getFile() + "--"
											+ docHandle.getPointer() + "--"
											+ selectedDoc.getPageCount() + "--"
											+ reprintOptions);
									vaultClient.submitForReprint(db.getName(),
											accountNumber, docHandle.getDate(),
											docHandle.getFile(),
											docHandle.getPointer(), 1,
											selectedDoc.getPageCount(), null);
									/*
									 * OutputStream output1 = null; String
									 * pathfile = "C:\\document.pdf"; File file
									 * = new File(pathfile); output1 = new
									 * FileOutputStream(file);
									 * output1.write(baos.toByteArray()); if
									 * (output1 != null) { output1.close(); }
									 * Executable ex = new Executable();
									 * ex.openDocument(pathfile);
									 * ex.printDocument(pathfile);
									 */

								}
							}
						}

					}
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (VaultException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServerErrorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return "SUCCESS";
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.servletResponse = response;
	}

}