package com.pb.ocbc;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

//import sun.text.CompactShortArray.Iterator;
import com.g1.e2.vault.OutputFormat;
import com.g1.e2.vault.RenderOptions;
import com.g1.e2.vault.ResolutionWidth;
import com.g1.e2.vault.Rotation;
import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.SearchMatches;
import com.g1.e2.vault.SearchMatchesIterator;
import com.g1.e2.vault.ServerErrorException;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Account;
import com.g1.e2.vault.VaultClient.Database;
import com.g1.e2.vault.VaultClient.Document;
import com.g1.e2.vault.VaultException;
import com.lowagie.tools.Executable;
import com.opensymphony.xwork2.ActionContext;
	//import com.pb.dao.DataDAO;
import com.pb.admin.Common;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.dao.DataDAO;
import com.pb.dao.DocMngrDAOOld;
import com.pb.dao.ResultDAO;
import com.pb.dao.UserDAO;
import com.pb.evault.DocumentHandle;
public class SearchDocManager extends BaseAction implements ServletRequestAware, ServletResponseAware{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		static final Logger logger = Logger.getLogger(DocManager.class);
		//private List<DataDAO> sampleList;
		private List<DataDAO> docList,docListProfile,searchBoxes,treeList,treeAccList,treeNameList,prePopulateList;
		private Map<Long,DataDAO> treeMapList;
		private HttpServletRequest servletRequest;
		private HttpServletResponse servletResponse;
		private DocumentHandle docHandle;
		private String accountNumber;
		private String databaseName;
		private Long profileId;
		private String message;
		private String pageType;
		private String flag;
		private String textSearch;
		private String searchReport;
		//private String searchValue;
		private Long[] docIds;
		private String[] accountNumbers;
		private String actType;
		private String format;
		private int pageSize;
		
		public void setPageSize(int pageSize) {
			this.pageSize = pageSize;
		}
		public int getPageSize() {
			return pageSize;
		}
		public void setFormat(String format) {
			this.format = format;
		}
		public String getFormat() {
			return format;
		}
		public void setActType(String actType) {
			this.actType = actType;
		}
		public String getActType() {
			return actType;
		}
		public void setAccountNumbers(String[] accountNumbers) {
			this.accountNumbers = accountNumbers;
		}
		public String[] getAccountNumbers() {
			return accountNumbers;
		}
		public void setDocIds(Long[] docIds) {
			this.docIds = docIds;
		}
		public Long[] getDocIds() {
			return docIds;
		}
		public void setSearchReport(String searchReport) {
			this.searchReport = searchReport;
		}
		public String getSearchReport() {
			return searchReport;
		}
		public void setPrePopulateList(ArrayList<DataDAO> prePopulateList)
		{
			this.prePopulateList = prePopulateList;
		}
		public List<DataDAO> getPrePopulateList()
		{
			return prePopulateList;
		}
		public void setTreeMapList(Map<Long,DataDAO> treeMapList) {
			this.treeMapList = treeMapList;
		}

		public Map<Long,DataDAO> getTreeMapList() {
			return treeMapList;
		}

		public void setFlag(String flag) {
			this.flag = flag;
		}

		public String getFlag() {
			return flag;
		}

		public void setPageType(String pageType) {
			this.pageType = pageType;
		}

		public String getPageType() {
			return pageType;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}

		public void setProfileId(Long profileId) {
			this.profileId = profileId;
		}

		public Long getProfileId() {
			return profileId;
		}

		public String getTextSearch() {
			return textSearch;
		}

		public void setTextSearch(String textSearch) {
			this.textSearch = textSearch;
		}
		
		public void setDatabaseName(String databaseName) {
			this.databaseName = databaseName;
		}

		public String getDatabaseName() {
			return databaseName;
		}

		public void setAccountNumber(String accountNumber) {
			this.accountNumber = accountNumber;
		}

		public String getAccountNumber() {
			return accountNumber;
		}

		public void setDocHandle(DocumentHandle docHandle) {
			this.docHandle = docHandle;
		}

		public DocumentHandle getDocHandle() {
			return docHandle;
		}

		public List getDocList() {
			return docList;
		}

		public void setDocList(ArrayList docList) {
			this.docList = docList;
		}
		
		public void setTreeList(ArrayList treeList) {
			this.treeList = treeList;
		}

		public List getTreeList() {
			return treeList;
		}
		
		public void setTreeAccList(ArrayList treeAccList) {
			this.treeAccList = treeAccList;
		}
		public List getTreeAccList() {
			return treeAccList;
		}
		
		public void setTreeNameList(ArrayList treeNameList) {
			this.treeNameList = treeNameList;
		}
		public List getTreeNameList() {
			return treeNameList;
		}
		
		public void setDocListProfile(ArrayList docListProfile) {
			this.docListProfile = docListProfile;
		}

		public List getDocListProfile() {
			return docListProfile;
		}
		
		public void setSearchBoxes(ArrayList searchBoxes) {
			this.searchBoxes = searchBoxes;
		}

		public List getSearchBoxes() {
			return searchBoxes;
		}
		
		public String execute() {
			DocMngrDAOOld dao = new DocMngrDAOOld();
			Long UserId = 0L;
			Long DbID = 0L;
			UserDAO userDao = new UserDAO();
			long logId = 0L;
			ResultDAO resDao = new ResultDAO();
			HttpSession sessionku = getServletRequest().getSession();
			logId = sessionku.getAttribute("logId") != null ? Long
					.parseLong(sessionku.getAttribute("logId")
							.toString()) : 0L;
					long serId = getSession().get("SerId") != null ? Long
							.parseLong(getSession().get("SerId").toString()) : 0L;
					
					long sresId = getSession().get("sresId") != null ? Long
							.parseLong(getSession().get("sresId").toString()) : 0L;
			System.out.println("USER ID" +getSession().get("UserID")+" DBID : "+servletRequest.getParameter("prID"));
			System.out.println("Namelist "+servletRequest.getParameter("Namelist"));
			System.out.println("date "+servletRequest.getParameter("date"));
			System.out.println("Accountlist "+servletRequest.getParameter("Accountlist"));
			System.out.println("1"+servletRequest.getParameter("docID"));
			System.out.println("2"+servletRequest.getParameter("prID"));
			System.out.println("3"+servletRequest.getParameter("guid"));
			System.out.println("4"+servletRequest.getParameter("actType"));
			docListProfile = new ArrayList();
			treeList = new ArrayList<DataDAO>();
			treeAccList = new ArrayList<DataDAO>();
			treeNameList = new ArrayList<DataDAO>();
			if (sessionku.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			if (getSession().get("UserID") != null)
			{
				UserId = Long.parseLong(getSession().get("UserID").toString());
			}
			if ((UserId != null || !UserId.equals(0)))
			{
				if(servletRequest.getParameter("prID") != null && !servletRequest.getParameter("prID").equals("0"))
				{
				DbID = Long.parseLong(servletRequest.getParameter("prID").toString());
				System.out.println("UserID: GUI Akshay" + UserId);
				System.out.println("DbID: GUI Akshay" + DbID);
				try {
					//searchBoxes = userDao.getIndexUser(UserId, DbID, "search");
					searchBoxes=dao.getUserIndex(UserId, DbID);
					System.out.println("searchBoxes:"+searchBoxes);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
			try {
				//docList = dao.getSearchResults();
				textSearch = servletRequest.getParameter("textSearch");
				
				if(getSearchReport() != null && !getSearchReport().equals(""))
				{
					treeMapList = dao.getRootSearchResults(getSearchReport(),UserId);
					
						treeList = dao.getResults(UserId,getSearchReport());
							//flag = "first";
					
				}
				else 
				{
					System.out.print("textSearch::"+textSearch);
					treeMapList = dao.getRootMapResults(UserId);
					if (servletRequest.getParameter("prID") == null || servletRequest.getParameter("prID").equals("0"))
					{
						
						if(textSearch == null && ((servletRequest.getParameter("Namelist"))!=null)||((servletRequest.getParameter("Accountlist"))!=null) || ((servletRequest.getParameter("date"))!=null))
						{
							String Namelist="";
							String Accountlist="";
							String date="";
							if(servletRequest.getParameter("Namelist")!=null){
								Namelist=servletRequest.getParameter("Namelist").toString();
								System.out.println("Namelist :"+Namelist);
							}
							if(servletRequest.getParameter("Accountlist")!=null){
								Accountlist=servletRequest.getParameter("Accountlist").toString();
								System.out.println("Accountlist :"+Accountlist);
							}
							if(servletRequest.getParameter("date")!=null){
								date=servletRequest.getParameter("date").toString();
								System.out.println("date :"+date);
							}
							treeList = dao.getSearchValues(UserId,Namelist,Accountlist,date);
						}
						else if(textSearch != null && (servletRequest.getParameter("Namelist")==null)&&(servletRequest.getParameter("Accountlist")==null) && (servletRequest.getParameter("date")==null))
						{
							treeList = dao.getSearchValues(UserId,textSearch);
							treeAccList = dao.getSearchValuesAcc(UserId,textSearch);
							treeNameList = dao.getSearchValuesName(UserId,textSearch);
						}
						else {
							//System.out.println("inside else>>>>>>>>>>>>>>>>>>>>PRasanna");
							treeList = dao.getResults(UserId,getSearchReport());
						}
					}
				}
				
				
				prePopulateList = new ArrayList<DataDAO>();
				
				//List res = dao.getDBIndexResults(UserId,);
				if (servletRequest.getParameter("prID") != null&& !servletRequest.getParameter("prID").equals("0"))
				{
					
					
					profileId = Long.parseLong(servletRequest.getParameter("prID"));
					
						//docListProfile = dao.getProfiles(profileId);
						System.out.print("Testing in action class"+docListProfile);
						System.out.println("guid:docManager-->before--"+servletRequest.getParameter("guid")+"--docId=="+servletRequest.getParameter("docId"));
						
						List<DataDAO> docProfile = dao.getProfiles(profileId);
						prePopulateList = dao.getDBIndexResults(UserId,profileId);
						int exp = 100,k=0;
						a:for(DataDAO dataDao:docProfile)
						{
							//System.out.println(prePopulateList);
							if(prePopulateList != null)
							{
							if(prePopulateList.contains(dataDao.getIndx1()) || (dataDao.getIndx2()!=null && prePopulateList.contains(dataDao.getIndx2())) || prePopulateList.contains(dataDao.getIndx3()) || prePopulateList.contains(dataDao.getIndx4()) && !docListProfile.contains(dataDao))
							{
								docListProfile.add(dataDao);
							}
							else
							{
								docListProfile.add(dataDao);
								if(k>exp)
								{
								
								break a;
								}
								k++;
							}
							}
						}
						
					if((servletRequest.getParameter("docId") != null || servletRequest.getParameter("docIds") != null) &&  servletRequest.getParameter("actType") != null )
					{
						System.out.println("docId:docManager-->"+servletRequest.getParameter("docId"));
						SearchMatches<Document> sm = null;
						List<Document> documents = null;
						
						if(servletRequest.getParameter("guid") != null && !servletRequest.getParameter("guid").equals(""))
						{
							System.out.println("guid:docManager-->"+servletRequest.getParameter("guid"));
							if( servletRequest.getParameter("actType").equalsIgnoreCase("view"))
							{
							for(DataDAO d : docProfile)
							{
								//DataDAO dao = (DataDAO) iter.hasNext();
								if(d.getGuid().equalsIgnoreCase(servletRequest.getParameter("guid")))
								{
								Database db = Common.getDBConnection(d.getProfileName());
								SearchIndex searchIndex = db.findSearchIndexByMatcher(SearchIndex.FindsDocumentByMasterGUID);
					      	 	
					      	   sm = db.searchForDocumentsWithoutAccount(searchIndex,  servletRequest.getParameter("guid"), "",Integer.MAX_VALUE);
					      	   documents = sm.getMatches();
					      	   DocumentHandle selectedDocHandle = new DocumentHandle();
					      	   Document selectedDoc = null;
					      	   docHandle = new DocumentHandle();
							 	int i=1;
					 			for(Document doc :documents)
					 			{
					 				String type = doc.getFormat().equals("collection")?doc.getType():doc.getFormat();
		                        	
					 				System.out.println(doc.getDocID());
					 				System.out.println("masterguid:"+doc.getMasterGUID());
					 				selectedDocHandle.setDate(doc.getDate());
					 				selectedDocHandle.setType(type);
					 				selectedDocHandle.setFile(doc.getFile());
					 				selectedDocHandle.setPointer(doc.getPointer());
								    selectedDoc = doc;
								    accountNumber = doc.getAccountNumber();
								    System.out.println("docHandle"+doc.getFile());
					 				i++;
					 				
					 			}
					 			docHandle = selectedDocHandle;
					 			databaseName = db.getName();
					 			
					 			HashMap map = new HashMap();
					 			if (logId != 0) {
									map.put("DBName",databaseName);
									map.put("accNum", accountNumber);
									map.put("Name", selectedDoc.getName());
									map.put("count", 0);
									map.put("serId", serId);
									map.put("logId", logId);
									map.put("sresId", sresId);
									long accId = resDao.saveAccountDetails(map);
					 			map.put("docName", selectedDoc.getName());
								map.put("Desc", selectedDoc.getDocumentInfo());
								map.put("docType", selectedDoc.getType());
								map.put("docDate", selectedDoc.getDate());
								map.put("accId", accId);
								map.put("logId", logId);
								//sessionku.setAttribute("logId", logId);
								long docId = resDao.saveDocumentDetails(map);
								if (logId != 0 && docId != 0) {
									map.put("logId", logId);
									map.put("docId", docId);
									map.put("action", "view");
									resDao.saveDocumentActionDetails(map);
								}
					 			}
					 			actType = "view";
					 			String rootName = dao.getRoot(Long.parseLong(servletRequest.getParameter("docId")));
					 			sessionku.setAttribute("SELECT_DB", db.getName());
					 			sessionku.setAttribute("SELECT_DB_DESC", db.getDescription());
					 			sessionku.setAttribute("SELECT_ROOT", rootName);
					 			//sessionku.setAttribute("userType","Repository");
					 			
								}
							}
							}
							else
								if( servletRequest.getParameter("actType").equalsIgnoreCase("download"))
								{
									VaultClient vaultClient = Common.getVaultConnection();
									ByteArrayOutputStream baos = new ByteArrayOutputStream();
									OutputStream output = getServletResponse().getOutputStream();
									String type = "";
									
									for(DataDAO d : docProfile)
									{
										//DataDAO dao = (DataDAO) iter.hasNext();
										if(d.getGuid().equalsIgnoreCase(servletRequest.getParameter("guid")))
										{
										Database db = Common.getDBConnection(d.getProfileName());
										SearchIndex searchIndex = db.findSearchIndexByMatcher(SearchIndex.FindsDocumentByMasterGUID);
							      	 	
							      	   sm = db.searchForDocumentsWithoutAccount(searchIndex,  servletRequest.getParameter("guid"), "",Integer.MAX_VALUE);
							      	   documents = sm.getMatches();
							      	   DocumentHandle selectedDocHandle = new DocumentHandle();
							      	   Document selectedDoc = null;
							      	   String acctNum = "";
							      	   docHandle = new DocumentHandle();
							      	   
									 	int i=1;
							 			for(Document doc :documents)
							 			{
							 				type = doc.getFormat().equals("collection")?doc.getType():doc.getFormat();
				                        	
							 				System.out.println(doc.getDocID());
							 				System.out.println("masterguid:"+doc.getMasterGUID());
							 				selectedDocHandle.setDate(doc.getDate());
							 				selectedDocHandle.setType(type);
							 				selectedDocHandle.setFile(doc.getFile());
							 				selectedDocHandle.setPointer(doc.getPointer());
										    selectedDoc = doc;
										    acctNum = doc.getAccountNumber();
										    System.out.println("docHandle"+doc.getFile());
							 				i++;
							 				
							 			}
							 			docHandle = selectedDocHandle;
							 			//databaseName = db.getName();
							 			docHandle = selectedDocHandle;
								      	RenderOptions renderOptions = new RenderOptions();
										renderOptions.setEnableBackground(true);
										if(type.equalsIgnoreCase("djdeline"))
										{
									    vaultClient.renderDirect(db.getName(), acctNum, docHandle.getDate(),
												docHandle.getFile(), docHandle.getPointer(),
												OutputFormat.TEXT, 1, selectedDoc.getPageCount(),
												ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
										}else
											vaultClient.renderDirect(db.getName(), acctNum, docHandle.getDate(),
													docHandle.getFile(), docHandle.getPointer(),
													OutputFormat.PDF, 1, selectedDoc.getPageCount(),
													ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
										getServletResponse().setHeader("Content-Disposition",
												"attachment; filename=\"download.PDF\"");
										getServletResponse().setContentType("application/x-download");
										output = getServletResponse().getOutputStream();
										output.write(baos.toByteArray());
							 			HashMap map = new HashMap();
							 			if (logId != 0) {
											map.put("DBName",databaseName);
											map.put("accNum", acctNum);
											map.put("Name", selectedDoc.getName());
											map.put("count", 0);
											map.put("serId", serId);
											map.put("logId", logId);
											map.put("sresId", sresId);
											long accId = resDao.saveAccountDetails(map);
							 			map.put("docName", selectedDoc.getName());
										map.put("Desc", selectedDoc.getDocumentInfo());
										map.put("docType", selectedDoc.getType());
										map.put("docDate", selectedDoc.getDate());
										map.put("accId", accId);
										map.put("logId", logId);
										//sessionku.setAttribute("logId", logId);
										long docId = resDao.saveDocumentDetails(map);
										if (logId != 0 && docId != 0) {
											map.put("logId", logId);
											map.put("docId", docId);
											map.put("action", "download");
											resDao.saveDocumentActionDetails(map);
										}
							 			}
							 			
									actType = "download";
								}
									}
								}
								else
									if( servletRequest.getParameter("actType").equalsIgnoreCase("print"))
									{
										actType = "print";
									}
									
							}
							else if(servletRequest.getParameter("docId") != null)
							{
								List<DataDAO> list = dao.getDocInfo(Long.parseLong(servletRequest.getParameter("docId")));
								
								if( servletRequest.getParameter("actType").equalsIgnoreCase("view"))
								{
								System.out.println("comin gere:docId::"+servletRequest.getParameter("docId"));
								actType = "view";
								for(DataDAO ddao : list)
								{
									System.out.println("comin gere:profile::"+ddao.getProfile());
									Database db = Common.getDBConnection(ddao.getProfile());
									System.out.println("comin gere:docId::"+db);
									System.out.println("comin gere:account::"+ddao.getIndx1());
									Account acct = db.getAccount(ddao.getIndx1());
									System.out.println("comin gere:accoutValut::"+acct);
									System.out.println("comin gere:date::"+ddao.getCreated_dt());
									SearchMatchesIterator<Document>  si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);
									int expectedResult = 500,i=0; 
									DocumentHandle selectedDocHandle = new DocumentHandle();
							      	 Document selectedDoc = null;
							      	 docHandle = new DocumentHandle();
									a:while(si.hasNext()){
										List<Document> docs = si.next();
										for(Document d1 : docs){
											
											if(d1.getPointer().equals(ddao.getPointer()))
											{
												//selectedDoc = d1;
												String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
					                        	
								 				System.out.println(d1.getDocID());
								 				System.out.println("masterguid:"+d1.getMasterGUID());
								 				selectedDocHandle.setDate(d1.getDate());
								 				selectedDocHandle.setType(type);
								 				selectedDocHandle.setFile(d1.getFile());
								 				selectedDocHandle.setPointer(d1.getPointer());
											    
											   // DocumentHandle docHandle1 = new DocumentHandle(docHandle.toString());
											    selectedDoc = d1;
											    accountNumber = d1.getAccountNumber();
											}
											System.out.println(d1);
											if(i>expectedResult){
												System.out.println("Database has more than " + expectedResult + " statements");
												break a;
											}
											i++;
										}
									}
							      	docHandle = selectedDocHandle;
						 			databaseName = db.getName();
						 			HashMap map = new HashMap();
						 			if (logId != 0) {
										map.put("DBName",databaseName);
										map.put("accNum", accountNumber);
										map.put("Name", acct.getName());
										map.put("count", 0);
										map.put("serId", serId);
										map.put("logId", logId);
										map.put("sresId", sresId);
										long accId = resDao.saveAccountDetails(map);
										//session.put("accId", accId);
									
						 			map.put("docName", selectedDoc.getName());
									map.put("Desc", selectedDoc.getDocumentInfo());
									map.put("docType", selectedDoc.getType());
									map.put("docDate", selectedDoc.getDate());
									map.put("accId", accId);
									map.put("logId", logId);
									//sessionku.setAttribute("logId", logId);
									long docuId = resDao.saveDocumentDetails(map);
									if (docuId != 0) {
										map.put("logId", logId);
										map.put("docId", docuId);
										map.put("action", "view");
										resDao.saveDocumentActionDetails(map);
									}
						 			}
						 			String rootName = dao.getRoot(Long.parseLong(servletRequest.getParameter("docId")));
						 			sessionku.setAttribute("SELECT_DB", db.getName());
						 			sessionku.setAttribute("SELECT_DB_DESC", db.getDescription());
						 			sessionku.setAttribute("SELECT_ROOT", rootName);
								
								}
							pageType = "search"+UserId;
				 			sessionku.setAttribute("pageType", pageType);
				 			boolean b = dao.GetDocFavourites(UserId, profileId, servletRequest.getParameter("guid"),"guid");
							System.out.println("2.guid--coming here::"+b);
							if(b){
								message = "This Name is already added to favourites!!";
								flag = "true";
							}
							else
								flag = "false";
								}
								else
									if( servletRequest.getParameter("actType").equalsIgnoreCase("download"))
									{
										actType = "download";
										System.out.println("actType::"+servletRequest.getParameter("actType"));
										VaultClient vaultClient = Common.getVaultConnection();
										ByteArrayOutputStream baos = new ByteArrayOutputStream();
										OutputStream output = getServletResponse().getOutputStream();
										System.out.println("format:"+getFormat());
										if(getFormat().equals("DJDELINE"))
										{
										getServletResponse().setHeader("Content-Disposition",
										"attachment; filename=\"download.txt\"");
										}
										else
											getServletResponse().setHeader("Content-Disposition",
											"attachment; filename=\"download.pdf\"");
										getServletResponse().setContentType("application/x-download");
										output = getServletResponse().getOutputStream();
										for(DataDAO ddao : list)
										{
											System.out.println("comin gere:profile::"+ddao.getProfile());
											Database db = Common.getDBConnection(ddao.getProfile());
											System.out.println("comin gere:docId::"+db);
											System.out.println("comin gere:account::"+ddao.getIndx1());
											Account acct = db.getAccount(ddao.getIndx1());
											System.out.println("comin gere:accoutValut::"+acct);
											System.out.println("comin gere:date::"+ddao.getCreated_dt());
											SearchMatchesIterator<Document>  si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);
											int expectedResult = 500,i=0; 
											DocumentHandle selectedDocHandle = new DocumentHandle();
									      	 Document selectedDoc = null;
									      	String accNumber ="";
									      	String type = "";
									      	 docHandle = new DocumentHandle();
											a:while(si.hasNext()){
												List<Document> docs = si.next();
												for(Document d1 : docs){
													
													if(d1.getPointer().equals(ddao.getPointer()))
													{
														//selectedDoc = d1;
														type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
							                        	
										 				System.out.println(type);
										 				System.out.println("masterguid:"+d1.getMasterGUID());
										 				selectedDocHandle.setDate(d1.getDate());
										 				selectedDocHandle.setType(type);
										 				selectedDocHandle.setFile(d1.getFile());
										 				selectedDocHandle.setPointer(d1.getPointer());
													    
													   // DocumentHandle docHandle1 = new DocumentHandle(docHandle.toString());
													    selectedDoc = d1;
													    accNumber = d1.getAccountNumber();
													    
													}
													
													if(i>expectedResult){
														System.out.println("Database has more than " + expectedResult + " statements");
														break a;
													}
													
													i++;
												}
											}
									      	docHandle = selectedDocHandle;
									      	RenderOptions renderOptions = new RenderOptions();
											renderOptions.setEnableBackground(true);
											if(type.equalsIgnoreCase("djdeline"))
											{
										    vaultClient.renderDirect(db.getName(), accNumber, docHandle.getDate(),
													docHandle.getFile(), docHandle.getPointer(),
													OutputFormat.TEXT, 1, selectedDoc.getPageCount(),
													ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
											}else
												vaultClient.renderDirect(db.getName(), accNumber, docHandle.getDate(),
														docHandle.getFile(), docHandle.getPointer(),
														OutputFormat.PDF, 1, selectedDoc.getPageCount(),
														ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
											
											output.write(baos.toByteArray());
											//System.out.println(d1);
										}
										output.close();
										return null;
									}
									else
										if( servletRequest.getParameter("actType").equalsIgnoreCase("print"))
										{
											actType = "print";
											System.out.println("actType::"+servletRequest.getParameter("actType"));
											VaultClient vaultClient = Common.getVaultConnection();
											ByteArrayOutputStream baos = new ByteArrayOutputStream();
											OutputStream output = getServletResponse().getOutputStream();
											System.out.println("format:"+getFormat());
											String pathfile = "";
											String contentType = null;
											if(getFormat().equals("DJDELINE"))
											{
											getServletResponse().setHeader("Content-Disposition",
											"attachment; filename=\"download.txt\"");
											contentType = "application/x-download";
											}
											else {
												getServletResponse().setHeader("Content-Disposition",
												"attachment; filename=\"download.pdf\"");
												contentType = "application/x-download";
											}
											if(!getFormat().equals("DJDELINE")) {
												pathfile = "C:\\document.pdf";
											} else {
												pathfile = "C:\\document.txt";
											}
											String type = "";
											getServletResponse().setContentType("application/x-download");
											output = getServletResponse().getOutputStream();
											for(DataDAO ddao : list)
											{
												System.out.println("comin gere:profile::"+ddao.getProfile());
												Database db = Common.getDBConnection(ddao.getProfile());
												System.out.println("comin gere:docId::"+db);
												System.out.println("comin gere:account::"+ddao.getIndx1());
												Account acct = db.getAccount(ddao.getIndx1());
												System.out.println("comin gere:accoutValut::"+acct);
												System.out.println("comin gere:date::"+ddao.getCreated_dt());
												SearchMatchesIterator<Document>  si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);
												int expectedResult = 500,i=0; 
												DocumentHandle selectedDocHandle = new DocumentHandle();
										      	 Document selectedDoc = null;
										      	String accNumber ="";
										      	
										      	 docHandle = new DocumentHandle();
												a:while(si.hasNext()){
													List<Document> docs = si.next();
													for(Document d1 : docs){
														
														if(d1.getPointer().equals(ddao.getPointer()))
														{
															//selectedDoc = d1;
															type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
								                        	
											 				System.out.println(type);
											 				System.out.println("masterguid:"+d1.getMasterGUID());
											 				selectedDocHandle.setDate(d1.getDate());
											 				selectedDocHandle.setType(type);
											 				selectedDocHandle.setFile(d1.getFile());
											 				selectedDocHandle.setPointer(d1.getPointer());
														    
														   // DocumentHandle docHandle1 = new DocumentHandle(docHandle.toString());
														    selectedDoc = d1;
														    accNumber = d1.getAccountNumber();
														    
														}
														
														if(i>expectedResult){
															System.out.println("Database has more than " + expectedResult + " statements");
															break a;
														}
														
														i++;
													}
												}
										      	docHandle = selectedDocHandle;
										      	RenderOptions renderOptions = new RenderOptions();
												renderOptions.setEnableBackground(true);
												if(type.equalsIgnoreCase("djdeline"))
												{
											    vaultClient.renderDirect(db.getName(), accNumber, docHandle.getDate(),
														docHandle.getFile(), docHandle.getPointer(),
														OutputFormat.TEXT, 1, selectedDoc.getPageCount(),
														ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
												}else
													vaultClient.renderDirect(db.getName(), accNumber, docHandle.getDate(),
															docHandle.getFile(), docHandle.getPointer(),
															OutputFormat.PDF, 1, selectedDoc.getPageCount(),
															ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
												
												output.write(baos.toByteArray());
												//System.out.println(d1);
											}
											File file = new File(pathfile);
											output = new FileOutputStream(file);
											output.write(baos.toByteArray());
											if (type.equals(".pdf")) {
												System.out.println("ViewDocAsPdf------");

												Executable ex = new Executable();
												ex.openDocument(pathfile);
												ex.printDocument(pathfile);
											} else if (type.equals(".htm")) {
												//System.out.println("htm------");
												String content = "", s;
												FileReader fr = new FileReader(pathfile);
												BufferedReader br = new BufferedReader(fr);
												while ((s = br.readLine()) != null) {
													content += s;
												}
												content = content.replace("</body>",
														"<script>window.print();</script></body>");
												output = getServletResponse().getOutputStream();
												output.write(content.getBytes());
												if (output != null) {
													output.close();
												}
											}else {
												//System.out.println("??????------");
												
												getServletResponse().setContentType(contentType);
												output = getServletResponse().getOutputStream();
												output.write(baos.toByteArray());
												if (output != null) {
													output.close();
												}
											}
											output.close();
											return null;
										}
							}
							else if(servletRequest.getParameter("docIds") != null)
							{
								docIds = getDocIds();
								
								if( servletRequest.getParameter("actType").equalsIgnoreCase("view"))
								{
								System.out.println("comin gere:docId::"+servletRequest.getParameter("docId"));
								actType = "view";
								for(int d=0;d<docIds.length;d++)
								{
								List<DataDAO> list = dao.getDocInfo(docIds[d]);
								
								for(DataDAO ddao : list)
								{
									System.out.println("comin gere:profile::"+ddao.getProfile());
									Database db = Common.getDBConnection(ddao.getProfile());
									System.out.println("comin gere:docId::"+db);
									System.out.println("comin gere:account::"+ddao.getIndx1());
									Account acct = db.getAccount(ddao.getIndx1());
									System.out.println("comin gere:accoutValut::"+acct);
									System.out.println("comin gere:date::"+ddao.getCreated_dt());
									SearchMatchesIterator<Document>  si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);
									int expectedResult = 500,i=0; 
									DocumentHandle selectedDocHandle = new DocumentHandle();
							      	 Document selectedDoc = null;
							      	 docHandle = new DocumentHandle();
									a:while(si.hasNext()){
										List<Document> docs = si.next();
										for(Document d1 : docs){
											
											if(d1.getPointer().equals(ddao.getPointer()))
											{
												//selectedDoc = d1;
												String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
					                        	
								 				System.out.println(d1.getDocID());
								 				System.out.println("masterguid:"+d1.getMasterGUID());
								 				selectedDocHandle.setDate(d1.getDate());
								 				selectedDocHandle.setType(type);
								 				selectedDocHandle.setFile(d1.getFile());
								 				selectedDocHandle.setPointer(d1.getPointer());
											    
											   // DocumentHandle docHandle1 = new DocumentHandle(docHandle.toString());
											    selectedDoc = d1;
											    accountNumber = d1.getAccountNumber();
											}
											System.out.println(d1);
											if(i>expectedResult){
												System.out.println("Database has more than " + expectedResult + " statements");
												break a;
											}
											i++;
										}
									}
							      	docHandle = selectedDocHandle;
						 			databaseName = db.getName();
						 			HashMap map = new HashMap();
						 			if (logId != 0) {
										map.put("DBName",databaseName);
										map.put("accNum", accountNumber);
										map.put("Name", acct.getName());
										map.put("count", 0);
										map.put("serId", serId);
										map.put("logId", logId);
										map.put("sresId", sresId);
										long accId = resDao.saveAccountDetails(map);
										//session.put("accId", accId);
									
						 			map.put("docName", selectedDoc.getName());
									map.put("Desc", selectedDoc.getDocumentInfo());
									map.put("docType", selectedDoc.getType());
									map.put("docDate", selectedDoc.getDate());
									map.put("accId", accId);
									map.put("logId", logId);
									//sessionku.setAttribute("logId", logId);
									long docuId = resDao.saveDocumentDetails(map);
									if (docuId != 0) {
										map.put("logId", logId);
										map.put("docId", docuId);
										map.put("action", "view");
										resDao.saveDocumentActionDetails(map);
									}
						 			}
						 			String rootName = dao.getRoot(Long.parseLong(servletRequest.getParameter("docId")));
						 			sessionku.setAttribute("SELECT_DB", db.getName());
						 			sessionku.setAttribute("SELECT_DB_DESC", db.getDescription());
						 			sessionku.setAttribute("SELECT_ROOT", rootName);
								
								}
							pageType = "search"+UserId;
				 			sessionku.setAttribute("pageType", pageType);
				 			boolean b = dao.GetDocFavourites(UserId, profileId, servletRequest.getParameter("guid"),"guid");
							System.out.println("2.guid--coming here::"+b);
							if(b){
								message = "This Name is already added to favourites!!";
								flag = "true";
							}
							else
								flag = "false";
								}
								}
								else
									if( servletRequest.getParameter("actType").equalsIgnoreCase("download"))
									{
										actType = "download";
										System.out.println("formats::"+getFormat());
										System.out.println("actType::"+servletRequest.getParameter("actType"));
										VaultClient vaultClient = Common.getVaultConnection();
										ByteArrayOutputStream baos = new ByteArrayOutputStream();
										OutputStream output = getServletResponse().getOutputStream();
										getServletResponse().setHeader("Content-Disposition",
										"attachment; filename=\"download\"");
										getServletResponse().setContentType("application/x-download");
										output = getServletResponse().getOutputStream();
										for(int d=0;d<docIds.length;d++)
										{
										List<DataDAO> list = dao.getDocInfo(docIds[d]);
										
										for(DataDAO ddao : list)
										{
											System.out.println("comin gere:profile::"+ddao.getProfile());
											Database db = Common.getDBConnection(ddao.getProfile());
											System.out.println("comin gere:docId::"+db);
											System.out.println("comin gere:account::"+ddao.getIndx1());
											Account acct = db.getAccount(ddao.getIndx1());
											System.out.println("comin gere:accoutValut::"+acct);
											System.out.println("comin gere:date::"+ddao.getCreated_dt());
											SearchMatchesIterator<Document>  si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);
											int expectedResult = 500,i=0; 
											DocumentHandle selectedDocHandle = new DocumentHandle();
									      	 Document selectedDoc = null;
									      	String accNumber ="";
									      	String type = "";
									      	 docHandle = new DocumentHandle();
											a:while(si.hasNext()){
												List<Document> docs = si.next();
												for(Document d1 : docs){
													
													if(d1.getPointer().equals(ddao.getPointer()))
													{
														//selectedDoc = d1;
														type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
							                        	
										 				System.out.println(type);
										 				System.out.println("masterguid:"+d1.getMasterGUID());
										 				selectedDocHandle.setDate(d1.getDate());
										 				selectedDocHandle.setType(type);
										 				selectedDocHandle.setFile(d1.getFile());
										 				selectedDocHandle.setPointer(d1.getPointer());
													    
													   // DocumentHandle docHandle1 = new DocumentHandle(docHandle.toString());
													    selectedDoc = d1;
													    accNumber = d1.getAccountNumber();
													    
													}
													
													if(i>expectedResult){
														System.out.println("Database has more than " + expectedResult + " statements");
														break a;
													}
													
													i++;
												}
											}
									      	docHandle = selectedDocHandle;
									      	RenderOptions renderOptions = new RenderOptions();
											renderOptions.setEnableBackground(true);
											if(type.equalsIgnoreCase("djdeline"))
											{
										    vaultClient.renderDirect(db.getName(), accNumber, docHandle.getDate(),
													docHandle.getFile(), docHandle.getPointer(),
													OutputFormat.TEXT, 1, selectedDoc.getPageCount(),
													ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
											}else
												vaultClient.renderDirect(db.getName(), accNumber, docHandle.getDate(),
														docHandle.getFile(), docHandle.getPointer(),
														OutputFormat.PDF, 1, selectedDoc.getPageCount(),
														ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
											
											output.write(baos.toByteArray());
											//System.out.println(d1);
										}
										}
										output.close();
										return null;
									
									}
									else
										if( servletRequest.getParameter("actType").equalsIgnoreCase("print"))
										{
											actType = "print";
											System.out.println("actType::"+servletRequest.getParameter("actType"));
											VaultClient vaultClient = Common.getVaultConnection();
											ByteArrayOutputStream baos = new ByteArrayOutputStream();
											OutputStream output = getServletResponse().getOutputStream();
											System.out.println("format:"+getFormat());
											String pathfile = "";
											String contentType = null;
											if(getFormat().equals("DJDELINE"))
											{
											getServletResponse().setHeader("Content-Disposition",
											"attachment; filename=\"download.txt\"");
											contentType = "application/x-download";
											}
											else {
												getServletResponse().setHeader("Content-Disposition",
												"attachment; filename=\"download.pdf\"");
												contentType = "application/x-download";
											}
											if(!getFormat().equals("DJDELINE")) {
												pathfile = "C:\\document.pdf";
											} else {
												pathfile = "C:\\document.txt";
											}
											String type = "";
											getServletResponse().setContentType("application/x-download");
											output = getServletResponse().getOutputStream();
											for(int d=0;d<docIds.length;d++)
											{
											List<DataDAO> list = dao.getDocInfo(docIds[d]);
											for(DataDAO ddao : list)
											{
												System.out.println("comin gere:profile::"+ddao.getProfile());
												Database db = Common.getDBConnection(ddao.getProfile());
												System.out.println("comin gere:docId::"+db);
												System.out.println("comin gere:account::"+ddao.getIndx1());
												Account acct = db.getAccount(ddao.getIndx1());
												System.out.println("comin gere:accoutValut::"+acct);
												System.out.println("comin gere:date::"+ddao.getCreated_dt());
												SearchMatchesIterator<Document>  si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);
												int expectedResult = 500,i=0; 
												DocumentHandle selectedDocHandle = new DocumentHandle();
										      	 Document selectedDoc = null;
										      	String accNumber ="";
										      	
										      	 docHandle = new DocumentHandle();
												a:while(si.hasNext()){
													List<Document> docs = si.next();
													for(Document d1 : docs){
														
														if(d1.getPointer().equals(ddao.getPointer()))
														{
															//selectedDoc = d1;
															type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
								                        	
											 				System.out.println(type);
											 				System.out.println("masterguid:"+d1.getMasterGUID());
											 				selectedDocHandle.setDate(d1.getDate());
											 				selectedDocHandle.setType(type);
											 				selectedDocHandle.setFile(d1.getFile());
											 				selectedDocHandle.setPointer(d1.getPointer());
														    
														   // DocumentHandle docHandle1 = new DocumentHandle(docHandle.toString());
														    selectedDoc = d1;
														    accNumber = d1.getAccountNumber();
														    
														}
														
														if(i>expectedResult){
															System.out.println("Database has more than " + expectedResult + " statements");
															break a;
														}
														
														i++;
													}
												}
										      	docHandle = selectedDocHandle;
										      	RenderOptions renderOptions = new RenderOptions();
												renderOptions.setEnableBackground(true);
												if(type.equalsIgnoreCase("djdeline"))
												{
											    vaultClient.renderDirect(db.getName(), accNumber, docHandle.getDate(),
														docHandle.getFile(), docHandle.getPointer(),
														OutputFormat.TEXT, 1, selectedDoc.getPageCount(),
														ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
												}else
													vaultClient.renderDirect(db.getName(), accNumber, docHandle.getDate(),
															docHandle.getFile(), docHandle.getPointer(),
															OutputFormat.PDF, 1, selectedDoc.getPageCount(),
															ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
												
												output.write(baos.toByteArray());
												//System.out.println(d1);
											}
											File file = new File(pathfile);
											output = new FileOutputStream(file);
											output.write(baos.toByteArray());
											}
											if (type.equals(".pdf")) {
												System.out.println("ViewDocAsPdf------");

												Executable ex = new Executable();
												ex.openDocument(pathfile);
												ex.printDocument(pathfile);
											} else if (type.equals(".htm")) {
												//System.out.println("htm------");
												String content = "", s;
												FileReader fr = new FileReader(pathfile);
												BufferedReader br = new BufferedReader(fr);
												while ((s = br.readLine()) != null) {
													content += s;
												}
												content = content.replace("</body>",
														"<script>window.print();</script></body>");
												output = getServletResponse().getOutputStream();
												output.write(content.getBytes());
												if (output != null) {
													output.close();
												}
											}else {
												//System.out.println("??????------");
												
												getServletResponse().setContentType(contentType);
												output = getServletResponse().getOutputStream();
												output.write(baos.toByteArray());
												if (output != null) {
													output.close();
												}
											}
											output.close();
											return null;
										}
								}
							
						
				 			//sessionku.setAttribute("prID", servletRequest.getParameter("prID"));
				 			//sessionku.setAttribute("guid", servletRequest.getParameter("guid"));
						
					}
					System.out.println("before--coming here::"+servletRequest.getParameter("query")+"&&&&&&"+servletRequest.getParameter("favName"));
					
					if(servletRequest.getParameter("query") != null && servletRequest.getParameter("listSize") != null && servletRequest.getParameter("favName") != null )
					{
						System.out.println("coming here::"+servletRequest.getParameter("query"));
						boolean b = dao.GetFavourites(UserId, profileId, servletRequest.getParameter("favName"));
						if(b){
							message = "This Name is already added to favourites!!";
						}
						else
						{
						dao.AddToFavourites(UserId, profileId, servletRequest.getParameter("favName"),servletRequest.getParameter("favDesc"),servletRequest.getParameter("query"), Integer.parseInt(servletRequest.getParameter("listSize")));
						message = "Successfully added to favourites";
						}
					}
					else
						if(servletRequest.getParameter("favName") != null && servletRequest.getParameter("docId") != null && servletRequest.getParameter("type") != null)
						{
							
							System.out.println("guid--coming here::"+servletRequest.getParameter("guid"));
							boolean b = dao.GetDocFavourites(UserId, profileId, servletRequest.getParameter("favName"),"name");
							System.out.println("2.guid--coming here::"+b);
							if(b){
								message = "This Name is already added to favourites!!";
							}
							else
							{
							dao.AddDocsToFavourites(UserId, profileId, servletRequest.getParameter("favName"),servletRequest.getParameter("favDesc"),servletRequest.getParameter("guid"),Long.parseLong(servletRequest.getParameter("docId")));
							message = "Successfully added to favourites";
							//sessionku.setAttribute("message", message);
							}
							}
							
				}
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (VaultException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServerErrorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			}
			return "SUCCESS";
		}
		public String executeFavourites() throws SQLException {
			DocMngrDAOOld dao = new DocMngrDAOOld();
			Long UserId = 0L;
			Long DbID = 0L;
			UserDAO userDao = new UserDAO();
			String guid = "";
			DataDAO dDao = new DataDAO();
			ResultDAO resDao = new ResultDAO();
			HttpSession sessionku = getServletRequest().getSession();
			Long logId = sessionku.getAttribute("logId") != null ? Long
					.parseLong(sessionku.getAttribute("logId")
							.toString()) : 0L;
			System.out.println("USER ID" +getSession().get("UserID")+" favId : "+servletRequest.getParameter("favouriteId"));
			System.out.println(" favId : Attribute"+servletRequest.getAttribute("favouriteId"));
			docListProfile = new ArrayList();
			if (sessionku.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			if (getSession().get("UserID") != null )
			{
				UserId = Long.parseLong(getSession().get("UserID").toString());
				/*if (servletRequest.getParameter("favDocId") != null && !servletRequest.getParameter("favDocId").equals(""))
			{
				System.out.println("favDocId: GUI Suhashini" + servletRequest.getParameter("favDocId"));
				dDao = dao.GetFavouriteByDocument(UserId, Long.parseLong(servletRequest.getParameter("favDocId")));
				if(dDao != null)
				{
				guid =dDao.getGuid();
				profileId = dDao.getProfileID();
				}
			}
			else
				
				if(servletRequest.getParameter("guid") != null && servletRequest.getParameter("prID") != null)
				{
					guid = servletRequest.getParameter("guid");
					profileId = Long.parseLong(servletRequest.getParameter("prID"));
				}*/
			
			
			
			if (servletRequest.getParameter("favouriteId") != null )
			{
				
				//DbID = Long.parseLong(servletRequest.getParameter("prID").toString());
				System.out.println("UserID: GUI Suhashini" + UserId);
				System.out.println("favouriteId: GUI Suhashini" + servletRequest.getParameter("favouriteId"));
				System.out.println("action: GUI Suhashini" + servletRequest.getParameter("action"));
				try {
					//searchBoxes = userDao.getIndexUser(UserId, DbID, "search");
					//searchBoxes=dao.getUserIndex(UserId, DbID);
					if(servletRequest.getParameter("action") != null)
					{
						System.out.println("coming here:"+servletRequest.getParameter("action"));
						if(servletRequest.getParameter("action").equalsIgnoreCase("edit") && servletRequest.getParameter("favName")!=null)
						{
							dao.EditFavourites(UserId, Long.parseLong(servletRequest.getParameter("favouriteId")), servletRequest.getParameter("favName"), servletRequest.getParameter("favDesc"));
							message = "Successfully added to favourites";
						}
							else if(servletRequest.getParameter("action").equalsIgnoreCase("delete"))
							{
								dao.DeleteFavourites(UserId, Long.parseLong(servletRequest.getParameter("favouriteId")));
								message = "Successfully deleted favourite";
							}
					}
					else
					{
					List<DataDAO> docListProfile1 = dao.GetFavourites(UserId, Long.parseLong(servletRequest.getParameter("favouriteId")));
					List<DataDAO> prePopulateList1 = dao.getDBIndexResults(UserId,Long.parseLong(servletRequest.getParameter("prID")));
					for(DataDAO dataDao:docListProfile1)
					{
						System.out.println(prePopulateList1);
						if(prePopulateList1 != null && prePopulateList1.size() != 0)
						{
						if(prePopulateList1.contains(dataDao.getIndx1()) || prePopulateList1.contains(dataDao.getIndx2()) || prePopulateList1.contains(dataDao.getIndx3()) || prePopulateList1.contains(dataDao.getIndx4()))
						{
							docListProfile.add(dataDao);
						}
							
						}
						else
						{
							docListProfile.add(dataDao);
						}
					}
					if(docListProfile.size() == 0)
					{
						setMessage("No Results Found!!");
					}
					
					}
					System.out.println("searchBoxes:"+searchBoxes);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(servletRequest.getParameter("action") != null&& servletRequest.getParameter("favDocId") != null )
			{
				System.out.println("coming here:"+servletRequest.getParameter("action")+";favdocId:"+servletRequest.getParameter("favDocId"));
				if(!servletRequest.getParameter("favDocId").equals("") && !servletRequest.getParameter("favDocId").equals(0))
				{
					System.out.println("coming inside:"+servletRequest.getParameter("action")+";favdocId:"+servletRequest.getParameter("favDocId").equals(""));
					
						 if(servletRequest.getParameter("action").equalsIgnoreCase("edit") )
						{
							dao.EditDocsToFavourites(UserId, Long.parseLong(servletRequest.getParameter("favDocId")), servletRequest.getParameter("favName"), servletRequest.getParameter("favDesc"));
							message = "Successfully Updated to favourites";
						}
							else if(servletRequest.getParameter("action").equalsIgnoreCase("delete"))
							{
								dao.DeleteDocsToFavourites(UserId, Long.parseLong(servletRequest.getParameter("favDocId")), servletRequest.getParameter("favName"), servletRequest.getParameter("favDesc"));
								message = "Successfully deleted favourite";
							}
				}
			}
			System.out.println("coming here: before guid"+servletRequest.getParameter("guid"));
			if (servletRequest.getParameter("prID") != null)
			{
				profileId = Long.parseLong(servletRequest.getParameter("prID"));
				System.out.println("profileId: GUI Suhashini******" + profileId);
				
			List<DataDAO> docProfile = dao.getProfiles(profileId);
			
			if((servletRequest.getParameter("docId") != null || servletRequest.getParameter("docIds") != null) && servletRequest.getParameter("actType") != null )
			{
				System.out.println("guid:docManager-->"+servletRequest.getParameter("docId"));
				SearchMatches<Document> sm = null;
				List<Document> documents = null;
				try
				{
				if(servletRequest.getParameter("guid")!= null && !servletRequest.getParameter("guid").equals(""))
				{
				for(DataDAO d : docProfile)
				{
					//DataDAO dao = (DataDAO) iter.hasNext();
					if(d.getGuid().equalsIgnoreCase(servletRequest.getParameter("guid")))
					{
					Database db = Common.getDBConnection(d.getProfileName());
				SearchIndex searchIndex = db.findSearchIndexByMatcher(SearchIndex.FindsDocumentByMasterGUID);
		      	  // searchIndex = db.findSearchIndexByName(SearchIndexDefault.AccountSearchIndex.getName());
			        	 
		      	  // System.out.println("seach"+searchIndex);
					
		      	   sm = db.searchForDocumentsWithoutAccount(searchIndex,  servletRequest.getParameter("guid"), "",Integer.MAX_VALUE);
		      	   documents = sm.getMatches();
		      	 DocumentHandle selectedDocHandle = new DocumentHandle();
		      	 Document selectedDoc = null;
		      	 docHandle = new DocumentHandle();
				 	int i=1;
		 			for(Document doc :documents)
		 			{
		 				//System.out.println(i+"date::"+doc.getDate()+"Statement Type:"+doc.getAccountNumber());
		 				//doc.
		 				String type = doc.getFormat().equals("collection")?doc.getType():doc.getFormat();
	                	
		 				System.out.println(doc.getDocID());
		 				System.out.println("masterguid:"+doc.getMasterGUID());
		 				selectedDocHandle.setDate(doc.getDate());
		 				selectedDocHandle.setType(type);
		 				selectedDocHandle.setFile(doc.getFile());
		 				selectedDocHandle.setPointer(doc.getPointer());
					    
					   // DocumentHandle docHandle1 = new DocumentHandle(docHandle.toString());
					    selectedDoc = doc;
					    accountNumber = doc.getAccountNumber();
					    
					  //doc.re
					    System.out.println("docHandle"+doc.getFile());
		 				i++;
		 				
		 			}
		 			docHandle = selectedDocHandle;
		 			databaseName = db.getName();
		 			HashMap map = new HashMap();
		 			map.put("docName", selectedDoc.getName());
					map.put("Desc", selectedDoc.getDocumentInfo());
					map.put("docType", selectedDoc.getType());
					map.put("docDate", selectedDoc.getDate());
					map.put("accId", selectedDoc);
					map.put("logId", logId);
					//sessionku.setAttribute("logId", logId);
					long docId = resDao.saveDocumentDetails(map);
					if (logId != 0 && docId != 0) {
						map.put("logId", logId);
						map.put("docId", docId);
						map.put("action", "view");
						resDao.saveDocumentActionDetails(map);
					}
		 			sessionku.setAttribute("SELECT_DB", db.getName());
		 			sessionku.setAttribute("SELECT_DB_DESC", db.getDescription());
		 			
					}
				}
				}
					else
					{
						System.out.println("comin gere:docId::"+servletRequest.getParameter("docId"));
						List<DataDAO> list = dao.getDocInfo(Long.parseLong(servletRequest.getParameter("docId")));
						for(DataDAO ddao : list)
						{
							System.out.println("comin gere:profile::"+ddao.getProfile());
							Database db = Common.getDBConnection(ddao.getProfile());
							System.out.println("comin gere:docId::"+db);
							System.out.println("comin gere:account::"+ddao.getIndx1());
							Account acct = db.getAccount(ddao.getIndx1());
							System.out.println("comin gere:accoutValut::"+acct);
							System.out.println("comin gere:date::"+ddao.getCreated_dt());
							SearchMatchesIterator<Document>  si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);
							int expectedResult = 500,i=0; 
							DocumentHandle selectedDocHandle = new DocumentHandle();
					      	 Document selectedDoc = null;
					      	 docHandle = new DocumentHandle();
							a:while(si.hasNext()){
								List<Document> docs = si.next();
								for(Document d1 : docs){
									
									if(d1.getPointer().equals(ddao.getPointer()))
									{
										//selectedDoc = d1;
										String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
			                        	
						 				System.out.println(d1.getDocID());
						 				System.out.println("masterguid:"+d1.getMasterGUID());
						 				selectedDocHandle.setDate(d1.getDate());
						 				selectedDocHandle.setType(type);
						 				selectedDocHandle.setFile(d1.getFile());
						 				selectedDocHandle.setPointer(d1.getPointer());
									    
									   // DocumentHandle docHandle1 = new DocumentHandle(docHandle.toString());
									    selectedDoc = d1;
									    accountNumber = d1.getAccountNumber();
									}
									System.out.println(d1);
									if(i>expectedResult){
										System.out.println("Database has more than " + expectedResult + " statements");
										break a;
									}
									i++;
								}
							}
					      	docHandle = selectedDocHandle;
				 			databaseName = db.getName();
				 			HashMap map = new HashMap();
				 			map.put("docName", selectedDoc.getName());
							map.put("Desc", selectedDoc.getDocumentInfo());
							map.put("docType", selectedDoc.getType());
							map.put("docDate", selectedDoc.getDate());
							map.put("accId", selectedDoc);
							map.put("logId", logId);
							//sessionku.setAttribute("logId", logId);
							long docId = resDao.saveDocumentDetails(map);
							if (logId != 0 && docId != 0) {
								map.put("logId", logId);
								map.put("docId", docId);
								map.put("action", "view");
								resDao.saveDocumentActionDetails(map);
							}
				 			sessionku.setAttribute("SELECT_DB", db.getName());
				 			sessionku.setAttribute("SELECT_DB_DESC", db.getDescription());
						}
					
					pageType = "Fav"+UserId;
		 			sessionku.setAttribute("pageType", pageType);
		 			
					}
				
		 			//sessionku.setAttribute("prID", servletRequest.getParameter("prID"));
		 			//sessionku.setAttribute("guid", servletRequest.getParameter("guid"));
				
			
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (VaultException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServerErrorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			}
			}
			docList = dao.GetListFavouritesByName(UserId);
			prePopulateList = dao.GetListFavouriteByDocument(UserId);
			if(docList.size() == 0)
			{
				//setMessage("No Favourites Found!!");
			}
			
			}
			
			return "SUCCESS";
		}
		/**public List<DataDAO> getDocList() {
			DocMngrDAO dao = new DocMngrDAO();
			List<DataDAO> docmanagerlist = null;
			try {
				docmanagerlist = dao.getSearchResults();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return docmanagerlist;
			
		}
		public void setSampleList(List<DataDAO> sampleList) {
			this.sampleList = sampleList;
		}**/
		
	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.servletResponse = response;
	}

}
