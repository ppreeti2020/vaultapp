package com.pb.ocbc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.g1.e2.vault.DocumentSection;
import com.g1.e2.vault.OutputFormat;
import com.g1.e2.vault.RenderOptions;
import com.g1.e2.vault.ReprintOptions;
import com.g1.e2.vault.ResolutionWidth;
import com.g1.e2.vault.Rotation;
import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.SearchMatchesIterator;
import com.g1.e2.vault.ServerErrorException;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Account;
import com.g1.e2.vault.VaultClient.Database;
import com.g1.e2.vault.VaultClient.Document;
import com.g1.e2.vault.VaultException;
import com.pb.admin.Common;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.common.ServerSideValidationUtil;
import com.pb.dao.DataDAO;
import com.pb.dao.DocMngrDAO;
import com.pb.dao.ResultDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.UserDTO;
import com.pb.evault.DocumentHandle;
public class SearchAccountNew extends BaseAction implements ServletRequestAware, ServletResponseAware{

	/**Class used for Customer search to view/download/print the document
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static final Logger logger = Logger.getLogger(SearchAccountNew.class);
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private String host;
	private String message;
	private String prName;
	private String prdbID;
	private Map<Long,DataDAO> dbMapList;
	private Map<Long,String> searchIdxList;
	private List<String> selectIdxList;
	private Map<Integer, Map<String,String>> docList;
	private String actType;
	private DocumentHandle docHandle;
	
	private int pageSize;
	private String accountNumber;
	private String databaseName;
	private boolean search;
	private String primarySearch;
	private List<String> searchStr;
	private String pageType;
	private List<UserDTO> actionList;
	private List<UserDTO> indexList;
	private String primaryidx;
	private String pidxName;
	private String crossSearch;
	private Map<String, String>  idxList;
	private List<UserDTO> csrDatabase;
	private boolean showDate;
	private String fromDate;
	private String toDate;
	private String tabId;
	private String strprdbID;
	private String vaultIndex;
	private String vaultIndexDesc;

	private String attachmentPath;
	private String attachment1;
	private String attachment2;
	private String attachment3;
	private String attachment4;

	/* Nithu Alexander : 05/09/2014
	 * Addition of filterSearchStr as parameter in
	 * Action class*/
	private List<String> filterSearchStr;
	
	/* Nithu testing Document View*/
	private int docPageCount;
	private String docFormat;
	private String docInfoType;
	private String outputFormats;
	private boolean imageOutput;
	private boolean pdfOutput;
	private boolean textOutput;
	private boolean tiffOutput;
	

	public String getAttachmentPath() {
		return attachmentPath;
	}

	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}

	public String getAttachment1() {
		return attachment1;
	}

	public void setAttachment1(String attachment1) {
		this.attachment1 = attachment1;
	}

	public String getAttachment2() {
		return attachment2;
	}

	public void setAttachment2(String attachment2) {
		this.attachment2 = attachment2;
	}

	public String getAttachment3() {
		return attachment3;
	}

	public void setAttachment3(String attachment3) {
		this.attachment3 = attachment3;
	}

	public String getAttachment4() {
		return attachment4;
	}

	public void setAttachment4(String attachment4) {
		this.attachment4 = attachment4;
	}	

	public String getVaultIndexDesc() {
		return vaultIndexDesc;
	}

	public void setVaultIndexDesc(String vaultIndexDesc) {
		this.vaultIndexDesc = vaultIndexDesc;
	}

	public String getVaultIndex() {
		return vaultIndex;
	}

	public void setVaultIndex(String vaultIndex) {
		this.vaultIndex = vaultIndex;
	}

	public String getStrprdbID() {
		return strprdbID;
	}

	public void setStrprdbID(String strprdbID) {
		this.strprdbID = strprdbID;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public boolean getShowDate() {
		return showDate;
	}

	public void setShowDate(boolean showDate) {
		this.showDate = showDate;
	}

	public List<UserDTO> getCsrDatabase() {
		return csrDatabase;
	}

	public void setCsrDatabase(ArrayList<UserDTO> csrDatabase) {
		this.csrDatabase = csrDatabase;
	}

	public String getCrossSearch() {
		return crossSearch;
	}

	public void setCrossSearch(String crossSearch) {
		this.crossSearch = crossSearch;
	}

	public Map<String, String>  getIdxList() {
		return idxList;
	}

	public void setIdxList(Map<String, String>  idxList) {
		this.idxList = idxList;
	}

	public String getPidxName() {
		return pidxName;
	}

	public void setPidxName(String pidxName) {
		this.pidxName = pidxName;
	}

	public String getPrimaryidx() {
		return primaryidx;
	}

	public void setPrimaryidx(String primaryidx) {
		this.primaryidx = primaryidx;
	}

	public List<UserDTO> getIndexList() {
		return indexList;
	}

	public void setIndexList(ArrayList<UserDTO> indexList) {
		this.indexList = indexList;
	}

	public List<UserDTO> getActionList() {
		return actionList;
	}

	public void setActionList(ArrayList<UserDTO> actList) {
		this.actionList = actList;
	}
	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getPageType() {
		return pageType;
	}
	public void setSearchStr(List<String> searchStr) {
		this.searchStr = searchStr;
	}

	public List<String> getSearchStr() {
		return searchStr;
	}
	public void setPrimarySearch(String primarySearch) {
		this.primarySearch = primarySearch;
	}

	public String getPrimarySearch() {
		return primarySearch;
	}
	public void setSearch(boolean search) {
		this.search = search;
	}

	public boolean getSearch() {
		return search;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getDatabaseName() {
		return databaseName;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setDocHandle(DocumentHandle docHandle) {
		this.docHandle = docHandle;
	}

	public DocumentHandle getDocHandle() {
		return docHandle;
	}
	
	
	public void setActType(String actType) {
		this.actType = actType;
	}
	public String getActType() {
		return actType;
	}
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setDbMapList(Map<Long,DataDAO> dbMapList) {
		this.dbMapList = dbMapList;
	}

	public Map<Long,DataDAO> getDbMapList() {
		return dbMapList;
	}

	public String getPrdbID() {
		return prdbID;
	}

	public void setPrdbID(String prdbID) {
		this.prdbID = prdbID;
	}

	public String getPrName() {
		return prName;
	}

	public void setPrName(String prName) {
		this.prName = prName;
	}

	public Map<Long,String> getSearchIdxList() {
		return searchIdxList;
	}

	public void setSelectIdxList(List<String> selectIdxList) {
		this.selectIdxList = selectIdxList;
	}

	public List<String> getSelectIdxList() {
		return selectIdxList;
	}

	public void ResultIdxList(Map<Long,String> searchIdxList) {
		this.searchIdxList = searchIdxList;
	}

	public void setDocList(Map<Integer, Map<String,String>> docList) {
		this.docList = docList;
	}

	public Map<Integer, Map<String,String>> getDocList() {
		return docList;
	}

	/* Nithu Alexander : 05/09/2014
	 * Addition of filterSearchStr as parameter in
	 * Action class*/
	public void setFilterSearchStr(List<String> filterSearchStr) {
		this.filterSearchStr = filterSearchStr;
	}

	public List<String> getFilterSearchStr() {
		return filterSearchStr;
	}
	
	//Nithu testing Document View
	
	public void setDocFormat(String docFormat) {
		this.docFormat = docFormat;
	}

	public String getDocFormat() {
		return docFormat;
	}
	
	public void setDocInfoType(String docInfoType) {
		this.docInfoType = docInfoType;
	}

	public String getDocInfoType() {
		return docInfoType;
	}
	
	public void setDocPageCount(int docPageCount) {
		this.docPageCount = docPageCount;
	}
	public int getDocPageCount() {
		return docPageCount;
	}
	
	public void setOutputFormats(String outputFormats) {
		this.outputFormats = outputFormats;
	}
	public String getOutputFormats() {
		return outputFormats;
	}
	
	public void setImageOutput(boolean imageOutput) {
		this.imageOutput = imageOutput;
	}

	public boolean getImageOutput() {
		return imageOutput;
	}
	
	public void setPdfOutput(boolean pdfOutput) {
		this.pdfOutput = pdfOutput;
	}

	public boolean getPdfOutput() {
		return pdfOutput;
	}
	
	public void setTextOutput(boolean textOutput) {
		this.textOutput = textOutput;
	}

	public boolean getTextOutput() {
		return textOutput;
	}
	
	public void setTiffOutput(boolean tiffOutput) {
		this.tiffOutput = tiffOutput;
	}

	public boolean getTiffOutput() {
		return tiffOutput;
	}
	/*Praveen Kumar: 2 Nov 2015 Added Charater to be displayed controlled Character start */
	private static List<String> characterNotToDisplayList = null;
	private static String getControlledCharacterPresence(String attachment)
	{
		String result = null;
		
			
			if (characterNotToDisplayList == null
				|| characterNotToDisplayList.size() == 0) {
				
			//  Initialisation block kept in synchronized block because characterNotToDisplayList will be used 
				// and it should conflict with the existing stage.
			synchronized (SearchAccountNew.class) {
				String str = PropertyUtils.getProperties(
						CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
						CommonConstants.CHARACTER_NOT_TO_DISPLAY);
				if (str != null && !"".equalsIgnoreCase(str)
						&& str.contains(",")) {
					String[] strArray = str.split(",");
					if (strArray != null && strArray.length > 0) {
						characterNotToDisplayList = new ArrayList<String>();
						for (String str1 : strArray) {
							characterNotToDisplayList.add(str1.toUpperCase());
						}
					}
				}
			}
		}
		if(characterNotToDisplayList != null && characterNotToDisplayList.size() > 0)
		{
			if(characterNotToDisplayList.contains(attachment.toUpperCase()))
			{
				System.out.println("Setting Attachment empty");
			}else
			{
				result = attachment;
				System.out.println("Setting Attachment not empty with value " + attachment);
			}
		}
		
		
		return result;
	}
	/*Praveen Kumar: 2 Nov 2015 Added Charater to be displayed controlled Character End*/
	
	@SuppressWarnings("unused")
	public String execute() {

		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();

		setTabId("CSR-search");

		DocMngrDAO dao = new DocMngrDAO();
		UserDAO userDao = new UserDAO();
		ResultDAO resDao = new ResultDAO();
		boolean foundDocument=false;
		int expectedMaxResult=0;
		
		host = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.HOST_NAME);
		vaultIndex = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.VAULT_IDX);
		
		/* Nithu Alexander: 14 Aug 2015, Adding Expected Max Result=500 parameter in property file*/
		
		if ((PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.EXPECTED_MAX_RESULT) != null) &&
				!(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.EXPECTED_MAX_RESULT).trim().equals(""))){

			expectedMaxResult = Integer.parseInt(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.EXPECTED_MAX_RESULT));
		}
	

		long UserId = 0L;
		long logId = 0L;
		long serId = 0L;
		long sresId = 0L;
		dbMapList = new HashMap<Long,DataDAO>();
		idxList = new HashMap<String, String>(); 
		indexList = new ArrayList<UserDTO>();
		
		Long profileAdminUserId = 0L;

		HttpSession sessionku = getServletRequest().getSession();
		logId = sessionku.getAttribute("logId") != null ? Long.parseLong(sessionku.getAttribute("logId").toString()) : 0L;
		serId = getSession().get("SerId") != null ? Long.parseLong(getSession().get("SerId").toString()) : 0L;
		sresId = getSession().get("sresId") != null ? Long.parseLong(getSession().get("sresId").toString()) : 0L;

		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		if (getSession().get("UserID") != null){	
			UserId = getSession().get("UserID") != null ? Long.parseLong(getSession().get("UserID").toString()) : 0L;
		}
		if (!(UserId==0L)){
			try {
				profileAdminUserId = userDao.getProfileAdminUserId(UserId);
				csrDatabase = userDao.getDatabase(UserId);
				if(csrDatabase.size() == 1){
					System.out.println("loop 1 ----------> ");
					for(Iterator<UserDTO> iter=csrDatabase.iterator();iter.hasNext();){
						UserDTO dto = (UserDTO) iter.next();
						prdbID = dto.getDbId().toString();
					}
				}else{
					if(getPrdbID()!=null){
						System.out.println("loop 2 ----------> ");
						String pid="";
						pid = getPrdbID().trim();
						setPrdbID(pid);
						System.out.println("getPrdbID::" + getPrdbID());
						prdbID = getPrdbID().toString();
					}
				}

				/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1 and SCT (VaultApp)*/

				if(prdbID!=null && !(prdbID.trim().equals("")) && !ServerSideValidationUtil.isNumeric(prdbID)){
					setMessage("Invalid Characters in Selected Field");
					tabId = "CSR-search";
					return "INVALID_PARAMETERS";
				}
				if(strprdbID!=null && !(strprdbID.trim().equals("")) && !ServerSideValidationUtil.isNumeric(strprdbID)){
					setMessage("Invalid Characters in Selected Field");
					tabId = "CSR-search";
					return "INVALID_PARAMETERS";
				}
				sessionku.setAttribute("ProdDBId", prdbID);

				System.out.println("ProfileAdminUserId--->"+profileAdminUserId.toString());
				if (prdbID != null){
					search = false;
					showDate = true;
					String selectStr = "";
					String indexDescription = "";
					prName = userDao.getDBName(UserId,Long.valueOf(prdbID));

					/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1 and SCT (VaultApp)*/
					if(prName!=null && !(prName.trim().equals("")) && !ServerSideValidationUtil.isAlpha(prName)){
						setMessage("Invalid Characters in Selected Field");
						tabId = "CSR-search";
						return "INVALID_PARAMETERS";
					}

					actionList = userDao.getUserActions(UserId);
					indexList = userDao.getIndexUsersList(Long.parseLong(prdbID),UserId,"Y");
					System.out.println("Search Index Size:::"+indexList.size());
					vaultIndexDesc = userDao.getIndexDescforName(Long.parseLong(prdbID), vaultIndex);
					if(indexList.size()!=0){
						searchIdxList = new HashMap<Long,String>();
						Long idx = 0L; String idxName = "";
						for(Iterator<UserDTO> iter= indexList.iterator();iter.hasNext();){
							UserDTO dto = (UserDTO) iter.next();
							idx = dto.getIndexId();
							idxName = "Idx_"+dto.getIndexName()+"_"+dto.getIndexDesc();
							searchIdxList.put(idx, idxName); 
						}

						System.out.println("SEARCH INDEX LIST searchIdxList:::"+searchIdxList.toString());

						List<String> selidxList = userDao.getUserColumnIndex(Long.parseLong(prdbID),UserId, "Y");
						System.out.println("select Index Size:::"+selidxList.size());
						selectIdxList = new ArrayList<String>();
						for(String idxstr:selidxList){
							String[] tempidx = idxstr.split("_");
							String idxName1 = tempidx[3];
							selectIdxList.add(idxName1);
						}

						System.out.println("SELECT INDEX LIST selectIdxList:::"+selectIdxList.toString());

						int j =1;
						for(String idx2:selectIdxList){
							if(j==selectIdxList.size())
								selectStr = selectStr+idx2;
							else
								selectStr = selectStr+idx2+",";
							j++;
						}

						/*Nithu Alexander: 24/09/2014
						 * Bug Fix to correct the Select String if No Indexes 
						 * are selected for the user in Select Index Results Tab
						 * (No entry in USER_SER_RES_INDEX) */

						if(!selectIdxList.contains(vaultIndex)){
							if(!(selectStr.trim().equals(""))){
								selectStr = selectStr+","+vaultIndex;
							}else{
								selectStr = vaultIndex;
							}
						}

						System.out.println("SELECT STRING selectStr for display of results:::"+selectStr);

						String tableName = servletRequest.getParameter("prName");
						docList = new HashMap<Integer,Map<String,String>>();
						searchStr = new ArrayList<String>();
						@SuppressWarnings("rawtypes")
						Enumeration params = servletRequest.getParameterNames();
						while (params.hasMoreElements()) {
							String paramName = params.nextElement().toString();
							String paramValue = servletRequest.getParameter(paramName);
							if (paramName.startsWith("textSearchs_Idx")&&paramValue.length()!=0) {
								//search = true;
								String temp[] = paramName.split("_");

								/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1 and SCT (VaultApp)*/
								String searchInptDataType = userDao.getonlyDataType(temp[3],tableName);

								if(searchInptDataType.equalsIgnoreCase("date")){
									if(paramValue!=null && !(paramValue.trim().equals("")) && !ServerSideValidationUtil.isValidDatePattern(paramValue)){
										setMessage("Invalid Characters entered in Search Field");
										tabId = "CSR-search";
										return "INVALID_PARAMETERS";
									}
								}else if((searchInptDataType.equalsIgnoreCase("int")) || (searchInptDataType.equalsIgnoreCase("bigint"))){
									if(paramValue!=null && !(paramValue.trim().equals("")) && !ServerSideValidationUtil.isNumeric(paramValue)){
										setMessage("Invalid Characters entered in Search Field");
										tabId = "CSR-search";
										return "INVALID_PARAMETERS";
									}
								}else{
									if(paramValue!=null && !(paramValue.trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithHyphenSpaceAndForwardSlash(paramValue)){
										setMessage("Invalid Characters entered in Search Field");
										tabId = "CSR-search";
										return "INVALID_PARAMETERS";
									}
								}
								/* Nithu Alexander: Change as per "XSS" and AVA-1 fixes (Cross-site scripting)
								 * Initialising search flag to 'true' after checking for Cross-Site scripting on user input search values*/
								search = true;
								String secsrc = temp[2]+"_"+paramValue;
								searchStr.add(secsrc);
							}
						}

						System.out.println("SEARCH STRING searchStr entered as search criteria :::"+searchStr);
						if(search == true){
							String conds = " where ";
							for(String str:searchStr){
								System.out.println("prior"+str);
								System.out.println("search str is"+StringEscapeUtils.escapeSql(str));
								//str=StringEscapeUtils.escapeHtml(str);
								Long sidx = Long.parseLong(str.split("_")[0]);
								String sIdxName =  userDao.getIndexName(Long.parseLong(prdbID),sidx);
								conds = conds+sIdxName+" = '"+StringEscapeUtils.escapeSql(str.split("_")[1])+"' and ";
							}

							/* Nithu Alexander: 05/09/2014
							 * Modifying Search query 'IN' clause to pick Filter Values 
							 * for Index Fields shown as Search Criteria for the User
							 * */
							filterSearchStr = new ArrayList();

							@SuppressWarnings("rawtypes")
							Enumeration filterParams = servletRequest.getParameterNames();


							while(filterParams.hasMoreElements()){
								String filterParamName = filterParams.nextElement().toString();
								if (filterParamName.startsWith("textSearchs_Idx")) {
									String filterTemp[] = filterParamName.split("_");
									String filterIndex = filterTemp[3];

									System.out.println("SCREEN SEARCH CRITERIA filterIndex "+filterIndex);
									if(filterIndex != null && !(filterIndex.trim().equals(""))){
										filterSearchStr.add(filterIndex.trim());
									}
								}
							}
							for(String condStrIndexName: filterSearchStr){

								if(condStrIndexName != null && !(condStrIndexName.trim().equals(""))){
									String filterCondResult = userDao.getUserIndexResultsForView(UserId, Long.parseLong(prdbID), condStrIndexName, "");

									System.out.println("FILTER STRING filterCondResult "+filterCondResult);

									if(filterCondResult != null && !(filterCondResult.trim().equals(""))){
										String filterCondVal = "";
										String[] filterCondArr = filterCondResult.split(",");
										for(String filterElement: filterCondArr){
											if(!(filterCondVal.equals(""))){
												filterCondVal = filterCondVal+",'"+filterElement+"'";
											}else{
												filterCondVal = "'"+filterElement+"'";
											}
										}

										System.out.println("CONDITION STRING AFTER APPLYING FILTER filterCondVal  "+filterCondVal);

										conds = conds+condStrIndexName+" IN( "+filterCondVal+") and ";

										System.out.println("FINAL CONDITION STRING conds String  "+conds);	


									}

								}
							}

							int index = conds.lastIndexOf("and");  
							if(index != -1) {  
								conds = conds.substring(0,index);  
							}
							System.out.println("select String:::"+selectStr);
							System.out.println("conditions"+conds);
							docList = dao.getDocData(tableName, selectStr, conds);
						}
						System.out.println("Number Of Documents retrieved:::"+docList.size());
					}
					else
						message = "Please configure search and result Indexes!!!";
					
					if(servletRequest.getParameter("vaultIdx") != null &&  servletRequest.getParameter("actType") != null ){
						String dbName = userDao.getDBName(UserId, Long.parseLong(prdbID));
						System.out.println("act type here"+servletRequest.getParameter("actType"));
						System.out.println("VaultIndexis"+vaultIndex);
						String vaultidx=servletRequest.getParameter("vaultIdx");
						List<DataDAO> list = dao.getDocInfo(vaultIndex, servletRequest.getParameter("vaultIdx"),servletRequest.getParameter("prName"));
						System.out.println("Number of Documents retrieved from VaultApp:::"+list.size());
						pageType = "search1"+UserId;
						sessionku.setAttribute("pageType", pageType);
						
						if( servletRequest.getParameter("actType").equalsIgnoreCase("view")){
							System.out.println("Document actType::"+servletRequest.getParameter("actType"));
							actType = "view";
							
							for(DataDAO ddao : list){
								Database db = Common.getDBConnection(dbName);
								System.out.println("printing db");
								System.out.println(db);
								System.out.println(db.getName());
								System.out.println(db.getDescription());
								System.out.println("printed db");
								System.out.println("Vault db::"+ddao.getAccNumber());
								
								Account acct= null;

								try{
									SearchIndex acctSearchIndex = db.findSearchIndexByMatcher(SearchIndex.FindsAccountByAccountNumber);
									if(acctSearchIndex == null){
										System.err.printf("ERROR: unable to find Account search index for Database \"%s\"\n", new Object[] {
												db.getName()
										});
									} else{
										SearchMatchesIterator acctSearchIter = db.searchForAccounts(acctSearchIndex, ddao.getAccNumber());
										
										int count = 0;
										while(acctSearchIter.hasNext()) {
											List accounts = acctSearchIter.next();
											acct = (Account)accounts.get(0);
											System.out.println("Account VO after getting accounts.get(0) ----------->"+acct);
											Iterator i = accounts.iterator();
											while(i.hasNext()) {
												Account account = (Account)i.next();
												count++;
												System.out.println();
												System.out.println((new StringBuilder()).append("( ").append(count).append(" ) --------------------------------------------------------------------").toString());
												System.out.println((new StringBuilder()).append("    Account #: ").append(account.getAccountNumber()).toString());
												System.out.println((new StringBuilder()).append("    Name:      ").append(account.getName()).toString());
												System.out.println((new StringBuilder()).append("    Address:   ").append(account.getAddress()).toString());
											}
										}

										if(acct == null){
											acct = db.getAccount(ddao.getAccNumber());
											System.out.println("Account VO from getAccount() ----------->"+acct);
										}
										System.out.println((new StringBuilder()).append("acct").append(acct).toString());
									}
								}
								catch(VaultException e){
									System.err.printf("Caught VaultException: %s\n", new Object[] {
											e.getMessage()
									});
									e.printStackTrace();
									System.out.println("Document not found. Vault Exception Encountered in Account Number Search");
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found. Vault Exception Encountered in Account Number Search");
									accountNumber="s";
								}
								
								catch(ServerErrorException e){
									System.err.printf("Caught ServerErrorException: errorCode=%d, errorMessage=\"%s\"\n", new Object[] {
											Integer.valueOf(e.getErrorCode()), e.getErrorMessage()
									});
									e.printStackTrace();
									System.out.println("Document not found. Vault Exception Encountered in Account Number Search");
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found. Vault Server Exception Encountered in Account Number Search");
									accountNumber="s";
								}
								
								catch(Exception e){
									e.printStackTrace();
									System.out.println("Document not found. Vault Exception Encountered in Account Number Search");
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found. Vault Server Exception Encountered in Account Number Search");
									accountNumber="s";
								}

								if(acct==null){
									System.out.println("Document Not Found. Account Number for Requested Document is Empty or Invalid");
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document Not Found. Account Number for Requested Document is Empty or Invalid");
									accountNumber="s";
								}

								if(acct != null){
									
									SearchMatchesIterator<Document>  si=null;
									try {
										si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);

									}
									catch(Exception e) {
										e.printStackTrace();
										System.out.println("Document not found. Exception Encountered in Document Search for Requested Account Number and Date");
										accountNumber="s";
										setMessage("Document not found. Exception Encountered in Document Search for Requested Account Number and Date");
										getServletRequest().getSession().setAttribute("documentfound",false);
										accountNumber="s";
										return "SUCCESS";
									}
									/*Nithu Alexander: 14th Aug 2015
									 * Removing int expectedResult = 500 since it is moved to property file
									 * */
									int i=0; 
									
									DocumentHandle selectedDocHandle = new DocumentHandle();
									Document selectedDoc = null;
									docHandle = new DocumentHandle();
									
									System.out.println("SI VALUE ---------> "+si.hasNext());
									a:while(si.hasNext()){
										
										System.out.println("Documents for requested account number and date are found");
										List<Document> docs = si.next();
										System.out.println("Size of Document List ---------->"+docs.size());
										System.out.println("Document List ---------->"+docs.toString());
										bb:	for(Document d1 : docs){
											Map<String,List<String>> docInfo = new HashMap<String,List<String>>();
											docInfo = d1.getDocumentInfo().getCustomAttributes();
											System.out.println("Document Info ---------->"+d1.getDocumentInfo().toString());
											
											System.out.println("docInfo::"+docInfo);
											for(String s:docInfo.keySet()){
												System.out.println("S is"+s);
												System.out.println("VaultIndexis"+vaultIndex);
												if(s.equalsIgnoreCase(vaultIndex)){
													if(vaultIndex.equalsIgnoreCase("DRN")){

														System.out.println("DOCINFO get(s)"+docInfo.get(s));
														System.out.println("DRN"+ddao.getDocRefNo());
														if(docInfo.get(s).contains(ddao.getDocRefNo())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															System.out.println("document format::"+d1.getFormat()+" and type::"+type);
															selectedDocHandle.setDate(d1.getDate());
															System.out.println("1 doc date"+d1.getDate());
															selectedDocHandle.setType(type);
															System.out.println("1 set type"+type);
															System.out.println("Doc reference"+ddao.getDocRefNo());
															System.out.println("gets"+docInfo.get(s));
															selectedDocHandle.setFile(d1.getFile());
															System.out.println("1 File is"+d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															System.out.println("1 Pointer here"+d1.getPointer());
															System.out.println("selected handle"+selectedDocHandle.toString());
															selectedDoc = d1;

															accountNumber = d1.getAccountNumber();
															/* Nithu Alexander: 01 Sep 2015
															 * Adding new request parameters (docPageCount,docFormat,outputFormats,imageOutput,pdfOutput,
															 * textOutput,tiffOutput,docInfoType) to Interface Servlet as part of Document View logic Re-structuring
															 * */
															docPageCount = d1.getPageCount();
															docFormat = d1.getFormat();
															Set<OutputFormat> docOutputFormats = d1.getOutputFormats();
															
															StringBuilder outformatStr = new StringBuilder();
															for (OutputFormat of : docOutputFormats) {
																if (outformatStr != null && outformatStr.toString().length()>0){
																	outformatStr.append(",");
																	outformatStr.append(of.getName());
																}else{
																	outformatStr.append(of.getName());
																}
															}
															
															if(outformatStr != null){
																outputFormats = outformatStr.toString();
															}
															
															imageOutput = (docOutputFormats.contains(OutputFormat.GIF) || docOutputFormats.contains(OutputFormat.PNG));
															pdfOutput = docOutputFormats.contains(OutputFormat.PDF);
															textOutput = docOutputFormats.contains(OutputFormat.TEXT);
															tiffOutput = docOutputFormats.contains(OutputFormat.TIFF);
															
															docInfoType = d1.getDocumentInfo().getType();	
															docHandle=selectedDocHandle;
															
															foundDocument=true;
															System.out.println("Vaultidx is"+vaultidx);
															System.out.println("selected handle1 finally"+selectedDocHandle.toString());
															getServletRequest().getSession().setAttribute("documentfound",foundDocument);
															setMessage("");
															break bb;
														}
													}
													else if(vaultIndex.equalsIgnoreCase("Guid")){
														if(docInfo.get(s).contains(ddao.getGuid())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															selectedDocHandle.setDate(d1.getDate());
															selectedDocHandle.setType(type);
															selectedDocHandle.setFile(d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();

															foundDocument=true;
															setMessage("");
															getServletRequest().getSession().setAttribute("documentfound",foundDocument);
															System.out.println("Guid selected handle"+selectedDocHandle.toString());
														}
													}
													else if(vaultIndex.equalsIgnoreCase("DocId")){
														if(docInfo.get(s).contains(ddao.getDocId())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															selectedDocHandle.setDate(d1.getDate());
															selectedDocHandle.setType(type);
															selectedDocHandle.setFile(d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();
															foundDocument=true;
															getServletRequest().getSession().setAttribute("documentfound",foundDocument);
															setMessage("");
															System.out.println("docid selected handle"+selectedDocHandle.toString());
														}
													}else{
														//no condition required
													}
													//	break;
												} else {

													String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
													System.out.println("Doc format"+type);
													selectedDocHandle.setDate(d1.getDate());
													selectedDocHandle.setType(type);
													selectedDocHandle.setFile(d1.getFile());
													selectedDocHandle.setPointer(d1.getPointer());
													selectedDoc = d1;
													accountNumber = d1.getAccountNumber();
													docHandle=selectedDocHandle;
													System.out.println("collection selected handle"+selectedDocHandle.toString());
													setMessage("");

													getServletRequest().getSession().setAttribute("documentfound",true);

													//return "SUCCESS";
													/* if(prName.equalsIgnoreCase("correspondences")){
														getServletRequest().getSession().setAttribute("documentfound",false);
														System.out.println("Invalid Database Configured for Collections Format");
														setMessage("Document not found");
													} */


													if(docHandle==null || (docHandle.toString().contains("null"))){
														foundDocument=false;	
														getServletRequest().getSession().setAttribute("documentfound",false);
														setMessage("Document not found");

													}

												}
											}
											/* Nithu Alexander: 14 Aug 2015, Adding expected Max Result=500 parameter in property file */
											if(i>expectedMaxResult){
												System.out.println("Database has more than " + expectedMaxResult + " statements");
												break a;
											}
											
											i++;
										}
									}
									
									System.out.println("Reached");
									//docHandle = selectedDocHandle;
									databaseName = db.getName();
									attachmentPath = ddao.getAttachmentPath();
									attachment1 = getControlledCharacterPresence(ddao.getAttachment1());
									attachment2 = getControlledCharacterPresence(ddao.getAttachment2());
									attachment3 = getControlledCharacterPresence(ddao.getAttachment3());
									attachment4 = getControlledCharacterPresence(ddao.getAttachment4());


									System.out.println("Very Finally selected handle"+selectedDocHandle.toString());
									
									if(selectedDocHandle!=null && selectedDocHandle.toString().contains("null")){
										System.out.println("Document not found. SelectedDocHandle is NULL");
										foundDocument=false;	
										getServletRequest().getSession().setAttribute("documentfound",foundDocument);
										setMessage("Document not found");
										/*Nithu Alexander: 26/09/2014
										 * Fix for Document View Issue where Document Search page was being displayed
										 * after clicking on 'View' link in the search results. This is due to accountNumber
										 * being EMPTY STRING */
										accountNumber="s";
									}
									if(selectedDoc==null){
										System.out.println("Document not found. Selected Document is NULL");
										foundDocument=false;	
										getServletRequest().getSession().setAttribute("documentfound",foundDocument);
										setMessage("Document not found");
										/*Nithu Alexander: 26/09/2014
										 * Fix for Document View Issue where Document Search page was being displayed
										 * after clicking on 'View' link in the search results. This is due to accountNumber
										 * being EMPTY STRING */
										accountNumber="s";
									}

									HashMap<Object,Object> map = new HashMap<Object,Object>();
									if (logId != 0) {
										map.put("DBName",databaseName);
										map.put("accNum", accountNumber);
										map.put("Name", acct.getName());
										map.put("count", 0);
										map.put("serId", serId);
										map.put("logId", logId);
										map.put("sresId", sresId);
										long accId = resDao.saveAccountDetails(map);

										map.put("docName", selectedDoc.getName());
										map.put("Desc", selectedDoc.getDocumentInfo());
										map.put("docType", selectedDoc.getType());
										map.put("docDate", selectedDoc.getDate());
										map.put("accId", accId);
										map.put("logId", logId);
										long docuId = resDao.saveDocumentDetails(map);

										if (docuId != 0) {
											map.put("logId", logId);
											map.put("docId", docuId);
											map.put("action", "view");
											resDao.saveDocumentActionDetails(map);
										}
									}
									sessionku.setAttribute("SELECT_DB", db.getName());
									sessionku.setAttribute("SELECT_DB_DESC", db.getDescription());
								}else{
									System.out.println("Document Not Found. Account Number for Requested Document is Empty or Invalid");
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document Not Found. Account Number for Requested Document is Empty or Invalid");
									accountNumber="s";
								}
							}
						}else if( servletRequest.getParameter("actType").equalsIgnoreCase("download")){
							boolean foundDownloadDoc=false;
							actType = "download";
							System.out.println("Document actType::"+servletRequest.getParameter("actType"));
							VaultClient vaultClient = Common.getVaultConnection();
							System.out.println("got vaulr db");
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							OutputStream output = getServletResponse().getOutputStream();
							System.out.println("got outstream");

							for(DataDAO ddao : list){
								Database db = Common.getDBConnection(dbName);
								//Database db = null;
								System.out.println("got db");
								Account acct = null;

								try{
									SearchIndex acctSearchIndex = db.findSearchIndexByMatcher(SearchIndex.FindsAccountByAccountNumber);
									if(acctSearchIndex == null){
										System.err.printf("ERROR: unable to find Account search index for Database \"%s\"\n", new Object[] {
												db.getName()
										});
									} else{
										SearchMatchesIterator acctSearchIter = db.searchForAccounts(acctSearchIndex, ddao.getAccNumber());
										int count = 0;
										while(acctSearchIter.hasNext()) {
											List accounts = acctSearchIter.next();
											acct = (Account)accounts.get(0);
											Iterator i = accounts.iterator();
											while(i.hasNext()) {
												Account account = (Account)i.next();
												count++;
												System.out.println();
												System.out.println((new StringBuilder()).append("( ").append(count).append(" ) --------------------------------------------------------------------").toString());
												System.out.println((new StringBuilder()).append("    Account #: ").append(account.getAccountNumber()).toString());
												System.out.println((new StringBuilder()).append("    Name:      ").append(account.getName()).toString());
												System.out.println((new StringBuilder()).append("    Address:   ").append(account.getAddress()).toString());
											}
										}

										if(acct == null) {
											acct = db.getAccount(ddao.getAccNumber());
										}
										System.out.println((new StringBuilder()).append("acct").append(acct).toString());
									}
								}catch(VaultException e){
									System.err.printf("Caught VaultException: %s\n", new Object[] {
											e.getMessage()
									});
									e.printStackTrace();
									foundDownloadDoc=false;	
									getServletRequest().getSession().setAttribute("downloadDocumentFlag",foundDownloadDoc);
									setMessage("Document not found");
									accountNumber="s";
								}catch(ServerErrorException e){
									System.err.printf("Caught ServerErrorException: errorCode=%d, errorMessage=\"%s\"\n", new Object[] {
											Integer.valueOf(e.getErrorCode()), e.getErrorMessage()
									});
									e.printStackTrace();
									foundDownloadDoc=false;	
									getServletRequest().getSession().setAttribute("downloadDocumentFlag",foundDownloadDoc);
									setMessage("Document not found");
									accountNumber="s";
								}
								catch(Exception e){
									e.printStackTrace();
									System.out.println(e);
									foundDownloadDoc=false;	
									getServletRequest().getSession().setAttribute("downloadDocumentFlag",foundDownloadDoc);
									setMessage("Document not found");
									accountNumber="s";
								}
								if(acct==null){
									System.out.println("Docrefno is"+ddao.getDocRefNo());
									foundDownloadDoc=false;	
									getServletRequest().getSession().setAttribute("downloadDocumentFlag",foundDownloadDoc);
									setMessage("Document not found");
									accountNumber="s";
								}
								System.out.println("got accnt"+ddao.getAccNumber());
								System.out.println("acct"+acct);
								
								if(acct != null){
									SearchMatchesIterator<Document>  si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);
									int expectedResult = 500,i=0; 
									DocumentHandle selectedDocHandle = new DocumentHandle();
									Document selectedDoc = null;
									docHandle = new DocumentHandle();
									a:while(si.hasNext()){
										System.out.println("Documents for requested account number and date are found");
										List<Document> docs = si.next();
										bb:	for(Document d1 : docs){
											Map<String,List<String>> docInfo = new HashMap<String,List<String>>();
											docInfo = d1.getDocumentInfo().getCustomAttributes();
											for(String s:docInfo.keySet()){
												if(s.equalsIgnoreCase(vaultIndex)){
													if(vaultIndex.equalsIgnoreCase("DRN")){
														System.out.println("index is DRN");
														if(docInfo.get(s).contains(ddao.getDocRefNo())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															System.out.println("drn doc ref number is"+ddao.getDocRefNo());
															selectedDocHandle.setDate(d1.getDate());
															System.out.println("drn date is"+d1.getDate());
															selectedDocHandle.setType(type);
															System.out.println("drn type is"+type);
															selectedDocHandle.setFile(d1.getFile());
															System.out.println("drn file is"+d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															System.out.println("drn pointer is"+d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();

															System.out.println("dselected handle"+selectedDocHandle.toString());

															docHandle = selectedDocHandle;
															RenderOptions renderOptions = new RenderOptions();
															renderOptions.setEnableBackground(true);
															foundDownloadDoc=true;
															
															/*vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																	docHandle.getFile(), docHandle.getPointer(),
																	OutputFormat.PDF, 1, selectedDoc.getPageCount(),
																	ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);*/
															if(OutputFormat.PDF.equals(true)){//Add
																vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																		docHandle.getFile(), docHandle.getPointer(),
																		OutputFormat.PDF, 1, selectedDoc.getPageCount(),
																		ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
																}//Add
																//Add
																else if(OutputFormat.TIFF.equals(true)){
																	vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																			docHandle.getFile(), docHandle.getPointer(),
																			OutputFormat.TIFF, 1, selectedDoc.getPageCount(),
																			ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
																	}
																
																//Add
																else{
																System.out.println("Error in Render L1266");	
																}
																}

															getServletResponse().setHeader("Content-Disposition","attachment; filename=\"download.pdf\"");
															getServletResponse().setContentType("application/x-download");
															System.out.println("pdf Byte array is"+baos);
															System.out.println("pdf Byte array size"+baos.size());
															output.write(baos.toByteArray());
															output.close();
															getServletRequest().getSession().setAttribute("downloadDocumentFlag",foundDownloadDoc);
															setMessage("");
															break bb;
														}

													}
													else if(vaultIndex.equalsIgnoreCase("Guid")){
														if(docInfo.get(s).contains(ddao.getGuid())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															selectedDocHandle.setDate(d1.getDate());
															selectedDocHandle.setType(type);
															selectedDocHandle.setFile(d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();
															System.out.println("dselected handle"+selectedDocHandle.toString());
														}
													}
													else if(vaultIndex.equalsIgnoreCase("DocId")){
														if(docInfo.get(s).contains(ddao.getDocId())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															selectedDocHandle.setDate(d1.getDate());
															selectedDocHandle.setType(type);
															selectedDocHandle.setFile(d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();

															System.out.println("selected handle"+selectedDocHandle.toString());
														}
													}
												//}
											else{
													System.out.println("Else here");
													String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
													System.out.println("type is ----------->"+type);

													System.out.println(docInfo.get(s)+"==="+ddao.getDocRefNo());
													System.out.println(d1.getFile()+"==="+d1.getPointer());
													if(type.equalsIgnoreCase(".mht")){
														System.out
														.println("Doc format inside collection "+type);
														selectedDocHandle.setDate(d1.getDate());
														System.out
														.println("date is "+d1.getDate());
														System.out
														.println("type is"+type);
														System.out
														.println("file is"+d1.getFile());
														System.out
														.println("pointr is"+d1.getPointer());
														selectedDocHandle.setType(type);
														selectedDocHandle.setFile(d1.getFile());
														selectedDocHandle.setPointer(d1.getPointer());
														selectedDoc = d1;
														accountNumber = d1.getAccountNumber();
														System.out
														.println("accountnumber ");
														docHandle=selectedDocHandle;
														foundDownloadDoc=true;
														System.out
														.println("selected handle"+selectedDocHandle.toString());
														getServletResponse().setHeader("Content-Disposition","attachment; filename=\"download.mht\"");
														getServletResponse().setContentType("application/x-download");
														docHandle = selectedDocHandle;
														RenderOptions renderOptions = new RenderOptions();
														renderOptions.setEnableBackground(true);
														vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																docHandle.getFile(), docHandle.getPointer(),
																OutputFormat.COLLECT, 1, selectedDoc.getPageCount(),
																ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
														System.out.println("mht Byte array is"+baos);
														System.out.println("mht Byte array size"+baos.size());

														output.write(baos.toByteArray());
														output.close();

														//vaultClient.submitForReprint(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7)
														break bb;
													}

													else if(type.equalsIgnoreCase(".txt")){
														System.out.println("Doc format inside collection "+type);
														System.out.println("doc format is"+d1.getFormat());
														selectedDocHandle.setDate(d1.getDate());
														System.out.println("date is "+d1.getDate());
														System.out.println("type is"+type);
														System.out.println("file is"+d1.getFile());
														System.out.println("pointr is"+d1.getPointer());
														selectedDocHandle.setType(type);
														selectedDocHandle.setFile(d1.getFile());
														selectedDocHandle.setPointer(d1.getPointer());
														selectedDoc = d1;
														accountNumber = d1.getAccountNumber();
														System.out.println("accountnumber ");
														docHandle=selectedDocHandle;
														foundDownloadDoc=true;
														System.out.println("selected handle"+selectedDocHandle.toString());
														getServletResponse().setHeader("Content-Disposition","attachment; filename=\"download.txt\"");
														getServletResponse().setContentType("text/plain");
														docHandle = selectedDocHandle;
														RenderOptions renderOptions = new RenderOptions();
														renderOptions.setEnableBackground(true);
														vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																docHandle.getFile(), docHandle.getPointer(),
																OutputFormat.COLLECT, 1, selectedDoc.getPageCount(),
																ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
														System.out.println("txt Byte array is"+baos);
														System.out.println("txt Byte array size"+baos.size());
														output.write(baos.toByteArray());
														output.flush();
														output.close();
														System.out.println("Wrote successfully sms");
														return null;

													}else if(type.equalsIgnoreCase("afp")){
														
													}
												}




											}
											if(i>expectedResult){
												System.out.println("Database has more than " + expectedResult + " statements");
												break a;
											}
											i++;
										}
										if(!foundDownloadDoc){
											getServletResponse().setHeader("Content-Disposition","attachment; filename=\"notavailable.txt\"");
											getServletResponse().setContentType("text/plain");
											getServletResponse().getOutputStream().write("Document not available".getBytes());
											getServletResponse().getOutputStream().flush();
											getServletResponse().getOutputStream().close();
											return null;
										}
									}
									
								}else{
									getServletResponse().setHeader("Content-Disposition","attachment; filename=\"notavailable.txt\"");
									getServletResponse().setContentType("text/plain");
									getServletResponse().getOutputStream().write("Document not available".getBytes());
									getServletResponse().getOutputStream().flush();
									getServletResponse().getOutputStream().close();
									return null;
								}
							}
							output.close();
						} else if( servletRequest.getParameter("actType").equalsIgnoreCase("print")){
							actType = "print";
							System.out.println("Document actType::"+servletRequest.getParameter("actType"));
							VaultClient vaultClient = Common.getVaultConnection();
							boolean printdocumentavailable=false;
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							OutputStream output = getServletResponse().getOutputStream();
							for(DataDAO ddao : list){
								Database db = Common.getDBConnection(dbName);
								//Database db = null;
								Account acct = null;



								/*Account acct = db.getAccount(ddao.getAccNumber());
									if(acct==null)
									{
										System.out.println("Docrefno is"+ddao.getDocRefNo());
										acct=db.getAccount(ddao.getDocRefNo());
									}*/

								try
								{
									SearchIndex acctSearchIndex = db.findSearchIndexByMatcher(SearchIndex.FindsAccountByAccountNumber);
									if(acctSearchIndex == null)
									{
										System.err.printf("ERROR: unable to find Account search index for Database \"%s\"\n", new Object[] {
												db.getName()
										});
									} else
									{
										SearchMatchesIterator acctSearchIter = db.searchForAccounts(acctSearchIndex, ddao.getAccNumber());
										int count = 0;
										while(acctSearchIter.hasNext()) 
										{
											List accounts = acctSearchIter.next();
											acct = (Account)accounts.get(0);
											Iterator i = accounts.iterator();
											while(i.hasNext()) 
											{
												Account account = (Account)i.next();
												count++;
												System.out.println();
												System.out.println((new StringBuilder()).append("( ").append(count).append(" ) --------------------------------------------------------------------").toString());
												System.out.println((new StringBuilder()).append("    Account #: ").append(account.getAccountNumber()).toString());
												System.out.println((new StringBuilder()).append("    Name:      ").append(account.getName()).toString());
												System.out.println((new StringBuilder()).append("    Address:   ").append(account.getAddress()).toString());
											}
										}

										if(acct == null)
										{
											acct = db.getAccount(ddao.getAccNumber());
										}
										System.out.println((new StringBuilder()).append("acct").append(acct).toString());



										// System.out.println();
										//  System.out.println((new StringBuilder()).append("Found ").append(count).append(" accounts matching search key \"").append(ddao.getAccNumber()).append("\"").toString());
									}
								}
								catch(VaultException e)
								{
									System.err.printf("Caught VaultException: %s\n", new Object[] {
											e.getMessage()
									});
									e.printStackTrace(System.err);
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found");
									accountNumber="s";
								}
								catch(ServerErrorException e)
								{
									System.err.printf("Caught ServerErrorException: errorCode=%d, errorMessage=\"%s\"\n", new Object[] {
											Integer.valueOf(e.getErrorCode()), e.getErrorMessage()
									});
									e.printStackTrace(System.err);
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found");
									accountNumber="s";
								}
								catch(Exception e)
								{
									e.printStackTrace();
									System.out.println(e);
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found");
									accountNumber="s";
								}



								if(acct==null)
								{

									System.out.println("account is null");
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found");
									accountNumber="s";
									//	accountNumber="x";
									//return "SUCCESS";
								}


								if(acct != null){
									SearchMatchesIterator<Document>  si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);
									int expectedResult = 500,i=0; 
									DocumentHandle selectedDocHandle = new DocumentHandle();
									Document selectedDoc = null;
									docHandle = new DocumentHandle();
									a:while(si.hasNext()){
										System.out.println("Documents for requested account number and date are found");
										List<Document> docs = si.next();
										bb:	for(Document d1 : docs){
											Map<String,List<String>> docInfo = new HashMap<String,List<String>>();
											docInfo = d1.getDocumentInfo().getCustomAttributes();
											for(String s:docInfo.keySet()){
												if(s.equalsIgnoreCase(vaultIndex)){
													if(vaultIndex.equalsIgnoreCase("DRN")){
														if(docInfo.get(s).contains(ddao.getDocRefNo())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															selectedDocHandle.setDate(d1.getDate());
															selectedDocHandle.setType(type);
															selectedDocHandle.setFile(d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();
															docHandle=selectedDocHandle;
															//	ByteArrayOutputStream baos = new ByteArrayOutputStream();
															//	OutputStream output = getServletResponse().getOutputStream();
															System.out.println("got outstream");
															getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.pdf\"");
															getServletResponse().setContentType("application/x-download");
															RenderOptions renderOptions = new RenderOptions();
															renderOptions.setEnableBackground(true);
															printdocumentavailable=true;
															if(OutputFormat.PDF.equals(true)){//Add
															vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																	docHandle.getFile(), docHandle.getPointer(),
																	OutputFormat.PDF, 1, selectedDoc.getPageCount(),
																	ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
															}//Add
															//Add
														else if(OutputFormat.TIFF.equals(true)){
																vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																		docHandle.getFile(), docHandle.getPointer(),
																		OutputFormat.TIFF, 1, selectedDoc.getPageCount(),
																		ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
																}
														else{//Add
														System.out.println("Error while render L1577");	
														}
														}//Add
															
															//Add
															output.write(baos.toByteArray());
															output.close();
															//return null;
															break bb;



														}
													}
													else if(vaultIndex.equalsIgnoreCase("Guid")){
														if(docInfo.get(s).contains(ddao.getGuid())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															selectedDocHandle.setDate(d1.getDate());
															selectedDocHandle.setType(type);
															selectedDocHandle.setFile(d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();
														}
													}
													else if(vaultIndex.equalsIgnoreCase("DocId")){
														if(docInfo.get(s).contains(ddao.getDocId())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															selectedDocHandle.setDate(d1.getDate());
															selectedDocHandle.setType(type);
															selectedDocHandle.setFile(d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();
														}
													}
												//}


												else
												{//ByteArrayOutputStream baos = new ByteArrayOutputStream();

													//	OutputStream output = getServletResponse().getOutputStream();
													System.out
													.println("Else ehere");
													String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
													System.out
													.println("type is"+type);

													System.out
													.println(docInfo.get(s)+"==="+ddao.getDocRefNo());
													System.out
													.println(d1.getFile()+"==="+d1.getPointer());



													/*if(prName.equalsIgnoreCase("correspondences"))
														{
															getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.txt\"");
															getServletResponse().setContentType("text/plain");
															docHandle = selectedDocHandle;
															RenderOptions renderOptions = new RenderOptions();
															renderOptions.setEnableBackground(true);
															vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																						docHandle.getFile(), docHandle.getPointer(),
																						OutputFormat.COLLECT, 1, selectedDoc.getPageCount(),
																						ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
															output.write("Document not available".getBytes());
															output.close();
															return null;
														}*/





													if(type.equalsIgnoreCase(".mht")){
														System.out
														.println("Doc format inside collection "+type);
														selectedDocHandle.setDate(d1.getDate());
														System.out
														.println("date is "+d1.getDate());
														System.out
														.println("type is"+type);
														System.out
														.println("file is"+d1.getFile());
														System.out
														.println("pointr is"+d1.getPointer());
														selectedDocHandle.setType(type);
														selectedDocHandle.setFile(d1.getFile());
														selectedDocHandle.setPointer(d1.getPointer());
														selectedDoc = d1;
														accountNumber = d1.getAccountNumber();
														System.out
														.println("accountnumber ");
														docHandle=selectedDocHandle;
														printdocumentavailable=true;
														System.out
														.println("selected handle"+selectedDocHandle.toString());
														getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.mht\"");
														getServletResponse().setContentType("application/x-download");
														docHandle = selectedDocHandle;
														RenderOptions renderOptions = new RenderOptions();
														renderOptions.setEnableBackground(true);
														vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																docHandle.getFile(), docHandle.getPointer(),
																OutputFormat.COLLECT, 1, selectedDoc.getPageCount(),
																ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
														output.write(baos.toByteArray());
														output.close();
														break bb;
													}




													else if(type.equalsIgnoreCase(".txt")){printdocumentavailable=true;
													System.out
													.println("Doc format inside collection "+type);
													selectedDocHandle.setDate(d1.getDate());
													System.out
													.println("date is "+d1.getDate());
													System.out
													.println("type is"+type);
													System.out
													.println("file is"+d1.getFile());
													System.out
													.println("pointr is"+d1.getPointer());
													selectedDocHandle.setType(type);
													selectedDocHandle.setFile(d1.getFile());
													selectedDocHandle.setPointer(d1.getPointer());
													selectedDoc = d1;
													accountNumber = d1.getAccountNumber();
													System.out
													.println("accountnumber ");
													docHandle=selectedDocHandle;
													System.out
													.println("selected handle"+selectedDocHandle.toString());
													getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.txt\"");
													getServletResponse().setContentType("text/plain");
													docHandle = selectedDocHandle;
													RenderOptions renderOptions = new RenderOptions();
													renderOptions.setEnableBackground(true);
													vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
															docHandle.getFile(), docHandle.getPointer(),
															OutputFormat.COLLECT, 1, selectedDoc.getPageCount(),
															ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
													output.write(baos.toByteArray());
													output.close();
													return null;
													//break bb;
													}
													else if(type.equalsIgnoreCase("afp"))
													{
														/*getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.txt\"");
															getServletResponse().setContentType("text/plain");

															output.write("Document not available".getBytes());
															output.close();
															return null;*/

													}


													//break bb;
												}








											}
											if(i>expectedResult){
												System.out.println("Database has more than " + expectedResult + " statements");
												break a;
											}
											i++;
										}


										if(!printdocumentavailable)

										{

											getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.txt\"");
											getServletResponse().setContentType("text/plain");

											output.write("Document not available".getBytes());
											output.close();
											return null;
										}

									}
									//	docHandle = selectedDocHandle;
									/*RenderOptions renderOptions = new RenderOptions();
										renderOptions.setEnableBackground(false);
										vaultClient.renderDirect(db.getName(), accountNumber,
																docHandle.getDate(), docHandle.getFile(),
																docHandle.getPointer(), OutputFormat.PDF, 1,
																selectedDoc.getPageCount(),
																ResolutionWidth.NONE, Rotation.NONE,
																renderOptions, baos);

										getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.pdf\"");
										getServletResponse().setContentType("application/pdf");


										OutputStream output = getServletResponse().getOutputStream();

										baos.writeTo(output);

										System.out.println("print message::"+message);
										output.close();*/
								}
								else
								{

									getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.txt\"");
									getServletResponse().setContentType("text/plain");
									/*docHandle = selectedDocHandle;
										RenderOptions renderOptions = new RenderOptions();
										renderOptions.setEnableBackground(true);
										vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																	docHandle.getFile(), docHandle.getPointer(),
																	OutputFormat.COLLECT, 1, selectedDoc.getPageCount(),
																	ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);*/
									output.write("Document not available".getBytes());
									output.close();
									return null;
								}
							}
						}

						else
						{

							System.out.println("start reprint");
							boolean reprintdocavailable=false;

							actType = "reprint";
							System.out.println("Document actType::"+servletRequest.getParameter("actType"));
							VaultClient vaultClient = Common.getVaultConnection();

							//ByteArrayOutputStream baos = new ByteArrayOutputStream();
							OutputStream output = getServletResponse().getOutputStream();
							for(DataDAO ddao : list){
								Database db = Common.getDBConnection(dbName);
								//Database db = null;
								Account acct = null;

								/*Account acct = db.getAccount(ddao.getAccNumber());
								if(acct==null)
								{
									System.out.println("Docrefno is"+ddao.getDocRefNo());
									acct=db.getAccount(ddao.getDocRefNo());
								}*/

								try
								{
									SearchIndex acctSearchIndex = db.findSearchIndexByMatcher(SearchIndex.FindsAccountByAccountNumber);
									if(acctSearchIndex == null)
									{
										System.err.printf("ERROR: unable to find Account search index for Database \"%s\"\n", new Object[] {
												db.getName()
										});
									} else
									{
										SearchMatchesIterator acctSearchIter = db.searchForAccounts(acctSearchIndex, ddao.getAccNumber());
										int count = 0;
										while(acctSearchIter.hasNext()) 
										{
											List accounts = acctSearchIter.next();
											acct = (Account)accounts.get(0);
											Iterator i = accounts.iterator();
											while(i.hasNext()) 
											{
												Account account = (Account)i.next();
												count++;
												System.out.println();
												System.out.println((new StringBuilder()).append("( ").append(count).append(" ) --------------------------------------------------------------------").toString());
												System.out.println((new StringBuilder()).append("    Account #: ").append(account.getAccountNumber()).toString());
												System.out.println((new StringBuilder()).append("    Name:      ").append(account.getName()).toString());
												System.out.println((new StringBuilder()).append("    Address:   ").append(account.getAddress()).toString());
											}
										}

										if(acct == null)
										{
											acct = db.getAccount(ddao.getAccNumber());
										}
										System.out.println((new StringBuilder()).append("acct").append(acct).toString());



										// System.out.println();
										//  System.out.println((new StringBuilder()).append("Found ").append(count).append(" accounts matching search key \"").append(ddao.getAccNumber()).append("\"").toString());
									}
								}
								catch(VaultException e)
								{
									System.err.printf("Caught VaultException: %s\n", new Object[] {
											e.getMessage()
									});
									e.printStackTrace();
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found");
									accountNumber="s";
								}
								catch(ServerErrorException e)
								{
									System.err.printf("Caught ServerErrorException: errorCode=%d, errorMessage=\"%s\"\n", new Object[] {
											Integer.valueOf(e.getErrorCode()), e.getErrorMessage()
									});
									e.printStackTrace(System.err);
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found");
									accountNumber="s";
								}
								catch(Exception e)
								{
									e.printStackTrace();
									System.out.println(e);
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found");
									accountNumber="s";
								}



								if(acct==null)
								{

									System.out.println("account is null");
									foundDocument=false;	
									getServletRequest().getSession().setAttribute("documentfound",foundDocument);
									setMessage("Document not found");
									accountNumber="s";
									//	accountNumber="x";
									//return "SUCCESS";
								}



								if(acct != null){
									SearchMatchesIterator<Document>  si = acct.searchForDocuments(ddao.getCreated_dt(), Integer.MAX_VALUE);
									int expectedResult = 500,i=0; 
									DocumentHandle selectedDocHandle = new DocumentHandle();
									Document selectedDoc = null;
									docHandle = new DocumentHandle();
									a:while(si.hasNext()){
										System.out.println("Documents for requested account number and date are found");
										List<Document> docs = si.next();
										bb:	for(Document d1 : docs){
											Map<String,List<String>> docInfo = new HashMap<String,List<String>>();
											docInfo = d1.getDocumentInfo().getCustomAttributes();
											for(String s:docInfo.keySet()){
												if(s.equalsIgnoreCase(vaultIndex)){
													if(vaultIndex.equalsIgnoreCase("DRN")){
														if(docInfo.get(s).contains(ddao.getDocRefNo())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															selectedDocHandle.setDate(d1.getDate());
															selectedDocHandle.setType(type);
															selectedDocHandle.setFile(d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();
															docHandle=selectedDocHandle;
															//	ByteArrayOutputStream baos = new ByteArrayOutputStream();
															//	OutputStream output = getServletResponse().getOutputStream();
															System.out.println("got outstream");
															//getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.pdf\"");
															//	getServletResponse().setContentType("application/x-download");
															RenderOptions renderOptions = new RenderOptions();
															renderOptions.setEnableBackground(true);
															/*vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																						docHandle.getFile(), docHandle.getPointer(),
																						OutputFormat.PDF, 1, selectedDoc.getPageCount(),
																						ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
															output.write(baos.toByteArray());
															output.close();*/
															ReprintOptions rp=new ReprintOptions();
															//dbgetnamecorrespondences==G1234561D==2013/11/28==20131128-mpgrmcocol270613-nl-2811201320232565-afp==0000005000000000
															//==dbgetnamecorrespondences==G1234561D==2013/11/28==20131128-mpgrmcocol270613-nl-2811201322553100-afp==0000005000000000
															System.out
															.println("==dbgetname"+db.getName()+"=="+accountNumber+"=="+docHandle.getDate()+"=="+docHandle.getFile()+"=="+docHandle.getPointer());
															reprintdocavailable=true;
															try
															{
																vaultClient.submitForReprint(db.getName(),accountNumber,docHandle.getDate(),docHandle.getFile(),docHandle.getPointer(),1,selectedDoc.getPageCount(),rp);
																output.write("Reprint successfull".getBytes());
															}
															catch(Exception e)
															{
																output.write("Reprint failed".getBytes());	
															}

															//output.close();
															return null;
															//break bb;




														}
													}
													else if(vaultIndex.equalsIgnoreCase("Guid")){
														if(docInfo.get(s).contains(ddao.getGuid())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															selectedDocHandle.setDate(d1.getDate());
															selectedDocHandle.setType(type);
															selectedDocHandle.setFile(d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();
														}
													}
													else if(vaultIndex.equalsIgnoreCase("DocId")){
														if(docInfo.get(s).contains(ddao.getDocId())){
															String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
															selectedDocHandle.setDate(d1.getDate());
															selectedDocHandle.setType(type);
															selectedDocHandle.setFile(d1.getFile());
															selectedDocHandle.setPointer(d1.getPointer());
															selectedDoc = d1;
															accountNumber = d1.getAccountNumber();
														}
													}
												}


												else
												{//ByteArrayOutputStream baos = new ByteArrayOutputStream();

													//	OutputStream output = getServletResponse().getOutputStream();
													System.out
													.println("Else ehere");
													String type = d1.getFormat().equals("collection")?d1.getType():d1.getFormat();
													System.out
													.println("type is"+type);

													System.out
													.println(docInfo.get(s)+"==="+ddao.getDocRefNo());
													System.out
													.println(d1.getFile()+"==="+d1.getPointer());

													if(type.equalsIgnoreCase(".mht")){
														System.out
														.println("Doc format inside collection "+type);
														selectedDocHandle.setDate(d1.getDate());
														System.out
														.println("date is "+d1.getDate());
														System.out
														.println("type is"+type);
														System.out
														.println("file is"+d1.getFile());
														System.out
														.println("pointr is"+d1.getPointer());
														selectedDocHandle.setType(type);
														selectedDocHandle.setFile(d1.getFile());
														selectedDocHandle.setPointer(d1.getPointer());
														selectedDoc = d1;
														accountNumber = d1.getAccountNumber();
														System.out
														.println("accountnumber ");
														docHandle=selectedDocHandle;
														System.out
														.println("selected handle"+selectedDocHandle.toString());
														//	getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.mht\"");
														//getServletResponse().setContentType("application/x-download");
														docHandle = selectedDocHandle;
														RenderOptions renderOptions = new RenderOptions();
														renderOptions.setEnableBackground(true);
														/*vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																				docHandle.getFile(), docHandle.getPointer(),
																				OutputFormat.COLLECT, 1, selectedDoc.getPageCount(),
																				ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);*/

														ReprintOptions rp=new ReprintOptions();
														try
														{
															vaultClient.submitForReprint(db.getName(),accountNumber,docHandle.getDate(),docHandle.getFile(),docHandle.getPointer(),1,selectedDoc.getPageCount(),rp);
															output.write("Reprint successfull".getBytes());
														}
														catch(Exception e)
														{
															output.write("Reprint failed".getBytes());	
														}


														//output.close();
														return null;
														//break bb;
													}

													else if(type.equalsIgnoreCase(".txt")){
														System.out
														.println("Doc format inside collection "+type);
														selectedDocHandle.setDate(d1.getDate());
														System.out
														.println("date is "+d1.getDate());
														System.out
														.println("type is"+type);
														System.out
														.println("file is"+d1.getFile());
														System.out
														.println("pointr is"+d1.getPointer());
														selectedDocHandle.setType(type);
														selectedDocHandle.setFile(d1.getFile());
														selectedDocHandle.setPointer(d1.getPointer());
														selectedDoc = d1;
														accountNumber = d1.getAccountNumber();
														System.out
														.println("accountnumber ");
														docHandle=selectedDocHandle;
														System.out
														.println("selected handle"+selectedDocHandle.toString());
														//	getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.txt\"");
														//	getServletResponse().setContentType("text/plain");
														docHandle = selectedDocHandle;
														RenderOptions renderOptions = new RenderOptions();
														renderOptions.setEnableBackground(true);
														/*vaultClient.renderDirect(db.getName(), accountNumber, docHandle.getDate(),
																					docHandle.getFile(), docHandle.getPointer(),
																					OutputFormat.COLLECT, 1, selectedDoc.getPageCount(),

																					ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);*/
														ReprintOptions rp=new ReprintOptions();
														try
														{
															vaultClient.submitForReprint(db.getName(),accountNumber,docHandle.getDate(),docHandle.getFile(),docHandle.getPointer(),1,selectedDoc.getPageCount(),rp);
															output.write("Reprint successfull".getBytes());
														}
														catch(Exception e)
														{
															output.write("Reprint failed".getBytes());	
														}



														//output.close();
														return null;
														//break bb;
													}

													else if(type.equalsIgnoreCase("afp"))
													{
														/*getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.txt\"");
														getServletResponse().setContentType("text/plain");

														output.write("Document not available".getBytes());
														output.close();
														return null;*/

													}

													//break bb;
												}

											}
											if(i>expectedResult){
												System.out.println("Database has more than " + expectedResult + " statements");
												break a;
											}
											i++;
										}


										if(!reprintdocavailable)
										{

											output.write("Reprint failed".getBytes());	
											return null;
										}
									}
									//	docHandle = selectedDocHandle;
									/*RenderOptions renderOptions = new RenderOptions();
									renderOptions.setEnableBackground(false);
									vaultClient.renderDirect(db.getName(), accountNumber,
															docHandle.getDate(), docHandle.getFile(),
															docHandle.getPointer(), OutputFormat.PDF, 1,
															selectedDoc.getPageCount(),
															ResolutionWidth.NONE, Rotation.NONE,
															renderOptions, baos);

									getServletResponse().setHeader("Content-Disposition","inline; filename=\"download.pdf\"");
									getServletResponse().setContentType("application/pdf");


									OutputStream output = getServletResponse().getOutputStream();

									baos.writeTo(output);

									System.out.println("print message::"+message);
									output.close();*/
								}

								else

								{
									output.write("Reprint failed".getBytes());	
									return null;



								}
							}

							System.out.println("end reprint");
						}
					}
				} 
			} catch (NumberFormatException e) {
				e.printStackTrace();
				System.out.println("Document not found. Unexpected Error Encountered in Document Search/View");	
				foundDocument=false;	
				getServletRequest().getSession().setAttribute("documentfound",foundDocument);
				setMessage("Document not found. Unexpected Error Encountered in Document Search/View");
				accountNumber="s";
			} catch (UnknownHostException e) {
				e.printStackTrace();
				System.out.println("Document not found. Unexpected Error Encountered in Document Search/View");	
				foundDocument=false;	
				getServletRequest().getSession().setAttribute("documentfound",foundDocument);
				setMessage("Document not found. Unexpected Error Encountered in Document Search/View");
				accountNumber="s";
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Document not found. Unexpected Error Encountered in Document Search/View");	
				foundDocument=false;	
				getServletRequest().getSession().setAttribute("documentfound",foundDocument);
				setMessage("Document not found. Unexpected Error Encountered in Document Search/View");
				accountNumber="s";
			} catch (VaultException e) {
				e.printStackTrace();
				System.out.println("Document not found. Unexpected Error Encountered in Document Search/View");	
				foundDocument=false;	
				getServletRequest().getSession().setAttribute("documentfound",foundDocument);
				setMessage("Document not found. Unexpected Error Encountered in Document Search/View");
				accountNumber="s";
			} catch (ServerErrorException e) {
				e.printStackTrace();
				System.out.println("Document not found. Unexpected Error Encountered in Document Search/View");	
				foundDocument=false;	
				getServletRequest().getSession().setAttribute("documentfound",foundDocument);
				setMessage("Document not found. Unexpected Error Encountered in Document Search/View");
				accountNumber="s";
			}
			catch(Exception e){
				e.printStackTrace();
				System.out.println("Document not found. Unexpected Error Encountered in Document Search/View");	
				foundDocument=false;	
				getServletRequest().getSession().setAttribute("documentfound",foundDocument);
				setMessage("Document not found. Unexpected Error Encountered in Document Search/View");
				accountNumber="s";
			}
		}
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for search results/preview(execute) method : " + difference);

		return "SUCCESS";
	}


	/*private byte[] readFile(String path) throws Exception
	{
		try {
			FileReader fr=new FileReader(new File(path));
		int i=-1;
			while((i=fr.read())!=-1)
			{

			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/


	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.servletResponse = response;
	}

}