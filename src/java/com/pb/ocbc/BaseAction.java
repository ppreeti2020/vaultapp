package com.pb.ocbc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport implements SessionAware,
		ServletRequestAware, ServletResponseAware {
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private Map<String, Object> session;
	private Map<String, Object> parameter;
	/* Nithu Alexander: As part of SCT/AVA Fixes*/
	public static final String SESSION_ACTION_ERRORS = "errors";
    public static final String SESSION_ACTION_INFOS = "infos";
    private static final String JSESSIONID = "jsessionid";

	public Map<String, Object> getParameter() {
		return parameter;
	}

	public void setParameter(Map<String, Object> parameter) {
		this.parameter = parameter;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public HttpServletResponse getServletResponse() {
		return this.servletResponse;
	}

	public void setServletResponse(HttpServletResponse paramHttpServletResponse) {
		this.servletResponse = paramHttpServletResponse;
	}

	public HttpServletRequest getServletRequest() {
		return this.servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public Map<String, Object> getSession() {
		return this.session;
	}

	public void loadStrutsSession() {
		this.session = ActionContext.getContext().getSession();
	}

	public void loadStrutsParameter() {
		this.parameter = ActionContext.getContext().getParameters();
	}

	public void loadDefault() {
		loadStrutsParameter();
		loadStrutsSession();
	}
	
	 /*
     * TODO: Refactor #addActionError(String) method. DefaultWorkflowInterceptor
     * uses check ValidationAware.hasErrors() to determine whether it continues
     * to action. However, we redirect writing of all action error messages to
     * session so it lives pass HTTP redirect. With this, the check on workflow
     * interceptor seems to be non-usable anymore.
     */
	@Override
    public void addActionError(String messageKey) {
        //saveActionErrors(getText(messageKey));
		saveActionErrors(messageKey);
    }
	
	public void addActionError(String messageKey, String defaultMessage) {
        saveActionErrors(getText(messageKey, defaultMessage));
    }

    public void addActionError(String messageKey, String defaultMessage,
            String[] args) {
        saveActionErrors(getText(messageKey, defaultMessage, args));
    }

    /**
     * Added this method signature for full compliance to L10n (i.e. no String
     * literals on classes)
     */
    public void addActionError(String messageKey, String[] args) {
        saveActionErrors(getText(messageKey, args));
    }
	
	/** Enables storage of messages in between request. */
    protected void saveActionErrors(String message) {
        List errors = (List) session.get(SESSION_ACTION_ERRORS);
        if (errors == null) {
            errors = new ArrayList();
        }
        errors.add(message);
        session.put(SESSION_ACTION_ERRORS, errors);
    }
    
    /**
     * Added this method signature for full compliance to L10n (i.e. no String
     * literals on classes)
     */
    public void addActionInfoMessage(String messageKey) {
        saveActionInfoMessage(getText(messageKey));
    }

    public void addActionInfoMessage(String messageKey, String defaultMessage) {
        saveActionInfoMessage(getText(messageKey, defaultMessage));
    }

    /** Enables storage of messages in between request. */
    protected void saveActionInfoMessage(String message) {
        List infos = (List) session.get(SESSION_ACTION_INFOS);
        if (infos == null) {
            infos = new ArrayList();
        }
        infos.add(message);
        session.put(SESSION_ACTION_INFOS, infos);
    }

    @Override
    public boolean hasActionErrors() {
        return super.hasActionErrors() || hasSessionActionErrors();
    }

    protected boolean hasSessionActionErrors() {
        List errors = (List) session.get(SESSION_ACTION_ERRORS);
        if (errors == null || errors.isEmpty()) {
            return false;
        }
        return true;
    }

    public void clearActionErrors() {
        session.put(SESSION_ACTION_ERRORS, null);
    }

    
}
