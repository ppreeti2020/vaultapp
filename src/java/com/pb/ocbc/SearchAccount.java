package com.pb.ocbc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pb.admin.Common;
import com.pb.common.CommonConstants;
import com.pb.common.PerformanceLogger;
import com.pb.common.Print;
import com.pb.common.PrintTest;
import com.pb.common.PropertyUtils;
import com.pb.dao.DocMngrDAOOld;
import com.pb.dao.ResultDAO;
import com.pb.dao.SearchDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.UserDTO;
import com.pb.evault.DocumentHandle;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.g1.e2.vault.OutputFormat;
import com.g1.e2.vault.RenderOptions;
import com.g1.e2.vault.ReprintOptions;
import com.g1.e2.vault.ResolutionWidth;
import com.g1.e2.vault.Rotation;
import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.SearchMatchesIterator;
import com.g1.e2.vault.ServerErrorException;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Account;
import com.g1.e2.vault.VaultClient.Database;
import com.g1.e2.vault.VaultClient.Document;
import com.g1.e2.vault.VaultException;
import com.opensymphony.xwork2.ActionContext;

import org.owasp.esapi.codecs.HTMLEntityCodec;


public class SearchAccount extends BaseAction implements ServletRequestAware, ServletResponseAware {

	static final Logger logger = Logger.getLogger(SearchAccount.class);
	SearchDAO dao = new SearchDAO();
	ResultDAO resDao = new ResultDAO();
	PerformanceLogger pl = new PerformanceLogger();
	private String message;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private String searchIndexValues;
	private List<String> csrGroups;
	private List prePopulateList;
	private List<Account> foundAccs;
	private int totalFound;
	private Map<String, Integer> detailProduct;
	private Account selectedAccount;
	private List<Document> listDocument;
	private Set<String> columnNames;
	private List<Account> accList;
	private List actionList;
	private List indexList;
	private String accountNumber;
	private String[] textSearches;
	private String tabName;
	private Map<String, String> textValues;
	private List csrDatabase;
	private Long dbId;
	private String fromDate;
	private String toDate;
	private boolean showDate;
	private String acType;
	private String docHandle;
	private String acc;
	private String pageType;
	//private String textSearch;
	
	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getPageType() {
		return pageType;
	}
	
	public void setAcType(String acType) {
		this.acType = acType;
	}
	public String getAcType() {
		return acType;
	}
	public void setDocHandle(String docHandle) {
		this.docHandle = docHandle;
	}
	public String getDocHandle() {
		return docHandle;
	}
	public void setAcc(String acc) {
		this.acc = acc;
	}
	public String getAcc() {
		return acc;
	}
	public List getPrePopulateList() {
		return prePopulateList;
	}

	public void setPrePopulateList(List prePopulateList) {
		this.prePopulateList = prePopulateList;
	}

	public boolean getShowDate() {
		return showDate;
	}

	public void setShowDate(boolean showDate) {
		this.showDate = showDate;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Long getDbId() {
		return dbId;
	}

	public void setDbId(Long dbId) {
		this.dbId = dbId;
	}

	public List getCsrDatabase() {
		return csrDatabase;
	}

	public void setCsrDatabase(ArrayList csrDatabase) {
		this.csrDatabase = csrDatabase;
	}

	public Map<String, String> getTextValues() {
		return textValues;
	}

	public void setTextValues(HashMap<String, String> textValues) {
		this.textValues = textValues;
	}

	public String getTabName() {
		return tabName;
	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
	}

	public String[] getTextSearches() {
		return textSearches;
	}

	public void setTextSearches(String[] textSearches) {
		this.textSearches = textSearches;
	}

	public List getIndexList() {
		return indexList;
	}

	public void setIndexList(ArrayList indexList) {
		this.indexList = indexList;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public List getActionList() {
		return actionList;
	}

	public void setActionList(ArrayList actList) {
		this.actionList = actList;
	}

	HashMap map = new HashMap();

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public Set<String> getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(Set<String> columnNames) {
		this.columnNames = columnNames;
	}

	public List<Document> getListDocument() {
		return listDocument;
	}

	public void setListDocument(List<Document> listDocument) {
		this.listDocument = listDocument;
	}

	public Account getSelectedAccount() {
		return selectedAccount;
	}

	public void setSelectedAccount(Account selectedAccount) {
		this.selectedAccount = selectedAccount;
	}

	public Map<String, Integer> getDetailProduct() {
		return detailProduct;
	}

	public void setDetailProduct(Map<String, Integer> detailProduct) {
		this.detailProduct = detailProduct;
	}

	private boolean searching;

	public boolean isSearching() {
		return searching;
	}

	public void setSearching(boolean searching) {
		this.searching = searching;
	}

	private String textSearch;

	public String getTextSearch() {
		return textSearch;
	}

	public void setTextSearch(String textSearch) {
		this.textSearch = textSearch;
	}

	public List<Account> getFoundAccs() {
		return foundAccs;
	}

	public void setFoundAccs(List<Account> foundAccs) {
		this.foundAccs = foundAccs;
	}

	public int getTotalFound() {
		return totalFound;
	}

	public void setTotalFound(int totalFound) {
		this.totalFound = totalFound;
	}

	public String execute() {
		UserDAO userDao = new UserDAO();
		Map session = ActionContext.getContext().getSession();
		Long UserId = 0L;
		HttpSession sessionku = getServletRequest().getSession();

		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		if (getSession().get("UserID") != null) {
			UserId = Long.parseLong(getSession().get("UserID").toString());
		}
		csrDatabase = userDao.getDatabase(UserId);
		System.out.println("databases" + csrDatabase);
		System.out.println("getDbId" + getDbId());
		if(csrDatabase.size() == 1){
			showDate = true;
			for(Iterator iter=csrDatabase.iterator();iter.hasNext();)
			{
				UserDTO dto = (UserDTO) iter.next();
				dbId = dto.getDbId();
				indexList = userDao.getIndexUser(UserId, dbId, "search");
			}
						
		}
		else if (getDbId() != null) {
			showDate = true;
			dbId = getDbId();
			indexList = userDao.getIndexUser(UserId, getDbId(), "search");
		}
		tabName = "Search";
		return "SUCCESS";
	}

	public String execute1() {
		UserDAO userDao = new UserDAO();
		Map session = ActionContext.getContext().getSession();
		Long UserId = 0L;
		HttpSession sessionku = getServletRequest().getSession();

		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		if (getSession().get("UserID") != null) {
			UserId = Long.parseLong(getSession().get("UserID").toString());

			System.out.println("UserID:search1" + UserId);
		}

		csrDatabase = userDao.getDatabase(UserId);
		System.out.println("databases" + csrDatabase);
		System.out.println("getDbId" + getDbId());
		if (getDbId() != null) {
			showDate = true;
			dbId = getDbId();
			indexList = userDao.getIndexUser(UserId, getDbId(), "search");
		}
		tabName = "Search";
		return "SUCCESS";
	}

	public String search() throws NumberFormatException, UnknownHostException,
			IOException, VaultException, ServerErrorException {
		loadDefault();
		UserDAO userDao = new UserDAO();
		try {
			execute();
			HTMLEntityCodec codec = new HTMLEntityCodec();
			boolean b = false;
			long logId = 0L;
			Map session = ActionContext.getContext().getSession();
			Enumeration params = this.getServletRequest().getParameterNames();
			String currentGrp = "";
			prePopulateList = new ArrayList();
			foundAccs = new ArrayList<VaultClient.Account>();
			totalFound = 0;
			Long UserId = 0L;
			String searchVal = "";
			List<Account> allAccs = new ArrayList<Account>();
			int j = 0;
			boolean param = false;
			HttpSession sessionku = getServletRequest().getSession();

			if (getSession().get("UserID") != null) {
				UserId = Long.parseLong(getSession().get("UserID").toString());
				System.out.println("UserID:search1" + UserId);
			}
			if (getDbId() != null && getDbId() != 0) {

				sessionku.setAttribute("dbId", getDbId());
				if (params.hasMoreElements()) {
					textValues = new HashMap<String, String>();
					while (params.hasMoreElements()) {
						String paramName = params.nextElement().toString();
						if (paramName.startsWith("textSearchs_")) {
							System.out.println("paramName" + paramName);
							searchVal = getServletRequest().getParameter(paramName);
							String[] tempArr = paramName.split("_");
							String indexName = tempArr[1];
							String dbId = tempArr[2];
							System.out.println("selectedIndex:" + indexName);
							System.out.println("selectedDb:" + dbId);
							logId = getSession().get("logId") != null ? Long.parseLong(getSession().get("logId").toString()) : 0L;
							String csrName = getSession().get("LDAP_GROUP") != null ? getSession().get("LDAP_GROUP").toString() : "";

							if (!searchVal.equalsIgnoreCase("")) {
								param = true;
								textValues.put(paramName, searchVal);
								System.out.println("SearchVal" + searchVal);
								pl.writeTime("The Keyword used for Search is : "+ searchVal);
								searching = getServletRequest().getParameter("search_criteria") != null ? true: false;
								Set<SearchIndex> searchIndexs = null;
								List<Account> acct = new ArrayList<Account>();
								String results = "";
								SearchIndex AcctSI = Common.getSearchIndex(UserId, getDbId(), indexName);
								List res = userDao.getIndexResults(UserId);
								SearchMatchesIterator<Account> SMI = Common.getDBConnection(UserId, getDbId()).searchForAccounts(AcctSI, searchVal);
								if (SMI.hasNext()) {
									int i = 0;
									while (SMI.hasNext()) {
										acct = (ArrayList<Account>) SMI.next();
										for (Account acc : acct) {
											if (!allAccs.contains(acc)) {
												if (res.size() != 0) {
													if (res.contains(acc.getAccountNumber())|| res.contains(acc.getAddress())|| res.contains(acc.getName()))
														allAccs.add(acc);
												} else
													allAccs.add(acc);
												results += acc.getAccountNumber()+ ","+ acc.getName()+ ";";
											}
										}
									}
								}
								logger.info("The account size is"+ allAccs.size());
								System.out.println("The account size is"+ allAccs.size());
								j++;
								System.out.println("fromDate:" + getFromDate()+ "toDate:" + getToDate());
								totalFound += allAccs.size();
								pl.writeTime("Total number of Accounts retrieved : "+ foundAccs.size());
								long SerID = 0;
								long sresId = 0;
								if (logId != 0) {
									map.put("logId", logId);
									map.put("searchVal", searchVal);
									map.put("resCount",	String.valueOf(totalFound));
									SerID = dao.saveSearchDetails(map);
									if (SerID != 0) {
										session.put("SerId", SerID);
										map.put("SerId", SerID);
										map.put("results", results);
										sresId = dao.saveSearchResults(map);
									}
									session.put("sresId", sresId);
								}
							}

						}
					}
					if (param) {
						fromDate = getFromDate();
						toDate = getToDate();
						foundAccs.addAll(Common.getAllAccountByAccNumber(getDbId(), UserId, searchVal, allAccs,fromDate, toDate));
						prePopulateList = userDao.getUsersColumnIndex(UserId,getDbId(), "Y");
					}
				}
				if (!param) {
					String results = "";
					indexList = userDao.getIndexUser(UserId, dbId, "search");
					for (Iterator iter = indexList.iterator(); iter.hasNext();) {
						UserDTO dto = (UserDTO) iter.next();
					//	String serValue = userDao.getUserIndexResults(UserId,dbId, dto.getDbId(), "search");
						String serValue = "";
						Set<SearchIndex> searchIndexs = null;
						List<Account> acct = new ArrayList<Account>();
						SearchIndex AcctSI = Common.getSearchIndex(UserId,getDbId(), dto.getIndexName());
						List res = userDao.getIndexResults(UserId);
						SearchMatchesIterator<Account> SMI = Common.getDBConnection(UserId, getDbId()).searchForAccounts(AcctSI, serValue);
						if (SMI.hasNext()) {
							int i = 0;
							while (SMI.hasNext()) {
								acct = (ArrayList<Account>) SMI.next();
								for (Account acc : acct) {
									if (!allAccs.contains(acc)) {
										if (res.size() != 0) {
											if (res.contains(acc.getAccountNumber())|| res.contains(acc.getAddress())|| res.contains(acc.getName()))
												allAccs.add(acc);
										} else
											allAccs.add(acc);

										results += acc.getAccountNumber() + ","+ acc.getName() + ";";
									}
								}

							}
						}
						pl.writeTime("Total number of Accounts retrieved : "+ foundAccs.size());
						j++;
						System.out.println("fromDate:" + getFromDate()+ "toDate:" + getToDate());

						fromDate = getFromDate();
						toDate = getToDate();
						foundAccs.addAll(Common.getAllAccountByAccNumber(getDbId(), UserId, searchVal, allAccs,fromDate, toDate));
						prePopulateList = userDao.getUsersColumnIndex(UserId,getDbId(), "Y");
						System.out.println("foundAccs:::"+foundAccs.size());
						totalFound += foundAccs.size();
						long SerID = 0;
						long sresId = 0;
						if (logId != 0) {
							map.put("logId", logId);
							map.put("searchVal", searchVal);
							map.put("resCount", String.valueOf(totalFound));
							SerID = dao.saveSearchDetails(map);
							if (SerID != 0) {
								session.put("SerId", SerID);
								map.put("SerId", SerID);
								map.put("results", results);
								sresId = dao.saveSearchResults(map);
							}
							session.put("sresId", sresId);
						}
					}
				}

			} else {
				return "SUCCESS";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
		}

		return "SUCCESS";
	}

	public String search1() throws NumberFormatException, UnknownHostException,
			IOException, VaultException, ServerErrorException {
		loadDefault();
		UserDAO userDao = new UserDAO();
		try {
			execute();
			HTMLEntityCodec codec = new HTMLEntityCodec();
			System.out.println("1-3-4-6");
			boolean b = false;
			long logId = 0L;
			Map session = ActionContext.getContext().getSession();
			Enumeration params = this.getServletRequest().getParameterNames();
			String currentGrp = "";
			System.out.println("getIndexSearches***>" + params);
			prePopulateList = new ArrayList();
			totalFound = 0;
			Long UserId = 0L;
			String searchVal = "";
			List<Account> allAccs;
			int j = 0;
			boolean param = false;
			HttpSession sessionku = getServletRequest().getSession();

			if (getSession().get("UserID") != null) {
				UserId = Long.parseLong(getSession().get("UserID").toString());
				System.out.println("UserID:search1" + UserId);
			}
			if (getDbId() != null && getDbId() != 0) {

				sessionku.setAttribute("dbId", getDbId());
				if (params.hasMoreElements()) {
					textValues = new HashMap<String, String>();
					allAccs = new ArrayList<Account>();
					foundAccs = new ArrayList<VaultClient.Account>();
					while (params.hasMoreElements()) {
						String paramName = params.nextElement().toString();
						if (paramName.startsWith("textSearchs_")) {
							System.out.println("paramName" + paramName);
							System.out.println(getServletRequest()
									.getAttribute(paramName)
									+ "search...."
									+ getServletRequest().getParameter(
											paramName));
							searchVal = getServletRequest().getParameter(
									paramName);
							System.out.println(getServletRequest()
									.getAttribute(paramName)
									+ "search...."
									+ getServletRequest().getParameter(
											paramName));

							String[] tempArr = paramName.split("_");

							String indexName = tempArr[1];
							String dbId = tempArr[2];
							System.out.println("selectedUser:" + indexName);
							System.out.println("selectedIndex:" + dbId);
							logId = getSession().get("logId") != null ? Long
									.parseLong(getSession().get("logId")
											.toString()) : 0L;
							String csrName = getSession().get("LDAP_GROUP") != null ? getSession()
									.get("LDAP_GROUP").toString() : "";

							if (!searchVal.equalsIgnoreCase("")) {
								param = true;
								textValues.put(paramName, searchVal);
								System.out.println("1a" + searchVal);
								pl.writeTime("The Keyword used for Search is : "
										+ searchVal);
								searching = getServletRequest().getParameter(
										"search_criteria") != null ? true
										: false;
								System.out.println("The value of searching is"
										+ searching);
								System.out.println("2");

								Set<SearchIndex> searchIndexs = null;
								System.out.println("3");
								List<Account> acct = new ArrayList<Account>();
								String results = "";
								SearchIndex AcctSI = Common.getSearchIndex(
										UserId, getDbId(), indexName);
								List res = userDao.getIndexResults(UserId);
								System.out.println("4::"+AcctSI);
								SearchMatchesIterator<Account> SMI = Common
										.getDBConnection(UserId, getDbId())
										.searchForAccounts(AcctSI, searchVal);
								if (SMI.hasNext()) {
									int i = 0;
									while (SMI.hasNext()) {
										acct = (ArrayList<Account>) SMI.next();
										for (Account acc : acct) {
											if (!allAccs.contains(acc)) {
												if (res.size() != 0) {
													if (res.contains(acc.getAccountNumber())|| res.contains(acc.getAddress())|| res.contains(acc.getName()))
														allAccs.add(acc);
												} else
													allAccs.add(acc);
												results += acc.getAccountNumber()+ ","+ acc.getName()+ ";";
											}
										}

									}
								}
								logger.info("The account size is"+ allAccs.size());
								System.out.println("The account size is"+ allAccs.size());
								j++;
								System.out.println("fromDate:" + getFromDate()+ "toDate:" + getToDate());
								totalFound += allAccs.size();
								System.out.println("5");
								pl.writeTime("Total number of Accounts retrieved : "+ foundAccs.size());
								long SerID = 0;
								long sresId = 0;
								if (logId != 0) {
									map.put("logId", logId);
									map.put("searchVal", searchVal);
									map.put("resCount",String.valueOf(totalFound));
									SerID = dao.saveSearchDetails(map);
									if (SerID != 0) {
										session.put("SerId", SerID);
										map.put("SerId", SerID);
										map.put("results", results);
										sresId = dao.saveSearchResults(map);
									}
									session.put("sresId", sresId);
								}
							}

						}

					}
					fromDate = getFromDate();
					toDate = getToDate();
					foundAccs.addAll(Common.getAllAccountByAccNumber(getDbId(), UserId, searchVal, allAccs, fromDate, toDate));

				}
				if (!param) {
					allAccs = new ArrayList<Account>();
					foundAccs = new ArrayList<VaultClient.Account>();
					System.out.println("coming here:else:list");
					String results = "";
					indexList = userDao.getIndexUser(UserId, dbId, "search");
					for (Iterator iter = indexList.iterator(); iter.hasNext();) {
						UserDTO dto = (UserDTO) iter.next();
				//		String serValue = userDao.getUserIndexResults(UserId,
				//				dbId, dto.getDbId(), "search");
						String serValue = "";
						Set<SearchIndex> searchIndexs = null;
						List<Account> acct = new ArrayList<Account>();
						SearchIndex AcctSI = Common.getSearchIndex(UserId, getDbId(), dto.getIndexName());
						System.out.println("AcctSI:::"+AcctSI);
						List res = userDao.getIndexResults(UserId);
						SearchMatchesIterator<Account> SMI = Common.getDBConnection(UserId, getDbId()).searchForAccounts(AcctSI, serValue);
						if (SMI.hasNext()) {
							int i = 0;
							while (SMI.hasNext()) {
								acct = (ArrayList<Account>) SMI.next();
								for (Account acc : acct) {
									System.out.println("acc:>>>>"+acc);
									
									if (results.lastIndexOf(acc.getAccountNumber())<0) {
										System.out.println("acc inside if loop:>>>>"+acc);
										if (res.size() != 0) {
											if (res.contains(acc.getAccountNumber())|| res.contains(acc.getAddress())|| res.contains(acc.getName()))
												allAccs.add(acc);
										} else
											allAccs.add(acc);
										results += acc.getAccountNumber() + ","+ acc.getName() + ";";
									}
								}

							}
						}
					}
					pl.writeTime("Total number of Accounts retrieved, allAccs.size : "+ allAccs.size());
					System.out.println("allAccs:>>>>"+allAccs.get(1));
					j++;
					System.out.println("fromDate:" + getFromDate()+ "toDate:" + getToDate());
					fromDate = getFromDate();
					toDate = getToDate();
					foundAccs.addAll(Common.getAllAccountByAccNumber(getDbId(), UserId, searchVal, allAccs,	fromDate, toDate));
					totalFound += foundAccs.size();
					System.out.println("foundAccs size::"+foundAccs.size()+" totalFound::"+totalFound);
					long SerID = 0;
					long sresId = 0;
					if (logId != 0) {
						map.put("logId", logId);
						map.put("searchVal", searchVal);
						map.put("resCount", String.valueOf(totalFound));
						SerID = dao.saveSearchDetails(map);
						if (SerID != 0) {
							session.put("SerId", SerID);
							map.put("SerId", SerID);
							map.put("results", results);
							sresId = dao.saveSearchResults(map);
						}
						session.put("sresId", sresId);
					}
				}

			} else {
				return "SUCCESS";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
		}

		return "SUCCESS";
	}

	public String detail() throws NumberFormatException, UnknownHostException,
			IOException, VaultException, ServerErrorException {
		loadDefault();
		String accountNumber = "";
		try {
			HttpSession sessionku = getServletRequest().getSession();
			UserDAO dao = new UserDAO();
			accountNumber = getServletRequest().getParameter("acc");
			detailProduct = new HashMap<String, Integer>();
			dbId = 0L;

			if (sessionku.getAttribute("dbId") != null) {
				dbId = Long
						.parseLong(sessionku.getAttribute("dbId").toString());
			}
			if (dbId != 0) {
				// System.out.println("The value of dbId is"+getDbId());
				System.out.println("The value of dbId is-----"
						+ sessionku.getAttribute("dbId"));
				String csrName = getSession().get("LDAP_GROUP").toString();
				long serId = getSession().get("SerId") != null ? Long
						.parseLong(getSession().get("SerId").toString()) : 0L;
				long LogId = getSession().get("logId") != null ? Long
						.parseLong(getSession().get("logId").toString()) : 0L;
				long sresId = getSession().get("sresId") != null ? Long
						.parseLong(getSession().get("sresId").toString()) : 0L;
				System.out.println("The value of accountNumber is"
						+ accountNumber);
				System.out.println("The value of csrName is" + csrName);

				pl.writeTime("Account number selected to View details : "
						+ accountNumber);
				pl.writeTime("csrName is : " + csrName);
				Long UserId = 0L;
				if (getSession().get("UserID") != null)
					UserId = Long.parseLong(getSession().get("UserID")
							.toString());
				System.out.println("UserID:details" + UserId);
				if (UserId != 0) {
					detailProduct = Common.getDetailProduct(accountNumber,
							UserId, dbId);
					System.out.println(UserId + "The value of detailProduct is"
							+ detailProduct);
					selectedAccount = Common.getAccountByNumber(accountNumber,
							UserId, dbId);
					System.out.println(detailProduct.get(0)
							+ "The value of selectedAccount is"
							+ selectedAccount.toString()
							+ detailProduct.keySet().size());
					Map session = ActionContext.getContext().getSession();
					for (String key : detailProduct.keySet()) {
						System.out.println(key);
					}

					actionList = dao.getUserActions(UserId);
					if (serId != 0 && LogId != 0) {
						map.put("DBName", detailProduct.keySet().iterator()
								.next());
						map.put("accNum", accountNumber);
						map.put("Name", selectedAccount.getName());
						map.put("count", detailProduct.size());
						map.put("serId", serId);
						map.put("logId", LogId);
						map.put("sresId", sresId);
						long accId = resDao.saveAccountDetails(map);
						session.put("accId", accId);
					}
				}
			} else {
				addActionError("Sorry for Inconvinience");
				setMessage("Server Down");
			}

		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Sorry for Inconvinience");
			return "FAILURE";
		}
		return "SUCCESS";
	}

	public String list() throws NumberFormatException, UnknownHostException,
			IOException, VaultException, ServerErrorException {
		loadDefault();
		Long UserId = 0L;
		DocMngrDAOOld docmngrdao = new DocMngrDAOOld();
		UserDAO userDao = new UserDAO();
		String productName = "";
		HttpSession sessionku = getServletRequest().getSession();

		try {
			if (sessionku.getAttribute("dbId") != null) {
				dbId = Long
						.parseLong(sessionku.getAttribute("dbId").toString());
			}
			if (dbId != 0) {

				accountNumber = getServletRequest().getParameter("acc");
				System.out.println("accountNumber" + accountNumber);

				if (getSession().get("UserID") != null)
					UserId = Long.parseLong(getSession().get("UserID")
							.toString());

				String dbName = userDao.getDBName(UserId, dbId);
				String dbDesc = userDao.getDBDesc(UserId, dbId);
				if (dbName != null) {
					getSession().put("SELECT_DB", dbName);
					sessionku.putValue("SELECT_DB", dbName);
					getSession().put("SELECT_DB_DESC", dbDesc);
					sessionku.putValue("SELECT_DB_DESC", dbDesc);
				}
				if (getServletRequest().getParameter("p") != null) {
					productName = getServletRequest().getParameter("p");
					getSession().put("SELECT_DB", productName);
					sessionku.putValue("SELECT_DB", productName);

				} else

					productName = getSession().get("SELECT_DB").toString();
				System.out
						.println("The value of accountNumber in list method is"
								+ accountNumber);
				System.out.println("The value of productName in list method is"
						+ productName);
				long accId = getSession().get("accId") != null ? Long
						.parseLong(getSession().get("accId").toString()) : 0L;
				long LogId = getSession().get("logId") != null ? Long
						.parseLong(getSession().get("logId").toString()) : 0L;
				pl.writeTime("productNameName is : " + productName);
				Map session = ActionContext.getContext().getSession();

				listDocument = new ArrayList<VaultClient.Document>();
				
				System.out.println("UserID:details" + UserId);
				listDocument = Common.getAllDocumentByAccNumberAndProductName(
						accountNumber, productName, UserId, dbId);
				pl.writeTime("Total number of Documents listed : "
						+ listDocument.size());
				Document doc = listDocument.get(0);
				System.out.println("The document name is" + doc.getFile());
				System.out.println("The action name is"
						+ getServletRequest().getParameter("op"));
				columnNames = doc.getCustomFields().keySet();
				actionList = userDao.getUserActions(UserId);
				System.out.println("docformat" + doc.getFormat());

				if (accId != 0 && LogId != 0) {
					map.put("docName", doc.getName());
					map.put("Desc", doc.getDocumentInfo());
					map.put("docType", doc.getType());
					map.put("docDate", doc.getDate());
					map.put("accId", accId);
					map.put("logId", LogId);
					sessionku.setAttribute("logId", LogId);
					long docId = resDao.saveDocumentDetails(map);
					session.put("docId", docId);
					sessionku.setAttribute("docId", docId);
					String rootName = docmngrdao.getRoot(Long.parseLong(getServletRequest().getParameter("docId")));
					getSession().put("SELECT_ROOT", rootName);
					System.out.println("Root Name :::"+rootName);
				}
				if (getServletRequest().getParameter("op") != null) {
					String action = getServletRequest().getParameter("op");
					if (action.equalsIgnoreCase("print")) {
						final String reqdoc = getServletRequest().getParameter(
								"doc");

						DocumentHandle docHandle = new DocumentHandle(reqdoc);
						Database db = Common.getDBConnection(productName);
						Account acct = db.getAccount(accountNumber);

						// XXXI: Note that this is potentially problematic.
						// If there are hundreds of documents under an account,
						// it will take time to build up that list, not to
						// mention
						// the potential for the JVM to exhaust its heap space.
						sessionku = getServletRequest().getSession();
						if (action != null) {
							action = "print";
							updateActionDetails(action, sessionku);
						}
						List<Document> documents = acct.getAllDocuments();

						Document selectedDoc = null;
						DocumentHandle selectedDocHandle = null;

						List<DocumentHandle> docHandles = new ArrayList<DocumentHandle>(
								documents.size());

						for (Document d : documents) {
							DocumentHandle dh = new DocumentHandle(d);
							docHandles.add(dh);

							if ((docHandle != null) && docHandle.equals(dh)) {
								selectedDoc = d;
								selectedDocHandle = dh;
							}
						}
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						Properties properties = PropertyUtils
								.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);

						RenderOptions renderOptions = new RenderOptions();
						renderOptions.setEnableBackground(false);
						VaultClient vaultClient = new VaultClient();
						vaultClient
								.connect(
										properties
												.getProperty(CommonConstants.EVAULT_HOST),
										Integer.valueOf(
												properties
														.getProperty(CommonConstants.EVAULT_PORT))
												.intValue());
						vaultClient.renderDirect(productName, accountNumber,
								docHandle.getDate(), docHandle.getFile(),
								docHandle.getPointer(), OutputFormat.PDF, 1,
								selectedDoc.getPageCount(),
								ResolutionWidth.NONE, Rotation.NONE,
								renderOptions, baos);
						String message = PrintTest.print(baos);
						setMessage(message);
						System.out.println("printed");

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}
		return "SUCCESS";
		/*
		 * if(!Common.isDataAccessible(accountNumber,csrName)) return
		 * "CANNOTACCESS"; else {
		 * 
		 * }
		 */
	}

	public String updateActionDetails(String action, HttpSession session) {
		long LogId = 0;
		long docId = 0;
		if (session.getAttribute("logId") != null
				&& session.getAttribute("docId") != null) {
			System.out.println(LogId + "session:" + docId);
			LogId = Long.parseLong(session.getAttribute("logId").toString());
			docId = Long.parseLong(session.getAttribute("docId").toString());
		}

		if (action != null) {
			if (action.equals("view")) {
				map.put("action", "view");
			} else if (action.equals("download")) {
				map.put("action", "download");

			} else if (action.equals("print")) {
				map.put("action", "print");

			}
			if (LogId != 0 && docId != 0) {
				map.put("logId", LogId);
				map.put("docId", docId);
				resDao.saveDocumentActionDetails(map);
			}
		}

		return null;
	}
public String ViewDoc() {
		String UserId = "";
	    Map parameters = ActionContext.getContext().getParameters();
	    HttpSession sessionku1 = getServletRequest().getSession();
	    String acType = parameters.get("acType").toString();
	    String docHandle = parameters.get("docHandle").toString();
	    String acc = parameters.get("acc").toString();
	    if (getSession().get("UserID") != null)
		{
			UserId = getSession().get("UserID").toString();
		}
	    pageType = "search1"+UserId;
	    getSession().put("pageType", pageType);
		if (acType == null) {
			return "FAILURE";
		}
		if (acType != null) {
			
			getSession().put("acType", acType);
			getSession().put("docHandle", docHandle);
			getSession().put("acc", acc);
			System.out.println("acType::::" + this.acType);
		}
		return "SUCCESS";
	}

	public String saveDocRenderDetails(HashMap map, HttpSession session) {

		if (session.getAttribute("logId") != null
				&& session.getAttribute("docId") != null) {
			long LogId = Long.parseLong(session.getAttribute("logId")
					.toString());
			long docId = Long.parseLong(session.getAttribute("docId")
					.toString());

			map.put("logId", LogId);
			map.put("docId", docId);
			resDao.saveDocumentRenderDetails(map);
		}

		return null;
	}

	public String download() throws VaultException, ServerErrorException,
			NumberFormatException, UnknownHostException, IOException {
		loadDefault();
		String dbName = "";
		try {
			dbName = getSession().get("SELECT_DB").toString();
			final String doc = getServletRequest().getParameter("doc");
			final String acctNumber = getServletRequest().getParameter("acct");
			String action = getServletRequest().getParameter("op");
			DocumentHandle docHandle = new DocumentHandle(doc);
			Database db = Common.getDBConnection(dbName);
			Account acct = db.getAccount(acctNumber);
			// XXXI: Note that this is potentially problematic.
			// If there are hundreds of documents under an account,
			// it will take time to build up that list, not to mention
			// the potential for the JVM to exhaust its heap space.
			HttpSession sessionku = getServletRequest().getSession();
			if (action != null) {
				action = "download";
				updateActionDetails(action, sessionku);
			}
			List<Document> documents = acct.getAllDocuments();
			System.out.println("Documents>>>>>>>>");
			Document selectedDoc = null;
			DocumentHandle selectedDocHandle = null;

			List<DocumentHandle> docHandles = new ArrayList<DocumentHandle>(
					documents.size());

			for (Document d : documents) {
				DocumentHandle dh = new DocumentHandle(d);
				docHandles.add(dh);

				if ((docHandle != null) && docHandle.equals(dh)) {
					selectedDoc = d;
					selectedDocHandle = dh;
				}
			}
			System.out.println("Selected doc handle>>>>>>>>");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Properties properties = PropertyUtils
					.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);

			RenderOptions renderOptions = new RenderOptions();
			renderOptions.setEnableBackground(false);
		/*	VaultClient vaultClient = new VaultClient();
			vaultClient
					.connect(
							properties.getProperty(CommonConstants.EVAULT_HOST),
							Integer.valueOf(
									properties
											.getProperty(CommonConstants.EVAULT_PORT))
									.intValue());*/
			VaultClient vaultClient = Common.getVaultConnection();
			System.out.println("vault connected>>>>>>>>");
			vaultClient.renderDirect(dbName, acctNumber, docHandle.getDate(),
					docHandle.getFile(), docHandle.getPointer(),
					OutputFormat.PDF, 1, selectedDoc.getPageCount(),
					ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
			System.out.println("After render redirect>>>>>>>>");
			getServletResponse().setHeader("Content-Disposition",
					"attachment; filename=\"download.PDF\"");
			getServletResponse().setContentType("application/x-download");
			OutputStream output = getServletResponse().getOutputStream();
			output = getServletResponse().getOutputStream();
			output.write(baos.toByteArray());
			output.close();
			return "SUCCESS";

		} catch (Exception e) {
			return "FAILURE";
		}
	}

	public String printdoc() throws VaultException, ServerErrorException,
			NumberFormatException, UnknownHostException, IOException {
		loadDefault();
		String dbName = "";
		Map parameters = ActionContext.getContext().getParameters();
	    HttpSession sessionku1 = getServletRequest().getSession();
	    String acType = parameters.get("acType").toString();
	    if (acType == null) {
			return "FAILURE";
		}
		if (acType != null) {
			getSession().put("acType", acType);
		}
		try {
			dbName = getSession().get("SELECT_DB").toString();
			final String doc = getServletRequest().getParameter("doc");
			final String acctNumber = getServletRequest().getParameter("acct");
			String action = getServletRequest().getParameter("op");
			DocumentHandle docHandle = new DocumentHandle(doc);
			Database db = Common.getDBConnection(dbName);
			Account acct = db.getAccount(acctNumber);
			
			// XXXI: Note that this is potentially problematic.
			// If there are hundreds of documents under an account,
			// it will take time to build up that list, not to mention
			// the potential for the JVM to exhaust its heap space.
			HttpSession sessionku = getServletRequest().getSession();
			if (action != null) {
				action = "print";
				updateActionDetails(action, sessionku);
			}
			List<Document> documents = acct.getAllDocuments();

			Document selectedDoc = null;
			DocumentHandle selectedDocHandle = null;

			List<DocumentHandle> docHandles = new ArrayList<DocumentHandle>(
					documents.size());

			for (Document d : documents) {
				DocumentHandle dh = new DocumentHandle(d);
				docHandles.add(dh);

				if ((docHandle != null) && docHandle.equals(dh)) {
					selectedDoc = d;
					selectedDocHandle = dh;
				}
			}
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Properties properties = PropertyUtils
					.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);

			RenderOptions renderOptions = new RenderOptions();
			renderOptions.setEnableBackground(false);
					
			VaultClient vaultClient = Common.getVaultConnection();
			vaultClient.renderDirect(dbName, acctNumber, docHandle.getDate(),
					docHandle.getFile(), docHandle.getPointer(),
					OutputFormat.PDF, 1, selectedDoc.getPageCount(),
					ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);
			
			System.out.println("vault connected>>>>>>>>");
			PrintTest.print(baos);
			setMessage("Document has been printed!!");
			System.out.println("printed");
			return "SUCCESS";

		} catch (Exception e) {
			System.out.println("inside catch");
			return "FAILURE";
		}
	}
	
	public String reprint() throws VaultException, ServerErrorException,NumberFormatException, UnknownHostException, IOException {
		loadDefault();
		String dbName = "";
		try {
			dbName = getSession().get("SELECT_DB").toString();
			final String doc = getServletRequest().getParameter("doc");
			final String acctNumber = getServletRequest().getParameter("acct");
			String action = getServletRequest().getParameter("op");
			DocumentHandle docHandle = new DocumentHandle(doc);
			Database db = Common.getDBConnection(dbName);
			Account acct = db.getAccount(acctNumber);
			HttpSession sessionku = getServletRequest().getSession();
			if (action != null) {
				action = "download";
				updateActionDetails(action, sessionku);
			}
			List<Document> documents = acct.getAllDocuments();
			System.out.println("Documents>>>>>>>>");
			Document selectedDoc = null;
			DocumentHandle selectedDocHandle = null;

			List<DocumentHandle> docHandles = new ArrayList<DocumentHandle>(
					documents.size());

			for (Document d : documents) {
				DocumentHandle dh = new DocumentHandle(d);
				docHandles.add(dh);

				if ((docHandle != null) && docHandle.equals(dh)) {
					selectedDoc = d;
					selectedDocHandle = dh;
				}
			}
			System.out.println("Selected doc handle>>>>>>>>");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Properties properties = PropertyUtils
			.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);

		/*	RenderOptions renderOptions = new RenderOptions();
			renderOptions.setEnableBackground(false); */
			
			ReprintOptions reprintOptions = new ReprintOptions();
			
			VaultClient vaultClient = Common.getVaultConnection();
			System.out.println("vault connected>>>>>>>>");
		/*	vaultClient.renderDirect(dbName, acctNumber, docHandle.getDate(),
					docHandle.getFile(), docHandle.getPointer(),
					OutputFormat.PDF, 1, selectedDoc.getPageCount(),
					ResolutionWidth.NONE, Rotation.NONE, renderOptions, baos);*/
			System.out.println("values to submit for reprint:::"
					+dbName+"::"+acctNumber+"::"+ docHandle.getDate()+"::"+docHandle.getFile()
					+"::"+docHandle.getPointer()+"::"+selectedDoc.getPageCount()+"::"+reprintOptions);
			
			vaultClient.submitForReprint(dbName, acctNumber, docHandle.getDate(),
					docHandle.getFile(), docHandle.getPointer(), 1, selectedDoc.getPageCount(), reprintOptions);
			System.out.println("After submitForReprint>>>>>>>>");
			
			/*getServletResponse().setHeader("Content-Disposition",
			"inline; filename=\"download.PDF\"");
			getServletResponse().setContentType("application/x-download");
			OutputStream output = getServletResponse().getOutputStream();
			output = getServletResponse().getOutputStream();
			output.write(baos.toByteArray());
			output.close();*/
			return "SUCCESS";

		} catch (Exception e) {
			return "FAILURE";
		}
	}
}
