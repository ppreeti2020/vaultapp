package com.pb.LogParse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.log4j.Logger;


import com.pb.dao.DataIntegrityDAO;
import com.pb.dto.DataDTO;
import com.pb.manager.DataIntegrityManager;


/**
 * Parse an Apache log file with StringTokenizer
 */
public class LogStrTok implements LogExample {
	
	static final Logger logger = Logger.getLogger(LogStrTok.class);
	// keywords for search
	
	private DataIntegrityDAO dataManager = new DataIntegrityDAO();

	/*public void setDataManager(DataIntegrityManager dataManager) {
		this.dataManager = dataManager;
	}
	public DataIntegrityManager getDataManager() {
		return dataManager;
	}*/
	private static final String DATE = "date";
	//private static final String STARTED = "started";
	private static final String COMPRESSING = "compressing";
	private static final String HAVE_JOURNAL = "have journal";
	private static final String PROFILE = "profile";
	private static final String FORMAT = "format";
	private static final String PAGES = "pages";
	private static final String TOTAL_READ = "total read";
	private static final String TOTAL_WRITTEN = "total written";
	private static final String FINAL_RATIO = "final ratio";
	private static final String METHOD = "method";
	private static final String DOCUMENTS = "documents";
	private static final String DOCUMENT_PAGES = "document pages";
	private static final String IGNORED_PAGES = "ignored pages";
	private static final String ERROR = "ERROR";
	private static final String WROTE = "wrote";
	private static final String SIZE = "size";
	private static final String START_INDEXING_FILE = "start indexing document file";
	private static final String FLUSHING_INDEX_CACHE = "flushing index cache";
	private static final String FINISHING_INDEXING_FILE = "finished indexing document file";
	private static final String HAS_BEEN_COMPRESSED_TO = "has been compressed to";
	private static final String FILE_SIZE = "file size";
	private static final String DISK_READ = "disk read";
	private static final String DISK_WRITE = "disk write";
	private static final String STACK_DEPTH = "stack depth";
	private static final String CACHE_READ_HIT = "cache read hit";
	private static final String CACHE_READ_MISS = "cache read miss";
	private static final String CACHE_WRITE_HIT = "cache write hit";
	private static final String CACHE_WRITE_MISS = "cache write miss";
	private static final String ADD_DUPLICATE = "add duplicate";
	private static final String ADD_UNIQUE = "add unique";
	
	// private static final String INDEX_PATH = "INDEX\\";

	public static boolean contains(String full, String searched) {
		// System.out.println("searched" +searched);
		if (full.indexOf(searched) != -1) {
			return true;
		} else {
			return false;
		}

	}

	public static int StringAccurance(String str, String findMe) {
		int len = findMe.length();
		int result = 0;
		if (len > 0) {
			int start = str.indexOf(findMe);
			while (start != -1) {
				result++;
				start = str.indexOf(findMe, start + len);
			}
		}
		return result;
	}

	public static String returnnoBrackets(String str) {
		String extracted = "";
		if (contains(str, "]") && contains(str, "["))
			extracted = str.substring(str.indexOf('[') + 1, str.indexOf(']'));
		else if (contains(str, "[") && !contains(str, "]"))
			extracted = str.substring(str.indexOf('[') + 1);
		else if (!contains(str, "[") && contains(str, "]"))
			extracted = str.substring(str.indexOf(']'));
		return extracted;
	}

	public void ReadLogData(Long e2logFileId, String str) throws Exception {

		
		LogStrTok log = new LogStrTok();
		// String str = readFile();
		String startingTime = "";
		String date = "";
		String jrnlFile = "";
		String compressingFile = "";
		String profile = "";
		String format = "";
		String pages = "";
		String totalRead = "";
		String totalWritten = "";
		String finalRatio = "";
		String methodName = "";
		String documents = "";
		String documentPages = "";
		String ignorePages = "";
		String indexingFile = "";
		int compCount = 0;
		///int journalCount = 0;
		String status = "";
		String compressingType = "";
		Long logFileId = 0L;
		int totalErrors = 0;
		/*
		 * reading first line of log file to get date and time
		 */
		try
		{
			if(e2logFileId != 0)
			{
		if (str.contains(DATE)) {
			System.out.println("date: "
					+ returnnoBrackets(str.split(DATE)[1].split(",")[0]));
			date = returnnoBrackets(str.split(DATE)[1].split(",")[0]);
			startingTime = str.split("open log")[0].split(" ")[0];
			System.out.println("date: " + date + " & time:" + startingTime);
		}
		//System.out.println("ReadLogData::1");
		/*
		 * This condition for to get journal file details
		 */
		DataDTO dao = new DataDTO();
					dao.setJournalFile(jrnlFile);
			//System.out.println("ReadLogData::2");
			if (str.contains(COMPRESSING)) {
				compCount = StringAccurance(str, COMPRESSING);
				System.out.println("ReadLogData::count of compressing::"+compCount);
				compressingType = "Indexing";
				for (int i = 1; i <= compCount; i++) {
					compressingFile = returnnoBrackets(str.split(COMPRESSING)[i]
							.split(PROFILE)[0]);
					dao.setCompressingFile(compressingFile);
					System.out.println("compressing file:" + compressingFile);
					String comp = str.split(COMPRESSING)[i];
					dao.setLogDate(date);
					dao.setLogTime(startingTime);
					//System.out.println("ReadLogData::3");
					if (comp.contains(HAVE_JOURNAL)){
						jrnlFile = returnnoBrackets(comp.split(HAVE_JOURNAL)[1]
						                            						.split(",")[0]);
						dao.setJournalFile(jrnlFile);
					}
					//System.out.println("ReadLogData::4");
					if (comp.contains(PROFILE)) {
						profile = returnnoBrackets(comp.split(PROFILE)[1]
								.split(FORMAT)[0]);
						System.out.println("profile:" + profile);
						dao.setProfile(profile.toUpperCase());
					}
					if (comp.contains(FORMAT)) {
						format = returnnoBrackets(comp.split(PROFILE)[1]
								.split(FORMAT)[1].split("\\n")[0]);
						System.out.println("format:" + format);
						dao.setFormat(format);
					}
					if (comp.contains(START_INDEXING_FILE)) {
						// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
						indexingFile = returnnoBrackets(comp
								.split(START_INDEXING_FILE)[1].split("]")[0]);
						System.out.println("indexingFile:" + indexingFile);
						dao.setDrdFile(indexingFile);
					}
					if (comp.contains(HAS_BEEN_COMPRESSED_TO)) {
						String drpFile = returnnoBrackets(comp
								.split(HAS_BEEN_COMPRESSED_TO)[1].split("]")[0]);
						System.out.println("drpfile:" + drpFile);
						dao.setDrpFile(drpFile);
					}
					else
					{
						String drFile = "";
						if(contains(indexingFile,"DOCDATA") &&  contains(indexingFile,".drd"))
						{
							drFile = indexingFile.split("DOCDATA")[1].split(".drd")[0];
						}
						else
							if(contains(indexingFile,"docdata") &&  contains(indexingFile,".drd"))
							{
								drFile = indexingFile.split("docdata")[1].split(".drd")[0];
							}
						dao.setDrpFile("pagedata"+drFile+".drp");
					}
					if (comp.contains(PAGES)) {
						pages = returnnoBrackets(comp.split(PROFILE)[1]
								.split(" t")[0]);
						System.out.println("pages:" + pages);
						dao.setPages(pages);
					}
					if (comp.contains(TOTAL_READ)) {
						totalRead = returnnoBrackets(comp.split(TOTAL_READ)[1]
								.split(" t")[0]);
						System.out.println("totalRead:" + totalRead);
						dao.setTotalRead(totalRead);
					}
					if (comp.contains(TOTAL_WRITTEN)) {
						totalWritten = returnnoBrackets(comp
								.split(TOTAL_WRITTEN)[1].split(" f")[0]);
						System.out.println("totalWritten:" + totalWritten);
						dao.setTotalWrite(totalWritten);
					}
					if (comp.contains(FINAL_RATIO)) {
						finalRatio = returnnoBrackets(comp.split(FINAL_RATIO)[1]
								.split("\n")[0]);
						System.out.println("finalRatio:" + finalRatio);
						dao.setFinalRatio(finalRatio);
					}
					if (comp.contains(METHOD)) {
						// System.out.println("method"+str.split("compressing")[i].split("method")[1].split("]")[0]);
						methodName = returnnoBrackets(comp.split(METHOD)[1]
								.split("]")[0]);
						System.out.println("method:" + methodName);
						dao.setMethod(methodName);
						if (comp.contains(DOCUMENTS)) {
							documents = returnnoBrackets(comp.split(METHOD)[1]
									.split("]")[1].split(DOCUMENTS)[0]);
							System.out.println("documents:" + documents);
							dao.setDocuments(documents);
						}
					} else if (comp.contains(DOCUMENTS)) {

						documents = returnnoBrackets(comp.split(PROFILE)[1]
								.split("]")[1].split(DOCUMENTS)[0]);
						System.out.println("documents:" + documents);
						dao.setDocuments(documents);
					}
					if (comp.contains(DOCUMENT_PAGES)) {
						// System.out.println("documents :"+str.split("compressing")[i].split("method")[1].split("]")[1].split("\\[")[1]);
						documentPages = returnnoBrackets(comp
								.split(DOCUMENT_PAGES)[0].split(", ")[1]);
						System.out.println("document pages:" + documentPages);
						dao.setDocumentPages(documentPages);
					}

					if (comp.contains(IGNORED_PAGES)) {
						ignorePages = returnnoBrackets(comp
								.split(DOCUMENT_PAGES)[1].split(", ")[1]);
						System.out.println("ignored pages:" + ignorePages);
						dao.setIgnoredPages(ignorePages);
					}
					if(comp.contains(FINISHING_INDEXING_FILE))
					{
						System.out.println("*********"+FINISHING_INDEXING_FILE);
					boolean errorOcc = false;
					if(comp.contains("errors"))
					{
				//System.out.println("errors:"+str.split("finished indexing document file")[1].split(",")[1].split("errors")[0]);
				totalErrors = Integer.parseInt(returnnoBrackets(comp.split("finished indexing document file")[1].split(",")[1].split("errors")[0]));
				System.out.println("totalErrors:"+totalErrors);
					}
					if(totalErrors != 0)
					{
					if (comp.contains(ERROR)) {
						errorOcc = true;
					}
					}
					
					status = "Not Finished";
					dao.setFileStatus(status);
					dao.setCompressingType(compressingType);
					String errorCodes[] = SaveErrors(comp, dao, logFileId, false);
					System.out.println("errorCodes:1:" + errorCodes);
					logFileId = this.dataManager.getLogFileData(compressingFile,e2logFileId,
							compressingType,errorCodes);
					System.out.println("logFileId" + logFileId);
					if (logFileId != 0)
						this.dataManager.updateLogFileData(dao, logFileId,
								compressingType, errorOcc);
					else

						logFileId = this.dataManager.saveLogFileData(dao, e2logFileId,
								errorOcc);

					if (errorOcc)
						SaveErrors(comp, dao, logFileId, true);
					//boolean resContains = false;
					if (comp.contains(FORMAT)) {
						//resContains = true;
						log.SaveResources(comp, dao, logFileId, date);
					}

					// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
					log.SaveIndexes(comp, dao, logFileId);

					if (comp.contains(FINISHING_INDEXING_FILE)) {
						status = "Finished";
						String finishTime = "";
						if (comp.contains("errors"))
							finishTime = comp.split("errors")[1].split("\n")[1]
									.split(" ")[0];
						dao.setFileStatus(status);
						dao.setLogEndDate(date);
						dao.setLogEndTime(finishTime);
						this.dataManager.updateLogFileStatus(dao, e2logFileId, status,
								errorOcc);
					}
				}
					else
					{
						boolean errorOcc = false;
						if (comp.contains(ERROR)) {
							
							errorOcc = true;
						}
						String errorCodes[] = SaveErrors(comp, dao, logFileId, false);
						System.out.println("errorCodes:2:" + errorCodes);
						logFileId = this.dataManager.getLogFileData(compressingFile,e2logFileId,
								compressingType,errorCodes);
						System.out.println("logFileId" + logFileId);
						if (logFileId != 0)
							this.dataManager.updateLogFileData(dao, logFileId,
									compressingType, errorOcc);
						else

							logFileId = this.dataManager.saveLogFileData(dao, e2logFileId,
									errorOcc);

						if (errorOcc)
							SaveErrors(comp, dao, logFileId, true);
					}
						
			}
			}
		//}
		/*
		 * This condition is to get the Collection file information
		 */
		 if (str.contains("compressing directory")) {

			compressingType = "Indexing";
			compCount = StringAccurance(str, "compressing directory");
			String addFile = "";
			String scanningFile = "";
			String addingFile = "";
			for (int i = 1; i <= compCount; i++) {
				compressingFile = returnnoBrackets(str
						.split("compressing directory")[i].split("profile")[0]);
				System.out.println("compressing file:" + compressingFile);
				String comp = str.split("compressing directory")[i];
				if (comp.contains(FINISHING_INDEXING_FILE)) {
				if (comp.contains("profile")) {
					profile = returnnoBrackets(comp.split("profile")[1]
							.split("format")[0]);
					System.out.println("profile:" + profile);
					dao.setProfile(profile.toUpperCase());
				}
				if (comp.contains("format")) {
					// System.out.println("format:"+str.split("compressing")[i].split("profile")[1].split("format")[1].split("\\n")[0]);
					format = returnnoBrackets(comp.split("format")[1]
							.split("documents")[0]);
					System.out.println("format:" + format);
					dao.setFormat(format);
				}
				if (comp.contains("documents")) {
					System.out.println("documents :"
							+ returnnoBrackets(comp.split("documents")[1]
									.split("\n")[0]));
					documents = returnnoBrackets(comp.split("documents")[1]
							.split("\n")[0]);
					System.out.println("documents:" + documents);
					dao.setDocuments(documents);
				}
				if (comp.contains("adding file")) {
					addFile = returnnoBrackets(comp.split("adding file")[1]
							.split("\n")[0]);
					System.out.println("adding file:" + addFile);
				}
				if (comp.contains("scanning journal")) {
					scanningFile = returnnoBrackets(comp
							.split("scanning journal")[1].split("\n")[0]);
					System.out.println("scanning file:" + scanningFile);
					dao.setJournalFile(addFile);
				}
				if (comp.contains("adding file")) {
					addingFile = returnnoBrackets(comp.split("adding file")[2]
							.split("\n")[0]);
					System.out.println("adding file:" + addingFile);
					dao.setCompressingFile(addingFile);
				}
				if (comp.contains("files") && comp.contains("pages")) {
					// System.out.println("pages:"+str.split("compressing")[i].split("pages")[1].split(" t")[0]);
					pages = returnnoBrackets(comp.split("files")[1].split(" t")[0]);
					System.out.println("pages:" + pages);
					dao.setPages(pages);
				}
				if (comp.contains("total read")) {
					// System.out.println("total read:"+str.split("compressing")[i].split("total read")[1]);
					totalRead = returnnoBrackets(comp.split("total read")[1]
							.split(" t")[0]);
					System.out.println("totalRead:" + totalRead);
					dao.setTotalRead(totalRead);
				}
				if (comp.contains("total written")) {
					// System.out.println("total written:"+str.split("compressing")[i].split("total written:")[1].split(" f")[0]);
					totalWritten = returnnoBrackets(comp.split("total written")[1]
							.split(" f")[0]);
					System.out.println("totalWritten:" + totalWritten);
					dao.setTotalWrite(totalWritten);
				}
				if (comp.contains("final ratio")) {
					// System.out.println("final ratio:"+str.split("compressing")[i].split("final ratio:")[1].split("\n")[0]);
					finalRatio = returnnoBrackets(comp.split("final ratio")[1]
							.split("\n")[0]);
					System.out.println("finalRatio:" + finalRatio);
					dao.setFinalRatio(finalRatio);
				}
				if (comp.contains("collection")) {
					// System.out.println("method"+str.split("compressing")[i].split("method")[1].split("]")[0]);
					methodName = returnnoBrackets(comp.split("collection")[1]
							.split(" h")[0]);
					System.out.println("collection:" + methodName);
					dao.setMethod(methodName);
				}

				boolean errorOcc = false;
				if(comp.contains("errors"))
				{
			//System.out.println("errors:"+str.split("finished indexing document file")[1].split(",")[1].split("errors")[0]);
			totalErrors = Integer.parseInt(returnnoBrackets(comp.split("finished indexing document file")[1].split(",")[1].split("errors")[0]));
			System.out.println("totalErrors:"+totalErrors);
				}
				if(totalErrors != 0)
				{
				if (comp.contains(ERROR)) {
					errorOcc = true;
				}
				}
				if (comp.contains(START_INDEXING_FILE)) {
					// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
					indexingFile = returnnoBrackets(comp
							.split(START_INDEXING_FILE)[1].split("]")[0]);
					System.out.println("indexingFile:" + indexingFile);
					dao.setDrdFile(indexingFile);
				}
				String errorCodes[] = SaveErrors(comp, dao, logFileId, true );
				logFileId = this.dataManager.getLogFileData(compressingFile,e2logFileId,
						compressingType,errorCodes);
				System.out.println("logFileId" + logFileId);
				if (logFileId != 0)
					this.dataManager.updateLogFileData(dao, logFileId,
							compressingType, errorOcc);
				else

					logFileId = this.dataManager.saveLogFileData(dao, e2logFileId,
							errorOcc);

				if (errorOcc)
					SaveErrors(comp, dao, logFileId, true);
				
				if (comp.contains(FORMAT)) {
					log.SaveResources(comp, dao, logFileId, date);
				}

				if (comp.contains(START_INDEXING_FILE)) {
					// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
					indexingFile = returnnoBrackets(comp
							.split(START_INDEXING_FILE)[1].split("]")[0]);
					System.out.println("indexingFile:" + indexingFile);
					log.SaveIndexes(comp, dao, logFileId);
				}
				if (comp.contains(FINISHING_INDEXING_FILE)) {
					status = "Finished";
					String finishTime = "";
					if (comp.contains("errors"))
						finishTime = comp.split("errors")[1].split("\n")[1]
								.split(" ")[0];
					dao.setFileStatus(status);
					dao.setLogEndTime(finishTime);
					this.dataManager.updateLogFileStatus(dao, e2logFileId, status,
							errorOcc);
				}
			}
				else
				{
					boolean errorOcc = false;
					if (comp.contains(ERROR)) {
						
						errorOcc = true;
					}
					String errorCodes[] = SaveErrors(comp, dao, logFileId, true);
					logFileId = this.dataManager.getLogFileData(compressingFile,e2logFileId,
							"compressing",errorCodes);
					System.out.println("logFileId" + logFileId);
					if (logFileId != 0)
						this.dataManager.updateLogFileData(dao, logFileId,
								compressingType, errorOcc);
					else

						logFileId = this.dataManager.saveLogFileData(dao, e2logFileId,
								errorOcc);

					if (errorOcc)
						SaveErrors(comp, dao, logFileId, true);
				}
					
		}
		}
		/*
		 * This condition is to get the Reindexing of existing drd file
		 */
		if (str.contains("start indexing document file")
				) {
			compressingType = "Reindexing";
			dao = new DataDTO();
			compCount = StringAccurance(str, "start indexing document file");
			for (int i = 1; i <= compCount; i++) {
				indexingFile = returnnoBrackets(str
						.split("start indexing document file")[i].split("]")[0]);
				if(!contains(str.split("start indexing document file")[i-1],"renaming file"))
				{
				String comp = str.split("start indexing document file")[i];
				
				System.out.println("Reindexing::indexingFile:" + indexingFile);
				if(comp.contains(FINISHING_INDEXING_FILE))
				{
				boolean errorOcc = false;
				if(comp.contains("errors"))
				{
			//System.out.println("errors:"+str.split("finished indexing document file")[1].split(",")[1].split("errors")[0]);
			totalErrors = Integer.parseInt(returnnoBrackets(comp.split("finished indexing document file")[1].split(",")[1].split("errors")[0]));
			System.out.println("totalErrors:"+totalErrors);
				}
				if(totalErrors != 0)
				{
				if (comp.contains(ERROR)) {
					errorOcc = true;
				}
				}
				// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
				dao.setDrdFile(indexingFile);
				dao.setIndexingFile(indexingFile);
				String drFile = "";
				if(indexingFile != null)
				{
					
					if(contains(indexingFile,"DOCDATA") &&  contains(indexingFile,".drd"))
					{
						drFile = indexingFile.split("DOCDATA")[1].split(".drd")[0];
					}
					else
						if(contains(indexingFile,"docdata") &&  contains(indexingFile,".drd"))
						{
							drFile = indexingFile.split("docdata")[1].split(".drd")[0];
						}
				}
				dao.setCompressingType(compressingType);
				String errorCodes[] = SaveErrors(comp, dao, logFileId, false);
				
				profile = this.dataManager.getLogProfile(indexingFile);
				dao.setProfile(profile);
				dao.setLogDate(date);
				dao.setLogTime(startingTime);
				logFileId = this.dataManager.getLogIndexFile(dao,errorOcc,e2logFileId,errorCodes,compressingType);
				if (logFileId == 0)
					logFileId = this.dataManager.saveLogFileData(dao, e2logFileId,
							errorOcc);
				else
					this.dataManager.updateLogFileType(logFileId, compressingType);
				
				if (errorOcc)
					log.SaveErrors(comp, dao, logFileId,true);

				if (comp.contains(FORMAT)) {
					log.SaveResources(comp, dao, logFileId, date);
				}

				// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
				SaveIndexes(comp, dao, logFileId);

			}
				else
				{
					boolean errorOcc = false;
					if(comp.contains("errors"))
					{
				//System.out.println("errors:"+str.split("finished indexing document file")[1].split(",")[1].split("errors")[0]);
				totalErrors = Integer.parseInt(returnnoBrackets(comp.split("finished indexing document file")[1].split(",")[1].split("errors")[0]));
				System.out.println("totalErrors:"+totalErrors);
					}
					if(totalErrors != 0)
					{
					if (comp.contains(ERROR)) {
						errorOcc = true;
					}
					}
					// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
					dao.setDrdFile(indexingFile);
					String errorCodes[] = SaveErrors(comp, dao, logFileId, false);
					logFileId = this.dataManager.getLogIndexFile(dao,errorOcc,e2logFileId,errorCodes,compressingType);
					if (logFileId == 0)
						logFileId = this.dataManager.saveLogFileData(dao, e2logFileId,
								errorOcc);
					else
						this.dataManager.updateLogFileType(logFileId, compressingType);
					if (errorOcc)
						log.SaveErrors(comp, dao, logFileId,true);

				}
			}
			}
		}
		/*
		 * This condition is to get the purging or unindexing of existing drd
		 * file
		 */
		if (str.contains("begin purging")) {

			if (str.contains("start unindexing document file")) {

				compressingType = "purging";
				compCount = StringAccurance(str,
						" start unindexing document file ");
				System.out.println(compCount);
				for (int i = 1; i <= compCount; i++) {
					System.out.println("purging indexingFile:"
							+ str.split(" start unindexing document file ")[1]
									.split("\n")[0]);
					indexingFile = returnnoBrackets(str
							.split(" start unindexing document file ")[1]
							.split("\n")[0]);
					String comp = str
							.split(" start unindexing  document file ")[0];
					dao.setDrdFile(indexingFile);
					System.out.println("indexingFile:" + indexingFile);
					if(comp.contains("finished unindexing document file"))
					{
					boolean errorOcc = false;
					if(comp.contains("errors"))
					{
				//System.out.println("errors:"+str.split("finished indexing document file")[1].split(",")[1].split("errors")[0]);
				totalErrors = Integer.parseInt(returnnoBrackets(comp.split("finished unindexing document file")[1].split(",")[1].split("errors")[0]));
				System.out.println("totalErrors:"+totalErrors);
					}
					if(totalErrors != 0)
					{
					if (comp.contains(ERROR)) {
						errorOcc = true;
					}
					}
					// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
					dao.setDrdFile(indexingFile);
					dao.setCompressingType(compressingType);
					if(indexingFile != null)
					{
						String drFile = "";
						if(contains(indexingFile,"DOCDATA") &&  contains(indexingFile,".drd"))
						{
							drFile = indexingFile.split("DOCDATA")[1].split(".drd")[0];
						}
						else
							if(contains(indexingFile,"docdata") &&  contains(indexingFile,".drd"))
							{
								drFile = indexingFile.split("docdata")[1].split(".drd")[0];
							}
					}
					String errorCodes[] = SaveErrors(comp, dao, logFileId, false);
					
					profile = this.dataManager.getLogProfile(indexingFile);
					dao.setProfile(profile);
					dao.setLogDate(date);
					dao.setLogTime(startingTime);
					logFileId = this.dataManager.getLogIndexFile(dao,errorOcc,e2logFileId,errorCodes,compressingType);
					if (logFileId == 0)
						logFileId = this.dataManager.saveLogFileData(dao, e2logFileId,
								errorOcc);
					
					
					if (errorOcc)
						log.SaveErrors(comp, dao, logFileId,true);

					if (comp.contains(FORMAT)) {
						log.SaveResources(comp, dao, logFileId, date);
					}

					log.SaveIndexes(comp, dao, logFileId);
					}
					else
					{
						boolean errorOcc = false;
						if(comp.contains("errors"))
						{
					//System.out.println("errors:"+str.split("finished indexing document file")[1].split(",")[1].split("errors")[0]);
					totalErrors = Integer.parseInt(returnnoBrackets(comp.split("finished unindexing document file")[1].split(",")[1].split("errors")[0]));
					System.out.println("totalErrors:"+totalErrors);
						}
						if(totalErrors != 0)
						{
						if (comp.contains(ERROR)) {
							errorOcc = true;
						}
						}
						// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
						dao.setDrdFile(indexingFile);
						dao.setCompressingType(compressingType);
						String errorCodes[] = SaveErrors(comp, dao, logFileId, false);
						
						
						logFileId = this.dataManager.getLogIndexFile(dao,errorOcc,e2logFileId,errorCodes,compressingType);
						if (logFileId == 0)
							logFileId = this.dataManager.saveLogFileData(dao, e2logFileId,
									errorOcc);
						else
							this.dataManager.updateLogFileType(logFileId, compressingType);
						if (errorOcc)
							log.SaveErrors(comp, dao, logFileId,true);

					}
				}
			}
		}
		/*
		 * This condition is to get the removing of existing drd file
		 */
		if (str.contains("start removing")) {

			if (str.contains("start unindexing document file")) {
				compressingType = "Remove";
				compCount = StringAccurance(str,
						" start unindexing document file ");
				System.out.println(compCount);

				for (int i = 1; i <= compCount; i++) {
					indexingFile = returnnoBrackets(str
							.split(" start unindexing document file ")[i]
							.split("]")[0]);
					String comp = str.split(" start unindexing document file ")[i];
					System.out.println("deleting indexingFile:" + indexingFile);
					dao.setDrdFile(indexingFile);
					
					if(comp.contains("finished unindexing document file"))
					{
					boolean errorOcc = false;
					if(comp.contains("errors"))
					{
				System.out.println("errors:"+str.split("finished indexing document file")[1].split(",")[1].split("errors")[0]);
						try
						{
				totalErrors = Integer.parseInt(returnnoBrackets(comp.split("finished unindexing document file")[1].split(",")[1].split("errors")[0]));
				
						}
						catch(Exception e)
						{
							totalErrors=0;
						}
						System.out.println("totalErrors:"+totalErrors);
					}
					if(totalErrors != 0)
					{
					if (comp.contains(ERROR)) {
						errorOcc = true;
					}
					}
					// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
					dao.setDrdFile(indexingFile);
					dao.setCompressingType(compressingType);
					if(indexingFile != null)
					{
						String drFile = "";
						if(contains(indexingFile,"DOCDATA") &&  contains(indexingFile,".drd"))
						{System.out.println("1"+drFile);
							drFile = indexingFile.split("DOCDATA")[1].split(".drd")[0];
							System.out.println("2"+drFile);
						}
						else
							if(contains(indexingFile,"docdata") &&  contains(indexingFile,".drd"))
							{System.out.println("3"+drFile);
								drFile = indexingFile.split("docdata")[1].split(".drd")[0];
								System.out.println("4"+drFile);
							}
					}
					//System.out.println("here1");
					String errorCodes[] = SaveErrors(comp, dao, logFileId, false);
					//System.out.println("here2");
					profile = this.dataManager.getLogProfile(indexingFile);
					System.out.println("profile"+profile);
					dao.setProfile(profile);
					dao.setLogDate(date);
					dao.setLogTime(startingTime);
					logFileId = this.dataManager.getLogIndexFile(dao,errorOcc,e2logFileId,errorCodes,compressingType);
					
				//	logFileId = this.dataManager.getLogIndexFile(dao,errorOcc,e2logFileId,errorCodes,compressingType);
					if (logFileId == 0)
						logFileId = this.dataManager.saveLogFileData(dao, e2logFileId,
								errorOcc);
					else
						this.dataManager.updateLogFileType(logFileId, compressingType);
					if (errorOcc)
						log.SaveErrors(comp, dao, logFileId,true);

					if (comp.contains(FORMAT)) {
						log.SaveResources(comp, dao, logFileId, date);
					}

					log.SaveIndexes(comp, dao, logFileId);

				}
					
						else
						{
							boolean errorOcc = false;
							if(comp.contains("errors"))
							{
						//System.out.println("errors:"+str.split("finished indexing document file")[1].split(",")[1].split("errors")[0]);
						totalErrors = Integer.parseInt(returnnoBrackets(comp.split("finished unindexing document file")[1].split(",")[1].split("errors")[0]));
						System.out.println("totalErrors:"+totalErrors);
							}
							if(totalErrors != 0)
							{
							if (comp.contains(ERROR)) {
								errorOcc = true;
							}
							}
							// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
							dao.setDrdFile(indexingFile);
							dao.setCompressingType(compressingType);
							//String errorCodes[] = SaveErrors(comp, dao, logFileId, false);
							if (logFileId == 0)
								logFileId = this.dataManager.saveLogFileData(dao, logFileId,
										errorOcc);
							if (errorOcc)
								log.SaveErrors(comp, dao, logFileId,true);

						}
				}

				/*
				 * if(comp.contains("finished unindexing document file")) {
				 * System.out.println("index end time :"+str.split(
				 * "finished unindexing document file"
				 * )[0].split("\n")[1].split("errors")[0]); indexFinishingTime =
				 * str.split("start unindexing document file")[0].split(
				 * "flushing index cache"
				 * )[1].split("add unique")[filesizecount].
				 * split("file size")[0].split("\n")[1].split(" ")[0]; }
				 */
			}
		}

		/*
		 * To read the errors e.g. licence expired, insufficient disk space etc.
		 */
		if(str.contains(ERROR) )
		{
			if(!str.contains(COMPRESSING) && !str.contains(FINISHING_INDEXING_FILE) && (!str.contains("start indexing document file") && !str.contains(" start unindexing document file ")))
					{
			System.out.println("errors:"+str.split(ERROR)[1].split("\n")[0]);
			//boolean errorOcc = true;
			
			log.SaveErrors(str, dao, logFileId,true);
					}
		}
		
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public boolean SaveResources(String comp, DataDTO dao, Long logFileId,
			String date) throws SQLException {
		DataIntegrityDAO DataDTO = new DataIntegrityDAO();
		String wrote[] = null;
		String sizes[] = null;
		String resstartTime = "";
		String resFinishingTime = "";
		int formatcount = StringAccurance(comp.split(FORMAT)[1], WROTE);
		System.out.println("wrote count" + formatcount);
		int x = 0;
		try
		{
		if (formatcount != 0) {
			wrote = new String[formatcount + 1];
			sizes = new String[formatcount + 1];
			System.out.println("wrote time :"
					+ comp.split(FORMAT)[1].split(WROTE)[0].split("\n")[1]
							.split(" ")[0]);
			resstartTime = comp.split(FORMAT)[1].split(WROTE)[0].split("\n")[1]
					.split(" ")[0];
			dao.setResStartTime(resstartTime);
			for (int j = 1; j <= formatcount; j++) {
				// System.out.println("wrote :"+comp.split("format")[1].split("wrote")[j].split("\n")[0].split("size")[0]);
				// System.out.println("size :"+comp.split("format")[1].split("wrote")[j].split("\n")[0].split("size")[1]);

				wrote[x] = returnnoBrackets(comp.split(FORMAT)[1].split(WROTE)[j]
						.split("\n")[0].split(SIZE)[0]);
				System.out.println("wrote:" + wrote[x]);
				sizes[x] = returnnoBrackets(comp.split(FORMAT)[1].split(WROTE)[j]
						.split("\n")[0].split(SIZE)[1]);
				System.out.println("size:" + sizes[x]);

				x++;
			}
			System.out.println("wrote end time :"
					+ comp.split(FORMAT)[1].split(WROTE)[formatcount - 1]
							.split("\n")[1].split(" ")[0]);
			resFinishingTime = comp.split(FORMAT)[1].split(WROTE)[formatcount - 1]
					.split("\n")[1].split(" ")[0];
			DataDTO.saveLogResources(wrote, sizes, date, resstartTime,
					resFinishingTime, logFileId, formatcount);
		}
		}
		catch(Exception r)
		{
			r.printStackTrace();
		}
		return true;
	}

	public boolean SaveIndexes(String comp, DataDTO dao, Long logFileId)
			throws SQLException {
		DataIntegrityDAO DataDTO = new DataIntegrityDAO();
		String indexFile[] = null;
		String indexFileSize[] = null;
		String indexDiskRead[] = null;
		String indexDiskWrite[] = null;
		String indexStackDepth[] = null;
		String indexCacheReadHit[] = null;
		String indexCacheReadMiss[] = null;
		String indexCacheWriteHit[] = null;
		String indexCacheWriteMiss[] = null;
		String indexAddDuplicate[] = null;
		String indexAddUnique[] = null;
		String totalErrors = "";

		String indexstartTime = "";
		String indexFinishingTime = "";
		String comp2 =  comp.split(FLUSHING_INDEX_CACHE)[1];
		String comp1 =  comp2.split(FINISHING_INDEXING_FILE)[0];
		int indexFiles = StringAccurance(comp1, "INDEX\\");
		int indexFiles1 = StringAccurance(comp1, "INDEX\\");
		System.out.println("count>1>" + indexFiles);
		System.out.println("count>2>" + indexFiles1);
		indexFile = new String[indexFiles + 1];
		indexFileSize = new String[indexFiles + 1];
		indexDiskRead = new String[indexFiles + 1];
		indexDiskWrite = new String[indexFiles + 1];
		indexStackDepth = new String[indexFiles + 1];
		indexCacheReadHit = new String[indexFiles + 1];
		indexCacheReadMiss = new String[indexFiles + 1];
		indexCacheWriteHit = new String[indexFiles + 1];
		indexAddDuplicate = new String[indexFiles + 1];
		indexAddUnique = new String[indexFiles + 1];
		indexCacheWriteMiss = new String[indexFiles + 1];
		if (comp.contains(FLUSHING_INDEX_CACHE)) {
			// System.out.println("index start time :"+comp.split(FLUSHING_INDEX_CACHE)[1].split("\n")[1].split(" ")[0]);
			indexstartTime = comp.split(FLUSHING_INDEX_CACHE)[1].split("\n")[1]
					.split(" ")[0];
			System.out.println("index start time :" + indexstartTime);

			dao.setIndexingStartTime(indexstartTime);
			System.out.println("filesizecount" + indexFiles);
			//System.out.println("disk read......::::"
					//+ comp.split(FLUSHING_INDEX_CACHE)[1]);
			for (int x = 1; x <= indexFiles; x++) {
				System.out.println("disk read......::::"
						+ comp.split(FLUSHING_INDEX_CACHE)[1]);
				
				if (comp.contains(FINISHING_INDEXING_FILE)) {
					// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
					String indexingFile = returnnoBrackets(comp
							.split(FINISHING_INDEXING_FILE)[1].split("]")[0]);
					System.out.println("indexingFile:" + indexingFile);
					dao.setIndexingFile(indexingFile);
				}

				indexFile[x] = comp.split(FLUSHING_INDEX_CACHE)[1]
						.split("INDEX\\\\")[x].split("\n")[0];

				System.out.println("index cache2 :" + indexFile[x]);
				System.out.println("disk read......::::"
						+ comp.split(FLUSHING_INDEX_CACHE)[1]
								.split("INDEX\\\\")[x].contains("disk read"));
				
				dao.setIndexName(indexFile[x]);
				
				
				if (comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
						.contains("file size")) {
					// System.out.println("index fileSize :"+comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x].split(FILE_SIZE)[1].split("\n")[0]);
					indexFileSize[x] = returnnoBrackets(comp
							.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
							.split(FILE_SIZE)[1].split("\n")[0]);
					indexFinishingTime = comp.split(FLUSHING_INDEX_CACHE)[1]
							.split("INDEX\\\\")[x].split(FILE_SIZE)[1]
							.split("\n")[1].split(" ")[0];
					System.out.println("indexFinishingTime"
							+ indexFinishingTime);
					dao.setIndexFileSize(indexFileSize[x]);
					dao.setIndexingFinishingTime(indexFinishingTime);
				}

				if (comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
						.contains("disk read")) {
					// System.out.println("index cache disk :"+comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x].split("disk read")[1].split("\n")[0]);
					indexDiskRead[x] = returnnoBrackets(comp
							.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
							.split("disk read")[1].split("\n")[0]);
					System.out.println("index disk read :" + indexDiskRead[x]);

					indexFinishingTime = comp.split(FLUSHING_INDEX_CACHE)[1]
							.split("INDEX\\\\")[x].split(DISK_READ)[1]
							.split("\n")[1].split(" ")[0];
					System.out.println("indexFinishingTime"
							+ indexFinishingTime);
					dao.setIndexDiskRead(indexDiskRead[x]);
					dao.setIndexingFinishingTime(indexFinishingTime);
				}

				if (comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
						.contains(DISK_WRITE)) {
					System.out.println("index cache disk write:"
							+ comp.split(FLUSHING_INDEX_CACHE)[1]
									.split("INDEX\\\\")[x].split(DISK_WRITE)[1]
									.split("\n")[0]);
					indexDiskWrite[x] = returnnoBrackets(comp
							.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
							.split(DISK_WRITE)[1].split("\n")[0]);
					indexFinishingTime = comp.split(FLUSHING_INDEX_CACHE)[1]
							.split("INDEX\\\\")[x].split(DISK_WRITE)[1]
							.split("\n")[1].split(" ")[0];
					System.out.println("indexFinishingTime"
							+ indexFinishingTime);
					dao.setIndexDiskWrite(indexDiskWrite[x]);
					dao.setIndexingFinishingTime(indexFinishingTime);
				}

				if (comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
						.contains(STACK_DEPTH)) {
					System.out
							.println("stack depth :"
									+ comp.split(FLUSHING_INDEX_CACHE)[1]
											.split("INDEX\\\\")[x]
											.split(STACK_DEPTH)[1].split("\n")[0]);
					indexStackDepth[x] = returnnoBrackets(comp
							.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
							.split(STACK_DEPTH)[1].split("\n")[0]);
					indexFinishingTime = comp.split(FLUSHING_INDEX_CACHE)[1]
							.split("INDEX\\\\")[x].split(STACK_DEPTH)[1]
							.split("\n")[1].split(" ")[0];
					System.out.println("indexFinishingTime"
							+ indexFinishingTime);
					dao.setIndexStackDepth(indexStackDepth[x]);
					dao.setIndexingFinishingTime(indexFinishingTime);
				}

				if (comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
						.contains(CACHE_READ_HIT)) {
					System.out.println("cache read hit :"
							+ comp.split(FLUSHING_INDEX_CACHE)[1]
									.split("INDEX\\\\")[x]
									.split(CACHE_READ_HIT)[1].split("\n")[0]);
					indexCacheReadHit[x] = returnnoBrackets(comp
							.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
							.split(CACHE_READ_HIT)[1].split("\n")[0]);
					indexFinishingTime = comp.split(FLUSHING_INDEX_CACHE)[1]
							.split("INDEX\\\\")[x].split(CACHE_READ_HIT)[1]
							.split("\n")[1].split(" ")[0];
					System.out.println("indexFinishingTime"
							+ indexFinishingTime);
					dao.setIndexCacheReadHit(indexCacheReadHit[x]);
					dao.setIndexingFinishingTime(indexFinishingTime);
				}

				if (comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
						.contains(CACHE_READ_MISS)) {
					System.out.println("cache read miss :"
							+ comp.split(FLUSHING_INDEX_CACHE)[1]
									.split("INDEX\\\\")[x]
									.split(CACHE_READ_MISS)[1].split("\n")[0]);
					indexCacheReadMiss[x] = returnnoBrackets(comp
							.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
							.split(CACHE_READ_MISS)[1].split("\n")[0]);
					indexFinishingTime = comp.split(FLUSHING_INDEX_CACHE)[1]
							.split("INDEX\\\\")[x].split(CACHE_READ_MISS)[1]
							.split("\n")[1].split(" ")[0];
					System.out.println("indexFinishingTime"
							+ indexFinishingTime);
					dao.setIndexCacheReadMiss(indexCacheReadMiss[x]);
					dao.setIndexingFinishingTime(indexFinishingTime);
				}

				if (comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
						.contains(CACHE_WRITE_HIT)) {
					System.out.println("cache write hit :"
							+ comp.split(FLUSHING_INDEX_CACHE)[1]
									.split("INDEX\\\\")[x]
									.split(CACHE_WRITE_HIT)[1].split("\n")[0]);
					indexCacheWriteHit[x] = returnnoBrackets(comp
							.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
							.split(CACHE_WRITE_HIT)[1].split("\n")[0]);
					indexFinishingTime = comp.split(FLUSHING_INDEX_CACHE)[1]
							.split("INDEX\\\\")[x].split(CACHE_WRITE_HIT)[1]
							.split("\n")[1].split(" ")[0];
					System.out.println("indexFinishingTime"
							+ indexFinishingTime);
					dao.setIndexCacheWriteHit(indexCacheWriteHit[x]);
					dao.setIndexingFinishingTime(indexFinishingTime);
				}

				if (comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
						.contains(CACHE_WRITE_MISS)) {
					System.out.println("cache write miss :"
							+ comp.split(FLUSHING_INDEX_CACHE)[1]
									.split("INDEX\\\\")[x]
									.split(CACHE_WRITE_MISS)[1].split("\n")[0]);
					indexCacheWriteMiss[x] = returnnoBrackets(comp
							.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
							.split(CACHE_WRITE_MISS)[1].split("\n")[0]);
					indexFinishingTime = comp.split(FLUSHING_INDEX_CACHE)[1]
							.split("INDEX\\\\")[x].split(CACHE_WRITE_MISS)[1]
							.split("\n")[1].split(" ")[0];
					System.out.println("indexFinishingTime"
							+ indexFinishingTime);
					dao.setIndexCacheWriteMiss(indexCacheWriteMiss[x]);
					dao.setIndexingFinishingTime(indexFinishingTime);
				}

				if (comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
						.contains(ADD_DUPLICATE)) {
					System.out
							.println("add duplicate :"
									+ comp.split(FLUSHING_INDEX_CACHE)[1]
											.split("INDEX\\\\")[x]
											.split(ADD_DUPLICATE)[1]
											.split("\n")[0]);
					indexAddDuplicate[x] = returnnoBrackets(comp
							.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
							.split(ADD_DUPLICATE)[1].split("\n")[0]);
					indexFinishingTime = comp.split(FLUSHING_INDEX_CACHE)[1]
							.split("INDEX\\\\")[x].split(ADD_DUPLICATE)[1]
							.split("\n")[1].split(" ")[0];
					dao.setIndexAddDuplicate(indexAddDuplicate[x]);
					dao.setIndexingFinishingTime(indexFinishingTime);
				}

				if (comp.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
						.contains(ADD_UNIQUE)) {
					System.out.println("add unique :"
							+ comp.split(FLUSHING_INDEX_CACHE)[1]
									.split("INDEX\\\\")[x].split(ADD_UNIQUE)[1]
									.split("\n")[0]);
					indexAddUnique[x] = returnnoBrackets(comp
							.split(FLUSHING_INDEX_CACHE)[1].split("INDEX\\\\")[x]
							.split(ADD_UNIQUE)[1].split("\n")[0]);
					indexFinishingTime = comp.split(FLUSHING_INDEX_CACHE)[1]
							.split("INDEX\\\\")[x].split(ADD_UNIQUE)[1]
							.split("\n")[1].split(" ")[0];
					dao.setIndexAddUnique(indexAddUnique[x]);
					dao.setIndexingFinishingTime(indexFinishingTime);
				}

				
				Long indexId = DataDTO.saveLogIndexing(dao, logFileId);
				System.out.println("coming here to save logindexing"+indexId);
			}

		}

		if (comp.contains("errors")) {
			// System.out.println("errors:"+str.split("finished indexing document file")[1].split(",")[1].split("errors")[0]);
			totalErrors = returnnoBrackets(comp.split(FLUSHING_INDEX_CACHE)[1]
					.split(",")[1].split("errors")[0]);
			System.out.println("totalErrors:" + totalErrors);

		}
		return true;
	}

	public String[] SaveErrors(String comp, DataDTO dao, Long logFileId, boolean save) {
		DataIntegrityDAO DataDTO = new DataIntegrityDAO();
		//boolean errorOcc = false;
		String[] errorCode = null;
		String[] errors = null;
		int errorcount = 0;
		
		try {
			if (comp.contains(ERROR)) {
				errorcount = StringAccurance(comp, ERROR);
				System.out.println("errorcount" + errorcount);
				errors = new String[errorcount + 1];
				errorCode = new String[errorcount + 1];
				for (int j = 1; j <= errorcount; j++) {

					errorCode[j] = comp.split(ERROR)[j].split("\n")[0]
							.split(":")[0];
					errors[j] = comp.split(ERROR)[j].split("\n")[0].split(":")[1];
					if(save)
					{
					Long id = DataDTO.saveLogErrorData(errorCode[j], errors[j],
							logFileId, errorcount);
					System.out.println("Error are saved successfully -->"+id);
					}
					System.out.println("Error codes are -->" + errorCode[j]);
					System.out.println("Errors are -->" + errors[j]);
					// dao.setErrorCode(errorCode);
					// dao.setErrors(errors);
					System.out.println("before saving errors" + j);

				}
				System.out.println("before saving errors" + errorCode);
				return errorCode;
				// errorOcc = true;
			}
		} catch (Exception e) {

		}
		return errorCode;
	}

	public static String readFile1() {

		// e2loaderd.20110802.115132.4584
		// Date date = new Date();
		Calendar cal1 = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String date = format.format(cal1.getTime());
		SimpleDateFormat format1 = new SimpleDateFormat("hhmmss");
		String time = format1.format(cal1.getTime());
		System.out.println("new date format:" + date);
		System.out.println("new time format:" + time);
		/*
		 * long pid = CJSSystemInfo.getInformationsAboutFileSystem();
		 * 
		 * if(pid != 0L) {
		 * 
		 * System.out.println("pid:"+pid); }
		 */
		// File file = new
		// File("C:\\PBBI_CCM\\e2Vault\\server\\log\\e2loaderd."+date+".093612."+pid+".log");
		// File file = new
		// File("C:\\PBBI_CCM\\e2Vault\\server\\log\\e2loaderd.20120215.185253.4552.log");
		// File file = new
		// File("C:\\hasini\\e2loaderd.20111124.131641.9032.log");
		// File file = new
		// File("C:\\New folder1\\re-indexing\\e2loaderd.20120314.122000.464.log");
		// File file = new
		// File("C:\\New folder1\\purge\\e2loaderd.20120314.141556.3896.log");//
		// purge
		// File file = new
		// File("C:\\New folder1\\delete\\e2loaderd.20120314.132428.2088.log");//remove
		// File file = new
		// File("C:\\New folder1\\another_log\\e2loaderd.20120215.175005.2700.log");
		// //errors while compressing
		// File file = new
		// File("C:\\New folder1\\log\\e2loaderd.20111208.141417.676.log");
		// File file = new
		// File("C:\\New folder1\\log\\e2loaderd.20111208.164219.11792.log");
		// File file = new
		// File("C:\\New folder1\\log\\e2loaderd.20120125.090737.2308.log");//only
		// for errors
		// File file = new
		// File("C:\\New folder1\\log\\e2loaderd.20111230.125009.2384.log");
		// File file = new
		// File("C:\\New folder1\\another_log\\e2loaderd.20120213.180614.9344.log");
		// // collection
		// File file = new
		// File("C:\\New folder1\\another_log\\e2loaderd.20120216.143339.8484.log");
		File file = new File(
				"C:\\New folder1\\log\\e2loaderd.20120207.135852.9808.log");

		StringBuffer contents = new StringBuffer();

		BufferedReader reader = null;

		try {

			reader = new BufferedReader(new FileReader(file));

			String text = null;

			// repeat until all lines is read

			while ((text = reader.readLine()) != null) {

				contents.append(text).append(
						System.getProperty("line.separator"));

			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (reader != null) {

					reader.close();

				}

			} catch (IOException e) {

				e.printStackTrace();

			}

		}
		return contents.toString();
	}

}

/**
 * Common fields for Apache Log demo.
 */
interface LogExample {
	/** The number of fields that must be found. */
	public static final int NUM_FIELDS = 9;

	/** The sample log entry to be parsed. */
	public static final String logEntryLine = "123.45.67.89 - - [27/Oct/2000:09:27:09 -0400] \"GET /java/javaResources.html HTTP/1.0\" 200 10450 \"-\" \"Mozilla/4.6 [en] (X11; U; OpenBSD 2.8 i386; Nav) \\n 123.45.67.89 - - [27/Oct/2000:09:27:09 -0400] \"GET /java/javaResources.html HTTP/1.0\" 200 10450 \"-\" \"Mozilla/4.6 [en] (X11; U; OpenBSD 2.8 i386; Nav)\"";

}