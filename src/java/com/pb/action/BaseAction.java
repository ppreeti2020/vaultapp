package com.pb.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport implements SessionAware,
		ServletRequestAware, ServletResponseAware {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private Map<String, Object> session;
	private Map<String, Object> parameter;

	public Map<String, Object> getParameter() {
		return parameter;
	}

	public void setParameter(Map<String, Object> parameter) {
		this.parameter = parameter;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public HttpServletResponse getServletResponse() {
		return this.servletResponse;
	}

	public void setServletResponse(HttpServletResponse paramHttpServletResponse) {
		this.servletResponse = paramHttpServletResponse;
	}

	public HttpServletRequest getServletRequest() {
		return this.servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public Map<String, Object> getSession() {
		return this.session;
	}

	public void loadStrutsSession() {
		this.session = ActionContext.getContext().getSession();
	}

	public void loadStrutsParameter() {
		this.parameter = ActionContext.getContext().getParameters();
	}

	public void loadDefault() {
		loadStrutsParameter();
		loadStrutsSession();
	}
}
