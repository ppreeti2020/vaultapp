package com.pb.admin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.ldap.LDAPUtils;

public class MakerSubmitAction extends ActionSupport implements
		ServletRequestAware, ServletResponseAware {

	static final Logger logger = Logger.getLogger(MakerSubmitAction.class);

	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	private String message;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private String searchIndexValues;
	private List<String> csrGroups;
	private List<String> prePopulateList;

	public List<String> getPrePopulateList() {
		return prePopulateList;
	}

	public void setPrePopulateList(List<String> prePopulateList) {
		this.prePopulateList = prePopulateList;
	}

	public List<String> getCsrGroups() {
		return csrGroups;
	}

	public void setCsrGroups(List<String> csrGroups) {
		this.csrGroups = csrGroups;
	}

	public String getSearchIndexValues() {
		return searchIndexValues;
	}

	public void setSearchIndexValues(String searchIndexValues) {
		this.searchIndexValues = searchIndexValues;
	}

	public HttpServletRequest getServletRequest() {
		return request;
	}

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getServletResponse() {
		return response;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public String submit() {
		//System.out.println("masuk4");
		HashMap<String, String> propertiesMap = new HashMap<String, String>();
		Enumeration params = request.getParameterNames();
		String currentGrp = "";
		//System.out.println("masuk3");
		prePopulateList = new ArrayList<String>();
		while (params.hasMoreElements()) {
			String paramName = params.nextElement().toString();
			if (paramName.startsWith("LDAP_")) {
				String selectedCheckBoxValue = request.getParameter(paramName);
				logger.info("value:  " + selectedCheckBoxValue);
				if (selectedCheckBoxValue != null) {
					//System.out.println("masuk5");
					// for prepopulating checkboxes
					prePopulateList.add(paramName);

					String[] tempArr = paramName.split("_");
					String selectedGrp = tempArr[1];
					String selectedCategory = tempArr[2];

					if (currentGrp.equals("")) {
						// first time
						propertiesMap.put(selectedGrp, selectedCategory);
					} else if (currentGrp.equals(selectedGrp)) {
						// second or third or fourth time etc.
						// get previous category value and append new category
						// value.
						String prevCat = propertiesMap.get(selectedGrp);

						propertiesMap.put(selectedGrp, prevCat + ","
								+ selectedCategory);
					} else {
						propertiesMap.put(selectedGrp, selectedCategory);
					}

					currentGrp = selectedGrp;
				}
			}
		}
		logger.info("propertiesMap ------------ " + propertiesMap);
		//System.out.println("masuk");
		try {
			Properties properties = new Properties();
			Iterator<String> itr = propertiesMap.keySet().iterator();
			while (itr.hasNext()) {
				//System.out.println("masuk2");
				String key = itr.next();
				String value = propertiesMap.get(key);
				properties.setProperty(key, value);

			}
			//System.out.println("masuk7");
			properties
					.store(new FileOutputStream(
							PropertyUtils
									.getProperties(
											CommonConstants.CSR_PROPERTY_FILE_NAME)
									.getProperty(
											CommonConstants.APPROVAL_PROPERTIES_FILE_PATH)),
							null);
			properties = PropertyUtils.getProperties(PropertyUtils
					.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME)
					.getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
			properties.setProperty(CommonConstants.STATUS_APPROVAL, "1");
			properties
					.store(new FileOutputStream(
							PropertyUtils
									.getProperties(
											CommonConstants.CSR_PROPERTY_FILE_NAME)
									.getProperty(
											CommonConstants.STATUS_PROPERTIES_FILE_PATH)),
							null);
			//System.out.println("masuk6");
			setMessage("Permissions successfully pending for approval!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("IOException in MakerSubmitAction writing permissoins file "
					+ e.getMessage());
			e.printStackTrace();
		}

		csrGroups = LDAPUtils.getLDAPCSRGroups(CommonConstants.CSR_PROPERTY_FILE_NAME);
		searchIndexValues = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.AVAILABLE_PBIDX_VALUES);

		return "SUCCESS";
	}

	public String cancel() {
		Properties properties = PropertyUtils
				.getProperties(CommonConstants.STATUS_PROPERTIES_FILE_PATH);
		properties.setProperty(CommonConstants.STATUS_APPROVAL, "0");
		try {
			properties
					.store(new FileOutputStream(
							PropertyUtils
									.getProperties(
											CommonConstants.CSR_PROPERTY_FILE_NAME)
									.getProperty(
											CommonConstants.STATUS_PROPERTIES_FILE_PATH)),
							null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		init();
		return "SUCCESS";
	}

	public String init() {
		//System.out.println("1");
		String permissionsApprovalFilePath = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.STATUS_PROPERTIES_FILE_PATH);
		System.out.println("2" + permissionsApprovalFilePath);
		Properties prePopulateApproval = PropertyUtils
				.getProperties(permissionsApprovalFilePath);
		csrGroups = LDAPUtils.getLDAPCSRGroups(CommonConstants.CSR_PROPERTY_FILE_NAME);
		searchIndexValues = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.AVAILABLE_PBIDX_VALUES);
		Properties propStatus = PropertyUtils.getProperties(PropertyUtils
				.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME)
				.getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
		int status = Integer.parseInt(propStatus
				.getProperty(CommonConstants.STATUS_APPROVAL));
		prePopulateList = new ArrayList<String>();
		String permissionsFilePath = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.PERMISSION_FILE_PATH);
		System.out.println("statusnya:" + status);
		if (status == 0 || status == 2) {
			//System.out.println("3");
			if (status == 2) {
				setMessage("Your permissions submission approval has been rejected!! you can submit again now");
			}

		} else {
			permissionsFilePath = PropertyUtils.getProperties(
					CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
					CommonConstants.APPROVAL_PROPERTIES_FILE_PATH);
			setMessage("Your permissions approval has pending for approval");
		}
		//System.out.println("4");
		Properties prePopulate = PropertyUtils
				.getProperties(permissionsFilePath);
		//System.out.println("5");
		Enumeration en = prePopulate.keys();
		while (en.hasMoreElements()) {
			String key = en.nextElement().toString();
			String value = prePopulate.getProperty(key);
			logger.info("-------- key: " + key + "     value: " + value);
			// multiple categories
			if (value.indexOf(",") != -1) {
				String[] tempArr = value.split(",");
				for (String temp : tempArr) {
					prePopulateList.add("LDAP_" + key + "_" + temp);
				}
				// single category
			} else {
				prePopulateList.add("LDAP_" + key + "_" + value);
			}
		}
		return "SUCCESS";
	}

	public String execute() {
		boolean isSubmit = request.getParameter("submit_button") != null ? true
				: false;
		if (isSubmit) {
			boolean isCancel = request.getParameter("cancel_button") != null ? true
					: false;
			if (isCancel) {
				System.out.println("cancel");
				cancel();
			} else {
				System.out.println("submit");

				submit();
			}
		} else {
			System.out.println("init");
			init();
		}
		String permissionsApprovalFilePath = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.STATUS_PROPERTIES_FILE_PATH);
		Properties prePopulateApproval = PropertyUtils
				.getProperties(permissionsApprovalFilePath);
		Properties propStatus = PropertyUtils.getProperties(PropertyUtils
				.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME)
				.getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
		int status = Integer.parseInt(propStatus
				.getProperty(CommonConstants.STATUS_APPROVAL));
		setStatus(status);
		return "SUCCESS";
	}
}
