package com.pb.admin;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.NamingException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.ListUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.common.ServerSideValidationUtil;
import com.pb.dao.DataDAO;
import com.pb.dao.DataIntegrityDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.DataDTO;
import com.pb.dto.UserDTO;
import com.pb.ldap.LDAPUtils;

public class AdminSubmitAction extends ActionSupport implements
		ServletRequestAware, ServletResponseAware {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static final Logger logger = Logger.getLogger(AdminSubmitAction.class);

	private String message;
	private String tabId;
	private String subtabId;
	private String[] csrGrps;
	private String[] selcsrGrps;
	private List<String> selprGrps;
	private Long[] roleId;
	private Long[] dbId;
	private List<UserDTO> seldb;
	private String active;
	private String validdateFrom;
	private String validdateTo;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private String searchIndexValues;
	private List<String> csrGroups;
	private List<String> preSelCsrGroups;
	private List<String> preSelAllCsrGroups;
	private List csrAllUsers;
	private String[] csrUsers;
	private String[] selCsrUsers;
	private List<String> selCsrGroups;
	private List<UserDTO> csrRoles;
	private List<UserDTO> csrDatabases;
	private List<Long> selCsrDatabases;
	private List<Long> selProfileDatabases;
	private List<UserDTO> profileList;
	private List<String> prePopulateList;
	private Map<String,List<String>> profilePrePopulateList;
	private String validfromDays;
	private String validtoDays;
	private int currYear;
	private String accessdateFrom;
	private String accessdateTo;
	private int accessfromDays;
	private int accesstoDays;
	private String weekend;
	private Long[] actionId;
	private List<String> csrActions;
	private Long currUserId;
	private Map<String, Object> session;
	private Map<String, List> userGroups;
	private Map<String, List> userProfiles;
	private String[] profileNames;
	private Long[] profileIds;
	private String selProfile;
	private Long[] newUserId;
	private Long[] newProfileId;
	private Long groupId[];
	private String userGrp;
	private String userName;
	private String[] roleName;
	private String[] dbName;
	private Long userId;
	private String tabName;
	private String subTabName;
	private String profileName;
	private UserDAO userManager = new UserDAO();
	private Map<String, List> profileUserGroups;
	private List<UserDTO> ldapUsersList;
	private String[] csrProfiles;

	private String selectedGroup;
	private List<String> printerNames;

	private List<String> addedPrinterNames;
	private String[] seladdedPrinterNames;
	
	private List<String> profiles;
	List<UserDTO> profileGroupList = new ArrayList<UserDTO>();
	
	private String tableName;
	private String tableName1;
	private int columnnum;
	private int columnnum1;
	private String columnNames;
	private String columnDescs;
	private String dataTypes;
	private String constraints;
	private String widths;
	private List<Long> seldbIds;
	private Long seldbId;
	private Map<Long,DataDAO> profMapList;
	private ArrayList<String> dbindexList;
	private Map<String, Integer> dbdataTypeMap;
	private String subAccessTabId;
	private String subSystemTabId;
	private Map<String, Long> detailIndex;
	private Map<String,Map<String, Long>> profileDetailIndex;
	private List<Long> dbIndexes;
	private String databaseName;
	private String profileAction;
	private Long profileId;
	private Map<String, List<String>> prdbIndex;
	private Map<String, List<String>> dbIndex;
	
	private String profileAdmin;
	
	
	
	public String getProfileAdmin() {
		return profileAdmin;
	}

	public void setProfileAdmin(String profileAdmin) {
		this.profileAdmin = profileAdmin;
	}

	public Map<String, List<String>> getDbIndex() {
		return dbIndex;
	}

	public void setDbIndex(Map<String, List<String>> dbIndex) {
		this.dbIndex = dbIndex;
	}
	
	public Map<String, List<String>> getPrdbIndex() {
		return prdbIndex;
	}

	public void setPrdbIndex(Map<String, List<String>> prdbIndex) {
		this.prdbIndex = prdbIndex;
	}
	
	public List<UserDTO> getSeldb() {
		return seldb;
	}

	public void setSeldb(List<UserDTO> seldb) {
		this.seldb = seldb;
	}
	
	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}
	
	public String getProfileAction() {
		return profileAction;
	}

	public void setProfileAction(String profileAction) {
		this.profileAction = profileAction;
	}
	
	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	
	public List<Long> getDbIndexes() {
		return dbIndexes;
	}

	public void setDbIndexes(ArrayList<Long> dbIndexes) {
		this.dbIndexes = dbIndexes;
	}
		
	public Map<String, Long> getDetailIndex() {
		return detailIndex;
	}

	public void setDetailIndex(HashMap<String, Long> detailIndex) {
		this.detailIndex = detailIndex;
	}
	
	public Map<String,Map<String, Long>> getProfileDetailIndex() {
		return profileDetailIndex;
	}

	public void setProfileDetailIndex(Map<String,Map<String, Long>> profileDetailIndex) {
		this.profileDetailIndex = profileDetailIndex;
	}
	
	public void setSubSystemTabId(String subSystemTabId) {
		this.subSystemTabId = subSystemTabId;
	}

	public String getSubSystemTabId() {
		return subSystemTabId;
	}

	
	public void setSubAccessTabId(String subAccessTabId) {
		this.subAccessTabId = subAccessTabId;
	}

	public String getSubAccessTabId() {
		return subAccessTabId;
	}
	
	public Map<String, Integer> getDbdataTypeMap() {
		return dbdataTypeMap;
	}

	public void setDbdataTypeMap(Map<String, Integer> dbdataTypeMap) {
		this.dbdataTypeMap = dbdataTypeMap;
	}
	public ArrayList<String> getDbindexList() {
		return dbindexList;
	}

	public void setDbindexList(ArrayList<String> dbindexList) {
		this.dbindexList = dbindexList;
	}
	public void setProfMapList(Map<Long,DataDAO> profMapList) {
		this.profMapList = profMapList;
	}

	public Map<Long,DataDAO> getProfMapList() {
		return profMapList;
	}
	
	public List<Long> getSeldbIds() {
		return seldbIds;
	}

	public void setSeldbIds(List<Long> seldbIds) {
		this.seldbIds = seldbIds;
	}
	
	public Long getSeldbId() {
		return seldbId;
	}

	public void setSeldbId(Long seldbId) {
		this.seldbId = seldbId;
	}
	
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public String getTableName1() {
		return tableName1;
	}

	public void setTableName1(String tableName1) {
		this.tableName1 = tableName1;
	}
	
	public String getWidths() {
		return widths;
	}

	public void setWidths(String widths) {
		this.widths = widths;
	}
	
	public String getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String columnNames) {
		this.columnNames = columnNames;
	}
	
	public String getColumnDescs() {
		return columnDescs;
	}

	public void setColumnDescs(String columnDescs) {
		this.columnDescs = columnDescs;
	}
	
	public String getDataTypes() {
		return dataTypes;
	}

	public void setDataTypes(String dataTypes) {
		this.dataTypes = dataTypes;
	}
	
	public String getConstraints() {
		return constraints;
	}

	public void setConstraints(String constraints) {
		this.constraints = constraints;
	}
	public int getColumnnum() {
		return columnnum;
	}

	public void setColumnnum(int columnnum) {
		this.columnnum = columnnum;
	}
	
	public int getColumnnum1() {
		return columnnum1;
	}

	public void setColumnnum1(int columnnum1) {
		this.columnnum1 = columnnum1;
	}

	public List<String> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<String> profiles) {
		this.profiles = profiles;
	}

	public List<UserDTO> getProfileGroupList() {
		return profileGroupList;
	}

	public void setProfileGroupList(List<UserDTO> profileGroupList) {
		this.profileGroupList = profileGroupList;
	}

	public String[] getSeladdedPrinterNames() {
		return seladdedPrinterNames;
	}

	public void setSeladdedPrinterNames(String[] seladdedPrinterNames) {
		this.seladdedPrinterNames = seladdedPrinterNames;
	}

	public List<String> getAddedPrinterNames() {
		return addedPrinterNames;
	}

	public void setAddedPrinterNames(List<String> addedPrinterNames) {
		this.addedPrinterNames = addedPrinterNames;
	}

	public String[] getSelprinterNames() {
		return selprinterNames;
	}

	public void setSelprinterNames(String[] selprinterNames) {
		this.selprinterNames = selprinterNames;
	}

	private String[] selprinterNames;

	public List<String> getPrinterNames() {
		return printerNames;
	}

	public void setPrinterNames(List<String> printerNames) {
		this.printerNames = printerNames;
	}

	public String getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(String selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	public List<UserDTO> getLdapUsersList() {
		return ldapUsersList;
	}

	public void setLdapUsersList(List<UserDTO> ldapUsersList) {
		this.ldapUsersList = ldapUsersList;
	}

	public String[] getUsersInGroup() {
		return usersInGroup;
	}

	public void setUsersInGroup(String[] usersInGroup) {
		this.usersInGroup = usersInGroup;
	}

	private String[] usersInGroup;

	public Map<String, List> getProfileUserGroups() {
		return profileUserGroups;
	}

	public void setProfileUserGroups(Map<String, List> profileUserGroups) {
		this.profileUserGroups = profileUserGroups;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getSubTabName() {
		return subTabName;
	}

	public void setSubTabName(String subTabName) {
		this.subTabName = subTabName;
	}

	public String getTabName() {
		return tabName;
	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
	}

	public Map<String, List> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(HashMap<String, List> userGroups) {
		this.userGroups = userGroups;
	}
	
	public Map<String, List> getUserProfiles() {
		return userProfiles;
	}

	public void setUserProfiles(HashMap<String, List> userProfiles) {
		this.userProfiles = userProfiles;
	}
	
	public String[] getProfileNames() {
		return profileNames;
	}

	public void getProfileNames(String[] profileNames) {
		this.profileNames = profileNames;
	}
	
	public Long[] getProfileIds() {
		return profileIds;
	}

	public void getProfileIds(Long[] profileIds) {
		this.profileIds = profileIds;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String[] getDbName() {
		return dbName;
	}

	public void setDbName(String[] dbName) {
		this.dbName = dbName;
	}

	public String[] getRoleName() {
		return roleName;
	}

	public void setRoleName(String[] roleName) {
		this.roleName = roleName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserGrp() {
		return userGrp;
	}

	public void setUserGrp(String userGrp) {
		this.userGrp = userGrp;
	}

	public Long[] getGroupId() {
		return groupId;
	}

	public void setGroupId(Long[] groupId) {
		this.groupId = groupId;
	}

	public Long[] getNewUserId() {
		return newUserId;
	}

	public void setNewUserId(Long[] newUserId) {
		this.newUserId = newUserId;
	}
	
	public Long[] getNewProfileId() {
		return newProfileId;
	}

	public void setNewProfileId(Long[] newProfileId) {
		this.newProfileId = newProfileId;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession() {
		return this.session;
	}

	public Long getCurrUserId() {
		return currUserId;
	}

	public void setCurrUserId(Long currUserId) {
		this.currUserId = currUserId;
	}

	public List<String> getCsrAllUsers() {
		return csrAllUsers;
	}

	public void setCsrAllUsers(List<String> csrAllUsers) {
		this.csrAllUsers = csrAllUsers;
	}

	public List<String> getCsrActions() {
		return csrActions;
	}

	public void setCsrActions(List<String> csrActions) {
		this.csrActions = csrActions;
	}

	public Long[] getActionId() {
		return actionId;
	}

	public void setActionId(Long[] actionId) {
		this.actionId = actionId;
	}

	public Long[] getDbId() {
		return dbId;
	}

	public void setDbId(Long[] dbId) {
		this.dbId = dbId;
	}

	public String getWeekend() {
		return weekend;
	}

	public void setWeekend(String weekend) {
		this.weekend = weekend;
	}

	public String getAccessdateFrom() {
		return accessdateFrom;
	}

	public void setAccessdateFrom(String accessdateFrom) {
		this.accessdateFrom = accessdateFrom;
	}

	public String getAccessdateTo() {
		return accessdateTo;
	}

	public void setAccessdateTo(String accessdateTo) {
		this.accessdateTo = accessdateTo;
	}

	public int getAccesstoDays() {
		return accesstoDays;
	}

	public void setAccesstoDays(int accesstoDays) {
		this.accesstoDays = accesstoDays;
	}

	public int getAccessfromDays() {
		return accessfromDays;
	}

	public void setAccessfromDays(int accessfromDays) {
		this.accessfromDays = accessfromDays;
	}

	public int getCurrYear() {
		return currYear;
	}

	public void setCurrYear(int currYear) {
		this.currYear = currYear;
	}

	public String getValidfromDays() {
		return validfromDays;
	}

	public void setValidfromDays(String validfromDays) {
		this.validfromDays = validfromDays;
	}

	public String getValidtoDays() {
		return validtoDays;
	}

	public void setValidtoDays(String validtoDays) {
		this.validtoDays = validtoDays;
	}

	public String getValiddateFrom() {
		return validdateFrom;
	}

	public void setValiddateFrom(String validdateFrom) {
		this.validdateFrom = validdateFrom;
	}

	public String getValiddateTo() {
		return validdateTo;
	}

	public void setValiddateTo(String validdateTo) {
		this.validdateTo = validdateTo;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Long[] getRoleId() {
		return roleId;
	}

	public void setRoleId(Long[] roleId) {
		this.roleId = roleId;
	}

	public String[] getCsrGrps() {
		return csrGrps;
	}

	public void setCsrGrps(String[] csrGrps) {
		this.csrGrps = csrGrps;
	}

	public String[] getSelcsrGrps() {
		return selcsrGrps;
	}

	public void setSelcsrGrps(String[] selcsrGrps) {
		this.selcsrGrps = selcsrGrps;
	}
	
	public List<String> getSelprGrps() {
		return selprGrps;
	}

	public void setSelprGrps(List<String> selprGrps) {
		this.selprGrps = selprGrps;
	}

	public List<String> getPrePopulateList() {
		return prePopulateList;
	}

	public void setPrePopulateList(List<String> prePopulateList) {
		this.prePopulateList = prePopulateList;
	}
	
	public Map<String,List<String>> getProfilePrePopulateList() {
		return profilePrePopulateList;
	}

	public void setProfilePrePopulateList(Map<String,List<String>> profilePrePopulateList) {
		this.profilePrePopulateList = profilePrePopulateList;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}
	
	public String getSubTabId() {
		return subtabId;
	}

	public void setSubTabId(String subtabId) {
		this.subtabId = subtabId;
	}
	
	public List<String> getCsrGroups() {
		return csrGroups;
	}

	public void setCsrGroups(List<String> csrGroups) {
		this.csrGroups = csrGroups;
	}
	
	public List<String> getPreSelCsrGroups() {
		return preSelCsrGroups;
	}

	public void setPreSelCsrGroups(List<String> preSelCsrGroups) {
		this.preSelCsrGroups = preSelCsrGroups;
	}
	
	public List<String> getPreSelAllCsrGroups() {
		return preSelAllCsrGroups;
	}

	public void setPreSelAllCsrGroups(List<String> preSelAllCsrGroups) {
		this.preSelAllCsrGroups = preSelAllCsrGroups;
	}

	public String[] getCsrUsers() {
		return csrUsers;
	}

	public void setCsrUsers(String[] csrUsers) {
		this.csrUsers = csrUsers;
	}
	
	public String[] getCsrProfiles() {
		return csrProfiles;
	}

	public void setCsrProfiles(String[] csrProfiles) {
		this.csrProfiles = csrProfiles;
	}

	public String[] getSelCsrUsers() {
		return selCsrUsers;
	}

	public void setSelCsrUsers(String[] selCsrUsers) {
		this.selCsrUsers = selCsrUsers;
	}

	public List<String> getSelCsrGroups() {
		return selCsrGroups;
	}

	public void setSelCsrGroups(List<String> selCsrGroups) {
		this.selCsrGroups = selCsrGroups;
	}

	public List getCsrRoles() {
		return csrRoles;
	}

	public void setCsrRoles(List csrRoles) {
		this.csrRoles = csrRoles;
	}

	public List getCsrDatabases() {
		return csrDatabases;
	}

	public void setCsrDatabases(ArrayList csrDatabases) {
		this.csrDatabases = csrDatabases;
	}
	
	public List<Long> getSelCsrDatabases() {
		return selCsrDatabases;
	}

	public void setSelCsrDatabases(ArrayList<Long> selCsrDatabases) {
		this.selCsrDatabases = selCsrDatabases;
	}
	
	public List<Long> getSelProfileDatabases() {
		return selProfileDatabases;
	}

	public void setSelProfileDatabases(ArrayList<Long> selProfileDatabases) {
		this.selProfileDatabases = selProfileDatabases;
	}
	
	public List getProfileList() {
		return profileList;
	}

	public void setProfileDatabases(ArrayList profileList) {
		this.profileList = profileList;
	}

	public String getSearchIndexValues() {
		return searchIndexValues;
	}

	public void setSearchIndexValues(String searchIndexValues) {
		this.searchIndexValues = searchIndexValues;
	}

	public HttpServletRequest getServletRequest() {
		return request;
	}

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getServletResponse() {
		return response;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	public String getSelProfile() {
		return selProfile;
	}

	public void setSelProfile(String selProfile) {
		this.selProfile = selProfile;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	

	public String execute() {
		UserDAO uDao = new UserDAO();
		boolean userExist = false;
		setTabId("User Access Control");
		setSubTabId("Groups");
		setSubAccessTabId("Access Groups");
		try {

			HashMap<String, String> propertiesMap = new HashMap<String, String>();
			Enumeration params = request.getParameterNames();
			String currentGrp = "";
			System.out.println("execute:AdminSubmitAction*-*");
			prePopulateList = new ArrayList<String>();
			csrGroups = new ArrayList();
			// String userId = getSession().get("userName")!=
			// null?getSession().get("userName").toString():"";
			List groups = LDAPUtils.getLDAPCSRGroups(CommonConstants.CSR_PROPERTY_FILE_NAME);
			for (Iterator iter = groups.iterator(); iter.hasNext();) {
				String group = (String) iter.next();
				Long groupId = uDao.getGroupIdbelongstoGroup(group);
				//System.out.println("execute:AdminSubmitAction*-*" + groupId);
				if (groupId != null || groupId != 0) {
					csrAllUsers = new ArrayList();
					csrAllUsers = LDAPUtils.getUsersBelongstoGroup2(group);
					int j = 0;
					for (Iterator iter1 = csrAllUsers.iterator(); iter1
							.hasNext();) {
						String str = (String) iter1.next();
					str=	LDAPUtils.getUserIDByDistinguishedName(str,"");
						userExist = uDao.getUserAccessWithGroupId(str, groupId);
						if (userExist) {
							j++;
						}
					}

					if (csrAllUsers.size() > j) {
						csrGroups.add(group);
					}
				}
			}
			logger.info("Before getting Groups:propertiesMap ------------ "
					+ propertiesMap);
			csrActions = uDao.getCsrActions();
			csrRoles = uDao.getRoles();
			csrDatabases = uDao.getDatabases();
			tabId = "User Access Control";

		} catch (Exception e) {
			e.printStackTrace();
			return "FAILURE";
		}
		return "SUCCESS";
	}

	public String ShowCreateProfile() {
		setTabId("User Profile Control");
		setSubTabId("Create Profiles");
		HttpSession sessionku = getServletRequest().getSession();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		
		System.out.println("sysaccesscontrol is"+(String)sessionku.getAttribute("sysaccesscontrol"));
		
		if(!((String)sessionku.getAttribute("sysaccesscontrol")).equalsIgnoreCase("true")){
			return "UNAUTHORIZED";
		}
		return "SUCCESS";
	}

	public String executeUpdate() {
		UserDAO uDao = new UserDAO();
		setTabId("User Access Control");
		setSubTabId("Access Control");
		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();
		try {
			HttpSession sessionku = getServletRequest().getSession();
			if (sessionku.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			//System.out.println("User executeUpdate:"+ request.getParameter("seluserId"));
			List<Long> uprofileList = (List<Long>)(getServletRequest().getSession().getAttribute("profileIdList"));
			List<UserDTO> userProfileList = new ArrayList<UserDTO>();
			Long userProfileId = 0L;
			for(Long profileId:uprofileList){
				userProfileId = profileId;
				String uprofileName = uDao.getProfileName(profileId);
				UserDTO dto = new UserDTO();
				dto.setUserProfileId(profileId);
				dto.setUserProfileName(uprofileName);
				userProfileList.add(dto);
			}
			csrActions = uDao.getCsrActions();
			csrRoles = uDao.getcsrRoles();
			csrDatabases = uDao.getProfileDatabases(userProfileId);
			List<UserDTO> dbList = new ArrayList<UserDTO>();
			for(UserDTO db:csrDatabases){
				if(uDao.isDbBelongsToProfile(userProfileId, db.getDbName()))
				{
					UserDTO dto = new UserDTO();
					dto.setDbDesc(db.getDbDesc());
					dto.setDbName(db.getDbName());
					dbList.add(dto);
				}
			}
			if (request.getParameter("seluserId") != null) {
				Long seluserId = Long.parseLong(request.getParameter(
						"seluserId").toString());
				//System.out.println("seluserId" + seluserId);
				List Users = uDao.getUser(seluserId);
				System.out.println("Before for loop" + Users.size());
				Iterator Useriter = Users.iterator();
				while( Useriter.hasNext()) {
					System.out.println("inside while loop " + seluserId);
					UserDTO dto = (UserDTO) Useriter.next();
					userGrp = dto.getUserGrp();
					getServletRequest().getSession().setAttribute("userGrp",
							userGrp);
					System.out.println("userGrp::::::" + userGrp);
					currYear = dto.getCurrYear();
					accessdateFrom = dto.getAccessdateFrom();
					accessdateTo = dto.getAccessdateTo();
					accessfromDays = dto.getAccessfromDays();
					accesstoDays = dto.getAccesstoDays();
					active = dto.getActive();
					userName = dto.getUserName();
					System.out.println("userName" + userName);
					System.out.println("active" + active);
					userId = dto.getUserId();
				}
				System.out.println("After for loop exit" + seluserId);
				List roles = uDao.getRole(seluserId);
				int i = 0;
				roleId = new Long[roles.size()];
				roleName = new String[roles.size()];
				for (Iterator iter = roles.iterator(); iter.hasNext();) {
					UserDTO dto = (UserDTO) iter.next();
					roleId[i] = dto.getRoleId();
					roleName[i] = dto.getRoleName();
					//System.out.println("roleId" + roleId[i]);
					i++;
				}
				List dbs = uDao.getDatabase(seluserId);
				System.out.println("dbSize" + dbs.size());
				dbId = new Long[dbs.size()];
				dbName = new String[dbs.size()];
				i = 0;
				for (Iterator iter = dbs.iterator(); iter.hasNext();) {
					UserDTO dto = (UserDTO) iter.next();
					dbId[i] = dto.getDbId();
					dbName[i] = dto.getDbDesc();
					System.out.println("dbId" + dbId[i]);
					i++;

				}
				List actions = uDao.getActions(seluserId);
				i = 0;
				actionId = new Long[actions.size()];

				for (Iterator iter = actions.iterator(); iter.hasNext();) {
					UserDTO dto = (UserDTO) iter.next();
					actionId[i] = dto.getActionId();

					//System.out.println("actionid" + dto.getRoleId());
					i++;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for executeUpdate(edit user) method : " + difference);
		
		return "SUCCESS";
	}
	
	

	public String updateUserGroup() {

		try {
			HashMap<String, String> propertiesMap = new HashMap<String, String>();
			Enumeration params = request.getParameterNames();
			String currentGrp = "";
			UserDAO uDao = new UserDAO();
			prePopulateList = new ArrayList<String>();

			// String userId = getSession().get("userName")!=
			// null?getSession().get("userName").toString():"";
			logger.info("Before getting Groups:propertiesMap ------------ "
					+ propertiesMap);
			// selCsrGroups = uDao.getUserAccess();
			List groups = LDAPUtils.getLDAPCSRGroups(CommonConstants.CSR_PROPERTY_FILE_NAME);
			// csrGroups
			csrRoles = uDao.getRoles();
			// uDao.getRole(userId)
			csrDatabases = uDao.getDatabases();

			searchIndexValues = PropertyUtils.getProperties(
					CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
					CommonConstants.AVAILABLE_PBIDX_VALUES);
			tabId = "User Access Control";
		} catch (Exception e) {
			e.printStackTrace();
			return "FAILURE";
		}
		return "SUCCESS";
	}

	public String applySettings() {

		UserDAO uDao = new UserDAO();
		UserDTO uDto = new UserDTO();
		System.out.println("applySettings" + getSelcsrGrps());
		try {

			selcsrGrps = getSelcsrGrps();
			roleId = getRoleId();
			dbId = getDbId();
			active = getActive();
			validdateFrom = getValiddateFrom();
			validdateTo = getValiddateTo();
			validfromDays = getValidfromDays();
			validtoDays = getValidtoDays();
			accessdateFrom = getAccessdateFrom();
			accessdateTo = getAccessdateTo();
			weekend = getWeekend();
			currYear = getCurrYear();
			accessfromDays = getAccessfromDays();
			accesstoDays = getAccesstoDays();
			actionId = getActionId();
			System.out.println("dbId:" + dbId);
			System.out.println("roleId:" + roleId);
			System.out.println("csrGrps:" + selcsrGrps);
			Long userId = 0L;

			uDto.setAccessdateFrom(accessdateFrom);
			uDto.setAccessdateTo(accessdateTo);
			uDto.setWeekend(weekend);
			uDto.setAccessfromDays(accessfromDays);
			uDto.setAccesstoDays(accesstoDays);
			uDto.setActive(active);
			uDto.setCurrYear(currYear);
			// uDto.setActionName(actionName);
			// uDto.setDbId(dbId);
			if (getSelcsrGrps() != null) {

				for (int i = 0; i < selcsrGrps.length; i++) {
					List list = uDao.getUserAccessUsingGroups(selcsrGrps[i]);
					Iterator iter = list.iterator();
					if (iter.hasNext()) {
						UserDTO udto = (UserDTO) iter.next();
						uDto.setUserId(udto.getUserId());
						// userId = uDao.saveUserPrevDetails(uDto,"update");

						for (int j = 0; j < roleId.length; j++) {
							uDao.updateUSERRole(udto.getUserId(), roleId[j]);
							// uDao.insertRole(userId, roleId[j]);
						}
						for (int j = 0; j < actionId.length; j++) {
							uDao.updateUserActions(userId, actionId[j]);
						}
						if (dbId != null) {
							// uDao.deleteUSERDB(udto.getUserId());
							for (int j = 0; j < dbId.length; j++) {
								uDao.updateDatabase(userId, dbId[j]);
							}
						}

					} else {

						System.out.println("getCsrGrps---->" + selcsrGrps[i]);
						uDto.setUserGrp(selcsrGrps[i]);
						// userId = uDao.saveUserPrevDetails(uDto,"create");
						if (roleId != null) {
							for (int j = 0; j < roleId.length; j++) {

								System.out.println("roles" + roleId[j]);
								uDao.insertRole(userId, roleId[j]);
							}
						}
						if (actionId != null) {
							for (int j = 0; j < actionId.length; j++) {
								uDao.saveUserActions(userId, actionId[j]);
							}
						}
						if (dbId != null) {
							for (int j = 0; j < dbId.length; j++) {
								uDao.insertDatabase(userId, dbId[j]);
							}
						}

					}
				}

			} else {
				setMessage("Please Select Group Name");
			}

			if (userId != 0) {
				setMessage("Permissions updated successfully!");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception is in applying settings " + e.getMessage());
			e.printStackTrace();
		}
		csrActions = uDao.getCsrActions();
		csrGroups = LDAPUtils.getLDAPCSRGroups(CommonConstants.CSR_PROPERTY_FILE_NAME);
		csrRoles = uDao.getRoles();
		csrDatabases = uDao.getDatabases();
		searchIndexValues = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.AVAILABLE_PBIDX_VALUES);
		tabId = "User Access Control";
		return "SUCCESS";
	}

	public String AssignCreateProfileGroups() {
		setTabId("Prof Assign");
		/* Nithu Alexander: 12 Aug 2015
		 * Below SubTabId set as part of Profile Admin screen Tabs changes
		 * */
		setSubTabId("User Creation");
		
		System.out.println("Tab ID ------------->"+getTabId());
		try {
			UserDAO uDao = new UserDAO();
			UserDTO uDto = new UserDTO();
			System.out.println("CreateGroups" + getSelcsrGrps());

			//selcsrGrps = getSelcsrGrps();
			csrAllUsers = new ArrayList();
			userGroups = new HashMap<String, List>();
			userProfiles = new HashMap<String, List>();
			String userName = (String) getServletRequest().getSession().getAttribute("UserName");
			Long userID = uDao.getUserIdofUser(userName);
			profileIds = uDao.getAllProfiles(userID);
			System.out.println("Profiles:::"+profileIds.length);
			for(int i=0; i<profileIds.length;i++){
				String profilename = uDao.getProfileName(profileIds[i]);
				System.out.println("Profiles Name:::"+profilename);
				selcsrGrps = uDao.getProfileGroups(profileIds[i]);
				System.out.println("Profile Groups:::"+selcsrGrps[0]);
				System.out.println("Profile Groups:::"+selcsrGrps.length);
				selprGrps = new ArrayList<String>();
				for(int m=0; m<selcsrGrps.length;m++){
					System.out.println("sel Profile Groups:::"+selcsrGrps[m]);
					selprGrps.add(selcsrGrps[m]);
				}
				userProfiles.put(profilename, selprGrps);
				
			}
			System.out.println("Profiles:::"+userProfiles.get(1));
			
			Long userId = 0L;
			List users = new ArrayList();
			boolean userExist = false;
			if (selcsrGrps != null) {
				for (int i = 0; i < selcsrGrps.length; i++) {

					System.out.println("getCsrGrps---->" + selcsrGrps[i]);
					uDto.setUserGrp(selcsrGrps[i]);
					String saveFlag = "N";
					Long groupId = uDao.getGroupIdbelongstoGroup(selcsrGrps[i]);

					System.out.println("cretaed Groups");
					csrAllUsers = new ArrayList();
					List allLdapusers = LDAPUtils.getUsersBelongstoGroup2(selcsrGrps[i]);
					int j = 0;
					for (Iterator iter = allLdapusers.iterator(); iter.hasNext();) {
						String user = (String) iter.next();
						System.out.println("user from getUsersBelongstoGroup2::"+user);
						
						String ldapuserId = LDAPUtils.getUserIDByDistinguishedName(user,"");
						System.out.println("ldapuserId::"+ldapuserId);
						userExist = uDao.getUserAccessWithGroupId(ldapuserId, groupId);
						if (userExist) {
							j++;
						} else {
							csrAllUsers.add(user);
						}
					}
					if (allLdapusers.size() == j) {
						setMessage(selcsrGrps[i]+ " has already been given permission.");
					}
			
					userGroups.put(selcsrGrps[i], csrAllUsers);
				}
			}
			
			csrActions = uDao.getCsrActions();
		tabId = "Prof Assign";
		} catch (Exception e) {
			e.printStackTrace();
			return "FAILURE";
		}
		return "SUCCESS";
	}

	public String CreateGroups() {
		try {
			UserDAO uDao = new UserDAO();
			UserDTO uDto = new UserDTO();
			System.out.println("CreateGroups" + getSelcsrGrps());
			setSubTabId("Groups");
			setTabId("User Access Control");
			setSubAccessTabId("Access Users");
			selcsrGrps = getSelcsrGrps();
			csrAllUsers = new ArrayList();
			userGroups = new HashMap<String, List>();
			System.out.println("selcsrGrps:" + selcsrGrps);
			Long userId = 0L;
			List users = new ArrayList();
			boolean userExist = false;
			if (getSelcsrGrps() != null) {
				for (int i = 0; i < selcsrGrps.length; i++) {

					System.out.println("getCsrGrps---->" + selcsrGrps[i]);
					uDto.setUserGrp(selcsrGrps[i]);
					String saveFlag = "N";
					Long groupId = uDao.getGroupIdbelongstoGroup(selcsrGrps[i]);

					if (groupId != null || groupId != 0) {
						System.out.println("groupId" + groupId);
						uDao.saveTempUsergrpDetails1(uDto, "create", saveFlag);
					}
					System.out.println("cretaed Groups");
					csrAllUsers = new ArrayList();
					List allLdapusers = LDAPUtils
							.getUsersBelongstoGroup2(selcsrGrps[i]);
					int j = 0;
					for (Iterator iter = allLdapusers.iterator(); iter
							.hasNext();) {
						String user = (String) iter.next();
						
						user=(LDAPUtils.getUserIDByDistinguishedName(user,""));
						userExist = uDao
								.getUserAccessWithGroupId(user, groupId);
						if (userExist) {
							j++;
						} else {
							csrAllUsers.add(user);
						}
					}
					if (allLdapusers.size() == j) {
						setMessage(selcsrGrps[i]
								+ " has already given permission.");
					}
					// .getUsersBelongstoGroup(selcsrGrps[i]);//,PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_ADMIN_USER_ID),
					// PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_ADMIN_PWD),
					// CommonConstants.CSR_PROPERTY_FILE_NAME);
					/*
					 * for(Iterator iter=users.iterator();iter.hasNext();) {
					 * csrAllUsers.add((String)iter.next()); }
					 */

					userGroups.put(selcsrGrps[i], csrAllUsers);

				}

			}

			// csrActions = uDao.getCsrActions();
			// csrGroups =
			// LDAPUtils.getLDAPCSRGroups(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_ADMIN_USER_ID),
			// PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_ADMIN_PWD),
			// CommonConstants.CSR_PROPERTY_FILE_NAME,
			// PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_ADMIN_GROUP_NAME));

			// csrRoles = uDao.getRoles();
			// csrDatabases = uDao.getDatabases();
			// searchIndexValues =
			// PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.AVAILABLE_PBIDX_VALUES);
			tabId = "User Access Control";
		} catch (Exception e) {
			e.printStackTrace();
			return "FAILURE";
		}
		return "SUCCESS";
	}

	

	public String CreateUsers() {
		Long userId = 0l;
		try {
			UserDAO uDao = new UserDAO();
			UserDTO uDto = new UserDTO();
			setTabId("User Access Control");
			setSubTabId("Groups");
			setSubAccessTabId("Access Settings");
			System.out.println("CreateUsers" + getSelCsrUsers());
			System.out.println("CreateUsers:CSRUSERS" + getCsrUsers());
			csrAllUsers = new ArrayList();
			selcsrGrps = getSelcsrGrps();
			csrUsers = getCsrUsers();
			selCsrUsers = getSelCsrUsers();
			groupId = getGroupId();

			if (csrUsers != null)
				selCsrUsers = csrUsers;
			System.out.println(getCsrGroups() + "selCsrUsers:" + selCsrUsers);
			if (selCsrUsers != null) {
				newUserId = new Long[selCsrUsers.length];
				for (int i = 0; i < selCsrUsers.length; i++) {

					System.out.println("selCsrUsers---->" + selCsrUsers[i]);

					String saveFlag = "N";
					String[] split = selCsrUsers[i].split(",");
					String groupName = LDAPUtils.authenticateGroup(split[0],
							CommonConstants.CSR_PROPERTY_FILE_NAME);
					Long groupId = uDao.getGroupIdbelongstoGroup(split[0]);
					String ldapuserId = LDAPUtils.getUserIDByDistinguishedName(split[1],"");
					System.out.println("ldapuserId::"+ldapuserId);
					uDto.setUserName(ldapuserId);
					Long newuserId = uDao.createTempuserMgmt1(uDto, "create",
							saveFlag, groupId);
					System.out.println("allusers created success fully");
					newUserId[i] = newuserId;
					csrAllUsers.add(selCsrUsers[i] + ";" + newuserId);

				}

			} else {
				setMessage("Please Select User Name");
			}
			//System.out.println("allusers");
			csrActions = uDao.getCsrActions();
			csrRoles = uDao.getRoles();
			csrDatabases = uDao.getDatabases();

			// csrActions = uDao.getCsrActions();
			// csrGroups =
			// LDAPUtils.getLDAPCSRGroups(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_ADMIN_USER_ID),
			// PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_ADMIN_PWD),
			// CommonConstants.CSR_PROPERTY_FILE_NAME,
			// PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_ADMIN_GROUP_NAME));
			// csrUsers =
			// LDAPUtils.getLDAPCSRUsers(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_ADMIN_USER_ID),
			// PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_ADMIN_PWD),CommonConstants.CSR_PROPERTY_FILE_NAME);
			// csrRoles = uDao.getRoles();
			// csrDatabases = uDao.getDatabases();
			// searchIndexValues =
			// PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.AVAILABLE_PBIDX_VALUES);
			tabId = "User Access Control";
		} catch (Exception e) {
			return "FAILURE";
		}
		return "SUCCESS";
	}
	
	public String populateProfileDbSettings() {
		System.out.println("Inside populateProfileDbSettings");
		setTabId("User Profile Control");
		setSubTabId("Create Profiles");
		UserDAO uDao = new UserDAO();
		
		try {
			HttpSession sessionku = getServletRequest().getSession();
			if (sessionku.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			profileName = getProfileName();
			profileAction = getProfileAction();
			
			/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1 and SCT (VaultApp)*/
			
			if(profileAction!=null && !(profileAction.trim().equals("")) && !ServerSideValidationUtil.isAlpha(profileAction)){
				return "INVALID_PARAMETERS";
			}
			/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1 and SCT (VaultApp)
			 * Commenting below code of setting profileName and profileAction in session
			 * Adding profileName and profileAction in session after cross-site scripting validation*/
			
			//sessionku.setAttribute("profileName", profileName);
			//sessionku.setAttribute("profileAction", profileAction);
			
			if(profileAction.equalsIgnoreCase("create")){
				if (profileName == null || profileName.equalsIgnoreCase("")) {
					setMessage("Profile Name cannot be empty");
					tabId = "User Access Control";
					return "INVALID_PROFILENAME";
				}
				
				/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1 and SCT (VaultApp)*/
				if(profileName!=null && !(profileName.trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithSpaceAndUnderscoreDOT(profileName)){
					setMessage("Invalid Characters in Profile Name. Please Re-Enter a New Profile Name");
					tabId = "User Access Control";
					return "INVALID_PROFILENAME";
				}
				Long profileId = uDao.isProfileExists(profileName);

				if(profileId != 0)
				{
					setMessage("Profile Name already exists, please give another name!!!");
					tabId = "User Access Control";
					return "INVALID_PROFILENAME";
				}
				csrDatabases = uDao.getDatabases();
				getServletRequest().getSession().setAttribute("profileName",profileName);
			}
			sessionku.setAttribute("profileName", profileName);
			sessionku.setAttribute("profileAction", profileAction);
			
		}

		catch (Exception e) {
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}
		//System.out.println("Returning from populateProfileDbSettings");
		return "SUCCESS";
	}
	
	@SuppressWarnings("unchecked")
	public String saveProfileDbSettings() {
		System.out.println("Inside saveProfileDbSettings");
		UserDAO uDao = new UserDAO();
		
		try {
			HttpSession session = getServletRequest().getSession();
			if (session.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			profileName = session.getAttribute("profileName").toString();
			profileAction = session.getAttribute("profileAction").toString();
			if(profileAction.equalsIgnoreCase("create")){
				setTabId("User Profile Control");
				setSubTabId("Create Profiles");
			}
			else if(profileAction.equalsIgnoreCase("edit")){
				setTabId("User Profile Control");
				setSubTabId("View Profiles");
			}
			/* Nithu Alexander: 24/10/2014 Changes made as part of BACK Button issue*/
			List<String> dbIdList = new ArrayList<String>();
			List<String> selectedDBListEdit = new ArrayList<String>();
			List<String> selectedDBListCreate = new ArrayList<String>();
			
			if(request.getParameter("backSubmitFlag") != null && request.getParameter("backSubmitFlag").equals("editBackSubmit") && profileAction.equalsIgnoreCase("edit")){
				csrDatabases = (List<UserDTO>)session.getAttribute("csrDatabases");
				selCsrDatabases = (List<Long>)session.getAttribute("selCsrDatabases");
				selProfileDatabases = (List<Long>)session.getAttribute("selProfileDatabases");
				selectedDBListEdit = (List<String>)session.getAttribute("selectedDBList");
				dbIdList = selectedDBListEdit;
				System.out.println("----> selectedDBListEdit"+selectedDBListEdit.toString());
				
			}else if(request.getParameter("backSubmitFlag") != null && request.getParameter("backSubmitFlag").equals("createBackSubmit") && profileAction.equalsIgnoreCase("create")){
				csrDatabases = (List<UserDTO>)session.getAttribute("csrDatabases");
				selCsrDatabases = (List<Long>)session.getAttribute("selCsrDatabases");
				selProfileDatabases = (List<Long>)session.getAttribute("selProfileDatabases");
				selectedDBListCreate = (List<String>)session.getAttribute("selectedDBList");
				dbIdList = selectedDBListCreate;
				System.out.println("----> selectedDBListCreate"+selectedDBListCreate.toString());
				
			}else{
			
				Enumeration<?> params = (Enumeration<?>)request.getParameterNames();
				//List<String> dbIdList = new ArrayList<String>();
				while (params.hasMoreElements()) {
					String paramName = params.nextElement().toString();
					if (paramName.startsWith("dbId_")) {
						String selectedCheckBoxValue = request.getParameter(paramName);
						if (!selectedCheckBoxValue.equalsIgnoreCase("unchecked")) {
							dbIdList.add(selectedCheckBoxValue);
							System.out.println("----> dbIdList"+dbIdList.toString());
							session.setAttribute("selectedDBList", dbIdList);
						}
					}
				}
			}
			System.out.println("----> Final dbIdList"+dbIdList.toString());
		
			
			seldb = new ArrayList<UserDTO>();
			for (String tdbId:dbIdList) {
				long dbId = Long.parseLong(tdbId);
				String dbName = uDao.getDBName(dbId);
				String dbDesc = uDao.getDBDesc(dbId);
				UserDTO udto = new UserDTO();
				udto.setDbId(dbId);
				udto.setDbName(dbName);
				udto.setDbDesc(dbDesc);
				System.out.println("Added"+dbName);
				seldb.add(udto);
			}
			session.setAttribute("seldb", seldb);
			profileDetailIndex = new HashMap<String, Map<String,Long>>();
			profilePrePopulateList = new HashMap<String, List<String>>();
			
			Iterator<UserDTO> dbItrer = seldb.iterator();
			while (dbItrer.hasNext()) {
				UserDTO uDto = (UserDTO) dbItrer.next();
				detailIndex = new HashMap<String,Long>();
				prePopulateList = new ArrayList<String>();
				Long databaseId = uDto.getDbId();
				String databaseName = uDto.getDbName();
				detailIndex = uDao.getIndex(databaseId);
				Long profileId = uDao.isProfileExists(profileName);
				if(profileId != 0){
					if (databaseId != 0) {
						prePopulateList = uDao.getProfileDbIndex(databaseId,profileId);
					}
				}
				profileDetailIndex.put(databaseName,detailIndex);
				profilePrePopulateList.put(databaseName, prePopulateList);
			}
			session.setAttribute("profileDetailIndex", profileDetailIndex);
			session.setAttribute("profilePrePopulateList", profilePrePopulateList);
			
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}
		System.out.println("profileDetailIndex::"+profileDetailIndex);
		System.out.println("profilePrePopulateList::"+profilePrePopulateList);
		System.out.println("Returning from saveProfileDbSettings");
		return "SUCCESS";

	}
	
	public String saveProfileGroupSettings() {
		
		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();
		
		System.out.println("Inside saveProfileGroupSettings");
		UserDAO uDao = new UserDAO();
		try {
			HttpSession session = getServletRequest().getSession();
			if (session.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			profileName = session.getAttribute("profileName").toString();
			profileAction = session.getAttribute("profileAction").toString();
			System.out.println("profileName::"+profileName+"--profileAction::"+profileAction);
			if(session.getAttribute("profileId")!=null){
				profileId = Long.parseLong(session.getAttribute("profileId").toString());
			}
			if(profileAction.equalsIgnoreCase("create")){
				setTabId("User Profile Control");
				setSubTabId("Create Profiles");
			}
			else if(profileAction.equalsIgnoreCase("edit")){
				setTabId("User Profile Control");
				setSubTabId("View Profiles");
			}
			seldb = (ArrayList<UserDTO>)session.getAttribute("seldb");
			prdbIndex = new HashMap<String,List<String>>();
			Iterator<UserDTO> dbItrer = seldb.iterator();
			while (dbItrer.hasNext()) {
				UserDTO uDto = (UserDTO) dbItrer.next();
				System.out.println("databaseName::"+uDto.getDbName());
				List<String> dbIdxList = new ArrayList<String>();
				String dbName = "";
				Enumeration<?> params = (Enumeration<?>)request.getParameterNames();
				while (params.hasMoreElements()) {
					String paramName = params.nextElement().toString();
					if (paramName.startsWith("Index_")) {
						String selectedCheckBoxValue = request.getParameter(paramName);
						if (!selectedCheckBoxValue.equalsIgnoreCase("unchecked")) {
							dbName = paramName.split("_")[1];
							if(dbName.equalsIgnoreCase(uDto.getDbName())){
								dbIdxList.add(paramName.split("_")[2]+"_"+paramName.split("_")[3]+"_checked");
							}

						}else{
							dbName = paramName.split("_")[1];
							if(dbName.equalsIgnoreCase(uDto.getDbName())){
								dbIdxList.add(paramName.split("_")[2]+"_"+paramName.split("_")[3]+"_unchecked");
							}
						}
					}
				}
				prdbIndex.put(uDto.getDbName(), dbIdxList);
			}
			System.out.println("prdbIndex::"+prdbIndex);
			session.setAttribute("prdbIndex", prdbIndex);
			csrGroups = new ArrayList<String>();
			csrGroups = LDAPUtils.getLDAPCSRGroups(CommonConstants.CSR_PROPERTY_FILE_NAME);
			if(profileId!=null && profileAction.equalsIgnoreCase("edit")){
				preSelCsrGroups = uDao.getGroups(profileId);
				preSelAllCsrGroups = uDao.getpGroups(profileId);
			List<String> tmp= uDao.getpGroups(profileId);
				try
				{
				/*if(preSelCsrGroups.contains(preSelAllCsrGroups.get(0)))
				{
					preSelAllCsrGroups=preSelCsrGroups;
				}
				
				else
				{*/
					preSelAllCsrGroups.addAll(preSelCsrGroups);
					removeDuplicates(preSelAllCsrGroups);
				//}
				}
				catch(Exception e)
				{
					preSelAllCsrGroups.addAll(preSelCsrGroups);
				}
				
				List<String> temp = csrGroups;
				csrGroups = new ArrayList<String>();
				for(String group: temp){
					if(!preSelAllCsrGroups.contains(group)){
						csrGroups.add(group);
					}
				}
				preSelCsrGroups=tmp;
				session.setAttribute("preSelCsrGroups", preSelCsrGroups);
				session.setAttribute("preSelAllCsrGroups", preSelAllCsrGroups);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for saveProfileGroupSettings method : " + difference);
		
		return "SUCCESS";
	}
	
	
	public static <T> void removeDuplicates(List  <T> list)
	{
		HashSet<T> h=new HashSet<T>(list);
		list.clear();
		list.addAll(h);
	}
	
	public String CreateGroupUsersForProfiles() {
		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();
		
		System.out.println("Inside CreateGroupUsersForProfiles");
		UserDAO uDao = new UserDAO();
		UserDTO uDto = new UserDTO();
		String tempSelCsrGrps[] = null;
		setTabId("User Profile Control");
		setSubTabId("Create Profiles");
		try {
			HttpSession session = getServletRequest().getSession();
			if (session.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			selcsrGrps = getSelcsrGrps();
			tempSelCsrGrps = selcsrGrps;
			
			/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1 and SCT (VaultApp)*/
			if(getSelcsrGrps() != null && tempSelCsrGrps != null){
				for(int k=0; k < tempSelCsrGrps.length; k++){
					if(tempSelCsrGrps[k]!=null && !(tempSelCsrGrps[k].trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithSpaceAndUnderscore(tempSelCsrGrps[k])){
						setMessage("Selected Group has Invalid Characters");
						tabId = "User Profile Control";
						return "INVALID_PARAMETERS";
					}
				}
			}
			
			getServletRequest().getSession().setAttribute("selcsrGrps",selcsrGrps);
			csrAllUsers = new ArrayList();
			userGroups = new HashMap<String, List>();
			boolean userExist = false;
			if (getSelcsrGrps() != null) {
				for (int i = 0; i < selcsrGrps.length; i++) {
					uDto.setUserGrp(selcsrGrps[i]);
					String saveFlag = "N";
					Long groupId = uDao.getGroupIdbelongstoGroup(selcsrGrps[i]);

					if (groupId != null || groupId != 0) {
						groupId = uDao.saveTempUsergrpDetails1(uDto, "create",saveFlag);
					}
					csrAllUsers = new ArrayList<String>();
					List allLdapusers = LDAPUtils.getUsersBelongstoGroup2(selcsrGrps[i]);
					int j = 0;
					for (Iterator iter = allLdapusers.iterator(); iter.hasNext();) {
						String user = (String) iter.next();
						String ldapuserId = LDAPUtils.getUserIDByDistinguishedName(user,"");
						//
						userExist = uDao.getUserAccessWithGroupId(ldapuserId, groupId);
						if (userExist) {
							j++;
						} else {
							csrAllUsers.add(user);
						}
					}
					if (allLdapusers.size() == j) {
						setMessage(selcsrGrps[i]+ " has already given permission.");
					}
					userGroups.put(selcsrGrps[i], csrAllUsers);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for CreateGroupUsersForProfiles method : " + difference);
		
		return "SUCCESS";
	}
	
	public String CreateProfileProjectAdmin() {
		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();
		System.out.println("Inside CreateProfileProjectAdmin");
		UserDAO uDao = new UserDAO();
		UserDTO uDto = new UserDTO();
		
		setTabId("User Profile Control");
		setSubTabId("Create Profiles");
		
		try {
			HttpSession session = getServletRequest().getSession();
			if (session.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			
			String profileName = (String) session.getAttribute("profileName");
			Long userId = 0L;
			seldb = (List<UserDTO>) session.getAttribute("seldb");
			selcsrGrps = (String[]) session.getAttribute("selcsrGrps");
			prdbIndex = (HashMap<String, List<String>>)session.getAttribute("prdbIndex");
			System.out.println("prdbIndex::"+prdbIndex);
						
			Long pid = uDao.createProfile(profileName);
			System.out.println("Created profile");
			
			createProfileUsers();
			System.out.println("Created profile Admin");
			
			for (Iterator<UserDTO> iter = seldb.iterator(); iter.hasNext();) {
				UserDTO db = (UserDTO) iter.next();
				for(String dbname:prdbIndex.keySet()){
					if(db.getDbName().equalsIgnoreCase(dbname)){
						List<String> dbIdxList = new ArrayList<String>();
						dbIdxList = prdbIndex.get(dbname);
						for(String idx:dbIdxList){
							Long idxId = Long.parseLong(idx.split("_")[0]);
							String idxName = idx.split("_")[1];
							String updated = idx.split("_")[2];
							Long indexId = uDao.checkprofileDbIdx(pid,db.getDbId(),idxId);
							if(indexId == 0){
								if(updated.equalsIgnoreCase("checked")){
									uDao.insertProfileDbIndex(pid,db.getDbId(),dbname,idxId,idxName,"Y");
								}
							}
							else{
								if(updated.equalsIgnoreCase("checked")){
									Long b = uDao.updateProfileDbIndex(pid,db.getDbId(),dbname,idxId, "Y");
								}
								else if(updated.equalsIgnoreCase("unchecked")){
									Long b = uDao.updateProfileDbIndex(pid,db.getDbId(),dbname,idxId, "N");
								}
							}
								
						}
					}
				}
			}
			System.out.println("Created ProfileDbIndex");
			String adminNames = "";
			if (selcsrGrps != null) {
				for (int i = 0; i < selcsrGrps.length; i++) {
					Long groupId = uDao.getGroupIdbelongstoGroup(selcsrGrps[i]);
					for (int k = 0; k < selCsrUsers.length; k++){
						String[] split1 = selCsrUsers[k].split(",");
						String ldapuserId = LDAPUtils.getUserIDByDistinguishedName(split1[1],"");
						Long UserId = uDao.getUserIdofUser(ldapuserId);
						String admin_flag = "Y";
						for (Iterator<UserDTO> iter1 = seldb.iterator(); iter1.hasNext();) {
							UserDTO db = iter1.next();
							uDao.createProfileGroup(pid,db.getDbId(), groupId, selcsrGrps[i], UserId,admin_flag);
							uDao.insertDatabase(UserId, db.getDbId());
						}
						
					}
					
				}

			} else {
				setMessage("Please Select Group Name");
			}
			System.out.println("Created Profile Groups");
			if(adminNames.lastIndexOf(',')!=-1){
				adminNames = adminNames.substring(0, adminNames.lastIndexOf(','));
			}
			setMessage("Profile " + profileName + " successfully created");
		} catch (Exception e) {
			logger.error("Exception is in applying settings " + e.getMessage());
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for CreateProfileProjectAdmin method : " + difference);
		
		return "SUCCESS";

	}
	
	public void createProfileUsers() throws Exception{
		System.out.println("Inside createProfileUsers");
		UserDAO uDao = new UserDAO();
		UserDTO uDto = new UserDTO();
		csrAllUsers = new ArrayList();
		selcsrGrps = (String[]) getServletRequest().getSession().getAttribute("selcsrGrps");
		csrUsers = getCsrUsers();
		selCsrUsers = getSelCsrUsers();
		if (csrUsers != null){
				selCsrUsers = csrUsers;
		}
		if (selCsrUsers != null) {
			newUserId = new Long[selCsrUsers.length];
			Long profileadminRoleId = getProfileAdminRoleId();
			for (int i = 0; i < selCsrUsers.length; i++) {
				String saveFlag = "Y";
				String[] split = selCsrUsers[i].split(",");
				Long userId = uDao.getUserIdofUser(split[1]);
				if(userId ==0L){
					Long groupId = uDao.getGroupIdbelongstoGroup(split[0]);
					String ldapuserId = LDAPUtils.getUserIDByDistinguishedName(split[1],"");
					uDto.setUserName(ldapuserId.toLowerCase());
					Properties properties = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);
					uDto.setAccessdateFrom(properties.getProperty("CSR_ADMIN_accessdateFrom"));
					uDto.setAccessdateTo(properties.getProperty("CSR_ADMIN_accessdateTo"));
					uDto.setAccessfromDays(Integer.parseInt(properties.getProperty("CSR_ADMIN_accessfromDays")));
					uDto.setAccesstoDays(Integer.parseInt(properties.getProperty("CSR_ADMIN_accesstoDays")));
					uDto.setActive("Active");
					
					Long newuserId = uDao.createTempuserMgmt1(uDto, "create",saveFlag, groupId);
					newUserId[i] = newuserId;
					csrAllUsers.add(selCsrUsers[i] + ";" + newuserId);
					
					uDto.setUserId(newuserId);
					userManager.saveUserPrevDetails(uDto, "update", "Y");
					userManager.insertRole(newuserId, profileadminRoleId);
				}
			}
		} else {
			setMessage("Please Select User Name");
		}
	}
	
	public String listProfiles() {
		System.out.println("Inside listProfiles");
		setTabId("User Profile Control");
		setSubTabId("View Profiles");
		UserDAO uDao = new UserDAO();
		try {
			HttpSession sessionku = getServletRequest().getSession();
			if (sessionku.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			
			if(!((String)sessionku.getAttribute("sysaccesscontrol")).equalsIgnoreCase("true"))
			{
		return "UNAUTHORIZED";
			}
			profiles = new ArrayList();
			profileGroupList = new ArrayList();
			profileList = new ArrayList();
			
			profiles = uDao.getProfiles();
			List<UserDTO> profiles = uDao.getProfileList();
			Iterator iter = profiles.iterator();
			while (iter.hasNext()) {
				UserDTO uDto = (UserDTO) iter.next();
				String profileGroup = uDao.getProfileGroup(uDto.getProfileId());
				uDto.setProfileGroup(profileGroup);
				String profiledb = uDao.getProfileDb(uDto.getProfileId());
				uDto.setProfileDb(profiledb);
				String profileAdmin = uDao.getProfileAdminUserName(uDto.getProfileId());
				uDto.setProfileAdmin(profileAdmin);
				String profileUsers = uDao.getProfileUsers(uDto.getProfileId());
				uDto.setProfileUsers(profileUsers);
				profileList.add(uDto);
			}
									
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}
		return "SUCCESS";

	}
	
	

	/***
	 * Start Added by Isuru
	 * @return
	 */
	
	
	public String UpdateProfileAdmin()
	{
		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();
		System.out.println("Size is"+csrUsers.length);
		System.out.println("Selected user is"+csrUsers[0]);
		
		UserDAO uDao=new UserDAO();
		UserDTO uDto=new UserDTO();

		String newprofileadminloginid=	LDAPUtils.getUserIDByDistinguishedName(csrUsers[0].split(",")[1], "");
			
			
	boolean success = uDao.updateProfileAdminBatch(newprofileadminloginid, profileId);
	if(success)
		setMessage("Profile Admin was changed successfully "+ csrUsers[0].split(",")[1]);
	else{
		setMessage("Unable to update Profile Admin. Please contact administrator");
	}
		
		
		profiles = new ArrayList();
		profileGroupList = new ArrayList();
		profileList = new ArrayList();
		
		profiles = uDao.getProfiles();
		List<UserDTO> profiles = uDao.getProfileList();
		Iterator iter = profiles.iterator();
		while (iter.hasNext()) {
			uDto = (UserDTO) iter.next();
			String profileGroup = uDao.getProfileGroup(uDto.getProfileId());
			uDto.setProfileGroup(profileGroup);
			String profiledb = uDao.getProfileDb(uDto.getProfileId());
			uDto.setProfileDb(profiledb);
			String profileAdmin = uDao.getProfileAdminUserName(uDto.getProfileId());
			uDto.setProfileAdmin(profileAdmin);
			String profileUsers = uDao.getProfileUsers(uDto.getProfileId());
			uDto.setProfileUsers(profileUsers);
			profileList.add(uDto);
		}
		
		setTabId("User Profile Control");
		setSubTabId("View Profiles");
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for UpdateProfileAdmin method : " + difference);
		
		return "SUCCESS";
	}
	
	
	
	
	
	
	public String editProfileAdmin()
	{setTabId("User Profile Control");
	setSubTabId("View Profiles");
	long endTime;
	long difference;
	long startTimeInitial;
	startTimeInitial =  System.currentTimeMillis();
	
	try {
		HttpSession sessionku = getServletRequest().getSession();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}	
			System.out.println("Profile admin is"+profileAdmin);
			System.out.println("Pofileid id"+profileId);
			
			
			UserDAO uDao = new UserDAO();
			UserDTO uDto=new UserDTO();
			
			
			
			selcsrGrps = uDao.getProfileGroups(profileId);
			getServletRequest().getSession().setAttribute("selcsrGrps",selcsrGrps);
			csrAllUsers = new ArrayList();
			userGroups = new HashMap<String, List>();
			boolean userExist = false;
			if (getSelcsrGrps() != null) {
				for (int i = 0; i < selcsrGrps.length; i++) {
					uDto.setUserGrp(selcsrGrps[i]);
					String saveFlag = "N";
					Long groupId = uDao.getGroupIdbelongstoGroup(selcsrGrps[i]);

					if (groupId != null || groupId != 0) {
						groupId = uDao.saveTempUsergrpDetails1(uDto, "create",saveFlag);
					}
					csrAllUsers = new ArrayList<String>();
					List allLdapusers = LDAPUtils.getUsersBelongstoGroup2(selcsrGrps[i]);
					int j = 0;
					for (Iterator iter = allLdapusers.iterator(); iter.hasNext();) {
						String user = (String) iter.next();
						String ldapuserId = LDAPUtils.getUserIDByDistinguishedName(user,"");
						userExist = uDao.getUserAccessWithGroupId(ldapuserId, groupId);
						boolean inProfile=uDao.isUserInProfile(ldapuserId, profileId);
						System.out.println("In profile"+inProfile);
						/*if (userExist) {
							j++;
						} else {*/
						if(userExist&& inProfile)
							csrAllUsers.add(user);
						//}
					}
					if (allLdapusers.size() == j) {
						//setMessage(selcsrGrps[i]+ " has already given permission.");
					}
					userGroups.put(selcsrGrps[i], csrAllUsers);
				}
			
			
			
			
			}
			
			
			
			
			
			
			
			
			
			
			
			
			//setMessage("Profile admin is"+profileAdmin);
			
			
			
			
			
	} catch (Exception e) {
		e.printStackTrace();
	}
	endTime =  System.currentTimeMillis();
	difference = endTime-startTimeInitial;
	System.out.println("********* Elapsed milli seconds - Total time taken for editProfileAdmin method : " + difference);
	return "SUCCESS";
	
}
	
	

	/***
	 * End Added by Isuru
	 * @return
	 */
	
	public String DeleteProfile() {
		System.out.println("Inside DeleteProfile");
		setTabId("User Profile Control");
		setSubTabId("View Profiles");
		UserDAO uDao = new UserDAO();
		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();
		try {
			HttpSession sessionku = getServletRequest().getSession();
			if (sessionku.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			
			boolean b = false;
			boolean userExists= false;
			System.out.println("Number of profile selected to delete::"+getNewProfileId().length);
			if (getNewProfileId() != null) {
				for (int i = 0; i < getNewProfileId().length; i++) {
					String profile = uDao.getProfileName(getNewProfileId()[i]);
					Long profileAdmin = uDao.getProfileAdmin(getNewProfileId()[i]);
					List userList = (ArrayList) uDao.getAllProfileUsers(profile);
					if(userList.size()>0){
						setMessage("You cannot delete a Profile which have users assigned to the profile!!!");
						userExists = true;
						endTime =  System.currentTimeMillis();
						difference = endTime-startTimeInitial;
						System.out.println("********* Elapsed milli seconds - Total time taken for DeleteProfile method : " + difference);
						return "SUCCESS";
					}
					if(userExists != true){
						uDao.removeprofileIndexes(getNewProfileId()[i]);
						boolean b1 = uDao.deleteUser(profileAdmin);
						if(b1){
							b = uDao.deleteProfile(getNewProfileId()[i]);
						}
					}
				}
			}
			if (b) {
				setMessage("Selected profiles successfully deleted");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for DeleteProfile method : " + difference);
		
		return "SUCCESS";
	}
	
	public String updateProfile() {
		System.out.println("Inside updateProfile");
		setTabId("User Profile Control");
		setSubTabId("View Profiles");
		UserDAO uDao = new UserDAO();

		try {
			HttpSession sessionku = getServletRequest().getSession();
			if (sessionku.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			if (request.getParameter("selprofileId") != null) {
				Long selprofileID = Long.parseLong(request.getParameter("selprofileId").toString());
				System.out.println("selected profileId::"+selprofileID);
				String profile = uDao.getProfileName(selprofileID);
				setProfileName(profile);
				setProfileAction("edit");
				setProfileId(selprofileID);
				sessionku.setAttribute("profileName", profileName);
				sessionku.setAttribute("profileAction", "edit");
				sessionku.setAttribute("profileId", selprofileID);
				csrDatabases = uDao.getDatabases();
				selProfileDatabases = uDao.getProfiledbIds(selprofileID);
				selCsrDatabases = uDao.getProfileuserdbIds(selprofileID);
				sessionku.setAttribute("selProfileDatabases", selProfileDatabases);
				sessionku.setAttribute("selCsrDatabases", selCsrDatabases);
				
				/* Nithu Alexander : 24/10/2014. Putting csrDatabases in session as part of BACK Button issue fix*/
				sessionku.setAttribute("csrDatabases", csrDatabases);
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}
		return "SUCCESS";
	}
	
	public String ApplyProfileSettings() {
		System.out.println("Inside ApplyProfileSettings");
		UserDAO uDao = new UserDAO();
		UserDTO uDto = new UserDTO();
		
		setTabId("User Profile Control");
		setSubTabId("View Profiles");
		
		try {
			HttpSession session = getServletRequest().getSession();
			if (session.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			
			String profileName = (String) session.getAttribute("profileName");
			String profileAction = (String) session.getAttribute("profileAction");
			Long pid = Long.parseLong(session.getAttribute("profileId").toString());
			Long userId = 0L;
			seldb = (List<UserDTO>) session.getAttribute("seldb");
			selcsrGrps = getSelcsrGrps();
			preSelAllCsrGroups = (List<String>)session.getAttribute("preSelAllCsrGroups");
			preSelCsrGroups = (List<String>)session.getAttribute("preSelCsrGroups");
			
			prdbIndex = (HashMap<String, List<String>>)session.getAttribute("prdbIndex");
			selProfileDatabases = (List<Long>)session.getAttribute("selProfileDatabases");
			
			System.out.println("prdbIndex::"+prdbIndex);
			System.out.println("preSelAllCsrGroups::"+preSelAllCsrGroups);
			System.out.println("preSelCsrGroups::"+preSelCsrGroups);
			System.out.println("selcsrGrps::"+selcsrGrps);
			System.out.println("seldb::"+seldb);
						
			for (Iterator<UserDTO> iter = seldb.iterator(); iter.hasNext();) {
				UserDTO db = (UserDTO) iter.next();
				for(String dbname:prdbIndex.keySet()){
					if(db.getDbName().equalsIgnoreCase(dbname)){
						List<String> dbIdxList = new ArrayList<String>();
						dbIdxList = prdbIndex.get(dbname);
						for(String idx:dbIdxList){
							Long idxId = Long.parseLong(idx.split("_")[0]);
							String idxName = idx.split("_")[1];
							String updated = idx.split("_")[2];
							Long indexId = uDao.checkprofileDbIdx(pid,db.getDbId(),idxId);
							if(indexId == 0){
								if(updated.equalsIgnoreCase("checked")){
									uDao.insertProfileDbIndex(pid,db.getDbId(),dbname,idxId,idxName,"Y");
								}
							}
							else{
								if(updated.equalsIgnoreCase("checked")){
									Long b = uDao.updateProfileDbIndex(pid,db.getDbId(),dbname,idxId, "Y");
								}
								else if(updated.equalsIgnoreCase("unchecked")){
									Long b = uDao.updateProfileDbIndex(pid,db.getDbId(),dbname,idxId, "N");
								}
							}
								
						}
					}
				}
			}
			System.out.println("Updated ProfileDbIndex");
			String adminNames = "";
			String admin_flag = "Y";
			String profileAdmin = uDao.getProfileAdminUserName(pid);
			Long UserId = uDao.getUserIdofUser(profileAdmin);
							
			List<String> selectedGroups = new ArrayList<String>();
			if (selcsrGrps != null) {
				
				for(String group:selcsrGrps){
					selectedGroups.add(group);
				}
				//Append existing user groups to selected groups
				for(String grp:preSelCsrGroups){
					selectedGroups.add(grp);
				}
				if (preSelAllCsrGroups != null) {
					for(String group:selcsrGrps){
						if(group!=null){
							if(!preSelAllCsrGroups.contains(group)){
								
								Long groupId = uDao.getGroupIdbelongstoGroup(group);
								if(groupId==0l){
									groupId=uDao.createGroup(group);
								}
								for (Long dbId: selProfileDatabases) {
										uDao.createProfileGroup(pid,dbId, groupId, group, UserId, admin_flag);
								}
							}
						}
					}
					for (String group1:preSelAllCsrGroups) {
						if(!selectedGroups.contains(group1)){
							Long groupId = uDao.getGroupIdbelongstoGroup(group1);
							if(groupId==0l){
								groupId=uDao.createGroup(group1);
							}
							uDao.deleteGroupFromProfile(pid, groupId, group1);
						}
					}
				}
				
				
				System.out.println("Updated Profile Groups");
				List<Long> selecteddb = new ArrayList<Long>();
				for (Iterator<UserDTO> iter1 = seldb.iterator(); iter1.hasNext();) {
					UserDTO db = iter1.next();
					selecteddb.add(db.getDbId());
				}
				for(Long db:selecteddb){
					if(!selProfileDatabases.contains(db)){
						for (String grp:selectedGroups) {
							Long groupId = uDao.getGroupIdbelongstoGroup(grp);
							uDao.createProfileGroup(pid,db, groupId, grp, UserId,admin_flag);
							uDao.insertDatabase(UserId, db);
						}
					}
				}
				for (Long dbId: selProfileDatabases) {
					if(!selecteddb.contains(dbId)){
						uDao.deleteDbFromProfile(pid,dbId);
						uDao.updateDatabase(UserId, dbId);
					}
				}
				System.out.println("Updated Profile Databases");
		
			} else {
				setMessage("Please Select Group Name");
			}
			
			setMessage("Profile " + profileName + " successfully updated");
		} catch (Exception e) {
			logger.error("Exception is in updating profile settings " + e.getMessage());
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}

		return "SUCCESS";

	}

	public String populateProfileSettings() {
		setTabId("User Profile Control");
		setSubTabId("Create Profiles");
		profileName = getProfileName();
		getServletRequest().getSession().setAttribute("profileName",
				profileName);
		// getServletRequest().getSession().setAttribute("profileNam, arg1)
		if (profileName == null || profileName.equalsIgnoreCase("")) {
			setMessage("Profile Name cannot be empty");
			tabId = "User Access Control";
			return "INVALID_PROFILENAME";
		}
		try {
			UserDAO uDao = new UserDAO();
			System.out.println("Profile Name:::???"+profileName);
			Long profileId = uDao.isProfileExists(profileName);
			System.out.println("Profile ID:::???"+profileId);
			if(profileId != 0)
			{
				setMessage("Profile Name already exists, please give another name!!!");
				tabId = "User Access Control";
				return "INVALID_PROFILENAME";
			}
			csrActions = uDao.getCsrActions();
			csrRoles = new ArrayList();
			List tmpcsrRoles = uDao.getRoles();
			Iterator itrer = tmpcsrRoles.iterator();
			while (itrer.hasNext()) {
				UserDTO cDao = (UserDTO)itrer.next();
				if(cDao.getRoleName().equalsIgnoreCase("Profile Admin")){
					UserDTO cdao = new UserDTO();
					cdao.setRoleId(cDao.getRoleId());
					cdao.setRoleName(cDao.getRoleName());
					cdao.setRoleDesc(cDao.getRoleDesc());
					csrRoles.add(cdao);
				}
			}
			csrDatabases = uDao.getDatabases();
			tabId = "User Access Control";
		}

		catch (Exception e) {
			return "FAILURE";
		}

		return "SUCCESS";
	}

	public String AssignUsersToProfile() {
		setTabId("Prof Assign");
		UserDAO uDao = new UserDAO();
		DataDTO dataDto = new DataDTO();
		DataIntegrityDAO dataManager = new DataIntegrityDAO();
		String host ="";
		String instance = "";
		Long hostId = 0L;
		Long profileId = 0L;
		boolean idxsearch = false;
		boolean idxresult = false;
		long endTime;
		long difference;
		long startTimeInitial;
		
		startTimeInitial =  System.currentTimeMillis();
		
		String group = (String) getServletRequest().getSession().getAttribute("UserGroup");
		String userName = (String) getServletRequest().getSession().getAttribute("UserName");
		
		actionId = getActionId();
		System.out.println("actionId size:::"+actionId.length+"element::"+actionId[0]);
		String userNames = "";

		try {

			UserDTO uDto = new UserDTO();
			csrAllUsers = new ArrayList();
			selcsrGrps = getSelcsrGrps();
			csrUsers = getCsrUsers();
			selCsrUsers = getSelCsrUsers();
			csrProfiles = getCsrProfiles();
			if (csrUsers != null)
				selCsrUsers = csrUsers;
			
			profileId = uDao.getuserprofileId(csrProfiles[0]);
			
			List<UserDTO> roleIds = uDao.getRoles();
			System.out.println("roleIds length::"+roleIds.size());
			Long roleId = 0L;
			for(UserDTO tmproleId:roleIds){
				if(tmproleId.getRoleName().equalsIgnoreCase("CSR")){
					System.out.println("inside csr block");
					roleId = tmproleId.getRoleId();
				}
			}
			System.out.println("roleIDs :" + roleId);
			if (selCsrUsers != null) {
				newUserId = new Long[selCsrUsers.length];
				for (int i = 0; i < selCsrUsers.length; i++) {

					System.out.println("selCsrUsers---->" + selCsrUsers[i]);

					String saveFlag = "Y";
					String[] split = selCsrUsers[i].split(",");
					
					/* Nithu Alexander: 02 Oct 2015. Added replaceAll.("&#x27;", "'"); for value of "csrUsers" entity. 
					 * As part of bug fix in AssignUsersToProfile() due to single quotes in user display name 
					 * */
					
					Long groupId1 = uDao.getGroupIdbelongstoGroup(split[0].replaceAll("&#x27;", "'"));
					String ldapuserId = LDAPUtils.getUserIDByDistinguishedName(split[1].replaceAll("&#x27;", "'"),"");
					
					System.out.println("ldapuserId::"+ldapuserId);
					
					uDto.setUserName(ldapuserId.toLowerCase());
					userNames += (ldapuserId + ",");
					uDto.setAccessdateFrom(getAccessdateFrom());
					uDto.setAccessdateTo(getAccessdateTo());
					uDto.setWeekend(getWeekend());
					uDto.setAccessfromDays(getAccessfromDays());
					uDto.setAccesstoDays(getAccesstoDays());
					uDto.setActive(getActive());
					
					Long newuserId = uDao.createTempuserMgmt1(uDto, "create",saveFlag, groupId1);

					uDto.setUserId(newuserId);
					System.out.println("New user id::"+newuserId+" Selected ProfileId:"+profileId);
					
					seldbIds = uDao.getProfiledbIds(profileId);
					
					System.out.println("seldbId length::"+seldbIds.size());
					
					
					for(Long dbId:seldbIds){
						uDao.createProfileGroup(profileId,dbId,groupId1, split[0], newuserId,"N");
					}
					
					uDao.saveUserPrevDetails(uDto, "update", "Y");
					uDao.insertRole(newuserId, roleId);
				
					for (int j = 0; j < actionId.length; j++) {
						uDao.saveUserActions(newuserId, actionId[j]);
						
					}
					
					for(Long dbId:seldbIds){
						uDao.insertDatabase(newuserId, dbId);
					}
					
					Long padminId = uDao.getProfileAdmin(profileId);
					
					Long hassearchIndex = 0L;
					Long hasresultIndex = 0L;
					idxsearch = false;
					idxresult = false;
					List<String> configuredIndexList = new ArrayList<String> ();
					List<String> nonConfiguredIndexList = new ArrayList<String> ();
					List<String> configuredColumnIndexList = new ArrayList<String> ();
					List<String> nonConfiguredColumnIndexList = new ArrayList<String> ();
					Map<String,String> indexResultMap = null;
					Map<Long,String> indexIdMap = null;
					
					for(Long dbId:seldbIds){
						String dbname = uDao.getDBName(newuserId, dbId);
						List<String> indexes = uDao.getColumnNames(dbname);
						
						configuredIndexList = uDao.getIndexListForProfileAdmin(indexes, dbId, padminId, "Y","Y");
						if(configuredIndexList != null && configuredIndexList.size() > 0){
							idxsearch = true;
							uDao.saveIndexesForNewUser(configuredIndexList, dbId, newuserId, "Y");
						}
						
						nonConfiguredIndexList = uDao.getIndexListForProfileAdmin(indexes, dbId, padminId, "Y","N");
						if(nonConfiguredIndexList != null && nonConfiguredIndexList.size() > 0){
							uDao.saveIndexesForNewUser(nonConfiguredIndexList, dbId, newuserId, "N");
						}
						
						configuredColumnIndexList = uDao.getIndexColumnsForProfileAdmin(indexes, dbId, padminId, "Y","Y");
						if(configuredColumnIndexList != null && configuredColumnIndexList.size() > 0){
							idxresult = true;
							uDao.saveIndexColumnsForNewUser(configuredColumnIndexList, dbId, newuserId, "Y");
						}
						
						nonConfiguredColumnIndexList = uDao.getIndexColumnsForProfileAdmin(indexes, dbId, padminId, "Y","N");
						if(nonConfiguredColumnIndexList != null && nonConfiguredColumnIndexList.size() > 0){
							uDao.saveIndexColumnsForNewUser(nonConfiguredColumnIndexList, dbId, newuserId, "N");
						}
						
						indexResultMap = uDao.getUserIndexResultsMap(padminId, dbId, indexes, "index");
						if(indexResultMap != null){
							indexIdMap = uDao.getUserIndexIdMap(dbId, newuserId, indexResultMap);
						}
						if(indexIdMap != null){
							uDao.saveIndexResultsList(indexIdMap);
						}
						
					}
						
					System.out.println("allusers created successfully");
					newUserId[i] = newuserId;
					csrAllUsers.add(selCsrUsers[i] + ";" + newuserId);
				}

			} else {
				setMessage("Please Select User Name");
			}
			System.out.println("allusers");

			tabId = "Prof Assign";
		} catch (Exception e) {
			return "FAILURE";
		}

		if(idxsearch == true && idxresult == true){
			setMessage("Selected Users " + userNames
					+ " successfully assigned to the profile "
					+ uDao.getProfileName(profileId)+" !!!");
		}
		else{
			setMessage("Selected Users " + userNames
					+ " successfully assigned to the profile "
					+ uDao.getProfileName(profileId)+" but indexes have not configured as ProfileAdmin have no configured indexes!!!");
		}
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for AssignUsersToProfile method : " + difference);
		
		return "SUCCESS";

	}

	public String ApplyUserSettings() {
		Long userId = 0l;
		try {
			UserDAO uDao = new UserDAO();
			UserDTO uDto = new UserDTO();
			setSubTabId("Access Control");
			setTabId("User Access Control");
			System.out.println("applySettings" + getSelcsrGrps());

			roleId = getRoleId();
			dbId = getDbId();
			active = getActive();
			groupId = getGroupId();
			accessdateFrom = getAccessdateFrom();
			accessdateTo = getAccessdateTo();
			weekend = getWeekend();
			currYear = getCurrYear();
			accessfromDays = getAccessfromDays();
			accesstoDays = getAccesstoDays();
			actionId = getActionId();
			csrAllUsers = getCsrAllUsers();

			System.out.println("dbId:" + dbId);
			System.out.println("roleId:" + roleId);
			System.out.println("csrGrps:" + selcsrGrps);
			System.out.println("csrGrps:" + groupId);
			uDto.setAccessdateFrom(accessdateFrom);
			uDto.setAccessdateTo(accessdateTo);
			uDto.setWeekend(weekend);
			uDto.setAccessfromDays(accessfromDays);
			uDto.setAccesstoDays(accesstoDays);
			uDto.setActive(active);
			uDto.setCurrYear(currYear);

			if (newUserId != null) {
				for (int i = 0; i < newUserId.length; i++) {

					System.out.println("selCsrUsers---->" + newUserId[i]);
					boolean b = uDao.getCsrUsers(newUserId[i]);
					uDto.setUserId(newUserId[i]);
					if (b) {
						UserDTO udto = uDao.getUserName(newUserId[i]);
						uDto.setUserName(udto.getUserName());
						uDto.setGroupId(udto.getGroupId());
						String saveFlag = "Y";
						uDao.saveUserPrevDetails(uDto, "update", saveFlag);
						if (roleId != null) {
							for (int j = 0; j < roleId.length; j++) {

								System.out.println("roles" + roleId[j]);
								uDao.insertRole(newUserId[i], roleId[j]);
							}
						}
						if (actionId != null) {
							for (int j = 0; j < actionId.length; j++) {
								uDao.saveUserActions(newUserId[i], actionId[j]);
							}
						}
						if (dbId != null) {
							for (int j = 0; j < dbId.length; j++) {
								uDao.insertDatabase(newUserId[i], dbId[j]);
							}
						}
					}
				}

			} else {
				setMessage("Please Select Details");
			}

			tabId = "User Access Control";
		} catch (Exception e) {
			return "FAILURE";
		}
		return "SUCCESS";
	}

	public String saveProfileSettings() {

		roleId = getRoleId();
		setTabId("User Profile Control");
		setSubTabId("Create Profiles");
		HttpSession session = getServletRequest().getSession();
		session.setAttribute("roleId", roleId);
		dbId = getDbId();
		session.setAttribute("dbId", dbId);
		active = getActive();
		session.setAttribute("active", active);

		groupId = getGroupId();
		accessdateFrom = getAccessdateFrom();
		session.setAttribute("accessdateFrom", accessdateFrom);
		System.out.println("accessdateFrom??????================="+accessdateFrom);
		accessdateTo = getAccessdateTo();
		session.setAttribute("accessdateTo", accessdateTo);

		weekend = getWeekend();
		session.setAttribute("weekend", weekend);
		currYear = getCurrYear();
		session.setAttribute("currYear", currYear);

		accessfromDays = getAccessfromDays();
		session.setAttribute("accessfromDays", accessfromDays);
		accesstoDays = getAccesstoDays();

		session.setAttribute("accesstoDays", accesstoDays);
		UserDAO uDao = new UserDAO();
		List actions = uDao.getCsrActions();
		Long[] aids = new Long[4];
		int i=0;
		for (Iterator aiter = actions.iterator(); aiter.hasNext();) {
			UserDTO udto = (UserDTO) aiter.next();
			System.out.println("actionID::"+udto.getActionId());
			if(udto.getActionId()!=null){
				aids[i] = udto.getActionId();
			}
			i++;
		}
		setActionId(aids);
		actionId = getActionId();
		System.out.println("actionId::"+actionId);
		session.setAttribute("actionId", actionId);
		csrAllUsers = getCsrAllUsers();

		boolean userExist = false;
		try {
			HashMap<String, String> propertiesMap = new HashMap<String, String>();
			Enumeration params = request.getParameterNames();
			List<String> dbIdList = new ArrayList<String>();
			while (params.hasMoreElements()) {
				String paramName = params.nextElement().toString();
				if (paramName.startsWith("dbId_")) {
					String selectedCheckBoxValue = request.getParameter(paramName);
					System.out.println("Value is............"+selectedCheckBoxValue);
					if (!selectedCheckBoxValue.equalsIgnoreCase("unchecked")) {
						dbIdList.add(selectedCheckBoxValue);
					}
				}
			}
			dbId = new Long[dbIdList.size()];
			int k =0;
			for (String tdbId:dbIdList) {
				dbId[k] = Long.parseLong(tdbId);
				k++;
			}
			System.out.println("Selected dbId size::"+dbId.length);
			session.setAttribute("dbId", dbId);
			String currentGrp = "";
			System.out.println("execute:AdminSubmitAction*-*");
			prePopulateList = new ArrayList<String>();
			csrGroups = new ArrayList();
			csrGroups = LDAPUtils.getLDAPCSRGroups(CommonConstants.CSR_PROPERTY_FILE_NAME);
			
			logger.info("Before getting Groups:propertiesMap ------------ "
					+ propertiesMap);

			tabId = "User Access Control";

		} catch (Exception e) {

			e.printStackTrace();
			return "FAILURE";
		}
		return "SUCCESS";

	}

	

	

	public String ListUsers() {
		UserDAO uDao = new UserDAO();

		setTabId("User Access Control");
		setSubTabId("Access Control");
		try {
			HttpSession sessionku = getServletRequest().getSession();
			if (sessionku.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			csrAllUsers = new ArrayList();
			csrRoles = new ArrayList();
			csrDatabases = new ArrayList();
			List populateuserList = (ArrayList) uDao.getAllUsers();
			System.out.println("populateuserList in ListUsers:::"+populateuserList);
			Iterator iter = populateuserList.iterator();
			while (iter.hasNext()) {
				UserDTO uDto = (UserDTO) iter.next();
				String dbDesc = uDao.getDBDesc(uDto.getUserId(), 0L);
				dbDesc = dbDesc.substring(0,dbDesc.length()-1);
				String roleName = uDao.getRoleName(uDto.getUserId());
				roleName = roleName.substring(0,roleName.length()-1);
				System.out.println(dbDesc + "roleName" + roleName);
				uDto.setDbName(dbDesc);
				uDto.setRoleName(roleName);
				csrAllUsers.add(uDto);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}

	public String ProfileListUsers() {
		UserDAO uDao = new UserDAO();
		setTabId("User Access Control");
		setSubTabId("Access Control");
		try {
			HttpSession sessionku = getServletRequest().getSession();
			if (sessionku.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			String ldapuserId = sessionku.getAttribute("UserName").toString();
			ldapuserId = ldapuserId.toLowerCase();
			String profile = uDao.getProfileForUser(ldapuserId);
			System.out.println("ProfileListUsers >> profile ::"+profile);
			csrAllUsers = new ArrayList();
			csrRoles = new ArrayList();
			csrDatabases = new ArrayList();
			List populateuserList = (ArrayList) uDao.getAllProfileUsers(profile);
			Iterator iter = populateuserList.iterator();
			while (iter.hasNext()) {
				UserDTO uDto = (UserDTO) iter.next();
				String dbDesc = uDao.getDBDesc(uDto.getUserId(), 0L);
				if(dbDesc!=""){
					dbDesc = dbDesc.substring(0,dbDesc.length()-1);
				}
				String roleName = uDao.getRoleName(uDto.getUserId());
				roleName = roleName.substring(0,roleName.length()-1);
				System.out.println(dbDesc + "roleName" + roleName);
				uDto.setDbName(dbDesc);
				uDto.setRoleName(roleName);
				/* Nithu Alexander: 22/09/2014
				 * Invoking getDisplayNameOfUser() in LDAPUtils class
				 * to obtain the displayName of every UserId*/
				if(uDto.getUserName() != null && !(uDto.getUserName().trim().equals(""))){
					String displayFullName = LDAPUtils.getDisplayNameOfUser(uDto.getUserName().trim());
					if(displayFullName != null && !(displayFullName.trim().equals(""))){
						uDto.setUserDisplayName(displayFullName);
					}
				}
				csrAllUsers.add(uDto);
			}
						
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}
		return "SUCCESS";
	}
	
	

	public String UpdateUser() {
		setTabId("User Access Control");
		setSubTabId("Access Control");
		UserDAO uDao = new UserDAO();
		UserDTO uDto = new UserDTO();
		try {
			roleId = getRoleId();
			dbId = getDbId();
			active = getActive();
			groupId = getGroupId();
			accessdateFrom = getAccessdateFrom();
			accessdateTo = getAccessdateTo();
			weekend = getWeekend();

			accessfromDays = getAccessfromDays();
			accesstoDays = getAccesstoDays();
			actionId = getActionId();
			csrAllUsers = getCsrAllUsers();
			System.out.println("dbId:" + dbId);
			System.out.println("roleId:" + roleId);
			System.out.println("csrGrps:" + selcsrGrps);
			System.out.println("csrGrps:" + groupId);
			uDto.setAccessdateFrom(accessdateFrom);
			uDto.setAccessdateTo(accessdateTo);
			uDto.setWeekend(weekend);
			uDto.setAccessfromDays(accessfromDays);
			uDto.setAccesstoDays(accesstoDays);
			uDto.setActive(active);

			System.out.println(request.getParameter("seluserId")
					+ "selCsrUsers---->" + request.getAttribute("seluserId"));
			if (request.getParameter("seluserId") != null) {
				Long userId = Long.parseLong(request.getParameter("seluserId")
						.toString());
				System.out.println("selCsrUsers---->" + userId);
				boolean b = uDao.getCsrUsers(userId);
				uDto.setUserId(userId);
				if (b) {
					UserDTO udto = uDao.getUserName(userId);
					uDto.setUserName(udto.getUserName());
					uDto.setGroupId(udto.getGroupId());
					String saveFlag = "Y";
					uDao.saveUserPrevDetails(uDto, "update", saveFlag);

					if (roleId != null) {
						uDao.DeleteRole(userId);
						for (int j = 0; j < roleId.length; j++) {

							System.out.println("roles" + roleId[j]);

							uDao.insertRole(userId, roleId[j]);
						}
					}
					if (actionId != null) {
						uDao.DeleteUserActions(userId);
						for (int j = 0; j < actionId.length; j++) {
							uDao.saveUserActions(userId, actionId[j]);
						}
					}
					if (dbId != null) {
						uDao.DeleteDatabase(userId);
						for (int j = 0; j < dbId.length; j++) {
							uDao.insertDatabase(userId, dbId[j]);
						}
					}
					else{
						uDao.DeleteDatabase(userId);
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}
	
	public String UpdateProfAdmin(Long userId) {
		System.out.println("Inside UpdateProfAdmin");
		UserDAO uDao = new UserDAO();
		UserDTO uDto = new UserDTO();
		try {
			seldb = getSeldb();
			active = "Active";
			csrAllUsers = getCsrAllUsers();
			uDto.setActive(active);
			boolean b = uDao.getCsrUsers(userId);
			uDto.setUserId(userId);
			if (b) {
				UserDTO udto = uDao.getUserName(userId);
				uDto.setUserName(udto.getUserName());
				uDto.setGroupId(udto.getGroupId());
				String saveFlag = "Y";
				uDao.saveUserPrevDetails(uDto, "update", saveFlag);

				if (roleId != null) {
					uDao.DeleteRole(userId);
					for (int j = 0; j < roleId.length; j++) {
						System.out.println("roles" + roleId[j]);
						uDao.insertRole(userId, roleId[j]);
					}
				}
				if (actionId != null) {
					uDao.DeleteUserActions(userId);
					for (int j = 0; j < actionId.length; j++) {
						uDao.saveUserActions(userId, actionId[j]);
					}
				}
				if (dbId != null) {
					uDao.DeleteDatabase(userId);
					for (int j = 0; j < dbId.length; j++) {
						uDao.insertDatabase(userId, dbId[j]);
					}
				}
				

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}

	

	public String DeleteUser() {
		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();
		UserDAO uDao = new UserDAO();
		boolean b = false;
		boolean isPadmin = false;
		try {
			System.out.println(request.getAttribute("seluserId")
					+ "deleteUser:" + getNewUserId());
			if (getNewUserId() != null) {

				for (int i = 0; i < getNewUserId().length; i++) {
					System.out.println("deleteUser:inside" + getNewUserId()[i]);
					List<Long> pid = uDao.getProfileIdForGroups(getNewUserId()[i]);
					if(pid.size()>0){
						Long profileId = 0L;
						for (Iterator iter = pid.iterator(); iter.hasNext();) {
							profileId = (Long) iter.next();
						}
						if(profileId!=0){
							String profile = uDao.getProfileName(profileId);
							List userList = (ArrayList) uDao.getAllProfileUsers(profile);
							if(userList.size()>0){
								setMessage("You cannot delete a Profile Admin which have users assigned to the profile!!!");
								isPadmin = true;
								endTime =  System.currentTimeMillis();
								difference = endTime-startTimeInitial;
								System.out.println("********* Elapsed milli seconds - Total time taken for DeleteUser method : " + difference);
								
								return "SUCCESS";
							}
						}
					}
					if(isPadmin != true){
						b = uDao.deleteUser(getNewUserId()[i]);
					}
				}
			}
			if (b) {
				setMessage("Successfully Deleted");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for DeleteUser method : " + difference);
		
		return "SUCCESS";
	}

	private Long getAdminRoleId() {

		List roles = userManager.getRoles();
		Long roleId = null;
		for (Iterator iter = roles.iterator(); iter.hasNext();) {
			UserDTO cdao = (UserDTO) iter.next();
			if (cdao.getRoleDesc().equalsIgnoreCase(
					CommonConstants.ROLE_DESC_ADMIN)) {
				roleId = cdao.getRoleId();
			}
		}
		return roleId;
	}
	
	private Long getProfileAdminRoleId() {

		List roles = userManager.getRoles();
		Long roleId = null;
		for (Iterator iter = roles.iterator(); iter.hasNext();) {
			UserDTO cdao = (UserDTO) iter.next();
			if (cdao.getRoleDesc().equalsIgnoreCase(
					CommonConstants.ROLE_DESC_PROJECTADMIN)) {
				roleId = cdao.getRoleId();
			}
		}
		return roleId;
	}

	public String ListLdapUsers() {
		ldapUsersList = new ArrayList<UserDTO>();
		subTabName = "actioncontrol";
		tabName = "useradmin";
		UserDAO uDao = new UserDAO();
		
		try {
			HttpSession sessionku = getServletRequest().getSession();
			
			//System.out.println("******************** session id : " + sessionku.getId());
			
			if (sessionku.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}

			List<String> groupList = LDAPUtils.getLDAPCSRGroups(CommonConstants.CSR_PROPERTY_FILE_NAME);
			this.csrGroups = groupList;
						
			for (String group : groupList) {
				List<String> users = LDAPUtils.getUsersBelongstoGroup2(group);
				for (String user : users) {
					UserDTO u = new UserDTO();
					u.setUserName(user);
					u.setUserGrp(group);
					ldapUsersList.add(u);
				}

			}
			
			/*for (String group : groupList) {
				List<String> users = LDAPUtils.getUsersBelongstoGroup(CommonConstants.CSR_PROPERTY_FILE_NAME,group);
				for (String user : users) {
					UserDTO u = new UserDTO();
					u.setUserName(user);
					u.setUserGrp(group);
					ldapUsersList.add(u);
				}

			}*/

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";

	}

	public String CreateAdmin() {
		// String realPath = getServerPath(request);
		subTabName = "actioncontrol";
		tabName = "useradmin";
		// setSubTabId("CreateAdmin");
System.out.println("create admi ncalled"+selectedGroup);
//System.out.println(selectedGroup);
System.out.println(usersInGroup);
for(String g:usersInGroup)
{
	System.out.println("pritning"+g);
}


		Long adminRoleId = getAdminRoleId();
		System.out.println("admi nrolei d"+adminRoleId);
		try {
			String group = selectedGroup;

			for (String user : usersInGroup) {

				UserDTO uDto = new UserDTO();
				// LDAPUtils.authenticateUser(userId, password,
				// propertiesFileNameWithPath)
				uDto.setUserGrp(group);
System.out.println("User grp"+user);
				String saveFlag = "Y";
				String ldapuserId = LDAPUtils.getUserIDByDistinguishedName(user,"");
				//String ldapuserId=user;
				System.out.println("ldapuserId::"+ldapuserId);
				uDto.setUserName(ldapuserId.toLowerCase());
				Long newuserId = 0L;
				Long groupId = this.userManager.getGroupIdbelongstoGroup(group);
				Properties properties = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);
				
				uDto.setAccessdateFrom(properties.getProperty("CSR_ADMIN_accessdateFrom"));
				uDto.setAccessdateTo(properties.getProperty("CSR_ADMIN_accessdateTo"));
				uDto.setAccessfromDays(Integer.parseInt(properties.getProperty("CSR_ADMIN_accessfromDays")));
				uDto.setAccesstoDays(Integer.parseInt(properties.getProperty("CSR_ADMIN_accesstoDays")));
				uDto.setActive("Active");
				
				if (groupId == null || groupId == 0) {
					System.out.println("groupId" + groupId);
					groupId = userManager.saveTempUsergrpDetails1(uDto,
							"create", saveFlag);
					
					System.out.println("group id"+groupId);
				}

				if (groupId != 0) {
					uDto.setGroupId(groupId);
					boolean userExist = userManager.getUserAccessWithGroupId(
							user, groupId);
					
					System.out.println("here user exist"+userExist);
					if (!userExist) {
						newuserId = userManager.createTempuserMgmt1(uDto,
								"create", saveFlag, groupId);
						
						System.out.println("new suer id"+newuserId);
						uDto.setUserId(newuserId);
						userManager.saveUserPrevDetails(uDto, "update", "Y");

						userManager.insertRole(newuserId, adminRoleId);
					}

				}
			}

			String path = CommonConstants.CSR_PROPERTY_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(path);
			properties.setProperty(CommonConstants.CSR_ADMIN_USER_ID, " ");
			// .remove(CommonConstants.CSR_ADMIN_USER_ID);
			properties.setProperty(CommonConstants.CSR_ADMIN_PWD, " ");
			properties.setProperty(CommonConstants.CSR_ADMIN_GROUP_NAME, " ");
			//properties.store(new FileOutputStream(path), null);

			setMessage(usersInGroup[0] + " In group " + group
					+ " Successfully set as the Administrator");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";

	}

	private boolean isPrinterAdded(String printerName) {
		UserDAO u = new UserDAO();
		List<String> addedPrinters = u.getAllPrinters();

		for (String printer : addedPrinters) {
			if (printerName.equalsIgnoreCase(printer)) {
				return true;

			}
		}

		return false;
	}

	public String getAddedPrinters() {
		setTabId("System Admin");
		setSubSystemTabId("Printer Config");
		setSubAccessTabId("Remove Printer");
		UserDAO u = new UserDAO();
		try {
			addedPrinterNames = u.getAllPrinters();
		} catch (Exception e) {
			return "FAILURE";
		}
		return "SUCCESS";
	}

	public String getPrinters() {
		setTabId("System Admin");
		setSubSystemTabId("Printer Config");
		setSubAccessTabId("Add Printer");
		try {
			PrintService[] ser = PrintServiceLookup.lookupPrintServices(null,
					null);
			printerNames = new ArrayList();
			//System.out
				//	.println("**************** All Printers ******************");

			for (int i = 0; i < ser.length; ++i) {
				if (!isPrinterAdded(ser[i].getName()))
					printerNames.add(ser[i].getName());
				// }
			}
		} catch (Exception e) {
			System.out.println(e);
			return "FAILURE";
		}
		return "SUCCESS";
	}

	public String AddPrinter() {
		setTabId("System Admin");
		setSubSystemTabId("Printer Config");
		setSubAccessTabId("Add Printer");
		UserDAO u = new UserDAO();
		String printers = "";
		try {
			for (String s : selprinterNames) {
				u.AddPrinter(s);
				printers += s;

			}

			getPrinters();

		} catch (Exception e) {
			System.out.println(e);
			return "FAILURE";
		}
		setMessage("Printers" + printers + " Successfully added");
		return "SUCCESS";
	}

	public String RemovePrinter() {
		setTabId("System Admin");
		setSubSystemTabId("Printer Config");
		setSubAccessTabId("Remove Printer");
		UserDAO u = new UserDAO();
		String printers = "";
		try {
			for (String s : seladdedPrinterNames) {
				u.RemovePrinter(s);
				printers += s;

			}

			getAddedPrinters();

		} catch (Exception e) {
			System.out.println(e);
			return "FAILURE";
		}
		setMessage("Printers" + printers + " Successfully removed");
		return "SUCCESS";
	}
	
	public String createTableName() {
		setTabId("System Admin");
		setSubSystemTabId("DB Config");
		setSubAccessTabId("Create Table");
		HttpSession sessionku = getServletRequest().getSession();
		UserDAO uDao = new UserDAO();
		csrDatabases = new ArrayList();
		
		/* Nithu Alexander : 12th Nov 2014
		 * Change as part of ECMS -AVA 3.2 --> Access Control*/
		if(!((String)sessionku.getAttribute("sysaccesscontrol")).equalsIgnoreCase("true")){
			return "UNAUTHORIZED";
		}
		
		try{
			csrDatabases = uDao.getDatabases();
			System.out.println("Inside createTableName:::"+csrDatabases.size());
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return "SUCCESS";
	}
	
	public String createCrossRefTableName() {
		UserDAO uDao = new UserDAO();
		setTabId("System Admin");
		setSubSystemTabId("DB Config");
		setSubAccessTabId("Create CrossReftable");
		csrDatabases = new ArrayList();
		try{
			csrDatabases = uDao.getDatabases();
			System.out.println("Inside createCrossRefTableName:::"+csrDatabases.size());
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return "SUCCESS";
	}
	
	public String PopulateColumnsAction() {
		UserDAO u = new UserDAO();
		setTabId("System Admin");
		setSubSystemTabId("DB Config");
		setSubAccessTabId("Create Table");
		String message1 = "";
		System.out.println("Inside PopulateColumnsActionMethod???");
		dbindexList = new ArrayList<String>();
		dbdataTypeMap = new HashMap<String, Integer>();
		try {
			
			/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1,AVA-2 and SCT (VaultApp)*/
			
			if(tableName!=null && !(tableName.trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithUnderscore(tableName)){
				setMessage("Invalid Characters in Selected Table Name");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(columnNames!=null && !(columnNames.trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithCommaAndUnderscore(columnNames)){
				setMessage("Invalid Characters entered in Database Column Field");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			if(columnDescs!=null && !(columnDescs.trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithSpaceAndComma(columnDescs)){
				setMessage("Invalid Characters entered in Database Column Description Field");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(dataTypes!=null && !(dataTypes.trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithSpaceAndComma(dataTypes)){
				setMessage("Invalid Characters entered in Database Column Datatype Field");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(constraints!=null && !(constraints.trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithCommaAndUnderscore(constraints)){
				setMessage("Invalid Characters entered in Database Column Constraints Field");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(widths!=null && !(widths.trim().equals("")) && !ServerSideValidationUtil.isNumericWithComma(widths)){
				setMessage("Invalid Characters entered in Database Column Width Field");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(!ServerSideValidationUtil.isNumeric(String.valueOf(columnnum1))){
				setMessage("Invalid Characters in entered in Database Column Number");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(seldbId!=null && !ServerSideValidationUtil.isNumeric(String.valueOf(seldbId))){
				setMessage("Invalid Characters in Selected Database Identifier");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(tableName1!=null && !(tableName1.trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithSpace(tableName1)){
				setMessage("Invalid Characters in Selected Table Name");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			
			if(columnNames.length()==0)
			{	
				seldbId = Long.parseLong(tableName.split("_")[0]);
				
				/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1,AVA-2 and SCT (VaultApp)*/
				if(seldbId!=null && !ServerSideValidationUtil.isNumeric(String.valueOf(seldbId))){
					setMessage("Invalid Characters in Selected Database Identifier");
					tabId = "System Admin";
					return "INVALID_PARAMETERS";
				}
				System.out.println("sel dbId::"+seldbId);
				int i = tableName.indexOf("_");
				tableName1 = tableName.substring(i+1, tableName.length());
				
				/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1,AVA-2 and SCT (VaultApp)*/
				if(tableName1!=null && !(tableName1.trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithSpace(tableName1)){
					setMessage("Invalid Characters in Selected Table Name");
					tabId = "System Admin";
					return "INVALID_PARAMETERS";
				}
				setTableName1(tableName1);
				System.out.println("tableName1:::"+tableName1);
				
				dbindexList = u.getDbIndex(seldbId);
				dbdataTypeMap = u.getDbTypes();
				System.out.println("dbdataTypeMap size::"+dbdataTypeMap.size());
				System.out.println("Inside if???");
				boolean tableExists = u.checkTableName(tableName1);
				if(tableExists){
					setMessage("Table with name "+tableName1+" already exists ");
				}
			}
			else
			{	
				
				//System.out.println("Inside else");
				System.out.println("columnNames length::"+columnNames.length());
				System.out.println("columnDescs length::"+columnDescs.length());
				System.out.println("dataTypes length::"+dataTypes.length());
				System.out.println("constraints length::"+constraints.length());
				System.out.println("widths length::"+widths);
				String[] colNamesArr = columnNames.split(",");
				String[] colDescsArr = columnDescs.split(",");
				String[] dTypesArr = dataTypes.split(",");
				String[] constArr = constraints.split(",");
				String[] widthArr = widths.split(",");
				
				System.out.println("colNamesArr length::"+colNamesArr.length);
				System.out.println("colDescsArr length::"+colDescsArr.length);
				System.out.println("dTypesArr length::"+dTypesArr.length);
				System.out.println("constArr length::"+constArr.length);
				System.out.println("widthArr length::"+widthArr.length);
					
				String createQry ="";
				for(int i=0;i<columnnum1;i++)
				{   
					createQry += colNamesArr[i]+" "+dTypesArr[i];
					if(!widthArr[i].equalsIgnoreCase("default")){
						createQry += "("+widthArr[i]+") "+constArr[i]+",";
					}
					else{
						createQry += " "+constArr[i]+",";
					}
				}
				createQry += "Flag nchar(1), RID bigint IDENTITY(1,1) NOT NULL";
				System.out.println("Create Query:::"+createQry);
				String[] tName = tableName1.split("\\s");
				String tblName = "";
				for(int i = 0;i<tName.length;i++){
					if(i<tName.length-1)
						tblName += tName[i]+"_";
					else
						tblName += tName[i];
				}
				message1 = u.createTable(tblName,createQry,message1);
				if(message1.equalsIgnoreCase("")){
					boolean b = u.saveVaultAppIndex(seldbId, colNamesArr, colDescsArr);
					if(b)
						setMessage(tblName+" Table Successfully added");
					else
						setMessage(tblName+" Table Successfully added but Error while inserting VaultApp db Idx");
				}
					
				else
					setMessage(message1);
			}
		}
		catch(Exception e) {
			System.out.println(e);
			return "FAILURE";
		}
		return "SUCCESS";
	}
	
	
}
