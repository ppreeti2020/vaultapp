package com.pb.admin;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.dao.UserDAO;
import com.pb.dto.DataDTO;
import com.pb.scheduler.DataLoader;
import com.pb.scheduler.GenericExtFilter;

public class LoadDataTask extends TimerTask implements Serializable {

	private static Timer TIMER = null;
	
	
	public static synchronized void startMonitor(){
		if(TIMER==null){
			Properties properties = getCSRAppProperties();
			
			String schtime=properties.getProperty("SCHEDULER_TIME");
			String schdularFile=properties.getProperty("SCHEDULER_STATUS_FILE");
			File f = new File(schdularFile);
			if(f.exists())
				f.delete();
			
			int i=120;
			try{i = Integer.parseInt(schtime);}catch(Exception e){/*burpp*/}
			Timer localTimer=new Timer();
			localTimer.schedule(new LoadDataTask(),60*1000,i*60*1000);
			
			TIMER = localTimer;
			try{
			File fs=new File(schdularFile);
			FileWriter fileWr=new FileWriter(fs);
			fileWr.write("Started");
			fileWr.close();
			}catch(Exception e){
				//burpp
			}
			
		}
	}
	
	public static synchronized boolean isMonitoring(){
		return TIMER!=null;
	}
	
	public static synchronized void stopMonitoring(){
		if(TIMER!=null){
			TIMER.cancel();
			TIMER=null;
			
			Properties properties = getCSRAppProperties();
			String schdularFile=properties.getProperty("SCHEDULER_STATUS_FILE");
			File f = new File(schdularFile);
			if(f.exists())
				f.delete();
		}
	}
	
	private static Properties getCSRAppProperties(){
		String filePath = CommonConstants.CSR_PROPERTY_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(filePath);
		return properties;
	}

	private LoadDataTask(){
		
	}
	
	
	private void moveFile(File afile) {
		String strFile = PropertyUtils.getProperties(
				CommonConstants.VAULT_CONFIG_FILE_NAME).getProperty(
				CommonConstants.SERVER1_PROCESSED_JRN_PATH);

		if (afile.renameTo(new File(strFile + "\\" + afile.getName()))) {
			System.out.println("File is moved successful!");
		} else {

			afile.renameTo(new File(strFile + "\\" + new Date() + "-"
					+ afile.getName()));
			System.out.println("File moved with renaming with date!");
			if (afile.exists())
				afile.delete();
		}

		// /SERVER1_PROCESSED_JRN_PATH
	}

	public void run() {
		// TODO Auto-generated method stub
		String filePath = CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(filePath);
		String jrnfolder = properties
				.getProperty(CommonConstants.SOFTWARE_PATH) + "\\jrn";

		UserDAO userManager1 = new UserDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		System.out.println("JrnFileIntegrity:" + jrnfolder);
		String path = jrnfolder;
		try {
			File file = new File(path);
			File[] allFiles = file.listFiles(new GenericExtFilter(".TXT"));

			System.out.println("Listed allfiles" + allFiles.length);

			// System.out.println(allFiles+"allfiles:JrnFileIntegrity-Address------>"
			// + dataPath);
			if (allFiles != null) {
				for (File f : allFiles) {
					System.out.println("file:" + f + "length is" + f.length()
							+ "ame" + "==" + f.getName());

					if (f.exists() && f.length() > 0) {
						DataLoader jcn = new DataLoader();
						String fileName = f.getName();
						// int count = userManager1.getJrnStatus(fileName);

						// boolean b = userManager1.saveJrnDetails(fileName);
						// System.out.println("saved::"+b);
						// if(b!=false){
						System.out.println("inside to process");
						String message = jcn.processJrn(f.toString());
						System.out.println("Message returned for" + fileName
								+ "===messageis" + message);
						if (message.contains("Completed")) {
							userManager1.saveJrnDetailsForTimer(fileName,
									"Completed");
							// userManager1.updateJrnDetails(fileName);
						} else {
							userManager1.saveJrnDetails(fileName);
						}
						moveFile(f);

					}

					else {
						if (f.exists()) {
							moveFile(f);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
