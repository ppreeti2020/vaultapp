/**
 * 
 * @author suhasini
 * @class VaultConfigAction
 * To create e2vault connection by system admin
 */
package com.pb.admin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.opensymphony.xwork2.ActionSupport;
import com.pb.DatabaseWatch.DatabaseWatcher;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.common.ServerSideValidationUtil;
import com.pb.dao.DataIntegrityDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.DataDTO;
import com.pb.manager.DataIntegrityManager;
import com.pb.scheduler.DataLoader;
import com.pb.scheduler.DirectoryWatchListener;
import com.pb.scheduler.GenericExtFilter;
import com.pb.scheduler.QuartzSchedulerListener;

public class VaultConfigAction extends ActionSupport implements
ServletRequestAware, ServletResponseAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private DataIntegrityDAO dataManager = new DataIntegrityDAO();

	private String message;
	private String status;
	private String tabId;

	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private String serverHost;
	private int serverPort;
	//private String serverIP;
	private String softwarePath;
	private String vaultPath;
	private String server;

	//private String serverSoftwarePath;
	private int archivalServerPort;
	private int renderServerPort;
	private int dataRouterPort;

	private String serverInstanceName;

	private String subSystemTabId;
	//error needs to change:
//public static Timer timer;


	public String getServerInstanceName() {
		return serverInstanceName;
	}

	public void setServerInstanceName(String serverInstanceName) {
		this.serverInstanceName = serverInstanceName;
	}

	public void setVaultPath(String vaultPath) {
		this.vaultPath = vaultPath;
	}

	public String getVaultPath() {
		return vaultPath;
	}

	public void setSubSystemTabId(String subSystemTabId) {
		this.subSystemTabId = subSystemTabId;
	}

	public String getSubSystemTabId() {
		return subSystemTabId;
	}



	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public String getTabId() {
		return tabId;
	}

	public void setDataRouterPort(int dataRouterPort) {
		this.dataRouterPort = dataRouterPort;
	}

	public int getDataRouterPort() {
		return dataRouterPort;
	}

	public void setRenderServerPort(int renderServerPort) {
		this.renderServerPort = renderServerPort;
	}

	public int getRenderServerPort() {
		return renderServerPort;
	}

	public void setArchivalServerPort(int archivalServerPort) {
		this.archivalServerPort = archivalServerPort;
	}

	public int getArchivalServerPort() {
		return archivalServerPort;
	}




	public void setServer(String server) {
		this.server = server;
	}

	public String getServer() {
		return server;
	}

	public String getServerHost() {
		return serverHost;
	}

	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}



	public String getSoftwarePath() {
		return softwarePath;
	}

	public void setSoftwarePath(String softwarePath) {
		this.softwarePath = softwarePath;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.servletResponse = response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute() This method is used
	 * to display database values already created by system admin
	 */
	public String execute() throws Exception {
		Long UserId = 0L;
		HttpSession sessionku = getServletRequest().getSession();

		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}

		if(!((String)sessionku.getAttribute("sysaccesscontrol")).equalsIgnoreCase("true")){
			return "UNAUTHORIZED";
		}
		
		try {
			System.out.println("param::"+ getServletRequest().getAttribute("server"));
			setTabId("System Admin");
			setSubSystemTabId("Vault Config");
			
			/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1 and SCT (VaultApp)*/
			if((getServletRequest().getParameter("server")) !=null && !((getServletRequest().getParameter("server")).trim().equals("")) && !ServerSideValidationUtil.isAlphaNumeric((getServletRequest().getParameter("server")))){
				setMessage("Invalid Characters in Server Type");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			if (getServletRequest().getParameter("server") != null) {
				String filePath = CommonConstants.VAULT_CONFIG_FILE_NAME;
				//String filePath1 = CommonConstants.CSR_PROPERTY_FILE_NAME;
				//System.out.println("here1");
				//System.out.println("here2");
				Properties properties = PropertyUtils.getProperties(filePath);
				//Properties properties1 = PropertyUtils.getProperties(filePath1);
				if (getServletRequest().getParameter("server").equals("server1") )
				{
					if (properties
							.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null)
						setServerHost(properties
								.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST));
					if (properties
							.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT) != null)
						setArchivalServerPort(Integer
								.parseInt(properties
										.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT)));
					if (properties
							.getProperty(CommonConstants.RENDERING_SERVER_PORT) != null)
						setRenderServerPort(Integer
								.parseInt(properties
										.getProperty(CommonConstants.RENDERING_SERVER_PORT)));
					if (properties
							.getProperty(CommonConstants.DATA_ROUTER_SERVER_PORT) != null )
						setDataRouterPort(Integer
								.parseInt(properties
										.getProperty(CommonConstants.DATA_ROUTER_SERVER_PORT)));

					if (properties.getProperty(CommonConstants.SOFTWARE_PATH) != null)
						setSoftwarePath(properties
								.getProperty(CommonConstants.SOFTWARE_PATH));
					if (properties.getProperty(CommonConstants.SERVER_PATH) != null)
						setVaultPath(properties
								.getProperty(CommonConstants.SERVER_PATH));

					if (properties.getProperty(CommonConstants.SERVER_INSTANCE_NAME) != null)
						setServerInstanceName(properties
								.getProperty(CommonConstants.SERVER_INSTANCE_NAME));
					return "SUCCESS";
				}
				else if (getServletRequest().getParameter("server").equals("server2") )
					{	
					Properties properties1 = PropertyUtils.getProperties("");
					if(properties1.getProperty(CommonConstants.VAULT_SERVER2).equalsIgnoreCase("yes")){
						if (properties
								.getProperty(CommonConstants.SERVER2_HOST) != null)
							setServerHost(properties
									.getProperty(CommonConstants.SERVER2_HOST));
						if (properties
								.getProperty(CommonConstants.ARCHIVAL_SERVER2_PORT) != null)
							setArchivalServerPort(Integer
									.parseInt(properties
											.getProperty(CommonConstants.ARCHIVAL_SERVER2_PORT)));
						if (properties
								.getProperty(CommonConstants.RENDERING_SERVER2_PORT) != null && !properties
								.getProperty(CommonConstants.RENDERING_SERVER2_PORT).equals("")&& !properties
								.getProperty(CommonConstants.RENDERING_SERVER2_PORT).equals(0))
							setRenderServerPort(Integer
									.parseInt(properties
											.getProperty(CommonConstants.RENDERING_SERVER2_PORT)));
						if (properties
								.getProperty(CommonConstants.DATA_ROUTER_SERVER2_PORT) != null && !properties
								.getProperty(CommonConstants.DATA_ROUTER_SERVER2_PORT).equals("") && !properties
								.getProperty(CommonConstants.DATA_ROUTER_SERVER2_PORT).equals(0))
							setDataRouterPort(Integer
									.parseInt(properties
											.getProperty(CommonConstants.DATA_ROUTER_SERVER2_PORT)));


						if (properties.getProperty(CommonConstants.SERVER2_SOFTWARE_PATH) != null)
							setSoftwarePath(properties
									.getProperty(CommonConstants.SERVER2_SOFTWARE_PATH));
						if (properties.getProperty(CommonConstants.SERVER2_VAULT_SERVER_PATH) != null)
							setVaultPath(properties
									.getProperty(CommonConstants.SERVER2_VAULT_SERVER_PATH));
						if (properties.getProperty(CommonConstants.SERVER2_INSTANCE_NAME) != null)
							setServerInstanceName(properties
									.getProperty(CommonConstants.SERVER2_INSTANCE_NAME));
						return "SUCCESS1";
					}
				}else{
					status = "";
					
					if(!LoadDataTask.isMonitoring())
						status="Stopped";
					else
						status="Running";
					setStatus(status);
					return "SUCCESS2";

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			return "FAILURE";
		}
		return "SUCCESS";
	}

	public String startSchedular(){
		String message="SUCCESS";
		setTabId("System Admin");
		setSubSystemTabId("Vault Config");
	
	
		try {
			if(LoadDataTask.isMonitoring()){
				setMessage("Scheduler already started");
			}else{
				LoadDataTask.startMonitor();
				setMessage("Scheduler started successfully ");
			}
		}catch(Exception e){
	
			System.out.println("error write occured"+e);
			e.printStackTrace();
			message="FAILURE";
		}
	
		return message;
	}

	public String stopSchedular()
	{
		setTabId("System Admin");
		setSubSystemTabId("Vault Config");
		String message="SUCCESS";
		try{
			LoadDataTask.stopMonitoring();
		}catch(Exception e)
		{
			message="FAILURE";
			e.printStackTrace();
		}
		return message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute() This method is used
	 * to create archival database values
	 */
	public String VaultConfigServer1() throws Exception {
		/*String propert = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.VAULT_CONFIG_FILE_PATH)
				+ "/" + CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		 */
		HttpSession sessionku = getServletRequest().getSession();

		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		setTabId("System Admin");
		setSubSystemTabId("Vault Config");
		
		/* Nithu Alexander: 12th Nov 2014
		 * Change as part of ECMS -AVA 3.2 --> Access Control*/
		if(!((String)sessionku.getAttribute("sysaccesscontrol")).equalsIgnoreCase("true")){
			return "UNAUTHORIZED";
		}
		Long hostId = 0L;
		DataDTO dataDto = new DataDTO();
		try {
			
			/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1,AVA-2 and SCT (VaultApp)*/
			
			if(getServer()!=null && !(getServer().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumeric(getServer())){
				setMessage("Invalid Characters entered in Server Type");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			/*Never Promote this code change to production*/
			if(getServerHost()!=null && (getServerHost().trim().equals("")) /*&& !ServerSideValidationUtil.isAlphaNumeric(getServerHost())*/){
				setMessage("Invalid Characters entered in Server Host");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getServerInstanceName()!=null && !(getServerInstanceName().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumeric(getServerInstanceName())){
				setMessage("Invalid Characters entered in Server Instance Name");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(!ServerSideValidationUtil.isNumeric(String.valueOf(getArchivalServerPort()))){
				setMessage("Invalid Characters entered in Archival Server Port");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(!ServerSideValidationUtil.isNumeric(String.valueOf(getRenderServerPort()))){
				setMessage("Invalid Characters entered in Render Server Port");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(!ServerSideValidationUtil.isNumeric(String.valueOf(getDataRouterPort()))){
				setMessage("Invalid Characters entered in Data Router Port");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getSoftwarePath()!=null && !(getSoftwarePath().trim().equals("")) && !ServerSideValidationUtil.isValidFolderPathPattern(getSoftwarePath())){
				setMessage("Invalid Characters entered in Vault Data Path");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getVaultPath()!=null && !(getVaultPath().trim().equals("")) && !ServerSideValidationUtil.isValidFolderPathPattern(getVaultPath())){
				setMessage("Invalid Characters entered in Vault Server Path");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getServletRequest().getParameter("act") != null && !(getServletRequest().getParameter("act").trim().equals("")) && !ServerSideValidationUtil.isAlpha(getServletRequest().getParameter("act"))){
				setMessage("Invalid Characters in Action Type");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			
			

			String filePath = CommonConstants.VAULT_CONFIG_FILE_NAME;
			//String filePath = CommonConstants.CSR_PROPERTY_FILE_NAME;
			//	String propert = PropertyUtils.getProperties(filePath)
			//.getProperty(CommonConstants.VAULT_CONFIG_FILE_PATH)
			//+ "/" + CommonConstants.VAULT_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(filePath);

			if(getServerHost() != null && getRenderServerPort()!=0)
			{

				properties.setProperty(CommonConstants.ARCHIVAL_SERVER_HOST,
						getServerHost());

				properties.setProperty(CommonConstants.SOFTWARE_PATH,
						getSoftwarePath());
				properties.setProperty(CommonConstants.SERVER_PATH,
						getVaultPath());
				properties.setProperty(CommonConstants.ARCHIVAL_SERVER_PORT,
						String.valueOf(getArchivalServerPort()));
				properties.setProperty(CommonConstants.RENDERING_SERVER_PORT,
						String.valueOf(getRenderServerPort()));
				properties.setProperty(CommonConstants.DATA_ROUTER_SERVER_PORT,
						String.valueOf(getDataRouterPort()));
				properties.setProperty(CommonConstants.SERVER_INSTANCE_NAME,
						getServerInstanceName());
				//System.out.println("####### before storing in properties file");
				PropertyUtils.save(properties, filePath);
				//System.out.println("####### after storing in properties file");
			}

			dataDto.setHostName(getServerHost());
			//dataDto.setIpAddr(getServerIP());
			dataDto.setSoftwarePath(getSoftwarePath());
			dataDto.setVaultPath(getVaultPath());
			dataDto.setArchivalServerPort(getArchivalServerPort());
			dataDto.setRenderServerPort(getRenderServerPort());
			dataDto.setDataRouterPort(getDataRouterPort());
			dataDto.setServerInstanceName(getServerInstanceName());
			hostId = this.dataManager.getLogHost(dataDto);
			System.out.println("LogFileIntegrity-hostId:"+hostId);
			if(hostId == 0)
			{
				hostId = this.dataManager.saveLogHost(dataDto);
			}
			//	properties.store(new FileOutputStream(filePath), null);
			String value = null;	

			// properties =
			// PropertyUtils.getProperties(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
			if (getServletRequest().getParameter("act") != null
					&& getServletRequest().getParameter("act").equals("test")) {
				System.out.println("action:act"
						+ getServletRequest().getParameter("act"));

				value = Common.getDBConnections(filePath, getServerHost(),
						getRenderServerPort());
				if (value != null && value.equals("connected"))
				{
					setMessage("Permissions successfully updated! Connected to Vault Server");
					value = Common.getDBConnections(filePath, getServerHost(),
							getArchivalServerPort());
					if (value != null && value.equals("connected"))
					{
						setMessage("Permissions successfully updated! Connected to Vault Server");
						/*Nithu Alexander: 09/09/2014
						 * Below Code Snippet Uncommented as part of SIT and Eclipse Code merge*/
						value = Common.getDBConnections(filePath, getServerHost(),
								getDataRouterPort());

						if (value != null && value.equals("connected"))
						{
							setMessage("Permissions successfully updated! Connected to Vault Server");
						}
						else
							if (value != null && value.equals("error"))
								setMessage("Permissions successfully updated! Not Connected to Vault Server!!");
					}
					else
						if (value != null && value.equals("error"))
							setMessage("Permissions successfully updated! Not Connected to Vault Server!!");
				}
				else
					if (value != null && value.equals("error"))
						setMessage("Permissions successfully updated! Not Connected to Vault Server!!");

			} else {

				setMessage("Permissions successfully updated!");
			}
			System.out.println("done!!");
		} catch (Exception e) {

		}

		/*DatabaseWatcher dw=new DatabaseWatcher("D:\\jboss-5.0.1.GA\\bin");

		dw.checkVaultIndexes();*/

		return "SUCCESS";
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.ActionSupport#execute() This method is used
	 * to create archival database values
	 */
	public String VaultConfigServer2() throws Exception {
		/*String propert = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.VAULT_CONFIG_FILE_PATH)
				+ "/" + CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		 */
		HttpSession sessionku = getServletRequest().getSession();

		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		Long hostId = 0L;
		setTabId("System Admin");
		setSubSystemTabId("Vault Config");
		DataDTO dataDto = new DataDTO();
		try {

			String filePath = CommonConstants.VAULT_AUDIT_CONFIG_FILE_NAME;
			File file = new File(filePath);
			System.out.println(file.exists()+"-VaultConfigServer1-filePath:"+filePath);

			//String filePath = CommonConstants.CSR_PROPERTY_FILE_NAME;
			//	String propert = PropertyUtils.getProperties(filePath)
			//.getProperty(CommonConstants.VAULT_CONFIG_FILE_PATH)
			//+ "/" + CommonConstants.VAULT_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(filePath);

			if(getServerHost() != null && getRenderServerPort()!= 0)
			{

				properties.setProperty(CommonConstants.SERVER2_HOST,
						getServerHost());

				properties.setProperty(CommonConstants.SERVER2_SOFTWARE_PATH,
						getSoftwarePath());
				properties.setProperty(CommonConstants.SERVER2_VAULT_SERVER_PATH,
						getVaultPath());
				properties.setProperty(CommonConstants.ARCHIVAL_SERVER2_PORT,
						String.valueOf(getArchivalServerPort()));
				properties.setProperty(CommonConstants.RENDERING_SERVER2_PORT,
						String.valueOf(getRenderServerPort()));
				properties.setProperty(CommonConstants.DATA_ROUTER_SERVER2_PORT,
						String.valueOf(getDataRouterPort()));
				properties.setProperty(CommonConstants.SERVER2_INSTANCE_NAME,
						getServerInstanceName());
			}

			dataDto.setHostName(getServerHost());
			dataDto.setServerInstanceName(getServerInstanceName());
			//dataDto.setIpAddr(getServerIP());
			dataDto.setSoftwarePath(getSoftwarePath());
			dataDto.setArchivalServerPort(getArchivalServerPort());
			dataDto.setRenderServerPort(getRenderServerPort());
			dataDto.setDataRouterPort(getDataRouterPort());
			hostId = this.dataManager.getLogHost(dataDto);
			System.out.println("LogFileIntegrity-hostId:"+hostId);
			if(hostId == 0)
			{
				hostId = this.dataManager.saveLogHost(dataDto);
			}

			//	properties.store(new FileOutputStream(filePath), null);

			// properties =
			String value= null;
			// PropertyUtils.getProperties(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
			if (getServletRequest().getParameter("act") != null
					&& getServletRequest().getParameter("act").equals("test")) {
				System.out.println("action:act"
						+ getServletRequest().getParameter("act"));
				value = Common.getDBConnections(filePath, getServerHost(),
						getRenderServerPort());
				System.out.println("value after getRenderServerPort::"+value);
				if (value != null && value.equals("connected"))
				{
					setMessage("Permissions successfully updated! Connected to Vault Server");
					value = Common.getDBConnections(filePath, getServerHost(),
							getArchivalServerPort());
					System.out.println("value after getArchivalServerPort::"+value);
					if (value != null && value.equals("connected"))
					{
						setMessage("Permissions successfully updated! Connected to Vault Server");
						value = Common.getDBConnections(filePath, getServerHost(),
								getDataRouterPort());
						System.out.println("value after getDataRouterPort::"+value);
						if (value != null && value.equals("connected"))
						{
							setMessage("Permissions successfully updated! Connected to Vault Server");
						}
						else
							if (value != null && value.equals("error"))
								setMessage("Permissions successfully updated! Not Connected to Vault Server!!");
					}
					else
						if (value != null && value.equals("error"))
							setMessage("Permissions successfully updated! Not Connected to Vault Server!!");
				}
				else
					if (value != null && value.equals("error"))
						setMessage("Permissions successfully updated! Not Connected to Vault Server!!");
			} else {

				setMessage("Permissions successfully updated!");
			}
			System.out.println("done!!");
		} catch (Exception e) {

		}
		return "SUCCESS";
	}
	

}
