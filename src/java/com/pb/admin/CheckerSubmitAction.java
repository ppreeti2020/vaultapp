package com.pb.admin;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.ldap.LDAPUtils;

public class CheckerSubmitAction extends ActionSupport implements
		ServletRequestAware, ServletResponseAware {

	static final Logger logger = Logger.getLogger(CheckerSubmitAction.class);

	private String message;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private String searchIndexValues;
	private List<String> csrGroups;
	private List<String> prePopulateList;
	private List<String> nowPopulateList;

	public List<String> getNowPopulateList() {
		return nowPopulateList;
	}

	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setNowPopulateList(List<String> nowPopulateList) {
		this.nowPopulateList = nowPopulateList;
	}

	public List<String> getPrePopulateList() {
		return prePopulateList;
	}

	public void setPrePopulateList(List<String> prePopulateList) {
		this.prePopulateList = prePopulateList;
	}

	public List<String> getCsrGroups() {
		return csrGroups;
	}

	public void setCsrGroups(List<String> csrGroups) {
		this.csrGroups = csrGroups;
	}

	public String getSearchIndexValues() {
		return searchIndexValues;
	}

	public void setSearchIndexValues(String searchIndexValues) {
		this.searchIndexValues = searchIndexValues;
	}

	public HttpServletRequest getServletRequest() {
		return request;
	}

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getServletResponse() {
		return response;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public String apply() {
		prePopulateList = new ArrayList<String>();
		String permissionsApprovalFilePath = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.APPROVAL_PROPERTIES_FILE_PATH);
		Properties prePopulate = PropertyUtils
				.getProperties(permissionsApprovalFilePath);
		Enumeration en = prePopulate.keys();
		Map<String, String> mapPre = new HashMap<String, String>();
		while (en.hasMoreElements()) {
			String key = en.nextElement().toString();
			String value = prePopulate.getProperty(key);
			mapPre.put(key, value);
		}
		//System.out.println("review2");
		nowPopulateList = new ArrayList<String>();
		String permissionsFilePath = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.PERMISSION_FILE_PATH);
		Properties nowPopulate = PropertyUtils
				.getProperties(permissionsFilePath);
		en = prePopulate.keys();
		Properties properties = new Properties();
		while (en.hasMoreElements()) {
			String key = en.nextElement().toString();
			String value = mapPre.get(key);
			System.out.println("setProp:" + key + ",value:" + value);
			properties.setProperty(key, value);
		}
		try {
			properties
					.store(new FileOutputStream(PropertyUtils.getProperties(
							CommonConstants.CSR_PROPERTY_FILE_NAME)
							.getProperty(CommonConstants.PERMISSION_FILE_PATH)),
							null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		properties = PropertyUtils
				.getProperties(CommonConstants.STATUS_PROPERTIES_FILE_PATH);
		properties.setProperty(CommonConstants.STATUS_APPROVAL, "0");
		try {
			properties
					.store(new FileOutputStream(
							PropertyUtils
									.getProperties(
											CommonConstants.CSR_PROPERTY_FILE_NAME)
									.getProperty(
											CommonConstants.STATUS_PROPERTIES_FILE_PATH)),
							null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		review();
		setMessage("Permissions `updated successfully!");
		return "SUCCESS";
	}

	public void init() {
		csrGroups = LDAPUtils.getLDAPCSRGroups(CommonConstants.CSR_PROPERTY_FILE_NAME);
		searchIndexValues = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.AVAILABLE_PBIDX_VALUES);
	}

	public String review() {
		//System.out.println("review1");
		prePopulateList = new ArrayList<String>();
		String permissionsApprovalFilePath = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.APPROVAL_PROPERTIES_FILE_PATH);
		Properties prePopulate = PropertyUtils
				.getProperties(permissionsApprovalFilePath);
		Enumeration en = prePopulate.keys();
		while (en.hasMoreElements()) {
			String key = en.nextElement().toString();
			String value = prePopulate.getProperty(key);
			logger.info("-------- key: " + key + "     value: " + value);
			// multiple categories
			if (value.indexOf(",") != -1) {
				String[] tempArr = value.split(",");
				for (String temp : tempArr) {
					prePopulateList.add("LDAP_" + key + "_" + temp);
				}
				// single category
			} else {
				prePopulateList.add("LDAP_" + key + "_" + value);
			}
		}
		//System.out.println("review2");
		nowPopulateList = new ArrayList<String>();
		String permissionsFilePath = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.PERMISSION_FILE_PATH);
		Properties nowPopulate = PropertyUtils
				.getProperties(permissionsFilePath);
		en = nowPopulate.keys();
		while (en.hasMoreElements()) {
			String key = en.nextElement().toString();
			String value = nowPopulate.getProperty(key);
			logger.info("-------- key: " + key + "     value: " + value);
			// multiple categories
			if (value.indexOf(",") != -1) {
				String[] tempArr = value.split(",");
				for (String temp : tempArr) {
					nowPopulateList.add("LDAP_" + key + "_" + temp);
				}
				// single category
			} else {
				nowPopulateList.add("LDAP_" + key + "_" + value);
			}
		}
		//System.out.println("review3");
		setMessage("No Submission for approval");
		return "SUCCESS";
	}

	public String reject() {
		Properties properties = PropertyUtils
				.getProperties(CommonConstants.STATUS_PROPERTIES_FILE_PATH);
		properties.setProperty(CommonConstants.STATUS_APPROVAL, "2");
		try {
			properties
					.store(new FileOutputStream(
							PropertyUtils
									.getProperties(
											CommonConstants.CSR_PROPERTY_FILE_NAME)
									.getProperty(
											CommonConstants.STATUS_PROPERTIES_FILE_PATH)),
							null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		review();
		return "SUCCESS";
	}

	public String execute() {
		init();
		boolean isPost = request.getParameter("post_submit") != null ? true
				: false;
		if (isPost) {
			boolean isSubmit = request.getParameter("approve_permission") != null ? true
					: false;
			if (isSubmit) {
				System.out.println("Apply");
				apply();
			} else {
				System.out.println("reject");
				reject();
			}
		} else {
			System.out.println("review");
			review();
		}
		String permissionsApprovalFilePath = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.STATUS_PROPERTIES_FILE_PATH);
		Properties prePopulateApproval = PropertyUtils
				.getProperties(permissionsApprovalFilePath);
		Properties propStatus = PropertyUtils.getProperties(PropertyUtils
				.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME)
				.getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
		int status = Integer.parseInt(propStatus
				.getProperty(CommonConstants.STATUS_APPROVAL));
		setStatus(status);
		System.out.println("status" + status);
		return "SUCCESS";
	}
}
