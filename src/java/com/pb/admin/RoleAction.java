/*
 * created by suhashini
 */
package com.pb.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;
import com.pb.dao.UserDAO;
import com.pb.dto.RoleDTO;

public class RoleAction extends ActionSupport implements ServletRequestAware,
		ServletResponseAware {

	private String message;
	private String tabId;
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private String roleName;
	private String roleDesc;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.servletResponse = response;
	}

	public String execute() throws Exception {
		return "SUCCESS";
	}

	public String CreateRole() throws Exception {
		try {
			RoleDTO roleDTO = new RoleDTO();
			UserDAO userDAO = new UserDAO();
			roleName = getRoleName();
			roleDesc = getRoleDesc();
			roleDTO.setRoleName(roleName);
			roleDTO.setRoleDesc(roleDesc);
			Long roleId = userDAO.CreateNewRole(roleDTO);
			System.out.println("successfully created role : " + roleId);

		} catch (Exception e) {

		}
		return "SUCCESS";
	}

}
