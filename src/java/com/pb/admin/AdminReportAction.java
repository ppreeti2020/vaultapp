/*
 * Class for generating Reports
 */
package com.pb.admin;

import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import com.opensymphony.xwork2.ActionSupport;
import com.pb.dao.AdminReportDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.DataDTO;
import com.pb.dto.UserDTO;
public class AdminReportAction extends ActionSupport implements
		ServletRequestAware, ServletResponseAware {
	private static final long serialVersionUID = 1L;

	static final Logger logger = Logger.getLogger(AdminReportAction.class);
	public static final String XML_PROLOG = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	public static final String STORE_FRONT = "<Reports>";
	public static final String STORE_FRONT_CLOSE = "</Reports>";

	private String message;
	private String tabId;
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private String indexValues;
	private List<DataDTO> populateLoginList;
	private List<DataDTO> populateUnsuccessLoginList;
	private List<UserDTO> populateUserList;
	private List<DataDTO> populateLogoutList;
	private List<DataDTO> populateReportList;
	private List<DataDTO> populateDocList;
	private List<DataDTO> populateFreqList;
	private List<DataDTO> populateLogList;
	private String searchReport;
	private String logCount;
	private String userName;
	private String linkType;
	private AdminReportDAO reportManager = new AdminReportDAO();
	private UserDAO userManager = new UserDAO();
	
	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	public String getLinkType() {
		return linkType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public List<DataDTO> getPopulateUnsuccessLoginList() {
		return this.populateUnsuccessLoginList;
	}

	public void setPopulateUnsuccessLoginList(
			ArrayList<DataDTO> populateUnsuccessLoginList) {
		this.populateUnsuccessLoginList = populateUnsuccessLoginList;
	}

	public String getLogCount() {
		return this.logCount;
	}

	public void setLogCount(String logCount) {
		this.logCount = logCount;
	}

	public List<DataDTO> getPopulateLogList() {
		return populateLogList;
	}

	public void setPopulateLogList(ArrayList<DataDTO> populateLogList) {
		this.populateLogList = populateLogList;
	}

	public List<DataDTO> getPopulateDocList() {
		return this.populateDocList;
	}

	public void setPopulateDocList(ArrayList<DataDTO> populateDocList) {
		this.populateDocList = populateDocList;
	}

	public List<DataDTO> getPopulateFreqList() {
		return this.populateFreqList;
	}

	public void setPopulateFreqList(ArrayList<DataDTO> populateFreqList) {
		this.populateFreqList = populateFreqList;
	}

	public List<DataDTO> getPopulateLoginList() {
		return this.populateLoginList;
	}

	public void setPopulateLoginList(ArrayList<DataDTO> populateLoginList) {
		this.populateLoginList = populateLoginList;
	}

	public List<UserDTO> getPopulateUserList() {
		return this.populateUserList;
	}

	public void setPopulateUserList(ArrayList<UserDTO> populateUserList) {
		this.populateUserList = populateUserList;
	}

	public List<DataDTO> getPopulateLogoutList() {
		return this.populateLogoutList;
	}

	public void setPopulateLogoutList(ArrayList<DataDTO> populateLogoutList) {
		this.populateLogoutList = populateLogoutList;
	}

	public String getSearchReport() {
		return this.searchReport;
	}

	public void setSearchReport(String searchReport) {
		this.searchReport = searchReport;
	}

	public List<DataDTO> getPopulateReportList() {
		return this.populateReportList;
	}

	public void setPopulateReportList(ArrayList<DataDTO> populateReportList) {
		this.populateReportList = populateReportList;
	}

	public String getIndexValues() {
		return indexValues;
	}

	public void setIndexValues(String indexValues) {
		this.indexValues = indexValues;
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.servletResponse = response;
	}

	public String search() throws Exception {
		if (getServletRequest().getSession().getAttribute("userName") != null)
			setUserName(getServletRequest().getSession().getAttribute("userName").toString());
		setTabId("Reports");
		return "SUCCESS";
	}

	public String execute() {
		searchReport = servletRequest.getParameter("searchReport");
		setTabId("Reports");
		try {
			if (getServletRequest().getSession().getAttribute("userName") != null)
				setUserName(getServletRequest().getSession().getAttribute("userName").toString());
			if (searchReport != null) {
				System.out.println("Selected Report::" + searchReport);
				if (searchReport.equals("login")) {
					populateLoginList = new ArrayList<DataDTO>();
					populateLoginList = (ArrayList<DataDTO>) this.reportManager.getLoginReports();
					System.out.println("Number Of Records fetched:::"+populateLoginList.size());
				} else if (searchReport.equals("unsuccess")) {
					populateUnsuccessLoginList = new ArrayList<DataDTO>();
					populateUnsuccessLoginList = (ArrayList<DataDTO>) reportManager.getUnsuccessfulLoginReports();
					System.out.println("Number Of Records fetched:::"+populateUnsuccessLoginList.size());
				} else if (searchReport.equals("logout")) {
					populateLogoutList = new ArrayList<DataDTO>();
					populateLogoutList = (ArrayList<DataDTO>) this.reportManager.getLogoutReports();
					System.out.println("Number Of Records fetched:::"+populateLogoutList.size());
				} else if (searchReport.equals("doc")) {
					populateDocList = new ArrayList<DataDTO>();
					populateDocList = (ArrayList<DataDTO>) this.reportManager.getDocumentDetails();
					System.out.println("Number Of Records fetched:::"+populateDocList.size());
				}else if (searchReport.equals("freq")) {
					populateFreqList = new ArrayList<DataDTO>();
					populateFreqList = (ArrayList<DataDTO>) this.reportManager.getFrequentlySearchedUser();
					System.out.println("Number Of Records fetched:::"+populateFreqList.size());
				}else if (searchReport.equals("calLog")) {
					populateLogList = new ArrayList<DataDTO>();
					this.logCount = "Date" + "," + "Count" + "\n";
					populateLogList = (ArrayList<DataDTO>) this.reportManager.getCountLogged("");
					System.out.println("Number Of Records fetched:::"+populateLogList.size());
				} else if (searchReport.equals("useraccess")) {
					System.out.println("userAccess********");
					populateUserList = new ArrayList<UserDTO>();
					List<UserDTO> populateuserList = (ArrayList<UserDTO>) this.userManager.getAllUsers();
					Iterator<UserDTO> iter = populateuserList.iterator();
					while (iter.hasNext()) {
						UserDTO uDto = (UserDTO) iter.next();
						String roleName = this.userManager.getRoleName(uDto.getUserId());
						uDto.setRoleName(roleName);
						populateUserList.add(uDto);
					}
					System.out.println("Number Of Records fetched:::"+populateUserList.size());
				}  
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			addActionError("Server Down, Sorry for Inconvinience");
			return "FAILURE";
		}
		return "SUCCESS";
	}
	
}
