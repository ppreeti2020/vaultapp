package com.pb.admin;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.struts2.dispatcher.SessionMap;

import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.SearchIndexDefault;
import com.g1.e2.vault.SearchMatches;
import com.g1.e2.vault.SearchMatchesIterator;
import com.g1.e2.vault.ServerErrorException;
import com.g1.e2.vault.SearchIndex.SearchIndexMatch;
import com.g1.e2.vault.ServerLoopingException;
import com.g1.e2.vault.TooManyResultsException;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Account;
import com.g1.e2.vault.VaultClient.Database;
import com.g1.e2.vault.VaultClient.Document;
import com.g1.e2.vault.VaultException;
import com.opensymphony.xwork2.ActionContext;
import com.pb.common.CommonConstants;
import com.pb.common.CompareDate;
import com.pb.common.DateIncre;
import com.pb.common.PropertyUtils;
import com.pb.dao.UserDAO;
import com.pb.ocbc.SearchAccount;

public class Common {
	public static final String EVAULT_SESSION = "com.pb.session";

	static final Logger logger = Logger.getLogger(Common.class);
	static UserDAO uDao = new UserDAO();

	public static VaultClient getVaultConnection()
	throws NumberFormatException, UnknownHostException, IOException,
	VaultException, ServerErrorException {

		VaultClient client = new VaultClient();
		String propert = CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		String host = "";
		if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null
				|| properties
				.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != "")
			host = properties
			.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);
		/*else if (properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != null
				|| properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != "")
			host = properties.getProperty(CommonConstants.RENDERING_SERVER_IP);*/

		// Properties properties = PropertyUtils
		// .getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);
		System.out.println("Vault host::"+host);
		client.connect(
				host,
				Integer.valueOf(
						properties
						.getProperty(CommonConstants.RENDERING_SERVER_PORT))
						.intValue());

		return client;
	}

	public static String getDBConnections(String path, String IP, int portName)
	throws NumberFormatException, UnknownHostException, IOException,
	VaultException, ServerErrorException {
		String db = "";
		try {
			VaultClient client = new VaultClient();
			
			Properties properties = PropertyUtils.getProperties(path);
			client.connect(IP,

					portName);

			db = "connected";
		} catch (Exception e) {
			e.printStackTrace();
			db = "error";
		}

		return db;
	}

	public static Database getDBConnection(Long userId, Long dbId)
	throws NumberFormatException, UnknownHostException, IOException,
	VaultException, ServerErrorException {

		VaultClient client = new VaultClient();
		String propert = CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		String host = "";
		if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null
				|| properties
				.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != "")
			host = properties
			.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);
		/*else if (properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != null
				|| properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != "")
			host = properties.getProperty(CommonConstants.RENDERING_SERVER_IP);
*/
		client.connect(
				host,
				Integer.valueOf(
						properties
						.getProperty(CommonConstants.RENDERING_SERVER_PORT))
						.intValue());

		Database db = null;
		String dbName = getDBNameByUser(userId, dbId);
		db = client.getDatabase(dbName);
		return db;
	}

	public static SearchIndex getSearchIndex(Long userId, Long dbId,
			String Index) throws NumberFormatException, UnknownHostException,
			IOException, VaultException, ServerErrorException {
		System.out.println("index:getSearchIndex" + Index);
		SearchIndex SI = getDBConnection(userId, dbId).findSearchIndexByName(
				Index);

		return SI;
	}

	public static String getDBNameByUser(Long userId, Long dbId) {

		String dbName = uDao.getDBName(userId, dbId);
		return dbName;
	}

	public static Database getDBConnection(String dbName)
	throws NumberFormatException, UnknownHostException, IOException,
	VaultException, ServerErrorException {
		VaultClient client = new VaultClient();
		String propert = CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		String host = "";
		if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null
				|| properties
				.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != "")
			host = properties
			.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);
		/*else if (properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != null
				|| properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != "")
			host = properties.getProperty(CommonConstants.RENDERING_SERVER_IP);*/

		client.connect(
				host,
				Integer.valueOf(
						properties
						.getProperty(CommonConstants.RENDERING_SERVER_PORT))
						.intValue());
		Database db = null;
		System.out.println("Database Name:::"+dbName);
		
		System.out.println("Client is"+client);
		System.out.println(client.getHostname());
		System.out.println(client.getPort());
		System.out.println("Done here"+dbName);
	//	dbName="correspondences";
		
		try
		{
			
		db = client.getDatabase(dbName);
		System.out.println("After client.getDatabase");
		}
		catch(Exception e)
		{
			e.printStackTrace()
			;System.out.println(e);
		}
		//System.out.println("DB is"+db);
		//System.out.println(db.getName());
		//System.out.println(db.getDescription());
		return db;
	}

	public static void saveLogSearch(String CSR, Date dateSearch,
			String textSearch, int totalResult, List<Account> foundAccs) {

	}

	public static String getIdUser() {
		return "";
		// return
		// ActionContext.getContext().getSession().get(Common.EVAULT_SESSION)!=null?ActionContext.getContext().getSession().get(Common.EVAULT_SESSION).toString():"";
	}

	public static List<String> getAllProduct(Long userId, Long dbId)
	throws NumberFormatException, UnknownHostException, IOException,
	VaultException, ServerErrorException {
		List<String> allProduct = new ArrayList<String>();

		String dbName = getDBNameByUser(userId, dbId);
		allProduct.add(dbName);

		return allProduct;
	}

	public static int getMaxResults() {
		return 1000000000;
	}

	public static Map<String, Integer> getDetailProduct(String accountNumber,
			Long userId, Long dbId) throws NumberFormatException,
			UnknownHostException, IOException, VaultException,
			ServerErrorException {
		System.out.println("getDetailProduct" + userId);
		Database db = Common.getDBConnection(userId, dbId);
		System.out.println(db + "dbgetDetailProduct" + userId);
		Map<String, Integer> map = new HashMap<String, Integer>();
		List<String> allProduct = Common.getAllProduct(userId, dbId);
		// String product = allProduct.get(0);
		for (String product : allProduct) {
			map.put(product, 0);
			Account acc = Common.getAccountByNumber(accountNumber, product);
			List<Document> documentsSelected;
			try {
				documentsSelected = acc.getAllDocuments();
				for (Document doc : documentsSelected) {
					// if(doc.getAccountNumber().equals(accountNumber))
					// for(String key:doc.getCustomFields().keySet())
					{
						String key = product;// doc.getCustomFields().get(doc.getCustomFields().keySet().toArray()[0]);
						map.put(key, map.get(key) + 1);
					}

					// System.out.println(doc.getDocID()+":"+doc.getCustomFields().get(doc.getCustomFields().keySet().toArray()[0]));
				}
			} catch (TooManyResultsException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServerLoopingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return map;
	}

	public static String getProductIndexName() {
		return "Product";
	}

	public static String getAccountIndexName() {
		return "account";
	}

	public static String getGroupIDIndexName() {
		return "GroupID";
	}

	public static boolean isDataAccessible(String acc, String csrGroup,
			Long userId, Long dbId) throws NumberFormatException,
			UnknownHostException, VaultException, ServerErrorException,
			IOException {
		Account account = Common.getAccountByNumber(acc, userId, dbId);
		// List<String> allAccess = Common.getGroupIDAccess(csrGroup);
		return true;
	}

	public static Account getAccountByNumber(String accNumber, Long userId,
			Long dbId) throws VaultException, ServerErrorException,
			NumberFormatException, UnknownHostException, IOException {

		// List<Account> allAcc =Common.getAllAccounts();

		String dbName = getDBNameByUser(userId, dbId);
		//System.out.println("8");
		System.out.println("dbName is" + dbName);
		Database db = Common.getDBConnection(dbName);
		//System.out.println("7");
		Account account = db.getAccount(accNumber);

		if (account != null) {
			return account;

		}

		/*
		 * for(Account acc:allAcc) {
		 * if(acc.getAccountNumber().trim().toUpperCase
		 * ().equals(accNumber.trim().toUpperCase())) { return acc; } }
		 */
		return null;
	}

	public static Account getAccountByNumber(String accNumber, String dbName)
	throws VaultException, ServerErrorException, NumberFormatException,
	UnknownHostException, IOException {
		Database db = Common.getDBConnection(dbName);

		/*
		 * List<Account> allAcc = db.getAllAccounts(); for(Account acc:allAcc) {
		 * if
		 * (acc.getAccountNumber().trim().toUpperCase().equals(accNumber.trim()
		 * .toUpperCase())) { return acc; } }
		 */

		if (db == null) {
			System.err
			.printf("ERROR: database \"%s\" does not exist\n", dbName);
		} else {

			Account account = db.getAccount(accNumber);
			if (account != null) {
				return account;
			}

		}

		return null;
	}

	public static List<Account> getAllAccountByAccNumber(Long dbId,
			Long userId, String accNumber, List<Account> allAccount,
			String fromDate, String toDate) throws VaultException,
			ServerErrorException, ParseException, NumberFormatException,
			UnknownHostException, IOException {
		UserDAO uDao = new UserDAO();
		Database db = Common.getDBConnection(userId, dbId);
		List<Account> permitAccount = new ArrayList<VaultClient.Account>();
		String accountNumber = "";
		SearchMatches<Document> sm = null;
		List<Document> documents = null;
		System.out.println("fromDate:" + fromDate + " toDate:" + toDate);
		for (Account acc : allAccount) {
			/*
			 * String groupId =
			 * acc.getCustomFields().get(getAccountIndexName());
			 * 
			 * logger.info("The value of groupid is"+groupId);
			 * System.out.println("The value of groupid is"+groupId);
			 */

			if (fromDate != null && toDate != null) {
				if (!fromDate.equals("") && !toDate.equals("")) {
					Long days = CompareDate.diffDays(fromDate, toDate);
					//System.out.println("days:" + days);
					if (days > 0) {
						for (int i = 1; i <= days; i++) {
							String date = DateIncre.incrementDate(fromDate, i);
							SearchIndex searchIndex = db
							.findSearchIndexByMatcher(SearchIndex.FindsDocumentWithAccount);
							searchIndex = db
							.findSearchIndexByName(SearchIndexDefault.InvlinkSearchIndex
									.getName());
							sm = db.searchForDocumentsWithAccount(searchIndex,
									acc.getAccountNumber(), date, null,
									Integer.MAX_VALUE);
							documents = sm.getMatches();
							for (Document doc : documents) {
								if (doc.getAccountNumber().equals(
										acc.getAccountNumber())){
									System.out.println("from Date & to Date not null::"+acc);
									permitAccount.add(acc);
								}
							}
						}
					} else {
						SearchIndex searchIndex = db
						.findSearchIndexByMatcher(SearchIndex.FindsDocumentWithAccount);

						if (fromDate != null) {
							SimpleDateFormat simpformat = new SimpleDateFormat(
									"dd-MM-yyyy");
							Date testfromDate = simpformat.parse(fromDate);
							Calendar calc = Calendar.getInstance();
							calc.setTime(testfromDate);
							SimpleDateFormat formats = new SimpleDateFormat(
							"yyyy-MM-dd");
							searchIndex = db
							.findSearchIndexByName(SearchIndexDefault.InvlinkSearchIndex
									.getName());

							sm = db.searchForDocumentsWithAccount(searchIndex,
									acc.getAccountNumber(),
									formats.format(calc.getTime()), null,
									Integer.MAX_VALUE);
						} else
							sm = db.searchForDocumentsWithAccount(searchIndex,
									acc.getAccountNumber(), "", null,
									Integer.MAX_VALUE);

						documents = sm.getMatches();
						for (Document doc : documents) {
							if (doc.getAccountNumber().equals(
									acc.getAccountNumber()))
								permitAccount.add(acc);
							System.out.println("to Date not null::"+acc);
						}
					}
				} else{
					permitAccount.add(acc);
					System.out.println("from Date & to Date null::"+acc);
				}

				/*
				 * else { SearchIndex searchIndex =
				 * db.findSearchIndexByMatcher(SearchIndex
				 * .FindsDocumentWithAccount);
				 * 
				 * sm = db.searchForDocumentsWithAccount(searchIndex,
				 * acc.getAccountNumber(),"", null, Integer.MAX_VALUE);
				 * 
				 * documents = sm.getMatches(); for(Document doc :documents) {
				 * if(doc.getAccountNumber().equals(acc.getAccountNumber()))
				 * permitAccount.add(acc); } }
				 */
			} else {
				permitAccount.add(acc);
			}

			/*
			 * if(isCanAccess(groupId, allAccess)) {
			 * 
			 * }
			 */

			System.out.println("The test value is:common" + acc);

		}
		return permitAccount;
	}

	/*
	 * public static List<Account> getAllAccountWithoutAccount(Long dbId,Long
	 * userId, String fromDate,String toDate) throws VaultException,
	 * ServerErrorException, ParseException, NumberFormatException,
	 * UnknownHostException, IOException { UserDAO uDao = new UserDAO();
	 * Database db = Common.getDBConnection(userId,dbId); List<Account>
	 * permitAccount = new ArrayList<VaultClient.Account>(); String
	 * accountNumber = ""; SearchMatches<Document> sm = null; List<Document>
	 * documents = null;
	 * System.out.println("fromDate:"+fromDate+" toDate:"+toDate); /* String
	 * groupId = acc.getCustomFields().get(getAccountIndexName());
	 * 
	 * logger.info("The value of groupid is"+groupId);
	 * System.out.println("The value of groupid is"+groupId);
	 */
	/*
	 * Long days = CompareDate.diffDays(fromDate, toDate);
	 * System.out.println("days:"+days); if(days > 0) { for(int i=1;i<days;i++)
	 * { String date = DateIncre.incrementDate(fromDate,i); SearchIndex
	 * searchIndex =
	 * db.findSearchIndexByMatcher(SearchIndex.FindsDocumentWithAccount);
	 * searchIndex =
	 * db.findSearchIndexByName(SearchIndexDefault.InvlinkSearchIndex
	 * .getName()); sm = db.searchForDocumentsWithAccount(searchIndex,
	 * acc.getAccountNumber(),date, null, Integer.MAX_VALUE); documents =
	 * sm.getMatches(); for(Document doc :documents) { permitAccount.add(acc);
	 * 
	 * } } } else { SimpleDateFormat simpformat = new
	 * SimpleDateFormat("dd-MM-yyyy"); Date testfromDate =
	 * simpformat.parse(fromDate); Calendar calc = Calendar.getInstance();
	 * calc.setTime(testfromDate); SimpleDateFormat formats = new
	 * SimpleDateFormat("yyyy-MM-dd"); SearchIndex searchIndex =
	 * db.findSearchIndexByMatcher(SearchIndex.FindsDocumentWithAccount);
	 * searchIndex =
	 * db.findSearchIndexByName(SearchIndexDefault.InvlinkSearchIndex
	 * .getName()); sm = db.searchForDocumentsWithAccount(searchIndex,
	 * acc.getAccountNumber(),formats.format(calc.getTime()), null,
	 * Integer.MAX_VALUE); documents = sm.getMatches(); for(Document doc
	 * :documents) { permitAccount.add(doc.getAccountNumber()); } } /*
	 * if(isCanAccess(groupId, allAccess)) {
	 * 
	 * }
	 */

	/*
	 * System.out.println("The test value is:common"+acc );
	 * 
	 * 
	 * return permitAccount; }
	 */
	public static List<Account> getAllAccounts(Long userId, Long dbId)
	throws NumberFormatException, UnknownHostException, IOException,
	VaultException, ServerErrorException {

		List<Account> allAccount = new ArrayList<VaultClient.Account>();
		//System.out.println("8");
		Map<String, Account> map = new HashMap<String, VaultClient.Account>();
		//System.out.println("9");
		String dbName = getDBNameByUser(userId, dbId);
		System.out.println("dbName is" + dbName);
		Database db = Common.getDBConnection(dbName);
		//System.out.println("7");
		SearchIndex index = db.findSearchIndexByName(getAccountIndexName());
		//System.out.println("10");
		SearchMatches<Account> accounts = db.searchForAccounts(index, "", "",
				getMaxResults());
		//System.out.println("11");

		for (Account acc : accounts.getMatches()) {
			//System.out.println("12");
			map.put(acc.getAccountNumber(), acc);
		}

		for (String key : map.keySet()) {
			//System.out.println("13");
			allAccount.add(map.get(key));
		}
		return allAccount;
	}

	public static List<String> getGroupIDAccess(String csrGroup) {
		List<String> allGroup = new ArrayList<String>();
		try {
			String permissionsFilePath = PropertyUtils.getProperties(
					CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
							CommonConstants.PERMISSION_FILE_PATH);
			Properties prop = PropertyUtils.getProperties(permissionsFilePath);
			String[] permissions = prop.getProperty(csrGroup).split(",");
			for (String perm : permissions) {
				logger.info("The value of permissions is" + perm);
				System.out.println("The value of permissions is" + perm);
				allGroup.add(perm.trim());
			}
		} catch (Exception ex) {
			System.out.println("getGroupIDAccess:" + ex.getMessage());
		}
		return allGroup;
	}

	public static List<Document> getAllDocumentByAccNumberAndProductName(
			String accNumber, String prodName, Long userId, Long dbId)
			throws NumberFormatException, UnknownHostException, IOException,
			VaultException, ServerErrorException {
		Database db = Common.getDBConnection(userId, dbId);
		Account account = Common.getAccountByNumber(accNumber, userId, dbId);
		// SearchIndex accIndex =
		// db.findSearchIndexByName(getProductIndexName());
		System.out.println("account:" + account);
		List<Document> documentsSelected;
		List<Document> listDocuments = new ArrayList<VaultClient.Document>();
		try {
			documentsSelected = account.getAllDocuments();
			for (Document doc : documentsSelected) {
				// String key =
				// doc.getCustomFields().get(doc.getCustomFields().keySet().toArray()[0]);
				// if(doc.getAccountNumber().equals(accNumber) &&
				// key.trim().toUpperCase().equals(prodName.trim().toUpperCase()))
				{
					listDocuments.add(doc);
				}
			}
		} catch (TooManyResultsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServerLoopingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listDocuments;
	}
	public static VaultClient getVaultConnection(String host,int port,String server)
	throws NumberFormatException, UnknownHostException, IOException,
	VaultException, ServerErrorException {

		VaultClient client = new VaultClient();
		System.out.println("host:"+host+"-port:"+port);
		client.connect(host,port);
		return client;
	}
}
