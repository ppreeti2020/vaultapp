package com.pb.admin;

//import java.io.File;
import java.io.FileOutputStream;
//import java.util.Enumeration;
//import java.util.HashMap;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;
import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;
import com.pb.common.PropertyUtils;
import com.pb.common.ServerSideValidationUtil;
import com.pb.ldap.LDAPUtils;


public class LDAPConfigAction extends ActionSupport implements
		ServletRequestAware, ServletResponseAware {

	private String message;
	private String tabId;
	
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private String ldapHost;
	private int ldapPort;
	private String ldapNamingCtx;
	private String ldapUserDn;
	private String ldapUserFilter;
	private String ldapUserName;
	private String ldapGrpFilter;
	private String ldapGrpName;
	private String ldapGrpAttr;
	private String ldapGrpMember;
	private String userName;
	private String password;
	private String tabName;
	private String subTabName;
	private String subSystemTabId;
	private String secureLdap;
	private String sslTrustPass;
	private String sslTrustPath;
	
	
	
	public void setSslTrustPath(String sslTrustPath) {
		this.sslTrustPath = sslTrustPath;
	}

	public String getSslTrustPath() {
		return sslTrustPath;
	}

	public void setSslTrustPass(String sslTrustPass) {
		this.sslTrustPass = sslTrustPass;
	}

	public String getSslTrustPass() {
		return sslTrustPass;
	}

	public void setSecureLdap(String secureLdap) {
		this.secureLdap = secureLdap;
	}

	public String getSecureLdap() {
		return secureLdap;
	}

	public void setSubSystemTabId(String subSystemTabId) {
		this.subSystemTabId = subSystemTabId;
	}

	public String getSubSystemTabId() {
		return subSystemTabId;
	}

	public String getSubTabName() {
		return subTabName;
	}

	public void setSubTabName(String subTabName) {
		this.subTabName = subTabName;
	}

	public String getTabName() {
		return tabName;
	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLdapHost() {
		return ldapHost;
	}

	public void setLdapHost(String ldapHost) {
		this.ldapHost = ldapHost;
	}

	public int getLdapPort() {
		return ldapPort;
	}

	public void setLdapPort(int ldapPort) {
		this.ldapPort = ldapPort;
	}

	public String getLdapNamingCtx() {
		return ldapNamingCtx;
	}

	public void setLdapNamingCtx(String ldapNamingCtx) {
		this.ldapNamingCtx = ldapNamingCtx;
	}

	public String getLdapUserDn() {
		return ldapUserDn;
	}

	public void setLdapUserDn(String ldapUserDn) {
		this.ldapUserDn = ldapUserDn;
	}

	public String getLdapUserFilter() {
		return ldapUserFilter;
	}

	public void setLdapUserFilter(String ldapUserFilter) {
		this.ldapUserFilter = ldapUserFilter;
	}

	public String getLdapUserName() {
		return ldapUserName;
	}

	public void setLdapUserName(String ldapUserName) {
		this.ldapUserName = ldapUserName;
	}

	public String getLdapGrpFilter() {
		return ldapGrpFilter;
	}

	public void setLdapGrpFilter(String ldapGrpFilter) {
		this.ldapGrpFilter = ldapGrpFilter;
	}

	public String getLdapGrpName() {
		return ldapGrpName;
	}

	public void setLdapGrpName(String ldapGrpName) {
		this.ldapGrpName = ldapGrpName;
	}

	public String getLdapGrpAttr() {
		return ldapGrpAttr;
	}

	public void setLdapGrpAttr(String ldapGrpAttr) {
		this.ldapGrpAttr = ldapGrpAttr;
	}

	public String getLdapGrpMember() {
		return ldapGrpMember;
	}

	public void setLdapGrpMember(String ldapGrpMember) {
		this.ldapGrpMember = ldapGrpMember;
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.servletResponse = response;
	}

	public String execute() throws Exception {
		//String filePath = CommonConstants.CSR_PROPERTY_FILE_NAME;
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		HttpSession sessionku = getServletRequest().getSession();
		/*
		 * int index =
		 * getServletRequest().getRealPath("/").lastIndexOf("server");
		 * System.out.println(getServletRequest().getRealPath("/").substring(0,
		 * index)+"..path.."+ getServletRequest().getServletPath()); String
		 * realPath = getServletRequest().getRealPath("/").substring(0, index);
		 * String filePath =
		 * realPath+"\\bin\\"+CommonConstants.LDAP_CONFIG_FILE_NAME; File file =
		 * new File(filePath); if(!file.exists()) { file.createNewFile(); }
		 * Properties properties = PropertyUtils.getProperties(propert);
		 */
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		
		System.out.println("sysaccesscontrol is"+(String)sessionku.getAttribute("sysaccesscontrol"));
		
		if(!((String)sessionku.getAttribute("sysaccesscontrol")).equalsIgnoreCase("true")){
			return "UNAUTHORIZED";
		}

		setTabId("System Admin");
		setSubSystemTabId("LDAP Config");
		if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
		{
			setSecureLdap(properties.getProperty(CommonConstants.ENABLE_SSL));
					//getSecureLdap());
			if(getSecureLdap().equalsIgnoreCase("T"))
			{
				if(properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD) != null)
					setSslTrustPass(properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD));
				if(properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH) != null)
					setSslTrustPath(properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH));
			}
		}
		if (properties.getProperty(CommonConstants.LDAP_Port) != null) {
			System.out.println(properties
					.getProperty(CommonConstants.LDAP_Port));
			setLdapPort(Integer.parseInt(properties
					.getProperty(CommonConstants.LDAP_Port)));
		}
		if (properties.getProperty(CommonConstants.LDAP_Host) != null)
			setLdapHost(properties.getProperty(CommonConstants.LDAP_Host));
		if (properties.getProperty(CommonConstants.LDAP_Naming_Context) != null)
			setLdapNamingCtx(properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
		if (properties.getProperty(CommonConstants.LDAP_User_DN) != null)
			setLdapUserDn(properties.getProperty(CommonConstants.LDAP_User_DN));
		if (properties.getProperty(CommonConstants.LDAP_User_Filter) != null)
			setLdapUserFilter(properties
					.getProperty(CommonConstants.LDAP_User_Filter));
		if (properties.getProperty(CommonConstants.USER_NAME) != null)
			setUserName(properties.getProperty(CommonConstants.USER_NAME));
		if (properties.getProperty(CommonConstants.LDAP_User_Name_Attribute) != null)
			setLdapUserName(properties
					.getProperty(CommonConstants.LDAP_User_Name_Attribute));
		if (properties.getProperty(CommonConstants.LDAP_Group_Filter) != null)
			setLdapGrpFilter(properties
					.getProperty(CommonConstants.LDAP_Group_Filter));
		if (properties.getProperty(CommonConstants.LDAP_Group_Name_Attribute) != null)
			setLdapGrpName(properties
					.getProperty(CommonConstants.LDAP_Group_Name_Attribute));
		if (properties.getProperty(CommonConstants.LDAP_Group_Member_Attribute) != null)
			setLdapGrpMember(properties
					.getProperty(CommonConstants.LDAP_Group_Member_Attribute));
		if (properties.getProperty(CommonConstants.Password) != null)
			
		{
		String decPassword=	"";
		try
		{
		decPassword=	DataEncryptDecrypt.decryptData(properties.getProperty(CommonConstants.Password));
		}
		catch(Exception e)
		{
			decPassword=properties.getProperty(CommonConstants.Password);
		}
			
			
			setPassword(decPassword);
		}
			
			return "SUCCESS";

	}

	public String ldapConfig() throws Exception {
		// HashMap<String, String> propertiesMap = new HashMap<String,
		// String>();
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		HttpSession sessionku = getServletRequest().getSession();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
			
		}
		
		if(!((String)sessionku.getAttribute("sysaccesscontrol")).equalsIgnoreCase("true")){
			return "UNAUTHORIZED";
		}
		try {
			/*
			 * int index =
			 * getServletRequest().getRealPath("/").lastIndexOf("server");
			 * System
			 * .out.println(getServletRequest().getRealPath("/").substring(0,
			 * index)+"..path.."+ getServletRequest().getServletPath()); String
			 * realPath = getServletRequest().getRealPath("/").substring(0,
			 * index); String filePath =
			 * realPath+"\\bin\\"+CommonConstants.LDAP_CONFIG_FILE_NAME; File
			 * file = new File(filePath); if(!file.exists()) {
			 * file.createNewFile(); } Properties properties =
			 * PropertyUtils.getProperties(filePath);
			 */
			
			
			setTabId("System Admin");
			setSubSystemTabId("LDAP Config");
			
			/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1,AVA-2 and SCT (VaultApp)*/
			
			if(getSecureLdap()!=null && !(getSecureLdap().trim().equals("")) && !ServerSideValidationUtil.isAlpha(getSecureLdap())){
				setMessage("Invalid Characters in Secure LDAP Indicator");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getLdapHost()!=null && !(getLdapHost().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithDOT(getLdapHost())){
				setMessage("Invalid Characters entered in LDAP Host Name");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(!ServerSideValidationUtil.isNumeric(String.valueOf(getLdapPort()))){
				setMessage("Invalid Characters entered in LDAP Port Number");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getLdapNamingCtx()!=null && !(getLdapNamingCtx().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumeric(getLdapNamingCtx()) && !ServerSideValidationUtil.checkValidLDAPCharacters(getLdapNamingCtx())){
				setMessage("Invalid Characters entered in LDAP Naming Context");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getLdapUserName()!=null && !(getLdapUserName().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumeric(getLdapUserName()) && !ServerSideValidationUtil.checkValidLDAPCharacters(getLdapUserName()) ){
				setMessage("Invalid Characters entered in LDAP User Name");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getPassword()!=null && !(getPassword().trim().equals("")) && !ServerSideValidationUtil.isValidPasswordPattern(getPassword())){
				setMessage("Invalid Characters entered in LDAP Password");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getLdapUserDn()!=null && !(getLdapUserDn().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumeric(getLdapUserDn()) && !ServerSideValidationUtil.checkValidLDAPCharacters(getLdapUserDn())){
				setMessage("Invalid Characters entered in LDAP User DN");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getLdapUserFilter()!=null && !(getLdapUserFilter().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumeric(getLdapUserFilter()) && !ServerSideValidationUtil.checkValidLDAPCharacters(getLdapUserFilter())){
				setMessage("Invalid Characters entered in LDAP User Filter");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getLdapUserName()!=null && !(getLdapUserName().trim().equals("")) && !ServerSideValidationUtil.isAlpha(getLdapUserName())){
				setMessage("Invalid Characters entered in LDAP User Name Attribute");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getLdapGrpFilter()!=null && !(getLdapGrpFilter().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumeric(getLdapGrpFilter()) && !ServerSideValidationUtil.checkValidLDAPCharacters(getLdapGrpFilter())){
				setMessage("Invalid Characters entered in LDAP Group Filter");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getLdapGrpName()!=null && !(getLdapGrpName().trim().equals("")) && !ServerSideValidationUtil.isAlpha(getLdapGrpName())){
				setMessage("Invalid Characters entered in LDAP Group Name");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getLdapGrpMember()!=null && !(getLdapGrpMember().trim().equals("")) && !ServerSideValidationUtil.isAlpha(getLdapGrpMember())){
				setMessage("Invalid Characters entered in LDAP Group Member");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getServletRequest().getParameter("act") != null && !(getServletRequest().getParameter("act").trim().equals("")) && !ServerSideValidationUtil.isAlpha(getServletRequest().getParameter("act"))){
				setMessage("Invalid Characters in Action Type");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			System.out.println(getServletRequest().getParameter("act")
					+ "config****" + getLdapPort());
			if(getSecureLdap() != null)
			{
				
				properties.setProperty(CommonConstants.ENABLE_SSL,
						getSecureLdap());
				if(getSecureLdap().equalsIgnoreCase("T"))
				{
					properties.setProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD,
							getSslTrustPass());
					properties.setProperty(CommonConstants.LDAP_TRUST_STORE_PATH,
							getSslTrustPath());
				}
			properties.setProperty(CommonConstants.LDAP_Port,
					String.valueOf(getLdapPort()));

			properties.setProperty(
					CommonConstants.LDAP_INITIAL_CONTEXT_FACTORY,
					CommonConstants.LDAP_INITIAL_CTX_FACTORY);
			properties.setProperty(CommonConstants.LDAP_Naming_Context,
					getLdapNamingCtx());
			properties.setProperty(CommonConstants.LDAP_Host, getLdapHost());
			properties.setProperty(CommonConstants.LDAP_User_DN,
					getLdapUserDn());
			properties.setProperty(CommonConstants.LDAP_User_Filter,
					getLdapUserFilter());
			properties.setProperty(CommonConstants.LDAP_User_Name_Attribute,
					getLdapUserName());
			properties.setProperty(CommonConstants.LDAP_Group_Filter,
					getLdapGrpFilter());
			properties.setProperty(CommonConstants.LDAP_Group_Name_Attribute,
					getLdapGrpName());
			properties.setProperty(CommonConstants.LDAP_Group_Member_Attribute,
					getLdapGrpMember());
			properties.setProperty(CommonConstants.USER_NAME, getUserName());

			String pwd=DataEncryptDecrypt.encryptData(getPassword());
					
			properties.setProperty(CommonConstants.Password,  pwd);

			//properties.store(new FileOutputStream(propert), null);
			
			PropertyUtils.save(properties, propert);
			// properties =
			// PropertyUtils.getProperties(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
			if (getServletRequest().getParameter("act") != null
					&& getServletRequest().getParameter("act")
							.equalsIgnoreCase("testldap")) {
				System.out.println("action:act"
						+ getServletRequest().getParameter("act"));
				String group = LDAPUtils.authenticateUserForLdapConfig(getUserName(),
						getPassword());
				if (group != null && group.equals("error"))
					setMessage("Permissions successfully updated! Error authenticating user!! ");
				else if (group != null)
					setMessage("Permissions successfully updated! Connected to LDAP Server");
			} if (getServletRequest().getParameter("act") != null
					&& getServletRequest().getParameter("act")
							.equalsIgnoreCase("saveldap")) {
				setMessage("Permissions successfully updated!");
			}
			System.out.println("done!!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "SUCCESS";
	}
	
	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public String getTabId() {
		return tabId;
	}

}
