
package com.pb.admin;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;
import com.pb.common.PropertyUtils;
import com.pb.dao.CommonDAO;

public class TestApp {
	
	public static void main(String[] args) throws Exception {
		
		String str3 = "DsfuY7wzi1cn7dtqDNfscw==";
		//System.out.println("Decrypted data String1:::"+encryptData("Y"));
				
		
	/*	System.out.println("Decrypted data String1:::"+encryptData(str2));*/
		System.out.println("Decrypted data String1:::"+decryptData(str3));
		/*System.out.println("Decrypted data String1:::"+decryptData(str4));
		System.out.println("Decrypted data String1:::"+decryptData(str5));
		System.out.println("Decrypted data String1:::"+decryptData(str6));*/
		
	
	/*	LogStrTok lst = new LogStrTok();
		String comp = "sorted queue flush [0] entries for [\\\\WIN642008\\Vault_Storage\\INDEX\\personel_letters\\account.dri]";
		System.out.println(comp.split("sorted queue flush")[1]);
		String comp1 =  comp.split("sorted queue flush")[1];
		int indexFiles = lst.StringAccurance(comp1, "INDEX");
		System.out.println("count1" + indexFiles);*/
		
	//	String fname = (CommonConstants.CSR_PROPERTY_FILE_NAME+"\\"+CommonConstants.CSR_PROPERTY_FILE_NAME);
	//	System.out.println("FileName::"+fname);
	}
	public static String encryptData(String str) throws Exception {
		// obtaining byte array from string argument
		byte[] valuesBytes = str.getBytes();

		// Encrypting the data
		byte[] encrypted;
		try {
			encrypted = getCipher("ENCRYPT_MODE").doFinal(valuesBytes);
		} catch (Exception e) {
			throw new Exception(e);
		}

		// return encrypted value
		return new String(new BASE64Encoder().encode(encrypted));
	}

	/**
	 * Return decrypted value for a given encrypted String argument.
	 * 
	 * @param str
	 * @return decrypted string value.
	 * @throws Exception
	 */
	public static String decryptData(String str) throws Exception {
		try {
			// decrypting the data
			byte[] actualValue = getCipher("DECRYPT_MODE").doFinal(
					new BASE64Decoder().decodeBuffer(str));
			// return decrypted value
			return new String(actualValue, "UTF-8");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception("Error decrypting " + str);
			
		}
	}

	
	private static Cipher getCipher(String mode) throws Exception {
		// encryption Key in byte array
		/*
		 * byte[] encryptKey = { (byte) 0x91, (byte) 0x02, (byte) 0x9, (byte)
		 * 0x44, (byte) 0x85, (byte) 0x66, (byte) 0x23, (byte) 0x08};CSR_PROPERTY_FILE_NAME
		 */
		String saltKey = "CSR APPS VAULT APPLICATION WITH PITNEY BOWES BUSINESS INSIGHT CO";
		// String saltKey =
		// "CSR APPS VAULT APPLICATION WITH PITNEY BOWES BUSINESS INSIGHT CO";
		// System.out.println("lenght"+saltKey.length());
		byte[] encryptKey = saltKey.getBytes();
		/*
		 * KeyGenerator gen = KeyGenerator.getInstance("AES/CTR/PKCS5PADDING");
		 * gen.init(256); SecretKey k = gen.generateKey(); Cipher ciph =
		 * Cipher.getInstance("AES"); ciph.init(Cipher.ENCRYPT_MODE, k);
		 */
		// initialization of vector (necessary for CBC mode)
		IvParameterSpec IvParameters = new IvParameterSpec(new byte[] { 92, 48,
				36, 78, 91, 80, 05, 43 });
		// creating a DES key spec from the byte array key
		DESedeKeySpec spec = new DESedeKeySpec(encryptKey);

		// initializing secret key factory for generating DES keys
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");

		// creating a DES SecretKey object
		SecretKey theKey = keyFactory.generateSecret(spec);

		// obtaining a DES Cipher object
		// aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");

		if (mode.equalsIgnoreCase("ENCRYPT_MODE")) {
			// Initializing the cipher and put it into encrypt mode
			cipher.init(Cipher.ENCRYPT_MODE, theKey, IvParameters);
		} else if (mode.equalsIgnoreCase("DECRYPT_MODE")) {
			// Initializing the cipher and put it into decrypt mode
			cipher.init(Cipher.DECRYPT_MODE, theKey, IvParameters);
		}
		return cipher;
	}
	
	
}
