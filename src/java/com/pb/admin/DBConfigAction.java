package com.pb.admin;


import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;
import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;
import com.pb.common.PropertyUtils;
import com.pb.common.ServerSideValidationUtil;
import com.pb.dao.CommonDBConnection;


public class DBConfigAction extends ActionSupport implements
ServletRequestAware, ServletResponseAware {

	private String message;
	//	private String tabId;
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private String serverType="MS SQL Server";
	private String serverHost;

	private int serverPort;
	private String dbName;
	private String dbUsername;
	private String password;
	private List<String> servers;
	private List<String> dbList;
	private String tabId;
	private String subSystemTabId;
	private String subAccessTabId;
	private String serverIP;

	
	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public String getServerIP() {
		return serverIP;
	}
	
	public void setSubSystemTabId(String subSystemTabId) {
		this.subSystemTabId = subSystemTabId;
	}

	public String getSubSystemTabId() {
		return subSystemTabId;
	}
	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public String getTabId() {
		return tabId;
	}
	
	public void setSubAccessTabId(String subAccessTabId) {
		this.subAccessTabId = subAccessTabId;
	}

	public String getSubAccessTabId() {
		return subAccessTabId;
	}

	public List<String> getDbList() {
		return dbList;
	}

	public void setDbList(List<String> dbList) {
		this.dbList = dbList;
	}

	public List<String> getServers() {
		return servers;
	}

	public void setServers(List<String> servers) {
		this.servers = servers;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getServerHost() {
		return serverHost;
	}

	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}


	public String getServerType() {
		return serverType;
	}

	public void setServerType(String serverType) {
		this.serverType = serverType;
	}

	/**
	 * @return the userName
	 */
	public String getDbUsername() {
		return dbUsername;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.servletResponse = response;
	}

	public String execute() throws Exception {
		System.out.println("getDbLists()" + getServerType());
		HttpSession sessionku = getServletRequest().getSession();

		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		
		if(!((String)sessionku.getAttribute("sysaccesscontrol")).equalsIgnoreCase("true")){
			return "UNAUTHORIZED";
		}
		
		
		setTabId("System Admin");
		setSubSystemTabId("DB Config");
		setSubAccessTabId("DB Config");
		servers = CommonDBConnection.getDBList();
		String propert = CommonConstants.DB_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(propert);

			if (properties.getProperty(CommonConstants.DB_SERVER_TYPE) != null
					&& properties.getProperty(CommonConstants.DB_SERVER_HOST) != null

					&& properties.getProperty(CommonConstants.DB_SERVER_PORT) != null
					&& properties.getProperty(CommonConstants.DB_USER_NAME) != null
					&& properties.getProperty(CommonConstants.DB_PASSWORD) != null) {
				setServerType(properties
						.getProperty(CommonConstants.DB_SERVER_TYPE));
				setServerHost(properties
						.getProperty(CommonConstants.DB_SERVER_HOST));
				setServerPort(Integer.parseInt(properties
						.getProperty(CommonConstants.DB_SERVER_PORT)));
				setServerIP(properties.getProperty(CommonConstants.DB_SERVER_IP));
				setDbUsername(properties.getProperty(CommonConstants.DB_USER_NAME));
				if (properties.getProperty(CommonConstants.DATABASE_NAME) != null)
					setDbName(properties.getProperty(CommonConstants.DATABASE_NAME));

				
				

				String decPassword=	"";
				try
				{
				decPassword=	DataEncryptDecrypt.decryptData(properties.getProperty(CommonConstants.DB_PASSWORD));
				}
				catch(Exception e)
				{
					decPassword=properties.getProperty(CommonConstants.DB_PASSWORD);
				}
				
				
				setPassword(decPassword);

				String url = CommonDBConnection.getURL(
						properties.getProperty(CommonConstants.DB_SERVER_TYPE),
						properties.getProperty(CommonConstants.DB_SERVER_HOST),
						properties.getProperty(CommonConstants.DB_SERVER_PORT));
				//System.out.println("@@@@@@@@@@@@@@@@@@@@@@ BEFORE CALLING DB LIST");
				dbList = CommonDBConnection
				.getDBList(CommonDBConnection.getConnection(properties.getProperty(CommonConstants.DB_SERVER_TYPE),url,properties.getProperty(CommonConstants.DB_USER_NAME),
								decPassword), properties
								.getProperty(CommonConstants.DB_SERVER_TYPE));
				//System.out.println("@@@@@@@@@@@@@@@@@@@@@@ AFTER CALLING DB LIST");
								/*properties
								.getProperty(CommonConstants.DB_SERVER_TYPE))*/;
			}
		
		return "SUCCESS";
	}

	/*public String getDbLists() throws Exception {
		System.out.println("getDbLists()" + getServerType()+" "+getServerIP()+" "+getServerHost()+" "+getPassword());
		servers = CommonDBConnection.getDBList();
		if (getServerType() != null && getServerPort() != 0
				&& getServerHost() != null && getServerIP() != null
				&& getDbUsername() != null && getPassword() != null) {
			String url = CommonDBConnection.getURL(getServerType(),
					getServerHost(), String.valueOf(getServerPort()));
			System.out.println("url:"+url);
			dbList = CommonDBConnection.getDBList(CommonDBConnection
					.getConnection(getServerType(), url, getDbUsername(),
							getPassword()), getServerType());
		} else {
			System.out.println("Please Enter Values!!");
			setMessage("Please Enter Values!!");
		}
		return "SUCCESS";

	}
	 */
	public String dbConfig() throws Exception {
		
		HttpSession sessionku=getServletRequest().getSession();
		
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		
		if(!((String)sessionku.getAttribute("sysaccesscontrol")).equalsIgnoreCase("true")){
			return "UNAUTHORIZED";
		}
		servers = CommonDBConnection.getDBList();
		String propert = CommonConstants.DB_CONFIG_FILE_NAME;
		setTabId("System Admin");
		setSubSystemTabId("DB Config");
		setSubAccessTabId("DB Config");
		String url = "";
		try {
			/* Nithu Alexander: Cross-Site Scripting ("XSS") fixes as part of AVA-1,AVA-2 and SCT (VaultApp)*/
			
			if(getServerType()!=null && !(getServerType().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithSpace(getServerType())){
				setMessage("Invalid Characters in Selected Server Type");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getServerHost()!=null && !(getServerHost().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithHyphenAndUnderscore(getServerHost())){
				setMessage("Invalid Characters entered in Server Host");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(!ServerSideValidationUtil.isNumeric(String.valueOf(getServerPort()))){
				setMessage("Invalid Characters entered in Server Port Number");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getServerIP()!=null && (getServerIP().trim().equals("")) /*&& !ServerSideValidationUtil.isNumericWithDOT(getServerIP())*/){
				setMessage("Invalid Characters entered in Server IP Address");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getDbName()!=null && !(getDbName().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumericWithHyphenAndUnderscore(getDbName())){
				setMessage("Invalid Characters entered in DataBase Name");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getDbUsername()!=null && !(getDbUsername().trim().equals("")) && !ServerSideValidationUtil.isAlphaNumeric(getDbUsername())){
				setMessage("Invalid Characters entered in DataBase User Name");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getPassword()!=null && !(getPassword().trim().equals("")) && !ServerSideValidationUtil.isValidPasswordPattern(getPassword())){
				setMessage("Invalid Characters entered in DataBase Password");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			if(getServletRequest().getParameter("act") != null && !(getServletRequest().getParameter("act").trim().equals("")) && !ServerSideValidationUtil.isAlpha(getServletRequest().getParameter("act"))){
				setMessage("Invalid Characters in Action Type");
				tabId = "System Admin";
				return "INVALID_PARAMETERS";
			}
			
			System.out.println("Inside dbconfig");
			if (getServerType() != null && getServerPort() != 0
					&& getServerHost() != null 
					&& getDbUsername() != null && getPassword() != null) {
				if(!getServerHost().equals(""))
				{
					url = CommonDBConnection.getURL(getServerType(),
							getServerHost(), String.valueOf(getServerPort()));
				}

				System.out.println("parameter:"+getServletRequest().getParameter("act"));
				//System.out.println("attribute:"+getServletRequest().getAttribute("act"));
				dbList = CommonDBConnection.getDBList(CommonDBConnection
						.getConnection(getServerType(), url, getDbUsername(),
								getPassword()), getServerType());
				System.out.println("dbList"+dbList);
				if(dbList == null ||dbList.size() == 0)
					setMessage("Problem connecting with Server");
				if (getServletRequest().getParameter("act") != null
						&& getServletRequest().getParameter("act").equals(
						"dblist")) {
					return "SUCCESS";
				}

				
				Properties properties = PropertyUtils.getProperties(propert);

				System.out.println(getServletRequest().getParameter("act")
						+ "config****" + getServerPort());
				properties.setProperty(CommonConstants.DB_SERVER_TYPE,
						getServerType());

				properties.setProperty(CommonConstants.DB_SERVER_PORT,
						String.valueOf(getServerPort()));
				properties.setProperty(CommonConstants.DB_SERVER_HOST,
						getServerHost());

				properties.setProperty(CommonConstants.DB_USER_NAME,
						getDbUsername());
				properties.setProperty(CommonConstants.DATABASE_NAME,
						getDbName());
				properties.setProperty(CommonConstants.DB_PASSWORD,
						DataEncryptDecrypt.encryptData(getPassword()));

				PropertyUtils.save(properties, propert);
				
				
				//properties.store(new FileOutputStream(propert), null);
				// properties =
				// PropertyUtils.getProperties(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
				if (getServletRequest().getParameter("act") != null &&  getServletRequest().getParameter("act")
						.equalsIgnoreCase("test")) {
					System.out.println("action:act"
							+ getServletRequest().getParameter("act"));
					if(!getServerHost().equals(""))
					{
						url = CommonDBConnection.getURL(getServerType(),
								getServerHost(), String.valueOf(getServerPort()));
					}


					String group = CommonDBConnection.testConnection(
							getServerType(), url, getDbUsername(),
							getPassword());
					if (group != null && group.equals("notconnected"))
						setMessage("Permissions successfully updated! Error connecting "
								+ getServerType());
					else if (group != null && group.equals("connected"))
						setMessage("Permissions successfully updated! Connected to "
								+ getServerType());
				} else if( getServletRequest().getParameter("act")
							.equalsIgnoreCase("save")){

					setMessage("Permissions successfully updated!");
				}
			} else {
				System.out.println("Please Enter Values!!");
				setMessage("Please Enter Values!!");
				return "SUCCESS";
			}
			System.out.println("done!!");


		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}

}
