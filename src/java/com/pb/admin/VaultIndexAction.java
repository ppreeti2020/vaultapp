package com.pb.admin;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.SearchMatchesIterator;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Account;
import com.g1.e2.vault.VaultClient.Database;
import com.g1.e2.vault.protocol.UConstants.UOperationGroup.UDatabaseOperationsGroup;
import com.opensymphony.xwork2.ActionSupport;

import com.pb.common.CommonConstants;
import com.pb.common.INIFile;
import com.pb.common.PropertyUtils;
import com.pb.dao.UserDAO;
import com.pb.dto.UserDTO;
import com.pb.ldap.LDAPUtils;
import com.pb.manager.IndexManager;
import com.pb.manager.UserDAOManager;

public class VaultIndexAction extends ActionSupport implements
ServletRequestAware, ServletResponseAware {
	static final Logger logger = Logger.getLogger(VaultIndexAction.class);
	public static final String XML_PROLOG = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	public static final String STORE_FRONT = "<Results>";

	public static final String STORE_FRONT_CLOSE = "</Results>";
	IndexManager indexManager;
	UserDAO userDao = new UserDAO();
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private String[] indexValues;
	private List dbList;
	private List userProfileList;
	private List profileList;
	private String dbProfName;
	private String dbName;
	private String host;
	private List csrUsers;
	private List dbIndexes;
	private List prePopulateList;
	private Map<String, Long> detailIndex;
	private String message;
	private List indexResults;
	private Long userIndexId;
	private String[] indexMatch;
	private String[] indexName;
	private String tabName;
	private String subTabName;
	private List<String> csrProfiles;
	private List<SearchIndex> searchIndexes;
	private String userProfileName;
	private Long userProfileId;
	private String tabId;
	private String subAccessTabId;
	private String subSystemTabId;
	private String subtabId;
	private Long padmin;
	private String changeValue;


	public String getChangeValue() {
		return changeValue;
	}

	public void setChangeValue(String changeValue) {
		this.changeValue = changeValue;
	}

	public Long getPadmin() {
		return padmin;
	}

	public void setPadmin(Long padmin) {
		this.padmin = padmin;
	}

	public String getSubTabId() {
		return subtabId;
	}

	public void setSubTabId(String subtabId) {
		this.subtabId = subtabId;
	}

	public void setSubSystemTabId(String subSystemTabId) {
		this.subSystemTabId = subSystemTabId;
	}

	public String getSubSystemTabId() {
		return subSystemTabId;
	}

	public void setSubAccessTabId(String subAccessTabId) {
		this.subAccessTabId = subAccessTabId;
	}

	public String getSubAccessTabId() {
		return subAccessTabId;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}
	public List<String> getCsrProfiles() {
		return csrProfiles;
	}

	public void setCsrProfiles(List<String> csrProfiles) {
		this.csrProfiles = csrProfiles;
	}

	public String[] getIndexMatch() {
		return indexMatch;
	}

	public void setIndexMatch(String[] indexMatch) {
		this.indexMatch = indexMatch;
	}

	public String getSubTabName() {
		return subTabName;
	}

	public void setSubTabName(String subTabName) {
		this.subTabName = subTabName;
	}

	public String getTabName() {
		return tabName;
	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
	}

	public Long getUserIndexId() {
		return userIndexId;
	}

	public void setUserIndexId(Long userIndexId) {
		this.userIndexId = userIndexId;
	}

	public Long getUserProfileId() {
		return userProfileId;
	}

	public void setUserProfileId(Long userProfileId) {
		this.userProfileId = userProfileId;
	}


	public List getIndexResults() {
		return indexResults;
	}

	public void setIndexResults(ArrayList indexResults) {
		this.indexResults = indexResults;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String, Long> getDetailIndex() {
		return detailIndex;
	}

	public void setDetailIndex(HashMap<String, Long> detailIndex) {
		this.detailIndex = detailIndex;
	}

	public List<String> getPrePopulateList() {
		return prePopulateList;
	}

	public void setPrePopulateList(ArrayList prePopulateList) {
		this.prePopulateList = prePopulateList;
	}

	public List getCsrUsers() {
		return csrUsers;
	}

	public void setCsrUsers(ArrayList csrUsers) {
		this.csrUsers = csrUsers;
	}

	public List getDbIndexes() {
		return dbIndexes;
	}

	public void setDbIndexes(ArrayList dbIndexes) {
		this.dbIndexes = dbIndexes;
	}

	public String getDbProfName() {
		return dbProfName;
	}

	public void setDbProfName(String dbProfName) {
		this.dbProfName = dbProfName;
	}

	public String getUserProfileName() {
		return userProfileName;
	}

	public void setUserProfileName(String userProfileName) {
		this.userProfileName = userProfileName;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public List getDbList() {
		return dbList;
	}

	public void setDbList(ArrayList dbList) {
		this.dbList = dbList;
	}

	public List getUserProfileList() {
		return userProfileList;
	}

	public void setUserProfileList(ArrayList userProfileList) {
		this.userProfileList = userProfileList;
	}

	public List getProfileList() {
		return profileList;
	}

	public void setProfileList(ArrayList profileList) {
		this.profileList = profileList;
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.servletResponse = response;
	}

	public String[] getIndexValues() {
		return indexValues;
	}

	public void setIndexValues(String[] indexValues) {
		this.indexValues = indexValues;
	}

	public String execute() {
		setTabId("Ind Cntrl");
		setSubTabId("resultIdx");
		dbList = new ArrayList<String>();
		userProfileList = new ArrayList<String>();

		try {
			String hostName = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.HOST_NAME);
			//int hostPort = Integer.parseInt(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.HOST_PORT));
			System.out.println("VaultIndexAction > execute() > hostName:::"+hostName);
			host = hostName;
			String tempdb = "";
			changeValue = getChangeValue();
			System.out.println("changeValue1::"+servletRequest.getParameter("changeValue"));
			System.out.println("changeValue2::"+getChangeValue());
			List<Long> uprofileList = (List<Long>)(getServletRequest().getSession().getAttribute("profileIdList"));
			for(Long profileId:uprofileList){
				String uprofileName = userDao.getProfileName(profileId);
				UserDTO dto = new UserDTO();
				dto.setUserProfileId(profileId);
				dto.setUserProfileName(uprofileName);
				userProfileList.add(dto);
			}
			if(servletRequest.getParameter("userProfileName") != null) {
				System.out.println("userProfileName::"+servletRequest.getParameter("userProfileName"));
				userProfileId = Long.parseLong(servletRequest.getParameter("userProfileId")); 
				List<UserDTO> databases = userDao.getDatabases();
				for(UserDTO db:databases){
					if(userDao.isDbBelongsToProfile(userProfileId, db.getDbName()))
					{
						UserDTO dto = new UserDTO();
						dto.setDbDesc(db.getDbDesc());
						dto.setDbName(db.getDbName());
						dbList.add(dto);
					}
				}
			}
			Long dbId = 0L;
			if (servletRequest.getParameter("dbName") != null) {
				detailIndex = new HashMap<String, Long>();
				dbName = servletRequest.getParameter("dbName").toString();
				dbId = userDao.getDatabaseId(dbName);
				detailIndex = userDao.getProfileIndex(dbId,userProfileId);
				System.out.println("detailIndex size::"+detailIndex.size());
				csrUsers = new ArrayList<String>();
				csrProfiles=new ArrayList<String>();

				prePopulateList = new ArrayList<String>();
				indexResults = new ArrayList();
				if (dbId != 0) {
					if(changeValue.equalsIgnoreCase("ProfileIndexes")){
						csrUsers = userDao.getProfileAdminUsers(userProfileId,dbId);
					}
					else if(changeValue.equalsIgnoreCase("UserIndexes")){
						csrUsers = userDao.getProfileUsers(userProfileId,dbId);
					}
					/* Nithu Alexander : 22/09/2014
					 * Change to add the userDisplayName in the UserDTO
					 * Invoking getDisplayNameOfUser() in LDAPUtils class
					 * to obtain the displayName of every UserId
					 * */
					for (Iterator iter = csrUsers.iterator(); iter.hasNext();) {
						UserDTO dto = (UserDTO) iter.next();
						
						if(dto.getUserName() != null && !(dto.getUserName().trim().equals(""))){
							String displayFullName = LDAPUtils.getDisplayNameOfUser(dto.getUserName().trim());
							if(displayFullName != null && !(displayFullName.trim().equals(""))){
								dto.setUserDisplayName(displayFullName);
							}
						}
					}
					prePopulateList = userDao.getUserColumnIndex(dbId, "Y");

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		tabName = "useradmin";
		subTabName = "vaultindex";
		return "SUCCESS";
	}

	public String setIndexColumnPreviliges() {
		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();
		
		try {

			
			Enumeration params = servletRequest.getParameterNames();
			String currentGrp = "";
			System.out.println("setIndexPreviliges" + params);
			Long hasIndex = 0L;
			Long b = 0L;
			changeValue = getChangeValue();
			System.out.println("changeValue2::"+getChangeValue());
			System.out.println("userProfileName::"+userProfileName);
			System.out.println("userProfileId::"+userProfileId);
			if(changeValue.equalsIgnoreCase("ProfileIndexes")){
				String saved = "Y";
				while (params.hasMoreElements()) {
					String paramName = params.nextElement().toString();
					if (paramName.startsWith("Index_")) {
						System.out.println("index values---paramnames?????:::::"+paramName);
						String selectedCheckBoxValue = servletRequest
						.getParameter(paramName);
						System.out.println("index values?????:::::"+selectedCheckBoxValue);
						logger.info("value:  " + selectedCheckBoxValue);

						if (selectedCheckBoxValue.equalsIgnoreCase("unchecked")) {
							System.out.println("index values?????:::::not checked"+paramName);
							String[] tempArr = paramName.split("_");
							String unselectedUser = tempArr[1];
							Long unselecteddbId = Long.parseLong(tempArr[2]);
							String unselectedIndex = tempArr[3];
							System.out.println("unselectedUser:" + unselectedUser);
							System.out.println("unselecteddbId:" + unselecteddbId);
							System.out.println("unselectedIndex:" + unselectedIndex);
							hasIndex = userDao.getColumnIndexUsers(
									unselectedIndex,unselecteddbId,								
									Long.parseLong(unselectedUser), saved);

							System.out.println("hasIndex" + hasIndex);
							if (hasIndex != 0) {
								saved = "N";
								b = userDao.updateUserColumnIndex(
										unselectedIndex,unselecteddbId,								
										Long.parseLong(unselectedUser), saved);
							}
							saved = "Y";
							List userList = (ArrayList) userDao.getAllProfileUsers(userProfileName);
							if(userList.size()>0){
								for (Iterator iter = userList.iterator(); iter.hasNext();) {
									UserDTO uDto = (UserDTO)iter.next();
									Long userId = uDto.getUserId();
									hasIndex = userDao.getColumnIndexUsers(
											unselectedIndex,unselecteddbId,								
											userId, saved);
									if (hasIndex != 0) {
										saved = "N";
										b = userDao.updateUserColumnIndex(
												unselectedIndex,unselecteddbId,
												userId,saved);
									}
								}

							}
						}
						else {
							saved = "Y";
							String[] tempArr = paramName.split("_");
							String selectedUser = tempArr[1];
							Long selecteddbId = Long.parseLong(tempArr[2]);
							String selectedIndex = tempArr[3];
							System.out.println("selectedUser:" + selectedUser);
							System.out.println("selectedIndex:" + selectedIndex);
							hasIndex = userDao.getColumnIndexUsers(
									selectedIndex,selecteddbId,								
									Long.parseLong(selectedUser), saved);

							saved = "Y";
							System.out.println("hasIndex" + hasIndex);
							if (hasIndex.equals(0) || hasIndex == 0) {
								b = userDao.saveUserColIndex(
										selectedIndex,selecteddbId,
										Long.parseLong(selectedUser),saved);
							} else {
								b = userDao.updateUserColumnIndex(
										selectedIndex,selecteddbId,								
										Long.parseLong(selectedUser), saved);
							}

							List userList = (ArrayList) userDao.getAllProfileUsers(userProfileName);
							if(userList.size()>0){
								for (Iterator iter = userList.iterator(); iter.hasNext();) {
									UserDTO uDto = (UserDTO)iter.next();
									Long userId = uDto.getUserId();
									hasIndex = userDao.getColumnIndexUsers(
											selectedIndex,selecteddbId,								
											userId, saved);
									saved = "Y";
									System.out.println("hasIndex" + hasIndex);
									if (hasIndex.equals(0) || hasIndex == 0) {
										b = userDao.saveUserColIndex(
												selectedIndex,selecteddbId,
												userId,saved);
									} else {
										b = userDao.updateUserColumnIndex(
												selectedIndex,selecteddbId,								
												userId, saved);
									}
								}

							}
							if (b != 0)
								setMessage("Successfully updated!");

						}
					}

				}
			}
			else if(changeValue.equalsIgnoreCase("UserIndexes")){
				String saved = "Y";
				while (params.hasMoreElements()) {
					String paramName = params.nextElement().toString();
					if (paramName.startsWith("Index_")) {
						System.out.println("index values---paramnames?????:::::"+paramName);
						String selectedCheckBoxValue = servletRequest
						.getParameter(paramName);
						System.out.println("index values?????:::::"+selectedCheckBoxValue);
						logger.info("value:  " + selectedCheckBoxValue);

						if (selectedCheckBoxValue.equalsIgnoreCase("unchecked")) {
							System.out.println("index values?????:::::not checked"+paramName);
							String[] tempArr = paramName.split("_");
							String unselectedUser = tempArr[1];
							Long unselecteddbId = Long.parseLong(tempArr[2]);
							String unselectedIndex = tempArr[3];
							System.out.println("unselectedUser:" + unselectedUser);
							System.out.println("unselecteddbId:" + unselecteddbId);
							System.out.println("unselectedIndex:" + unselectedIndex);
							hasIndex = userDao.getColumnIndexUsers(
									unselectedIndex,unselecteddbId,								
									Long.parseLong(unselectedUser), saved);

							System.out.println("hasIndex" + hasIndex);
							if (hasIndex != 0) {
								saved = "N";
								b = userDao.updateUserColumnIndex(
										unselectedIndex,unselecteddbId,								
										Long.parseLong(unselectedUser), saved);
							}

						}
						else {
							saved = "Y";
							String[] tempArr = paramName.split("_");
							String selectedUser = tempArr[1];
							Long selecteddbId = Long.parseLong(tempArr[2]);
							String selectedIndex = tempArr[3];
							System.out.println("selectedUser:" + selectedUser);
							System.out.println("selectedIndex:" + selectedIndex);
							hasIndex = userDao.getColumnIndexUsers(
									selectedIndex,selecteddbId,								
									Long.parseLong(selectedUser), saved);

							saved = "Y";
							System.out.println("hasIndex" + hasIndex);
							if (hasIndex.equals(0) || hasIndex == 0) {
								b = userDao.saveUserColIndex(
										selectedIndex,selecteddbId,
										Long.parseLong(selectedUser),saved);
							} else {
								b = userDao.updateUserColumnIndex(
										selectedIndex,selecteddbId,								
										Long.parseLong(selectedUser), saved);
							}


							if (b != 0)
								setMessage("Successfully updated!");

						}
					}

				}
			}
			String succ = execute();

		} catch (Exception e) {

		}
		
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for setIndexColumnPreviliges method : " + difference);
		return "SUCCESS";
	}

	public String getDatabases() {
		System.out.println("Inside SearchIndex Control");
		setTabId("Ind Cntrl");
		setSubTabId("searchIdx");
		dbList = new ArrayList<String>();
		userProfileList = new ArrayList<String>();
			
		try {
			HttpSession session = getServletRequest().getSession();
			if (session.getAttribute("UserName") == null) {
				return "TIMEOUT";
			}
			String hostName = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.HOST_NAME);
			host = hostName;
			changeValue = getChangeValue();
			List<Long> uprofileList = (List<Long>)(getServletRequest().getSession().getAttribute("profileIdList"));
			for(Long profileId:uprofileList){
				String uprofileName = userDao.getProfileName(profileId);
				UserDTO dto = new UserDTO();
				dto.setUserProfileId(profileId);
				dto.setUserProfileName(uprofileName);
				userProfileList.add(dto);
			}
			if(servletRequest.getParameter("userProfileName") != null) {
				userProfileId = Long.parseLong(servletRequest.getParameter("userProfileId"));
				session.setAttribute("userProfileId", userProfileId);
				List<UserDTO> databases = userDao.getDatabases();
				for(UserDTO db:databases){
					if(userDao.isDbBelongsToProfile(userProfileId, db.getDbName()))
					{
						UserDTO dto = new UserDTO();
						dto.setDbDesc(db.getDbDesc());
						dto.setDbName(db.getDbName());
						dbList.add(dto);
					}
				}
			}
			Long dbId = 0L;
			if (servletRequest.getParameter("dbName") != null) {
				padmin = userDao.getProfileAdmin(userProfileId);
				detailIndex = new HashMap<String, Long>();
				dbName = servletRequest.getParameter("dbName").toString();
				dbId = userDao.getDatabaseId(dbName);
				detailIndex = userDao.getProfileIndex(dbId,userProfileId);
				System.out.println("detailIndex size::"+detailIndex.size());
				csrUsers = new ArrayList<String>();
				csrProfiles=new ArrayList<String>();

				prePopulateList = new ArrayList<String>();
				indexResults = new ArrayList();
				if (dbId != 0) {
					if(changeValue.equalsIgnoreCase("ProfileIndexes")){
						csrUsers = userDao.getProfileAdminUsers(userProfileId,dbId);
					}
					else if(changeValue.equalsIgnoreCase("UserIndexes")){
						csrUsers = userDao.getProfileUsers(userProfileId,dbId);
					}

					for (Iterator iter = csrUsers.iterator(); iter.hasNext();) {
						UserDTO dto = (UserDTO) iter.next();
						
						//System.out.println("-------------------------------> user id : " + dto.getUserId());
						
						
						
						//Long profileAdminId = userDao.getProfileAdminUserId(dto.getUserId());
						
						for (String key : detailIndex.keySet()) {
							String str = userDao.getUserIndexResults(
									dto.getUserId(), dbId,
									key, "index");
							UserDTO dto1 = new UserDTO();
							
							
							
							
							
							//System.out.println("++++++++++++++ Index id key : " + key);
							//System.out.println("++++++++++++++ Index Results from USER_INDEX_RESULTS put into session : " + str);
							//get user index id from 
							Long userIndexId = userDao.getUserIndexId(dbId, dto.getUserId(), key);
							
							
							
							/*String str = userDao.getUserIndexResultsForDisplay(
									profileAdminId, dbId,
									key, "index", userIndexId);
							UserDTO dto1 = new UserDTO();*/
							dto1.setUserIndexId(userIndexId);
							dto1.setIndexResults(str);
							dto1.setDbId(dbId);
							dto1.setUserId(dto.getUserId());
							dto1.setIndexName(key);
							indexResults.add(dto1);
							//session.removeAttribute("indexResults");
							session.setAttribute("indexResults", indexResults);
							
						}
						/* Nithu Alexander: 22/09/2014
						 * Invoking getDisplayNameOfUser() in LDAPUtils class
						 * to obtain the displayName of every UserId*/
						if(dto.getUserName() != null && !(dto.getUserName().trim().equals(""))){
							String displayFullName = LDAPUtils.getDisplayNameOfUser(dto.getUserName().trim());
							if(displayFullName != null && !(displayFullName.trim().equals(""))){
								dto.setUserDisplayName(displayFullName);
							}
						}

					}
					prePopulateList = userDao.getUserIndex(dbId, "Y");

				}
			}

		} catch (Exception e) {

		}
		return "SUCCESS";
	}
	public String setIndexPreviliges() {
		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();
		try {

			Enumeration params = servletRequest.getParameterNames();
			String currentGrp = "";
			System.out.println("setIndexPreviliges" + params);
			Long hasIndex = 0L;
			Long b = 0L;
			changeValue = getChangeValue();
			System.out.println("changeValue2::"+getChangeValue());
			System.out.println("userProfileName::"+userProfileName);
			System.out.println("userProfileId::"+userProfileId);
			if(changeValue.equalsIgnoreCase("ProfileIndexes")){
				String saved = "Y";
				
				
				while (params.hasMoreElements()) {
					String paramName = params.nextElement().toString();
					System.out.println("P_arameter Names"+paramName);

					if (paramName.startsWith("Index_")) {
						String selectedCheckBoxValue = servletRequest
						.getParameter(paramName);
						logger.info("value:  " + selectedCheckBoxValue);
						System.out.println("Value is............"+selectedCheckBoxValue);
						if (selectedCheckBoxValue.equalsIgnoreCase("unchecked")) {
							System.out.println("index values?????:::::not checked"+paramName);
							String[] tempArr = paramName.split("_");
							String unselectedUser = tempArr[1];
							Long unselecteddbId = Long.parseLong(tempArr[2]);
							String unselectedIndex = tempArr[3];
							System.out.println("unselectedUser:" + unselectedUser);
							System.out.println("unselecteddbId:" + unselecteddbId);
							System.out.println("unselectedIndex:" + unselectedIndex);
							hasIndex = userDao.getIndexUsers(
									unselectedIndex,unselecteddbId,								
									Long.parseLong(unselectedUser), saved);

							System.out.println("hasIndex" + hasIndex);
							if (hasIndex != 0) {
								//boolean returnVal = userDao.deleteIndexResults(hasIndex);
								saved = "N";
								b = userDao.updateUserIndex(
										unselectedIndex,unselecteddbId,
										Long.parseLong(unselectedUser),saved);
							}
							saved = "Y";
							List userList = (ArrayList) userDao.getAllProfileUsers(userProfileName);
							if(userList.size()>0){
								for (Iterator iter = userList.iterator(); iter.hasNext();)
								{
									UserDTO uDto = (UserDTO)iter.next();
									Long userId = uDto.getUserId();
									hasIndex = userDao.getIndexUsers(
											unselectedIndex,unselecteddbId,								
											userId, saved);
									//boolean returnVal = userDao.deleteIndexResults(hasIndex);
									if (hasIndex != 0) {
										saved = "N";
										b = userDao.updateUserIndex(
												unselectedIndex,unselecteddbId,
												userId,saved);
										/* Nithu Alexander: 08/10/2014
										 * Change to delete FilterValues from USER_INDEX_RESULTS table
										 * if the Index Field is 'unchecked' for the user in UserIndexes tab*/
										if(b != 0){
											boolean delFlag = userDao.deleteIndexResults(hasIndex);
										}
										
									}
								}
							}

						}
						else {
							saved = "Y";
							String[] tempArr = paramName.split("_");
							String selectedUser = tempArr[1];
							Long  selecteddbId = Long.parseLong(tempArr[2]);
							String selectedIndex = tempArr[3];
							System.out.println("selectedUser:" + selectedUser);
							System.out.println("selecteddbId:" + selecteddbId);
							System.out.println("selectedIndex:" + selectedIndex);
							
							hasIndex = userDao.getIndexUsers(
									selectedIndex,selecteddbId,								
									Long.parseLong(selectedUser), saved);
							System.out.println("hasIndex" + hasIndex);
							//boolean returnVal = userDao.deleteIndexResults(hasIndex);
							if (hasIndex.equals(0) || hasIndex == 0) {
								b = userDao.saveUserIndex(
										selectedIndex,selecteddbId,
										Long.parseLong(selectedUser),saved);
							} else {
								//boolean	returnVal = userDao.deleteIndexResults(hasIndex);
								b = userDao.updateUserIndex(
										selectedIndex,selecteddbId,
										Long.parseLong(selectedUser),saved);
							}

							List userList = (ArrayList) userDao.getAllProfileUsers(userProfileName);
							if(userList.size()>0){
								for (Iterator iter = userList.iterator(); iter.hasNext();) {
									UserDTO uDto = (UserDTO)iter.next();
									Long userId = uDto.getUserId();
									hasIndex = userDao.getIndexUsers(
											selectedIndex,selecteddbId,								
											userId, saved);
									// returnVal = userDao.deleteIndexResults(hasIndex);
									if (hasIndex.equals(0) || hasIndex == 0) {
										b = userDao.saveUserIndex(
												selectedIndex,selecteddbId,
												userId,saved);
									} else {
									
										b = userDao.updateUserIndex(
												selectedIndex,selecteddbId,
												userId,saved);
									}
								}

							}


						}

					}

				}
			}
			else if(changeValue.equalsIgnoreCase("UserIndexes")){

				String saved = "Y";
				Long userIndxId = (Long) servletRequest.getAttribute("userIndexId");
				System.out.println("User IndexId in setIndexPrivilages()"+userIndxId);
				while (params.hasMoreElements()) {
					String paramName = params.nextElement().toString();
					System.out.println("P_arameter Names"+paramName);

					if (paramName.startsWith("Index_")) {
						String selectedCheckBoxValue = servletRequest
						.getParameter(paramName);
						logger.info("value:  " + selectedCheckBoxValue);
						System.out.println("Value is............"+selectedCheckBoxValue);
						if (selectedCheckBoxValue.equalsIgnoreCase("unchecked")) {
							System.out.println("index values?????:::::not checked"+paramName);
							String[] tempArr = paramName.split("_");
							String unselectedUser = tempArr[1];
							Long unselecteddbId = Long.parseLong(tempArr[2]);
							String unselectedIndex = tempArr[3];
							System.out.println("unselectedUser:" + unselectedUser);
							System.out.println("unselecteddbId:" + unselecteddbId);
							System.out.println("unselectedIndex:" + unselectedIndex);
							hasIndex = userDao.getIndexUsers(
									unselectedIndex,unselecteddbId,								
									Long.parseLong(unselectedUser), saved);

							System.out.println("hasIndex" + hasIndex);
							if (hasIndex != 0) {
								saved = "N";
								b = userDao.updateUserIndex(
										unselectedIndex,unselecteddbId,
										Long.parseLong(unselectedUser),saved);
								
								/* Nithu Alexander: 08/10/2014
								 * Change to delete FilterValues from USER_INDEX_RESULTS table
								 * if the Index Field is 'unchecked' for the user in UserIndexes tab*/
								if(b != 0){
									boolean deleteFlag = userDao.deleteIndexResults(hasIndex);
								}
								
							}
							

						}
						else {
							saved = "Y";
							String[] tempArr = paramName.split("_");
							String selectedUser = tempArr[1];
							Long  selecteddbId = Long.parseLong(tempArr[2]);
							String selectedIndex = tempArr[3];
							System.out.println("selectedUser:" + selectedUser);
							System.out.println("selecteddbId:" + selecteddbId);
							System.out.println("selectedIndex:" + selectedIndex);
							hasIndex = userDao.getIndexUsers(
									selectedIndex,selecteddbId,								
									Long.parseLong(selectedUser), saved);
							System.out.println("hasIndex" + hasIndex);
							if (hasIndex.equals(0) || hasIndex == 0) {
								b = userDao.saveUserIndex(
										selectedIndex,selecteddbId,
										Long.parseLong(selectedUser),saved);
							} else {
								b = userDao.updateUserIndex(
										selectedIndex,selecteddbId,
										Long.parseLong(selectedUser),saved);
							}
						}

					}

				}

			}
			String succ = getDatabases();
			setMessage("Successfully updated!");

		} catch (Exception e) {
			e.printStackTrace();
		}
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for setIndexPreviliges method : " + difference);
		return "SUCCESS";
	}

	public String getResults() {
		String saved = "Y";
		try {
			
			Long dbId = 0L;
			Enumeration params = servletRequest.getParameterNames();
			String currentGrp = "";
			indexResults = new ArrayList();
			System.out.println("getResults"
					+ servletRequest.getParameter("userId"));
			int i = 0;
			if (servletRequest.getParameter("userId") != null && servletRequest.getParameter("dbId") != null
					&& servletRequest.getParameter("index") != null) {
				dbId = Long.parseLong(servletRequest.getParameter("dbId"));
				Long userId = Long.parseLong(servletRequest.getParameter("userId"));
				String dbname = userDao.getDBName(userId, dbId);
				String index = servletRequest.getParameter("index");
				int expectedResult = 100;
				List searchIndex = userDao.getSearchIndex(dbname, index);

				Iterator iter2 = searchIndex.iterator();
				a: while (iter2.hasNext()) {
					UserDTO doc = (UserDTO)iter2.next();


					if (i > expectedResult) {
						System.err.println("Database has more than "
								+ expectedResult + "statements");
						break a;
					}
					/* Nithu Alexander : 04/09/2014
					 * Addition of Code to avoid EMPTY STRING to appear in IndexResults
					 * Popup of Search Index Control.
					 * Putting condition to avoid adding EMPTY STRING in indexResults List.
					 * Checking for EMPTY STRING in getSearchIndex() output [Data from correspondences table]
					 */
					if(doc.getIndexResults() != null && !(doc.getIndexResults().trim().equals(""))){
						indexResults.add(doc.getIndexResults().trim());
					}
					
					//System.out
					//.println("setAjaxResults:accounts..." + doc.getIndexResults());
					i++;

				}

				Long hasIndex = userDao.getIndexUsers(servletRequest.getParameter("index"),
						Long.parseLong(servletRequest.getParameter("dbId")),
						Long.parseLong(servletRequest.getParameter("userId")),
						saved);
				Long b = 0L;
				saved = "Y";
				System.out.println("hasIndex" + hasIndex);
				if (hasIndex.equals(0) || hasIndex == 0) {
					b = userDao.saveUserIndex(servletRequest.getParameter("index"),
							Long.parseLong(servletRequest.getParameter("dbId")), 
							Long.parseLong(servletRequest.getParameter("userId")),
							saved);
					
				} else {
					b = userDao.updateUserIndex(servletRequest.getParameter("index"),
							Long.parseLong(servletRequest.getParameter("dbId")), 
							Long.parseLong(servletRequest.getParameter("userId")),
							saved);
				}
				if (b != 0)
					userIndexId = userDao.getIndexUsers(servletRequest.getParameter("index"),
							Long.parseLong(servletRequest.getParameter("dbId")),
							Long.parseLong(servletRequest.getParameter("userId")),
							saved);
				/* Nithu Alexander: 23/09/2014
				 * Bug Fix for Updating the session with New UserIndexId after it has been created 
				 * in Database for a particular UserId/DBId/IndexName combination*/
				if (hasIndex.equals(0) || hasIndex == 0) {
					@SuppressWarnings("unchecked")
					List<UserDTO> updatedIndexResults = (ArrayList<UserDTO>)(getServletRequest().getSession().getAttribute("indexResults"));
					UserDTO updateDTO = null;
					for(int j=0; j<updatedIndexResults.size(); j++){
						updateDTO = (UserDTO)updatedIndexResults.get(j);
						if(updateDTO.getUserId().equals(userId) && updateDTO.getDbId().equals(dbId) && updateDTO.getIndexName().equals(index)){
							System.out.println(" Updated UserIndex"+userIndexId);
							updateDTO.setUserIndexId(userIndexId);
						}
						
					}
				}
				
				System.out.println("userIndexId:java" + userIndexId);
			}


		} catch (Exception e) {

		}
		return "SUCCESS";
	}

	public String setResults() {
		try {
			//System.out.println("******** CALLING setResults METHOD");
			Long dbId = 0L;
			Enumeration params = servletRequest.getParameterNames();
			String currentGrp = "";
			indexResults = new ArrayList();
			System.out.println("getResults"
					+ servletRequest.getParameter("userId"));
			if (servletRequest.getParameter("userId") != null
					&& servletRequest.getParameter("index") != null && servletRequest.getParameter("dbId") != null
					&& servletRequest.getParameter("res") != null) {
				dbId = Long.parseLong(servletRequest.getParameter("dbId"));
				Long userId = Long.parseLong(servletRequest.getParameter("userId"));
				String dbname = userDao.getDBName(userId, dbId);
				String index = servletRequest.getParameter("index");
				int expectedResult = 100;
				int i = 0;
				if (!servletRequest.getParameter("res").equalsIgnoreCase("all"))
					expectedResult = Integer.parseInt(servletRequest
							.getParameter("res"));
				List searchlist = userDao.getSearchIndex(dbname, index);
				Iterator iter2 = searchlist.iterator();
				a: while (iter2.hasNext()) {
					UserDTO doc = (UserDTO)iter2.next();

					if (!servletRequest.getParameter("res")
							.equalsIgnoreCase("all")) {

						if (i > expectedResult) {
							System.err
							.println("Database has more than "
									+ expectedResult
									+ "statements");
							break a;
						}
					}
					indexResults.add(doc.getIndexResults());
					//System.out.println("accounts..." + doc.getIndexResults());
					i++;

				}
			}


		} catch (Exception e) {

		}
		return "SUCCESS";
	}

	public String saveIndexResults() {
		boolean hasUser = false;
		boolean b = false;
		try {
			Long dbId = 0L;
			System.out.println("saveIndexResults" + getIndexMatch());
			
			if (servletRequest.getAttribute("userIndexId") != null
					&& getIndexMatch() != null) {
				System.out.println("%%%%%%%%%%%%%%%%% userIndexId : " + servletRequest.getAttribute("userIndexId"));
				Long userindexid = (Long) servletRequest.getAttribute("userIndexId");
				Long UserId = userDao.getUserIDFromUID(Long.parseLong(servletRequest.getAttribute("userIndexId").toString()));
				String IndexNameDbId = userDao.getIndexNameDBIDFromUID(Long.parseLong(servletRequest.getAttribute("userIndexId").toString()));
				String IndexName = IndexNameDbId.split(">")[0];
				Long db_id = Long.parseLong(IndexNameDbId.split(">")[1]);
				//System.out.println("IndexName::"+IndexName+"--db_id::"+db_id);
				boolean returnVal = userDao.deleteIndexResults(userindexid);
				//System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& user id : " + UserId);
				//System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& db id : " + db_id);
				//boolean rval = userDao.deleteIndexResultFromDb(UserId, db_id, userindexid);
				
				for (int i = 0; i < getIndexMatch().length; i++) {
					System.out.println("updated" + getIndexMatch()[i]);
					hasUser = userDao.isIndexResults(
							Long.parseLong(servletRequest.getAttribute(
							"userIndexId").toString()),
							getIndexMatch()[i]);
					if (!hasUser) {
						b = userDao.saveIndexResults(
								Long.parseLong(servletRequest.getAttribute(
								"userIndexId").toString()),
								getIndexMatch()[i]);

					}

					List<Long> pid = userDao.getProfileIdForGroups(UserId);
					if(pid.size()>0){
						Long profileId = 0L;
						for (Iterator iter = pid.iterator(); iter.hasNext();) {
							profileId = (Long) iter.next();
						}
						if(profileId!=0){
							String profile = userDao.getProfileName(profileId);
							List userList = (ArrayList) userDao.getAllProfileUsers(profile);
							if(userList.size()>0){
								for (Iterator iter = userList.iterator(); iter.hasNext();) {
									UserDTO uDto = (UserDTO)iter.next();
									Long userId = uDto.getUserId();
									Long uid = userDao.getUserIndexId(db_id,userId, IndexName);
									hasUser = userDao.isIndexResults(uid,getIndexMatch()[i]);
									if (!hasUser) {
										//returnVal = userDao.deleteIndexResults(uid);
										b = userDao.saveIndexResults(uid,getIndexMatch()[i]);
										

									}
								}

							}
						}
					}
				}
			}
			if (b) {
				setMessage("updated");
			}
		} catch (Exception e) {

		}
		return "SUCCESS";
	}

	public String setAjaxResults() {

		String xmlData = "";
		try {
			Long dbId = 0L;
			String saved = "Y";
			int expectedResult = 100;
			Enumeration params = servletRequest.getParameterNames();
			String currentGrp = "";
			indexResults = new ArrayList();
			//System.out.println("setAjaxResults"
				//	+ servletRequest.getParameter("userId"));
			
			String filterResults = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.FILTER_RESULT);
			System.out.println("<><><><<><><>><><>> filterResults : " + filterResults);
			int i = 0;
			if (servletRequest.getParameter("userId") != null && servletRequest.getParameter("dbId") != null
					&& servletRequest.getParameter("index") != null) {
				dbId = Long.parseLong(servletRequest.getParameter("dbId"));
				Long userId = Long.parseLong(servletRequest.getParameter("userId"));
				String dbname = userDao.getDBName(userId, dbId);
				String index = servletRequest.getParameter("index");
				List searchIndex = userDao.getSearchIndex(dbname, index);

				Iterator iter2 = searchIndex.iterator();
				a: while (iter2.hasNext()) {
					UserDTO doc = (UserDTO)iter2.next();
					

					if (i > expectedResult) {
						System.err.println("Database has more than "
								+ expectedResult + "statements");
						break a;
					}

					indexResults.add(doc.getIndexResults());
					//System.out
					//.println("setAjaxResults:accounts..." + doc.getIndexResults());
					i++;

				}
			}

			//System.out.println("total" + i);
			if (i > Integer.parseInt(filterResults)) {
				xmlData = "More than 100 Results!!";
			} else {
				//i = i - 1;
				//	xmlData = String.valueOf(i);
				xmlData = String.valueOf(i);
			}

			String xml = addXMLProlog(xmlData);
			servletResponse.setContentType("text/xml");
			PrintWriter out = servletResponse.getWriter();
			//System.out.println("xmlData--->" + xml);
			out.println(xml);
			return null;


		} catch (Exception e) {

		}
		return "SUCCESS";
	}

	public static String addXMLProlog(String xmlDATA) {
		// xmlDATA = XML_PROLOG + STORE_FRONT + xmlDATA + STORE_FRONT_CLOSE;
		StringBuffer xmlBuff = new StringBuffer();
		xmlBuff.append(XML_PROLOG);
		xmlBuff.append(STORE_FRONT);
		xmlBuff.append("<IndexResults>");
		xmlBuff.append(xmlDATA);
		xmlBuff.append("</IndexResults>");
		xmlBuff.append(STORE_FRONT_CLOSE);
		return xmlBuff.toString();
	}



	public String createCSRSearchIndexes()
	{
		searchIndexes=new ArrayList<SearchIndex>();
		return "";
	}

	public void setIndexName(String[] indexName) {
		this.indexName = indexName;
	}

	public String[] getIndexName() {
		return indexName;
	}

		
}
