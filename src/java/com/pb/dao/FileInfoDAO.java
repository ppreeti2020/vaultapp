package com.pb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;
import com.pb.common.DateUtil;
import com.pb.common.PropertyUtils;
import com.pb.dto.DataDTO;
import com.pb.dto.FileDTO;
import com.pb.dto.UserDTO;

public class FileInfoDAO {
	DBConnectionDAO db = new DBConnectionDAO();
	DataEncryptDecrypt data = new DataEncryptDecrypt();

	public void saveAFPFileinfo(FileDTO dto) {
		try {
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Connection conn = db.getDBConnection();
			String query = "INSERT INTO COMPRESSION_CHECK(FILE_NAME,NO_OF_PAGES,PROFILE,RESOURCE_SET,UNCOMP_SIZE,COMP_SIZE,FILE_DATE,CHECK_DATE,BYTES_PER_PAGE,COMP_RATIO,HASH_ID) VALUES('"
				+ dto.getFileName()
				+ "','"
				+ dto.getPages()
				+ "','"
				+ dto.getProfile()
				+ "','"
				+ dto.getResourceSet()
				+ "','"
				+ dto.getUnCompress()
				+ "','"
				+ dto.getCompress()
				+ "','"
				+ dto.getFileDate()
				+ "','"
				+ s
				+ "','"
				+ dto.getBytesPerpage()
				+ "','"
				+ dto.getRatio()
				+ "','"
				+ dto.getHashId() + "')";
			Statement stmt = conn.createStatement();
			stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				Long Id = rs.getLong(1);
				saveFileIntegrity(CommonConstants.COMPRESSION_INTEGRITY_CHECK,
						"COM_FILE_ID", dto.getHashId(), dto.getHashId(), Id,
				"yes");
			}
		} catch (Exception e) {

		}
	}

	public List getAFPFileinfos(String fileName) {
		List list = new ArrayList();
		try {
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Connection conn = db.getDBConnection();
			String query = "Select COM_FILE_ID,FILE_NAME,NO_OF_PAGES,PROFILE,RESOURCE_SET,UNCOMP_SIZE,COMP_SIZE,FILE_DATE,CHECK_DATE,BYTES_PER_PAGE,COMP_RATIO,HASH_ID from COMPRESSION_CHECK where FILE_NAME='"
				+ fileName + "'";// "INSERT INTO COMPRESSION_CHECK(FILE_NAME,NO_OF_PAGES,PROFILE,RESOURCE_SET,UNCOMP_SIZE,COMP_SIZE,FILE_DATE,CHECK_DATE,BYTES_PER_PAGE,COMP_RATIO,HASH_ID) VALUES('"+dto.getFileName()+"','"+dto.getPages()+"','"+dto.getProfile()+"','"+dto.getResourceSet()+"','"+dto.getUnCompress()+"','"+dto.getCompress()+"','"+dto.getFileDate()+"','"+s+"','"+dto.getBytesPerpage()+"','"+dto.getRatio()+"','"+dto.getHashId()+"')";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				FileDTO dto = new FileDTO();
				dto.setFileName(data.decryptData(rs.getString(2)));
				dto.setPages(data.decryptData(rs.getString(3)));
				dto.setProfile(data.decryptData(rs.getString(4)));
				dto.setResourceSet(data.decryptData(rs.getString(5)));
				dto.setUnCompress(data.decryptData(rs.getString(6)));
				dto.setCompress(data.decryptData(rs.getString(7)));
				dto.setFileDate(data.decryptData(rs.getString(8)));
				// dto.setBytesPerpage(Float.parseFloat(rs.getString(10)));
				// dto.setRatio(Float.parseFloat(rs.getString(11)));
				dto.setHashId(rs.getString(12));

				list.add(dto);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public List getAFPFileinfo() {
		List list = new ArrayList();
		try {
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Connection conn = db.getDBConnection();
			String query = "Select COM_FILE_ID,FILE_NAME,NO_OF_PAGES,PROFILE,RESOURCE_SET,UNCOMP_SIZE,COMP_SIZE,FILE_DATE,CHECK_DATE,BYTES_PER_PAGE,COMP_RATIO,HASH_ID from COMPRESSION_CHECK";// "INSERT INTO COMPRESSION_CHECK(FILE_NAME,NO_OF_PAGES,PROFILE,RESOURCE_SET,UNCOMP_SIZE,COMP_SIZE,FILE_DATE,CHECK_DATE,BYTES_PER_PAGE,COMP_RATIO,HASH_ID) VALUES('"+dto.getFileName()+"','"+dto.getPages()+"','"+dto.getProfile()+"','"+dto.getResourceSet()+"','"+dto.getUnCompress()+"','"+dto.getCompress()+"','"+dto.getFileDate()+"','"+s+"','"+dto.getBytesPerpage()+"','"+dto.getRatio()+"','"+dto.getHashId()+"')";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				FileDTO dto = new FileDTO();
				dto.setFileName(data.decryptData(rs.getString(2)));
				dto.setPages(data.decryptData(rs.getString(3)));
				dto.setProfile(data.decryptData(rs.getString(4)));
				dto.setResourceSet(data.decryptData(rs.getString(5)));
				dto.setUnCompress(data.decryptData(rs.getString(6)));
				dto.setCompress(data.decryptData(rs.getString(7)));
				dto.setFileDate(data.decryptData(rs.getString(8)));
				// dto.setBytesPerpage(Float.parseFloat(rs.getString(10)));
				// dto.setRatio(Float.parseFloat(rs.getString(11)));
				dto.setHashId(rs.getString(12));

				list.add(dto);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public void saveFileIntegrity(String tableName, String ID, String newHash,
			String oldHash, Long logId, String flag) {
		try {
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Statement stmt = db.getDBConnection().createStatement();
			String query1 = "INSERT INTO "
				+ tableName
				+ "(INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG,"
				+ ID + ") VALUES('" + newHash + "','" + oldHash + "','" + s
				+ "',DEFAULT,'" + flag + "'," + logId + ");";
			stmt.execute(query1);
		} catch (Exception e) {

		}

	}

	public DataDAO getFileIntegrity(String fileName) {
		DataDAO dto = new DataDAO();
		try {

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Statement stmt = db.getDBConnection().createStatement();
			String query1 = "Select FILE_NAME,HOST_NAME,IP_ADDR,DATE_CREATION from FILE_DATA_INTEGRITY where FILE_NAME='"
				+ fileName + "'";
			ResultSet rs = stmt.executeQuery(query1);
			while (rs.next()) {
				dto.setFileName(rs.getString(1));
				dto.setHostName(rs.getString(2));
				dto.setIpAddr(rs.getString(3));
				dto.setFileCreatedDate(rs.getString(4));
			}
		} catch (Exception e) {

		}
		return dto;
	}
	public List<DataDTO> getProfileDetails(Long hostId,String profile)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			System.out.println("getProfileDetails:hostid:"+hostId+"-profile:"+profile);
			ps = conn.prepareStatement("select PROFILE_ID,DB_ID,PROFILE_NAME,INTE_CHECK_DATE,HOST_ID,FILE_PATH,FILE_EXISTS,INTE_CHECK,PROFILE_DATE from PROFILE_INTE_CHECK where HOST_ID=? and PROFILE_NAME=?");
			ps.setLong(1, hostId);
			ps.setString(2, comDao.CheckEncryptData(profile.toUpperCase()));
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				System.out.println("getProfileDetails:"+hostId+"dbId:"+rs.getLong(1));
				DataDTO dataDTO = new DataDTO();

				dataDTO.setProfileInteId(rs.getLong(1));
				dataDTO.setDbId(rs.getLong(2));
				dataDTO.setProfile(comDao.CheckDecryptData( rs.getString(3)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(4)));
				dataDTO.setHostId(rs.getLong(5));
				dataDTO.setFilePath(comDao.CheckDecryptData( rs.getString(6)));
				dataDTO.setFileStatus(comDao.CheckDecryptData( rs.getString(7)));
				dataDTO.setAction(comDao.CheckDecryptData( rs.getString(8)));
				dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(9)));
				//dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(5)));
				//dataDTO.setUpdated_dt(comDao.CheckDecryptData( rs.getString(6)));
				list.add(dataDTO);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	public List<DataDTO> getProfileDetails(Long hostId)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			System.out.println("getProfileDetails"+hostId);
			ps = conn.prepareStatement("select PROFILE_ID,DB_ID,PROFILE_NAME,INTE_CHECK_DATE,HOST_ID,FILE_PATH,FILE_EXISTS,INTE_CHECK from PROFILE_INTE_CHECK WHERE HOST_ID=?");
			ps.setLong(1, hostId);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setProfileInteId(rs.getLong(1));
				dataDTO.setDbId(rs.getLong(2));
				dataDTO.setProfile(comDao.CheckDecryptData( rs.getString(3)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(4)));
				dataDTO.setHostId(rs.getLong(5));
				dataDTO.setFilePath(comDao.CheckDecryptData( rs.getString(6)));
				dataDTO.setFileStatus(comDao.CheckDecryptData( rs.getString(7)));
				dataDTO.setAction(comDao.CheckDecryptData( rs.getString(8)));
				//dataDTO.setCreatedDate(comDao.CheckDecryptData(realPath, rs.getString(5)));
				//dataDTO.setUpdated_dt(comDao.CheckDecryptData(realPath, rs.getString(6)));
				list.add(dataDTO);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace()
			;		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	public List<DataDTO> getProfileDetails()
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			ps = conn.prepareStatement("select PROFILE_ID,DB_ID,PROFILE_NAME,INTE_CHECK_DATE,HOST_ID,FILE_PATH,FILE_EXISTS,INTE_CHECK from PROFILE_INTE_CHECK");
			//ps.setString(1, fileName);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setProfileInteId(rs.getLong(1));
				dataDTO.setDbId(rs.getLong(2));
				dataDTO.setProfile(comDao.CheckDecryptData( rs.getString(3)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(4)));
				dataDTO.setHostId(rs.getLong(5));
				dataDTO.setFileStatus(comDao.CheckDecryptData( rs.getString(7)));
				dataDTO.setAction(comDao.CheckDecryptData( rs.getString(8)));
				//dataDTO.setCreatedDate(comDao.CheckDecryptData(realPath, rs.getString(5)));
				//dataDTO.setUpdated_dt(comDao.CheckDecryptData(realPath, rs.getString(6)));
				list.add(dataDTO);
			}
		}
		catch(Exception e)
		{e.printStackTrace();

		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	public List<DataDTO> getIndexDetails()
	{
		System.out.println("getIndexDetails");
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			ps = conn.prepareStatement("select LOG_FILE_ID, INDEX_ID,INDEX_NAME,INDEX_FILE_NAME, INDEX_DATE,FILE_EXISTS,CHECK_VALID,INTE_CHECK_DATE from LOG_INDEX ");
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setIndexId(rs.getLong(1));
				dataDTO.setIndexName(comDao.CheckDecryptData(rs.getString(2)));
				dataDTO.setIndexFileName(comDao.CheckDecryptData(rs.getString(3)));
				dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(4)));
				dataDTO.setFileExists(comDao.CheckDecryptData( rs.getString(5)));
				dataDTO.setFileStatus(comDao.CheckDecryptData( rs.getString(6)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(10)));
				list.add(dataDTO);
				

			}
		}
		catch(SQLException e)
		{

		}
		catch(Exception e)
		{

		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	
	public List<DataDTO> getIndexDetailsWithProfile(String profile)
	{
		System.out.println("getIndexDetails");
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			ps = conn.prepareStatement("select INDEX_ID,INDEX_NAME,INDEX_FILE_NAME, INDEX_DATE,FILE_EXISTS,CHECK_VALID,INTE_CHECK_DATE, LOG_FILE_ID from LOG_INDEX ");
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setIndexId(rs.getLong(1));
				dataDTO.setIndexName(comDao.CheckDecryptData(rs.getString(2)));
				dataDTO.setIndexFileName(comDao.CheckDecryptData(rs.getString(3)));
				dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(4)));
				dataDTO.setFileExists(comDao.CheckDecryptData( rs.getString(5)));
				dataDTO.setFileStatus(comDao.CheckDecryptData( rs.getString(6)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(10)));
				dataDTO.setLogFileId(rs.getLong(11));
				if(getIdxFileDetailsFromLogFiles(profile,dataDTO.getLogFileId()))
					list.add(dataDTO);
				

			}
		}
		catch(SQLException e)
		{

		}
		catch(Exception e)
		{

		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	
	private boolean getIdxFileDetailsFromLogFiles(String profile,Long LogFileId)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = db.getDBConnection();
		CommonDAO comDao = new CommonDAO();

		String profileName=profile.toUpperCase();
		System.out.println("getIdxFileDetailsFromLogFiles--Profile Name is======="+profileName);
		try {
			String encryptedProfile=comDao.CheckEncryptData(profileName);
			String query = "select * from E2_LOG_MONITOR lm WHERE lm.PROFILE='"+encryptedProfile+"' and lm.LOG_FILE_ID="+LogFileId;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{

				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return false;
	}

	
/*	public List<DataDTO> getIndexDetails()
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			ps = conn.prepareStatement("select TOP 10000 x.PROFILE_INDEX_ID,x.DB_ID,x.INDEX_ID,x.FILE_SIZE,x.INDEX_DATE,x.UPDATED_DATE,x.FILE_PATH,x.FILE_EXISTS,x.INTE_CHECK,x.INTE_CHECK_DATE,x.INDEX_NAME from INDEX_INTE_CHECK x");
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setIndexInteId(rs.getLong(1));
				dataDTO.setDbId(rs.getLong(2));
				dataDTO.setIndexId(rs.getLong(3));
				dataDTO.setFileSize((int)rs.getLong(4));

				dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(5)));
				dataDTO.setUpdated_dt(comDao.CheckDecryptData( rs.getString(6)));

				dataDTO.setFilePath(comDao.CheckDecryptData( rs.getString(7)));
				dataDTO.setFileStatus(comDao.CheckDecryptData( rs.getString(8)));
				dataDTO.setAction(comDao.CheckDecryptData( rs.getString(9)));
				//dataDTO.setProfileName(comDao.CheckDecryptData( rs.getString(10)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(10)));
				dataDTO.setIndexName(comDao.CheckDecryptData( rs.getString(11)));
				list.add(dataDTO);

			}
			System.out.println("List 15:"+list);

		}
		catch(SQLException e)
		{

		}
		catch(Exception e)
		{

		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}*/
	public List<DataDTO> getIndexDetails(Long hostId,Long dbId)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			ps = conn.prepareStatement("select x.PROFILE_INDEX_ID,x.DB_ID,x.INDEX_ID,x.FILE_SIZE,x.INDEX_DATE,x.UPDATED_DATE,x.FILE_PATH,x.FILE_EXISTS,x.INTE_CHECK,x.INTE_CHECK_DATE,x.INDEX_NAME  from INDEX_INTE_CHECK x where x.HOST_ID=? and x.DB_ID=?");
			//	ps.setString(1, fileName);
			ps.setLong(1, hostId);
			ps.setLong(2, dbId);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setIndexInteId(rs.getLong(1));
				dataDTO.setDbId(rs.getLong(2));
				dataDTO.setIndexId(rs.getLong(3));
				dataDTO.setFileSize((int)rs.getLong(4));

				dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(5)));
				dataDTO.setUpdated_dt(comDao.CheckDecryptData( rs.getString(6)));
				dataDTO.setFilePath(comDao.CheckDecryptData( rs.getString(7)));
				dataDTO.setFileStatus(comDao.CheckDecryptData( rs.getString(8)));
				dataDTO.setAction(comDao.CheckDecryptData( rs.getString(9)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(10)));
				dataDTO.setIndexName(comDao.CheckDecryptData( rs.getString(11)));
				//dataDTO.setProfileName(comDao.CheckDecryptData(path, rs.getString(10)));

				list.add(dataDTO);

			}
			System.out.println("List 14:"+list);

		}
		catch(SQLException e)
		{

		}
		catch(Exception e)
		{

		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	public void saveProfileDetails(DataDTO dao) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try {
			String filePath = "";
			String status = "";
			Long fileSize= 0l;
			if(dao.getFilePath() != null)
			{
				filePath = dao.getFilePath();
			}
			if(dao.getFileStatus() != null)
			{
				status = dao.getFileStatus();
			}
			if(dao.getDataFileSize() != null)
			{
				fileSize = dao.getDataFileSize();
			}
			String s = DateUtil.getCurrentDate();
			System.out.println("not completed" + s);
			ps = conn.prepareStatement("INSERT INTO PROFILE_INTE_CHECK(INTE_CHECK_DATE,HOST_ID,PROFILE_NAME,INTE_CHECK,FILE_SIZE,DB_ID,FILE_EXISTS,PROFILE_DATE,FILE_PATH) VALUES(?, ?, ?, ?, ?, ?, ?,?,?)");
			
			ps.setString(1, comDao.CheckEncryptData(s));
			ps.setLong(2, dao.getHostId());
			ps.setString(3, comDao.CheckEncryptData(dao.getProfileName()));
			ps.setString(4, comDao.CheckEncryptData(dao.getAction()));
			ps.setLong(5, fileSize);
			ps.setLong(6, dao.getDbId());
			ps.setString(7, comDao.CheckEncryptData(status));
			ps.setString(8, comDao.CheckEncryptData(s));
			ps.setString(9, comDao.CheckEncryptData(filePath));
			ps.executeUpdate();
			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
			
		} 
		catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
	public void updateProfileDetails(DataDTO dao) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try {
			
			String s = DateUtil.getCurrentDate();
			System.out.println("not completed" + s);
			ps = conn.prepareStatement("UPDATE PROFILE_INTE_CHECK SET INTE_CHECK_DATE=?,HOST_ID=?,INTE_CHECK=?,FILE_SIZE=?,FILE_EXISTS=?,UPDATED_DATE=? where PROFILE_ID=?");
			
			ps.setString(1, comDao.CheckEncryptData(s));
			
			//System.out.println("here1");
			ps.setLong(2, dao.getHostId());
			//System.out.println("here2");
			//ps.setString(3, comDao.CheckEncryptData(dao.getProfileName()));
			ps.setString(3, comDao.CheckEncryptData(dao.getAction()));
			//System.out.println("here3");
			ps.setLong(4, dao.getDataFileSize());
			//System.out.println("here4");
			//ps.setLong(5, dao.getDbId());
			ps.setString(5, comDao.CheckEncryptData(dao.getFileStatus()));
			//System.out.println("here5");
			ps.setString(6, comDao.CheckEncryptData(dao.getCreatedDate()));
			//System.out.println("here6");
			ps.setLong(7,dao.getProfileInteId() );
			//System.out.println("here7");
			
			ps.executeUpdate();
			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
			
		} 
		catch (SQLException e) {
			System.out.println("Error is Here1");
			e.printStackTrace();
		}catch (Exception e) {
			System.out.println("Error is Here2");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	public List<DataDTO> getIndexInte(String fileName,Long dbid,Long fileSize)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		
		try
		{
			String ecryptedPath=comDao.CheckEncryptData( fileName);
			System.out.println("Encrypted Path1"+ecryptedPath);
			ps = conn.prepareStatement("select PROFILE_INDEX_ID,DB_ID,INDEX_ID,FILE_SIZE,INDEX_DATE,UPDATED_DATE,INTE_CHECK_DATE from INDEX_INTE_CHECK where FILE_PATH=? and (FILE_SIZE > ? or FILE_SIZE < ? and DB_ID=?)");
			ps.setString(1,ecryptedPath);
			ps.setLong(2, fileSize);
			ps.setLong(3, fileSize);
			ps.setLong(4, dbid);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setIndexInteId(rs.getLong(1));
				dataDTO.setDbId(rs.getLong(2));
				dataDTO.setIndexId(rs.getLong(3));
				dataDTO.setFileSize((int)rs.getLong(4));
				dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(5)));
				dataDTO.setUpdated_dt(comDao.CheckDecryptData( rs.getString(6)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(7)));
				list.add(dataDTO);
				
			}
			System.out.println("List 12:"+list);
		
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			
		}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally {
				try
				{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			return list;
		}
	
	public List<DataDTO> getIndexInteFromDB(String fileName,Long dbId)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			
			String ecryptedPath=comDao.CheckEncryptData( fileName);
			System.out.println("Encrypted Path2"+ecryptedPath);
			ps = conn.prepareStatement("select PROFILE_INDEX_ID,DB_ID,INDEX_ID,FILE_SIZE,INDEX_DATE,UPDATED_DATE,INTE_CHECK_DATE from INDEX_INTE_CHECK where FILE_PATH=? and DB_ID=?");
			ps.setString(1,ecryptedPath);
			ps.setLong(2, dbId);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setIndexInteId(rs.getLong(1));
				dataDTO.setDbId(rs.getLong(2));
				dataDTO.setIndexId(rs.getLong(3));
				dataDTO.setFileSize((int)rs.getLong(4));
				dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(5)));
				dataDTO.setUpdated_dt(comDao.CheckDecryptData( rs.getString(6)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(7)));
				list.add(dataDTO);
				
			}
			System.out.println("List 11:"+list);
		
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			
		}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally {
				try
				{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			return list;
		}
	
	public void updateIndexDetails(DataDTO dao) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try {
			
			String s = DateUtil.getCurrentDate();
			System.out.println("not completed" + s);
			ps = conn.prepareStatement("UPDATE INDEX_INTE_CHECK SET INTE_CHECK_DATE=?,HOST_ID=?,INTE_CHECK=?,FILE_SIZE=?,FILE_EXISTS=?,UPDATED_DATE=? where PROFILE_INDEX_ID=?");
			
			ps.setString(1, comDao.CheckEncryptData(s));
			ps.setLong(2, dao.getHostId());
			//ps.setString(3, comDao.CheckEncryptData(dao.getProfileName()));
			ps.setString(3, comDao.CheckEncryptData(dao.getAction()));
			ps.setLong(4, dao.getDataFileSize());
			//ps.setLong(5, dao.getDbId());
			ps.setString(5, comDao.CheckEncryptData(dao.getFileStatus()));
			ps.setString(6, comDao.CheckEncryptData(dao.getCreatedDate()));
			ps.setLong(7,dao.getIndexInteId() );
			
			ps.executeUpdate();
			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
		
		} 
		catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	public void saveIndexDetails(DataDTO dao) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		
		Connection conn = db.getDBConnection();
		System.out.println("Connectiiib us--------"+conn);
		try {
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			String s = DateUtil.getCurrentDate();
			System.out.println("not completed" + s);
			ps = conn.prepareStatement("INSERT INTO INDEX_INTE_CHECK(INTE_CHECK_DATE,HOST_ID,INDEX_NAME,INTE_CHECK,FILE_SIZE,INDEX_ID,FILE_EXISTS,DB_ID,INDEX_DATE,FILE_PATH) VALUES(?,?,?,?,?,?,?,?,?,?)");
			if(encrypt.equalsIgnoreCase("YES")){
				ps.setString(1, comDao.CheckEncryptData(s));
				ps.setLong(2, dao.getHostId());
				ps.setString(3, comDao.CheckEncryptData(dao.getIndexName()));
				ps.setString(4, comDao.CheckEncryptData(dao.getAction()));
				ps.setLong(5, dao.getDataFileSize()==null?0:dao.getDataFileSize());
				ps.setLong(6, dao.getIndexId());
				ps.setString(7, comDao.CheckEncryptData(dao.getFileStatus()));
				ps.setLong(8, dao.getDbId());
				ps.setString(9, comDao.CheckEncryptData(dao.getCreatedDate()));
				ps.setString(10, comDao.CheckEncryptData( dao.getFilePath()));
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				ps.setString(1, s);
				ps.setLong(2, dao.getHostId());
				ps.setString(3, dao.getIndexName());
				ps.setString(4, dao.getAction());
				ps.setLong(5, dao.getDataFileSize()==null?0:dao.getDataFileSize());
				ps.setLong(6, dao.getIndexId());
				ps.setString(7, dao.getFileStatus());
				ps.setLong(8, dao.getDbId());
				ps.setString(9, dao.getCreatedDate());
				ps.setString(10, dao.getFilePath());
			}
					
			ps.executeUpdate();
			
			System.out.println("created successfully");
			
		} 
		catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}catch (Exception e) {
			System.out.println("Error is Here Now"+e);
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	public Long getProfileInteIdExists(String profileName)
	{
		
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long profileId=0l;
		try
		{
			
			profileName=comDao.CheckEncryptData( profileName);
			System.out.println("profile Name====="+profileName);
			ps = conn.prepareStatement("select PROFILE_ID from PROFILE_INTE_CHECK WHERE PROFILE_NAME=?");
			ps.setString(1, profileName);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
			profileId=	rs.getLong(1);
				System.out.println("Profile Id is"+profileId);
				//dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(5)));
				//dataDTO.setUpdated_dt(comDao.CheckDecryptData( rs.getString(6)));
				list.add(dataDTO);
			}
		}
		catch(Exception e)
		{e.printStackTrace();
			
		}
		finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return profileId;
		
	}
	public List<DataDTO> getIndexDetails(Long hostId)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			ps = conn.prepareStatement("select PROFILE_INDEX_ID,DB_ID,INDEX_ID,FILE_SIZE,INDEX_DATE,UPDATED_DATE,FILE_PATH,FILE_EXISTS,INTE_CHECK,INTE_CHECK_DATE,INDEX_NAME from INDEX_INTE_CHECK where HOST_ID=?");
		//	ps.setString(1, fileName);
			ps.setLong(1, hostId);
		//	ps.setLong(3, fileSize);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setIndexInteId(rs.getLong(1));
				dataDTO.setDbId(rs.getLong(2));
				dataDTO.setIndexId(rs.getLong(3));
				dataDTO.setFileSize((int)rs.getLong(4));
				
				dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(5)));
				dataDTO.setUpdated_dt(comDao.CheckDecryptData( rs.getString(6)));
				dataDTO.setFilePath(comDao.CheckDecryptData( rs.getString(7)));
				dataDTO.setFileStatus(comDao.CheckDecryptData( rs.getString(8)));
				dataDTO.setAction(comDao.CheckDecryptData( rs.getString(9)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(10)));
				dataDTO.setIndexName(comDao.CheckDecryptData( rs.getString(11)));
				list.add(dataDTO);
				
			}
			//System.out.println("List 13:"+list);
			
		}
	catch(SQLException e)
	{
		
	}
	catch(Exception e)
	{
		
	}
	finally {
		try
		{
		if (ps != null)
			ps.close();
		if (conn != null)
			conn.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	return list;
}
	public List<DataDTO> getIndexDetails(Long hostId,Long dbId,String path)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			ps = conn.prepareStatement("select x.PROFILE_INDEX_ID,x.DB_ID,x.INDEX_ID,x.FILE_SIZE,x.INDEX_DATE,x.UPDATED_DATE,x.FILE_PATH,x.FILE_EXISTS,x.INTE_CHECK,x.INTE_CHECK_DATE,x.INDEX_NAME  from INDEX_INTE_CHECK x where x.HOST_ID=? and x.DB_ID=?");
		//	ps.setString(1, fileName);
			ps.setLong(1, hostId);
			ps.setLong(2, dbId);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setIndexInteId(rs.getLong(1));
				dataDTO.setDbId(rs.getLong(2));
				dataDTO.setIndexId(rs.getLong(3));
				dataDTO.setFileSize((int)rs.getLong(4));
				
				dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(5)));
				dataDTO.setUpdated_dt(comDao.CheckDecryptData( rs.getString(6)));
				dataDTO.setFilePath(comDao.CheckDecryptData( rs.getString(7)));
				dataDTO.setFileStatus(comDao.CheckDecryptData( rs.getString(8)));
				dataDTO.setAction(comDao.CheckDecryptData( rs.getString(9)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(10)));
				dataDTO.setIndexName(comDao.CheckDecryptData( rs.getString(11)));
				//dataDTO.setProfileName(comDao.CheckDecryptData( rs.getString(10)));
				
				list.add(dataDTO);
				
			}
			System.out.println("List 14:"+list);
		
		}
	catch(SQLException e)
	{
		
	}
	catch(Exception e)
	{
		
	}
	finally {
		try
		{
		if (ps != null)
			ps.close();
		if (conn != null)
			conn.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	return list;
}
	
	public List<DataDTO> getIndexDetails(String path)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try
		{
			ps = conn.prepareStatement("select TOP 10000 x.PROFILE_INDEX_ID,x.DB_ID,x.INDEX_ID,x.FILE_SIZE,x.INDEX_DATE,x.UPDATED_DATE,x.FILE_PATH,x.FILE_EXISTS,x.INTE_CHECK,x.INTE_CHECK_DATE,x.INDEX_NAME from INDEX_INTE_CHECK x");
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dataDTO = new DataDTO();
				dataDTO.setIndexInteId(rs.getLong(1));
				dataDTO.setDbId(rs.getLong(2));
				dataDTO.setIndexId(rs.getLong(3));
				dataDTO.setFileSize((int)rs.getLong(4));
			
				dataDTO.setCreatedDate(comDao.CheckDecryptData( rs.getString(5)));
				dataDTO.setUpdated_dt(comDao.CheckDecryptData( rs.getString(6)));
			
				dataDTO.setFilePath(comDao.CheckDecryptData( rs.getString(7)));
				dataDTO.setFileStatus(comDao.CheckDecryptData( rs.getString(8)));
				dataDTO.setAction(comDao.CheckDecryptData( rs.getString(9)));
				//dataDTO.setProfileName(comDao.CheckDecryptData( rs.getString(10)));
				dataDTO.setInteCheckDate(comDao.CheckDecryptData( rs.getString(10)));
				dataDTO.setIndexName(comDao.CheckDecryptData( rs.getString(11)));
				list.add(dataDTO);
				
			}
			System.out.println("List 15:"+list);
			
		}
	catch(SQLException e)
	{
		
	}
	catch(Exception e)
	{
		
	}
	finally {
		try
		{
		if (ps != null)
			ps.close();
		if (conn != null)
			conn.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	return list;
}
	
}
