package com.pb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.pb.common.AeSimpleSHA1;
import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;
import com.pb.common.PropertyUtils;

public class ResultDAO {

	DBConnectionDAO db = new DBConnectionDAO();
	DataEncryptDecrypt data = new DataEncryptDecrypt();
	AeSimpleSHA1 sha = new AeSimpleSHA1();
	long save = 0;
	Statement stmt = null;

	public long saveAccountDetails(HashMap map) {
		String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
		String dbName = "";
		String nricNo = "";
		String docCount = "";
		String custName = "";
		String hash = "";
		try {
			Timestamp tsReturn = null;
			Date date = new Date();
			System.out.println("LogId" + map.get("logId"));
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			// tsReturn = new Timestamp(date.getTime());
			// And the default time and date-time DateFormats
			if(encrypt.equalsIgnoreCase("YES")){
				if (map.get("DBName") != null)
					dbName = data.encryptData(map.get("DBName").toString());
				if (map.get("accNum") != null)
					nricNo = data.encryptData(map.get("accNum").toString());
				if (map.get("count") != null)
					docCount = data.encryptData(map.get("count").toString());
				if (map.get("Name") != null)
					custName = data.encryptData(map.get("Name").toString());

				String accDate = data.encryptData(s);
				hash = sha.SHA1(dbName + "," + nricNo + "," + docCount + ","
						+ custName + "," + accDate);
				String query = "INSERT INTO ACCOUNT_DETAILS(DB_NAME,NRIC_NO,Doc_Count,ACC_Date,ACC_Time,LOG_ID,SER_ID,Cust_Name,SRES_ID,HASH_ID) VALUES('"
						+ dbName
						+ "','"
						+ nricNo
						+ "','"
						+ docCount
						+ "','"
						+ accDate
						+ "', DEFAULT, '"
						+ map.get("logId")
						+ "','"
						+ map.get("serId")
						+ "','"
						+ custName
						+ "','"
						+ map.get("sresId") + "','" + hash + "');";
				stmt = db.getDBConnection().createStatement();
				stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
				System.out.println("search values^^^^^^^^^updated");
				ResultSet rs = stmt.getGeneratedKeys();
				while (rs.next()) {
					save = rs.getLong(1);
					System.out.println("^^^^^^^^^^^^^updated" + save);
					saveResultIntegrity(CommonConstants.ACCOUNT_INTEGRITY_CHECK,
							"ACC_ID", hash, hash, save, "yes");
				}
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				if (map.get("DBName") != null)
					dbName = map.get("DBName").toString();
				if (map.get("accNum") != null)
					nricNo = map.get("accNum").toString();
				if (map.get("count") != null)
					docCount = map.get("count").toString();
				if (map.get("Name") != null)
					custName = map.get("Name").toString();

				String accDate = s;
				hash = sha.SHA1(dbName + "," + nricNo + "," + docCount + ","
						+ custName + "," + accDate);
				String query = "INSERT INTO ACCOUNT_DETAILS(DB_NAME,NRIC_NO,Doc_Count,ACC_Date,ACC_Time,LOG_ID,SER_ID,Cust_Name,SRES_ID,HASH_ID) VALUES('"
						+ dbName
						+ "','"
						+ nricNo
						+ "','"
						+ docCount
						+ "','"
						+ accDate
						+ "', DEFAULT, '"
						+ map.get("logId")
						+ "','"
						+ map.get("serId")
						+ "','"
						+ custName
						+ "','"
						+ map.get("sresId") + "','" + hash + "');";
				stmt = db.getDBConnection().createStatement();
				stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
				System.out.println("search values^^^^^^^^^updated");
				ResultSet rs = stmt.getGeneratedKeys();
				while (rs.next()) {
					save = rs.getLong(1);
					System.out.println("^^^^^^^^^^^^^updated" + save);
					saveResultIntegrity(CommonConstants.ACCOUNT_INTEGRITY_CHECK,
							"ACC_ID", hash, hash, save, "yes");
				}
			}
			
			return save;
			// stmt.
		} catch (Exception e) {
			e.printStackTrace();
		}
		return save;
	}

	public void saveResultIntegrity(String tableName, String ID,
			String newHash, String oldHash, Long logId, String flag) {
		try {
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Statement stmt = db.getDBConnection().createStatement();
			String query1 = "INSERT INTO "
					+ tableName
					+ "(INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG,"
					+ ID + ") VALUES('" + newHash + "','" + oldHash + "','" + s
					+ "',DEFAULT,'" + flag + "'," + logId + ");";
			stmt.execute(query1);
		} catch (Exception e) {

		}

	}

	public long saveDocumentDetails(HashMap map) {
		String docName = "";
		String description = "";
		String contType = "";
		String hash = "";
		PreparedStatement ps = null;
		String docDate = "";
		Long logId= 0l;
		Long accId = 0l;
		String docaccDate = "";
		try {
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			Timestamp tsReturn = null;
			Date date = new Date();
			System.out.println("saveDocumentDetails"+map.get("logId"));
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			if(encrypt.equalsIgnoreCase("YES")){
				if (map.get("docName") != null)
					docName = DataEncryptDecrypt.encryptData(map.get("docName").toString());
				if (map.get("Desc") != null)
					description = DataEncryptDecrypt.encryptData(map.get("Desc").toString());
				if (map.get("docType") != null)
					contType = DataEncryptDecrypt.encryptData(map.get("docType").toString());
				if (map.get("docDate") != null)
					docDate = DataEncryptDecrypt.encryptData(map.get("docDate").toString());
				if(map.get("logId") != null)
					logId = Long.parseLong(map.get("logId").toString());
				if(map.get("accId") != null)
					accId = Long.parseLong(map.get("accId").toString());
				docaccDate = DataEncryptDecrypt.encryptData(s);
				hash = AeSimpleSHA1.SHA1(docName + "," + description + "," + contType + ","
						+ docDate);
				System.out.println("saveDocumentDetails:desc-->" + description
						+ "type-->" + contType + "date" + docDate);
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				if (map.get("docName") != null)
					docName = map.get("docName").toString();
				if (map.get("Desc") != null)
					description = map.get("Desc").toString();
				if (map.get("docType") != null)
					contType = map.get("docType").toString();
				if (map.get("docDate") != null)
					docDate = map.get("docDate").toString();
				if(map.get("logId") != null)
					logId = Long.parseLong(map.get("logId").toString());
				if(map.get("accId") != null)
					accId = Long.parseLong(map.get("accId").toString());
				docaccDate = s;
				hash = AeSimpleSHA1.SHA1(docName + "," + description + "," + contType + ","
						+ docDate);
				System.out.println("saveDocumentDetails:desc-->" + description
						+ "type-->" + contType + "date" + docDate);
			}
			
			String query = "INSERT INTO DOCUMENT_DETAILS(DOC_NAME,Description,Content_Type,Doc_Gen_Date,Doc_Gen_Time,LOG_ID,ACC_ID,HASH_ID,DOC_ACCESS_DATE) VALUES(?,?,?,?,DEFAULT,?,?,?,?)";
					
			ps = db.getDBConnection().prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, docName);
			ps.setString(2, description);
			ps.setString(3, contType);
			ps.setString(4, docDate);
			ps.setLong(5, logId);
			ps.setLong(6, accId);
			ps.setString(7, hash);
			ps.setString(8, docaccDate);
			//stmt = db.getDBConnection().createStatement();
			ps.executeUpdate();
			System.out.println("search values^^^^^^^^^updated");
			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()) {
				save = rs.getLong(1);
				System.out.println("^^^^^^^^^^^^^updated" + save);
				saveResultIntegrity(
						CommonConstants.DOCUMENT_INTE_DETAILS_CHECK, "DOC_ID",
						hash, hash, save, "yes");

			}
			return save;
			// stmt.
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return save;
	}

	public long saveDocumentActionDetails(HashMap map) {
		String docAction = "";
		String hash = "";
		try {
			// Timestamp tsReturn = null;tsReturn = new
			// Timestamp(date.getTime());
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);

			System.out.println("updateDocumentDetails");
			if(encrypt.equalsIgnoreCase("YES")){
				if (map.get("action") != null)
					docAction = data.encryptData(map.get("action").toString());
				hash = sha.SHA1(docAction);
				String docaccDate = data.encryptData(s);
				String query = "INSERT INTO DOCUMENT_ACTION(Doc_Action,DOC_ID,HASH_ID,DOC_ACCESS_DATE,DOC_ACCESS_TIME) values('"
						+ docAction
						+ "',"
						+ map.get("docId")
						+ ",'"
						+ hash
						+ "','"
						+ docaccDate + "',DEFAULT);";
				stmt = db.getDBConnection().createStatement();
				stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
				System.out.println("Document Details^^^^^^^^^updated");
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				if (map.get("action") != null)
					docAction = map.get("action").toString();
				hash = sha.SHA1(docAction);
				String docaccDate = s;
				String query = "INSERT INTO DOCUMENT_ACTION(Doc_Action,DOC_ID,HASH_ID,DOC_ACCESS_DATE,DOC_ACCESS_TIME) values('"
						+ docAction
						+ "',"
						+ map.get("docId")
						+ ",'"
						+ hash
						+ "','"
						+ docaccDate + "',DEFAULT);";
				stmt = db.getDBConnection().createStatement();
				stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
				System.out.println("Document Details^^^^^^^^^updated");
			}
			
			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()) {
				save = rs.getLong(1);
				System.out.println("^^^^^^^^^^^^^updated" + save);
				saveResultIntegrity(CommonConstants.DOC_INTEGRITY_CHECK,
						"DOC_ACT_ID", hash, hash, save, "yes");

			}
			return save;

		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return save;
	}

	public long saveDocumentRenderDetails(HashMap map) {
		String action = "";
		String nric = "";
		String docType = "";
		String pageNo = "";
		String hash = "";
		try {

			Date date = new Date();
			System.out.println("LogId:::saveDocumentRenderDetails"
					+ map.get("logId"));
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			System.out.println(s + " 3. "
					+ DateFormat.getTimeInstance().format(date));
			if (map.get("action") != null) {
				action = data.encryptData(map.get("action").toString());
			}
			if (map.get("acctNumber") != null) {
				nric = data.encryptData(map.get("acctNumber").toString());
			}
			if (map.get("docType") != null) {
				docType = data.encryptData(map.get("docType").toString());
			}
			if (map.get("pageNo") != null) {
				pageNo = data.encryptData(map.get("pageNo").toString());
			}
			String ActDate = data.encryptData(s);
			hash = sha.SHA1(action + "," + nric + "," + docType + "," + pageNo
					+ "," + ActDate);
			System.out.println("actDate--->" + ActDate);
			String query = "INSERT INTO DOCUMENT_RENDER_DETAILS(ACTION,DOC_ID,LOG_ID,NRIC_NO,Doc_Type,Page_NO,ACT_DATE,ACT_TIME,HASH_ID) VALUES('"
					+ action
					+ "','"
					+ map.get("docId")
					+ "','"
					+ map.get("logId")
					+ "','"
					+ nric
					+ "','"
					+ docType
					+ "','"
					+ pageNo + "','" + ActDate + "', DEFAULT,'" + hash + "');";
			stmt = db.getDBConnection().createStatement();
			stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
			System.out.println("Document Render values^^^^^^^^^updated");
			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()) {
				save = rs.getLong(1);
				System.out.println("^^^^^^^^^^^^^updated" + save);
				saveResultIntegrity(
						CommonConstants.DOCUMENT_RENDER_INTE_DETAILS, "ACT_ID",
						hash, hash, save, "yes");

			}
			return save;
			// stmt.
		} catch (Exception e) {
			e.printStackTrace();
		}
		return save;
	}
}
