/*
 * Created @Sep 28, 2001
 * @author suhashini
 * Create the connection from database
 */
package com.pb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;
import com.pb.common.PropertyUtils;
import com.pb.manager.DBConnectionManager;

public class DBConnectionDAO implements DBConnectionManager {
	/*
	 * To create Connection for connecting between SQLServer and JDBC
	 */
	public Connection getDBConnection() {
		Connection conn = null;
		System.out.println("get db connection1");
		
		try {
			// load the driver class
			//System.out.println("Connecting");
			// File file = new File(CommonConstants.CSR_PROPERTY_FILE_NAME)
			String dbconfigfile = CommonConstants.DB_CONFIG_FILE_NAME;
			//System.out.println("get db connection2");
			//System.out.println("path" + path);
			String dbType = PropertyUtils.getProperties(dbconfigfile).getProperty(
					CommonConstants.DB_SERVER_TYPE);
			//System.out.println("get db connection3");
			if (dbType.equalsIgnoreCase(CommonConstants.MSSQL)) {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				String databasePath = "jdbc:sqlserver://"
						+ PropertyUtils.getProperties(dbconfigfile).getProperty(
								CommonConstants.DB_SERVER_HOST)
						+ ":"
						+ PropertyUtils.getProperties(dbconfigfile).getProperty(
								CommonConstants.DB_SERVER_PORT)
						+ ";"
						+ "DatabaseName="
						+ PropertyUtils.getProperties(dbconfigfile).getProperty(
								CommonConstants.DATABASE_NAME);
				//System.out.println("databasePath" + databasePath);
				//System.out.println("get db connection4");
				String User = PropertyUtils.getProperties(dbconfigfile).getProperty(
						CommonConstants.DB_USER_NAME);
				
				
				String decPassword=	"";
				try
				{
					
					String beforedec=PropertyUtils.getProperties(dbconfigfile).getProperty(
							CommonConstants.DB_PASSWORD);
					//System.out.println("Beforedec password"+beforedec);
				decPassword=	DataEncryptDecrypt.decryptData(beforedec);
				//System.out.println("Afrter dec"+decPassword);
				
				}
				catch(Exception e)
				{
					decPassword=PropertyUtils.getProperties(dbconfigfile).getProperty(
							CommonConstants.DB_PASSWORD);
					
					System.out.println("Exception1"+decPassword);
				}
				
				if(decPassword==null || decPassword.equalsIgnoreCase(""))
				{
					
					decPassword=PropertyUtils.getProperties(dbconfigfile).getProperty(
							CommonConstants.DB_PASSWORD);
				System.out.println("decpassword null"+decPassword);
				
				}
				
			
				String Pwd = PropertyUtils.getProperties(dbconfigfile).getProperty(
						CommonConstants.DB_PASSWORD);
				//System.out.println("get db connection6");
			//	System.out.println("user name"+User);
				//System.out.println("Dec password"+decPassword);
				conn = DriverManager.getConnection(databasePath, User, decPassword);
			System.out.println("connection is"+conn);
			}
			System.out.println("Connected");
			if (!conn.isClosed())
				System.out.println();

		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
			System.err.println(cnfe);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			System.err.println(sqle);
		}
		return conn;
	}

	
	public Connection getDBConnection1() {
		// TODO Auto-generated method stub
		DataSource ds=null;
		Connection con=null;
		try {
			Properties env = new Properties();
			  //env.setProperty(Context.PROVIDER_URL, "http://localhost:9080");
			 // env.setProperty(Context.PROVIDER_URL, "iiop://localhost:900");
			  env.setProperty(Context.INITIAL_CONTEXT_FACTORY, "com.ibm.websphere.naming.WsnInitialContextFactory"); 
		//	  env.setProperty(Context.PROVIDER_URL,"iiop://localhost:2809"); 
			  //env.setProperty(Context.INITIAL_CONTEXT_FACTORY, "com.microsoft.jdbcx.sqlserver.SQLServerDataSource");
			InitialContext ctx=new InitialContext(env);
			//Datas
			System.out.println("Environment");
			//System.out.println(ctx.getEnvironment());
			
			//System.out.println("After1");
		//	Context c=(Context)ctx.lookup("java:comp/env");
			//System.out.println("After2");
			 ds=(DataSource)ctx.lookup("java:comp/env/jdbc/VaultAppDB");
			// System.out.println("After3");
			 con=ds.getConnection();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("DBConnection error"+e);
		}catch(SQLException sqe)
		{
			sqe.printStackTrace();
			System.out.println("Sql Exception occured"+sqe);
		}
		catch(Exception ex)
		{
			System.out.println("Exception is :"+ex.getCause());
			
			ex.printStackTrace();
			System.out.println("Exception"+ex);
		}
		
		
		return con;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * To get the Statement using Connection
	 */
	public Statement getStatement() {
		Connection conn = getDBConnection();
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stmt;
	}

	

}
