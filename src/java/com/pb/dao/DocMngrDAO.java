package com.pb.dao;

/*
 * created at Dec 31,2012
 * @author Maruthi
 * 
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.dto.UserDTO;

public class DocMngrDAO {
	//DocMngrDAONew dm = new DocMngrDAONew();
	DBConnectionDAO dao = new DBConnectionDAO();
	
	public Map<Long,DataDAO> getdbResults() throws SQLException {
		Map<Long,DataDAO> list = new HashMap<Long,DataDAO>();
		PreparedStatement rps = null;
		Connection conn = dao.getDBConnection();
		try {
			System.out.println("getdbResults");
			String query = "SELECT CD.DB_ID, CD.DB_NAME, CD.DB_DESC from CSR_DATABASE CD where CD.DB_NAME!='default'";
                
			rps = conn.prepareStatement(query);
			ResultSet rs = rps.executeQuery();
			
			while (rs.next()) {
				DataDAO cDao = new DataDAO();
				cDao.setDbId(rs.getLong(1));
				cDao.setDbName(rs.getString(2));
				cDao.setDbDesc(rs.getString(3));
				list.put(rs.getLong(1),cDao);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			rps.close();
			conn.close();
		}
		System.out.println("db results size::"+list.size());
		return list;
	}
	public Map<Integer,Map<String, String>> getDocData(String tableName, String selectStr, String conds) throws SQLException {
		System.out.println("%%%%%%%%%%%%%% conditions : " + conds);
		UserDAO u = new UserDAO();
		PreparedStatement ps = null;
		PreparedStatement ps2 = null; 
		String query = "select ";
		int k = 1;
		Connection conn = dao.getDBConnection();
		System.out.println("tableName:::" +tableName);
		Map<Integer,Map<String, String>> recMap = new HashMap<Integer, Map<String, String>>();
		Map<String, String> IdxMap = new HashMap<String, String>();
		try {
			if(tableName!=null){
				
				String query2= "select di.INDEX_DESC, di.INDEX_NAME from VP_DATABASE_INDEX di where DB_ID = (select DB_ID from CSR_DATABASE where DB_NAME = ?)";
				ps2 = conn.prepareStatement(query2);
				ps2.setString(1, tableName);
				ResultSet rs2 = ps2.executeQuery();
				System.out.println("query2:::"+query2);
				while(rs2.next()) {
					IdxMap.put(rs2.getString(2), rs2.getString(1));
				}
				/*
				 * Praveen Kumar
				 * Changes done to produce the stable result Start */
				String orderQuery = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.ORDER_BY_SQL_QUERY);
				if(orderQuery == null || "".equalsIgnoreCase(orderQuery))
				{
					orderQuery = " order by invlink desc";
				}else
				{
					orderQuery = " " + orderQuery;
					
				}
				System.out.println("Order by Clause is " + orderQuery);
				/*Changes done to produce the stable result End */
				if(conds.equalsIgnoreCase("")){
					query = query+selectStr+" from "+tableName +orderQuery;
				}
				else{
					query = query+selectStr+" from "+tableName+conds +orderQuery;
					System.out.println("%%%%%%%%%%%%%%%% query : " + query);
				}
				ps = conn.prepareStatement(query);
				System.out.println("selecting records:::"+query);
				ResultSet rs = ps.executeQuery();
				String[] idxs = selectStr.split(",");
				Map<String,String> columnsMap = new HashMap<String,String>();
				for(String column: idxs){
					List<UserDTO> dtypes = u.getDataType(column, tableName);
					String dtype ="";
					for (Iterator iter = dtypes.iterator(); iter.hasNext();) {
						UserDTO udto = (UserDTO) iter.next();
						dtype = udto.getDataType();
					}
					columnsMap.put(column, dtype);
				}
				while (rs.next()) {
					
					Map<String, String> rec = new HashMap<String, String>();
					for(String idx:idxs){
						String datatype = columnsMap.get(idx);
						String idxval = "";
						if(datatype.equalsIgnoreCase("date")){
							idxval = rs.getDate(idx).toString();
						}
						else if(datatype.equalsIgnoreCase("bigint")){
							idxval = rs.getLong(idx)+"";
						}
						else if(datatype.equalsIgnoreCase("int")){
							idxval = rs.getInt(idx)+"";
						}
						else if(datatype.equalsIgnoreCase("float")||datatype.equalsIgnoreCase("decimal")){
							idxval = rs.getFloat(idx)+"";
						}
						else if(datatype.equalsIgnoreCase("boolean")){
							idxval = rs.getBoolean(idx)+"";
					    }
						else
							idxval = rs.getString(idx);
						
						rec.put(IdxMap.get(idx),idxval);
					}
					System.out.println("Inside recmap put>k::"+k+">rec>"+rec);
					recMap.put(k, rec);
					k++;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ps.close();ps2.close();
			conn.close();

		}
		return recMap;
	}
	public Map<Integer,Map<String, String>> getCrossRefData(String xrefTableName, String tableName, Map<String, String> xrefValues, String selectStr,String pidxName) throws SQLException {

		PreparedStatement ps = null;
		PreparedStatement ps1 = null; 
		PreparedStatement ps2 = null; 
		String query = "select ";
		String PIdx;int k = 1;
		UserDAO u = new UserDAO();
		Connection conn = dao.getDBConnection();
		System.out.println("tableName:::" +tableName);
		Map<Integer,Map<String, String>> recMap = new HashMap<Integer, Map<String, String>>();
		Map<String, String> IdxMap = new HashMap<String, String>();
		Map<String, String> IdxVal = new HashMap<String, String>();
		
		ArrayList<ArrayList> recList = new ArrayList<ArrayList>();
		try {
			if(tableName!=null && xrefTableName!=null){
				
				String query1= "select * from "+xrefTableName+" where "+pidxName+"= '"+xrefValues.get(pidxName)+"'";
				ps1 = conn.prepareStatement(query1);
				ResultSet rs1 = ps1.executeQuery();
				System.out.println("query1:::"+query1);
				while(rs1.next()) {
					for(String idx:xrefValues.keySet()){
						if(!idx.equalsIgnoreCase(pidxName)){
							if(rs1.getString(idx).contains("#")){
								String[] cntstr = rs1.getString(idx).split("#");
								for(int j=0;j<cntstr.length;j++){
									if(rs1.getString(idx).split("#")[j].equalsIgnoreCase(xrefValues.get(idx))){
										System.out.println("Idx matched::"+idx+"::"+rs1.getString(idx).split("#")[j]);
										IdxVal.put(idx, rs1.getString(idx));
										break;
									}
								}
							}
							else{
								if(rs1.getString(idx).equalsIgnoreCase(xrefValues.get(idx))){
									System.out.println("Idx matched inside else loop::"+idx+"::"+rs1.getString(idx));
									IdxVal.put(idx, rs1.getString(idx));
									break;
								}
							}
								
						}
					}
				}
				query = query+selectStr+" from "+tableName+" where "+pidxName+"= '"+xrefValues.get(pidxName)+"' and (";
				String[] idxs = selectStr.split(",");
				String conds = "";
				String query2= "select di.INDEX_DESC, di.INDEX_NAME from VP_DATABASE_INDEX di where DB_ID = (select DB_ID from CSR_DATABASE where DB_NAME = ?)";
				ps2 = conn.prepareStatement(query2);
				ps2.setString(1, tableName);
				ResultSet rs2 = ps2.executeQuery();
				System.out.println("query2:::"+query2);
				while(rs2.next()) {
					IdxMap.put(rs2.getString(2), rs2.getString(1));
				}
				for(String idx:IdxVal.keySet()){
					if(!idx.equalsIgnoreCase(pidxName)){
						if(IdxVal.get(idx).contains("#")){
							String[] val = IdxVal.get(idx).split("#");
							conds = "";
							for(int j=0;j<val.length;j++){
							  conds = conds + idx+" = '"+IdxVal.get(idx).split("#")[j]+"' or ";
							}
							System.out.println("conds::"+conds);
							int index = conds.lastIndexOf("or");  
							if(index != -1) {  
								conds = conds.substring(0,index);  
							}
							conds = conds+" )";
							System.out.println("conds::"+conds);
						}
						else{
							conds = "";
							conds = idx+" = '"+IdxVal.get(idx) +"' )";
						}
						String subquery = query + conds;
						ps = conn.prepareStatement(subquery);
						ResultSet rs = ps.executeQuery();
						while(rs.next()){
							ArrayList recval = new ArrayList();
							for(String idx1:idxs){
								String dataType = u.getonlyDataType(idx1, tableName);
								if(dataType.equalsIgnoreCase("date")){
									recval.add(rs.getDate(idx1));
								}
								else if(dataType.equalsIgnoreCase("bigint")){
									recval.add(rs.getLong(idx1));
								}
								else if(dataType.equalsIgnoreCase("timestamp")){
									recval.add(rs.getTimestamp(idx1));
								}
								else if(dataType.equalsIgnoreCase("numeric")){
									recval.add(rs.getFloat(idx1));
								}
								else {
									recval.add(rs.getString(idx1));
								}
							}
							System.out.println("recval:::"+recval);
							System.out.println("recList:::"+recList);
							if(!recList.contains(recval)){
								recList.add(recval);
								Map<String, String> rec = new HashMap<String, String>();
								for(String idx1:idxs){
									String dataType = u.getonlyDataType(idx1, tableName);
									if(dataType.equalsIgnoreCase("date")){
										rec.put(IdxMap.get(idx1),rs.getDate(idx1).toString());
									}
									else if(dataType.equalsIgnoreCase("bigint")){
										rec.put(IdxMap.get(idx1),rs.getLong(idx1)+"");
									}
									else if(dataType.equalsIgnoreCase("timestamp")){
										rec.put(IdxMap.get(idx1),rs.getTimestamp(idx1).toString());
									}
									else if(dataType.equalsIgnoreCase("numeric")){
										rec.put(IdxMap.get(idx1),rs.getFloat(idx1)+"");
									}
									else {
										rec.put(IdxMap.get(idx1),rs.getString(idx1));
									}
								}
								recMap.put(k, rec);
								k++;
							}
						
						}
						
					}
				}
				for(int i=1;i<recMap.size();i++){
					System.out.println("recMap>>>"+recMap.get(i));
				}
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ps.close();ps1.close();
			conn.close();

		}
		return recMap;
	}
	public List<DataDAO> getDocInfo(String vaultIndex, String vaultIdx, String tableName) throws SQLException
	{
		List<DataDAO> list = new ArrayList<DataDAO>();
		Connection conn = dao.getDBConnection();
		PreparedStatement ps = null;
		try
		{ 
			String sql="select "+vaultIndex+",account,invlink,AttachmentPath, Attachment1, Attachment2, Attachment3, Attachment4 from "+ tableName+" where "+vaultIndex+" = '"+vaultIdx+"'";
			//String sql="select "+vaultIndex+",invlink from "+ tableName+" where "+vaultIndex+" = '"+vaultIdx+"'";
			//String sql="select "+vaultIndex+",AcctNo,invlink from "+ tableName+" where "+vaultIndex+" = '"+vaultIdx+"'";
			System.out.println("sql:::"+sql);
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDAO dao = new DataDAO();
				dao.setDocRefNo(rs.getString(1));
				dao.setAccNumber(rs.getString(2));
				dao.setCreated_dt(rs.getDate(3).toString());
				dao.setAttachmentPath(rs.getString(4));
				dao.setAttachment1(rs.getString(5));
				dao.setAttachment2(rs.getString(6));
				dao.setAttachment3(rs.getString(7));
				dao.setAttachment4(rs.getString(8));
				
				list.add(dao);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			ps.close();
			conn.close();

		}
		return list;
	}
	
}
