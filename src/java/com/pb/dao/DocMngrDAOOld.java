package com.pb.dao;

/*
 * created at Apr 2,2012
 * @author Akshay Koul
 * get the DocMetaData from database
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import java.util.List;

import com.pb.common.CommonConstants;

public class DocMngrDAOOld {

	DBConnectionDAO dao = new DBConnectionDAO();
	

	/*
	 * To get Document Management Related Data
	 */
public List<DataDAO> getRootResults() throws SQLException {
		List<DataDAO> list = new ArrayList<DataDAO>();
		PreparedStatement rps = null,srps=null;
		Connection conn = dao.getDBConnection();
		try {
			System.out.println("getSearchResults from Doc Management Table");
            String query = "select ROOT_ID,ROOT_NAME from ROOT_DOC_METADATA";
            String subquery = "select C.DB_DESC, C.DB_ID From [CSR-APP].dbo.ROOT_DB_Data B,[CSR-APP].dbo.CSR_DATABASE C where B.Root_ID=? and B.DB_ID=C.DB_ID";
			rps = conn.prepareStatement(query);
			srps = conn.prepareStatement(subquery);
			System.out.println("selecting records");
			ResultSet rs = rps.executeQuery();
			while (rs.next()) {
				DataDAO cDao = new DataDAO();
				srps.setLong(1,rs.getLong(1));
				ResultSet sprs = srps.executeQuery();
				while (sprs.next()){
				cDao.setRootID(rs.getLong(1));
				cDao.setRootName(rs.getString(2));
				cDao.setProfileName(sprs.getString(1));
				cDao.setProfileID(sprs.getLong(2));
				list.add(cDao);
				System.out.println("TreeList"+cDao.getRootID());
				System.out.println("TreeList"+cDao.getRootName());
				System.out.println("TreeList"+cDao.getProfileName());
				System.out.println("TreeList"+cDao.getProfileID());
				}
			}
			return list;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			rps.close();
			conn.close();

		}
		return list;
	}

public Map<Long,DataDAO> getRootMapResults(Long UserId) throws SQLException {
	Map<Long,DataDAO> list = new HashMap<Long,DataDAO>();
	PreparedStatement rps = null,srps=null;
	Connection conn = dao.getDBConnection();
	try {
		System.out.println("getRootMapResults from Doc Management Table:[[getRootMapResults]]:"+UserId);
       // String query = "select distinct C.DB_ID, rm.ROOT_ID,rm.ROOT_NAME, C.DB_DESC  from [CSR-APP].dbo.ROOT_DOC_METADATA rm left join [CSR-APP].dbo.DOC_METADATA dm on rm.ROOT_ID = dm.ROOT_ID left join  [CSR-APP].dbo.CSR_DATABASE C on dm.REPOSITORY_ID = c.DB_ID left join [CSR-APP].dbo.USER_DATABASE u on c.DB_ID = u.DB_ID where u.USER_ID =?";
        String query = "select distinct C.DB_ID, rm.ROOT_ID,rm.ROOT_NAME, C.DB_DESC  from [CSR-APP].dbo.ROOT_DOC_METADATA rm, [CSR-APP].dbo.ROOT_DB_Data rdd, [CSR-APP].dbo.CSR_DATABASE C where rm.ROOT_ID = rdd.ROOT_ID and rdd.DB_ID= c.DB_ID";
                
        rps = conn.prepareStatement(query);
		rps.setLong(1, UserId);
		//srps = conn.prepareStatement(subquery);
		System.out.println("selecting records::getRootMapResults-->"+UserId);
		ResultSet rs = rps.executeQuery();
		while (rs.next()) {
			DataDAO cDao = new DataDAO();
			//srps.setLong(1,rs.getLong(1));
			//ResultSet sprs = srps.executeQuery();
			//while (sprs.next()){
			cDao.setRootID(rs.getLong(2));
			cDao.setRootName(rs.getString(3));
			cDao.setProfileName(rs.getString(4));
			cDao.setProfileID(rs.getLong(1));
			list.put(rs.getLong(1),cDao);
			System.out.println("TreeList"+cDao.getRootID());
			System.out.println("TreeList"+cDao.getRootName());
			System.out.println("TreeList"+cDao.getProfileName());
			System.out.println("TreeList"+cDao.getProfileID());
			//}
		}
		return list;

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		rps.close();
		conn.close();

	}
	return list;
}

public Map<Long,DataDAO> getProfMapResults(Long UserId) throws SQLException {
	Map<Long,DataDAO> list = new HashMap<Long,DataDAO>();
	PreparedStatement rps = null,srps=null;
	Connection conn = dao.getDBConnection();
	try {
		System.out.println("getRootMapResults from Doc Management Table:[[getRootMapResults]]:"+UserId);
       // String query = "select distinct C.DB_ID, rm.ROOT_ID,rm.ROOT_NAME, C.DB_DESC  from [CSR-APP].dbo.ROOT_DOC_METADATA rm left join [CSR-APP].dbo.DOC_METADATA dm on rm.ROOT_ID = dm.ROOT_ID left join  [CSR-APP].dbo.CSR_DATABASE C on dm.REPOSITORY_ID = c.DB_ID left join [CSR-APP].dbo.USER_DATABASE u on c.DB_ID = u.DB_ID where u.USER_ID =?";
        String query = "select distinct C.DB_ID, rm.ROOT_ID,rm.ROOT_NAME, C.DB_DESC  from [CSR-APP].dbo.ROOT_DOC_METADATA rm, [CSR-APP].dbo.ROOT_DB_Data rdd, [CSR-APP].dbo.CSR_DATABASE C where rm.ROOT_ID = rdd.ROOT_ID and rdd.DB_ID= c.DB_ID";
                
        rps = conn.prepareStatement(query);
		rps.setLong(1, UserId);
		System.out.println("selecting records::getRootMapResults-->"+UserId);
		ResultSet rs = rps.executeQuery();
		while (rs.next()) {
			DataDAO cDao = new DataDAO();
			//srps.setLong(1,rs.getLong(1));
			//ResultSet sprs = srps.executeQuery();
			//while (sprs.next()){
			cDao.setRootID(rs.getLong(2));
			cDao.setRootName(rs.getString(3));
			cDao.setProfileName(rs.getString(4));
			cDao.setProfileID(rs.getLong(1));
			list.put(rs.getLong(1),cDao);
			System.out.println("TreeList"+cDao.getRootID());
			System.out.println("TreeList"+cDao.getRootName());
			System.out.println("TreeList"+cDao.getProfileName());
			System.out.println("TreeList"+cDao.getProfileID());
			//}
		}
		return list;

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		rps.close();
		conn.close();

	}
	return list;
}
public Map<Long,DataDAO> getRootSearchResults(String profile,Long UserId) throws SQLException {
	Map<Long,DataDAO> list = new HashMap<Long,DataDAO>();
	PreparedStatement rps = null,srps=null;
	Connection conn = dao.getDBConnection();
	try {
		System.out.println("getRootSearchResults from Doc Management Table--profile::"+profile);
        String query = "select distinct C.DB_ID, rm.ROOT_ID,rm.ROOT_NAME, C.DB_DESC  from [CSR-APP].dbo.ROOT_DOC_METADATA rm left join [CSR-APP].dbo.DOC_METADATA dm on rm.ROOT_ID = dm.ROOT_ID left join  [CSR-APP].dbo.CSR_DATABASE C on dm.REPOSITORY_ID = c.DB_ID left join [CSR-APP].dbo.USER_DATABASE u on c.DB_ID = u.DB_ID where C.DB_DESC like '%"+profile+"%' and u.USER_ID=?";
       // String subquery = "select B.INC_ID,C.DB_DESC, C.DB_ID From [CSR-APP].dbo.ROOT_DB_Data B,[CSR-APP].dbo.CSR_DATABASE C where B.Root_ID=? and B.DB_ID=C.DB_ID";
		rps = conn.prepareStatement(query);
		//rps.setString(1, profile);
		rps.setLong(1, UserId);
		//srps = conn.prepareStatement(subquery);
		System.out.println("selecting records:UserId::"+UserId);
		ResultSet rs = rps.executeQuery();
		while (rs.next()) {
			DataDAO cDao = new DataDAO();
			//srps.setLong(1,rs.getLong(1));
			//ResultSet sprs = srps.executeQuery();
			//while (sprs.next()){
			cDao.setRootID(rs.getLong(2));
			cDao.setRootName(rs.getString(3));
			cDao.setProfileName(rs.getString(4));
			cDao.setProfileID(rs.getLong(1));
			list.put(rs.getLong(1),cDao);
			/*System.out.println("TreeList"+cDao.getRootID());
			System.out.println("TreeList"+cDao.getRootName());
			System.out.println("TreeList"+cDao.getProfileName());
			System.out.println("TreeList"+cDao.getProfileID());*/
			//}
		}
		return list;

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		rps.close();
		conn.close();

	}
	return list;
}
public Map<Long,DataDAO> getRootFileResults() throws SQLException {
	Map<Long,DataDAO> list = new HashMap<Long,DataDAO>();
	PreparedStatement rps = null,srps=null;
	Connection conn = dao.getDBConnection();
	try {
		System.out.println("getSearchResults from Doc Management Table");
        String query = "select rm.ROOT_ID,rm.ROOT_NAME,C.DB_DESC, C.DB_ID,B.INC_ID  from [CSR-APP].dbo.ROOT_DOC_METADATA rm left join [CSR-APP].dbo.ROOT_DB_Data B on rm.ROOT_ID = b.ROOT_ID left join [CSR-APP].dbo.CSR_DATABASE C on b.DB_ID = c.DB_ID";
       // String subquery = "select B.INC_ID,C.DB_DESC, C.DB_ID From [CSR-APP].dbo.ROOT_DB_Data B,[CSR-APP].dbo.CSR_DATABASE C where B.Root_ID=? and B.DB_ID=C.DB_ID";
		rps = conn.prepareStatement(query);
		//srps = conn.prepareStatement(subquery);
		System.out.println("selecting records");
		ResultSet rs = rps.executeQuery();
		while (rs.next()) {
			DataDAO cDao = new DataDAO();
			//srps.setLong(1,rs.getLong(1));
			//ResultSet sprs = srps.executeQuery();
			//while (sprs.next()){
			cDao.setRootID(rs.getLong(1));
			cDao.setRootName(rs.getString(2));
			cDao.setProfileName(rs.getString(3));
			cDao.setProfileID(rs.getLong(4));
			list.put(rs.getLong(5),cDao);
			System.out.println("TreeList"+cDao.getRootID());
			System.out.println("TreeList"+cDao.getRootName());
			System.out.println("TreeList"+cDao.getProfileName());
			System.out.println("TreeList"+cDao.getProfileID());
			//}
		}
		return list;

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		rps.close();
		conn.close();

	}
	return list;
	}
	public List<DataDAO> getDocInfo(Long docid)
	{
		List<DataDAO> list = new ArrayList<DataDAO>();
		Connection conn = dao.getDBConnection();
		PreparedStatement ps = null;
		try
		{
			String sql="select d.DOC_ID,d.ROOT_ID,d.SERVER_ID,d.REPOSITORY_ID,d.account,d.address,d.name,d.invlink,d.GUID,d.OFFSET, p.DB_NAME,d.FORMAT from CSR_DATABASE p, DOC_METADATA d where p.DB_ID=d.REPOSITORY_ID and d.STATUS='O' and d.DOC_ID=?";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, docid);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDAO dao = new DataDAO();
				dao.setIndx1(rs.getString(5));
				dao.setIndx2(rs.getString(6));
				dao.setCreated_dt(rs.getString(8));
				dao.setPointer(rs.getString(10));
				dao.setProfile(rs.getString(11));
				dao.setFormat(rs.getString(12));
				list.add(dao);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	public String getRoot(Long docid)
	{
		//List<DataDAO> list = new ArrayList<DataDAO>();
		Connection conn = dao.getDBConnection();
		PreparedStatement ps = null;
		String name="";
		try
		{
			String sql="select distinct C.DB_ID, rm.ROOT_ID,rm.ROOT_NAME, C.DB_DESC  from [CSR-APP].dbo.ROOT_DOC_METADATA rm left join [CSR-APP].dbo.DOC_METADATA dm on rm.ROOT_ID = dm.ROOT_ID left join  [CSR-APP].dbo.CSR_DATABASE C on dm.REPOSITORY_ID = c.DB_ID left join [CSR-APP].dbo.USER_DATABASE u on c.DB_ID = u.DB_ID where dm.DOC_ID =?  ";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, docid);
			ResultSet rs = ps.executeQuery();
			if(rs.next())
			{
				//DataDAO dao = new DataDAO();
				//dao.setRootID(rs.getLong(2));
				//dao.setRootName(rs.getString(3));
				//dao.setProfileName(rs.getString(4));
				//dao.setProfileID(rs.getLong(1));
				//list.add(dao);
				name = rs.getString(3);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return name;
	}
	public List<DataDAO> getSearchResults() throws SQLException {
		List<DataDAO> list = new ArrayList<DataDAO>();
		PreparedStatement ps = null,rootps = null, profileps=null;
		Connection conn = dao.getDBConnection();
		try {
			System.out.println("getSearchResults from Doc Management Table");
            String query = "select * from DOC_METADATA where STATUS='O'";
			ps = conn.prepareStatement(query);
			System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDAO cDao = new DataDAO();
				//System.out.println("selected records" + rs.getString(1));
				cDao.setDocId(rs.getLong(1));
				cDao.setRootID(rs.getLong(2));
				cDao.setServerID(rs.getLong(3));
				cDao.setProfileID(rs.getLong(4));
				String rootquery = "select ROOT_NAME from ROOT_DOC_METADATA where ROOT_ID=?";
				rootps = conn.prepareStatement(rootquery);
				rootps.setLong(1, cDao.getRootID());
				ResultSet rootrs = rootps.executeQuery();
				while (rootrs.next()){
				cDao.setRootName(rootrs.getString(1));	
				}
				String profilequery = "select p.DB_NAME from CSR_DATABASE p,ROOT_DOC_METADATA r, DOC_METADATA d where p.DB_ID=d.REPOSITORY_ID and r.ROOT_ID=d.ROOT_ID and p.DB_ID=? and r.ROOT_ID=?";
				profileps = conn.prepareStatement(profilequery);
				profileps.setLong(2, cDao.getRootID());
				profileps.setLong(1, cDao.getProfileID());
				ResultSet profilers = profileps.executeQuery();
				while (profilers.next()){
					if (profilers.getString(1) != null){
				cDao.setProfileName(profilers.getString(1));}
				}
				list.add(cDao);
			}
			return list;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();

		}
		return list;
	}
	public List<DataDAO> getResults(Long UserId,String search) throws SQLException {
		List<DataDAO> list = new ArrayList<DataDAO>();
		PreparedStatement ps = null,rootps = null, profileps=null;
		Connection conn = dao.getDBConnection();
		try {
			System.out.println("getResults from Doc Management Table results:UserId"+UserId);
			System.out.println("getResults from Doc Management Table results:search+++++++++++"+search);
			String query = "";
			if(search != null && !search.equals(""))
			{
            query = "select distinct doc.Filename, C.DB_DESC, C.DB_ID,doc.invlink  from [CSR-APP].dbo.CSR_DATABASE C left join [CSR-APP].dbo.DOC_METADATA doc on doc.REPOSITORY_ID = c.DB_ID left join [CSR-APP].dbo.USER_DATABASE dm on  c.DB_ID = dm.DB_ID  where invlink != 'null'  and C.DB_DESC like '%"+search+"%' and dm.USER_ID=?";
			}
			else
				query = "select distinct doc.Filename, C.DB_DESC, C.DB_ID,doc.invlink  from [CSR-APP].dbo.CSR_DATABASE C left join [CSR-APP].dbo.DOC_METADATA doc on doc.REPOSITORY_ID = c.DB_ID left join [CSR-APP].dbo.USER_DATABASE dm on  c.DB_ID = dm.DB_ID  where invlink != 'null' and dm.USER_ID=?";
			
			ps = conn.prepareStatement(query);
			System.out.println("selecting records in getResults"+query);
			ps.setLong(1, UserId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDAO cDao = new DataDAO();
				System.out.println("selected records" + rs.getString(1));
				
				cDao.setFileName(rs.getString(1));
				
				cDao.setProfileID(rs.getLong(3));
				cDao.setProfileName(rs.getString(2));
				cDao.setCreated_dt(rs.getString(4));
				
				list.add(cDao);
			}
			return list;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();

		}
		return list;
	}
public List<DataDAO> getProfiles(Long profileID) throws SQLException {
		PreparedStatement ps = null;
		Connection conn = dao.getDBConnection();
		System.out.println("testagain" +profileID);
		List<DataDAO> list = new ArrayList<DataDAO>();
		try {
			System.out.println("getProfiles from Doc Management Table as per Profile ID");
			String query = "";
				
				query = "select d.DOC_ID,d.ROOT_ID,d.SERVER_ID,d.REPOSITORY_ID,d.account,d.address,d.name,d.invlink,d.GUID, p.DB_NAME,d.FORMAT from CSR_DATABASE p, DOC_METADATA d where p.DB_ID=d.REPOSITORY_ID and p.DB_ID=?";
				ps = conn.prepareStatement(query);
				ps.setLong(1, profileID);
			
			
			System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDAO cDao = new DataDAO();
				//System.out.println("selected records" + rs.getString(1));
				cDao.setDocId(rs.getLong(1));
				cDao.setRootID(rs.getLong(2));
				cDao.setServerID(rs.getLong(3));
				cDao.setProfileID(rs.getLong(4));
				cDao.setIndx1(rs.getString(5));
				cDao.setIndx2(rs.getString(6));
				cDao.setIndx3(rs.getString(7));
				cDao.setIndx4(rs.getString(8));
					cDao.setGuid(rs.getString(9));
				cDao.setProfileName(rs.getString(10));	
				cDao.setFormat(rs.getString(11));	
				//System.out.println("Hello2"+rs.getString(4)+"   "+ rs.getString(5));
				
				
				list.add(cDao);
			}
			return list;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();

		}
		return list;
	}

public List<DataDAO> getProfiles() throws SQLException {
	PreparedStatement ps = null;
	Connection conn = dao.getDBConnection();
	//System.out.println("testagain" );
	List<DataDAO> list = new ArrayList<DataDAO>();
	try {
		System.out.println("getProfiles from Doc Management Table as per Profile ID");
		String query = "";
			
			query = "select d.DOC_ID,d.ROOT_ID,d.SERVER_ID,d.REPOSITORY_ID,d.account,d.address,d.name,d.invlink,d.GUID, p.DB_NAME, d.FORMAT from CSR_DATABASE p, DOC_METADATA d where p.DB_ID=d.REPOSITORY_ID";
			ps = conn.prepareStatement(query);
			//ps.setLong(1, profileID);
		
		
		//System.out.println("selecting records");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			DataDAO cDao = new DataDAO();
			//System.out.println("selected records" + rs.getString(1));
			cDao.setDocId(rs.getLong(1));
			cDao.setRootID(rs.getLong(2));
			cDao.setServerID(rs.getLong(3));
			cDao.setProfileID(rs.getLong(4));
			cDao.setIndx1(rs.getString(5));
			cDao.setIndx2(rs.getString(6));
			cDao.setIndx3(rs.getString(7));
			cDao.setIndx4(rs.getString(8));
				cDao.setGuid(rs.getString(9));
			cDao.setProfileName(rs.getString(10));	
			cDao.setFormat(rs.getString(11));
			//System.out.println("Hello2"+rs.getString(4)+"   "+ rs.getString(5));
			
			
			list.add(cDao);
		}
		return list;

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		ps.close();
		conn.close();

	}
	return list;
}
	public List getDBIndexResults(Long userId,Long dbId) {
	List map = new ArrayList();
	CommonDAO cDao = new CommonDAO();
	PreparedStatement ps = null;
	try {
		System.out.println("getDBIndexResults" + userId);
		if (userId != 0) {
			String query = "select ur.INDEX_RESULTS_PERM from USER_INDEX_RESULTS ur,USER_MGMT u, USER_DB_INDEX ui,DATABASE_INDEX di where u.USER_ID = ui.USER_ID and ui.USER_INDEX_ID = ur.USER_INDEX_ID and di.INDEX_ID = ui.INDEX_ID and  ui.USER_ID=?";
			
					
			ps = dao.getDBConnection().prepareStatement(query);
			ps.setLong(1, userId);
			//ps.setLong(2,dbId );

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println("indexes"
						+ cDao.CheckDecryptData(rs.getString(1)));
				map.add(cDao.CheckDecryptData(rs.getString(1)));
			}

		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return map;
}

	public List<DataDAO> getIndexResults(Long profileID) throws SQLException {
		PreparedStatement ps = null;
		Connection conn = dao.getDBConnection();
		List<DataDAO> list = new ArrayList<DataDAO>();
		try {
			System.out.println("getSearchResults from Doc Management Table as per Profile ID");
			String query = "";
			
				query = "select d.ROOT_ID,d.SERVER_ID,d.REPOSITORY_ID,d.account,d.address,d.name,d.invlink,d.GUID, p.DB_NAME,d.FORMAT from CSR_DATABASE p, DOC_METADATA d where p.DB_ID=d.REPOSITORY_ID and p.DB_ID=?";
				ps = conn.prepareStatement(query);
				ps.setLong(1, profileID);
			
			
			System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDAO cDao = new DataDAO();
				System.out.println("selected records" + rs.getString(1));
				cDao.setDocId(rs.getLong(1));
				cDao.setRootID(rs.getLong(2));
				cDao.setServerID(rs.getLong(3));
				cDao.setProfileID(rs.getLong(4));
				cDao.setIndx1(rs.getString(5));
				cDao.setIndx2(rs.getString(6));
				cDao.setIndx3(rs.getString(7));
				cDao.setIndx4(rs.getString(8));
				cDao.setGuid(rs.getString(9));
				cDao.setProfileName(rs.getString(10));	
				cDao.setFormat(rs.getString(11));
				System.out.println("Hello2"+rs.getString(4)+"   "+ rs.getString(5));
				list.add(cDao);
			}
			return list;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();

		}
		return list;
	}
	//select di.INDEX_ID, di.INDEX_NAME,di.DB_ID,c.DB_NAME from  [CSR-APP].dbo.DATABASE_INDEX di,[CSR-APP].dbo.USER_DB_INDEX u,[CSR-APP].dbo.USER_MGMT um, [CSR-APP].dbo.PROFILES_DOC_METADATA p,[CSR-APP].dbo.CSR_DATABASE c where um.USER_ID = u.USER_ID and u.INDEX_ID = di.INDEX_ID and um.USER_ID = 29 and di.DB_ID = c.DB_ID 
	//select C.INDEX_NAME
	//from USER_DB_INDEX A, USER_INDEX C,DATABASE_INDEX D
	//where A.USER_ID=29 and D.DB_ID=1
	//and A.INDEX_ID=C.INDEX_ID
	//and C.INDEX_ID=D.INDEX_ID
	
	public List<DataDAO> getSearchValues(Long UserId,String textSearch)
	{
		PreparedStatement ps = null;
		Connection conn = dao.getDBConnection();
		List<DataDAO> list = new ArrayList<DataDAO>();
		try
		{
			System.out.println("getSearchValues::"+textSearch);
			String sql = "select d.ROOT_ID,d.SERVER_ID,d.REPOSITORY_ID,d.account,d.address,d.name,d.invlink,d.FileName,d.GUID, p.DB_NAME,d.DOC_ID,d.FORMAT from DOC_METADATA d left join CSR_DATABASE p on d.REPOSITORY_ID = p.DB_ID left join USER_DATABASE u on p.DB_ID = u.DB_ID where d.STATUS= 'O' and d.account like '%"+textSearch+"%' or d.address like '%"+textSearch+"%' or d.name like '%"+textSearch+"%' or d.invlink like '%"+textSearch+"%' or d.filename like '%"+textSearch+"%' and u.User_ID=?";
			System.out.println("getSearchValues:sql:"+sql);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, UserId);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDAO cDao = new DataDAO();
				cDao.setRootID(rs.getLong(1));
				cDao.setServerID(rs.getLong(2));
				cDao.setProfileID(rs.getLong(3));
				cDao.setIndx1(rs.getString(4));
				cDao.setIndx2(rs.getString(5));
				cDao.setIndx3(rs.getString(6));
				cDao.setIndx4(rs.getString(7));
				cDao.setIndx5(rs.getString(8));
				cDao.setGuid(rs.getString(9));
				cDao.setProfileName(rs.getString(10));	
				cDao.setDocId(rs.getLong(11));
				cDao.setFormat(rs.getString(12));
				System.out.println("coming here:"+rs.getLong(3));
				list.add(cDao);
			}
		}
		catch(Exception e)
		{
			
		}
		return list;
	}
	
	public List<DataDAO> getSearchValues(Long UserId,String Namelist,String Accountlist, String date)
	{
		PreparedStatement ps = null;
		Connection conn = dao.getDBConnection();
		List<DataDAO> list = new ArrayList<DataDAO>();
		try
		{
			System.out.println("getSearchValues::"+Namelist+" "+Accountlist+" "+date);
			String sql = "select d.ROOT_ID,d.SERVER_ID,d.REPOSITORY_ID,d.account,d.address,d.name,d.invlink,d.FileName,d.GUID, p.DB_NAME,d.DOC_ID,d.FORMAT from DOC_METADATA d left join CSR_DATABASE p on d.REPOSITORY_ID = p.DB_ID left join USER_DATABASE u on p.DB_ID = u.DB_ID where d.STATUS= 'O' and d.account = '"+Accountlist+"' or d.name = '"+Namelist+"' or d.invlink = '"+date+"' and u.User_ID=?";
			System.out.println("getSearchValues:sql:"+sql);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, UserId);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDAO cDao = new DataDAO();
				cDao.setRootID(rs.getLong(1));
				cDao.setServerID(rs.getLong(2));
				cDao.setProfileID(rs.getLong(3));
				cDao.setIndx1(rs.getString(4));
				cDao.setIndx2(rs.getString(5));
				cDao.setIndx3(rs.getString(6));
				cDao.setIndx4(rs.getString(7));
				cDao.setIndx5(rs.getString(8));
				cDao.setGuid(rs.getString(9));
				cDao.setProfileName(rs.getString(10));	
				cDao.setDocId(rs.getLong(11));
				cDao.setFormat(rs.getString(12));
				System.out.println("coming here:"+rs.getLong(3));
				list.add(cDao);
			}
		}
		catch(Exception e)
		{
			
		}
		return list;
	}
	
	public List<DataDAO> getSearchValuesAcc(Long UserId,String textSearch)
	{
		PreparedStatement ps = null;
		Connection conn = dao.getDBConnection();
		List<DataDAO> acclist = new ArrayList<DataDAO>();
		try
		{
			System.out.println("getSearchValues::"+textSearch);
			String sql = "select d.ROOT_ID,d.SERVER_ID,d.REPOSITORY_ID,d.account,d.address,d.name,d.invlink,d.FileName,d.GUID, p.DB_NAME,d.DOC_ID,d.FORMAT from DOC_METADATA d left join CSR_DATABASE p on d.REPOSITORY_ID = p.DB_ID left join USER_DATABASE u on p.DB_ID = u.DB_ID where d.STATUS= 'O' and d.account like '%"+textSearch+"%' or d.address like '%"+textSearch+"%' or d.name like '%"+textSearch+"%' or d.invlink like '%"+textSearch+"%' or d.filename like '%"+textSearch+"%' and u.User_ID=?";
			System.out.println("getSearchValues:sql:"+sql);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, UserId);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDAO accDao = new DataDAO();
				accDao.setIndx1(rs.getString(4));
				acclist.add(accDao);
			}
		}
		catch(Exception e)
		{
			
		}
		return acclist;
	}
	
	public List<DataDAO> getSearchValuesName(Long UserId,String textSearch)
	{
		PreparedStatement ps = null;
		Connection conn = dao.getDBConnection();
		List<DataDAO> namelist = new ArrayList<DataDAO>();
		try
		{
			System.out.println("getSearchValues::"+textSearch);
			String sql = "select d.ROOT_ID,d.SERVER_ID,d.REPOSITORY_ID,d.account,d.address,d.name,d.invlink,d.FileName,d.GUID, p.DB_NAME,d.DOC_ID,d.FORMAT from DOC_METADATA d left join CSR_DATABASE p on d.REPOSITORY_ID = p.DB_ID left join USER_DATABASE u on p.DB_ID = u.DB_ID where d.STATUS= 'O' and d.account like '%"+textSearch+"%' or d.address like '%"+textSearch+"%' or d.name like '%"+textSearch+"%' or d.invlink like '%"+textSearch+"%' or d.filename like '%"+textSearch+"%' and u.User_ID=?";
			System.out.println("getSearchValues:sql:"+sql);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, UserId);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDAO nameDao = new DataDAO();
				nameDao.setIndx3(rs.getString(6));
				namelist.add(nameDao);
			}
		}
		catch(Exception e)
		{
			
		}
		return namelist;
	}
	
	public List<DataDAO> getUserIndex(Long userId, Long profileID) throws SQLException
	{
		PreparedStatement ps = null;
		Connection conn = dao.getDBConnection();
		List<DataDAO> list = new ArrayList<DataDAO>();
		
		try
		{
			String query = "";
			System.out.println("getUserIndex from as per DB ID and User ID");
			if(profileID != null && !profileID.equals(0))
			{
			query = "select di.DB_ID,ui.INDEX_ID,di.INDEX_NAME,u.USER_ID,c.DB_NAME,di.INDEX_DESC from USER_MGMT u, USER_DB_INDEX ui, DATABASE_INDEX di,CSR_DATABASE c where u.USER_ID= ui.USER_ID and ui.INDEX_ID = di.INDEX_ID and u.USER_ID=? and c.DB_ID=di.DB_ID and di.DB_ID=?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, userId);
			ps.setLong(2, profileID);
			
			/*else
			{
				query = "select di.DB_ID,ui.INDEX_ID,di.INDEX_NAME,u.USER_ID,c.DB_NAME,di.INDEX_DESC from USER_MGMT u, USER_DB_INDEX ui, DATABASE_INDEX di,CSR_DATABASE c where u.USER_ID= ui.USER_ID and ui.INDEX_ID = di.INDEX_ID and u.USER_ID=? and c.DB_ID=di.DB_ID";
				ps = conn.prepareStatement(query);
				ps.setLong(1, userId);
				//ps.setLong(2, profileID);
			}
			*/
			//String query = "select C.INDEX_NAME from USER_DB_INDEX A, USER_INDEX C, DATABASE_INDEX D where A.USER_ID=? and D.DB_ID=? and A.INDEX_ID=C.INDEX_ID and C.INDEX_ID=D.INDEX_ID";
			
			System.out.println("Selecting Index Records: "+query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDAO cDao = new DataDAO();
				System.out.println("selected records" + rs.getString(3));
				cDao.setIndexName(rs.getString(3));
				cDao.setIndexDesc(rs.getString(6));
				list.add(cDao);
			}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();

		}
		return list;
	}
	
		public boolean AddToFavourites(Long userId,Long prId, String favName, String favDesc, String query, int results) throws SQLException
		{
			PreparedStatement ps = null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			Date date = new Date();
			try
			{
				SimpleDateFormat formatter = new SimpleDateFormat(
						CommonConstants.DATE_FORMAT);
				String s = formatter.format(date);
				String sql = "INSERT INTO USER_SEARCH_FAV(QUERY,REPOSIROTY_ID,USER_ID,RESULTS_SIZE,CREATED_DATE,FAV_NAME,FAV_DESC,DELETE_FLAG) VALUES(?,?,?,?,?,?,?,?)";
				ps = conn.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, query);
				ps.setLong(2, prId);
				ps.setLong(3, userId);
				ps.setInt(4, results);
				ps.setString(5, s);
				ps.setString(6, favName);
				ps.setString(7, favDesc);
				ps.setString(8, "N");
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next())
				b= true;
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				conn.close();

			}
			return b;
		}
		public boolean EditFavourites(Long userId,Long favouriteId,String favName,String favDesc) throws SQLException
		{
			PreparedStatement ps = null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			Date date = new Date();
			try
			{
				System.out.println("Here..EditFavourites:"+userId);
				SimpleDateFormat formatter = new SimpleDateFormat(
						CommonConstants.DATE_FORMAT);
				String s = formatter.format(date);
				String sql = "UPDATE USER_SEARCH_FAV SET FAV_NAME=?,UPDATED_DATE=?,FAV_DESC=? where USER_ID=? and FAVOURITE_ID=?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, favName);
				ps.setString(2, s);
				ps.setString(3, favDesc);
				ps.setLong(4, userId);
				ps.setLong(5, favouriteId);
				
				ps.executeUpdate();
				//ResultSet rs = ps.getGeneratedKeys();
				//if(rs.next())
				b= true;
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				conn.close();

			}
			return b;
		}
		public boolean DeleteFavourites(Long userId,Long favouriteId) throws SQLException
		{
			PreparedStatement ps = null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			Date date = new Date();
			try
			{
				
				System.out.println("Here..DeleteFavourites:"+userId);SimpleDateFormat formatter = new SimpleDateFormat(
						CommonConstants.DATE_FORMAT);
				String s = formatter.format(date);
				
				String sql = "UPDATE USER_SEARCH_FAV SET DELETE_FLAG=?, DELETED_DATE=? where USER_ID=? and FAVOURITE_ID=?";
				ps = conn.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, "Y");
				ps.setString(2, s);
				ps.setLong(3, userId);
				ps.setLong(4, favouriteId);
				
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next())
				b= true;
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				conn.close();

			}
			return b;
		}
		public boolean AddDocsToFavourites(Long userId,Long prId, String favName, String favDesc, String guid,Long docId) throws SQLException
		{
			PreparedStatement ps = null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			Date date = new Date();
			try
			{
				SimpleDateFormat formatter = new SimpleDateFormat(
						CommonConstants.DATE_FORMAT);
				String s = formatter.format(date);
				String sql = "INSERT INTO USER_FAV_DOCS(DOC_GUID,REPOSIROTY_ID,USER_ID,CREATED_DATE,FAV_NAME,FAV_DESC,DELETE_FLAG,DOC_ID) VALUES(?,?,?,?,?,?,?,?)";
				ps = conn.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, guid);
				ps.setLong(2, prId);
				ps.setLong(3, userId);
				ps.setString(4, s);
				ps.setString(5, favName);
				ps.setString(6, favDesc);
				ps.setString(7, "N");
				ps.setLong(8,docId);
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next())
				b= true;
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				conn.close();

			}
			return b;
		}
		public boolean EditDocsToFavourites(Long userId,Long favdocId, String favName, String favDesc) throws SQLException
		{
			PreparedStatement ps = null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			Date date = new Date();
			try
			{
				System.out.println("Here..EditDocsToFavourites:"+userId);
				SimpleDateFormat formatter = new SimpleDateFormat(
						CommonConstants.DATE_FORMAT);
				String s = formatter.format(date);
				String sql = "UPDATE USER_FAV_DOCS SET FAV_NAME=? , UPDATED_DATE=? , FAV_DESC=? where USER_ID=? and FAV_DOC_ID=?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, favName);
				ps.setString(2, s);
				ps.setString(3, favDesc);
				ps.setLong(4, userId);
				ps.setLong(5, favdocId);
				ps.executeUpdate();
				//ResultSet rs = ps.getGeneratedKeys();
				//if(rs.next())
				b= true;
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				conn.close();

			}
			return b;
		}
		public boolean DeleteDocsToFavourites(Long userId,Long favdocId, String favName, String favDesc) throws SQLException
		{
			PreparedStatement ps = null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			Date date = new Date();
			try
			{
				System.out.println("Here..DeleteDocsToFavourites:"+userId);
				SimpleDateFormat formatter = new SimpleDateFormat(
						CommonConstants.DATE_FORMAT);
				String s = formatter.format(date);
				String sql = "UPDATE USER_FAV_DOCS SET DELETE_FLAG=? , DELETED_DATE=? where USER_ID=? and FAV_DOC_ID=?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, "Y");
				ps.setString(2, s);
				ps.setLong(3, userId);
				ps.setLong(4, favdocId);
				
				ps.executeUpdate();
				//ResultSet rs = ps.getGeneratedKeys();
				//if(rs.next())
				b= true;
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				conn.close();

			}
			return b;
		}
		public boolean GetDocFavourites(Long userId,Long prId, String favName, String type) throws SQLException
		{
			PreparedStatement ps = null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			Date date = new Date();
			try
			{
				SimpleDateFormat formatter = new SimpleDateFormat(
						CommonConstants.DATE_FORMAT);
				String s = formatter.format(date);
				String sql = "";
				if(type.equals("guid"))
				{
					sql = "SELECT * from USER_FAV_DOCS where USER_ID=? and DOC_GUID=? and DELETE_FLAG=?";// INTO USER_SEARCH_FAV(QUERY,REPOSIROTY_ID,USER_ID,RESULTS_SIZE,CREATED_DATE,FAV_NAME,FAV_DESC) VALUES(?,?,?,?,?,?,?)";
					
				}
				else if(type.equals("name"))
				{
				sql = "SELECT * from USER_FAV_DOCS where USER_ID=? and FAV_NAME=? and DELETE_FLAG=?";// INTO USER_SEARCH_FAV(QUERY,REPOSIROTY_ID,USER_ID,RESULTS_SIZE,CREATED_DATE,FAV_NAME,FAV_DESC) VALUES(?,?,?,?,?,?,?)";
				}
				ps = conn.prepareStatement(sql);
				
				
				ps.setLong(1, userId);
				
				ps.setString(2, favName);
				ps.setString(3, "N");
				//ps.executeUpdate();
				ResultSet rs = ps.executeQuery();
				if(rs.next())
				b= true;
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				conn.close();

			}
			return b;
		}
		public boolean GetFavourites(Long userId,Long prId, String favName) throws SQLException
		{
			PreparedStatement ps = null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			Date date = new Date();
			try
			{
				SimpleDateFormat formatter = new SimpleDateFormat(
						CommonConstants.DATE_FORMAT);
				String s = formatter.format(date);
				String sql = "SELECT * from USER_SEARCH_FAV where USER_ID=? and FAV_NAME=? and DELETE_FLAG=?";// INTO USER_SEARCH_FAV(QUERY,REPOSIROTY_ID,USER_ID,RESULTS_SIZE,CREATED_DATE,FAV_NAME,FAV_DESC) VALUES(?,?,?,?,?,?,?)";
				ps = conn.prepareStatement(sql);
				
				
				ps.setLong(1, userId);
				
				ps.setString(2, favName);
				ps.setString(3, "N");
				//ps.executeUpdate();
				ResultSet rs = ps.executeQuery();
				if(rs.next())
				b= true;
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				conn.close();

			}
			return b;
		}
		public List<DataDAO> GetListFavouritesByName(Long userId) throws SQLException
		{
			PreparedStatement ps=null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			List searchDisplay = new ArrayList();
			System.out.println("GetListFavourites--");
			try
			{
				String sql = "SELECT u.FAVOURITE_ID,u.QUERY,u.REPOSIROTY_ID,u.USER_ID,u.RESULTS_SIZE,u.FAV_NAME,u.FAV_DESC FROM USER_SEARCH_FAV u where u.USER_ID=? and u.DELETE_FLAG=?";
				ps = conn.prepareStatement(sql);
				
				
				ps.setLong(1, userId);
				ps.setString(2, "N");
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					System.out.println("coming here:GetListFavourites--"+rs.getString(6));
					DataDAO dao = new DataDAO();
					dao.setFavId(rs.getLong(1));
					dao.setProfileID(rs.getLong(3));
					dao.setFavName(rs.getString(6));
					
					dao.setFavDesc(rs.getString(7));
					searchDisplay.add(dao);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				
				conn.close();

			}
			return searchDisplay;
		}
		public List<DataDAO> GetListFavouriteByDocument(Long userId) throws SQLException
		{
			PreparedStatement ps=null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			List searchDisplay = new ArrayList();
			System.out.println("GetListFavouriteByDocument--"+userId);
			try
			{
				String sql = "SELECT u.FAV_DOC_ID,u.REPOSIROTY_ID,u.FAV_NAME,u.DOC_GUID,u.USER_ID,u.FAV_DESC,u.DOC_ID FROM USER_FAV_DOCS u where u.USER_ID=? and u.DELETE_FLAG=?";
				ps = conn.prepareStatement(sql);
				
				
				ps.setLong(1, userId);
				ps.setString(2, "N");
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					System.out.println("coming here:GetListFavouriteByDocument--"+rs.getString(6));
					DataDAO dao = new DataDAO();
					dao.setFavDocId(rs.getLong(1));
					dao.setProfileID(rs.getLong(2));
					dao.setFavName(rs.getString(3));
					dao.setGuid(rs.getString(4));
					dao.setFavDesc(rs.getString(6));
					dao.setDocId(rs.getLong(7));
					searchDisplay.add(dao);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				
				conn.close();

			}
			return searchDisplay;
		}
		public DataDAO GetFavouriteByDocument(Long userId,Long favId) throws SQLException
		{
			PreparedStatement ps=null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			DataDAO dao = null;
			List searchDisplay = new ArrayList();
			System.out.println("GetListFavouriteByDocument--"+userId);
			try
			{
				String sql = "SELECT u.FAV_DOC_ID,u.REPOSIROTY_ID,u.FAV_NAME,u.DOC_GUID,u.USER_ID,u.FAV_DESC, u.DOC_ID FROM USER_FAV_DOCS u where u.USER_ID=? and u.FAV_DOC_ID=?  and u.DELETE_FLAG=?";
				ps = conn.prepareStatement(sql);
				
				ps.setLong(1, userId);
				ps.setLong(2, favId);
				ps.setString(3, "N");
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					System.out.println("coming here:GetListFavouriteByDocument--"+rs.getString(6));
					dao = new DataDAO();
					dao.setFavDocId(rs.getLong(1));
					dao.setProfileID(rs.getLong(2));
					dao.setFavName(rs.getString(3));
					dao.setGuid(rs.getString(4));
					dao.setFavDesc(rs.getString(6));
					dao.setDocId(rs.getLong(7));
					searchDisplay.add(dao);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				
				conn.close();

			}
			return dao;
		}
		public List<DataDAO> GetListFavourites(Long userId) throws SQLException
		{
			PreparedStatement ps=null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			List searchDisplay = new ArrayList();
			System.out.println("GetListFavourites--");
			try
			{
				String sql = "SELECT u.FAVOURITE_ID,u.QUERY,u.REPOSIROTY_ID,u.USER_ID,u.RESULTS_SIZE,c.DB_DESC,r.ROOT_ID,rm.ROOT_NAME FROM USER_SEARCH_FAV u right join ROOT_DB_Data r on r.DB_ID = u.REPOSIROTY_ID right join ROOT_DOC_METADATA rm on r.ROOT_ID=rm.ROOT_ID right join CSR_DATABASE c on r.DB_ID = c.DB_ID where u.USER_ID=? and u.DELETE_FLAG=?";
				ps = conn.prepareStatement(sql);
				
				
				ps.setLong(1, userId);
				ps.setString(2, "N");
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					System.out.println("coming here:GetListFavourites--"+rs.getString(6));
					DataDAO dao = new DataDAO();
					dao.setProfileID(rs.getLong(3));
					dao.setProfileName(rs.getString(6));
					dao.setRootID(rs.getLong(7));
					dao.setRootName(rs.getString(8));
					searchDisplay.add(dao);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				
				conn.close();

			}
			return searchDisplay;
		}
		/*public List<DataDAO> GetFavourites(Long userId,Long prId) throws SQLException
		{
			PreparedStatement ps=null,ps1 = null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			List searchDisplay = new ArrayList();
			try
			{
				String sql = "SELECT u.FAVOURITE_ID,u.QUERY,u.REPOSIROTY_ID,u.USER_ID,u.RESULTS_SIZE,c.DB_DESC,r.ROOT_ID,rm.ROOT_NAME FROM USER_SEARCH_FAV u right join ROOT_DB_Data r on r.DB_ID = u.REPOSIROTY_ID right join ROOT_DOC_METADATA rm on r.ROOT_ID=rm.ROOT_ID right join CSR_DATABASE c on r.DB_ID = c.DB_ID where u.USER_ID=? and u.REPOSIROTY_ID=?";
				ps = conn.prepareStatement(sql);
				
				
				ps.setLong(1, userId);
				ps.setLong(2, prId);
				
				ResultSet rs = ps.executeQuery();
				if(rs.next())
				{
					String query = rs.getString(2);
					System.out.println("query is from database::" + query);
					ps1 = conn.prepareStatement(query);
					ResultSet rsRes = ps1.executeQuery();
					while (rsRes.next()) {
						DataDAO cDao = new DataDAO();
						System.out.println("selected records");
						cDao.setRootID(rsRes.getLong(1));
						cDao.setServerID(rsRes.getLong(2));
						cDao.setProfileID(rsRes.getLong(3));
						cDao.setIndx1(rsRes.getString(4));
						cDao.setIndx2(rsRes.getString(5));
						cDao.setIndx3(rsRes.getString(6));
						cDao.setIndx4(rsRes.getString(7));
						cDao.setIndx5(rsRes.getString(8));
						cDao.setGuid(rsRes.getString(9));
						cDao.setProfileName(rsRes.getString(10));		
						//System.out.println("Hello2"+rs1.getString(4)+"   "+ rs1.getString(5));
						searchDisplay.add(cDao);
					}
					}
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				ps1.close();
				conn.close();

			}
			return searchDisplay;
		}*/
		public List<DataDAO> GetFavourites(Long userId,Long favId) throws SQLException
		{
			PreparedStatement ps=null,ps1 = null;
			Connection conn = dao.getDBConnection();
			List<DataDAO> list = new ArrayList<DataDAO>();
			boolean b = false;
			List searchDisplay = new ArrayList();
			try
			{
				String sql = "SELECT u.FAVOURITE_ID,u.QUERY,u.REPOSIROTY_ID,u.USER_ID,u.RESULTS_SIZE FROM USER_SEARCH_FAV u where u.USER_ID=? and u.FAVOURITE_ID=? and u.DELETE_FLAG=?";
				ps = conn.prepareStatement(sql);
				
				
				ps.setLong(1, userId);
				ps.setLong(2, favId);
				ps.setString(3, "N");
				ResultSet rs = ps.executeQuery();
				if(rs.next())
				{
					String query = rs.getString(2);
					System.out.println("query is from database::" + query);
					ps1 = conn.prepareStatement(query);
					ResultSet rsRes = ps1.executeQuery();
					while (rsRes.next()) {
						DataDAO cDao = new DataDAO();
						System.out.println("selected records");
						cDao.setRootID(rsRes.getLong(1));
						cDao.setServerID(rsRes.getLong(2));
						cDao.setProfileID(rsRes.getLong(3));
						cDao.setIndx1(rsRes.getString(4));
						cDao.setIndx2(rsRes.getString(5));
						cDao.setIndx3(rsRes.getString(6));
						cDao.setIndx4(rsRes.getString(7));
						cDao.setIndx5(rsRes.getString(8));
						cDao.setGuid(rsRes.getString(9));
						cDao.setProfileName(rsRes.getString(10));
						if(rsRes.getLong(11) != 0)
						{
						cDao.setDocId(rsRes.getLong(11));
						}
						//System.out.println("Hello2"+rs1.getString(4)+"   "+ rs1.getString(5));
						searchDisplay.add(cDao);
					}
					}
			}
			catch (Exception e) {
				e.printStackTrace();
			} finally {
				ps.close();
				ps1.close();
				conn.close();

			}
			return searchDisplay;
		}
		
		
	}
