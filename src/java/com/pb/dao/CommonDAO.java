package com.pb.dao;

import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;
import com.pb.common.PropertyUtils;

public class CommonDAO {
//	DataEncryptDecrypt dataEncDec = new DataEncryptDecrypt();

	public String CheckDecryptData(String data) throws Exception {
		
		 String dbencrypt=PropertyUtils.getProperties(
					CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
					CommonConstants.DB_ENCRYPT);
		if(dbencrypt.equalsIgnoreCase("NO"))
			return data;
		
		if (data != null)
			data = DataEncryptDecrypt.decryptData(data);
		else
			data = "";
		return data;
	}

	public String CheckEncryptData(String data) throws Exception {
		
		 String dbencrypt=PropertyUtils.getProperties(
					CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
					CommonConstants.DB_ENCRYPT);
		if(dbencrypt.equalsIgnoreCase("NO"))
			return data;
		
		if (data != null)
			data = DataEncryptDecrypt.encryptData(data);
		else
			data = "";
		return data;
	}
	public static void main(String[] args)
	{
		try {
			System.out.println(DataEncryptDecrypt.encryptData("pitney@123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
