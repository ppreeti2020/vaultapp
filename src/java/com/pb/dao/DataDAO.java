/*
 * created @ sep 28,2011
 * @author suhasini
 * setter and getter methods for database objects
 */
package com.pb.dao;

import java.util.ArrayList;
import java.util.List;

public class DataDAO {

	private Long logId;
	private Long sresId;
	private Long serId;
	private String loginName;
	private String loginGroup;
	private String loginDate;
	private String loginTime;
	private String resCount;
	private String searchValue;
	private String searchDate;
	private Long dbId;
	private String dbName;
	private String dbDesc;
	private String nricNo;
	private String logout;
	private String logoutDate;
	private List reportList;
	private String searchResults;
	private String accNumber;
	private String accName;
	private int count;
	private int countLog;
	private String docgenDate;
	private String contentType;
	private String docAction;
	private String IP4;
	private String IP6;
	private String browser;
	private String logDate;
	private String intCheckDate;
	private String hostName;
	private String ipAddr;
	private String fileCreatedDate;
	private String sha1;
	private String filePath;
	private String fileName;
	private String newFileName;
	private Long inteId;
	private String hashId;
	private Long logoutId;
	private Long docId;
	private String docDesc;
	private String docAccessDate;
	private int fileSize;
	private String createdDate;
	private String fileAction;
	private Long e2LogId;
	private String logBlob;
	private String journalFile;
	private String compressingFile;
	private String profile;
	private String format;
	private String pages;
	private String totalRead;
	private String totalWrite;
	private String finalRatio;
	private String method;
	private String documents;
	private String documentPages;
	private String ignoredPages;
	private String[] errors;
	private String resStartTime;
	private String resStartDate;
	private String resWrote;
	private String resSize;
	private String resFinishTime;
	private String resFinishDate;
	private String indexingFile;
	private String indexingStartTime;
	private String indexingFinishingTime;
	private String indexFileSize;
	private String indexDiskRead;
	private String indexDiskWrite;
	private String indexStackDepth;
	private String indexCacheReadHit;
	private String indexCacheReadMiss;
	private String indexCacheWriteHit;
	private String indexCacheWriteMiss;
	private String indexAddDuplicate;
	private String indexAddUnique;
	private String logTime;
	private long logFileId;
	private String logEndDate;
	private String logEndTime;
	private String[] errorCode;
	private String drpFile;
	private String drdFile;
	private String fileStatus;
	private String compressingType;
	private String errorQccurs;
	private Long resourceId;
	private int resourceCount;
	private Long errorId;
	private String errCode;
	private String errDesc;
	private int errCount;
	private int indexCount;
	private String indexName;
	private Long dbProfileID;
	private Long rootID;
	private String rootName;
	private Long serverID;
	private Long profileID;
	private Long pdbID;
	private String pdbName;
	private String indx1;
	private String indx2;
	private String indx3;
	private String indx4;
	private String indx5;
	private String indx6;
	private String indx7;
	private String indx8;
	private String indx9;
	private String indx10;
	private String indx11;
	private String indx12;
	private String guid;
	private String status;
	private String created_dt;
	private String created_by;
	private String updated_dt;
	private String updated_by;
	private String deleted_dt;
	private String deleted_by;
	private String dbProfileName;
	private String profileName;
	//private Long docId;
	private String favName;
	private String favDesc;
	private Long favId;
	private Long favDocId;
	private String pointer;
	private String indexDesc;
	private String printerName;
	private String docRefNo;
	private Long hostId;
	
	private String attachmentPath;
	private String attachment1;
	private String attachment2;
	private String attachment3;
	private String attachment4;
	
	public String getAttachmentPath() {
		return attachmentPath;
	}

	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}
	
	public String getAttachment1() {
		return attachment1;
	}

	public void setAttachment1(String attachment1) {
		this.attachment1 = attachment1;
	}
	
	public String getAttachment2() {
		return attachment2;
	}

	public void setAttachment2(String attachment2) {
		this.attachment2 = attachment2;
	}
	
	public String getAttachment3() {
		return attachment3;
	}

	public void setAttachment3(String attachment3) {
		this.attachment3 = attachment3;
	}
	
	public String getAttachment4() {
		return attachment4;
	}

	public void setAttachment4(String attachment4) {
		this.attachment4 = attachment4;
	}	
	
	public void setHostId(Long hostId) {
		this.hostId = hostId;
	}

	public Long getHostId() {
		return hostId;
	}
	
	public void setDocRefNo(String docRefNo) {
		this.docRefNo = docRefNo;
	}

	public String getDocRefNo() {
		return docRefNo;
	}
	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}

	public String getPrinterName() {
		return printerName;
	}

	public void setIndexDesc(String indexDesc) {
		this.indexDesc = indexDesc;
	}

	public String getIndexDesc() {
		return this.indexDesc;
	}

	public void setPointer(String pointer) {
		this.pointer = pointer;
	}

	public String getPointer() {
		return this.pointer;
	}

	public void setFavDocId(Long favDocId) {
		this.favDocId = favDocId;
	}

	public Long getFavDocId() {
		return this.favDocId;
	}

	public void setFavName(String favName) {
		this.favName = favName;
	}

	public String getFavName() {
		return this.favName;
	}

	public void setFavDesc(String favDesc) {
		this.favDesc = favDesc;
	}

	public String getFavDesc() {
		return this.favDesc;
	}

	public void setFavId(Long favId) {
		this.favId = favId;
	}

	public Long getFavId() {
		return this.favId;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public String getIndexName() {
		return this.indexName;
	}

	public void setIndexCount(int indexCount) {
		this.indexCount = indexCount;
	}

	public int getIndexCount() {
		return this.indexCount;
	}

	public void setResourceCount(int resourceCount) {
		this.resourceCount = resourceCount;
	}

	public int getResourceCount() {
		return this.resourceCount;
	}

	public void setErrCount(int errCount) {
		this.errCount = errCount;
	}

	public int getErrCount() {
		return this.errCount;
	}

	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}

	public String getErrDesc() {
		return this.errDesc;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrCode() {
		return this.errCode;
	}

	public void setErrorId(Long errorId) {
		this.errorId = errorId;
	}

	public Long getErrorId() {
		return this.errorId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public Long getResourceId() {
		return this.resourceId;
	}

	public void setErrorQccurs(String errorQccurs) {
		this.errorQccurs = errorQccurs;
	}

	public String getErrorQccurs() {
		return this.errorQccurs;
	}

	public void setCompressingType(String compressingType) {
		this.compressingType = compressingType;
	}

	public String getCompressingType() {
		return this.compressingType;
	}

	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}

	public String getFileStatus() {
		return this.fileStatus;
	}

	public String getDrdFile() {
		return this.drdFile;
	}

	public void setDrdFile(String drdFile) {
		this.drdFile = drdFile;
	}

	public String getDrpFile() {
		return this.drpFile;
	}

	public void setDrpFile(String drpFile) {
		this.drpFile = drpFile;
	}

	public String[] getErrorCode() {
		return this.errorCode;
	}

	public void setErrorCode(String[] errorCode) {
		this.errorCode = errorCode;
	}

	public void setIndexingFinishingTime(String indexingFinishingTime) {
		this.indexingFinishingTime = indexingFinishingTime;
	}

	public String getIndexingFinishingTime() {
		return this.indexingFinishingTime;
	}

	public void setLogEndDate(String logEndDate) {
		this.logEndDate = logEndDate;
	}

	public String getLogEndDate() {
		return this.logEndDate;
	}

	public void setLogEndTime(String logEndTime) {
		this.logEndTime = logEndTime;
	}

	public String getLogEndTime() {
		return this.logEndTime;
	}

	public void setLogFileId(long logFileId) {
		this.logFileId = logFileId;
	}

	public long getLogFileId() {
		return this.logFileId;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}

	public String getLogTime() {
		return this.logTime;
	}

	public void setIndexAddUnique(String indexAddUnique) {
		this.indexAddUnique = indexAddUnique;
	}

	public String getIndexAddUnique() {
		return this.indexAddUnique;
	}

	public void setIndexAddDuplicate(String indexAddDuplicate) {
		this.indexAddDuplicate = indexAddDuplicate;
	}

	public String getIndexAddDuplicate() {
		return this.indexAddDuplicate;
	}

	public void setIndexCacheWriteMiss(String indexCacheWriteMiss) {
		this.indexCacheWriteMiss = indexCacheWriteMiss;
	}

	public String getIndexCacheWriteMiss() {
		return this.indexCacheWriteMiss;
	}

	public void setIndexCacheWriteHit(String indexCacheWriteHit) {
		this.indexCacheWriteHit = indexCacheWriteHit;
	}

	public String getIndexCacheWriteHit() {
		return this.indexCacheWriteHit;
	}

	public void setIndexCacheReadMiss(String indexCacheReadMiss) {
		this.indexCacheReadMiss = indexCacheReadMiss;
	}

	public String getIndexCacheReadMiss() {
		return this.indexCacheReadMiss;
	}

	public void setIndexCacheReadHit(String indexCacheReadHit) {
		this.indexCacheReadHit = indexCacheReadHit;
	}

	public String getIndexCacheReadHit() {
		return this.indexCacheReadHit;
	}

	public void setIndexStackDepth(String indexStackDepth) {
		this.indexStackDepth = indexStackDepth;
	}

	public String getIndexStackDepth() {
		return this.indexStackDepth;
	}

	public void setIndexDiskWrite(String indexDiskWrite) {
		this.indexDiskWrite = indexDiskWrite;
	}

	public String getIndexDiskWrite() {
		return this.indexDiskWrite;
	}

	public void setIndexDiskRead(String indexDiskRead) {
		this.indexDiskRead = indexDiskRead;
	}

	public String getIndexDiskRead() {
		return this.indexDiskRead;
	}

	public void setIndexFileSize(String indexFileSize) {
		this.indexFileSize = indexFileSize;
	}

	public String getIndexFileSize() {
		return this.indexFileSize;
	}

	public void setIndexingStartTime(String indexingStartTime) {
		this.indexingStartTime = indexingStartTime;
	}

	public String getIndexingStartTime() {
		return this.indexingStartTime;
	}

	public void setIndexingFile(String indexingFile) {
		this.indexingFile = indexingFile;
	}

	public String getIndexingFile() {
		return this.indexingFile;
	}

	public void setResFinishDate(String resFinishDate) {
		this.resFinishDate = resFinishDate;
	}

	public String getResFinishDate() {
		return this.resFinishDate;
	}

	public void setResFinishTime(String resFinishTime) {
		this.resFinishTime = resFinishTime;
	}

	public String getResFinishTime() {
		return this.resFinishTime;
	}

	public void setResSize(String resSize) {
		this.resSize = resSize;
	}

	public String getResSize() {
		return this.resSize;
	}

	public void setResWrote(String resWrote) {
		this.resWrote = resWrote;
	}

	public String getResWrote() {
		return this.resWrote;
	}

	public void setResStartDate(String resStartDate) {
		this.resStartDate = resStartDate;
	}

	public String getResStartDate() {
		return this.resStartDate;
	}

	public String[] getErrors() {
		return this.errors;
	}

	public void setErrors(String[] errors) {
		this.errors = errors;
	}

	public String getDocumentPages() {
		return this.documentPages;
	}

	public void setDocumentPages(String documentPages) {
		this.documentPages = documentPages;
	}

	public String getIgnoredPages() {
		return this.ignoredPages;
	}

	public void setIgnoredPages(String ignoredPages) {
		this.ignoredPages = ignoredPages;
	}

	public String getMethod() {
		return this.method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getDocuments() {
		return this.documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public String getFinalRatio() {
		return this.finalRatio;
	}

	public void setFinalRatio(String finalRatio) {
		this.finalRatio = finalRatio;
	}

	public String getTotalRead() {
		return this.totalRead;
	}

	public void setTotalRead(String totalRead) {
		this.totalRead = totalRead;
	}

	public String getTotalWrite() {
		return this.totalWrite;
	}

	public void setTotalWrite(String totalWrite) {
		this.totalWrite = totalWrite;
	}

	public String getPages() {
		return this.pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getProfile() {
		return this.profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getCompressingFile() {
		return this.compressingFile;
	}

	public void setCompressingFile(String compressingFile) {
		this.compressingFile = compressingFile;
	}

	public String getJournalFile() {
		return this.journalFile;
	}

	public void setJournalFile(String journalFile) {
		this.journalFile = journalFile;
	}

	public String getLogBlob() {
		return this.logBlob;
	}

	public void setLogBlob(String logBlob) {
		this.logBlob = logBlob;
	}

	public Long getE2LogId() {
		return this.e2LogId;
	}

	public void setE2LogId(Long e2LogId) {
		this.e2LogId = e2LogId;
	}

	public String getFileAction() {
		return this.fileAction;
	}

	public void setFileAction(String fileAction) {
		this.fileAction = fileAction;
	}

	public String getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getNewFileName() {
		return this.newFileName;
	}

	public void setNewFileName(String newFileName) {
		this.newFileName = newFileName;
	}

	public int getFileSize() {
		return this.fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getDocAccessDate() {
		return this.docAccessDate;
	}

	public void setDocAccessDate(String docAccessDate) {
		this.docAccessDate = docAccessDate;
	}

	public String getDocDesc() {
		return this.docDesc;
	}

	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	public Long getDocId() {
		return this.docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public Long getLogoutId() {
		return this.logoutId;
	}

	public void setLogoutId(Long logoutId) {
		this.logoutId = logoutId;
	}

	public String getHashId() {
		return this.hashId;
	}

	public void setHashId(String hashId) {
		this.hashId = hashId;
	}

	public Long getInteId() {
		return this.inteId;
	}

	public void setInteId(Long inteId) {
		this.inteId = inteId;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSha1() {
		return this.sha1;
	}

	public void setSha1(String sha1) {
		this.sha1 = sha1;
	}

	public String getFileCreatedDate() {
		return this.fileCreatedDate;
	}

	public void setFileCreatedDate(String fileCreatedDate) {
		this.fileCreatedDate = fileCreatedDate;
	}

	public String getIpAddr() {
		return this.ipAddr;
	}

	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	public String getHostName() {
		return this.hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getIntCheckDate() {
		return this.intCheckDate;
	}

	public void setIntCheckDate(String intCheckDate) {
		this.intCheckDate = intCheckDate;
	}

	public String getLogDate() {
		return this.logDate;
	}

	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}

	public String getIP4() {
		return this.IP4;
	}

	public void setIP4(String IP4) {
		this.IP4 = IP4;
	}

	public String getIP6() {
		return this.IP6;
	}

	public void setIP6(String IP6) {
		this.IP6 = IP6;
	}

	public String getBrowser() {
		return this.browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getDocAction() {
		return this.docAction;
	}

	public void setDocAction(String docAction) {
		this.docAction = docAction;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getDocgenDate() {
		return this.docgenDate;
	}

	public void setDocgenDate(String docgenDate) {
		this.docgenDate = docgenDate;
	}

	public int getCountLog() {
		return this.countLog;
	}

	public void setCountLog(int countLog) {
		this.countLog = countLog;
	}

	public int getCount() {
		return this.count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getAccNumber() {
		return this.accNumber;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public String getAccName() {
		return this.accName;
	}

	public void setAccName(String accName) {
		this.accName = accName;
	}

	public String getSearchResults() {
		return this.searchResults;
	}

	public void setSearchResults(String searchResults) {
		this.searchResults = searchResults;
	}

	public Long getLogId() {
		return this.logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public Long getSerId() {
		return this.serId;
	}

	public void setSerId(Long serId) {
		this.serId = serId;
	}

	public Long getSresId() {
		return this.sresId;
	}

	public void setSresId(Long sresId) {
		this.sresId = sresId;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginGroup() {
		return this.loginGroup;
	}

	public void setLoginGroup(String loginGroup) {
		this.loginGroup = loginGroup;
	}

	public String getLoginDate() {
		return this.loginDate;
	}

	public void setLoginDate(String loginDate) {
		this.loginDate = loginDate;
	}

	public String getLoginTime() {
		return this.loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public String getResCount() {
		return this.resCount;
	}

	public void setResCount(String resCount) {
		this.resCount = resCount;
	}

	public String getSearchValue() {
		return this.searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getSearchDate() {
		return this.searchDate;
	}

	public void setSearchDate(String searchDate) {
		this.searchDate = searchDate;
	}

	public List getReportList() {
		return this.reportList;
	}

	public void setReportList(ArrayList reportList) {
		this.reportList = reportList;
	}

	public String getDbName() {
		return this.dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	
	public String getDbDesc() {
		return this.dbDesc;
	}

	public void setDbDesc(String dbDesc) {
		this.dbDesc = dbDesc;
	}
	
	public Long getDbId() {
		return this.dbId;
	}

	public void setDbId(Long dbId) {
		this.dbId = dbId;
	}

	public String getNricNo() {
		return this.nricNo;
	}

	public void setNricNo(String nricNo) {
		this.nricNo = nricNo;
	}

	public String getLogout() {
		return this.logout;
	}

	public void setLogout(String logout) {
		this.logout = logout;
	}

	public String getLogoutDate() {
		return this.logoutDate;
	}

	public void setLogoutDate(String logoutDate) {
		this.logoutDate = logoutDate;
	}

	public void setResStartTime(String resStartTime) {
		this.resStartTime = resStartTime;
	}

	public String getResStartTime() {
		return this.resStartTime;
	}

	public void setDbProfileID(Long dbProfileID) {
		this.dbProfileID = dbProfileID;
	}

	public Long getDbProfileID() {
		return dbProfileID;
	}
	
	public void setRootID(Long rootID) {
		this.rootID = rootID;
	}

	public Long getRootID() {
		return rootID;
	}

	public void setServerID(Long serverID) {
		this.serverID = serverID;
	}

	public Long getServerID() {
		return serverID;
	}

	public void setProfileID(Long profileID) {
		this.profileID = profileID;
	}

	public Long getProfileID() {
		return profileID;
	}

	public void setIndx1(String indx1) {
		this.indx1 = indx1;
	}

	public String getIndx1() {
		return indx1;
	}

	public void setIndx2(String indx2) {
		this.indx2 = indx2;
	}

	public String getIndx2() {
		return indx2;
	}

	public void setIndx3(String indx3) {
		this.indx3 = indx3;
	}

	public String getIndx3() {
		return indx3;
	}
	
	public void setIndx4(String indx4) {
		this.indx4 = indx4;
	}

	public String getIndx4() {
		return indx4;
	}

	public void setIndx5(String indx5) {
		this.indx5 = indx5;
	}

	public String getIndx5() {
		return indx5;
	}

	public void setIndx6(String indx6) {
		this.indx6 = indx6;
	}

	public String getIndx6() {
		return indx6;
	}

	public void setIndx7(String indx7) {
		this.indx7 = indx7;
	}

	public String getIndx7() {
		return indx7;
	}

	public void setIndx8(String indx8) {
		this.indx8 = indx8;
	}

	public String getIndx8() {
		return indx8;
	}

	public void setIndx9(String indx9) {
		this.indx9 = indx9;
	}

	public String getIndx9() {
		return indx9;
	}

	public void setIndx10(String indx10) {
		this.indx10 = indx10;
	}

	public String getIndx10() {
		return indx10;
	}

	public void setIndx11(String indx11) {
		this.indx11 = indx11;
	}

	public String getIndx11() {
		return indx11;
	}

	public void setIndx12(String indx12) {
		this.indx12 = indx12;
	}

	public String getIndx12() {
		return indx12;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getGuid() {
		return guid;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setCreated_dt(String created_dt) {
		this.created_dt = created_dt;
	}

	public String getCreated_dt() {
		return created_dt;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setUpdated_dt(String updated_dt) {
		this.updated_dt = updated_dt;
	}

	public String getUpdated_dt() {
		return updated_dt;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setDeleted_dt(String deleted_dt) {
		this.deleted_dt = deleted_dt;
	}

	public String getDeleted_dt() {
		return deleted_dt;
	}

	public void setDeleted_by(String deleted_by) {
		this.deleted_by = deleted_by;
	}

	public String getDeleted_by() {
		return deleted_by;
	}

	public void setDbProfileName(String dbProfileName) {
		this.dbProfileName = dbProfileName;
	}

	public String getDbProfileName() {
		return dbProfileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getProfileName() {
		return profileName;
	}
	
	public void setPdbName(String pdbName) {
		this.pdbName = pdbName;
	}

	public String getPdbName() {
		return pdbName;
	}
	
	public void setPdbID(Long pdbID) {
		this.pdbID = pdbID;
	}

	public Long getPdbID() {
		return pdbID;
	}
	
	public void setRootName(String rootName) {
		this.rootName = rootName;
	}

	public String getRootName() {
		return rootName;
	}

}
