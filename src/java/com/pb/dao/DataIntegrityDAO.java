package com.pb.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.pb.common.AeSimpleSHA1;
import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;
import com.pb.common.DateUtil;
import com.pb.common.PropertyUtils;
import com.pb.dto.DataDTO;

public class DataIntegrityDAO {
	DBConnectionDAO db = new DBConnectionDAO();
	Date date = new Date();

	public void saveFileDetails(DataDTO dao) throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		String size= "";
		if(dao.getDataFileSize() != null)
			size = dao.getDataFileSize().toString();
		try {
			if(conn != null)
			{
			String s = DateUtil.getCurrentDate();
			System.out.println(dao.getFilePath()+"saveFileDetails:" + s);
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			ps = conn
					.prepareStatement("INSERT INTO FILE_DATA_INTEGRITY(INTE_CHECK_DATE,HOST_ID,FILE_PATH,FILE_NAME,DATE_CREATION,SHA1,ACTION,FILE_SIZE,UPDATED_DATE,DATE_CHECKED,SHA1_CONFIRM,CHANGED_FILE_NAME,CHECK_VALID,FIlE_EXISTS) VALUES(?, ?, ?, ?, ?, ?, ?,?, ?,?,?,?,?,?)");
			if(encrypt.equalsIgnoreCase("YES")){
				ps.setString(1, s);
				ps.setLong(2, dao.getHostId());
				ps.setString(3, comDao.CheckEncryptData(dao.getFilePath()));
				ps.setString(4, dao.getNewFileName());
				ps.setString(5, comDao.CheckEncryptData(dao.getCreatedDate()));
				ps.setString(6, comDao.CheckEncryptData(dao.getHashId()));
				ps.setString(7, comDao.CheckEncryptData(dao.getFileAction()));
				ps.setString(8, size);
				ps.setString(9, s);
				ps.setString(10, s);
				ps.setString(11,comDao.CheckEncryptData(dao.getHashId()));
				ps.setString(12,dao.getNewFileName());
				ps.setString(13,comDao.CheckEncryptData(dao.getStatus()));
				ps.setString(14,comDao.CheckEncryptData(dao.getFileExists()));
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				ps.setString(1, s);
				ps.setLong(2, dao.getHostId());
				ps.setString(3, dao.getFilePath());
				ps.setString(4, dao.getNewFileName());
				ps.setString(5, dao.getCreatedDate());
				ps.setString(6, dao.getHashId());
				ps.setString(7, dao.getFileAction());
				ps.setString(8, size);
				ps.setString(9, s);
				ps.setString(10, s);
				ps.setString(11,dao.getHashId());
				ps.setString(12,dao.getNewFileName());
				ps.setString(13,dao.getStatus());
				ps.setString(14,dao.getFileExists());
			}
			ps.executeUpdate();
			System.out.println("saveFileDetails--created successfully");
		
			}
		} 
		catch (SQLException e) {
			System.out.println("SQLError is Here");
			e.printStackTrace();
		}catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	public void saveIntegrityCheckDetails(DataDTO dao) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			System.out.println("not completed" + s);
			String valid = "N";
			String query = "INSERT INTO FILE_INTEGRITY_CHECK(SHA1_CONFIRM,DATE_CHECKED,INTE_ID,CHECK_VALID,CHANGED_FILE_NAME) VALUES('"
				+ dao.getHashId()
				+ "','"
				+ s
				+ "',"
				+ dao.getInteId()
				+ ",'" + valid + "','" + dao.getNewFileName() + "');";
			Statement stmt = db.getDBConnection().createStatement();
			stmt.execute(query);
			System.out.println("created successfully");
		} catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}

	}

	public List getSingleFileDetails(String fileName, String path) {
		List list = new ArrayList();
		DataDAO cDao = new DataDAO();
		CommonDAO cmnDao = new CommonDAO();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String action = cmnDao.CheckEncryptData("deleted file");
			String query = "select INTE_ID,FILE_NAME,DATE_CREATION,SHA1 from FILE_DATA_INTEGRITY where FILE_NAME = '"
				+ fileName
				+ "' or CHANGED_FILE_NAME = '"
				+ fileName + "' and ACTION != '"+action+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				cDao.setInteId(rs.getLong(1));
				cDao.setFileName(rs.getString(2));
				cDao.setFileCreatedDate(rs.getString(3));
				cDao.setSha1(rs.getString(4));
				list.add(cDao);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	public List getSingleResourceFileDetails(String fileName, String path) {
		List list = new ArrayList();
		DataDAO cDao = new DataDAO();
		CommonDAO cmnDao = new CommonDAO();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String action = cmnDao.CheckEncryptData("deleted file");
			String query = "select INTE_ID,FILE_NAME,DATE_CREATION,SHA1 from RESOURCE_DATA_INTEGRITY where FILE_NAME = '"
				+ fileName
				+ "' or CHANGED_FILE_NAME = '"
				+ fileName + "' and ACTION !='"+action+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("successfully");
			while (rs.next()) {
				System.out.println("successfully" + rs.getLong(1));
				cDao.setInteId(rs.getLong(1));
				cDao.setFileName(rs.getString(2));
				cDao.setFileCreatedDate(rs.getString(3));
				cDao.setSha1(rs.getString(4));
				list.add(cDao);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List getDocSingleFileDetails(String fileName, String path) {
		List list = new ArrayList();
		DataDAO cDao = new DataDAO();
		try {
			System.out.println("getDocSingleFileDetails::fileName::"+fileName);
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);

			// String query =
			// "select INTE_ID,INTE_CHECK_DATE,HOST_NAME,IP_ADDR,FILE_PATH,FILE_NAME,DATE_CREATION,SHA1 from DATA_INTEGRITY where FILE_NAME='"+fileName+"' or CHGED_FILE_NAME='"+fileName+"'";
			// String query =
			// "select d.INTE_ID,d.FILE_NAME,d.DATE_CREATION,d.SHA1 from [CSR-APP].dbo.DATA_INTEGRITY as d, [CSR-APP].dbo.Integrity_Check as i where d.INTE_ID = i.INTE_ID and (d.FILE_NAME='"+fileName+"' or i.CHANGED_FILE_NAME='"+fileName+"')";
			String query = "select INTE_ID,FILE_NAME,DATE_CREATION,SHA1 from FILE_DOCDATA_INTEGRITY where FILE_NAME = '"
				+ fileName
				+ "' or CHANGED_FILE_NAME = '"
				+ fileName + "'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("successfully");
			while (rs.next()) {
				System.out.println("successfully" + rs.getLong(1));
				cDao.setInteId(rs.getLong(1));

				cDao.setFileName(rs.getString(2));
				cDao.setFileCreatedDate(rs.getString(3));

				cDao.setSha1(rs.getString(4));
				list.add(cDao);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public void saveLogIntegrityCheckDetails(DataDTO dao) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			System.out.println("not completed" + s);
			String valid = "N";
			String query = "INSERT INTO LOG_FILE_INTEGRITY_CHECK(SHA1_CONFIRM,DATE_CHECKED,E2_LOG_FILE_ID,CHECK_VALID,CHANGED_FILE_NAME) VALUES(?,?,?,?,?);";
			ps = conn.prepareStatement(query);
			ps.setString(1, dao.getHashId());
			ps.setString(2, s);
			ps.setLong(3, dao.getE2LogId());
			ps.setString(4, valid);
			ps.setString(5, dao.getNewFileName());
			// Statement stmt = db.getDBConnection().createStatement();
			ps.executeUpdate();
			System.out.println("created successfully");
		} catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}

	}

	public Long saveLogFileDetails(DataDTO dao, InputStream in)
	throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long e2LogFileId = 0L;
		try {
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			System.out.println("not completed" + s);
			ps = conn
			.prepareStatement(
					"INSERT INTO E2_LOG_FILE_MONITOR(CREATED_DATE,HOST_ID,HOST_NAME,IP_ADDR,LOG_FILE_PATH,LOG_FILE_NAME,FILE_CREATED_DATE,HASH_ID,FILE_ACTION,LOG_FILE_CONTENT,NEW_LOG_FILE_NAME,LOG_FILE_SIZE) VALUES(?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			if(encrypt.equalsIgnoreCase("YES")){
				ps.setString(1, s);
				ps.setLong(2, dao.getHostId());
				ps.setString(3, comDao.CheckEncryptData(dao.getHostName()));
				ps.setString(4, comDao.CheckEncryptData(dao.getIpAddr()));
				ps.setString(5, comDao.CheckEncryptData(dao.getFilePath()));
				ps.setString(6, comDao.CheckEncryptData(dao.getFileName()));
				ps.setString(7, comDao.CheckEncryptData(dao.getCreatedDate()));
				ps.setString(8, comDao.CheckEncryptData(dao.getHashId()));
				ps.setString(9, comDao.CheckEncryptData(dao.getFileAction()));
				ps.setBinaryStream(10, in, dao.getFileSize());
				ps.setString(11, comDao.CheckEncryptData(dao.getNewFileName()));
				ps.setLong(12, dao.getFileSize());
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				ps.setString(1, s);
				ps.setLong(2, dao.getHostId());
				ps.setString(3, dao.getHostName());
				ps.setString(4, dao.getIpAddr());
				ps.setString(5, dao.getFilePath());
				ps.setString(6, dao.getFileName());
				ps.setString(7, dao.getCreatedDate());
				ps.setString(8, dao.getHashId());
				ps.setString(9, dao.getFileAction());
				ps.setBinaryStream(10, in, dao.getFileSize());
				ps.setString(11, dao.getNewFileName());
				ps.setLong(12, dao.getFileSize());
			}
			
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next())
				e2LogFileId = rs.getLong(1);
			System.out.println("created successfully");
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return e2LogFileId;
	}

	public void updateLogFileDetails(DataDTO dao, InputStream in, String status) {
		String query = "";
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String valid = "N";
			Connection conn = db.getDBConnection();
			System.out.println("updateLogFileDetails--action::"+status);
			if (status.equalsIgnoreCase("delete")) {
				System.out.println("delete");
				query = "Update E2_LOG_FILE_MONITOR SET FILE_ACTION=?, FILE_MODIFIED_DATE=?, FILE_EXIST=?, LOG_FILE_STATUS=? where LOG_FILE_NAME=?";
				ps = conn.prepareStatement(query);
				ps.setString(1, comDao.CheckEncryptData(dao.getFileAction()));
				ps.setString(2, dao.getCreatedDate());
				ps.setString(3, comDao.CheckEncryptData(dao.getFileExists()));
				ps.setString(4, comDao.CheckEncryptData(dao.getStatus()));
				ps.setString(5, dao.getNewFileName());
				ps.executeUpdate();
				System.out.println("executed:delete query");
			} else if (status.equalsIgnoreCase("update")) {
				System.out.println("update: before");
				query = "Update E2_LOG_FILE_MONITOR SET FILE_ACTION=?,  LOG_FILE_CONTENT=?,  FILE_MODIFIED_DATE=?, FILE_EXIST=?, LOG_FILE_STATUS=? where E2_LOG_FILE_ID=?";
				ps = conn.prepareStatement(query);
				ps.setString(1, comDao.CheckEncryptData(dao.getFileAction()));
				ps.setBinaryStream(2, in, dao.getFileSize());
				ps.setString(3, dao.getCreatedDate());
				ps.setString(4, comDao.CheckEncryptData(dao.getFileExists()));
				ps.setString(5, comDao.CheckEncryptData(dao.getStatus()));
				ps.setLong(6, dao.getE2LogId());
				ps.executeUpdate();
				System.out.println("executed: update query");
			}
			System.out.println("updated successfully");
			ps.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println("Error is Here--SQL");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error is Here-- e");
			e.printStackTrace();
		}
	}

	public List getLogFileDetails(String fileName, String path, Long size)
	throws Exception {
		List list = new ArrayList();
		// boolean list = false;
		CommonDAO cDao = new CommonDAO();
		DataDAO dao = new DataDAO();
		PreparedStatement ps = null;
		System.out.println("getLogFileDetails: " + fileName);
		try {
			Connection conn = db.getDBConnection();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String action = cDao.CheckEncryptData("deleted file");
			String query = "select E2_LOG_FILE_ID,LOG_FILE_NAME,LOG_FILE_SIZE,FILE_MODIFIED_DATE,FILE_ACTION from E2_LOG_FILE_MONITOR where (LOG_FILE_NAME = ? or NEW_LOG_FILE_NAME=?) and FILE_ACTION !='"+action+"'and LOG_FILE_SIZE < ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, cDao.CheckDecryptData(fileName));
			ps.setString(2, cDao.CheckDecryptData(fileName));
			ps.setLong(3, size);
			ResultSet rs = ps.executeQuery();
			System.out.println("executed query");
			while (rs.next()) {
				System.out.println("results:" + rs.getString(2));
				dao.setE2LogId(rs.getLong(1));
				dao.setFileName(cDao.CheckDecryptData(rs.getString(2)));
				dao.setFileSize(Integer.parseInt(rs.getString(3)));
				dao.setFileCreatedDate(cDao.CheckDecryptData(rs.getString(4)));
				dao.setFileAction(cDao.CheckDecryptData(rs.getString(5)));
				list.add(dao);
			}
			ps.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public Long saveLogFileData(DataDTO dao, Long e2logFileId,
			boolean errorOccurs) throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		String s = DateUtil.getCurrentDate();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			System.out.println("saveLogFileData::" + e2logFileId);
			if(encrypt.equalsIgnoreCase("YES")){
				String hash = AeSimpleSHA1.SHA1(comDao.CheckEncryptData(dao
						.getCompressingFile())
						+ ","
						+ comDao.CheckEncryptData(dao.getProfile())
						+ ","
						+ comDao.CheckEncryptData(dao.getFormat())
						+ ","
						+ comDao.CheckEncryptData(dao.getTotalRead())
						+ ","
						+ comDao.CheckEncryptData(dao.getTotalWrite())
						+ ","
						+ comDao.CheckEncryptData(dao.getFinalRatio())
						+ ","
						+ comDao.CheckEncryptData(dao.getMethod())
						+ ","
						+ comDao.CheckEncryptData(dao.getJournalFile())
						+ ","
						+ comDao.CheckEncryptData(dao.getDocuments())
						+ ","
						+ comDao.CheckEncryptData(dao.getDocumentPages())
						+ ","
						+ comDao.CheckEncryptData(dao.getIgnoredPages())
						+ ","
						+ comDao.CheckEncryptData(String.valueOf(errorOccurs))
						+ ","
						+ comDao.CheckEncryptData(dao.getLogDate())
						+ ","
						+ comDao.CheckEncryptData(dao.getLogTime())
						+ ","
						+ comDao.CheckEncryptData(dao.getDrpFile())
						+ ","
						+ comDao.CheckEncryptData(dao.getDrdFile())
						+ ","
						+ comDao.CheckEncryptData(dao.getFileStatus()));
				// String query =
				// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";

				ps = conn
				.prepareStatement(
						"INSERT INTO E2_LOG_MONITOR(COMPRESSING_LIN_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,E2_LOG_FILE_ID,DRP_FILE,DRD_FILE,DRD_FILE_STATUS,COMPRESSION_TYPE,LAST_UPDATED) VALUES(?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?,?,?)",
						PreparedStatement.RETURN_GENERATED_KEYS);

		ps.setString(1, comDao.CheckEncryptData(dao.getCompressingFile()));
		ps.setString(2, comDao.CheckEncryptData(dao.getProfile()));
		ps.setString(3, comDao.CheckEncryptData(dao.getFormat()));
		ps.setString(4, comDao.CheckEncryptData(dao.getTotalRead()));
		ps.setString(5, comDao.CheckEncryptData(dao.getTotalWrite()));
		ps.setString(6, comDao.CheckEncryptData(dao.getFinalRatio()));
		ps.setString(7, comDao.CheckEncryptData(dao.getMethod()));
		ps.setString(8, comDao.CheckEncryptData(dao.getJournalFile()));
		ps.setString(9, comDao.CheckEncryptData(dao.getDocuments()));
		ps.setString(10, comDao.CheckEncryptData(dao.getDocumentPages()));
		ps.setString(11, comDao.CheckEncryptData(dao.getIgnoredPages()));
		ps.setString(12,
				comDao.CheckEncryptData(String.valueOf(errorOccurs)));
		ps.setString(13, hash);
		ps.setString(14, comDao.CheckEncryptData(dao.getLogDate()));
		ps.setString(15, comDao.CheckEncryptData(dao.getLogTime()));
		ps.setLong(16, e2logFileId);
		ps.setString(17, comDao.CheckEncryptData(dao.getDrpFile()));
		ps.setString(18, comDao.CheckEncryptData(dao.getDrdFile()));
		ps.setString(19, comDao.CheckEncryptData(dao.getFileStatus()));
		ps.setString(20, dao.getCompressingType());
		ps.setString(21, s);
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					logId = rs.getLong(1);
				}
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				String hash = AeSimpleSHA1.SHA1(dao.getCompressingFile()
						+ ","
						+ dao.getProfile()
						+ ","
						+ dao.getFormat()
						+ ","
						+ dao.getTotalRead()
						+ ","
						+ dao.getTotalWrite()
						+ ","
						+ dao.getFinalRatio()
						+ ","
						+ dao.getMethod()
						+ ","
						+ dao.getJournalFile()
						+ ","
						+ dao.getDocuments()
						+ ","
						+ dao.getDocumentPages()
						+ ","
						+ dao.getIgnoredPages()
						+ ","
						+ String.valueOf(errorOccurs)
						+ ","
						+ dao.getLogDate()
						+ ","
						+ dao.getLogTime()
						+ ","
						+ dao.getDrpFile()
						+ ","
						+ dao.getDrdFile()
						+ ","
						+ dao.getFileStatus());
				// String query =
				// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";

				ps = conn
				.prepareStatement(
						"INSERT INTO E2_LOG_MONITOR(COMPRESSING_LIN_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,E2_LOG_FILE_ID,DRP_FILE,DRD_FILE,DRD_FILE_STATUS,COMPRESSION_TYPE,LAST_UPDATED) VALUES(?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?,?,?)",
						PreparedStatement.RETURN_GENERATED_KEYS);

		ps.setString(1, dao.getCompressingFile());
		ps.setString(2, dao.getProfile());
		ps.setString(3, dao.getFormat());
		ps.setString(4, dao.getTotalRead());
		ps.setString(5, dao.getTotalWrite());
		ps.setString(6, dao.getFinalRatio());
		ps.setString(7, dao.getMethod());
		ps.setString(8, dao.getJournalFile());
		ps.setString(9, dao.getDocuments());
		ps.setString(10, dao.getDocumentPages());
		ps.setString(11, dao.getIgnoredPages());
		ps.setString(12,
				String.valueOf(errorOccurs));
		ps.setString(13, hash);
		ps.setString(14, dao.getLogDate());
		ps.setString(15, dao.getLogTime());
		ps.setLong(16, e2logFileId);
		ps.setString(17, dao.getDrpFile());
		ps.setString(18, dao.getDrdFile());
		ps.setString(19, dao.getFileStatus());
		ps.setString(20, dao.getCompressingType());
		ps.setString(21, s);
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					logId = rs.getLong(1);
				}
			}
			
			// Statement stmt = db.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("saveLogFileData:created successfully-->"
					+ logId);
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	public Long updateLogFileData(DataDTO dao, Long logFileId,
			String compressionType, boolean errorOccurs) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			
			//String s = DateUtil.getCurrentDate();
			System.out.println("updateLogFileData::" + logFileId);
			System.out.println("updateLogFileData:profile:" + dao.getProfile());
			System.out.println("updateLogFileData:format:" +dao.getFormat());
			System.out.println("updateLogFileData:read:" +dao.getTotalRead());
			String hash = AeSimpleSHA1.SHA1(comDao.CheckEncryptData(dao
					.getCompressingFile())
					+ ","
					+ comDao.CheckEncryptData(dao.getProfile())
					+ ","
					+ comDao.CheckEncryptData(dao.getFormat())
					+ ","
					+ comDao.CheckEncryptData(dao.getTotalRead())
					+ ","
					+ comDao.CheckEncryptData(dao.getTotalWrite())
					+ ","
					+ comDao.CheckEncryptData(dao.getFinalRatio())
					+ ","
					+ comDao.CheckEncryptData(dao.getMethod())
					+ ","
					+ comDao.CheckEncryptData(dao.getJournalFile())
					+ ","
					+ comDao.CheckEncryptData(dao.getDocuments())
					+ ","
					+ comDao.CheckEncryptData(dao.getDocumentPages())
					+ ","
					+ comDao.CheckEncryptData(dao.getIgnoredPages())
					+ ","
					+ comDao.CheckEncryptData(String.valueOf(errorOccurs))
					+ ","
					+ comDao.CheckEncryptData(dao.getLogDate())
					+ ","
					+ comDao.CheckEncryptData(dao.getLogTime())
					+ ","
					+ dao.getDrpFile()
					+ ","
					+ dao.getDrdFile()
					+ ","
					+ comDao.CheckEncryptData(dao.getFileStatus()));
			// String query =
			// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";

			ps = conn
					.prepareStatement("UPDATE E2_LOG_MONITOR SET COMPRESSING_LIN_FILE=?,PROFILE=?,FORMAT=?,TOTTAL_READ=?,TOTTAL_WRITTEN=?,FINAL_RATIO=?,METHOD=?,DOCUMENTS=?,DOCUMENT_PAGES=?,IGNORED_PAGES=?,ERROR_OCCURS=?,HASH_ID=?,DRP_FILE=?,DRD_FILE=?,DRD_FILE_STATUS=?,COMPRESSION_TYPE=? where LOG_FILE_ID=?");

			ps.setString(1, comDao.CheckEncryptData(dao.getCompressingFile()));
			ps.setString(2, comDao.CheckEncryptData(dao.getProfile()));
			ps.setString(3, comDao.CheckEncryptData(dao.getFormat()));
			ps.setString(4, comDao.CheckEncryptData(dao.getTotalRead()));
			ps.setString(5, comDao.CheckEncryptData(dao.getTotalWrite()));
			ps.setString(6, comDao.CheckEncryptData(dao.getFinalRatio()));
			ps.setString(7, comDao.CheckEncryptData(dao.getMethod()));
			// ps.setString(8, comDao.CheckEncryptData(path,dao.getJournalFile()));
			ps.setString(8, comDao.CheckEncryptData(dao.getDocuments()));
			ps.setString(9, comDao.CheckEncryptData(dao.getDocumentPages()));
			ps.setString(10, comDao.CheckEncryptData(dao.getIgnoredPages()));
			ps.setString(11,
					comDao.CheckEncryptData(String.valueOf(errorOccurs)));
			ps.setString(12, hash);
			ps.setString(13, comDao.CheckEncryptData(dao.getDrpFile()));
			ps.setString(14,  comDao.CheckEncryptData(dao.getDrdFile()));
			ps.setString(15, comDao.CheckEncryptData(dao.getFileStatus()));
			ps.setString(16, compressionType);
			ps.setLong(17, logFileId);

			ps.executeUpdate();

			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("updated successfully");
		
		}catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return logId;
	}

	public Long updateLogFileStatus(DataDAO dao, Long logFileId, String status,
			boolean errorOccurs) throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			System.out.println("updateLogFileStatus::" + status);
			String hash = AeSimpleSHA1.SHA1(comDao.CheckEncryptData(dao
					.getCompressingFile())
					+ ","
					+ comDao.CheckEncryptData(dao.getProfile())
					+ ","
					+ comDao.CheckEncryptData(dao.getFormat())
					+ ","
					+ comDao.CheckEncryptData(dao.getTotalRead())
					+ ","
					+ comDao.CheckEncryptData(dao.getTotalWrite())
					+ ","
					+ comDao.CheckEncryptData(dao.getFinalRatio())
					+ ","
					+ comDao.CheckEncryptData(dao.getMethod())
					+ ","
					+ comDao.CheckEncryptData(dao.getJournalFile())
					+ ","
					+ comDao.CheckEncryptData(dao.getDocuments())
					+ ","
					+ comDao.CheckEncryptData(dao.getDocumentPages())
					+ ","
					+ comDao.CheckEncryptData(dao.getIgnoredPages())
					+ ","
					+ comDao.CheckEncryptData(String.valueOf(errorOccurs))
					+ ","
					+ comDao.CheckEncryptData(dao.getLogDate())
					+ ","
					+ comDao.CheckEncryptData(dao.getLogTime())
					+ ","
					+ comDao.CheckEncryptData(dao.getDrpFile())
					+ ","
					+ comDao.CheckEncryptData(dao.getDrdFile())
					+ ","
					+ comDao.CheckEncryptData(dao.getFileStatus()));
			// String query =
			// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";

			ps = conn
			.prepareStatement("UPDATE E2_LOG_MONITOR SET HASH_ID=?,LOG_END_DATE=?,LOG_END_TIME=?,DRD_FILE_STATUS=? where LOG_FILE_ID=?");

			ps.setString(1, hash);
			ps.setString(2, comDao.CheckEncryptData(dao.getLogDate()));
			ps.setString(3, comDao.CheckEncryptData(dao.getLogEndTime()));
			ps.setString(4, comDao.CheckEncryptData(dao.getFileStatus()));

			ps.setLong(5, logFileId);

			ps.executeUpdate();

			// Statement stmt = db.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	public Long getLogFileData(String comressingFile, String desc)
	throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			String errorOccurs = "";
			System.out.println("getLogFileData::" + desc);
			if (desc.equalsIgnoreCase("compressing")) {

				ps = conn
				.prepareStatement("SELECT LOG_FILE_ID from E2_LOG_MONITOR where COMPRESSING_LIN_FILE=? and ERROR_OCCURS=?");
				errorOccurs = "false";
			} else if (desc.equalsIgnoreCase("journal")) {
				ps = conn
				.prepareStatement("SELECT LOG_FILE_ID from E2_LOG_MONITOR where JRNL_FILE_NAME=? and ERROR_OCCURS=?");
				errorOccurs = "true";
			}
			ps.setString(1, comDao.CheckEncryptData(comressingFile));
			ps.setString(2, comDao.CheckEncryptData(errorOccurs));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				logId = rs.getLong(1);
			}

		} catch (Exception e) {

		}
		return logId;
	}

	public Long getLogFileErrorData(String comressingFile, String desc)
	throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			if (desc.equalsIgnoreCase("compressing"))
				ps = conn
				.prepareStatement("SELECT LOG_FILE_ID from E2_LOG_MONITOR where COMPRESSING_LIN_FILE=?");
			else if (desc.equalsIgnoreCase("journal"))
				ps = conn
				.prepareStatement("SELECT LOG_FILE_ID from E2_LOG_MONITOR where JRNL_FILE_NAME=?");
			ps.setString(1, comressingFile);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				logId = rs.getLong(1);
			}

		} catch (Exception e) {

		}
		return logId;
	}

	public Long saveLogErrorData(String errorCode, String errors,
			Long logFileId, int errorCount) throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			System.out.println("saveLogErrorData::" + errorCount);
			String hash = "";
			if(encrypt.equalsIgnoreCase("YES")){
				hash = AeSimpleSHA1.SHA1(comDao.CheckEncryptData(errorCode) + ","
						+ comDao.CheckEncryptData(errors) + "," + logFileId);
				ps = conn
				.prepareStatement(
						"INSERT INTO E2_LOG_ERROR(ERROR_CODE,ERROR_DESC,LOG_FILE_ID,HASH_ID) VALUES(?, ?, ?,?)",
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, comDao.CheckEncryptData(errorCode));
				ps.setString(2, comDao.CheckEncryptData(errors));
				ps.setLong(3, logFileId);
				ps.setString(4, hash);
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				hash = AeSimpleSHA1.SHA1(errorCode + ","
						+ errors + "," + logFileId);
				ps = conn
				.prepareStatement(
						"INSERT INTO E2_LOG_ERROR(ERROR_CODE,ERROR_DESC,LOG_FILE_ID,HASH_ID) VALUES(?, ?, ?,?)",
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, errorCode);
				ps.setString(2, errors);
				ps.setLong(3, logFileId);
				ps.setString(4, hash);
			}
			
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				logId = rs.getLong(1);
			}
			System.out.println("created successfully");

		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	// wrote[j],sizes[j],date,resstartTime,resFinishingTime,logId
	public Long saveLogResources(String[] wrote, String[] size, String date,
			String resStartTime, String resFinishingTime, Long logFileId,
			int count) throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			
			System.out.println("saveLogResources::" + date);
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			if(encrypt.equalsIgnoreCase("YES")){
				for (int i = 0; i < count; i++) {
					
					logId = getLogResources(wrote[i], size[i], date, resStartTime,
							resFinishingTime, logFileId);
					if (logId == 0) {
						ps = conn
						.prepareStatement(
								"INSERT INTO LOG_RESOURCES(RESOURCE_NAME,SIZE,RESOURCES_DATE,RESOURCES_TIME,RESOURCES_END_DATE,RESOURCES_END_TIME,LOG_FILE_ID,HASH_ID) VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
								PreparedStatement.RETURN_GENERATED_KEYS);
						
						String hash = AeSimpleSHA1.SHA1(comDao
								.CheckEncryptData(wrote[i])
								+ ","
								+ comDao.CheckEncryptData(size[i])
								+ ","
								+ comDao.CheckEncryptData(date)
								+ ","
								+ comDao.CheckEncryptData(resStartTime)
								+ ","
								+ comDao.CheckEncryptData(resFinishingTime)
								+ ","
								+ logFileId);
						System.out.println("hash" + hash);
						
						ps.setString(1, comDao.CheckEncryptData(wrote[i]));
						ps.setString(2, comDao.CheckEncryptData(size[i]));
						ps.setString(3, comDao.CheckEncryptData(date));
						ps.setString(4, comDao.CheckEncryptData(resStartTime));
						ps.setString(5, comDao.CheckEncryptData(date));
						ps.setString(6, comDao.CheckEncryptData(resFinishingTime));
						ps.setLong(7, logFileId);
						ps.setString(8, hash);
						ps.executeUpdate();
						ResultSet rs = ps.getGeneratedKeys();
						if (rs.next()) {
							logId = rs.getLong(1);
						}
					}
				}
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				for (int i = 0; i < count; i++) {
					
					logId = getLogResources(wrote[i], size[i], date, resStartTime,
							resFinishingTime, logFileId);
					if (logId == 0) {
						ps = conn
						.prepareStatement(
								"INSERT INTO LOG_RESOURCES(RESOURCE_NAME,SIZE,RESOURCES_DATE,RESOURCES_TIME,RESOURCES_END_DATE,RESOURCES_END_TIME,LOG_FILE_ID,HASH_ID) VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
								PreparedStatement.RETURN_GENERATED_KEYS);
						
						String hash = AeSimpleSHA1.SHA1(wrote[i]
								+ ","
								+ size[i]
								+ ","
								+ date
								+ ","
								+ resStartTime
								+ ","
								+ resFinishingTime
								+ ","
								+ logFileId);
						System.out.println("hash" + hash);
						
						ps.setString(1, wrote[i]);
						ps.setString(2, size[i]);
						ps.setString(3, date);
						ps.setString(4, resStartTime);
						ps.setString(5, date);
						ps.setString(6, resFinishingTime);
						ps.setLong(7, logFileId);
						ps.setString(8, hash);
						ps.executeUpdate();
						ResultSet rs = ps.getGeneratedKeys();
						if (rs.next()) {
							logId = rs.getLong(1);
						}
					}
				}
			}
			
			System.out.println("created successfully");
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	public Long getLogResources(String wrote, String size, String date,
			String resStartTime, String resFinishingTime, Long logFileId)
	throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			// SimpleDateFormat formatter = new
			// SimpleDateFormat(CommonConstants.DATE_FORMAT);
			// String s = formatter.format(date);
			System.out.println("getLogResources" + date);

			// String hash =
			// AeSimpleSHA1.SHA1(comDao.CheckEncryptData(wrote[i])+","+comDao.CheckEncryptData(size[i])+","+comDao.CheckEncryptData(date)+","+comDao.CheckEncryptData(resStartTime)+","+comDao.CheckEncryptData(resFinishingTime)+","+logFileId);
			// System.out.println("hash"+hash);

			// String query1 =
			// "INSERT INTO LOG_RESOURCES(RESOURCE_NAME,SIZE,HASH_ID,RESOURCES_DATE,RESOURCES_TIME,RESOURCES_END_DATE,RESOURCES_END_TIME,LOG_FILE_ID) VALUES('"+checkNull(resourceName)+"','"+checkNull(resourceSize)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startTime)+"','"+checkNull(date)+"','"+checkNull(endTime)+"',"+logId+");";

			ps = conn
			.prepareStatement("SELECT RESOURCE_ID FROM LOG_RESOURCES where RESOURCE_NAME=? and SIZE=? and RESOURCES_DATE=? and RESOURCES_TIME=? and RESOURCES_END_DATE = ? and RESOURCES_END_TIME = ? and LOG_FILE_ID=?");

			ps.setString(1, comDao.CheckEncryptData(wrote));
			ps.setString(2, comDao.CheckEncryptData(size));
			ps.setString(3, comDao.CheckEncryptData(date));
			ps.setString(4, comDao.CheckEncryptData(resStartTime));
			ps.setString(5, comDao.CheckEncryptData(date));
			ps.setString(6, comDao.CheckEncryptData(resFinishingTime));
			ps.setLong(7, logFileId);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				logId = rs.getLong(1);
			}

			// Statement stmt = db.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	public Long getLogIndexFile(DataDAO dao) throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {

			System.out.println("getLogIndexFile::" + dao.getDrdFile());
			ps = conn
			.prepareStatement("SELECT LOG_FILE_ID from E2_LOG_MONITOR where DRD_FILE=?");

			ps.setString(1, comDao.CheckEncryptData(dao.getDrdFile()));

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				logId = rs.getLong(1);
			}

			System.out.println("created successfully");
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	public Long saveLogIndexing(DataDAO dao, Long logFileId)
	throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			System.out.println("saveLogIndexing::" + logFileId);
			String hash = AeSimpleSHA1.SHA1(comDao.CheckEncryptData(dao
					.getIndexingFile())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexName())
					+ ","
					+ comDao.CheckEncryptData(dao.getLogDate())
					+ ","
					+ comDao.CheckEncryptData(dao.getResStartTime())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexFileSize())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexDiskRead())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexDiskWrite())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexStackDepth())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheReadHit())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheReadMiss())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheWriteHit())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheWriteMiss())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexAddDuplicate())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexAddUnique())
					+ "," + logFileId);
			// String query =
			// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";
			System.out.println("hash" + hash);
			// String query1 =
			// "INSERT INTO LOG_INDEX(INDEX_FILE_NAME,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID,INDEX_DATE,INDEX_TIME,INDEX_END_DATE,INDEX_END_TIME,LOG_FILE_ID) VALUES('"+checkNull(indexFile)+"','"+checkNull(indexFileSize)+"','"+checkNull(indexDiskRead)+"','"+checkNull(indexDiskWrite)+"','"+checkNull(indexStackDepth)+"','"+checkNull(indexCacheReadHit)+"','"+checkNull(indexCacheReadMiss)+"','"+checkNull(indexCacheWriteHit)+"','"+checkNull(indexCacheWriteMiss)+"','"+checkNull(indexAddDuplicate)+"','"+checkNull(indexAddUnique)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startTime)+"','"+checkNull(date)+"','"+checkNull(endTime)+"',"+logId+");";
			logId = getLogIndexing(dao, logFileId);
			if (logId == 0) {
				ps = conn
				.prepareStatement(
						"INSERT INTO LOG_INDEX(INDEX_FILE_NAME,INDEX_NAME,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID,INDEX_DATE,INDEX_TIME,INDEX_END_DATE,INDEX_END_TIME,LOG_FILE_ID) VALUES(?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?)",
						PreparedStatement.RETURN_GENERATED_KEYS);
			} else {
				ps = conn
				.prepareStatement("UPDATE LOG_INDEX SET INDEX_FILE_NAME=?,INDEX_NAME=?,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID=?,INDEX_DATE=?,INDEX_TIME=?,INDEX_END_DATE=?,INDEX_END_TIME=? where LOG_FILE_ID=?");

			}
			ps.setString(1, comDao.CheckEncryptData(dao.getIndexingFile()));
			ps.setString(2, comDao.CheckEncryptData(dao.getIndexName()));
			ps.setString(3, comDao.CheckEncryptData(dao.getIndexFileSize()));
			ps.setString(4, comDao.CheckEncryptData(dao.getIndexDiskRead()));
			ps.setString(5, comDao.CheckEncryptData(dao.getIndexDiskWrite()));
			ps.setString(6, comDao.CheckEncryptData(dao.getIndexStackDepth()));
			ps.setString(7, comDao.CheckEncryptData(dao.getIndexCacheReadHit()));
			ps.setString(8,
					comDao.CheckEncryptData(dao.getIndexCacheReadMiss()));
			ps.setString(9,
					comDao.CheckEncryptData(dao.getIndexCacheWriteHit()));
			ps.setString(10,
					comDao.CheckEncryptData(dao.getIndexCacheWriteMiss()));
			ps.setString(11,
					comDao.CheckEncryptData(dao.getIndexAddDuplicate()));
			ps.setString(12, comDao.CheckEncryptData(dao.getIndexAddUnique()));
			ps.setString(13, hash);
			ps.setString(14, comDao.CheckEncryptData(dao.getLogDate()));
			ps.setString(15,
					comDao.CheckEncryptData(dao.getIndexingStartTime()));
			ps.setString(16, comDao.CheckEncryptData(dao.getLogDate()));
			ps.setString(17,
					comDao.CheckEncryptData(dao.getIndexingFinishingTime()));
			ps.setLong(18, logFileId);
			ps.executeUpdate();
			if (logId == 0) {
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					logId = rs.getLong(1);
				}
			}

			// Statement stmt = db.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	public Long updateLogIndexing(DataDAO dao, Long logFileId)
	throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String flag = "F";
			System.out.println("saveLogIndexing::" + logFileId);
			// String query =
			// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";
			// String query1 =
			// "INSERT INTO LOG_INDEX(INDEX_FILE_NAME,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID,INDEX_DATE,INDEX_TIME,INDEX_END_DATE,INDEX_END_TIME,LOG_FILE_ID) VALUES('"+checkNull(indexFile)+"','"+checkNull(indexFileSize)+"','"+checkNull(indexDiskRead)+"','"+checkNull(indexDiskWrite)+"','"+checkNull(indexStackDepth)+"','"+checkNull(indexCacheReadHit)+"','"+checkNull(indexCacheReadMiss)+"','"+checkNull(indexCacheWriteHit)+"','"+checkNull(indexCacheWriteMiss)+"','"+checkNull(indexAddDuplicate)+"','"+checkNull(indexAddUnique)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startTime)+"','"+checkNull(date)+"','"+checkNull(endTime)+"',"+logId+");";
			logId = getLogIndexing(dao, logFileId);
			if (logId != 0) {

				ps = conn
				.prepareStatement("UPDATE LOG_INDEX SET STATUS=? where LOG_FILE_ID=?");

			}
			ps.setString(1, comDao.CheckEncryptData(flag));

			ps.setLong(2, logFileId);
			ps.executeUpdate();
			if (logId == 0) {
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					logId = rs.getLong(1);
				}
			}

			// Statement stmt = db.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	public Long saveLogIndexDetails(DataDAO dao, Long indexId, int count)
	throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String flag = "T";
			System.out.println("saveLogIndexDetails::" + indexId);

			String hash = AeSimpleSHA1.SHA1(comDao.CheckEncryptData(dao
					.getIndexFileSize())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexDiskRead())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexDiskWrite())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexStackDepth())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheReadHit())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheReadMiss())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheWriteHit())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheWriteMiss())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexAddDuplicate())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexAddUnique())
					+ "," + indexId);
			// String query =
			// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";
			System.out.println("hash" + hash);
			// String query1 =
			// "INSERT INTO LOG_INDEX(INDEX_FILE_NAME,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID,INDEX_DATE,INDEX_TIME,INDEX_END_DATE,INDEX_END_TIME,LOG_FILE_ID) VALUES('"+checkNull(indexFile)+"','"+checkNull(indexFileSize)+"','"+checkNull(indexDiskRead)+"','"+checkNull(indexDiskWrite)+"','"+checkNull(indexStackDepth)+"','"+checkNull(indexCacheReadHit)+"','"+checkNull(indexCacheReadMiss)+"','"+checkNull(indexCacheWriteHit)+"','"+checkNull(indexCacheWriteMiss)+"','"+checkNull(indexAddDuplicate)+"','"+checkNull(indexAddUnique)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startTime)+"','"+checkNull(date)+"','"+checkNull(endTime)+"',"+logId+");";

			logId = getLogIndexDetails(dao, indexId);
			if (logId == 0) {
				ps = conn
				.prepareStatement(
						"INSERT INTO LOG_INDEX_DETAILS(INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID,STATUS,INDEX_ID) VALUES(?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?)",
						PreparedStatement.RETURN_GENERATED_KEYS);
			} else {
				ps = conn
				.prepareStatement("UPDATE LOG_INDEX_DETAILS SET INDEX_FILE_SIZE=?,INDEX_DISK_READ=?,INDEX_DISK_WRITE=?,STATCK_DEPTH=?,CACHE_READ_HIT=?,CACHE_READ_MISS=?,CAHE_WRITE_HIT=?,CAHE_WRITE_MISS=?,ADD_DUPLICATE=?,ADD_UNIQUE=?,HASH_ID=?,STATUS=? where INDEX_ID=?");

			}

			ps.setString(1, comDao.CheckEncryptData(dao.getIndexFileSize()));
			ps.setString(2, comDao.CheckEncryptData(dao.getIndexDiskRead()));
			ps.setString(3, comDao.CheckEncryptData(dao.getIndexDiskWrite()));
			ps.setString(4, comDao.CheckEncryptData(dao.getIndexStackDepth()));
			ps.setString(5, comDao.CheckEncryptData(dao.getIndexCacheReadHit()));
			ps.setString(6,
					comDao.CheckEncryptData(dao.getIndexCacheReadMiss()));
			ps.setString(7,
					comDao.CheckEncryptData(dao.getIndexCacheWriteHit()));
			ps.setString(8,
					comDao.CheckEncryptData(dao.getIndexCacheWriteMiss()));
			ps.setString(9, comDao.CheckEncryptData(dao.getIndexAddDuplicate()));
			ps.setString(10, comDao.CheckEncryptData(dao.getIndexAddUnique()));
			ps.setString(11, comDao.CheckEncryptData(flag));
			ps.setString(12, hash);

			ps.setLong(13, indexId);
			ps.executeUpdate();
			if (logId == 0) {
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					logId = rs.getLong(1);
				}
			}

			// Statement stmt = db.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	public Long getLogIndexing(DataDAO dao, Long logFileId) throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			System.out.println("getLogIndexing>>" + logFileId);
			String flag = "T";
			ps = conn
			.prepareStatement("SELECT INDEX_ID FROM LOG_INDEX where (INDEX_FILE_NAME=? or INDEX_NAME=? or INDEX_DATE=? or INDEX_TIME=? or INDEX_END_DATE=? or INDEX_END_TIME=?) and STATUS=? and INDEX_ID=?");

			ps.setString(1, comDao.CheckEncryptData(dao.getIndexingFile()));
			ps.setString(2, comDao.CheckEncryptData(dao.getIndexName()));
			ps.setString(3, comDao.CheckEncryptData(dao.getLogDate()));
			ps.setString(4, comDao.CheckEncryptData(dao.getIndexingStartTime()));
			ps.setString(5, comDao.CheckEncryptData(dao.getLogDate()));
			ps.setString(6,
					comDao.CheckEncryptData(dao.getIndexingFinishingTime()));
			ps.setString(7, comDao.CheckEncryptData(flag));
			ps.setLong(8, logFileId);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				logId = rs.getLong(1);
			}
			// Statement stmt = db.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	public Long getLogIndexDetails(DataDAO dao, Long indexId)
	throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			System.out.println("getLogIndexing>>" + indexId);

			ps = conn
			.prepareStatement("SELECT INDEX_DET_ID FROM LOG_INDEX_DETAILS where (INDEX_FILE_SIZE=? or INDEX_DISK_READ=? or INDEX_DISK_WRITE=? or STATCK_DEPTH=? or CACHE_READ_HIT=? or CACHE_READ_MISS=? or CAHE_WRITE_HIT=? or CAHE_WRITE_MISS=? or ADD_DUPLICATE=? or ADD_UNIQUE=?) and INDEX_ID=?");

			ps.setString(1, comDao.CheckEncryptData(dao.getIndexFileSize()));
			ps.setString(2, comDao.CheckEncryptData(dao.getIndexDiskRead()));
			ps.setString(3, comDao.CheckEncryptData(dao.getIndexDiskWrite()));
			ps.setString(4, comDao.CheckEncryptData(dao.getIndexStackDepth()));
			ps.setString(5, comDao.CheckEncryptData(dao.getIndexCacheReadHit()));
			ps.setString(6,
					comDao.CheckEncryptData(dao.getIndexCacheReadMiss()));
			ps.setString(7,
					comDao.CheckEncryptData(dao.getIndexCacheWriteHit()));
			ps.setString(8,
					comDao.CheckEncryptData(dao.getIndexCacheWriteMiss()));
			ps.setString(9, comDao.CheckEncryptData(dao.getIndexAddDuplicate()));
			ps.setString(10, comDao.CheckEncryptData(dao.getIndexAddUnique()));

			ps.setLong(11, indexId);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				logId = rs.getLong(1);
			}

			// Statement stmt = db.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return logId;
	}

	public List getLogProfiles() {
		List list = new ArrayList();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		try {
			String sql = "SELECT LOG_FILE_ID,PROFILE FROM E2_LOG_MONITOR";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDAO dao = new DataDAO();
				dao.setLogFileId(rs.getLong(1));
				dao.setProfile(comDao.CheckDecryptData(rs.getString(2)));
				list.add(dao);

			}
			ps.close();
			conn.close();
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public List getLogResources(Long logFileId) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		List res = new ArrayList();
		try {
			String sql = "SELECT RESOURCE_ID, RESOURCE_NAME FROM LOG_RESOURCES WHERE LOG_FILE_ID=?";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, logFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDAO dao = new DataDAO();
				dao.setResourceId(rs.getLong(1));
				dao.setResWrote(comDao.CheckDecryptData(rs.getString(1)));
				res.add(dao);

			}
			ps.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	public List saveLogResourceStatus(Long logFileId,Long resourceId, String resourceName, String status, String newFileName) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		List res = new ArrayList();
		try {
			if(newFileName == null)
			{
				String sql = "UPDATE LOG_RESOURCES SET RES_STATUS=?,NEW_RESOURCE_NAME=? WHERE RESOURCE_NAME=? and LOG_FILE_ID=? and RESOURCE_ID=?";
				ps = conn.prepareStatement(sql);

				ps.setString(1, comDao.CheckEncryptData(status));
				ps.setString(2, comDao.CheckEncryptData(newFileName));
				ps.setString(3, comDao.CheckEncryptData(resourceName));
				ps.setLong(4, logFileId);
				ps.setLong(5, resourceId);
				ps.executeUpdate();


			}
			else
			{
				String sql = "UPDATE LOG_RESOURCES SET RES_STATUS=? WHERE RESOURCE_NAME=? and LOG_FILE_ID=? and RESOURCE_ID=?";
				ps = conn.prepareStatement(sql);

				ps.setString(1, comDao.CheckEncryptData(status));
				ps.setString(2, comDao.CheckEncryptData(resourceName));
				ps.setLong(3, logFileId);
				ps.setLong(4, resourceId);
				ps.executeUpdate();
			}
			ps.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public Long CheckLogFileDetails(String fileName, String path, Long size, String desc)
	{
		Long list = 0l;
		//boolean list = false;
		CommonDAO cDao = new CommonDAO();

		PreparedStatement ps = null;
		Connection conn = null;
		try {
			conn = db.getDBConnection();
			System.out.println(size+"CheckLogFileDetails: " + fileName);
			String query = "";
			if(desc.equalsIgnoreCase("logcheck"))
				query = "select E2_LOG_FILE_ID,LOG_FILE_NAME,LOG_FILE_SIZE,FILE_MODIFIED_DATE from E2_LOG_FILE_MONITOR where LOG_FILE_NAME = ? or NEW_LOG_FILE_NAME=? and LOG_FILE_SIZE != ?";
			else
				query = "select E2_LOG_FILE_ID,LOG_FILE_NAME,LOG_FILE_SIZE,FILE_MODIFIED_DATE from E2_LOG_FILE_MONITOR where LOG_FILE_NAME = ? or NEW_LOG_FILE_NAME=?";

			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			fileName = cDao.CheckEncryptData(fileName);
			ps = conn.prepareStatement(query);
			ps.setString(1,fileName);
			ps.setString(2, fileName);
			if(desc.equalsIgnoreCase("logcheck"))
				ps.setLong(3, size);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				System.out.println("results:" + rs.getLong(1));

				list = rs.getLong(1);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	
	public Long getLogHost(DataDTO dao) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		if(dao.getHostName()==null)
			dao.setHostName("");
		if(dao.getIpAddr()==null)
			dao.setIpAddr("");
		Long id = 0l;
		try {
			System.out.println(dao.getIpAddr()+":getLogHost:"+comDao.CheckEncryptData(dao.getHostName()));
			ps = conn
			.prepareStatement("select HOST_ID from LOG_HOST where (HOST_NAME=? and INSTANCE_NAME=?)");
			ps.setString(1, comDao.CheckEncryptData(dao.getHostName()));
			ps.setString(2, comDao.CheckEncryptData(dao.getServerInstanceName()));
			//ps.executeUpdate();
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				id = rs.getLong(1);
			}
			System.out.println("getLogHost:id:"+id);

		} 
		catch (SQLException e) {
			System.out.println("Error is Here"+e);
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error is Here"+e);
			e.printStackTrace();
		} finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		System.out.println("getLogHost:id:"+id);
		return id;
	}
	public Long saveLogHost(DataDTO dao) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		Long id = 0l;
		try {
			ps = conn
			.prepareStatement("INSERT INTO LOG_HOST(HOST_NAME,HOST_IP,SOFTWARE_PATH,INSTANCE_NAME) VALUES(?, ?, ?,?)",PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, comDao.CheckEncryptData(dao.getHostName()));
			ps.setString(2, comDao.CheckEncryptData(dao.getIpAddr()));
			ps.setString(3, comDao.CheckEncryptData(dao.getSoftwarePath()));
			ps.setString(4, comDao.CheckEncryptData(dao.getServerInstanceName()));

			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next())
				id = rs.getLong(1);


		} 
		catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return id;
	}
	

	private boolean getPageFileDetailsFromLogFiles(Long dbId,Long hostId,String fileName)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = db.getDBConnection();
		CommonDAO comDao = new CommonDAO();

		String profileName=this.getDBName(dbId, hostId);
		System.out.println("getPageFileDetailsFromLogFiles --Profile Name is===="+profileName);
		try {
			String encryptedProfile=comDao.CheckEncryptData( profileName);
			String query = "select DRP_FILE from E2_LOG_MONITOR lm WHERE lm.PROFILE='"+encryptedProfile+"'";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("successfully");
			while (rs.next()) {
				String drdFile=rs.getString(1);
				if(drdFile!=null)
					drdFile=comDao.CheckDecryptData( drdFile);

				System.out.println("Drd File"+drdFile);
				int i=drdFile.lastIndexOf("\\");

				String sub=drdFile.substring(i+1,drdFile.length());
				System.out.println("Sub  is"+sub);
				System.out.println("file Name is"+fileName);
				if(sub.equalsIgnoreCase(fileName))
					return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{

				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return false;
	}
	private String getDBName(Long dbId,Long hostId)
	{

		Connection conn = db.getDBConnection();
		//CommonDAO comDao = new CommonDAO(path);
		String dbName="";
		try {

			String query = "select DB_NAME from CSR_DATABASE WHERE DB_ID="+dbId+" and host_id="+hostId;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			System.out.println("successfully");
			while (rs.next()) {
				dbName=rs.getString(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{

				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return dbName;
	}

	public List<DataDTO> getFileDetails()  {
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = db.getDBConnection();
		CommonDAO comDao = new CommonDAO();
		try {

			String query = "select INTE_ID,INTE_CHECK_DATE,host_id,FILE_PATH,FILE_NAME,DATE_CREATION,SHA1,ACTION,CHECK_VALID,FILE_EXISTS from FILE_DATA_INTEGRITY";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("successfully");
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("successfully" + rs.getLong(1));
				cDao.setInteId(rs.getLong(1));
				cDao.setIntCheckDate(rs.getString(2));
				cDao.setHostId(rs.getLong(3));

				cDao.setFilePath(comDao.CheckDecryptData(rs.getString(4)));
				cDao.setFileName(rs.getString(5));
				cDao.setFileCreatedDate(comDao.CheckDecryptData(rs.getString(6)));

				cDao.setSha1(rs.getString(7));
				cDao.setAction(comDao.CheckDecryptData(rs.getString(8)));
				cDao.setCheckValid(comDao.CheckDecryptData(rs.getString(9)));
				cDao.setFileExists(comDao.CheckDecryptData(rs.getString(10)));
				list.add(cDao);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{

				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	

	public List<DataDTO> getDOCFileDetails()  {
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = db.getDBConnection();
		CommonDAO comDao = new CommonDAO();
		try {

			String query = "select INTE_ID,INTE_CHECK_DATE,HOST_ID,FILE_PATH,FILE_NAME,DATE_CREATION,SHA1,ACTION,CHECK_VALID,FILE_EXISTS from FILE_DOCDATA_INTEGRITY";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("getDOCFileDetails");
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getDOCFileDetails:" + rs.getLong(1));
				cDao.setInteId(rs.getLong(1));
				cDao.setIntCheckDate(rs.getString(2));
				cDao.setHostId(rs.getLong(3));

				cDao.setFilePath(comDao.CheckDecryptData(rs.getString(4)));
				cDao.setFileName(rs.getString(5));
				cDao.setFileCreatedDate(comDao.CheckDecryptData(rs.getString(6)));

				cDao.setSha1(rs.getString(7));
				cDao.setAction(comDao.CheckDecryptData(rs.getString(8)));
				cDao.setCheckValid(comDao.CheckDecryptData(rs.getString(9)));
				cDao.setFileExists(comDao.CheckDecryptData(rs.getString(10)));
				list.add(cDao);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{

				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	private boolean getDocFileDetailsFromLogFiles(String profile,Long hostId,String fileName)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = db.getDBConnection();
		CommonDAO comDao = new CommonDAO();

		String profileName=profile.toUpperCase();
		System.out.println("getDocFileDetailsFromLogFiles--Profile Name is======="+profileName);
		try {
			String encryptedProfile=comDao.CheckEncryptData(profileName);
			String query = "select DRD_FILE from E2_LOG_MONITOR lm WHERE lm.PROFILE='"+encryptedProfile+"'";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("successfully");
			while (rs.next()) {
				String drdFile=rs.getString(1);
				if(drdFile!=null)
					drdFile=comDao.CheckDecryptData( drdFile);

				System.out.println("Drd File"+drdFile);
				int i=drdFile.lastIndexOf("\\");

				String sub=drdFile.substring(i+1,drdFile.length());
				System.out.println("Sub  is"+sub);
				System.out.println("file Name is"+fileName);
				if(sub.equalsIgnoreCase(fileName))
					return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{

				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return false;
	}

	public List<DataDTO> getSingleFileDetails(String fileName,String newFileName, String action) {
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = db.getDBConnection();
		try {
			if(conn != null)
			{
				System.out.println(action+"action:getSingleFileDetails::"+fileName);

				String query = "select INTE_ID,FILE_NAME,DATE_CREATION,SHA1 from FILE_DATA_INTEGRITY where File_Name = '"+ fileName+"' or CHANGED_FILE_NAME='"+newFileName+"'";

				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
					DataDTO cDao = new DataDTO();
					System.out.println("getSingleFileDetails:" + rs.getLong(1));
					cDao.setInteId(rs.getLong(1));

					cDao.setFileName(rs.getString(2));
					cDao.setFileCreatedDate(rs.getString(3));

					cDao.setSha1(rs.getString(4));
					list.add(cDao);
				}

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{

				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	public List<DataDTO> getSingleResourceFileDetails(String fileName,String newFileName, String action) {
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = db.getDBConnection();
		CommonDAO comDao = new CommonDAO();
		try {
			if(conn != null)
			{
				System.out.println(action+"action:getSingleFileDetails::"+fileName);

				String query = "select INTE_ID,FILE_NAME,DATE_CREATION,SHA1 from RESOURCE_DATA_INTEGRITY where File_Name = '"+ comDao.CheckEncryptData( fileName)+"' or CHANGED_FILE_NAME='"+newFileName+"'";

				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
					DataDTO cDao = new DataDTO();
					System.out.println("getSingleFileDetails:" + rs.getLong(1));
					cDao.setInteId(rs.getLong(1));

					cDao.setFileName(comDao.CheckDecryptData( rs.getString(2)));
					cDao.setFileCreatedDate(rs.getString(3));

					cDao.setSha1(rs.getString(4));
					list.add(cDao);
				}

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{

				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	public List<DataDTO> getSingleIndexFileDetails(String fileName,String filePath, String action) {
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = db.getDBConnection();
		CommonDAO comDao = new CommonDAO();
		try {
			if(conn != null)
			{
				System.out.println("getSingleIndexFileDetails--fname::"+fileName);
				String query = "select INTE_ID,FILE_NAME, FILE_PATH, DATE_CREATION,SHA1 from INDEX_DATA_INTEGRITY where File_Name = '"+ comDao.CheckEncryptData( fileName)+"' and FILE_PATH='"+filePath+"'";
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
					DataDTO cDao = new DataDTO();
					cDao.setInteId(rs.getLong(1));
					cDao.setFileName(comDao.CheckDecryptData( rs.getString(2)));
					cDao.setFilePath(comDao.CheckDecryptData( rs.getString(3)));
					cDao.setFileCreatedDate(rs.getString(4));

					cDao.setSha1(rs.getString(5));
					list.add(cDao);
				}

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{

				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	
	public void saveResourceFileDetails(DataDTO dao) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		String size= "";
		if(dao.getDataFileSize() != null)
			size = dao.getDataFileSize().toString();
		try {
			if(conn != null)
			{
				String s = DateUtil.getCurrentDate();

				System.out.println(dao.getFilePath()+"saveResourceFileDetails:" + s);
				String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
				ps = conn
				.prepareStatement("INSERT INTO RESOURCE_DATA_INTEGRITY(INTE_CHECK_DATE,HOST_ID,FILE_PATH,FILE_NAME,DATE_CREATION,SHA1,ACTION,FILE_SIZE,UPDATED_DATE,DATE_CHECKED,SHA1_CONFIRM,CHANGED_FILE_NAME,CHECK_VALID,FIlE_EXISTS) VALUES(?, ?, ?, ?, ?, ?, ?,?, ?,?,?,?,?,?)");
				if(encrypt.equalsIgnoreCase("YES")){
					ps.setString(1, s);
					ps.setLong(2, dao.getHostId());
					ps.setString(3, comDao.CheckEncryptData(dao.getFilePath()));
					ps.setString(4,  comDao.CheckEncryptData(dao.getNewFileName()));
					ps.setString(5, comDao.CheckEncryptData(dao.getCreatedDate()));
					ps.setString(6, comDao.CheckEncryptData(dao.getHashId()));
					ps.setString(7, comDao.CheckEncryptData(dao.getFileAction()));
					ps.setString(8, size);
					ps.setString(9, s);
					ps.setString(10, s);
					ps.setString(11,comDao.CheckEncryptData(dao.getHashId()));
					ps.setString(12,dao.getNewFileName());
					ps.setString(13,comDao.CheckEncryptData(dao.getStatus()));
					ps.setString(14,comDao.CheckEncryptData(dao.getFileExists()));
				}
				else if(encrypt.equalsIgnoreCase("NO")){
					ps.setString(1, s);
					ps.setLong(2, dao.getHostId());
					ps.setString(3, dao.getFilePath());
					ps.setString(4,  dao.getNewFileName());
					ps.setString(5, dao.getCreatedDate());
					ps.setString(6, dao.getHashId());
					ps.setString(7, dao.getFileAction());
					ps.setString(8, size);
					ps.setString(9, s);
					ps.setString(10, s);
					ps.setString(11,dao.getHashId());
					ps.setString(12,dao.getNewFileName());
					ps.setString(13,dao.getStatus());
					ps.setString(14,dao.getFileExists());
				}
				
				ps.executeUpdate();
				
				System.out.println("saveResourceFileDetails--created successfully");

			}
		} 
		catch (SQLException e) {
			System.out.println("SQLError is Here");
			e.printStackTrace();
		}catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	public void saveIndexFileDetails(DataDTO dao) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		String size= "";
		if(dao.getDataFileSize() != null)
			size = dao.getDataFileSize().toString();
		try {
			if(conn != null)
			{
				String s = DateUtil.getCurrentDate();

				System.out.println(dao.getFilePath()+"saveIndexFileDetails:" + s);
				ps = conn
				.prepareStatement("INSERT INTO RESOURCE_DATA_INTEGRITY(INTE_CHECK_DATE,HOST_ID,FILE_PATH,FILE_NAME,DATE_CREATION,SHA1,ACTION,FILE_SIZE,UPDATED_DATE,DATE_CHECKED,SHA1_CONFIRM,CHANGED_FILE_NAME,CHECK_VALID,FIlE_EXISTS) VALUES(?, ?, ?, ?, ?, ?, ?,?, ?,?,?,?,?,?)");
				ps.setString(1, s);
				ps.setLong(2, dao.getHostId());
				ps.setString(3, comDao.CheckEncryptData(dao.getFilePath()));
				ps.setString(4,  comDao.CheckEncryptData(dao.getNewFileName()));
				ps.setString(5, comDao.CheckEncryptData(dao.getCreatedDate()));
				ps.setString(6, comDao.CheckEncryptData(dao.getHashId()));
				ps.setString(7, comDao.CheckEncryptData(dao.getFileAction()));
				ps.setString(8, size);
				ps.setString(9, s);
				ps.setString(10, s);
				ps.setString(11,comDao.CheckEncryptData(dao.getHashId()));
				ps.setString(12,dao.getNewFileName());
				ps.setString(13,comDao.CheckEncryptData(dao.getStatus()));
				ps.setString(14,comDao.CheckEncryptData(dao.getFileExists()));
				ps.executeUpdate();
				
				System.out.println("saveResourceFileDetails--created successfully");

			}
		} 
		catch (SQLException e) {
			System.out.println("SQLError is Here");
			e.printStackTrace();
		}catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	public void updateResourceFileDetails(DataDTO dao) {
		String query = "";
		CommonDAO cdao = new CommonDAO();
		PreparedStatement ps = null;
		Connection conn = db.getDBConnection();
		try {

			System.out.println("--updateFileDetails--:");
			String s  = DateUtil.getCurrentDate();
			String hash = "";
			hash = AeSimpleSHA1.SHA1(dao.getFileName()+","+dao.getNewFileName());
			
			query = "Update RESOURCE_DATA_INTEGRITY SET ACTION='" + cdao.CheckEncryptData(dao.getFileAction())
					+ "', UPDATED_DATE='"+dao.getUpdated_dt()+"',FILE_SIZE='"+dao.getDataFileSize()+"',SHA1_CONFIRM='"+hash+"',DATE_CHECKED='"+s+"',INTE_CHECK_DATE='"+s+"',CHECK_VALID='"+cdao.CheckEncryptData(dao.getStatus())+"',CHANGED_FILE_NAME='"+dao.getNewFileName()+"',FILE_EXISTS='"+cdao.CheckEncryptData(dao.getFileExists())+"' where INTE_ID=" + dao.getInteId();
			System.out.println("updateFileDetails: query:"+query);
			ps = conn.prepareStatement(query);
			ps.executeUpdate();
			System.out.println("updated successfully");

		} catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
	public void updateFileDetails(DataDTO dao) {
		String query = "";
		CommonDAO cdao = new CommonDAO();
		PreparedStatement ps = null;
		Connection conn = db.getDBConnection();
		try {

			System.out.println("--updateFileDetails--:");
			String s  = DateUtil.getCurrentDate();
			String hash = "";
			hash = AeSimpleSHA1.SHA1(dao.getFileName()+","+dao.getNewFileName());
			
			query = "Update  FILE_DATA_INTEGRITY SET ACTION='" + cdao.CheckEncryptData(dao.getFileAction())
					+ "', UPDATED_DATE='"+dao.getUpdated_dt()+"',FILE_SIZE='"+dao.getDataFileSize()+"',SHA1_CONFIRM='"+hash+"',DATE_CHECKED='"+s+"',INTE_CHECK_DATE='"+s+"',CHECK_VALID='"+cdao.CheckEncryptData(dao.getStatus())+"',CHANGED_FILE_NAME='"+dao.getNewFileName()+"',FILE_EXISTS='"+cdao.CheckEncryptData(dao.getFileExists())+"' where INTE_ID=" + dao.getInteId();
			System.out.println("updateFileDetails: query:"+query);
			ps = conn.prepareStatement(query);
			ps.executeUpdate();
			System.out.println("updated successfully");

		} catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	public void saveDocFileDetails(DataDTO dao) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		String size="";
		if(dao.getDataFileSize() != null)
			size = dao.getDataFileSize().toString();
		try {

			String s = DateUtil.getCurrentDate();
			System.out.println("saveDocFileDetails");
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			ps = conn
			.prepareStatement("INSERT INTO FILE_DOCDATA_INTEGRITY(INTE_CHECK_DATE,HOST_ID,FILE_PATH,FILE_NAME,DATE_CREATION,SHA1,ACTION,FILE_SIZE,UPDATED_DATE,DATE_CHECKED,SHA1_CONFIRM,CHANGED_FILE_NAME,CHECK_VALID,FILE_EXISTS) VALUES(?, ?, ?, ?, ?, ?, ?,?, ?,?,?,?,?,?)");
			if(encrypt.equalsIgnoreCase("YES")){
				ps.setString(1, s);
				ps.setLong(2, dao.getHostId());
				ps.setString(3, comDao.CheckEncryptData(dao.getFilePath()));
				ps.setString(4, dao.getNewFileName());
				ps.setString(5, comDao.CheckEncryptData(dao.getCreatedDate()));
				ps.setString(6, comDao.CheckEncryptData(dao.getHashId()));
				ps.setString(7, comDao.CheckEncryptData(dao.getFileAction()));
				ps.setString(8, size);
				ps.setString(9, s);
				ps.setString(10, s);
				ps.setString(11,comDao.CheckEncryptData(dao.getHashId()));
				ps.setString(12,dao.getNewFileName());
				ps.setString(13,comDao.CheckEncryptData(dao.getStatus()));
				ps.setString(14,comDao.CheckEncryptData(dao.getFileExists()));
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				ps.setString(1, s);
				ps.setLong(2, dao.getHostId());
				ps.setString(3, comDao.CheckEncryptData(dao.getFilePath()));
				ps.setString(4, dao.getNewFileName());
				ps.setString(5, comDao.CheckEncryptData(dao.getCreatedDate()));
				ps.setString(6, comDao.CheckEncryptData(dao.getHashId()));
				ps.setString(7, comDao.CheckEncryptData(dao.getFileAction()));
				ps.setString(8, size);
				ps.setString(9, s);
				ps.setString(10, s);
				ps.setString(11,comDao.CheckEncryptData(dao.getHashId()));
				ps.setString(12,dao.getNewFileName());
				ps.setString(13,comDao.CheckEncryptData(dao.getStatus()));
				ps.setString(14,comDao.CheckEncryptData(dao.getFileExists()));
			}
			
			ps.executeUpdate();
			System.out.println("created successfully");

		} 
		catch (SQLException e) {
			System.out.println("SQLError is Here"+e);
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	public void updateDocFileDetails(DataDTO dao) {
		String query = "";
		CommonDAO cdao = new CommonDAO();
		PreparedStatement ps = null;
		Connection conn = db.getDBConnection();
		try {

			System.out.println("--updateDocFileDetails--:");
			String s  = DateUtil.getCurrentDate();
			String hash = "";
			hash = AeSimpleSHA1.SHA1(dao.getFileName()+","+dao.getNewFileName());
			
			query = "Update  FILE_DOCDATA_INTEGRITY SET ACTION='" + cdao.CheckEncryptData(dao.getFileAction())
					+ "', UPDATED_DATE='"+dao.getUpdated_dt()+"',FILE_SIZE='"+dao.getDataFileSize()+"',SHA1_CONFIRM='"+hash+"',DATE_CHECKED='"+s+"',INTE_CHECK_DATE='"+s+"',CHECK_VALID='"+cdao.CheckEncryptData(dao.getStatus())+"',CHANGED_FILE_NAME='"+dao.getNewFileName()+"',FILE_EXISTS='"+cdao.CheckEncryptData(dao.getFileExists())+"' where INTE_ID=" + dao.getInteId();
			System.out.println("updateFileDetails: query:"+query);
			ps = conn.prepareStatement(query);
			ps.executeUpdate();
			System.out.println("updated successfully");

		} catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
	public Long getLogFileData(String comressingFile,Long e2logFileId, String desc,String[] errorCodes)
	{
		CommonDAO comDao = new CommonDAO();
		PreparedStatement ps = null;
		Connection conn = db.getDBConnection();
		Long logId = 0L;
		try {
			String errorOccurs = "";
			System.out.println("getLogFileData::" + desc);
			String query = "";
			if(errorCodes != null && errorCodes.length != 0)
			{
				for(int i=1;i<=errorCodes.length;i++)
				{
					System.out.println(i+"getLogFileData::" + errorCodes[i]);


					query = "SELECT e.LOG_FILE_ID,r.ERROR_CODE from E2_LOG_MONITOR e join E2_LOG_ERROR r on e.LOG_FILE_ID= r.LOG_FILE_ID  where e.COMPRESSING_LIN_FILE=? and e.ERROR_OCCURS=? and r.ERROR_CODE=? and E2_LOG_FILE_ID=? and COMPRESSION_TYPE=?";
					ps = conn
					.prepareStatement(query);
					errorOccurs = "true";

					ps.setString(1, comDao.CheckEncryptData(comressingFile));
					ps.setString(2, comDao.CheckEncryptData(errorOccurs));
					ps.setString(3, comDao.CheckEncryptData(errorCodes[i]));
					ps.setLong(4, e2logFileId);
					ps.setString(5,desc);
					ResultSet rs = ps.executeQuery();
					while (rs.next()) {
						logId = rs.getLong(1);
					}
				}
			}
			else
			{
				System.out.println("getLogFileData--Coming Here::"+e2logFileId);
				query = "SELECT e.LOG_FILE_ID from E2_LOG_MONITOR e  where e.COMPRESSING_LIN_FILE=? and e.ERROR_OCCURS=? and e.E2_LOG_FILE_ID=? and e.COMPRESSION_TYPE=?";
				ps = conn
				.prepareStatement(query);
				errorOccurs = "false";
				ps.setString(1, comDao.CheckEncryptData(comressingFile));
				ps.setString(2, comDao.CheckEncryptData(errorOccurs));

				ps.setLong(3, e2logFileId);
				ps.setString(4,desc);
				System.out.println("getLogFileData--Coming Here:query:-->"+query);
				System.out.println(comDao.CheckEncryptData(comressingFile)+"getLogFileData--Coming Here:query:");
				System.out.println(comDao.CheckEncryptData(errorOccurs)+"getLogFileData--Coming Here:query:^^^"+desc);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					logId = rs.getLong(1);
				}
			}

		} catch (SQLException e) {

		}catch (Exception e) {

		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return logId;
	}
	
	public void saveDocIntegrityCheckDetails(DataDTO dao) {
		PreparedStatement ps = null;
		//CommonDAO comDao = new CommonDAO(realpath);
		Connection conn = db.getDBConnection();
		try {
			
			String s = DateUtil.getCurrentDate();
			System.out.println("not completed" + s);
			String valid = "N";
			String query = "INSERT INTO FILE_DOC_INTEGRITY_CHECK(SHA1_CONFIRM,DATE_CHECKED,INTE_ID,CHECK_VALID,CHANGED_FILE_NAME) VALUES('"
					+ dao.getHashId()
					+ "','"
					+ s
					+ "',"
					+ dao.getInteId()
					+ ",'" + valid + "','" + dao.getNewFileName() + "');";
			ps = conn.prepareStatement(query);
			ps.executeUpdate();
			System.out.println("created successfully");
			
		} 
		catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

	}

	
	
	private String getDBName(Long dbId,Long hostId,String path)
	{
		
		Connection conn = this.db.getDBConnection();
		//CommonDAO comDao = new CommonDAO(path);
		String dbName="";
		try {
			
			String query = "select DB_NAME from CSR_DATABASE WHERE DB_ID="+dbId+" and host_id="+hostId;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			System.out.println("successfully");
			while (rs.next()) {
				dbName=rs.getString(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return dbName;
	}
	
	private boolean getPageFileDetailsFromLogFiles(String profile,Long hostId,String fileName)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.db.getDBConnection();
		CommonDAO comDao = new CommonDAO();
		
		String profileName=profile.toUpperCase();
		System.out.println("getPageFileDetailsFromLogFiles -- Profile Name is==="+profileName);
		try {
			String encryptedProfile=comDao.CheckEncryptData( profileName);
			String query = "select DRP_FILE from E2_LOG_MONITOR lm WHERE lm.PROFILE='"+encryptedProfile+"'";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("successfully");
			while (rs.next()) {
				String drdFile=rs.getString(1);
				if(drdFile!=null)
					drdFile=comDao.CheckDecryptData( drdFile);
				
				System.out.println("Drd File"+drdFile);
				int i=drdFile.lastIndexOf("\\");
				
				String sub=drdFile.substring(i+1,drdFile.length());
				System.out.println("Sub  is"+sub);
				System.out.println("file Name is"+fileName);
				if(sub.equalsIgnoreCase(fileName))
					return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return false;
	}
	
	
	public List<DataDTO> getFileDetailsWithProfile(String profile,Long hostId)  {
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.db.getDBConnection();
		CommonDAO comDao = new CommonDAO();
		try {
			
			String query = "select INTE_ID,INTE_CHECK_DATE,host_id,FILE_PATH,FILE_NAME,DATE_CREATION,SHA1,ACTION,CHECK_VALID,FILE_EXISTS from FILE_DATA_INTEGRITY";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("successfully");
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("successfully" + rs.getLong(1));
				cDao.setInteId(rs.getLong(1));
				cDao.setIntCheckDate(rs.getString(2));
				cDao.setHostId(rs.getLong(3));
				
				cDao.setFilePath(comDao.CheckDecryptData(rs.getString(4)));
				cDao.setFileName(rs.getString(5));
				cDao.setFileCreatedDate(comDao.CheckDecryptData(rs.getString(6)));

				cDao.setSha1(rs.getString(7));
				cDao.setAction(comDao.CheckDecryptData(rs.getString(8)));
				cDao.setCheckValid(comDao.CheckDecryptData(rs.getString(9)));
				cDao.setFileExists(comDao.CheckDecryptData(rs.getString(10)));
				if(getPageFileDetailsFromLogFiles(profile, hostId, cDao.getFileName()))
				list.add(cDao);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	
		
	private boolean getDocFileDetailsFromLogFiles(String path,Long dbId,Long hostId,String fileName)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.db.getDBConnection();
		CommonDAO comDao = new CommonDAO();
		
		String profileName=this.getDBName(dbId, hostId, path);
	System.out.println("getDocFileDetailsFromLogFiles --Profile Name is========="+profileName);
		try {
			String encryptedProfile=comDao.CheckEncryptData( profileName);
			String query = "select DRD_FILE from E2_LOG_MONITOR lm WHERE lm.PROFILE='"+encryptedProfile+"'";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("successfully");
			while (rs.next()) {
				String drdFile=rs.getString(1);
				if(drdFile!=null)
					drdFile=comDao.CheckDecryptData( drdFile);
				
				System.out.println("Drd File"+drdFile);
				int i=drdFile.lastIndexOf("\\");
				
				String sub=drdFile.substring(i+1,drdFile.length());
				System.out.println("Sub  is"+sub);
				System.out.println("file Name is"+fileName);
				if(sub.equalsIgnoreCase(fileName))
					return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return false;
	}
	
	
	
	
	public List<DataDTO> getDOCFileDetailsWithProfile(String profile,Long hostId)  {
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.db.getDBConnection();
		CommonDAO comDao = new CommonDAO();
		try {
			
			String query = "select INTE_ID,INTE_CHECK_DATE,HOST_ID,FILE_PATH,FILE_NAME,DATE_CREATION,SHA1,ACTION,CHECK_VALID,FILE_EXISTS from FILE_DOCDATA_INTEGRITY";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("getDOCFileDetails");
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getDOCFileDetails:" + rs.getLong(1));
				cDao.setInteId(rs.getLong(1));
				cDao.setIntCheckDate(rs.getString(2));
				cDao.setHostId(rs.getLong(3));
				
				cDao.setFilePath(comDao.CheckDecryptData(rs.getString(4)));
				cDao.setFileName(rs.getString(5));
				cDao.setFileCreatedDate(comDao.CheckDecryptData(rs.getString(6)));

				cDao.setSha1(rs.getString(7));
				cDao.setAction(comDao.CheckDecryptData(rs.getString(8)));
				cDao.setCheckValid(comDao.CheckDecryptData(rs.getString(9)));
				cDao.setFileExists(comDao.CheckDecryptData(rs.getString(10)));
				if(getDocFileDetailsFromLogFiles(profile,hostId,cDao.getFileName()))
				list.add(cDao);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	

	public List<DataDTO> getLogFileDetails(Long e2LogFileId, Long size)
			{
		List<DataDTO> list = new ArrayList<DataDTO>();
		// boolean list = false;
		CommonDAO cDao = new CommonDAO();
		
		PreparedStatement ps = null;
		Connection conn =null;
		System.out.println(size+"getLogFileDetails: " + e2LogFileId);
		try {
		 conn = this.db.getDBConnection();
			
			//String s = DateUtil.getCurrentDate();

			// String query =
			// "select INTE_ID,INTE_CHECK_DATE,HOST_NAME,IP_ADDR,FILE_PATH,FILE_NAME,DATE_CREATION,SHA1 from DATA_INTEGRITY where FILE_NAME='"+fileName+"' or CHGED_FILE_NAME='"+fileName+"'";
			// String query =
			// "select d.INTE_ID,d.FILE_NAME,d.DATE_CREATION,d.SHA1 from [CSR-APP].dbo.DATA_INTEGRITY as d, [CSR-APP].dbo.Integrity_Check as i where d.INTE_ID = i.INTE_ID and (d.FILE_NAME='"+fileName+"' or i.CHANGED_FILE_NAME='"+fileName+"')";
			String query = "select E2_LOG_FILE_ID,LOG_FILE_NAME,LOG_FILE_SIZE,FILE_MODIFIED_DATE from E2_LOG_FILE_MONITOR where E2_LOG_FILE_ID=? and LOG_FILE_SIZE < ?";
			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			ps = conn.prepareStatement(query);
			ps.setLong(1, e2LogFileId);
			ps.setLong(2, size);
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				DataDTO dao = new DataDTO();
				System.out.println("results:" + rs.getString(2));
				// cDao.setInteId(rs.getLong(1));
				dao.setE2LogId(rs.getLong(1));
				dao.setFileName(cDao.CheckDecryptData(rs.getString(2)));
				dao.setFileSize((int)rs.getLong(3));
				dao.setFileCreatedDate(rs.getString(4));
				// cDao.setFileName(rs.getString(2));
				// /cDao.setFileCreatedDate(rs.getString(3));

				// cDao.setSha1(rs.getString(4));
				list.add(dao);

				// if(Integer.parseInt(dao.CheckDecryptData(path,rs.getString(2))) <
				// size)

			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	public String getLogProfile(String drpFile)
	{
String profile = "";
// boolean list = false;
CommonDAO cDao = new CommonDAO();

PreparedStatement ps = null;
Connection conn=null;
System.out.println("getLogProfile: " + drpFile);
try {
	conn = this.db.getDBConnection();
	String query = "select distinct PROFILE from E2_LOG_MONITOR where DRD_FILE = ?";
	ps = conn.prepareStatement(query);
	ps.setString(1, cDao.CheckEncryptData( drpFile));
	//ps.setLong(2, size);
	
	ResultSet rs = ps.executeQuery();
	
	while (rs.next()) {
		
		profile = cDao.CheckDecryptData( rs.getString(1));
		System.out.println("getLogProfile::"+profile);
		// if(Integer.parseInt(dao.CheckDecryptData(path,rs.getString(2))) <
		// size)

	}
	
} catch (SQLException e) {
	e.printStackTrace();
}
catch (Exception e) {
	e.printStackTrace();
}
finally {
	try
	{
	if (ps != null)
		ps.close();
	if (conn != null)
		conn.close();
	}
	catch(Exception ex)
	{
		ex.printStackTrace();
	}
}
return profile;
}

	
	public Long updateLogFileStatus(DataDTO dao, Long logFileId, String status,
			boolean errorOccurs) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.db.getDBConnection();
		Long logId = 0L;
		try {
			//String s = DateUtil.getCurrentDate();
			System.out.println("updateLogFileStatus::" + status);
			String hash = AeSimpleSHA1.SHA1(comDao.CheckEncryptData(dao
					.getCompressingFile())
					+ ","
					+ comDao.CheckEncryptData(dao.getProfile())
					+ ","
					+ comDao.CheckEncryptData(dao.getFormat())
					+ ","
					+ comDao.CheckEncryptData(dao.getTotalRead())
					+ ","
					+ comDao.CheckEncryptData(dao.getTotalWrite())
					+ ","
					+ comDao.CheckEncryptData(dao.getFinalRatio())
					+ ","
					+ comDao.CheckEncryptData(dao.getMethod())
					+ ","
					+ comDao.CheckEncryptData(dao.getJournalFile())
					+ ","
					+ comDao.CheckEncryptData(dao.getDocuments())
					+ ","
					+ comDao.CheckEncryptData(dao.getDocumentPages())
					+ ","
					+ comDao.CheckEncryptData(dao.getIgnoredPages())
					+ ","
					+ comDao.CheckEncryptData(String.valueOf(errorOccurs))
					+ ","
					+ comDao.CheckEncryptData(dao.getLogDate())
					+ ","
					+ comDao.CheckEncryptData(dao.getLogTime())
					+ ","
					+ comDao.CheckEncryptData(dao.getDrpFile())
					+ ","
					+ comDao.CheckEncryptData(dao.getDrdFile())
					+ ","
					+ dao.getFileStatus());
			// String query =
			// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";

			ps = conn
					.prepareStatement("UPDATE E2_LOG_MONITOR SET HASH_ID=?,LOG_END_DATE=?,LOG_END_TIME=?,DRD_FILE_STATUS=? where LOG_FILE_ID=?");

			ps.setString(1, hash);
			ps.setString(2, comDao.CheckEncryptData(dao.getLogDate()));
			ps.setString(3, comDao.CheckEncryptData(dao.getLogEndTime()));
			ps.setString(4, comDao.CheckEncryptData(dao.getFileStatus()));

			ps.setLong(5, logFileId);

			ps.executeUpdate();

			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
			
		}catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}  catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return logId;
	}
	public Long updateLogFileType( Long logFileId, String type)  {
		PreparedStatement ps = null;
		//CommonDAO comDao = new CommonDAO(realpath);
		Connection conn = this.db.getDBConnection();
		Long logId = 0L;
		try {
			//String s = DateUtil.getCurrentDate();
			System.out.println("updateLogFileStatus::" + type);
		
			// String query =
			// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";

			ps = conn
					.prepareStatement("UPDATE E2_LOG_MONITOR SET COMPRESSION_TYPE=? where LOG_FILE_ID=?");

			ps.setString(1, type);
			
			ps.setLong(2, logFileId);

			ps.executeUpdate();

			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
			
		}
		catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return logId;
	}
	//public String checkLogFileData()
	
	
	public Long getLogIndexFile(DataDTO dao,boolean errorOccurs,Long e2LogFileId, String[] errorCodes,String type) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.db.getDBConnection();
		Long logId = 0L;
		try {
			
			System.out.println("getLogIndexFile::" + dao.getDrdFile());
			String err = "";
			if(errorOccurs)
				err = "true";
			else
				err= "false";
			if(errorCodes != null && errorCodes.length !=0)
			{
			for(int i=0;i<errorCodes.length;i++)
			{
				
			ps = conn
					.prepareStatement("SELECT m.LOG_FILE_ID from E2_LOG_MONITOR m left join E2_LOG_ERROR e on m.LOG_FILE_ID = e.LOG_FILE_ID where m.DRD_FILE=? and m.ERROR_OCCURS=? and m.E2_LOG_FILE_ID=? and e.ERROR_CODE=? and COMPRESSION_TYPE=?");

			ps.setString(1, comDao.CheckEncryptData(dao.getDrdFile()));
			
			ps.setString(2, comDao.CheckEncryptData(err));
			ps.setLong(3, e2LogFileId);
			ps.setString(4, errorCodes[i]);
			ps.setString(5, type);
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				logId = rs.getLong(1);
			}
			}
			}
			else
			{
				ps = conn
				.prepareStatement("SELECT m.LOG_FILE_ID from E2_LOG_MONITOR m  where m.DRD_FILE=? and m.ERROR_OCCURS=? and m.E2_LOG_FILE_ID=? and COMPRESSION_TYPE=?");

		ps.setString(1, comDao.CheckEncryptData(dao.getDrdFile()));
		
		ps.setString(2, comDao.CheckEncryptData(err));
		ps.setLong(3, e2LogFileId);
		
		ps.setString(4, type);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			logId = rs.getLong(1);
		}
			}
			System.out.println("getLogIndexFile:created successfully"+logId);
			ps.close();
			conn.close();
		}catch (SQLException e) {
			System.out.println("getLogIndexFile:Error is Here");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("getLogIndexFile:Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return logId;
	}

	public Long saveLogIndexing(DataDTO dao, Long logFileId)
			{
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.db.getDBConnection();
		Long logId = 0L;
		try {
			System.out.println("saveLogIndexing::" + logFileId);
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			if(encrypt.equalsIgnoreCase("YES")){
				String hash = AeSimpleSHA1.SHA1(comDao.CheckEncryptData(dao
						.getIndexingFile())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexName())
						+ ","
						+ comDao.CheckEncryptData(dao.getLogDate())
						+ ","
						+ comDao.CheckEncryptData(dao.getResStartTime())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexFileSize())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexDiskRead())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexDiskWrite())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexStackDepth())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexCacheReadHit())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexCacheReadMiss())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexCacheWriteHit())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexCacheWriteMiss())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexAddDuplicate())
						+ ","
						+ comDao.CheckEncryptData(dao.getIndexAddUnique())
						+ "," + logFileId);
				logId = getLogIndexing(dao, logFileId);
				if (logId == 0) {
					ps = conn
							.prepareStatement(
									"INSERT INTO LOG_INDEX(INDEX_FILE_NAME,INDEX_NAME,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID,INDEX_DATE,INDEX_TIME,INDEX_END_DATE,INDEX_END_TIME,LOG_FILE_ID,PROFILE,CHECK_VALID,FILE_EXISTS,ACTION,INTE_CHECK_DATE) VALUES(?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
									PreparedStatement.RETURN_GENERATED_KEYS);
				} else {
					ps = conn
							.prepareStatement("UPDATE LOG_INDEX SET INDEX_FILE_NAME=?,INDEX_NAME=?,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID=?,INDEX_DATE=?,INDEX_TIME=?,INDEX_END_DATE=?,INDEX_END_TIME=? where LOG_FILE_ID=?");

				}
				ps.setString(1, comDao.CheckEncryptData(dao.getIndexingFile()));
				ps.setString(2, comDao.CheckEncryptData(dao.getIndexName()));
				ps.setString(3, comDao.CheckEncryptData(dao.getIndexFileSize()));
				ps.setString(4, comDao.CheckEncryptData(dao.getIndexDiskRead()));
				ps.setString(5, comDao.CheckEncryptData(dao.getIndexDiskWrite()));
				ps.setString(6, comDao.CheckEncryptData(dao.getIndexStackDepth()));
				ps.setString(7, comDao.CheckEncryptData(dao.getIndexCacheReadHit()));
				ps.setString(8,
						comDao.CheckEncryptData(dao.getIndexCacheReadMiss()));
				ps.setString(9,
						comDao.CheckEncryptData(dao.getIndexCacheWriteHit()));
				ps.setString(10,
						comDao.CheckEncryptData(dao.getIndexCacheWriteMiss()));
				ps.setString(11,
						comDao.CheckEncryptData(dao.getIndexAddDuplicate()));
				ps.setString(12, comDao.CheckEncryptData(dao.getIndexAddUnique()));
				ps.setString(13, hash);
				ps.setString(14, comDao.CheckEncryptData(dao.getLogDate()));
				ps.setString(15,
						comDao.CheckEncryptData(dao.getIndexingStartTime()));
				ps.setString(16, comDao.CheckEncryptData(dao.getLogDate()));
				ps.setString(17,
						comDao.CheckEncryptData(dao.getIndexingFinishingTime()));
				ps.setLong(18, logFileId);
				ps.setString(19, comDao.CheckEncryptData(dao.getProfile()));
				ps.setString(20, comDao.CheckEncryptData("T"));
				ps.setString(21, comDao.CheckEncryptData("T"));
				ps.setString(22, comDao.CheckEncryptData("new file"));
				ps.setString(23, comDao.CheckEncryptData(dao.getLogDate()));
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				String hash = AeSimpleSHA1.SHA1(dao.getIndexingFile()
						+ ","
						+ dao.getIndexName()
						+ ","
						+ dao.getLogDate()
						+ ","
						+ dao.getResStartTime()
						+ ","
						+ dao.getIndexFileSize()
						+ ","
						+ dao.getIndexDiskRead()
						+ ","
						+ dao.getIndexDiskWrite()
						+ ","
						+ dao.getIndexStackDepth()
						+ ","
						+ dao.getIndexCacheReadHit()
						+ ","
						+ dao.getIndexCacheReadMiss()
						+ ","
						+ dao.getIndexCacheWriteHit()
						+ ","
						+ dao.getIndexCacheWriteMiss()
						+ ","
						+ dao.getIndexAddDuplicate()
						+ ","
						+ dao.getIndexAddUnique()
						+ "," + logFileId);
				logId = getLogIndexing(dao, logFileId);
				if (logId == 0) {
					ps = conn
							.prepareStatement(
									"INSERT INTO LOG_INDEX(INDEX_FILE_NAME,INDEX_NAME,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID,INDEX_DATE,INDEX_TIME,INDEX_END_DATE,INDEX_END_TIME,LOG_FILE_ID,PROFILE,CHECK_VALID,FILE_EXISTS,ACTION,INTE_CHECK_DATE) VALUES(?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
									PreparedStatement.RETURN_GENERATED_KEYS);
				} else {
					ps = conn
							.prepareStatement("UPDATE LOG_INDEX SET INDEX_FILE_NAME=?,INDEX_NAME=?,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID=?,INDEX_DATE=?,INDEX_TIME=?,INDEX_END_DATE=?,INDEX_END_TIME=? where LOG_FILE_ID=?");

				}
				ps.setString(1, dao.getIndexingFile());
				ps.setString(2, dao.getIndexName());
				ps.setString(3, dao.getIndexFileSize());
				ps.setString(4, dao.getIndexDiskRead());
				ps.setString(5, dao.getIndexDiskWrite());
				ps.setString(6, dao.getIndexStackDepth());
				ps.setString(7, dao.getIndexCacheReadHit());
				ps.setString(8,
						dao.getIndexCacheReadMiss());
				ps.setString(9,
						dao.getIndexCacheWriteHit());
				ps.setString(10,
						dao.getIndexCacheWriteMiss());
				ps.setString(11,
						dao.getIndexAddDuplicate());
				ps.setString(12, dao.getIndexAddUnique());
				ps.setString(13, hash);
				ps.setString(14, dao.getLogDate());
				ps.setString(15,
						dao.getIndexingStartTime());
				ps.setString(16, dao.getLogDate());
				ps.setString(17,
						dao.getIndexingFinishingTime());
				ps.setLong(18, logFileId);
				ps.setString(19, dao.getProfile());
				ps.setString(20, "T");
				ps.setString(21, "T");
				ps.setString(22, "new file");
				ps.setString(23, dao.getLogDate());
			}
			
			ps.executeUpdate();
			if (logId == 0) {
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					logId = rs.getLong(1);
				}
			}
			System.out.println("created successfully");
			
		} catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}  catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return logId;
	}

	public Long updateLogIndexing(DataDTO dao, Long logFileId)
			 {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.db.getDBConnection();
		Long logId = 0L;
		try {
			//String s = DateUtil.getCurrentDate();
			String flag = "F";
			System.out.println("saveLogIndexing::" + logFileId);
			// String query =
			// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";
			// String query1 =
			// "INSERT INTO LOG_INDEX(INDEX_FILE_NAME,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID,INDEX_DATE,INDEX_TIME,INDEX_END_DATE,INDEX_END_TIME,LOG_FILE_ID) VALUES('"+checkNull(indexFile)+"','"+checkNull(indexFileSize)+"','"+checkNull(indexDiskRead)+"','"+checkNull(indexDiskWrite)+"','"+checkNull(indexStackDepth)+"','"+checkNull(indexCacheReadHit)+"','"+checkNull(indexCacheReadMiss)+"','"+checkNull(indexCacheWriteHit)+"','"+checkNull(indexCacheWriteMiss)+"','"+checkNull(indexAddDuplicate)+"','"+checkNull(indexAddUnique)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startTime)+"','"+checkNull(date)+"','"+checkNull(endTime)+"',"+logId+");";
			logId = getLogIndexing(dao, logFileId);
			if (logId != 0) {

				ps = conn
						.prepareStatement("UPDATE LOG_INDEX SET STATUS=? where LOG_FILE_ID=?");

			}
			ps.setString(1, comDao.CheckEncryptData(flag));

			ps.setLong(2, logFileId);
			ps.executeUpdate();
			if (logId == 0) {
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					logId = rs.getLong(1);
				}
			}

			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
			
		}catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}  catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return logId;
	}

	public Long saveLogIndexDetails(DataDTO dao, Long indexId, int count)
			 {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.db.getDBConnection();
		Long logId = 0L;
		try {
			//String s = DateUtil.getCurrentDate();
			String flag = "T";
			System.out.println("saveLogIndexDetails::" + indexId);

			String hash = AeSimpleSHA1.SHA1(comDao.CheckEncryptData(dao
					.getIndexFileSize())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexDiskRead())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexDiskWrite())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexStackDepth())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheReadHit())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheReadMiss())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheWriteHit())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexCacheWriteMiss())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexAddDuplicate())
					+ ","
					+ comDao.CheckEncryptData(dao.getIndexAddUnique())
					+ "," + indexId);
			// String query =
			// "INSERT INTO E2_LOG_MONITOR(DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,HASH_ID,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME) VALUES('"+checkNull(compressingFile)+"','"+checkNull(profile)+"','"+checkNull(format)+"','"+checkNull(totalRead)+"','"+checkNull(totalWritten)+"','"+checkNull(finalRatio)+"','"+checkNull(methodName)+"','"+checkNull(jrnlFile)+"','"+checkNull(documents)+"','"+checkNull(documentPages)+"','"+checkNull(ignorePages)+"','"+checkNull(errorOccur)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startingTime)+"','"+checkNull(date)+"','"+checkNull(indexFinishingTime)+"');";
			System.out.println("hash" + hash);
			// String query1 =
			// "INSERT INTO LOG_INDEX(INDEX_FILE_NAME,INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID,INDEX_DATE,INDEX_TIME,INDEX_END_DATE,INDEX_END_TIME,LOG_FILE_ID) VALUES('"+checkNull(indexFile)+"','"+checkNull(indexFileSize)+"','"+checkNull(indexDiskRead)+"','"+checkNull(indexDiskWrite)+"','"+checkNull(indexStackDepth)+"','"+checkNull(indexCacheReadHit)+"','"+checkNull(indexCacheReadMiss)+"','"+checkNull(indexCacheWriteHit)+"','"+checkNull(indexCacheWriteMiss)+"','"+checkNull(indexAddDuplicate)+"','"+checkNull(indexAddUnique)+"','"+hash+"','"+checkNull(date)+"','"+checkNull(startTime)+"','"+checkNull(date)+"','"+checkNull(endTime)+"',"+logId+");";

			logId = getLogIndexDetails(dao, indexId);
			if (logId == 0) {
				ps = conn
						.prepareStatement(
								"INSERT INTO LOG_INDEX_DETAILS(INDEX_FILE_SIZE,INDEX_DISK_READ,INDEX_DISK_WRITE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,HASH_ID,STATUS,INDEX_ID) VALUES(?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?)",
								PreparedStatement.RETURN_GENERATED_KEYS);
			} else {
				ps = conn
						.prepareStatement("UPDATE LOG_INDEX_DETAILS SET INDEX_FILE_SIZE=?,INDEX_DISK_READ=?,INDEX_DISK_WRITE=?,STATCK_DEPTH=?,CACHE_READ_HIT=?,CACHE_READ_MISS=?,CAHE_WRITE_HIT=?,CAHE_WRITE_MISS=?,ADD_DUPLICATE=?,ADD_UNIQUE=?,HASH_ID=?,STATUS=? where INDEX_ID=?");

			}

			ps.setString(1, comDao.CheckEncryptData(dao.getIndexFileSize()));
			ps.setString(2, comDao.CheckEncryptData(dao.getIndexDiskRead()));
			ps.setString(3, comDao.CheckEncryptData(dao.getIndexDiskWrite()));
			ps.setString(4, comDao.CheckEncryptData(dao.getIndexStackDepth()));
			ps.setString(5, comDao.CheckEncryptData(dao.getIndexCacheReadHit()));
			ps.setString(6,
					comDao.CheckEncryptData(dao.getIndexCacheReadMiss()));
			ps.setString(7,
					comDao.CheckEncryptData(dao.getIndexCacheWriteHit()));
			ps.setString(8,
					comDao.CheckEncryptData(dao.getIndexCacheWriteMiss()));
			ps.setString(9, comDao.CheckEncryptData(dao.getIndexAddDuplicate()));
			ps.setString(10, comDao.CheckEncryptData(dao.getIndexAddUnique()));
			ps.setString(11, comDao.CheckEncryptData(flag));
			ps.setString(12, hash);

			ps.setLong(13, indexId);
			ps.executeUpdate();
			if (logId == 0) {
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					logId = rs.getLong(1);
				}
			}

			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
			
		}catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return logId;
	}

	public Long getLogIndexing(DataDTO dao, Long logFileId)  {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.db.getDBConnection();
		Long logId = 0L;
		try {
			System.out.println("getLogIndexing>>" + logFileId);
			String flag = "T";
			ps = conn
					.prepareStatement("SELECT INDEX_ID FROM LOG_INDEX where (INDEX_FILE_NAME=? or INDEX_NAME=? or INDEX_DATE=? or INDEX_TIME=? or INDEX_END_DATE=? or INDEX_END_TIME=?) and STATUS=? and INDEX_ID=?");

			ps.setString(1, comDao.CheckEncryptData(dao.getIndexingFile()));
			ps.setString(2, comDao.CheckEncryptData(dao.getIndexName()));
			ps.setString(3, comDao.CheckEncryptData(dao.getLogDate()));
			ps.setString(4, comDao.CheckEncryptData(dao.getIndexingStartTime()));
			ps.setString(5, comDao.CheckEncryptData(dao.getLogDate()));
			ps.setString(6,
					comDao.CheckEncryptData(dao.getIndexingFinishingTime()));
			ps.setString(7, comDao.CheckEncryptData(flag));
			ps.setLong(8, logFileId);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				logId = rs.getLong(1);
			}
			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
			
		} catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		}  catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return logId;
	}

	public Long getLogIndexDetails(DataDTO dao, Long indexId)
			 {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.db.getDBConnection();
		Long logId = 0L;
		try {
			System.out.println("getLogIndexing>>" + indexId);

			ps = conn
					.prepareStatement("SELECT INDEX_DET_ID FROM LOG_INDEX_DETAILS where (INDEX_FILE_SIZE=? or INDEX_DISK_READ=? or INDEX_DISK_WRITE=? or STATCK_DEPTH=? or CACHE_READ_HIT=? or CACHE_READ_MISS=? or CAHE_WRITE_HIT=? or CAHE_WRITE_MISS=? or ADD_DUPLICATE=? or ADD_UNIQUE=?) and INDEX_ID=?");

			ps.setString(1, comDao.CheckEncryptData(dao.getIndexFileSize()));
			ps.setString(2, comDao.CheckEncryptData(dao.getIndexDiskRead()));
			ps.setString(3, comDao.CheckEncryptData(dao.getIndexDiskWrite()));
			ps.setString(4, comDao.CheckEncryptData(dao.getIndexStackDepth()));
			ps.setString(5, comDao.CheckEncryptData(dao.getIndexCacheReadHit()));
			ps.setString(6,
					comDao.CheckEncryptData(dao.getIndexCacheReadMiss()));
			ps.setString(7,
					comDao.CheckEncryptData(dao.getIndexCacheWriteHit()));
			ps.setString(8,
					comDao.CheckEncryptData(dao.getIndexCacheWriteMiss()));
			ps.setString(9, comDao.CheckEncryptData(dao.getIndexAddDuplicate()));
			ps.setString(10, comDao.CheckEncryptData(dao.getIndexAddUnique()));

			ps.setLong(11, indexId);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				logId = rs.getLong(1);
			}

			// Statement stmt = this.dbManager.getDBConnection().createStatement();
			// stmt.execute(query);
			System.out.println("created successfully");
			
		}catch (SQLException e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error is Here");
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return logId;
	}

			
	public List<DataDTO> getLogIndex(Long logFileId) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.db.getDBConnection();
		List<DataDTO> res = new ArrayList<DataDTO>();
		System.out.println("getLogResources.."+logFileId);
		try {
			String sql = "SELECT INDEX_ID, INDEX_NAME FROM LOG_INDEX WHERE LOG_FILE_ID=?";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, logFileId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO dao = new DataDTO();
				dao.setIndexId(rs.getLong(1));
				if(rs.getString(2) != null)
				{
				String indexName = comDao.CheckDecryptData(rs.getString(2)).split("\\\\")[1];
				dao.setIndexName(indexName);
				}
				res.add(dao);

			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return res;
	}
	public void saveLogResourceStatus(Long logFileId,Long resourceId, String resourceName, String status, String newFileName,String realpath,String action) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.db.getDBConnection();
		
		System.out.println("saveLogResourceStatus:"+logFileId);
		System.out.println("saveLogResourceStatus:"+resourceName);
		try {
			if(logFileId != null)
			{
				if(newFileName != null && !newFileName.equals(""))
				{
					System.out.println("coming here:not "+newFileName);
					String sql = "UPDATE LOG_RESOURCES SET RES_STATUS=? ,ACTION=? WHERE RESOURCE_NAME=? and LOG_FILE_ID=? and RESOURCE_ID=?";
					ps = conn.prepareStatement(sql);
					
					ps.setString(1, comDao.CheckEncryptData(status));
					ps.setString(2, action);
					ps.setString(3, comDao.CheckEncryptData(resourceName));
					ps.setLong(4, logFileId);
					ps.setLong(5, resourceId);
					
					ps.executeUpdate();
				}else
					
					{
				System.out.println("coming here:null");
				String sql = "UPDATE LOG_RESOURCES SET RES_STATUS=?,NEW_RESOURCE_NAME=?,ACTION=?  WHERE RESOURCE_NAME=? and LOG_FILE_ID=? and RESOURCE_ID=?";
				ps = conn.prepareStatement(sql);
				
				ps.setString(1, comDao.CheckEncryptData(status));
				
				ps.setString(2, comDao.CheckEncryptData(newFileName));
				ps.setString(3, action);
				ps.setString(4, comDao.CheckEncryptData(resourceName));
				ps.setLong(5, logFileId);
				ps.setLong(6, resourceId);
				ps.executeUpdate();
			
			
					}
			
		
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
	}

}