package com.pb.dao;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.pb.common.AeSimpleSHA1;
import com.pb.common.CommonConstants;
//import com.pb.common.DataEncryptDecrypt;
import com.pb.common.DateUtil;
import com.pb.common.GetDay;
import com.pb.common.PropertyUtils;
import com.pb.dto.DataDTO;
import com.pb.dto.RoleDTO;
import com.pb.dto.UserDTO;
import com.pb.ldap.LDAPUtils;

public class UserDAO {

	DBConnectionDAO db = new DBConnectionDAO();
	//DataEncryptDecrypt data = new DataEncryptDecrypt();
	CommonDAO data=new CommonDAO();
	Date date = new Date();
	private static String ecrypt="NO";


	// UserDTO dto = new UserDTO();
	public long saveUserPrevDetails(UserDTO dto, String action, String flag) {

		long save = 0;
		PreparedStatement stmt = null;
		String active = "";
		String delFlag = "N";
		String updateFlag = "N";
		String currYear = "";
		String accessdateFrom = "";
		String accessdateTo = "";
		String accessFromDays = "";
		String accessToDays = "";
		String success = "";
		String validfromDays = "";
		String validtoDays = "";
		String weekend = "";
		String userGrp = "";

		try {
			Timestamp tsReturn = null;
			System.out.println("accessdateFrom::" + dto.getAccessdateFrom());

			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);

			if (dto.getActive() != null) {
				active = data.CheckEncryptData(dto.getActive().toString());
			}

			delFlag =data.CheckEncryptData(delFlag);

			if (dto.getAccessdateFrom() != null) {
				accessdateFrom = data.CheckEncryptData(dto.getAccessdateFrom()
						.toString());
			}
			if (dto.getAccessdateTo() != null) {
				accessdateTo = data.CheckEncryptData(dto.getAccessdateTo()
						.toString());
			}

			updateFlag = data.CheckEncryptData(updateFlag);

			if (dto.getAccessfromDays() != 0) {
				accessFromDays =data.CheckEncryptData(String.valueOf(dto
						.getAccessfromDays()));
			}
			if (dto.getAccesstoDays() != 0) {
				accessToDays = data.CheckEncryptData(String.valueOf(dto
						.getAccesstoDays()));
			}
			if (dto.getValidfromDays() != null) {
				validfromDays =data.CheckEncryptData(dto.getValidfromDays()
						.toString());
			}
			if (dto.getValidtoDays() != null) {
				validtoDays = data.CheckEncryptData(dto.getValidtoDays().toString());
			}

			if (dto.getUserGrp() != null) {

				userGrp = data.CheckEncryptData(dto.getUserGrp().toString());
			}
			if (flag != null)
				flag = data.CheckEncryptData(flag);
			System.out.println("********" + success);
			String crDate = data.CheckEncryptData(s);

			String hash = AeSimpleSHA1.SHA1(dto.getUserName() + ","
					+ dto.getGroupId() + "," + active + "," + currYear + ","
					+ accessdateFrom + "," + accessdateTo + ","
					+ accessFromDays + "," + accessToDays + "," + crDate + ","
					+ flag + "," + delFlag);
			String query = "";

			if (action.equalsIgnoreCase("update")) {
				query = "UPDATE USER_MGMT SET ACTIVE_FLAG=" + "?"

				+ ",ACCESS_DATE_FROM=" + "?"
				+ ",ACCESS_DATE_TO=" + "?"
				+ ",ACCESS_DAYS_FROM=" + "?"
				+ ",ACCESS_DAYS_TO=" + "?"
				+ ",CREATE_DATE=" + "?" + ",HASH_ID=" + "?"
				+ ",PROFILE_COMPLETED_FLAG=" + "?"
				+ ",DELETE_FLAG=" + "?" + ",UPDATE_FLAG="
				+ "?" + " where USER_ID=" + "?"
				+ "";
				stmt = db.getDBConnection().prepareStatement(query);
				stmt.setString(1, active);
				stmt.setString(2,accessdateFrom );
				stmt.setString(3,accessdateTo );
				stmt.setString(4,accessFromDays );
				stmt.setString(5,accessToDays );
				stmt.setString(6,crDate );
				stmt.setString(7,hash );
				stmt.setString(8,flag );
				stmt.setString(9,delFlag );
				stmt.setString(10,updateFlag );

				stmt.setLong(11, dto.getUserId());
				stmt.executeUpdate();
				System.out.println("saveUserPrevDetails:Saved:User Details"
						+ dto.getUserId());
				// insertRole(save,dto.getRoleId());
				// insertDatabase(save,dto.getDbId());
				updateUserIntegrity(CommonConstants.USER_INTEGRITY_CHECK,
						"USER_ID", hash, hash, dto.getUserId(), "yes");

			}

			return dto.getUserId();
			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			//db.getd
		}

		return save;
	}

	public long saveTempUsergrpDetails1(UserDTO dto, String action,
			String saveFlag) {

		long userGrpId = 0;
		PreparedStatement stmt = null;
		String success = "";
		String userGrp = "";
		String grpDesc = "";
		//PreparedStatement ps = null;
		try {
			System.out.println("saveTempUsergrpDetails1:" + dto.getUserGrp());
			if (saveFlag != null)
				saveFlag = data.CheckEncryptData(saveFlag);
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			if (dto.getUserGrp() != null) {
				userGrp = data.CheckEncryptData(dto.getUserGrp().toString());
			}
			if (dto.getGroupDesc() != null) {
				grpDesc = data.CheckEncryptData(dto.getGroupDesc().toString());
			}
			System.out.println("********" + success);
			String crDate = data.CheckEncryptData(s);
			String hash = AeSimpleSHA1.SHA1(userGrp + "," + grpDesc);
			String query = "";
			if (action.equalsIgnoreCase("create")) {
				query = "INSERT INTO USER_GROUPS(USER_GROUP,GROUP_DESCRIPTION) VALUES("
						+ "?" + "," + "?" + ");";
				//	ps = db.getDBConnection().prepareStatement(query);
				stmt = db.getDBConnection().prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, userGrp);
				stmt.setString(2, grpDesc);
				stmt.executeUpdate();
				System.out.println("^^^^^^^^^^^^^Saved:User Group Details");

				ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					userGrpId = rs.getLong(1);
					System.out.println("GroupId--->" + userGrpId);
					// createTempuserMgmt(userGrpId,saveFlag);
					saveUserIntegrity(
							CommonConstants.USER_GROUPS_INTEGRITY_CHECK,
							"USER_GRP_ID", hash, hash, userGrpId, "yes");
					// String query1 =
					// "INSERT INTO USER_MGMT_REPORT(USER_GROUP,ACTIVE_FLAG,CURRENT_YEAR,ACCESS_DATE_FROM,ACCESS_DATE_TO,ACCESS_DAYS_FROM,ACCESS_DAYS_TO,CREATE_DATE,CREATE_TIME,HASH_ID,USER_ID) VALUES('"+userGrp+"','"+active+"','"+currYear+"','"+accessdateFrom+"','"+accessdateTo+"','"+accessFromDays+"','"+accessToDays+"','"+crDate+"',DEFAULT,'"+hash+"',"+save+");";
					// stmt.execute(query1);
				}
			}
			/*
			 * else if(action.equalsIgnoreCase("update")) { query =
			 * "UPDATE USER_MGMT SET ACTIVE_FLAG='"
			 * +active+"',CURRENT_YEAR='"+currYear
			 * +"',ACCESS_DATE_FROM='"+accessdateFrom
			 * +"',ACCESS_DATE_TO='"+accessdateTo
			 * +"',ACCESS_DAYS_FROM='"+accessFromDays
			 * +"',ACCESS_DAYS_TO='"+accessToDays
			 * +"',UPDATE_DATE='"+crDate+"',HASH_ID='"
			 * +hash+"' where USER_GROUP='"
			 * +userGrp+"' and USER_ID="+dto.getUserId()+";"; stmt =
			 * db.getDBConnection().createStatement();
			 * stmt.execute(query,Statement.RETURN_GENERATED_KEYS);
			 * System.out.println("^^^^^^^^^^^^^Saved:User Details");
			 * 
			 * 
			 * ResultSet rs = stmt.getGeneratedKeys(); if(rs.next()) { save =
			 * rs.getLong(1); System.out.println("^^^^^^^^^^^^^updated"+save);
			 * //insertRole(save,dto.getRoleId());
			 * //insertDatabase(save,dto.getDbId());
			 * updateUserIntegrity(CommonConstants
			 * .USER_INTEGRITY_CHECK,"USER_ID",hash,hash,save,"yes");
			 * 
			 * } }
			 */

			return userGrpId;
			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return userGrpId;
	}

	public Long getGroupIdbelongstoGroup(String groupName) {
		System.out.println("Inside getGroupIdbelongstoGroup"+groupName);
		System.out.println("Group name is null"+groupName==null);
		Long groupId = 0L;
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		try {
			if (groupName != null) {
				groupName = comDao.CheckEncryptData(groupName);
				System.out.println("Encrypted group Name"+groupName);
				String query1 = "SELECT USER_GRP_ID,USER_GROUP,GROUP_DESCRIPTION from USER_GROUPS where USER_GROUP=?";
				ps = db.getDBConnection().prepareStatement(query1);
				ps.setString(1, groupName);
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					groupId = rs.getLong(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return groupId;
	}





	public Long getUserIdofUser(String UserName) {
		System.out.println("getUserIdofUser!!!!!!!" + UserName);
		Long userId = 0L;
		PreparedStatement ps1 = null;
		CommonDAO comDao = new CommonDAO();
		try {
			if (UserName != null) {
				UserName = UserName.toLowerCase();
				UserName = comDao.CheckEncryptData(UserName);
				String delete_flag = comDao.CheckEncryptData("N");
				System.out.println("getUserIdofUser****" + UserName);
				String query1 = "SELECT USER_ID from USER_MGMT where USER_NAME=? and DELETE_FLAG=?";
				ps1 = db.getDBConnection().prepareStatement(query1);
				ps1.setString(1, UserName);
				ps1.setString(2, delete_flag);
				ResultSet rs1 = ps1.executeQuery();

				if (rs1.next()) {
					userId = rs1.getLong(1);
					System.out.println("getGroupIdbelongstoGroup:groupId>>>>"+ userId);

				}
				else
					userId = 0L;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return userId;
	}


	public void createTempuserMgmt(Long userGrpId, String saveFlag) {
		try {

			String hash = AeSimpleSHA1.SHA1(userGrpId + "," + saveFlag);
			String query1 = "INSERT INTO USER_MGMT(USER_GRP_ID,PROFILE_COMPLETED_FLAG,HASH_ID) VALUES("
					+ "?" + "," + "?" + "," + "?" + ");";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query1,Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1,userGrpId);
			stmt.setString(2, saveFlag);
			stmt.setString(3, hash);

			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				Long id = rs.getLong(1);
				saveUserIntegrity(CommonConstants.USER_INTEGRITY_CHECK,
						"USER_ID", hash, hash, id, "yes");

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Long createTempuserMgmt1(UserDTO dto, String action, String flag,
			Long groupId) {

		String userName = "";
		Long userId = 0l;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String crDate = data.CheckEncryptData(s);
			if (dto.getUserName() != null) {
				userName = data.CheckEncryptData(dto.getUserName());
			}
			if (flag != null) {
				flag = data.CheckEncryptData(flag);
			}
			String delFlag = data.CheckEncryptData("N");
			String hash = AeSimpleSHA1.SHA1(groupId + "," + userName + ","
					+ flag);
			String query1 = "INSERT INTO USER_MGMT(DELETE_FLAG,USER_NAME,USER_GRP_ID,PROFILE_COMPLETED_FLAG,HASH_ID,CREATE_DATE,CREATE_TIME) values("
					+ "?"
					+ ","
					+ "?"
					+ ","
					+ "?"
					+ ","
					+ "?"
					+ "," + "?" + "," + "?" + ",DEFAULT);";

			System.out.println("Query1 is"+query1);
			System.out.println("del flag"+delFlag);
			System.out.println("username"+userName);
			System.out.println("groupid"+groupId);
			System.out.println("flag is"+flag);
			System.out.println("hash is"+hash);
			System.out.println("crdate is"+crDate);
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query1, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, delFlag);
			stmt.setString(2, userName);
			stmt.setLong(3, groupId);
			stmt.setString(4, flag);
			stmt.setString(5, hash);
			stmt.setString(6, crDate);
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				userId = rs.getLong(1);
				saveUserIntegrity(CommonConstants.USER_INTEGRITY_CHECK,
						"USER_ID", hash, hash, userId, "yes");

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userId;
	}

	public boolean getCsrUsers(Long userId) {
		boolean b = false;
		try {
			String query1 = "select u.user_Id,u.USER_NAME,ug.USER_GROUP from USER_MGMT u, USER_GROUPS ug where u.USER_GRP_ID = ug.USER_GRP_ID and USER_ID="
					+ userId;
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query1);
			if (rs.next()) {
				b = true;
			}

		} catch (Exception e) {

		}
		return b;
	}

	public List<UserDTO> getAllUsers() {
		System.out.println("getAllUsers");
		List users = new ArrayList();
		CommonDAO cDao = new CommonDAO();
		String day1 = "";
		String day2 = "";
		PreparedStatement ps = null;
		try {
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");
			String admin = (PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME)).getProperty(CommonConstants.ADMINISTRATOR);
			admin = cDao.CheckEncryptData(admin.toLowerCase());
			String test = cDao.CheckEncryptData("test");
			System.out.println("admin username::"+admin);
			System.out.println("test::"+test);

			String query1 = "select u.USER_ID,ug.USER_GRP_ID,u.USER_NAME,u.ACCESS_DAYS_FROM, u.ACCESS_DAYS_TO, u.ACCESS_Date_FROM, u.ACCESS_DATE_TO, ug.USER_GROUP,u.ACTIVE_FLAG from  USER_MGMT u, USER_GROUPS ug where u.USER_GRP_ID = ug.USER_GRP_ID and u.DELETE_FLAG = ? and u.PROFILE_COMPLETED_FLAG = ? and u.User_NAME!=?"
					+ " and u.User_NAME!=?";

			ps = db.getDBConnection().prepareStatement(query1);
			ps.setString(1, delflag);
			ps.setString(2, flag);
			ps.setString(3, admin);
			ps.setString(4, test);
			System.out.println("query1::"+query1);
			// Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserDTO uDto = new UserDTO();

				uDto.setUserId(rs.getLong(1));
				uDto.setGroupId(rs.getLong(2));
				uDto.setUserName(rs.getString(3)==null?"":cDao.CheckDecryptData(rs.getString(3)));

				if (rs.getString(4) != null && !rs.getString(4).equals("")) {
					uDto.setAccessfromDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(4))));
					day1 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(4))));
				}
				if (rs.getString(5) != null && !rs.getString(4).equals("")) {
					uDto.setAccesstoDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(5))));

					day2 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(5))));
				}
				System.out.println("day1" + day1 + "day2" + day2);
				uDto.setAccessfromDay(day1);
				uDto.setAccesstoDay(day2);
				if (rs.getString(6) != null && !rs.getString(6).equals("")) {
					uDto.setAccessdateFrom(data.CheckDecryptData(rs.getString(6)));
				}

				if (rs.getString(7) != null && !rs.getString(7).equals("")) {
					uDto.setAccessdateTo(data.CheckDecryptData(rs.getString(7)));
				}
				//	uDto.setAccessdateTo(data.decryptData(rs.getString(7)));

				if (rs.getString(8) != null && !rs.getString(8).equals("")) {
					uDto.setUserGrp(data.CheckDecryptData(rs.getString(8)));
				}

				if (rs.getString(9) != null && !rs.getString(9).equals("")) {
					uDto.setActive(data.CheckDecryptData(rs.getString(9)));
				}

				System.out.println("getAllUsers");
				users.add(uDto);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	public List getAllProfileUsers(String profile) {
		System.out.println("getAllProfileUsers");
		List users = new ArrayList();
		CommonDAO cDao = new CommonDAO();
		String day1 = "";
		String day2 = "";
		PreparedStatement ps = null;
		try {
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");
			String admin = cDao.CheckEncryptData(CommonConstants.ADMINISTRATOR);

			String query1 = "select distinct u.USER_ID,ug.USER_GRP_ID,u.USER_NAME,u.ACCESS_DAYS_FROM, u.ACCESS_DAYS_TO, u.ACCESS_Date_FROM, u.ACCESS_DATE_TO, ug.USER_GROUP,u.ACTIVE_FLAG from  USER_MGMT u, USER_GROUPS ug, PROFILE_GROUP pg, profile p where u.USER_GRP_ID = ug.USER_GRP_ID and u.DELETE_FLAG = ? and u.PROFILE_COMPLETED_FLAG = ? and u.USER_ID = pg.USER_ID and "
					+"pg.ADMIN_FLAG = 'N' and pg.PROFILE_ID = p.PROFILE_ID and p.NAME =? and p.DELETE_FLAG =?";

			ps = db.getDBConnection().prepareStatement(query1);
			ps.setString(1, delflag);
			ps.setString(2, flag);
			ps.setString(3, profile);
			ps.setString(4, delflag);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserDTO uDto = new UserDTO();

				uDto.setUserId(rs.getLong(1));
				uDto.setGroupId(rs.getLong(2));
				uDto.setUserName(rs.getString(3)==null?"":cDao.CheckDecryptData(rs.getString(3)));

				if (rs.getString(4) != null && !rs.getString(4).equals("")) {
					uDto.setAccessfromDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(4))));
					day1 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(4))));
				}
				if (rs.getString(5) != null && !rs.getString(4).equals("")) {
					uDto.setAccesstoDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(5))));

					day2 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(5))));
				}
				System.out.println("day1" + day1 + "day2" + day2);
				uDto.setAccessfromDay(day1);
				uDto.setAccesstoDay(day2);
				if (rs.getString(6) != null && !rs.getString(6).equals("")) {
					uDto.setAccessdateFrom(data.CheckDecryptData(rs.getString(6)));
				}

				if (rs.getString(7) != null && !rs.getString(7).equals("")) {
					uDto.setAccessdateTo(data.CheckDecryptData(rs.getString(7)));
				}
				//	uDto.setAccessdateTo(data.decryptData(rs.getString(7)));

				if (rs.getString(8) != null && !rs.getString(8).equals("")) {
					uDto.setUserGrp(data.CheckDecryptData(rs.getString(8)));
				}

				if (rs.getString(9) != null && !rs.getString(9).equals("")) {
					uDto.setActive(data.CheckDecryptData(rs.getString(9)));
				}

				System.out.println("getAllProfileUsers");
				users.add(uDto);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	public List<UserDTO> getProfileList() {
		System.out.println("Inside getProfileList");
		List<UserDTO> profileList = new ArrayList<UserDTO>();
		try {
			String delflag = data.CheckEncryptData("N");
			PreparedStatement ps = null;
			//String query1 = "select PROFILE_ID, NAME from Profile where DELETE_FLAG = '"+delflag+"'";
			/* Nithu Alexander: 23/10/2014. Changing query to retrive profileNames from DB in alphabetical order*/
			String query1 = "select PROFILE_ID, NAME from Profile where DELETE_FLAG = '"+delflag+"' ORDER BY NAME ASC";
			ps = db.getDBConnection().prepareStatement(query1);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserDTO uDto = new UserDTO();
				uDto.setProfileId(rs.getLong(1));
				uDto.setProfileName(rs.getString(2));
				profileList.add(uDto);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return profileList;
	}

	public String getProfileGroup(Long profileID) {
		System.out.println("Inside getProfileGroup");
		String profileGroup = "";
		try {
			String delflag = data.CheckEncryptData("N");
			Statement stmt = null;
			/*String query = "select distinct GROUP_NAME from PROFILE_GROUP where PROFILE_ID= "
				+ profileID+" and ADMIN_FLAG='Y' and DELETE_FLAG='"+delflag+"'";*/
			String query = "select distinct GROUP_NAME from PROFILE_GROUP where PROFILE_ID= "
					+ profileID+"  and DELETE_FLAG='"+delflag+"'";

			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				profileGroup += rs.getString(1) + ", ";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("before::"+profileGroup);
		if(profileGroup.indexOf(",")!=-1){
			profileGroup = profileGroup.substring(0,profileGroup.length()-1);
		}
		System.out.println("after::"+profileGroup);
		System.out.println("Returning from getProfileGroup");
		return profileGroup;
	}

	public String getProfileDb(Long profileID) {
		System.out.println("Inside getProfileDb");
		String ProfileDb = "";
		try {
			Statement stmt = null;
			String delflag = data.CheckEncryptData("N");
			String query = "select distinct DB_NAME from CSR_DATABASE csrdb, PROFILE_GROUP pg where pg.DB_ID = csrdb.DB_ID and pg.PROFILE_ID= "
					+ profileID+" and pg.ADMIN_FLAG='Y' and pg.DELETE_FLAG='"+delflag+"'";
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				ProfileDb += rs.getString(1) + ",";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("before::"+ProfileDb);
		if(ProfileDb.indexOf(",")!=-1){
			ProfileDb = ProfileDb.substring(0,ProfileDb.length()-1);
		}
		System.out.println("after::"+ProfileDb);
		System.out.println("Returning from getProfileDb");
		return ProfileDb;
	}

	public String getProfileUsers(Long profileID) {
		System.out.println("Inside getProfileUsers");
		String ProfileUsers = "";
		try {
			PreparedStatement ps = null;
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");
			String query = "select distinct u.USER_NAME from  USER_MGMT u, PROFILE_GROUP pg" +
					" where u.DELETE_FLAG = ? and u.PROFILE_COMPLETED_FLAG = ? and u.USER_ID = pg.USER_ID and"+
					" pg.ADMIN_FLAG = 'N' and pg.PROFILE_ID= ? and pg.DELETE_FLAG = ?";
			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, delflag);
			ps.setString(2, flag);
			ps.setLong(3, profileID);
			ps.setString(4, delflag);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ProfileUsers += data.CheckDecryptData(rs.getString(1)) + ", ";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("before::"+ProfileUsers);
		if(ProfileUsers.indexOf(",")!=-1){
			ProfileUsers = ProfileUsers.substring(0,ProfileUsers.length()-1);
		}
		System.out.println("after::"+ProfileUsers);
		System.out.println("Returning from getProfileUsers");
		return ProfileUsers;
	}

	public List<UserDTO> getUser(Long userId) {
		List<UserDTO> users = new ArrayList<UserDTO>();
		String day1 = "";
		String day2 = "";
		try {

			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");
			String query1 = "select u.USER_ID,ug.USER_GRP_ID,u.USER_NAME,u.CURRENT_YEAR,u.ACCESS_DAYS_FROM, u.ACCESS_DAYS_TO, u.ACCESS_Date_FROM, u.ACCESS_DATE_TO, ug.USER_GROUP, u.ACTIVE_FLAG from  USER_MGMT u, USER_GROUPS ug where u.USER_GRP_ID = ug.USER_GRP_ID and u.DELETE_FLAG = '"
					+ delflag
					+ "' and u.PROFILE_COMPLETED_FLAG = '"
					+ flag
					+ "' and u.USER_ID=" + userId;
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query1);

			while (rs.next()) {
				UserDTO uDto = new UserDTO();

				uDto.setUserId(rs.getLong(1));
				uDto.setGroupId(rs.getLong(2));
				uDto.setUserName(data.CheckDecryptData(rs.getString(3)));

				if (rs.getString(5) != null) {
					uDto.setAccessfromDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(5))));
					day1 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(5))));
				}
				if (rs.getString(6) != null) {
					uDto.setAccesstoDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(6))));

					day2 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(6))));
				}
				System.out.println("day1" + day1 + "day2" + day2);
				uDto.setAccessfromDay(day1);
				uDto.setAccesstoDay(day2);
				if (rs.getString(7) != null) {
					uDto.setAccessdateFrom(data.CheckDecryptData(rs.getString(7)));}
				if (rs.getString(8) != null) {
					uDto.setAccessdateTo(data.CheckDecryptData(rs.getString(8)));}
				if (rs.getString(9) != null) {
					uDto.setUserGrp(data.CheckDecryptData(rs.getString(9)));}
				if (rs.getString(10) != null) {
					uDto.setActive(data.CheckDecryptData(rs.getString(10)));}
				users.add(uDto);
				System.out.println("list size:::" + users.size());

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;

	}

	public List getDBProjectAdminUsersBelongsToProfile(Long profileId,Long dbId)
	{List users = new ArrayList();
	String day1 = "";
	String day2 = "";
	PreparedStatement ps = null;
	try {
		System.out.println("getDBUsers" + dbId);
		String flag = data.CheckEncryptData("Y");
		String delflag = data.CheckEncryptData("N");

		String padminroledesc = CommonConstants.ROLE_DESC_PROJECTADMIN;

		String sql="select distinct  umg.USER_ID,umg.USER_NAME,umg.ACCESS_DAYS_FROM, umg.ACCESS_DAYS_TO, umg.ACCESS_Date_FROM, umg.ACCESS_DATE_TO, umg.ACTIVE_FLAG,pr.NAME from USER_MGMT umg, USER_DATABASE d,USER_GROUPS ug,PROFILE_GROUP pg,PROFILE pr,USER_ROLES ur,MASTER_ROLES mr where umg.USER_ID=d.USER_ID and ug.USER_GRP_ID=umg.USER_GRP_ID and pg.GROUP_ID=ug.USER_GRP_ID and ur.USER_ID=umg.USER_ID and mr.ROLE_ID=ur.ROLE_ID and pr.PROFILE_ID=pg.PROFILE_ID and pg.PROFILE_ID="+profileId+" and mr.ROLE_DESC='"+padminroledesc+"'and d.USER_ID=ur.USER_ID "+

		" and d.DB_ID="
		+ dbId
		+ " and  umg.DELETE_FLAG = '"
		+ delflag
		+ "' and umg.PROFILE_COMPLETED_FLAG = '"
		+ flag
		+ "' and d.DELETE_FLAG = '" + delflag + "'";

		ps = db.getDBConnection().prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			UserDTO uDto = new UserDTO();

			uDto.setUserId(rs.getLong(1));
			System.out.println("userId" + rs.getLong(1));
			uDto.setUserName(data.CheckDecryptData(rs.getString(2)));

			if (rs.getString(3) != null && !rs.getString(3).equals("")) {
				uDto.setAccessfromDays(Integer.parseInt(data.CheckDecryptData(rs
						.getString(3))));
				day1 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(3))));
			}
			if (rs.getString(4) != null && !rs.getString(4).equals("")) {
				uDto.setAccesstoDays(Integer.parseInt(data.CheckDecryptData(rs
						.getString(4))));

				day2 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(4))));
			}
			System.out.println("day1" + day1 + "day2" + day2);
			uDto.setAccessfromDay(day1);
			uDto.setAccesstoDay(day2);

			if (rs.getString(5) != null && !rs.getString(5).equals("")) {

				uDto.setAccessdateFrom(data.CheckDecryptData(rs.getString(5)));
			}

			if (rs.getString(6) != null && !rs.getString(6).equals("")) {
				uDto.setAccessdateTo(data.CheckDecryptData(rs.getString(6)));
			}
			if (rs.getString(7) != null && !rs.getString(7).equals("")) {
				uDto.setActive(data.CheckDecryptData(rs.getString(7)));
			}
			uDto.setDbId(dbId);
			uDto.setUserProfileName(rs.getString(8));
			users.add(uDto);
		}

	} catch (Exception e) {
		System.out.println("exception "+e);
	}
	return users;

	}


	public List getDBUsers(Long dbId) {
		List users = new ArrayList();
		String day1 = "";
		String day2 = "";
		PreparedStatement ps = null;
		try {
			System.out.println("getDBUsers" + dbId);
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");
			String query1 = "select u.USER_ID,u.USER_NAME,u.ACCESS_DAYS_FROM, u.ACCESS_DAYS_TO, u.ACCESS_Date_FROM, u.ACCESS_DATE_TO, u.ACTIVE_FLAG from USER_MGMT u, USER_DATABASE d where u.USER_ID = d.USER_ID and d.DB_ID="
					+ dbId
					+ " and  u.DELETE_FLAG = '"
					+ delflag
					+ "' and u.PROFILE_COMPLETED_FLAG = '"
					+ flag
					+ "' and d.DELETE_FLAG = '" + delflag + "'";
			System.out.println("getDBUsers:query1:" + query1);
			ps = db.getDBConnection().prepareStatement(query1);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserDTO uDto = new UserDTO();

				uDto.setUserId(rs.getLong(1));
				System.out.println("userId" + rs.getLong(1));
				uDto.setUserName(data.CheckDecryptData(rs.getString(2)));

				if (rs.getString(3) != null && !rs.getString(3).equals("")) {
					uDto.setAccessfromDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(3))));
					day1 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(3))));
				}
				if (rs.getString(4) != null && !rs.getString(4).equals("")) {
					uDto.setAccesstoDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(4))));

					day2 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(4))));
				}
				System.out.println("day1" + day1 + "day2" + day2);
				uDto.setAccessfromDay(day1);
				uDto.setAccesstoDay(day2);

				if (rs.getString(5) != null && !rs.getString(5).equals("")) {

					uDto.setAccessdateFrom(data.CheckDecryptData(rs.getString(5)));
				}

				if (rs.getString(6) != null && !rs.getString(6).equals("")) {
					uDto.setAccessdateTo(data.CheckDecryptData(rs.getString(6)));
				}
				if (rs.getString(7) != null && !rs.getString(7).equals("")) {
					uDto.setActive(data.CheckDecryptData(rs.getString(7)));
				}
				uDto.setDbId(dbId);
				users.add(uDto);
			}

		} catch (Exception e) {
			System.out.println("exception "+e);
		}
		return users;
	}

	public List getProfileUsers(Long profileId, Long dbId) {
		System.out.println("dbId::"+dbId+"profileId::"+profileId);
		List users = new ArrayList();
		String day1 = "";
		String day2 = "";
		PreparedStatement ps = null;
		try {
			System.out.println("getDBUsers" + dbId);
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");
			String query1 = "select distinct u.USER_ID,u.USER_NAME,u.ACCESS_DAYS_FROM, u.ACCESS_DAYS_TO, u.ACCESS_Date_FROM, u.ACCESS_DATE_TO, u.ACTIVE_FLAG from USER_MGMT u, PROFILE_GROUP pg, USER_DATABASE d where u.USER_ID = d.USER_ID and u.USER_ID = pg.USER_ID and pg.GROUP_ID=u.USER_GRP_ID and d.DB_ID="
					+ dbId+ " and pg.PROFILE_ID='"+profileId+ "' and  u.DELETE_FLAG = '"
					+ delflag
					+ "' and u.PROFILE_COMPLETED_FLAG = '"
					+ flag
					+ "' and d.DELETE_FLAG = '" + delflag + "'and pg.ADMIN_FLAG!='Y'";
			System.out.println("getDBUsers:query1:" + query1);
			ps = db.getDBConnection().prepareStatement(query1);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserDTO uDto = new UserDTO();
				Long userId = rs.getLong(1);
				uDto.setUserId(rs.getLong(1));
				Long padmin = getProfileAdmin(profileId);
				if(padmin == userId){
					uDto.setIsProfileAdmin(true);
					System.out.println("getProfileUsers>>ISProfileAdmin");
				}
				System.out.println("userId" + rs.getLong(1));
				uDto.setUserName(data.CheckDecryptData(rs.getString(2)));

				if (rs.getString(3) != null && !rs.getString(3).equals("")) {
					uDto.setAccessfromDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(3))));
					day1 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(3))));
				}
				if (rs.getString(4) != null && !rs.getString(4).equals("")) {
					uDto.setAccesstoDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(4))));

					day2 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(4))));
				}
				System.out.println("day1" + day1 + "day2" + day2);
				uDto.setAccessfromDay(day1);
				uDto.setAccesstoDay(day2);

				if (rs.getString(5) != null && !rs.getString(5).equals("")) {

					uDto.setAccessdateFrom(data.CheckDecryptData(rs.getString(5)));
				}

				if (rs.getString(6) != null && !rs.getString(6).equals("")) {
					uDto.setAccessdateTo(data.CheckDecryptData(rs.getString(6)));
				}
				if (rs.getString(7) != null && !rs.getString(7).equals("")) {
					uDto.setActive(data.CheckDecryptData(rs.getString(7)));
				}
				uDto.setDbId(dbId);
				users.add(uDto);
			}

		} catch (Exception e) {
			System.out.println("exception "+e);
		}
		return users;
	}


	public List getProfileAdminUsers(Long profileId, Long dbId) {
		System.out.println("dbId::"+dbId+"profileId::"+profileId);
		List users = new ArrayList();
		String day1 = "";
		String day2 = "";
		PreparedStatement ps = null;
		try {
			System.out.println("getDBUsers" + dbId);
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");
			String query1 = "select distinct u.USER_ID,u.USER_NAME,u.ACCESS_DAYS_FROM, u.ACCESS_DAYS_TO, u.ACCESS_Date_FROM, u.ACCESS_DATE_TO, u.ACTIVE_FLAG from USER_MGMT u, PROFILE_GROUP pg, USER_DATABASE d where u.USER_ID = d.USER_ID and u.USER_ID = pg.USER_ID and pg.GROUP_ID=u.USER_GRP_ID and d.DB_ID="
					+ dbId+ " and pg.PROFILE_ID='"+profileId+ "' and  u.DELETE_FLAG = '"
					+ delflag
					+ "' and u.PROFILE_COMPLETED_FLAG = '"
					+ flag
					+ "' and d.DELETE_FLAG = '" + delflag + "' and pg.ADMIN_FLAG='Y'";
			System.out.println("getDBUsers:query1:" + query1);
			ps = db.getDBConnection().prepareStatement(query1);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserDTO uDto = new UserDTO();
				Long userId = rs.getLong(1);
				uDto.setUserId(rs.getLong(1));
				Long padmin = getProfileAdmin(profileId);
				if(padmin == userId){
					uDto.setIsProfileAdmin(true);
					System.out.println("getProfileUsers>>ISProfileAdmin");
				}
				System.out.println("userId" + rs.getLong(1));
				uDto.setUserName(data.CheckDecryptData(rs.getString(2)));

				if (rs.getString(3) != null && !rs.getString(3).equals("")) {
					uDto.setAccessfromDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(3))));
					day1 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(3))));
				}
				if (rs.getString(4) != null && !rs.getString(4).equals("")) {
					uDto.setAccesstoDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(4))));

					day2 = GetDay.returnday(Integer.parseInt(data.CheckDecryptData(rs.getString(4))));
				}
				System.out.println("day1" + day1 + "day2" + day2);
				uDto.setAccessfromDay(day1);
				uDto.setAccesstoDay(day2);

				if (rs.getString(5) != null && !rs.getString(5).equals("")) {

					uDto.setAccessdateFrom(data.CheckDecryptData(rs.getString(5)));
				}

				if (rs.getString(6) != null && !rs.getString(6).equals("")) {
					uDto.setAccessdateTo(data.CheckDecryptData(rs.getString(6)));
				}
				if (rs.getString(7) != null && !rs.getString(7).equals("")) {
					uDto.setActive(data.CheckDecryptData(rs.getString(7)));
				}
				uDto.setDbId(dbId);
				users.add(uDto);
			}

		} catch (Exception e) {
			System.out.println("exception "+e);
		}
		return users;
	}

	public boolean deleteUser(Long userId) {
		boolean b = false;
		try {
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("Y");
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String crDate = data.CheckEncryptData(s);
			List users = getUser(userId);
			String hash = "";
			for (Iterator iter = users.iterator(); iter.hasNext();) {
				UserDTO dto = (UserDTO) iter.next();

				hash = AeSimpleSHA1.SHA1(dto.getUserName() + ","
						+ dto.getGroupId() + "," + dto.getActive() + ","
						+ dto.getCurrYear() + "," + dto.getAccessdateFrom()
						+ "," + dto.getAccessdateTo() + ","
						+ dto.getAccessfromDays() + "," + dto.getAccesstoDays()
						+ "," + crDate + "," + flag + "," + delflag);

			}
			String query = "UPDATE USER_MGMT SET DELETE_DATE='" + crDate
					+ "',HASH_ID='" + hash + "',DELETE_FLAG='" + delflag
					+ "' where PROFILE_COMPLETED_FLAG='" + flag
					+ "' and USER_ID=" + userId + ";";

			Statement stmt = db.getDBConnection().createStatement();
			stmt.execute(query);
			updateProfileGroup(userId);
			updateUserIntegrity(CommonConstants.USER_INTEGRITY_CHECK,
					"USER_ID", hash, hash, userId, "yes");

			b = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}

	public void updateProfileGroup(Long userId) {

		try {
			/* Nithu Alexander: 11-Aug-2015
			 * Changing Delete Flag from 'N' to 'Y' to update PROFILE_GROUP table correctly
			 * */
			String delflag = data.CheckEncryptData("Y");
			String query = "UPDATE PROFILE_GROUP SET DELETE_FLAG='" + delflag
					+ "' where USER_ID=" + userId;
			Statement stmt = db.getDBConnection().createStatement();
			stmt.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public boolean deleteProfile(Long profileId) {
		boolean b = false;
		try {
			String delflag = data.CheckEncryptData("Y");
			SimpleDateFormat formatter = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String delDate = data.CheckEncryptData(s);
			String query = "UPDATE PROFILE SET DELETE_DATE='" + delDate
					+ "', DELETE_FLAG='" + delflag
					+ "' where PROFILE_ID=" + profileId;

			Statement stmt = db.getDBConnection().createStatement();
			stmt.execute(query);
			Long user_id = getProfileAdmin(profileId);
			boolean b1 = deleteUser(user_id);
			if(b1 == true){
				b = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}

	public UserDTO getUserName(Long userId) {
		UserDTO dto = new UserDTO();
		try {
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");

			String query = "select u.USER_NAME,ug.USER_GRP_ID from  USER_MGMT u, USER_GROUPS ug where u.USER_GRP_ID = ug.USER_GRP_ID and u.DELETE_FLAG = '"
					+ delflag
					+ "' and u.PROFILE_COMPLETED_FLAG = '"
					+ flag
					+ "' and u.USER_ID=" + userId;
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				dto.setUserName(rs.getString(1));
				dto.setGroupId(rs.getLong(2));

			}
		} catch (Exception e) {

		}
		return dto;
	}


	public boolean isDrnExists(String table,String drn){
		boolean isDrnExists=false;

		String sql="select DRN from "+table+" where DRN='"+drn+"'";

		System.out.println("DRN fetching query is"+sql);
		try{
			Statement	ps = db.getDBConnection().createStatement();
			ResultSet rs = ps.executeQuery(sql);
			while (rs.next()) {
				isDrnExists=true;
			}

		}catch(Exception sqe){
			System.out.println("Error occured ----------->"+sqe);
		}

		System.out.println("DRN exists Flag Value"+isDrnExists);
		return isDrnExists;

	}






	public List<Long> getProfileIdForGroups(Long userId) {
		System.out.println("inside getProfileIdForGroups::");
		List<Long> pid = new ArrayList<Long>();
		try {
			String delflag = data.CheckEncryptData("N");
			String sql = "select  distinct PROFILE_ID from  PROFILE_GROUP where ADMIN_FLAG='Y' and USER_ID="+ userId + " and DELETE_FLAG='"+delflag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				pid.add(rs.getLong(1));
			}
			System.out.println("getProfileIdForGroups::"+pid.size());
		} catch (Exception e) {
			System.out.println(e);
		}

		return pid;
	}

	public Long getProfileIdForProfileadmin(Long userId) {
		System.out.println("Inside getProfileIdForProfileadmin");
		Long pid = 0L;
		try {
			String delflag = data.CheckEncryptData("N");
			String sql = "select  distinct PROFILE_ID from  PROFILE_GROUP where ADMIN_FLAG='Y' and USER_ID="+ userId + "  and DELETE_FLAG='"+delflag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				pid=rs.getLong(1);
			}
			System.out.println("Returning from getProfileIdForProfileadmin");
		} catch (Exception e) {
			System.out.println(e);
		}

		return pid;
	}

	public Long getProfileAdmin(Long profileId) {

		Long padminId = 0L;
		try {
			String delflag = data.CheckEncryptData("N");
			String sql = "select  distinct USER_ID from  PROFILE_GROUP where ADMIN_FLAG='Y' and PROFILE_ID="+ profileId+"  and DELETE_FLAG='"+delflag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				padminId = rs.getLong(1);
			}
			System.out.println("getProfileAdmin::"+padminId);
		} catch (Exception e) {
			System.out.println(e);
		}

		return padminId;
	}


	public Long getProfileAdminUserId(Long userId) {

		Long padminId = 0L;
		try {
			String delflag = data.CheckEncryptData("N");
			String sql = "select distinct(user_id) from profile_group where profile_id in (select distinct(pg.PROFILE_ID) from USER_MGMT umg,USER_GROUPS ug,PROFILE_GROUP pg where umg.USER_GRP_ID=ug.USER_GRP_ID and pg.GROUP_ID=ug.USER_GRP_ID and pg.USER_ID=umg.USER_ID and umg.USER_ID="+userId+") and ADMIN_FLAG='Y'";
			System.out.println("################################## getProfileAdminUserId query : " + sql);
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				padminId = rs.getLong(1);
			}
			System.out.println("getProfileAdminUserId::"+padminId);
		} catch (Exception e) {
			System.out.println(e);
		}

		return padminId;
	}


	public String getProfileAdminUserName(Long profileId) {
		System.out.println("Inside getProfileAdminUserName");
		String padmin = "";
		try {
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");

			String sql = "select  distinct USER_NAME from  PROFILE_GROUP pg, USER_MGMT um where um.USER_ID = pg.USER_ID and " +
					"um.DELETE_FLAG = '"+ delflag+ "' and um.PROFILE_COMPLETED_FLAG = '"+ flag+"' and pg.ADMIN_FLAG='Y' and pg.PROFILE_ID="+ profileId+"  and pg.DELETE_FLAG='"+delflag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				padmin = data.CheckDecryptData(rs.getString(1));
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return padmin;
	}


	public String[] getGroupsOfProfile(Long profileId)
	{
		List<String> groups=new ArrayList<String>();

		System.out.println("Inside getgetGroupsOfProfile");
		String padmin = "";
		try {
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");

			String sql = "select  distinct GROUP_NAME from  PROFILE_GROUP pg WHERE  pg.PROFILE_ID="+ profileId+"  and pg.DELETE_FLAG='"+delflag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				groups.add((rs.getString(1)));
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return (String[])groups.toArray();

	}



	public String getProfileName(Long profileId) {
		String proname = "";
		try {
			System.out.println("Inside getProfileName::"+profileId);
			String sql = "select  distinct NAME from  PROFILE where profile_id="
					+ profileId;
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				proname = rs.getString(1);

			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return proname;

	}
	public Long getUserIDFromUID(Long UID) {
		Long UserID = 0L;
		try {
			System.out.println("Inside getUserIDFromUID::"+UID);
			String sql = "select  distinct USER_ID from  USER_DB_INDEX where USER_INDEX_ID="
					+ UID;
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				UserID = rs.getLong(1);

			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return UserID;

	}

	public String getIndexNameDBIDFromUID(Long UID) {
		String idxName = "";
		try {
			System.out.println("Inside getIndexNameFromUID::"+UID);
			String sql = "select  distinct INDEX_NAME,DB_ID from  USER_DB_INDEX where USER_INDEX_ID="
					+ UID;
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				idxName = rs.getString(1)+">"+rs.getLong(2);

			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return idxName;

	}

	public List<UserDTO> DbBelongsToProfile(Long profileId,String dbName)
	{
		String sql="";
		List<UserDTO> userList=new ArrayList<UserDTO>();
		boolean isDbExist=false;
		//System.out.println("herexxxxxxxyyyyyyyyyyy........."+profileId+"xxxx"+dbName);
		try
		{
			sql="select cd.DB_NAME,cd.DB_DESC from PROFILE_GROUP pg,USER_GROUPS ug,USER_MGMT umg,CSR_DATABASE cd,USER_DATABASE ud where ug.USER_GRP_ID=pg.GROUP_ID and umg.USER_GRP_ID=ug.USER_GRP_ID and umg.USER_ID=ud.USER_ID and ud.DB_ID=cd.DB_ID and pg.PROFILE_ID="+profileId+" and cd.DB_NAME='"+dbName+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				UserDTO dto = new UserDTO();
				dto.setDbName(rs.getString(1));

				dto.setDbDesc(rs.getString(2));
				userList.add(dto);
			}
		}
		catch(Exception e)
		{
			System.out.println("exceptrfiom"+e);
		}

		return userList;
	}


	public boolean isDbBelongsToProfile(Long profileId,String dbName)
	{
		String sql="";
		List<UserDTO> userList=new ArrayList<UserDTO>();
		boolean isDbExist=false;
		System.out.println("herexxxxxxxyyyyyyyyyyy........."+profileId+"xxxx"+dbName);
		try
		{	
			sql="select cd.DB_NAME,cd.DB_DESC from PROFILE_GROUP pg,CSR_DATABASE cd where  cd.DB_ID=pg.DB_ID and pg.PROFILE_ID="+profileId+" and cd.DB_NAME='"+dbName+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				isDbExist=true;
			}
		}
		catch(Exception e)
		{
			System.out.println("exceptrfiom"+e);
		}

		return isDbExist;
	}


	public List<UserDTO> getUsersOfProfileAdmin(Long profileId)
	{
		//System.out.println("hereeeeeeeeeeeeeeeeeeeeeeeeeeeeee");

		String padminroledesc = CommonConstants.ROLE_DESC_PROJECTADMIN;
		String sql = "select umg1.USER_ID FROM USER_MGMT umg1 WHERE umg1.USER_GRP_ID IN (SELECT umg.USER_GRP_ID from PROFILE_GROUP pg,USER_GROUPS ug,USER_MGMT umg,USER_ROLES ur,MASTER_ROLES mr where ug.USER_GRP_ID=pg.GROUP_ID and umg.USER_GRP_ID=ug.USER_GRP_ID and ur.USER_ID=umg.USER_ID and mr.ROLE_ID=ur.ROLE_ID and pg.PROFILE_ID="
				+ profileId + " and mr.ROLE_DESC='" + padminroledesc + "')";
		List<UserDTO> users = new ArrayList<UserDTO>();

		try {
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				UserDTO u=new UserDTO();

				Long uid=rs.getLong(1);
				//System.out.println("hereeeeeeeeeeeeeeeeeeeeeeeeeeeedddddddddddee");

				u.setUserId(uid);

				users.add(u);
			}
		} catch (Exception e) {
			System.out.println("exception"+e);
		}

		return users;
	}


	public Long[] getRolesOfProfileAdmin(Long profileId) {
		String padminroledesc = CommonConstants.ROLE_DESC_PROJECTADMIN;
		String sql = "select ur.ROLE_ID from PROFILE_GROUP pg,USER_GROUPS ug,USER_MGMT umg,USER_ROLES ur,MASTER_ROLES mr where ug.USER_GRP_ID=pg.GROUP_ID and umg.USER_GRP_ID=ug.USER_GRP_ID and ur.USER_ID=umg.USER_ID and mr.ROLE_ID=ur.ROLE_ID and pg.PROFILE_ID="
				+ profileId + " and mr.ROLE_DESC<>'" + padminroledesc + "'";
		List<Long> roles = new ArrayList<Long>();

		try {
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				roles.add(rs.getLong((1)));

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		Long roleArray[] = new Long[roles.size()];
		int i = 0;
		for (Long role : roles) {
			roleArray[i++] = role;
		}

		return roleArray;
	}

	public Long[] getAllProfiles(Long userId) {
		List<Long> profileIds = new ArrayList<Long>();
		try {
			String sql = "select PROFILE_ID from PROFILE_GROUP WHERE ADMIN_FLAG='Y' and USER_ID='"+ userId + "'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				profileIds.add(Long.parseLong(rs.getString(1)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Long profileIdsArray[] = new Long[profileIds.size()];
		int i = 0;
		for (Long profileId : profileIds) {
			profileIdsArray[i++] = profileId;
		}
		System.out.println("profilesIDs:::===>"+profileIdsArray.length);
		return profileIdsArray;
	}

	public String[] getProfileGroups(Long profileId) {

		List<String> groups = new ArrayList<String>();

		try {

			String sql = "select distinct ug.USER_GROUP from PROFILE_GROUP pg, USER_GROUPS  ug WHERE ug.USER_GRP_ID=pg.GROUP_ID  and  pg.GROUP_ID IN (select GROUP_ID FROM PROFILE_GROUP where pg.PROFILE_ID='"
					+ profileId + "')";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				groups.add(data.CheckDecryptData(rs.getString(1)));

			}
		} catch (Exception e) {

		}
		String gps[] = new String[groups.size()];
		int i = 0;
		for (String pr : groups) {
			gps[i] = pr;
			i++;
		}

		return gps;

	}

	public List<Long> getProfiledbIds(Long profileId) {
		System.out.println("Inside getProfiledbIds");
		List<Long> dbIds = new ArrayList<Long>();
		try {
			String flag = data.CheckEncryptData("N");
			String sql = "select distinct DB_ID from PROFILE_GROUP WHERE PROFILE_ID="
					+ profileId+" and ADMIN_FLAG = 'Y' and DELETE_FLAG = '"+flag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				dbIds.add(rs.getLong(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dbIds;
	}

	public List<Long> getProfileuserdbIds(Long profileId) {
		System.out.println("Inside getProfileuserdbIds");
		List<Long> dbIds = new ArrayList<Long>();
		try {
			String flag = data.CheckEncryptData("N");
			String sql = "select distinct DB_ID from PROFILE_GROUP WHERE PROFILE_ID="
					+ profileId+" and ADMIN_FLAG = 'N' and DELETE_FLAG ='"+flag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				dbIds.add(rs.getLong(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dbIds;
	}

	public List<String> getGroups(Long profileId) {
		System.out.println("Inside getGroups");
		List<String> groups = new ArrayList<String>();
		try {
			String flag = data.CheckEncryptData("N");
			String sql = "select distinct GROUP_NAME from PROFILE_GROUP WHERE PROFILE_ID="
					+ profileId+" and ADMIN_FLAG = 'N' and DELETE_FLAG='"+flag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				groups.add(rs.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return groups;
	}

	public List<String> getpGroups(Long profileId) {
		System.out.println("Inside getGroups");
		List<String> groups = new ArrayList<String>();
		try {
			String flag = data.CheckEncryptData("N");
			String sql = "select distinct GROUP_NAME from PROFILE_GROUP WHERE PROFILE_ID="
					+ profileId+" and ADMIN_FLAG = 'Y' and DELETE_FLAG = '"+flag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				groups.add(rs.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return groups;
	}

	public void saveUserActions(Long userId, Long actionId) {

		try {
			System.out.println("saveUserActions" + actionId);
			if (actionId != 0) {
				String flag = data.CheckEncryptData("N");
				boolean b = getUserActions(userId, actionId);
				if (!b) {
					String hash = AeSimpleSHA1.SHA1(userId + "," + actionId);
					String query = "INSERT INTO USER_ACTION(USER_ID,HASH_ID,DELETE_FLAG,ACTION_ID) VALUES("
							+ "?"
							+ ","
							+ "?"
							+ ","
							+ "?"
							+ ","
							+ "?" + ");";
					PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
					stmt.setLong(1, userId);
					stmt.setString(2, hash);
					stmt.setString(3, flag);
					stmt.setLong(4, actionId);
					stmt.executeUpdate();
				} else {
					String hash = AeSimpleSHA1.SHA1(userId + "," + actionId);
					String query = "UPDATE USER_ACTION SET DELETE_FLAG="
							+"?" + " where USER_ID=" + "?"
							+ " and ACTION_ID=" + "?";
					PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
					stmt.setString(1, flag);
					stmt.setLong(2, userId);
					stmt.setLong(3, actionId);

					stmt.executeUpdate();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void DeleteUserActions(Long userId) {

		try {
			System.out.println("DeleteUserActions" + userId);

			String flag = data.CheckEncryptData("Y");

			String hash = AeSimpleSHA1.SHA1(userId + "," + userId);
			String query = "Update USER_ACTION SET DELETE_FLAG=" + "?"
					+ " where USER_ID=" + "?";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
			stmt.setString(1, flag);
			stmt.setLong(2, userId);

			stmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Long saveUserIndex(String index, Long dbId, Long userId, String saved) {
		Long id = 0L;
		try {
			System.out.println("saveUserIndex > "+userId+" > " +dbId+" > "+ index);
			String saveflag = data.CheckEncryptData(saved);
			String hash = AeSimpleSHA1.SHA1(userId + ","+dbId+"," + index);
			String query = "INSERT INTO USER_DB_INDEX(USER_ID,HASH_ID,INDEX_NAME,DB_ID,SAVED) VALUES("
					+ "?"
					+ ","
					+ "?"
					+ ","
					+ ""+"?"+""
					+ ","
					+ "?"
					+ ","
					+ "?" + ")";

			PreparedStatement stmt = db.getDBConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, userId);
			stmt.setString(2, hash);
			stmt.setString(3, index);
			stmt.setLong(4, dbId);
			stmt.setString(5, saveflag);

			stmt.executeUpdate();


			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				System.out.println("userIndexid created:" + rs.getLong(1));
				id = rs.getLong(1);
				return id;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public Long saveUserColIndex(String index, Long dbId, Long userId, String saved) {
		Long id = 0L;
		try {
			System.out.println("saveUserIndex > "+userId+" > " +dbId+" > "+ index);
			String saveflag = data.CheckEncryptData(saved);
			String hash = AeSimpleSHA1.SHA1(userId + ","+dbId+"," + index);
			String query = "INSERT INTO USER_SER_RES_INDEX(USER_ID,HASH_ID,INDEX_NAME,DB_ID,SAVED) VALUES("
					+ "?"
					+ ","
					+"?"
					+ ","
					+ ""+"?"+""
					+ ","
					+ "?"
					+ ","
					+ "?" + ")";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, userId);
			stmt.setString(2, hash);
			stmt.setString(3, index);
			stmt.setLong(4, dbId);
			stmt.setString(5, saveflag);

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				System.out.println("usersearchIndexid created:" + rs.getLong(1));
				id = rs.getLong(1);
				return id;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;

	}

	public Long updateUserColumnIndex(String index, Long dbId, Long userId, String saved) {
		Long id = 0L;
		try {
			System.out.println("updateUserIndex > "+userId+" > " +dbId+" > "+ index);
			String saveflag = data.CheckEncryptData(saved);
			String hash = AeSimpleSHA1.SHA1(userId + ","+dbId+"," + index);
			String query = "UPDATE USER_SER_RES_INDEX SET SAVED=" + "?"
					+ " where USER_ID=" + "?" + " and INDEX_NAME= " + "?" +" and DB_ID="+"?"+";";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query);

			stmt.setString(1, saveflag);
			stmt.setLong(2, userId);
			stmt.setString(3, index);
			stmt.setLong(4, dbId);

			stmt.executeUpdate();
			id = 1L;
			return id;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;

	}

	public Long getColumnIndexUsers(String index, Long dbId, Long userId, String saved) {

		Long b = 0L;
		PreparedStatement ps = null;
		try {
			System.out.println("getColumnIndexUsers > "+userId+" > " +dbId+" > "+ index);
			String saveflag = data.CheckEncryptData(saved);
			String query = "select ui.SER_RES_INDEX_ID from USER_SER_RES_INDEX ui where ui.INDEX_NAME ='"
					+ index + "' and ui.USER_ID=" + userId+ "  and ui.DB_ID=" + dbId+" and ui.SAVED='"+saveflag+"'";
			ps = db.getDBConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				b = rs.getLong(1);
				System.out.println("getuseindexid" + b);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;

	}

	public Long updateUserIndex(String index, Long dbId, Long userId, String saved) {
		Long id = 0L;
		try {
			System.out.println("updateUserIndex > "+userId+" > " +dbId+" > "+ index);
			String saveflag = data.CheckEncryptData(saved);
			String hash = AeSimpleSHA1.SHA1(userId + ","+dbId+"," + index);
			String query = "UPDATE USER_DB_INDEX SET SAVED=" + "?"
					+ " where USER_ID=" + "?" + " and INDEX_NAME= " + "?" +" and DB_ID="+"?"+";";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
			stmt.setString(1, saveflag);
			stmt.setLong(2, userId);
			stmt.setString(3, index);
			stmt.setLong(4, dbId);


			stmt.executeUpdate();
			id = 1L;
			return id;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public Long updateProfileDbIndex(Long profileId, Long dbId, String dbName, Long Index,String idxflag) {
		Long id = 0L;
		try {
			System.out.println("Inside updateProfileDbIndex");
			String indexFlag = data.CheckEncryptData(idxflag);
			String query = "UPDATE CSR_DBPROFILES SET INDEX_FLAG=" + "?"
					+"  where PROFILE_ID="+"?"+" and DB_ID="+ "?" +" and INDEX_ID="+ "?";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
			stmt.setString(1, indexFlag);
			stmt.setLong(2, profileId);
			stmt.setLong(3, dbId);
			stmt.setLong(4, Index);

			stmt.executeUpdate();
			id = 1L;
			System.out.println("Returning from updateProfileDbIndex");
			return id;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public void removeprofileIndexes(Long profileId) {
		try {
			System.out.println("Inside removeprofileIndexes");
			String delFlag = data.CheckEncryptData("Y");
			String query = "UPDATE CSR_DBPROFILES SET DELETE_FLAG=" + "?"
					+"  where PROFILE_ID="+"?";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
			stmt.setString(1, delFlag);
			stmt.setLong(2, profileId);

			stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Long checkprofileDbIdx(Long ProfileId, Long dbId,Long idxId)throws Exception {
		Long indexId = 0L;
		System.out.println("Inside checkprofileDbIdx");
		String delflag = data.CheckEncryptData("N");
		String query = "SELECT INDEX_ID from CSR_DBPROFILES WHERE DB_ID ="+dbId
				+" and PROFILE_ID = "+ProfileId+" and INDEX_ID="+idxId+" and DELETE_FLAG='"+delflag+"'";
		Statement stmt = db.getDBConnection().createStatement();
		ResultSet rs = stmt.executeQuery(query);
		while (rs.next()) {
			indexId = rs.getLong(1);
		}
		System.out.println("Returning from checkprofileDbIdx");
		return indexId;
	}

	public void insertProfileDbIndex(Long profileId, Long dbId, String dbName, Long IndexId, String IndexName, String idxflag) {
		try {
			System.out.println("Inside insertProfileDbIndex");
			String indexFlag = data.CheckEncryptData(idxflag);
			String deleteFlag = data.CheckEncryptData("N");
			String query = "INSERT INTO CSR_DBPROFILES(PROFILE_ID, DB_ID,DB_NAME,INDEX_ID,INDEX_NAME,INDEX_FLAG,DELETE_FLAG)VALUES (?,?,?,?,?,?,?)";
			PreparedStatement pstmt = db.getDBConnection().prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, profileId);
			pstmt.setLong(2, dbId);
			pstmt.setString(3, dbName);
			pstmt.setLong(4, IndexId);
			pstmt.setString(5, IndexName);
			pstmt.setString(6, indexFlag);
			pstmt.setString(7, deleteFlag);

			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			System.out.println("Returning from insertProfileDbIndex");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List getIndexUser(Long id, Long dbId, String desc) {
		System.out.println("Prasanna??????????????"+id+"---"+dbId+"---"+desc);
		List index = new ArrayList();
		try {
			System.out.println(id + ":getUserIndex:" + id);
			String query = "";
			String flag = data.CheckEncryptData("Y");
			if (id != 0 && desc.equals("user")) {
				query = "select di.DB_ID,ui.INDEX_ID,di.INDEX_NAME,u.USER_ID,di.INDEX_DESC from USER_MGMT u, USER_DB_INDEX ui, DATABASE_INDEX di where  u.USER_ID= ui.USER_ID and ui.INDEX_ID = di.INDEX_ID and ui.SAVED='"
						+ flag + "' and u.USER_ID=" + id;

			} else if (id != 0 && desc.equals("index")) {
				query = "select di.DB_ID,di.INDEX_ID,di.INDEX_NAME,di.INDEX_DESC from  DATABASE_INDEX di where  di.INDEX_ID="
						+ id;

			} else if (id != 0 && dbId != 0 && desc.equals("search")) {
				//query = "select di.DB_ID,ui.INDEX_ID,di.INDEX_NAME,u.USER_ID,c.DB_NAME,di.INDEX_DESC from USER_MGMT u, USER_DB_INDEX ui, DATABASE_INDEX di,CSR_DATABASE c where u.USER_ID= ui.USER_ID and ui.INDEX_ID = di.INDEX_ID and u.USER_ID="
				//	+ id + " and c.DB_ID=di.DB_ID and di.DB_ID=" + dbId;

				query = "select di.DB_ID,di.INDEX_ID,di.INDEX_NAME,ui.USER_ID,c.DB_NAME,di.INDEX_DESC from USER_DB_INDEX ui,DATABASE_INDEX di,CSR_DATABASE c where c.DB_ID=di.DB_ID and ui.INDEX_ID = di.INDEX_ID"
						+" and di.DB_ID="+dbId+" and ui.USER_ID = "+id+" and ui.SAVED = '"+flag+"'";

			}
			if (query != "") {
				// query =
				// "select di.DB_ID,ui.INDEX_ID,di.INDEX_NAME,u.USER_ID from USER_MGMT u, USER_DB_INDEX ui, DATABASE_INDEX di where u.USER_ID= ui.USER_ID and ui.INDEX_ID = di.INDEX_ID and u.USER_ID="+id;
				Statement stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
					UserDTO dto = new UserDTO();
					dto.setDbId(rs.getLong(1));
					dto.setIndexId(rs.getLong(2));
					dto.setIndexName(rs.getString(3));

					if (id != 0 && desc.equals("user")) {
						dto.setUserId(rs.getLong(4));
						dto.setIndexDesc(rs.getString(5));
					} else if (id != 0 && dbId != 0 && desc.equals("search")) {
						dto.setUserId(rs.getLong(4));
						dto.setIndexName(rs.getString(3));
						dto.setDbName(rs.getString(5));
						dto.setIndexDesc(rs.getString(6));
					}

					// String str = "Index_"+rs.getLong(4)+"_"+rs.getLong(2);
					index.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return index;
	}

	public List getUserIndex(Long dbId, String saved) {
		List<String> index = new ArrayList<String>();
		PreparedStatement ps = null;
		try {
			System.out.println("getUserIndex" + dbId);
			String query = "";
			String saveflag = data.CheckEncryptData(saved);
			String deleteflag = data.CheckEncryptData("Y");
			if (dbId != 0) {

				query = "select ui.DB_ID,ui.INDEX_NAME,ui.USER_ID,ui.USER_INDEX_ID from USER_DB_INDEX ui, USER_MGMT um where um.USER_ID=ui.USER_ID and um.DELETE_FLAG !='"
						+deleteflag+"' and ui.SAVED='"+ saveflag + "' and ui.DB_ID=" + dbId;
				ps = db.getDBConnection().prepareStatement(query);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					String str = "Index_" + rs.getLong(3) + "_"+ rs.getLong(1) + "_" + rs.getString(2);
					index.add(str);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return index;
	}

	public List getUserIndex(Long dbId, Long userId, String saved) {
		List<String> index = new ArrayList<String>();
		PreparedStatement ps = null;
		try {
			System.out.println("getUserIndex" + dbId);
			System.out.println("getUserIndex" + userId);
			String query = "";
			String flag = data.CheckEncryptData("Y");
			String saveflag = data.CheckEncryptData(saved);
			if (dbId != 0) {

				query = "select di.DB_ID,ui.INDEX_ID,di.INDEX_NAME,u.USER_ID,ui.USER_INDEX_ID from USER_MGMT u, USER_DB_INDEX ui, DATABASE_INDEX di where u.USER_ID= ui.USER_ID and ui.INDEX_ID = di.INDEX_ID and ui.SAVED='"
						+ flag + "' and di.DB_ID=" + dbId+" and ui.USER_ID="+userId;
				ps = db.getDBConnection().prepareStatement(query);
				// Statement stmt = db.getDBConnection().createStatement();
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {

					String str = "Index_" + rs.getLong(4) + "_" + rs.getLong(2);
					index.add(str);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return index;
	}

	public List getUserColumnIndex(Long dbId, String saved) {

		List<String> index = new ArrayList<String>();
		PreparedStatement ps = null;
		try {
			System.out.println("getUserColumnIndex" + dbId);
			String query = "";
			String saveflag = data.CheckEncryptData(saved);
			if (dbId != 0) {

				query = "select ui.DB_ID,ui.INDEX_NAME,ui.USER_ID,ui.SER_RES_INDEX_ID from USER_SER_RES_INDEX ui where ui.SAVED='"
						+ saveflag + "' and ui.DB_ID=" + dbId;
				ps = db.getDBConnection().prepareStatement(query);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					String str = "Index_" + rs.getLong(3) + "_"+ rs.getLong(1) + "_" + rs.getString(2);
					index.add(str);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return index;


	}

	public List<String> getUserColumnIndex(Long dbId,Long userId, String saved) {
		List<String> index = new ArrayList<String>();
		try {
			System.out.println("getUserColumnIndex-- dbid" + dbId);
			System.out.println("getUserColumnIndex-- userid" + userId);
			String query = "";
			String flag = data.CheckEncryptData("Y");
			String saveflag = data.CheckEncryptData(saved);
			if (dbId != 0) {

				query = "select distinct ui.DB_ID,di.INDEX_ID,di.INDEX_DESC,ui.INDEX_NAME,ui.USER_ID from USER_SER_RES_INDEX ui,VP_DATABASE_INDEX di where ui.SAVED='"
						+ flag + "' and ui.DB_ID = di.DB_ID and ui.INDEX_NAME = di.INDEX_NAME and ui.DB_ID=" + dbId+ " and ui.USER_ID=" + userId;

				Statement stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
					String str = "Index_" + rs.getLong(2) + "_" + rs.getString(3)+ "_" + rs.getString(4);
					index.add(str);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return index;
	}


	public List<String> getProfileDbIndex(Long dbId, Long profileId)throws Exception {
		List<String> index = new ArrayList<String>();
		PreparedStatement ps = null;
		System.out.println("Inside getProfileDbIndex");
		String flag = data.CheckEncryptData("Y");
		String query = "";
		if (dbId != 0) {
			query = "select DB_ID,INDEX_ID,INDEX_NAME from CSR_DBPROFILES where DB_ID = "+dbId
					+" and PROFILE_ID = "+profileId+" and INDEX_FLAG = '"+flag+"'";
			ps = db.getDBConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String str = "Index_"+ rs.getLong(2);
				index.add(str);
			}
		}
		//System.out.println("Returning from getProfileDbIndex");
		return index;
	}

	public List getUsersColumnIndex(Long userId, Long dbId, String saved) {
		List index = new ArrayList();
		try {
			System.out.println("getUserColumnIndex dbid" + dbId);
			System.out.println("getUserColumnIndex userid" + userId);
			String query = "";
			String flag = data.CheckEncryptData("Y");
			String saveflag = data.CheckEncryptData(saved);
			if (dbId != 0) {

				query = "select di.DB_ID,ui.INDEX_ID,di.INDEX_NAME,di.INDEX_DESC,u.USER_ID,ui.SER_RES_INDEX_ID from USER_MGMT u, USER_SER_RES_INDEX ui, DATABASE_INDEX di where u.USER_ID= ui.USER_ID and ui.INDEX_ID = di.INDEX_ID and ui.SAVED='"
						+ flag
						+ "' and ui.USER_ID="
						+ userId
						+ " and di.DB_ID=" + dbId;

				Statement stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
					UserDTO dto = new UserDTO();
					dto.setDbId(rs.getLong(1));
					dto.setIndexId(rs.getLong(2));
					dto.setIndexName(rs.getString(3));
					dto.setIndexDesc(rs.getString(4));
					dto.setUserId(rs.getLong(5));
					// String str = data.decryptData(rs.getString(3));
					dto.setSerResIndexId(rs.getLong(6));
					index.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return index;
	}


	public List<String> getProfiles()
	{
		List<String> profileNameList=new ArrayList<String>();
		String query="select p.NAME from Profile p";
		try{
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);


			while (rs.next()) {
				profileNameList.add(rs.getString(1));
			}
		}
		catch (Exception ee) {
			ee.printStackTrace();
		}

		return profileNameList;
	}

	public List<String> getdprofiles()
	{
		List<String> profileNameList=new ArrayList<String>();
		String query="select p.PROFILE_NAME from PROFILE_METADATA p";
		try{
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);


			while (rs.next()) {
				profileNameList.add(rs.getString(1));
			}
		}
		catch (Exception ee) {
			ee.printStackTrace();
		}

		return profileNameList;
	}



	public List<UserDTO> getProfileGroups()
	{

		List<UserDTO> profileNameList=new ArrayList<UserDTO>();
		//String query="select p.NAME,p.group_id from Profile p,Profile_GROUP pg,USER_GROUPS ug where pg.Profile_id=p.profile_id and ug.user_grp_id=pg.group_id";
		try{
			String delflag = data.CheckEncryptData("N");
			String query=" select distinct p.NAME,ug.user_group from Profile p,Profile_GROUP pg,USER_GROUPS ug where pg.Profile_id=p.profile_id " +
					"and ug.user_grp_id=pg.group_id and pg.DELETE_FLAG='"+delflag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);


			while (rs.next()) {

				UserDTO u=new UserDTO();
				String name=rs.getString(1);
				String ug=data.CheckDecryptData(rs.getString(2));
				u.setUserProfileName(name);
				u.setProfileGroup(ug);
				profileNameList.add(u);
			}
		}
		catch (Exception ee) {
			ee.printStackTrace();
		}

		return profileNameList;

	}


	public Long getIndexUsers(String index, Long dbId, Long userId, String saved) {
		Long b = 0L;
		PreparedStatement ps = null;
		try {
			System.out.println("getIndexUsers > "+userId+" > " +dbId+" > "+ index);
			String saveflag = data.CheckEncryptData(saved);
			String query = "select ui.USER_INDEX_ID from USER_DB_INDEX ui where ui.INDEX_NAME ='"
					+ index + "' and ui.USER_ID=" + userId+ "  and ui.DB_ID=" + dbId+" and ui.SAVED='"+saveflag+"'";
			ps = db.getDBConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			/*Nithu Alexander : 03/09/2014
			 * Fix for Filter Options not appearing in Popup
			 * of Admin Settings screen for an IndexName. This issue
			 * was found as part of Search Filter Issue in PROD
			 * (reported on 19th Aug)
			 * Changing the IF Condition to WHILE Condition
			 * Previous Code Snippet :
			 * if (rs.next()) {
				b = rs.getLong(1);
				}
			 */
			while (rs.next()) {
				b = rs.getLong(1);
				System.out.println("getuseindexid" + b);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}

	public List<UserDTO> getIndexUsersList(Long dbId, Long userId, String saved) {
		List idxList = new ArrayList();
		PreparedStatement ps = null;
		try {
			System.out.println("getIndexUsersList > "+userId+" > " +dbId);
			String flag = data.CheckEncryptData(saved);
			String query = "select di.DB_ID,di.INDEX_ID,di.INDEX_NAME,ui.USER_ID,di.INDEX_DESC from USER_DB_INDEX ui,VP_DATABASE_INDEX di where ui.INDEX_NAME = di.INDEX_NAME"
					+" and di.DB_ID=ui.DB_ID and di.DB_ID="+dbId+" and ui.USER_ID = "+userId+" and ui.SAVED = '"+flag+"'";
			ps = db.getDBConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserDTO dto = new UserDTO();
				dto.setIndexId(rs.getLong(2));
				dto.setIndexName(rs.getString(3));
				dto.setIndexDesc(rs.getString(5));
				System.out.println("idxList size in getIndexUSersList::"+dto);
				idxList.add(dto);
			}
			System.out.println("idxList size in getIndexUSersList::"+idxList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return idxList;
	}


	public List<Long> getIndexesOfProfile(Long profileId)
	{
		String padminroledesc = CommonConstants.ROLE_DESC_PROJECTADMIN;
		String sql = "select distinct dbi.INDEX_ID from PROFILE_GROUP pg,DATABASE_INDEX dbi where dbi.DB_ID=pg.DB_ID and pg.PROFILE_ID="
				+ profileId;
		List<Long> indexes = new ArrayList<Long>();

		try {
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				indexes.add(rs.getLong((1)));

			}
		} catch (Exception e) {
			System.out.println(e);
		}


		return indexes;
	}


	public boolean DeleteIndexUsers(Long indexId, Long userId) {
		boolean b = false;
		try {
			System.out.println("getUserIndex" + indexId);

			if (indexId != 0 && userId != 0) {
				String flag = data.CheckEncryptData("N");

				String query = "Delete from USER_DB_INDEX  where USER_ID= "
						+ userId;
				Statement stmt = db.getDBConnection().createStatement();
				stmt.execute(query);

				b = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}


	public boolean deleteIndexResultFromDb(Long UserId, Long db_id, Long UserIndexId) {
		boolean b = false;
		try {





			String query = "Delete from USER_DB_INDEX  where USER_ID="+UserId+" and DB_ID="+db_id+" and USER_INDEX_ID="+UserIndexId;
			System.out.println("************* deleteIndexResultFromDb query : " + query);	
			Statement stmt = db.getDBConnection().createStatement();
			stmt.execute(query);

			b = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}




	public Map<String, Long> saveIndex(Long dbId, String[] Indexes,
			String[] indexDesc) {
		CommonDAO dao = new CommonDAO();
		Map<String, Long> map = new HashMap<String, Long>();
		try {
			System.out.println("saveIndex" + dbId);
			if (dbId != 0) {
				String flag = data.CheckEncryptData("Y");
				for (int i = 0; i < Indexes.length; i++) {
					String hash = AeSimpleSHA1.SHA1(dbId + ","
							+ data.CheckEncryptData(Indexes[i]) + ","
							+ data.CheckEncryptData(indexDesc[i]));
					String query = "INSERT INTO DATABASE_INDEX(DB_ID,HASH_ID,INDEX_NAME,INDEX_DESC,INDEX_FLAG) VALUES(?, ?, ?, ?, ?);";
					System.out.println("***** saveIndex sql : " + query);
					PreparedStatement stmt = db.getDBConnection()
							.prepareStatement(query,
									PreparedStatement.RETURN_GENERATED_KEYS);
					stmt.setLong(1, dbId);
					stmt.setString(2, hash);
					stmt.setString(3, dao.CheckEncryptData(Indexes[i]));
					stmt.setString(4, dao.CheckEncryptData(indexDesc[i]));
					stmt.setString(5, dao.CheckEncryptData(flag));
					stmt.executeUpdate();
					ResultSet rs = stmt.getGeneratedKeys();
					if (rs.next()) {
						// 67725650
						map.put(indexDesc[i], rs.getLong(1));
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public List getSearchIndex(String dbname, String Index) {
		CommonDAO dao = new CommonDAO();
		List map = new ArrayList();
		try {
			System.out.println("getSearchIndex" + dbname+">>"+Index);
			if (dbname != null) {
				String query = "SELECT distinct " + Index
						+ " FROM "+dbname;
				System.out.println("query is :" + query);
				PreparedStatement stmt = db.getDBConnection().prepareStatement(
						query);
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					UserDTO dto = new UserDTO();
					dto.setIndexResults(rs.getString(1));
					map.add(dto);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}


	public boolean updateIndex(Long dbId, String[] Indexes, String[] indexDesc) {
		CommonDAO dao = new CommonDAO();
		Map<String, Long> map = new HashMap<String, Long>();
		PreparedStatement stmt = null;
		boolean b = false;
		try {
			System.out.println("updateIndex" + dbId);
			if (dbId != 0) {
				String flag = data.CheckEncryptData("N");
				for (int i = 0; i < Indexes.length; i++) {
					String hash = AeSimpleSHA1.SHA1(dbId + "," + Indexes[i]
							+ "," + indexDesc[i]);
					String query = "UPDATE DATABASE_INDEX  SET HASH_ID=?,INDEX_NAME=?,INDEX_DESC=?,INDEX_FLAG=? where DB_ID=?";// VALUES(?,
					// ?,
					// ?,
					// ?,
					// ?);";
					System.out.println("***** updateIndex sql : " + query);
					stmt = db.getDBConnection().prepareStatement(query);

					stmt.setString(1, hash);
					stmt.setString(2, Indexes[i]);
					stmt.setString(3, indexDesc[i]);
					stmt.setString(4, dao.CheckEncryptData(flag));
					stmt.setLong(5, dbId);
					stmt.executeUpdate();

					// 67725650
					b = true;

				}
				stmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}

	public boolean saveIndexResults(Long userIndexId, String Indexes) {
		boolean map = false;
		try {
			System.out.println("saveIndexResults" + userIndexId);
			if (userIndexId != 0) {

				String hash = AeSimpleSHA1.SHA1(userIndexId + ","
						+ data.CheckEncryptData(Indexes));
				String query = "INSERT INTO USER_INDEX_RESULTS(USER_INDEX_ID,HASH_ID,INDEX_RESULTS_PERM) VALUES("
						+ userIndexId
						+ ",'"
						+ hash
						+ "','"
						+ data.CheckEncryptData(Indexes) + "');";
				System.out.println("*****&&&&&&&&& saveIndexResults sql : " + query);
				Statement stmt = db.getDBConnection().createStatement();
				stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
				ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {

					map = true;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}


	public boolean deleteIndexResults(Long userIndexId) {
		boolean returnVal = false;
		try {

			if (userIndexId != 0) {


				String query = "DELETE FROM USER_INDEX_RESULTS WHERE USER_INDEX_ID="+userIndexId;
				Statement stmt = db.getDBConnection().createStatement();
				//System.out.println("############### BEFORE DELETE INDEX RESULTS");
				System.out.println("delete index query : " + query);
				stmt.execute(query);
				//System.out.println("############### AFTER DELETE INDEX RESULTS");
				returnVal = true;


			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnVal;
	}


	public boolean isIndexResults(Long userIndexId, String Indexes) {
		boolean map = false;
		try {
			System.out.println("saveIndexResults" + userIndexId);
			if (userIndexId != 0) {

				String hash = AeSimpleSHA1.SHA1(userIndexId + ","
						+ data.CheckEncryptData(Indexes));
				String query = "select USER_INDEX_ID from USER_INDEX_RESULTS where USER_INDEX_ID="
						+ userIndexId
						+ " and INDEX_RESULTS_PERM='"
						+ data.CheckEncryptData(Indexes) + "';";
				Statement stmt = db.getDBConnection().createStatement();

				ResultSet rs = stmt.executeQuery(query);
				if (rs.next()) {

					map = true;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public List getIndexResults(Long userId) {
		List map = new ArrayList();
		try {
			System.out.println("getIndexResults" + userId);
			if (userId != 0) {
				String query = "select ur.INDEX_RESULTS_PERM from USER_INDEX_RESULTS ur,USER_MGMT u, USER_DB_INDEX ui where u.USER_ID = ui.USER_ID and ui.USER_INDEX_ID = ur.USER_INDEX_ID and ui.USER_ID="
						+ userId;
				Statement stmt = db.getDBConnection().createStatement();

				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					System.out.println("indexes"
							+ data.CheckDecryptData(rs.getString(1)));
					map.add(data.CheckDecryptData(rs.getString(1)));
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public String getUserIndexResults(Long userId, Long dbId, String index, String ser) {
		String indexRes = "";
		try {
			System.out.println("getUserIndexResults:" + userId);
			System.out.println("index:" + index);
			String query = "";
			String flag = data.CheckEncryptData("Y");
			if (userId != 0) {

				query = "select ur.INDEX_RESULTS_PERM,ui.INDEX_NAME,ui.DB_ID from USER_INDEX_RESULTS ur, USER_DB_INDEX ui where ui.INDEX_NAME='"
						+ index
						+ "' and ui.DB_ID="
						+ dbId
						+ " and ui.USER_INDEX_ID = ur.USER_INDEX_ID and ui.USER_ID="
						+ userId;
				System.out.println("---------------------------------->>> getUserIndexResults query : " + query);
				Statement stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
					if (ser.equals("search"))
						indexRes = data.CheckDecryptData(rs.getString(1));
					else
						indexRes += data.CheckDecryptData(rs.getString(1)) + ", ";

					System.out.println("indexResults::" + indexRes);
				}
				System.out.println("indexResults final::" + indexRes);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return indexRes;
	}


	public String getUserIndexResultsForDisplay(Long userId, Long dbId, String index, String ser, Long userIndexId) {
		String indexRes = "";
		try {
			System.out.println("getUserIndexResults:" + userId);
			System.out.println("index:" + index);
			String query = "";
			String flag = data.CheckEncryptData("Y");
			if (userId != 0) {

				query = "select ur.INDEX_RESULTS_PERM,ui.INDEX_NAME,ui.DB_ID from USER_INDEX_RESULTS ur, USER_DB_INDEX ui where ui.INDEX_NAME='"
						+ index
						+ "' and ui.DB_ID="
						+ dbId
						+ " and ui.USER_INDEX_ID = ur.USER_INDEX_ID and ui.USER_ID="
						+ userId
						+ " and ui.USER_INDEX_ID="
						+ userIndexId;
				System.out.println("---------------------------------- getUserIndexResults query : " + query);
				Statement stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
					if (ser.equals("search"))
						indexRes = data.CheckDecryptData(rs.getString(1));
					else
						indexRes += data.CheckDecryptData(rs.getString(1)) + ", ";

					System.out.println("indexResults::" + indexRes);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return indexRes;
	}

	public String getUserIndexResultsForView(Long userId, Long dbId, String index, String ser) {
		String indexRes = "";
		try {
			System.out.println("getUserIndexResults:" + userId);
			System.out.println("index:" + index);
			String query = "";
			String flag = data.CheckEncryptData("Y");
			if (userId != 0) {

				query = "select ur.INDEX_RESULTS_PERM,ui.INDEX_NAME,ui.DB_ID from USER_INDEX_RESULTS ur, USER_DB_INDEX ui where ui.INDEX_NAME='"
						+ index
						+ "' and ui.DB_ID="
						+ dbId
						+ " and ui.USER_INDEX_ID = ur.USER_INDEX_ID and ui.USER_ID="
						+ userId;
				System.out.println("^^^^^^^ res query : " + query);
				Statement stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
					if (ser.equals("search"))
						indexRes = data.CheckDecryptData(rs.getString(1));
					else
						indexRes += data.CheckDecryptData(rs.getString(1)) + ",";

					System.out.println("indexResults::" + indexRes);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return indexRes;
	}

	public Map<String, Long> getIndex(Long dbId) {
		Map<String, Long> map = new HashMap<String, Long>();
		CommonDAO dao = new CommonDAO();
		try {
			System.out.println("getIndex" + dbId);
			if (dbId != 0) {
				String flag = "Y";
				// String hash =
				// AeSimpleSHA1.SHA1(dbId+","+data.encryptData(Indexes[i]));
				String query = "SELECT DB_ID,INDEX_ID,INDEX_NAME,INDEX_DESC FROM VP_DATABASE_INDEX where DB_ID=? and INDEX_FLAG=?";

				PreparedStatement stmt = db.getDBConnection().prepareStatement(
						query);
				stmt.setLong(1, dbId);
				stmt.setString(2, (flag));
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					System.out.println("indexId" + rs.getString(3));
					Long indexId = rs.getLong(2);
					String indexName = rs.getString(3);
					String indexdesc = rs.getString(4);
					map.put(indexName, indexId);
				}

				stmt.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public Map<String, Long> getProfileIndex(Long dbId, Long pid) {
		Map<String, Long> map = new HashMap<String, Long>();
		CommonDAO dao = new CommonDAO();
		try {
			System.out.println("Inside getProfileIndex for profileID:"+pid);
			if (dbId != 0) {
				String flag = "Y";
				String query = "SELECT dp.INDEX_ID,dp.INDEX_NAME FROM CSR_DBPROFILES dp where " +
						"dp.DB_ID=? and dp.PROFILE_ID =? and dp.INDEX_FLAG=?";

				PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
				stmt.setLong(1, dbId);
				stmt.setLong(2, pid);
				stmt.setString(3, dao.CheckEncryptData(flag));
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					Long indexId = rs.getLong(1);
					String indexName = rs.getString(2);
					map.put(indexName, indexId);
				}

				stmt.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public Map<String, Long> getVaultAppIndex(String dbName, Long dbId) {
		Map<String, Long> map = new HashMap<String, Long>();
		List<String> indexes = new ArrayList<String>();
		CommonDAO dao = new CommonDAO();
		try {
			System.out.println("getIndex" + dbName);
			if(!dbName.equalsIgnoreCase("") && dbName != null){
				indexes = getColumnNames(dbName);
			}
			if (dbId != 0) {
				for(String index:indexes){
					map.put(index, dbId);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public ArrayList<String> getDbIndex(Long dbId) {
		ArrayList<String> idxList = new ArrayList<String>();
		CommonDAO dao = new CommonDAO();
		try {
			System.out.println("getIndex" + dbId);
			if (dbId != 0) {
				String flag = "Y";
				String query = "SELECT DB_ID,INDEX_ID,INDEX_NAME,INDEX_DESC FROM DATABASE_INDEX where DB_ID=? and INDEX_FLAG=?";
				PreparedStatement stmt = db.getDBConnection().prepareStatement(	query);
				stmt.setLong(1, dbId);
				stmt.setString(2, dao.CheckEncryptData(flag));
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					System.out.println("indexId" + rs.getString(3));
					Long indexId = rs.getLong(2);
					String indexName = rs.getString(3);
					String indexdesc = rs.getString(4);
					idxList.add(indexName);
				}

				stmt.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return idxList;
	}

	public Map<String,Integer> getDbTypes() {
		Map<String,Integer> types = new HashMap<String,Integer>();
		CommonDAO dao = new CommonDAO();
		try {
			System.out.println("getDbTypes");
			String query = "select name,max_length from sys.types";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(	query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				System.out.println("type name::" + rs.getString(1)+"type length::" + rs.getInt(2));
				int max_length = rs.getInt(2);
				String typeName = rs.getString(1);
				types.put(typeName,max_length);
			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return types;
	}

	public boolean checkDBIndex(Long dbId) {
		try {
			String query = "SELECT DB_ID from DATABASE_INDEX where DB_ID="
					+ dbId + ";";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) {
				System.out.println("checkDBIndex:true");
				return true;
			}
		} catch (Exception e) {

		}
		return false;
	}

	public boolean checkDBIndex(Long dbId, String indexName) {
		PreparedStatement ps = null;
		try {
			System.out.println(dbId + "checkDBIndex:" + indexName);
			String query = "SELECT DB_ID from DATABASE_INDEX where DB_ID=? and INDEX_NAME=?";
			ps = db.getDBConnection().prepareStatement(query);
			// Statement stmt = db.getDBConnection().createStatement();
			ps.setLong(1, dbId);
			ps.setString(2, indexName);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				System.out.println("checkDBIndex:true");
				return true;
			}
		} catch (Exception e) {

		}
		return false;
	}

	public void updateUserActions(Long userId, Long actionId) {

		try {
			String query = "";
			Statement stmt = db.getDBConnection().createStatement();
			String flag = data.CheckEncryptData("Y");
			if (actionId != 0) {
				List list = getUserActions(userId);
				String hash = AeSimpleSHA1.SHA1(userId + "," + actionId);
				for (Iterator iter = list.iterator(); iter.hasNext();) {
					UserDTO dto = (UserDTO) iter.next();
					if (actionId == dto.getActionId()) {

						query = "UPDATE USER_ACTION SET ACTION_ID=" + actionId
								+ ",HASH_ID='" + hash + "' where USER_ID="
								+ dto.getUserId() + ";";

					} else {
						query = "UPDATE USER_ACTION SET ACTION_ID=" + actionId
								+ ",HASH_ID='" + hash + "',DELETE_FLAG='"
								+ flag + "' where USER_ID=" + dto.getUserId()
								+ ";";
					}
					stmt.execute(query);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<UserDTO> getUserActions(Long userId) {
		List<UserDTO> list = new ArrayList();
		try {
			String flag = data.CheckEncryptData("N");

			String query = "SELECT u.USER_ID,c.ACTION_NAME,u.ACTION_ID from USER_ACTION u,MASTER_ACTIONS c where u.USER_ID="
					+ userId
					+ " and c.ACTION_ID = u.ACTION_ID and DELETE_FLAG='"
					+ flag
					+ "';";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				UserDTO dto = new UserDTO();
				dto.setUserId(rs.getLong(1));
				dto.setActionName(rs.getString(2));
				dto.setActionId(rs.getLong(3));
				list.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<String> getCsrActions() {
		List list = new ArrayList();
		System.out.println("getCsrActions");
		try {

			String query = "SELECT ACTION_ID,ACTION_NAME from MASTER_ACTIONS";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				UserDTO dto = new UserDTO();
				dto.setActionId(rs.getLong(1));
				dto.setActionName(rs.getString(2));

				list.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/*
	 * public void updateUserMgmt(HashMap map) { try { SimpleDateFormat
	 * formatter = new SimpleDateFormat(CommonConstants.DATE_FORMAT); String s =
	 * formatter.format(date); Statement stmt =
	 * db.getDBConnection().createStatement();
	 * 
	 * } catch(Exception e) {
	 * 
	 * }
	 * 
	 * }
	 */
	public void saveUserIntegrity(String tableName, String ID, String newHash,
			String oldHash, Long logId, String flag) {
		System.out.println("saveUserIntegrity" + ID);
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);

			String query1 = "INSERT INTO "
					+ tableName
					+ "(INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG,"
					+ "?" + ") VALUES('" + "?" + "','" + "?" + "','" + "?"
					+ "',DEFAULT,'" + "?" + "'," + "?" + ");";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query1);
			stmt.setString(1, ID);
			stmt.setString(2, newHash);
			stmt.setString(3, oldHash);
			stmt.setString(4, s);
			stmt.setString(5, flag);
			stmt.setLong(6, logId);
			stmt.executeUpdate(query1);
			System.out
			.println("Successfully completed. saveUserIntegrity" + ID);
		} catch (Exception e) {

		}

	}

	public void updateUserIntegrity(String tableName, String ID,
			String newHash, String oldHash, Long logId, String flag) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Statement stmt = db.getDBConnection().createStatement();
			String query1 = "UPDATE " + tableName + " set INTE_NEW_HASH_ID='"
					+ newHash + "',INTE_OLD_HASH_ID='" + oldHash
					+ "',INTE_CHECK_DATE='" + s
					+ "',CHECK_TIME=DEFAULT,CHECK_FLAG='" + flag + "' where "
					+ ID + "=" + logId + ";";
			stmt.execute(query1);
		} catch (Exception e) {

		}

	}

	public List getUserAccessUsingGroups(String groupName) {
		List list = new ArrayList();
		CommonDAO cDao = new CommonDAO();


		try {
			String delete_flag= cDao.CheckEncryptData("N");
			String query = "select USER_ID,USER_GRP_ID,ACTIVE_FLAG,CURRENT_YEAR,ACCESS_DATE_FROM,ACCESS_DATE_TO,ACCESS_DAYS_FROM,ACCESS_DAYS_TO,USER_NAME from USER_MGMT where USER_GRP_ID = '"
					+ groupName + "' and DELETE_FLAG='"+delete_flag+ "'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				UserDTO dto = new UserDTO();
				System.out.println("UserList Size:::"+rs.getLong(1));
				dto.setUserId(rs.getLong(1));
				dto.setUserGrp(rs.getString(2));
				dto.setActive(cDao.CheckDecryptData(rs.getString(3)));

				dto.setAccessdateFrom(cDao.CheckDecryptData(rs.getString(5)));
				dto.setAccessdateTo(cDao.CheckDecryptData(rs.getString(6)));
				if (rs.getString(7) != null)
					dto.setAccessfromDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(7))));
				if (rs.getString(8) != null)
					dto.setAccesstoDays(Integer.parseInt(data.CheckDecryptData(rs
							.getString(8))));
				dto.setUserName(data.CheckDecryptData(rs.getString(9)));
				list.add(dto);

			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);

		}
		return list;
	}

	/*
	 * public List getUserAccess(String groupName,String userName) { List list =
	 * new ArrayList(); try { String query =
	 * "select USER_ID,USER_GROUP,USER_NAME,ACTIVE_FLAG,CURRENT_YEAR,ACCESS_DATE_FROM,ACCESS_DATE_TO,ACCESS_DAYS_FROM,ACCESS_DAYS_TO from USER_MGMT where USER_GROUP = '"
	 * +groupName+"' and USER_NAME='"+userName+"'"; Statement stmt =
	 * db.getDBConnection().createStatement(); ResultSet rs =
	 * stmt.executeQuery(query); while(rs.next()) { UserDTO dto = new UserDTO();
	 * dto.setUserId(rs.getLong(1));
	 * dto.setUserGrp(data.decryptData(rs.getString(2)));
	 * dto.setActive(data.decryptData(rs.getString(3)));
	 * dto.setCurrYear(Integer.parseInt(data.decryptData(rs.getString(4))));
	 * dto.setAccessdateFrom(data.decryptData(rs.getString(5)));
	 * dto.setAccessdateTo(data.decryptData(rs.getString(6)));
	 * dto.setAccessfromDays
	 * (Integer.parseInt(data.decryptData(rs.getString(7))));
	 * dto.setAccesstoDays(Integer.parseInt(data.decryptData(rs.getString(8))));
	 * 
	 * list.add(dto);
	 * 
	 * } } catch(Exception e) {
	 * 
	 * } return list; }
	 */
	public String getIntegrity(String tableName, String ID, String oldHash,
			Long tabId) {
		String hash = "";
		try {
			Statement stmt = db.getDBConnection().createStatement();
			String query = "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from "
					+ tableName + " where " + ID + " =" + tabId;

			ResultSet rs1 = stmt.executeQuery(query);
			if (rs1.next()) {
				if (rs1.getString(2) != null) {
					hash = rs1.getString(2);
				}
			}
		} catch (Exception e) {

		}
		return hash;

	}

	public void getTableIntegrity(String tableName, String ID, String newHash,
			String oldHash, Long logId, String flag) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Statement stmt = db.getDBConnection().createStatement();
			// LOGIN_TAB_INTEGRITY_CHECK
			// String query1 =
			// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from LOGIN_TAB_INTEGRITY_CHECK where LOG_ID = "+;
			// String query1 =
			// "INSERT INTO "+tableName+"(INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG,"+ID+") VALUES('"+newHash+"','"+oldHash+"','"+s+"',DEFAULT,'"+flag+"',"+logId+");";
			// stmt.execute(query1);
		} catch (Exception e) {

		}

	}

	public Long CreateNewRole(RoleDTO roleDto) {
		Long roleId = 0L;
		String roleName = "";
		String roleDesc = "";
		try {
			System.out.println("CreateNewRole" + roleId);
			if (roleDto.getRoleName() != null)
				roleName = data.CheckEncryptData(roleDto.getRoleName());
			if (roleDto.getRoleDesc() != null)
				roleDesc = data.CheckEncryptData(roleDto.getRoleDesc());
			String hash = AeSimpleSHA1.SHA1(roleName + "," + roleDesc);
			String query = "INSERT INTO MASTER_ROLES(ROLE_NAME,ROLE_DESC,HASH_ID) VALUES("
					+ roleName + "," + roleDesc + ",'" + hash + "');";
			Statement stmt = db.getDBConnection().createStatement();
			stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()) {
				roleId = rs.getLong(1);
				System.out.println("CreateNewRole:roleId>" + roleId);
				saveUserIntegrity(CommonConstants.CSR_ROLES_INTEGRITY_CHECK,
						"ROLE_ID", hash, hash, roleId, "yes");

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roleId;
	}
	//hint  PreparedStatement ps = con.prepareStatement(sql, new String[]{"USER_ID"})
	public void insertRole(Long userId, Long roleId) {
		Long save = 0L;
		try {
			System.out.println(userId + ":insertRole:" + roleId);
			String flag = data.CheckEncryptData("N");

			if (roleId != null && roleId != 0) {
				String hash = AeSimpleSHA1.SHA1(userId + "," + roleId + ","
						+ flag);
				boolean b = isRole(userId, roleId);
				if (!b)

				{
					String query = "INSERT INTO USER_ROLES(USER_ID,ROLE_ID,HASH_ID,DELETE_FLAG) VALUES("
							+ "?"
							+ ","
							+ "?"
							+ ","
							+ "?"
							+ ","
							+ "?" + ");";
					PreparedStatement stmt = db.getDBConnection().prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
					// Statement stmt1 = db.getDBConnection().createStatement();

					stmt.setLong(1, userId);
					stmt.setLong(2, roleId);
					stmt.setString(3, hash);
					stmt.setString(4, flag);
					stmt.executeUpdate();
					ResultSet rs = stmt.getGeneratedKeys();
					while (rs.next()) {
						save = rs.getLong(1);
						System.out.println("^^^^^^^^^save:insertRole" + save);
						saveUserIntegrity(
								CommonConstants.USER_ROLE_INTEGRITY_CHECK,
								"USER_ROLE_ID", hash, hash, save, "yes");
						insertRoleReport(userId, roleId, save, hash);
					}
				} else {
					System.out.println("already exist");
					String query = "UPDATE USER_ROLES SET DELETE_FLAG='" + flag
							+ "' where USER_ID=" + userId + " and ROLE_ID="
							+ roleId;
					Statement stmt = db.getDBConnection().createStatement();
					// Statement stmt1 = db.getDBConnection().createStatement();
					stmt.executeUpdate(query);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void DeleteRole(Long userId) {
		Long save = 0L;
		try {
			System.out.println(userId + ":insertRole:");

			String flag = data.CheckEncryptData("Y");

			String query = "UPDATE USER_ROLES SET DELETE_FLAG=" + "?"
					+ "  where USER_ID=" + "?";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
			// Statement stmt1 = db.getDBConnection().createStatement();
			stmt.setString(1, flag);
			stmt.setLong(2, userId);
			stmt.executeUpdate(query);
			// ResultSet rs = stmt.getGeneratedKeys();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertRoleReport(Long userId, Long roleId, Long userRoleId,
			String hash) {

		try {
			// String flag = data.encryptData("N");
			// String hash = AeSimpleSHA1.SHA1(userId+","+roleId+","+flag);
			Statement stmt = db.getDBConnection().createStatement();

			System.out.println("^^^^^^^^^^^^^updated:insertRoleReport"
					+ userRoleId);
			// saveUserIntegrity(CommonConstants.USER_ROLE_INTEGRITY_CHECK,"USER_ROLE_ID",hash,hash,userRoleId,"yes");
			String query1 = "INSERT INTO USER_ROLES_REPORT(USER_ID,ROLE_ID,HASH_ID,USER_ROLE_ID) VALUES("
					+ userId
					+ ","
					+ roleId
					+ ",'"
					+ hash
					+ "',"
					+ userRoleId
					+ ");";

			stmt.execute(query1);

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateUSERRole(Long userId, Long roleId) {
		Long save = 0L;
		String query = "";
		try {
			String flag = data.CheckEncryptData("Y");
			List list = getRole(userId);
			PreparedStatement stmt=null;

			String hash = AeSimpleSHA1.SHA1(userId + "," + roleId + "," + flag);
			for (Iterator iter = list.iterator(); iter.hasNext();) {
				UserDTO dto = (UserDTO) iter.next();
				if (roleId == dto.getRoleId()) {

					query = "UPDATE USER_ROLES SET ROLE_ID=" + "?"
							+ ",HASH_ID=" +  "?" + " where USER_ID="
							+  "?" + ";";

					stmt = db.getDBConnection().prepareStatement(query);
					stmt.setLong(1, roleId);
					stmt.setString(2,hash);
					stmt.setLong(3, dto.getUserId());

				} else {
					query = "UPDATE USER_ROLES SET ROLE_ID=" +  "?"
							+ ",HASH_ID=" +  "?" + ",DELETE_FLAG=" +  "?"
							+ " where USER_ID=" + "?" + ";";
					stmt = db.getDBConnection().prepareStatement(query);
					stmt.setLong(1, roleId);
					stmt.setString(2,hash);
					stmt.setString(3, flag);
					stmt.setLong(4, dto.getUserId());


				}
				// String query1 =
				// "UPDATE USER_ROLES SET DELETE_FLAG='"+flag+"' where  USER_ID= "+dto.getUserId();
				stmt.executeUpdate(query);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateDatabase(Long userId, Long dbId) {
		Long save = 0L;
		try {
			String flag = data.CheckEncryptData("Y");
			String query = "UPDATE USER_DATABASE SET DELETE_FLAG='" + flag
					+ "' where USER_ID = " + userId;
			List list = getDatabase(userId);
			PreparedStatement stmt = null;
			String hash = AeSimpleSHA1.SHA1(userId + "," + dbId + "," + flag);
			for (Iterator iter = list.iterator(); iter.hasNext();) {
				UserDTO dto = (UserDTO) iter.next();
				if (dbId == dto.getActionId()) {

					query = "UPDATE USER_DATABASE SET DB_ID=" + "?"
							+ ",HASH_ID=" + "?" + " where USER_ID="
							+ "?" + ";";

					stmt=db.getDBConnection().prepareStatement(query);
					stmt.setLong(1, dbId);
					stmt.setString(2, hash);
					stmt.setLong(3, dto.getUserId());

				} else {
					query = "UPDATE USER_DATABASE SET DB_ID=" + "?"
							+ ",HASH_ID=" + "?" + ",DELETE_FLAG=" +"?"
							+ " where USER_ID=" + "?" + ";";

					stmt=db.getDBConnection().prepareStatement(query);
					stmt.setLong(1, dbId);
					stmt.setString(2, hash);
					stmt.setString(3, flag);
					stmt.setLong(4, dto.getUserId());
				}
				// String query1 =
				// "UPDATE USER_ROLES SET DELETE_FLAG='"+flag+"' where  USER_ID= "+dto.getUserId();
				stmt.executeUpdate();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Long[] getUserDatabases(Long userId) {
		List<Long> dbList = new ArrayList<Long>();

		try {
			String query = "SELECT DB_ID FROM USER_DATABASE WHERE USER_ID="
					+ userId;
			Statement stmt = db.getDBConnection().createStatement();

			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {

				dbList.add(rs.getLong(1));
			}
		} catch (Exception e) {
			System.out.println(e);

		}
		Long dbArray[] = new Long[dbList.size()];
		int i = 0;
		for (Long long1 : dbList) {
			dbArray[i++] = long1;

		}
		return dbArray;
	}

	public Long[] getUserActions1(Long userId) {
		List<Long> actionList = new ArrayList<Long>();

		try {
			String query = "SELECT ACTION_ID FROM USER_ACTION WHERE USER_ID="
					+ userId;
			Statement stmt = db.getDBConnection().createStatement();

			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {

				actionList.add(rs.getLong(1));
			}
		} catch (Exception e) {
			System.out.println(e);

		}
		Long actionArray[] = new Long[actionList.size()];
		int i = 0;
		for (Long action : actionList) {
			actionArray[i++] = action;
		}

		return actionArray;
	}

	public void insertDatabase(Long userId, Long dbId) {
		Long save = 0L;
		PreparedStatement ps = null;
		try {
			System.out.println("insertDatabase" + dbId);
			if (dbId != null && dbId != 0) {
				String flag = data.CheckEncryptData("N");
				boolean b = getUserDatabase(userId, dbId);
				if (!b) {
					String hash = AeSimpleSHA1.SHA1(userId + "," + dbId + ","
							+ flag);
					String query = "INSERT INTO USER_DATABASE(USER_ID,DB_ID,HASH_ID,DELETE_FLAG) VALUES(?,?,?,?)";

					// Statement stmt = db.getDBConnection().createStatement();
					ps = db.getDBConnection().prepareStatement(query,
							PreparedStatement.RETURN_GENERATED_KEYS);
					ps.setLong(1, userId);
					ps.setLong(2, dbId);
					ps.setString(3, hash);
					ps.setString(4, flag);
					ps.executeUpdate();
					ResultSet rs = ps.getGeneratedKeys();
					while (rs.next()) {
						save = rs.getLong(1);
						System.out.println("^^^^^^^^^^^^^updated:insertDatabase"
								+ save);
						saveUserIntegrity(
								CommonConstants.USER_DB_INTEGRITY_CHECK,
								"USER_DB_ID", hash, hash, save, "yes");
						insertDatabaseReport(userId, dbId, save, hash);

					}
				} else {

					String query = "UPDATE USER_DATABASE SET DELETE_FLAG="
							+ "?" + " where USER_ID=" + "?"
							+ " and DB_ID=" + "?";
					PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
					stmt.setString(1, flag);
					stmt.setLong(2, userId);
					stmt.setLong(3, dbId);
					stmt.executeUpdate();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void DeleteDatabase(Long userId) {
		Long save = 0L;
		try {
			System.out.println("DeleteDatabase" + userId);

			String flag = data.CheckEncryptData("Y");

			String query = "UPDATE USER_DATABASE SET DELETE_FLAG=" + "?"
					+ " where USER_ID=" + "?";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query);

			stmt.setString(1, flag);
			stmt.setLong(2, userId);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertDatabaseReport(Long userId, Long dbId, Long userDbId,
			String hash) {
		// Long save=0L;
		try {

			String query1 = "INSERT INTO USER_DATABASE_REPORT(USER_ID,DB_ID,HASH_ID,USER_DB_ID) VALUES("
					+ "?"
					+ ","
					+ "?"
					+ ","
					+ "?"
					+ ","
					+ "?"
					+ ");";
			PreparedStatement stmt1 = db.getDBConnection().prepareStatement(query1);
			stmt1.setLong(1, userId);
			stmt1.setLong(2,dbId);
			stmt1.setString(3, hash);
			stmt1.setLong(4, userDbId);

			stmt1.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * public void updateDatabase(Long userId,Long dbId) { Long save=0L; try {
	 * if(dbId != 0) { String hash = AeSimpleSHA1.SHA1(userId+","+dbId); String
	 * query = "UPDATE USER_DATABASE set DB_ID = "+dbId+",HASH_ID = '"+hash+
	 * "' where USER_ID="+userId+";"; Statement stmt =
	 * db.getDBConnection().createStatement(); stmt.execute(query);
	 * 
	 * } } catch(SQLException e) { e.printStackTrace(); } catch(Exception e) {
	 * e.printStackTrace(); } }
	 */
	public List getUserAccess(String userName, String userGroup, String realPath) {

		List<UserDTO> users = new ArrayList<UserDTO>();
		System.out.println(userName + "--getUserAccess--" + userGroup);
		PreparedStatement ps = null;
		CommonDAO cDAO = new CommonDAO();
		Connection conn = db.getDBConnection();
		List<String> groups=	LDAPUtils.getGroupsOfUser("", userName);

		String day1 = "";
		String day2 ="";
		try {
			String InString=null;
			if(groups != null && groups.size() > 0){
			InString = createInQuery(groups, realPath);
			}else
			{
				if(userGroup != null && !"".equalsIgnoreCase(userGroup))
				{
					InString = " ('" + userGroup +"')"; 
				}
			}
			System.out.println("Instringxxxxxxxxxxxxxxxxxxxxxxx"+InString);
			if(conn != null)
			{
				String flag = cDAO.CheckEncryptData("Y");
				String delflag = cDAO.CheckEncryptData("N");
				System.out.println("User Name"+userName);
				System.out.println("Group Name"+userGroup);

				userName =cDAO.CheckEncryptData(userName.toLowerCase());
				userGroup = cDAO.CheckEncryptData(userGroup);


				System.out.println(userName);
				System.out.println(userGroup);


				System.out.println(delflag+"flag is :"+flag);
				String query1 = "select u.USER_ID,ug.USER_GRP_ID,u.USER_NAME,u.ACCESS_DAYS_FROM, u.ACCESS_DAYS_TO, u.ACCESS_Date_FROM, u.ACCESS_DATE_TO, ug.USER_GROUP, u.ACTIVE_FLAG from  USER_MGMT u, USER_GROUPS ug where u.USER_GRP_ID = ug.USER_GRP_ID and u.USER_NAME=? and ug.USER_GROUP IN "+InString+" and u.DELETE_FLAG = ? and u.PROFILE_COMPLETED_FLAG = ?";
				System.out.println("yyyyyyyyyyyyyyyyyyyyyyyy"+query1);
				ps = conn.prepareStatement(query1);
				ps.setString(1, userName);
				//ps.setString(2, userGroup);
				ps.setString(2, delflag);
				ps.setString(3, flag);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					UserDTO uDto = new UserDTO();

					uDto.setUserId(rs.getLong(1));
					uDto.setGroupId(rs.getLong(2));
					uDto.setUserName(cDAO.CheckDecryptData(rs.getString(3)));

					if(!cDAO.CheckDecryptData(rs
							.getString(4)).equals(""))
					{
						uDto.setAccessfromDays(Integer.parseInt(cDAO.CheckDecryptData(rs
								.getString(4))));
						day1 = DateUtil.returnday(Integer.parseInt(cDAO.CheckDecryptData(rs.getString(4))));
					}
					if(!cDAO.CheckDecryptData(rs
							.getString(5)).equals(""))	
					{
						uDto.setAccesstoDays(Integer.parseInt(cDAO.CheckDecryptData(rs
								.getString(5))));
						day2 = DateUtil.returnday(Integer.parseInt(cDAO.CheckDecryptData(rs.getString(5))));

					}
					System.out.println("day1" + day1 + "day2" + day2);
					uDto.setAccessfromDay(day1);
					uDto.setAccesstoDay(day2);

					uDto.setAccessdateFrom(cDAO.CheckDecryptData(rs.getString(6)));
					uDto.setAccessdateTo(cDAO.CheckDecryptData(rs.getString(7)));
					uDto.setUserGrp(cDAO.CheckDecryptData(rs.getString(8)));
					uDto.setActive(cDAO.CheckDecryptData(rs.getString(9)));
					System.out.println("Group:" + cDAO.CheckDecryptData(rs.getString(8)));
					users.add(uDto);
				}

			}
		}catch (SQLException e) {
			e.printStackTrace();
			System.out.println("****" + e);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("****" + e);
		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return users;
	}
	public List getUserAccess1(String userName, String userGroup) {

		List users = new ArrayList();
		System.out.println(userName + "--getUserAccess--" + userGroup);
		CommonDAO cDAO = new CommonDAO();
		String day1 = "";
		String day2 = "";
		try {
			String flag = data.CheckEncryptData("Y");
			System.out.println("flag is sss" + flag);
			String delflag = data.CheckEncryptData("N");
			System.out.println("del flag" + delflag);
			userName = data.CheckEncryptData(userName.toLowerCase());
			System.out.println("user name" + userName);
			userGroup = data.CheckEncryptData(userGroup);

			System.out.println("User group" + userGroup);
			System.out.println(userName + "--getUserAccess--" + userGroup);
			String query1 = "select u.USER_ID,ug.USER_GRP_ID,u.USER_NAME,u.CURRENT_YEAR,u.ACCESS_DAYS_FROM, u.ACCESS_DAYS_TO, u.ACCESS_Date_FROM, u.ACCESS_DATE_TO, ug.USER_GROUP, u.ACTIVE_FLAG from  USER_MGMT u, USER_GROUPS ug where u.USER_GRP_ID = ug.USER_GRP_ID and u.USER_NAME='"
					+ userName
					+ "' and ug.USER_GROUP='"
					+ userGroup
					+ "' and u.DELETE_FLAG = '"
					+ delflag
					+ "' and u.PROFILE_COMPLETED_FLAG = '" + flag + "'";

			System.out.println("query1"+query1);
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query1);
			while (rs.next()) {
				UserDTO uDto = new UserDTO();

				uDto.setUserId(rs.getLong(1));
				uDto.setGroupId(rs.getLong(2));
				uDto.setUserName(cDAO.CheckDecryptData(rs.getString(3)));

				if (!cDAO.CheckDecryptData(rs.getString(5)).equals("")) {
					uDto.setAccessfromDays(Integer.parseInt(cDAO
							.CheckDecryptData(rs.getString(5))));
					day1 = GetDay.returnday(Integer.parseInt(cDAO
							.CheckDecryptData(rs.getString(5))));
				}
				if (!cDAO.CheckDecryptData(rs.getString(6)).equals("")) {
					uDto.setAccesstoDays(Integer.parseInt(cDAO
							.CheckDecryptData(rs.getString(6))));
					day2 = GetDay.returnday(Integer.parseInt(cDAO
							.CheckDecryptData(rs.getString(6))));

				}
				System.out.println("day1" + day1 + "day2" + day2);
				uDto.setAccessfromDay(day1);
				uDto.setAccesstoDay(day2);

				uDto.setAccessdateFrom(cDAO.CheckDecryptData(rs.getString(7)));
				uDto.setAccessdateTo(cDAO.CheckDecryptData(rs.getString(8)));
				uDto.setUserGrp(cDAO.CheckDecryptData(rs.getString(9)));
				users.add(uDto);
			}

		} catch (Exception e) {
			System.out.println("****" + e);
		}
		return users;
	}

	public boolean getUserAccessWithGroupId(String userName, Long groupId) {

		// List users = new ArrayList();
		boolean userExist = false;
		String b4user=userName;
		System.out.println(userName + "--getUserAccess--" + groupId);
		PreparedStatement ps = null;
		try {
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");
			//String ldapuserId = LDAPUtils.getUserIDByDistinguishedName(userName,"");
			//System.out.println("ldapuserId::"+ldapuserId);

			userName = data.CheckEncryptData(userName.toLowerCase());
			// userGroup = data.encryptData(userGroup);
			System.out.println(userName + "--getUserAccess--" + groupId);
			String query = "select u.USER_ID,ug.USER_GRP_ID,u.USER_NAME,u.CURRENT_YEAR,u.ACCESS_DAYS_FROM, u.ACCESS_DAYS_TO, u.ACCESS_Date_FROM, u.ACCESS_DATE_TO, ug.USER_GROUP from  USER_MGMT u, USER_GROUPS ug where u.USER_GRP_ID = ug.USER_GRP_ID and u.USER_NAME=?"

				+ " and u.DELETE_FLAG = ?"

				+ " and u.PROFILE_COMPLETED_FLAG = ?";

			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, userName);
			ps.setString(2, delflag);
			ps.setString(3, flag);
			// Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				userExist = true;
			}
			System.out.println("user exist:" + userExist);
		} catch (Exception e) {
			System.out.println("****" + e);
			e.printStackTrace();
		}
		String propertyFile = CommonConstants.CSR_PROPERTY_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propertyFile);
		String adminId=	properties.getProperty(CommonConstants.ADMINISTRATOR);
		if(adminId==null) adminId="";



		if(!userExist)
		{
			userExist=(b4user.equalsIgnoreCase(adminId));
		}

		return userExist;
	}


	public boolean isUserInProfile(String userName,Long profileId) {

		// List users = new ArrayList();
		boolean userExist = false;
		System.out.println(userName + "--isUserInProfile--" + profileId);
		PreparedStatement ps = null;
		try {
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");
			//String ldapuserId = LDAPUtils.getUserIDByDistinguishedName(userName,"");
			//System.out.println("ldapuserId::"+ldapuserId);

			userName = data.CheckEncryptData(userName.toLowerCase());
			// userGroup = data.encryptData(userGroup);
			//	System.out.println(userName + "--getUserAccess--" + groupId);
			String query = "select u.USER_ID from  USER_MGMT u,profile_group pg where u.USER_ID = pg.USER_ID and u.USER_NAME=? and pg.profile_id=?"

					;

			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, userName);
			ps.setLong(2, profileId);
			// Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				userExist = true;
			}
			System.out.println("user exist:" + userExist);
		} catch (Exception e) {
			System.out.println("In Profile Exception" + e);
			e.printStackTrace();
		}
		return userExist;
	}



	public boolean getUserAccessWithUserId(Long userId, String groupName) {

		boolean list = false;
		Statement stmt = null;
		System.out.println("getUserAccess");
		try {
			String query = "select USER_ID,USER_GROUP,ACTIVE_FLAG,CURRENT_YEAR,ACCESS_DATE_FROM,ACCESS_DATE_TO,ACCESS_DAYS_FROM,ACCESS_DAYS_TO from USER_MGMT where USER_ID="
					+ userId + " and USER_GROUP = '" + groupName + "'";
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) {
				list = true;
			}
		} catch (Exception e) {
			System.out.println("****" + e);
		}
		return list;
	}

	public Long saveDBName(String dbName, String dbDesc) {
		PreparedStatement ps = null;
		Long dbId = 0l;
		try {
			// Statement stmt = null;
			String query = "INSERT INTO CSR_DATABASE(DB_NAME,DB_DESC) values(?,?)";

			ps = db.getDBConnection().prepareStatement(query,
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, dbName);
			ps.setString(2, dbDesc);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next())
				dbId = rs.getLong(1);

		} catch (Exception e) {

		}
		return dbId;
	}

	public Long saveDBIndex(Long dbId, String indexName, String indexDesc) {
		PreparedStatement ps = null;
		Long indexId = 0l;
		try {
			// Statement stmt = null;
			String flag = data.CheckEncryptData("Y");
			String query = "INSERT INTO DATABASE_INDEX(DB_ID,INDEX_NAME,INDEX_DESC,INDEX_FLAG) values(?,?,?,?)";

			ps = db.getDBConnection().prepareStatement(query,
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setLong(1, dbId);
			ps.setString(2, indexName);
			ps.setString(3, indexDesc);
			ps.setString(4, flag);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next())
				indexId = rs.getLong(1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return indexId;
	}

	public Long getVaultDBName(String dbName) {
		boolean b = false;
		Long dbId = 0l;
		PreparedStatement ps = null;
		try {
			Statement stmt = null;
			String query = "select DB_ID,DB_NAME from CSR_DATABASE where DB_NAME = ?";

			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, dbName);
			// stmt = db.getDBConnection().createStatement();
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				dbId = rs.getLong(1);
				dbName = rs.getString(2);
				b = true;
			}

		} catch (Exception e) {

		}
		return dbId;
	}

	public String getDBName(Long userId, Long dbId) {
		System.out.println("Inside getDBName");
		String dbName = "";
		try {
			String flag = data.CheckEncryptData("N");
			Statement stmt = null;
			String query = null;
			if (userId != 0)
				query = "select m.DB_NAME from CSR_DATABASE m,USER_DATABASE d where m.DB_ID=d.DB_ID and d.USER_ID = "
						+ userId + " and d.DELETE_FLAG='" + flag + "'";
			if (userId != 0 && dbId != 0)
				query = "select m.DB_NAME from CSR_DATABASE m,USER_DATABASE d where m.DB_ID=d.DB_ID and d.DB_ID = "
						+ dbId
						+ " and d.USER_ID = "
						+ userId
						+ " and d.DELETE_FLAG='" + flag + "'";
			if (query != null) {
				stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					if (dbId != 0)
						dbName = rs.getString(1);
					else
						dbName += rs.getString(1) + ",";
				}
			}

		} catch (Exception e) {

		}
		return dbName;
	}

	public String getIndexName(Long dbId, Long idxId) {
		String idxName = "";
		try {
			String flag = data.CheckEncryptData("N");
			Statement stmt = null;
			String query = null;
			query = "select INDEX_NAME from VP_DATABASE_INDEX where DB_ID= "
					+ dbId + " and INDEX_ID= " + idxId;
			if (query != null) {
				stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					idxName = rs.getString(1);
				}
			}

		} catch (Exception e) {

		}
		return idxName;
	}
	public Long getIndexId(Long dbId, String idxName) {
		Long idxId = 0L;
		try {
			String flag = data.CheckEncryptData("N");
			Statement stmt = null;
			String query = null;
			query = "select INDEX_ID from VP_DATABASE_INDEX where DB_ID= "
					+ dbId + " and INDEX_NAME= " + idxName;
			if (query != null) {
				stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					idxId = rs.getLong(1);
				}
			}

		} catch (Exception e) {

		}
		return idxId;
	}

	public String getIndexDesc(Long dbId, Long idxId) {
		String idxName = "";
		try {
			String flag = data.CheckEncryptData("N");
			Statement stmt = null;
			String query = null;
			query = "select INDEX_DESC from VP_DATABASE_INDEX where DB_ID= "
					+ dbId + " and INDEX_ID= " + idxId;
			if (query != null) {
				stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					idxName = rs.getString(1);
				}
			}

		} catch (Exception e) {

		}
		return idxName;
	}

	public String getIndexDescforName(Long dbId, String idxName) {
		String idxDesc = "";
		try {
			System.out.println("Inside getIndexDescforName::dbId:"+dbId+" idxname:"+idxName);
			String flag = data.CheckEncryptData("N");
			Statement stmt = null;
			String query = null;
			query = "select INDEX_DESC from VP_DATABASE_INDEX where DB_ID= "
					+ dbId + " and INDEX_NAME= '"+idxName+"'";
			if (query != null) {
				stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					idxDesc = rs.getString(1);
				}
			}

		} catch (Exception e) {

		}
		return idxDesc;
	}

	public String getDBDesc(Long userId, Long dbId) {
		String dbName = "";
		try {
			String flag = data.CheckEncryptData("N");
			Statement stmt = null;
			String query = null;
			if (userId != 0)
				query = "select m.DB_NAME,m.DB_DESC from CSR_DATABASE m,USER_DATABASE d where m.DB_ID=d.DB_ID and d.USER_ID = "
						+ userId + " and d.DELETE_FLAG='" + flag + "'";
			if (userId != 0 && dbId != 0)
				query = "select m.DB_NAME,m.DB_DESC from CSR_DATABASE m,USER_DATABASE d where m.DB_ID=d.DB_ID and d.DB_ID = "
						+ dbId
						+ " and d.USER_ID = "
						+ userId
						+ " and d.DELETE_FLAG='" + flag + "'";
			if (query != null) {
				stmt = db.getDBConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					if (dbId != 0)
						dbName = rs.getString(2);
					else
						dbName += rs.getString(2) + ",";
				}
			}

		} catch (Exception e) {

		}
		return dbName;
	}

	public String getDBDesc(Long dbId)throws Exception {
		String dbDesc = "";
		String flag = data.CheckEncryptData("N");
		Statement stmt = null;
		String query = null;
		query = "select m.DB_NAME,m.DB_DESC from CSR_DATABASE m where m.DB_ID = "+ dbId;
		if (query != null) {
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				dbDesc = rs.getString(2);
			}
		}
		return dbDesc;
	}

	public String getDBName(Long dbId)throws Exception {
		String dbName = "";
		String flag = data.CheckEncryptData("N");
		Statement stmt = null;
		String query = null;
		query = "select m.DB_NAME,m.DB_DESC from CSR_DATABASE m where m.DB_ID = "+ dbId;
		if (query != null) {
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				dbName = rs.getString(1);
			}
		}
		return dbName;
	}

	public String getRoleName(Long userID) {
		String roleName = "";
		try {
			String flag = data.CheckEncryptData("N");
			Statement stmt = null;
			String query = "select m.ROLE_NAME from MASTER_ROLES m,USER_ROLES d where m.ROLE_ID=d.ROLE_ID and d.USER_ID = '"
					+ userID + "' and DELETE_FLAG = '" + flag + "'";
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				roleName += rs.getString(1) + ",";
			}

		} catch (Exception e) {

		}
		return roleName;
	}

	public List getRoles() {

		List roles = new ArrayList();
		Statement stmt = null;
		System.out.println("UserDAO:getRoles");
		try {
			String query = "select ROLE_ID,ROLE_NAME,ROLE_DESC from MASTER_ROLES";
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				UserDTO cdao = new UserDTO();
				System.out.println("selecting roles" + rs.getString(2));
				cdao.setRoleId(rs.getLong(1));
				cdao.setRoleName(rs.getString(2));
				cdao.setRoleDesc(rs.getString(3));
				roles.add(cdao);
			}

			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return roles;
	}

	public List getcsrRoles() {

		List roles = new ArrayList();
		Statement stmt = null;
		System.out.println("getcsrRoles...");
		try {
			//String query = "select ROLE_ID,ROLE_NAME,ROLE_DESC from MASTER_ROLES where ROLE_NAME not in('Administrator','Profile Admin')";

			String query = "select ROLE_ID,ROLE_NAME,ROLE_DESC from MASTER_ROLES where ROLE_NAME  in('CSR')";
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				UserDTO cdao = new UserDTO();
				System.out.println("selecting roles" + rs.getString(2));
				cdao.setRoleId(rs.getLong(1));
				cdao.setRoleName(rs.getString(2));
				cdao.setRoleDesc(rs.getString(3));
				roles.add(cdao);
			}

			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return roles;
	}

	public List getRole(Long userId) {

		List roles = new ArrayList();
		Statement stmt = null;
		System.out.println("UserDAO:getRoles");
		try {
			String flag = data.CheckEncryptData("N");
			String query = "select r.ROLE_ID,r.ROLE_NAME,r.ROLE_DESC from MASTER_ROLES r,USER_ROLES u where r.ROLE_ID = u.ROLE_ID and u.DELETE_FLAG='"
					+ flag + "' and u.USER_ID=" + userId;
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				UserDTO dto = new UserDTO();

				dto.setRoleId(rs.getLong(1));
				dto.setRoleName(rs.getString(2));
				dto.setRoleDesc(rs.getString(3));
				roles.add(dto);
				System.out.println("selecting roles" + rs.getString(2));
			}

			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return roles;
	}

	public boolean isRole(Long userId, Long roleId) {

		boolean roles = false;
		Statement stmt = null;
		System.out.println("UserDAO:isRole(Long, Long)" + userId);
		try {
			String flag = data.CheckEncryptData("N");
			String query = "select r.ROLE_ID,r.ROLE_NAME,r.ROLE_DESC from MASTER_ROLES r,USER_ROLES u,USER_MGMT um where r.ROLE_ID = u.ROLE_ID and um.USER_ID=u.USER_ID and u.USER_ID="
					+ userId + " and u.ROLE_ID=" + roleId;
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			if (rs.next()) {
				roles = true;
			}

			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return roles;
	}

	public List<UserDTO> getDatabases() {
		System.out.println("Inside getDatabases");
		List<UserDTO> dbs = new ArrayList<UserDTO>();
		Statement stmt = null;
		try {
			String query = "select DB_ID,DB_NAME,DB_DESC from CSR_DATABASE";
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				UserDTO dto = new UserDTO();
				dto.setDbId(rs.getLong(1));
				dto.setDbName(rs.getString(2));
				dto.setDbDesc(rs.getString(3));
				dbs.add(dto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dbs;
	}

	public List<UserDTO> getProfileDatabases(Long profileId) {
		System.out.println("Inside getProfileDatabases");
		List<UserDTO> dbs = new ArrayList<UserDTO>();
		Statement stmt = null;
		try {
			String delflag = data.CheckEncryptData("N");
			String query = "select csr.DB_ID,csr.DB_NAME,csr.DB_DESC from CSR_DATABASE csr " +
					"where csr.DB_ID IN(select distinct pg.DB_ID from PROFILE_GROUP pg where " +
					"PROFILE_ID = "+profileId+" and DELETE_FLAG ='"+delflag+"')";
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				UserDTO dto = new UserDTO();
				dto.setDbId(rs.getLong(1));
				dto.setDbName(rs.getString(2));
				dto.setDbDesc(rs.getString(3));
				dbs.add(dto);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dbs;
	}

	public List<String> getColumnNames(String tableName)
	{	
		DBConnectionDAO db = new DBConnectionDAO();
		System.out.println("getColumnNames!!!!!!!" + tableName);
		List<String> columns = new ArrayList<String>();
		PreparedStatement ps = null;
		try {
			if (tableName != null) {
				String query = "SELECT COLUMN_NAME FROM [CSR-APP].INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ? and COLUMN_NAME NOT IN('RID','Flag')";
				ps = db.getDBConnection().prepareStatement(query);
				ps.setString(1, tableName);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					columns.add(rs.getString(1));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return columns;

	}

	public List<UserDTO> getDatabase(Long userId) {

		List dbs = new ArrayList();
		Statement stmt = null;
		System.out.println("getDatabase:userId" + userId);
		try {
			String flag = data.CheckEncryptData("N");
			String query = "select b.DB_ID,b.DB_NAME,b.DB_DESC from CSR_DATABASE b,USER_DATABASE u where b.DB_ID = u.DB_ID and u.DELETE_FLAG='"
					+ flag + "' and USER_ID=" + userId;
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				UserDTO dto = new UserDTO();

				dto.setDbId(rs.getLong(1));
				dto.setDbName(rs.getString(2));
				System.out.println("selecting database" + rs.getString(2));
				dto.setDbDesc(rs.getString(3));
				dbs.add(dto);
			}
			System.out.println("getDatabase:userId" + userId);

			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dbs;
	}

	public boolean getUserDatabase(Long userId, Long dbId) {

		boolean dbs = false;
		Statement stmt = null;
		System.out.println("getUserDatabase" + userId);
		try {
			String query = "select b.DB_ID,b.DB_NAME,b.DB_DESC from CSR_DATABASE b,USER_DATABASE u,USER_MGMT um where um.USER_ID=u.USER_ID and b.DB_ID = u.DB_ID and u.DB_ID="
					+ dbId + " and u.USER_ID=" + userId;
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {

				dbs = true;
			}
			System.out.println("getUserDatabase" + userId);

			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dbs;
	}

	public Long getDatabaseId(String dbName) {

		Long dbId = 0L;
		Statement stmt = null;
		System.out.println("Inside getDatabaseId for dbName::" + dbName);
		try {
			String query = "select b.DB_ID,b.DB_NAME,b.DB_DESC from CSR_DATABASE b where b.DB_NAME='"
					+ dbName + "'";
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				dbId = rs.getLong(1);
				System.out.println("dbId of database=" + rs.getLong(1));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dbId;
	}

	public List getActions(Long userId) {

		List actions = new ArrayList();
		Statement stmt = null;
		System.out.println("getDatabase:userId" + userId);
		try {
			String flag = data.CheckEncryptData("N");
			String query = "select ua.ACTION_ID,ca.ACTION_NAME from MASTER_ACTIONS ca,USER_ACTION ua where ca.ACTION_ID = ua.ACTION_ID and ua.DELETE_FLAG='"
					+ flag + "' and USER_ID=" + userId;
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				UserDTO dto = new UserDTO();
				System.out.println("selecting database" + rs.getString(2));
				dto.setActionId(rs.getLong(1));
				dto.setActionName(rs.getString(2));

				actions.add(dto);
			}

			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return actions;
	}

	public Long createProfile(String name) {

		PreparedStatement ps = null;
		Long indexId = 0l;
		try {
			String flag = data.CheckEncryptData("N");
			SimpleDateFormat formatter = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String crDate = data.CheckEncryptData(s);
			String query = "INSERT INTO PROFILE(NAME, CREATE_DATE, DELETE_FLAG) values(?,?,?)";

			ps = db.getDBConnection().prepareStatement(query,
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, name);
			ps.setString(2, crDate);
			ps.setString(3, flag);
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next())
				indexId = rs.getLong(1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return indexId;
	}

	public Long isProfileExists(String name) {

		PreparedStatement ps = null;
		Long pId = 0L;
		try {
			String delflag = data.CheckEncryptData("N");
			String query = "SELECT PROFILE_ID FROM PROFILE WHERE NAME=? and DELETE_FLAG='"+delflag+"'";
			System.out.println("ProfileName inside exists method???:::"+name);
			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, name);
			ps.execute();
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				pId = rs.getLong(1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("ProfileExists"+pId);
		return pId;
	}

	public void createProfileGroup(Long pid, Long dbId, Long gid, String group, Long uid, String admin_flag) {
		System.out.println("Inside createProfileGroup");
		PreparedStatement ps = null;

		try {
			String flag = data.CheckEncryptData("Y");
			String delflag = data.CheckEncryptData("N");
			String query = "INSERT INTO PROFILE_GROUP(PROFILE_ID,DB_ID,GROUP_ID,GROUP_NAME,USER_ID,ADMIN_FLAG,DELETE_FLAG) values(?,?,?,?,?,?,?)";

			ps = db.getDBConnection().prepareStatement(query,
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setLong(1, pid);
			ps.setLong(2, dbId);
			ps.setLong(3, gid);
			ps.setString(4, group);
			ps.setLong(5, uid);
			ps.setString(6, admin_flag);
			ps.setString(7, delflag);

			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public void deleteGroupFromProfile(Long pid, Long gid, String group) {
		System.out.println("Inside deleteGroupFromProfile");
		PreparedStatement ps = null;

		try {
			String delflag = data.CheckEncryptData("Y");
			String query = "UPDATE PROFILE_GROUP SET DELETE_FLAG =? WHERE " +
					" PROFILE_ID = ? AND GROUP_ID = ? AND GROUP_NAME = ?" +
					" AND ADMIN_FLAG='Y'";

			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, delflag);
			ps.setLong(2, pid);
			ps.setLong(3, gid);
			ps.setString(4, group);
			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteDbFromProfile(Long pid, Long dbid) {
		System.out.println("Inside deleteDbFromProfile");
		PreparedStatement ps = null;

		try {
			String delflag = data.CheckEncryptData("Y");
			String query = "UPDATE PROFILE_GROUP SET DELETE_FLAG =? WHERE " +
					" PROFILE_ID = ? AND DB_ID = ?" +
					" AND ADMIN_FLAG='Y'";

			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, delflag);
			ps.setLong(2, pid);
			ps.setLong(3, dbid);
			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void AddPrinter(String printerName) {

		PreparedStatement ps = null;

		try {
			// Statement stmt = null;
			String flag = data.CheckEncryptData("Y");
			String query = "INSERT INTO PRINTER(NAME) values(?)";

			ps = db.getDBConnection().prepareStatement(query,
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, printerName);


			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();
		}
		// return indexId;
	}


	public void RemovePrinter(String printerName) {

		System.out.println("Printer Name os========"+printerName);
		Statement ps = null;

		try {
			// Statement stmt = null;

			String query = "DELETE FROM PRINTER WHERE NAME='"+printerName+"'";

			ps = db.getDBConnection().createStatement(
					);



			ps.executeUpdate(query);

		} catch (Exception e) {
			e.printStackTrace();
		}
		// return indexId;
	}



	public List<String> getAllPrinters()
	{PreparedStatement ps = null;
	List<String> printers=new ArrayList<String>();
	try
	{
		String query = "SELECT NAME FROM PRINTER";

		ps = db.getDBConnection().prepareStatement(query);

		//ps.setString(1, printerName);


		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			printers.add(rs.getString(1));
		}

	}

	catch(Exception e)
	{

	}

	return printers;
	}

	public boolean getUserActions(Long userId, Long actionId) {

		boolean actions = false;
		Statement stmt = null;
		System.out.println("getDatabase:userId" + userId);
		try {
			String query = "select ua.ACTION_ID,ca.ACTION_NAME from MASTER_ACTIONS ca,USER_ACTION ua,USER_MGMT um where um.USER_ID=ua.USER_ID and ca.ACTION_ID = ua.ACTION_ID and ua.USER_ID="
					+ userId + " and ua.ACTION_ID=" + actionId;
			stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) {

				actions = true;
			}

			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return actions;
	}

	public boolean checkTableName(String tablename) {

		boolean exists = false;
		Statement stmt = null;
		System.out.println("inside checkTableName:::" + tablename);
		try {

			DatabaseMetaData md = db.getDBConnection().getMetaData();
			ResultSet rs = md.getTables(null, null, tablename, null);
			while (rs.next()) {
				System.out.println("Table Exists::::"+rs.getString(3));
				exists = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return exists;
	}


	public List<UserDTO> getDataType(String idxName, String tableName) {

		List<UserDTO> types = new ArrayList<UserDTO>();
		UserDTO udto = new UserDTO();
		PreparedStatement ps = null;
		try {
			String query = "select DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,IS_NULLABLE from [CSR-APP].INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ? and COLUMN_NAME = ?";
			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, tableName);
			ps.setString(2, idxName);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				udto.setDataType(rs.getString(1));
				udto.setWidth(rs.getString(2));
				udto.setIs_nullable(rs.getString(3));
				types.add(udto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return types;
	}

	public String getonlyDataType(String idxName, String tableName) {

		String dataType = "";
		PreparedStatement ps = null;
		try {
			String query = "select DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,IS_NULLABLE from [CSR-APP].INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ? and COLUMN_NAME = ?";
			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, tableName);
			ps.setString(2, idxName);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				dataType = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dataType;
	}

	public String createTable(String tableName, String query, String message1) {
		String message = "";
		Statement createstmt = null;
		try {
			String insertquery = "CREATE TABLE "+tableName+" ("+query+")";
			System.out.println("Final create table query???:::"+insertquery);
			createstmt = db.getDBConnection().createStatement();
			createstmt.execute(insertquery);
		}catch (SQLException e) {
			message = "SQL error message: "+e.getMessage();
			e.printStackTrace();
		}finally{
			message1 = message;
		}
		return message1;
	}

	public List<String> getProfilesForUser(String username)
	{
		List<String> profileNameList=new ArrayList<String>();
		String query="select p.NAME from Profile p";
		try{
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				profileNameList.add(rs.getString(1));
			}
		}
		catch (Exception ee) {
			ee.printStackTrace();
		}
		return profileNameList;
	}

	public String getProfileForUser(String username)
	{
		CommonDAO cDAO = new CommonDAO(); 
		String profileName="";
		Long userId = 0L;
		String username1 = username.toLowerCase();
		System.out.println("getProfileForUser >> username::"+username1);
		try{
			String uname = cDAO.CheckEncryptData(username1);
			String flag = cDAO.CheckEncryptData("N");
			String query = "SELECT distinct USER_ID FROM USER_MGMT WHERE USER_NAME = '"+uname+"' and DELETE_FLAG = '"+flag+"'";
			Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			if(rs.next()) {
				userId = rs.getLong(1);
			}
			rs.close();
			stmt.close();
			System.out.println("userID::"+userId);
			if(userId!=0L){
				String query1="select distinct p.NAME from PROFILE p, profile_group pg where pg.PROFILE_ID = p.PROFILE_ID and pg.USER_ID ="
						+userId;
				Statement stmt1 = db.getDBConnection().createStatement();
				ResultSet rs1 = stmt1.executeQuery(query1);
				if(rs1.next()) {
					profileName = rs1.getString(1);
				}
				rs1.close();
				stmt1.close();
			}
		}
		catch (Exception ee) {
			ee.printStackTrace();
		}
		return profileName;
	}

	public Long getprofileDbData(Long profileId, Long dbId) {
		System.out.println("Inside getprofileDbData:"+profileId+"::"+dbId);
		boolean b = false;
		Long pdId = 0l;
		PreparedStatement ps = null;
		try {
			Statement stmt = null;
			String query = "select INC_ID from PROFILE_DB_DATA where PROFILE_ID = ? and DB_ID = ?";
			ps = db.getDBConnection().prepareStatement(query);
			ps.setLong(1, profileId);
			ps.setLong(2, dbId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				pdId = rs.getLong(1);
				b = true;
			}

		} catch (Exception e) {

		}
		return pdId;
	}
	public Long getprofileId(String profileName) {
		System.out.println("Inside getProfileId:"+profileName);
		boolean b = false;
		Long profileId = 0l;
		PreparedStatement ps = null;
		try {
			Statement stmt = null;
			String query = "select PROFILE_ID,PROFILE_NAME from PROFILE_METADATA where PROFILE_NAME = ?";

			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, profileName);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				profileId = rs.getLong(1);
				profileName = rs.getString(2);
				b = true;
			}

		} catch (Exception e) {

		}
		return profileId;
	}

	public Long getuserprofileId(String profileName) {
		System.out.println("Inside getProfileId:"+profileName);
		boolean b = false;
		Long profileId = 0l;
		PreparedStatement ps = null;
		try {
			Statement stmt = null;
			/* Nithu Alexander: 14th May 2015
			 * Bug fix for Profile Admin not able to see Profile users created
			 * Reason: Wrong profile ID return by query when similar profile is created again
			 * Code Fix: Including ORDER BY (ASC) clause in below query to obtain correct Profile ID
			 * */
			String query = "select PROFILE_ID,NAME from PROFILE where NAME = ? ORDER BY PROFILE_ID ASC" ;
			
			System.out.println("getUserprofileId Query --------->"+query);

			ps = db.getDBConnection().prepareStatement(query);
			ps.setString(1, profileName);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				profileId = rs.getLong(1);
				profileName = rs.getString(2);
				b = true;
			}

		} catch (Exception e) {

		}
		return profileId;
	}

	public Long saveProfile( String profileName) {
		System.out.println("Inside saveProfile");
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		Long profileId = 0l;
		try {
			String query = "INSERT INTO PROFILE_METADATA(PROFILE_NAME)values(?)";

			ps = db.getDBConnection().prepareStatement(query,
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, profileName);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next())
				profileId = rs.getLong(1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return profileId;
	}
	public Long saveProfileDbData( Long profileId, Long dbId) {
		System.out.println("Inside saveProfileDbData:"+profileId+"::"+dbId);
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		Long pdId = 0l;
		try {
			String query = "INSERT INTO PROFILE_DB_DATA(PROFILE_ID,DB_ID)values(?,?)";
			ps = db.getDBConnection().prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setLong(1, profileId);
			ps.setLong(2, dbId);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next())
				pdId = rs.getLong(1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return pdId;
	}
	private String createInQuery(List<String> groups,String realpath) throws Exception

	{
		CommonDAO cDAO = new CommonDAO();
		String str="(";
		for (String string : groups) {
			str+="'"+cDAO.CheckEncryptData(string)+"'"+",";
		}

		String trimmed=str.substring(0,str.length()-1);
		trimmed+=")";

		return trimmed;
	}
	public List<DataDTO> getIndexByDBID(Long dbId,Long hostId)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = db.getDBConnection();
		PreparedStatement ps = null;
		try
		{
			ps = conn.prepareStatement("select d.INDEX_ID,d.INDEX_NAME,d.INDEX_DESC,c.DB_ID,c.DB_DESC from DATABASE_INDEX d join CSR_DATABASE c on d.DB_ID = c.DB_ID where  d.DB_ID = ? and c.HOST_ID=?");
			ps.setLong(1, dbId);
			ps.setLong(2,hostId);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DataDTO dto = new DataDTO();
				dto.setIndexId(rs.getLong(1));
				dto.setIndexName(rs.getString(2));
				dto.setIndexDesc(rs.getString(3));
				dto.setDbId(rs.getLong(4));
				dto.setDbDesc(rs.getString(5));
				list.add(dto);
			}


		}

		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;

	}

	public Long getVaultDBName(String dbName,Long hostId) {
		System.out.println("getting database::"+dbName+"hostID::"+hostId);
		Long dbId = 0l;
		PreparedStatement ps = null;
		Connection conn = db.getDBConnection();
		try {
			if(conn != null)
			{
				String query = "select DB_ID,DB_NAME from CSR_DATABASE where DB_NAME = ? and HOST_ID=?";

				ps =  conn.prepareStatement(query);
				ps.setString(1, dbName);
				ps.setLong(2, hostId);
				//stmt = db.getDBConnection().createStatement();
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					dbId = rs.getLong(1);
					dbName = rs.getString(2);

				}
			}

		} catch (Exception e) {

		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return dbId;
	}

	public Long saveDBName(String dbName, String dbDesc,Long hostId) {
		PreparedStatement ps = null;
		Connection conn = db.getDBConnection();
		Long dbId = 0l;
		try {
			//Statement stmt = null;
			System.out.println("Saving database::"+dbName+"hostID::"+hostId);
			if(conn != null)
			{
				String query = "INSERT INTO CSR_DATABASE(DB_NAME,DB_DESC,HOST_ID) values(?,?,?)";

				ps = conn.prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, dbName);
				ps.setString(2, dbDesc);
				ps.setLong(3, hostId);
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next())
					dbId = rs.getLong(1);


			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {

		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return dbId;
	}

	public boolean saveVaultAppIndex(Long dbId, String[] Indexes,String[] indexDesc) {
		CommonDAO dao = new CommonDAO();
		boolean b = true;
		try {
			System.out.println("saveIndex" + dbId);
			if (dbId != 0) {
				String flag = "Y";
				for (int i = 0; i < Indexes.length; i++) {
					String hash = AeSimpleSHA1.SHA1(dbId + ","
							+ data.CheckEncryptData(Indexes[i]) + ","
							+ data.CheckEncryptData(indexDesc[i]));
					String query = "INSERT INTO VP_DATABASE_INDEX(DB_ID,HASH_ID,INDEX_NAME,INDEX_DESC,INDEX_FLAG) VALUES(?, ?, ?, ?, ?);";
					PreparedStatement stmt = db.getDBConnection()
							.prepareStatement(query,
									PreparedStatement.RETURN_GENERATED_KEYS);
					stmt.setLong(1, dbId);
					stmt.setString(2, hash);
					stmt.setString(3, Indexes[i]);
					stmt.setString(4, indexDesc[i]);
					stmt.setString(5, dao.CheckEncryptData(flag));
					stmt.executeUpdate();
					ResultSet rs = stmt.getGeneratedKeys();
					if (!rs.next()) {
						b = false;
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}

	public boolean saveJrnDetails(String FileName) {
		CommonDAO dao = new CommonDAO();
		boolean b = true;
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
		String date1 = formatter.format(date);
		try {
			System.out.println("saveJrnDetails::" + FileName);
			if (FileName != "") {

				String query = "INSERT INTO JRN_DATA_INTEGRITY(PROCESSED_DATE,FILE_NAME,STATUS) VALUES(?, ?, ?)";
				PreparedStatement stmt = db.getDBConnection().prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
				stmt.setString(1, date1);
				stmt.setString(2, FileName);
				stmt.setString(3, "Under Process");
				stmt.executeUpdate();
				ResultSet rs = stmt.getGeneratedKeys();
				if (!rs.next()) {
					b=false;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}




	public boolean saveJrnDetailsForTimer(String FileName,String str) {
		CommonDAO dao = new CommonDAO();
		boolean b = true;
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
		String date1 = formatter.format(date);
		try {
			System.out.println("saveJrnDetails::" + FileName);
			if (FileName != "") {

				String query = "INSERT INTO JRN_DATA_INTEGRITY(PROCESSED_DATE,FILE_NAME,STATUS) VALUES(?, ?, ?)";
				PreparedStatement stmt = db.getDBConnection().prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
				stmt.setString(1, date1);
				stmt.setString(2, FileName);
				if(str.equalsIgnoreCase("Completed"))
				{
					stmt.setString(3, "Completed");
				}
				else
				{
					stmt.setString(3, "Under Process");
				}
				stmt.executeUpdate();
				ResultSet rs = stmt.getGeneratedKeys();
				if (!rs.next()) {
					b=false;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}



	public int getJrnStatus(String fileName) {
		System.out.println("Inside getJrnStatus::"+fileName);
		PreparedStatement ps = null;
		int count = 0;
		Connection conn = db.getDBConnection();
		try {
			if(conn != null)
			{
				String query = "select count(*) from JRN_DATA_INTEGRITY where STATUS = ? and FILE_NAME = ?";
				ps =  conn.prepareStatement(query);
				ps.setString(1, "Under Process");
				ps.setString(2, fileName);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					count = rs.getInt(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return count;
	}

	public boolean updateJrnDetails(String FileName) {
		CommonDAO dao = new CommonDAO();
		boolean b = true;
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
		String date1 = formatter.format(date);
		try {
			System.out.println("updateJrnDetails::" + FileName);
			if (FileName != "") {

				String query = "UPDATE JRN_DATA_INTEGRITY SET STATUS= ? WHERE FILE_NAME=?";
				PreparedStatement stmt = db.getDBConnection().prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
				stmt.setString(1, "Completed");
				stmt.setString(2, FileName);
				stmt.executeUpdate();
				ResultSet rs = stmt.getGeneratedKeys();
				if (!rs.next()) {
					b=false;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}
	/**
	 * Added isuru
	 * @param profileId
	 */

	public void updateProfileGroupEditAdmin(Long profileId,Long userId)
	{
		String sql="update  profile_group set admin_flag='N' where profile_id="+profileId +" and user_id="+userId ;

		try {
			PreparedStatement stmt = db.getDBConnection().prepareStatement(sql);
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Exception1"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteAdminFromDB()
	{

		/*try {
			 uname=	data.CheckEncryptData(groupName);
			} catch (Exception e) {

				System.out.println("Exception2"+e);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/

		Long rid=	getRoleIdbyRoleDescription(CommonConstants.ROLE_DESC_ADMIN);
		String sql="delete from user_mgmt where user_id in (select user_id from user_roles where role_id=?";

		try {
			PreparedStatement stmt = db.getDBConnection().prepareStatement(sql);
			stmt.setLong(1, rid);
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Exception1"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}






		String sql3="delete from profile_group where profile_id in (select profile_id from profile_group where admin_flag='Y' and user_id in (select user_id from user_roles where role_id=?))";

		try {
			PreparedStatement stmt = db.getDBConnection().prepareStatement(sql3);
			stmt.setLong(1, rid);
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Exception1"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}










		String sql2="delete from  user_roles where role_id=?";





		try {
			PreparedStatement stmt = db.getDBConnection().prepareStatement(sql2);
			stmt.setLong(1, rid);
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Exception1"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public Long getRoleIdbyRoleDescription(String description)
	{

		PreparedStatement ps=null;
		String	query = "select role_id from master_roles where role_desc='"+description+"'" ;

		Long roleid=0l;
		try
		{
			ps = db.getDBConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				roleid= rs.getLong(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return roleid;
	}

	public Long createGroup(String groupName)
	{
		String uname=null;
		try {
			uname=	data.CheckEncryptData(groupName);
		} catch (Exception e) {

			System.out.println("Exception2"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}





		PreparedStatement ps=null;Long uid=0l;
		String sql="insert into user_groups (user_group) values ('"+uname+"')";
		try
		{

			Statement	ps2=db.getDBConnection().createStatement();
			ResultSet rs1 = ps2.executeQuery("select user_grp_id from user_groups where user_group='"+uname+"'");
			while (rs1.next()) {
				uid= rs1.getLong(1);
			}

			if(uid==0l)
			{
				ps = db.getDBConnection().prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
				ps.executeUpdate();
			}
			Statement	ps1=db.getDBConnection().createStatement();
			ResultSet rs = ps1.executeQuery("select user_grp_id from user_groups where user_group='"+uname+"'");
			while (rs.next()) {
				uid= rs.getLong(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Create group return id"+uid);
		return uid;
	}


	public List<Long> getUserActionsByUserId(Long userid)
	{
		List<Long> ids=new ArrayList<Long>();
		String query="select action_id from user_action where user_id="+userid;

		try
		{
			PreparedStatement	ps = db.getDBConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ids.add(rs.getLong(1));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}



		return ids;
	}


	public void deleteuserActions(Long userId)
	{


		//List<Long> ids1=	getUserActionsByUserId(userId);
		/*if(ids1.size()==0)
	{
		for(int i=0;i<ids.size();i++)
		{
		 */	
		try
		{
			String query = "delete from user_actions where user_id="+userId;
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
			stmt.setLong(1, userId);
			//stmt.setString(2, dFlag);
			//stmt.setLong(3,ids.get(i));
			stmt.executeUpdate();
		}
		catch(Exception e)

		{
			e.printStackTrace();
		}
		finally
		{

		}
		//}
		//	}


	}


	public void insertOrUpdateUserActions(Long userId,List<Long> ids)
	{
		String dFlag="N";
		try {
			dFlag=	data.CheckEncryptData(dFlag);
		} catch (Exception e) {

			System.out.println("Exception2"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Long> ids1=	getUserActionsByUserId(userId);
		if(ids1.size()==0)
		{
			for(int i=0;i<ids.size();i++)
			{

				try
				{
					String query = "INSERT INTO user_action (user_id,delete_flag,action_id) VALUES(?,?,?)";
					PreparedStatement stmt = db.getDBConnection().prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
					stmt.setLong(1, userId);
					stmt.setString(2, dFlag);
					stmt.setLong(3,ids.get(i));
					stmt.executeUpdate();
				}
				catch(Exception e)

				{
					e.printStackTrace();
				}
				finally
				{

				}
			}
		}
	}

	public Long getuserIdByUserName(String uname)
	{
		//String delFlag="N"
		PreparedStatement ps=null;Long uid=0l;
		try {
			uname=	data.CheckEncryptData(uname);
		} catch (Exception e) {

			System.out.println("Exception2"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String query="select user_id from user_mgmt where user_name='"+uname+"'";


		try
		{
			ps = db.getDBConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				uid= rs.getLong(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return uid;
	}

	public Long getProfileAdminByProfileId(Long profileId)
	{
		PreparedStatement ps=null;
		String	query = "select user_id from profile_group where ADMIN_FLAG='Y' and profile_id="+profileId ;

		Long uid=0l;
		try
		{
			ps = db.getDBConnection().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				uid= rs.getLong(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return uid;
	}

	public void updateUserRole(Long userId,Long oldRoleid,Long newroleid)
	{
		String query="update user_roles set role_id="+newroleid+" where role_id="+oldRoleid+" and user_id="+userId;
		try {
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query);
			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}


	public void updateProfileAdmin(Long profileId,String userId)
	{
		System.out.println("Update Profile Admin secondpart"+profileId+"==="+userId);
		try {
			userId=	data.CheckEncryptData(userId);
		} catch (Exception e) {

			System.out.println("Exception2"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		String sql="update profile_group set admin_flag='Y' where profile_id="+profileId+" and user_id in (select user_id from user_mgmt where user_name='"+userId+"')";

		System.out.println("sql is updateprofileadmin"+sql);

		try {
			PreparedStatement stmt = db.getDBConnection().prepareStatement(sql);
			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
	
	public boolean updateProfileAdminBatch(String newprofileadminloginid,
									Long profileId){
		
		Long profileadminroleid=this.getRoleIdbyRoleDescription(CommonConstants.ROLE_DESC_PROJECTADMIN);
		Long csrroleid=this.getRoleIdbyRoleDescription(CommonConstants.ROLE_DESC_CSR);
		Long uidodnewpadmin=	this.getuserIdByUserName(newprofileadminloginid);
		Long uid=	this.getProfileAdminByProfileId(profileId);
		
		Connection conn = db.getDBConnection();
		try{
			boolean autoCommit = conn.getAutoCommit();
	
			try{
				
				conn.setAutoCommit(false);
				Statement stmt = conn.createStatement();
				
				String query="update user_roles set role_id="+csrroleid+" where role_id="+profileadminroleid+" and user_id="+uid;
				stmt.addBatch(query);
				
				String sql="update  profile_group set admin_flag='N' where profile_id="+profileId +" and user_id="+uid ;
				stmt.addBatch(sql);
				
				String userId=	data.CheckEncryptData(newprofileadminloginid);
				sql="update profile_group set admin_flag='Y' where profile_id="+profileId+" and user_id in (select user_id from user_mgmt where user_name='"+userId+"')";
				stmt.addBatch(sql);
	
				sql="update user_roles set role_id="+profileadminroleid+" where role_id="+csrroleid+" and user_id="+uidodnewpadmin;
				stmt.addBatch(sql);
				stmt.executeBatch();
				conn.commit();
			
			}catch(Exception e){
				
				conn.rollback();
				return false;
			}finally{
				conn.setAutoCommit(autoCommit);
				conn.close();
			}
		
		
		
		List<Long> l=this.getUserActionsByUserId(uidodnewpadmin);
		this.insertOrUpdateUserActions(uid, l);	
		this.deleteuserActions(uidodnewpadmin);
		
		return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * End added profileid
	 * @param dbId
	 * @param userId
	 * @param index
	 * @return
	 */

	public Long getUserIndexId(Long dbId, Long userId, String index) {
		Long indexId = 0L;
		PreparedStatement ps = null;
		try {
			System.out.println("getUserIndexId" + dbId);
			System.out.println("getUserIndexId" + userId);
			System.out.println("getUserIndexId" + index);
			String query = "";
			String flag = data.CheckEncryptData("Y");
			if (dbId != 0) {

				query = "select ui.USER_INDEX_ID from USER_DB_INDEX ui where ui.INDEX_NAME='"
						+ index + "' and ui.DB_ID=" + dbId+" and ui.USER_ID="+userId;
				ps = db.getDBConnection().prepareStatement(query);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					indexId= rs.getLong(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return indexId;
	}
	
	
	/* Nithu Alexander: 03-Aug-2015
	 * Below method added as part of performance optimisation for Profile - User Assignment
	 * */
	public List<String> getIndexListForProfileAdmin(List<String> indexList, Long dbId, Long userId, String saved, String getConfiguredIndexFlag) {
		PreparedStatement ps = null;
		List<String> configuredIndexList = new ArrayList<String>();
		List<String> nonConfiguredIndexList = new ArrayList<String>();
		List<String> profileAdminIndexList = new ArrayList<String>();

		try {
			String saveflag = data.CheckEncryptData(saved);
			String query = "select ui.USER_INDEX_ID from USER_DB_INDEX ui where ui.INDEX_NAME= ? and ui.USER_ID= ?  and ui.DB_ID= ? and ui.SAVED= ?";
			
			ps = db.getDBConnection().prepareStatement(query);
			
			for (String index : indexList) {
				Long hasIndex = 0L;
				
				System.out.println("getIndexesForProfileAdmin > "+userId+" > " +dbId+" > "+ index);
				
				ps.setString(1, index);
				ps.setLong(2, userId);
				ps.setLong(3, dbId);
				ps.setString(4, saveflag);
				
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					hasIndex = rs.getLong(1);
				}
				if(hasIndex != 0){
					System.out.println("Configured Index ------------>"+ index);
					configuredIndexList.add(index);
				}else{
					nonConfiguredIndexList.add(index);
				}
			}
			
			if(getConfiguredIndexFlag != null && getConfiguredIndexFlag.equalsIgnoreCase("Y")){
				profileAdminIndexList =  configuredIndexList;
			}else{
				profileAdminIndexList = nonConfiguredIndexList;
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return profileAdminIndexList;
	}
	
	
	/* Nithu Alexander: 03-Aug-2015
	 * Below method added as part of performance optimisation for Profile - User Assignment
	 * */
	public void saveIndexesForNewUser(List<String> indexList, Long dbId, Long userId, String saved) {
		Long id = 0L;
		try {
			
			String saveflag = data.CheckEncryptData(saved);
			//
			String query = "INSERT INTO USER_DB_INDEX(USER_ID,HASH_ID,INDEX_NAME,DB_ID,SAVED) VALUES("
					+ "?"
					+ ","
					+ "?"
					+ ","
					+ ""+"?"+""
					+ ","
					+ "?"
					+ ","
					+ "?" + ")";

			PreparedStatement stmt = db.getDBConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			
			for (String index : indexList) {
				
				System.out.println("saveIndexesForNewUser > "+userId+" > " +dbId+" > "+ index);
				
				String hash = AeSimpleSHA1.SHA1(userId + ","+dbId+"," + index);
				
				stmt.setLong(1, userId);
				stmt.setString(2, hash);
				stmt.setString(3, index);
				stmt.setLong(4, dbId);
				stmt.setString(5, saveflag);
				
				System.out.println("saveIndexesForNewUser >>>>> Query >>>>>>"+query);
				
				stmt.executeUpdate();
	
				ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					System.out.println("userIndexid created:" + rs.getLong(1));
					id = rs.getLong(1);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/* Nithu Alexander: 05-Aug-2015
	 * Below method added as part of performance optimisation for Profile - User Assignment
	 * */
	public List<String> getIndexColumnsForProfileAdmin(List<String> indexList, Long dbId, Long userId, String saved, String getConfiguredIndexFlag) {

		PreparedStatement ps = null;
		List<String> configuredIndexColList = new ArrayList<String>();
		List<String> nonConfiguredIndexColList = new ArrayList<String>();
		List<String> profileAdminIndexColList = new ArrayList<String>();
		try {
			
			String saveflag = data.CheckEncryptData(saved);
			String query = "select ui.SER_RES_INDEX_ID from USER_SER_RES_INDEX ui where ui.INDEX_NAME = ? and ui.USER_ID= ?  and ui.DB_ID= ? and ui.SAVED= ?";
			ps = db.getDBConnection().prepareStatement(query);
			
			for (String index : indexList) {
				Long hasIndexColIndex = 0L;
				System.out.println("getIndexColumnsForProfileAdmin > "+userId+" > " +dbId+" > "+ index);
				
				ps.setString(1, index);
				ps.setLong(2, userId);
				ps.setLong(3, dbId);
				ps.setString(4, saveflag);
			
				ResultSet rs = ps.executeQuery();
				
				while (rs.next()) {
					hasIndexColIndex = rs.getLong(1);
				}
				
				if(hasIndexColIndex != 0){
					configuredIndexColList.add(index);
				}else{
					nonConfiguredIndexColList.add(index);
				}
			}
			
			if(getConfiguredIndexFlag != null && getConfiguredIndexFlag.equalsIgnoreCase("Y")){
				profileAdminIndexColList = configuredIndexColList;
			}else{
				profileAdminIndexColList = nonConfiguredIndexColList;
			}
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		return profileAdminIndexColList;

	}
	
	/* Nithu Alexander: 03-Aug-2015
	 * Below method added as part of performance optimisation for Profile - User Assignment
	 * */
	public void saveIndexColumnsForNewUser(List<String> indexList, Long dbId, Long userId, String saved) {
		Long id = 0L;
		try {
			
			String saveflag = data.CheckEncryptData(saved);
			//
			String query = "INSERT INTO USER_SER_RES_INDEX(USER_ID,HASH_ID,INDEX_NAME,DB_ID,SAVED) VALUES("
					+ "?"
					+ ","
					+"?"
					+ ","
					+ ""+"?"+""
					+ ","
					+ "?"
					+ ","
					+ "?" + ")";

			PreparedStatement stmt = db.getDBConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			
			for (String index : indexList) {
				
				System.out.println("saveIndexColumnsForNewUser > "+userId+" > " +dbId+" > "+ index);
				
				String hash = AeSimpleSHA1.SHA1(userId + ","+dbId+"," + index);
				
				stmt.setLong(1, userId);
				stmt.setString(2, hash);
				stmt.setString(3, index);
				stmt.setLong(4, dbId);
				stmt.setString(5, saveflag);

				stmt.executeUpdate();

				ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					System.out.println("usersearchIndexid created:" + rs.getLong(1));
					id = rs.getLong(1);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/* Nithu Alexander: 03-Aug-2015
	 * Below method added as part of performance optimisation for Profile - User Assignment
	 * */
	public Map<String,String> getUserIndexResultsMap(Long userId, Long dbId, List<String> indexList, String ser) {
		
		PreparedStatement ps = null;
		Map<String,String> indexResultMap = new HashMap<String,String>();
		
		try {
			System.out.println("getUserIndexResultsMap:" + userId);
			
			String query = "";
			
			if (userId != 0) {
				query = "select ur.INDEX_RESULTS_PERM,ui.INDEX_NAME,ui.DB_ID from USER_INDEX_RESULTS ur, USER_DB_INDEX ui where ui.INDEX_NAME= ? and ui.DB_ID= ?  and ui.USER_INDEX_ID = ur.USER_INDEX_ID and ui.USER_ID= ?";
				ps = db.getDBConnection().prepareStatement(query);
				
				for(String index : indexList){
					String indexRes = "";
					ps.setString(1, index);
					ps.setLong(2, dbId);
					ps.setLong(3, userId);
					
					ResultSet rs = ps.executeQuery();
	
					while (rs.next()) {
						if (ser.equals("index")){
							if(rs.getString(1) != null){
								indexRes += data.CheckDecryptData(rs.getString(1)).trim() + ", ";
								indexRes = indexRes.trim();
							}
							
						}
						
					}
					System.out.println("getUserIndexResultsMap indexRes: --->" + indexRes);
					if(indexRes != null && !(indexRes.trim().equals(""))){
						indexResultMap.put(index, indexRes);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return indexResultMap;
	}
	
	/* Nithu Alexander: 03-Aug-2015
	 * Below method added as part of performance optimisation for Profile - User Assignment
	 * */
	
	public Map<Long,String> getUserIndexIdMap(Long dbId, Long userId, Map<String,String> indexResultMap) {
		Long indexId = 0L;
		PreparedStatement ps = null;
		Map<Long,String> indexIdMap = new HashMap<Long,String>();
		String indexResults = null;
		try {
			
			String query = "";
			
			if(dbId != 0 && userId != 0){
				query = "select ui.USER_INDEX_ID from USER_DB_INDEX ui where ui.INDEX_NAME= ? and ui.DB_ID= ? and ui.USER_ID= ?";
				ps = db.getDBConnection().prepareStatement(query);
				for(String indexKey : indexResultMap.keySet()){
					indexResults = (String)indexResultMap.get(indexKey);
					
					System.out.println("getUserIndexIdList dbId---->" + dbId);
					System.out.println("getUserIndexIdList userId---->" + userId);
					System.out.println("getUserIndexIdList indexKey---->" + indexKey);
					
					ps.setString(1, indexKey);
					ps.setLong(2, dbId);
					ps.setLong(3, userId);
					ResultSet rs = ps.executeQuery();
					while (rs.next()) {
						indexId= rs.getLong(1);
						if(indexResults != null && !(indexResults.trim().equals(""))){
							indexIdMap.put(indexId, indexResults);
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return indexIdMap;
	}
	
	/* Nithu Alexander: 03-Aug-2015
	 * Below method added as part of performance optimisation for Profile - User Assignment
	 * */
	
	public void saveIndexResultsList(Map<Long,String> indexIdMap) {
		Long id = 0L;
		String indexResults = null;
		try {
			
			if(indexIdMap != null && !(indexIdMap.isEmpty())){		
				String query = "INSERT INTO USER_INDEX_RESULTS(USER_INDEX_ID,HASH_ID,INDEX_RESULTS_PERM) VALUES("
						+ "?"
						+ ","
						+ "?"
						+ ","
						+ "?" + ")";
				PreparedStatement stmt = db.getDBConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				for(Long indexId : indexIdMap.keySet()){
					indexResults = (String)indexIdMap.get(indexId);
					
					System.out.println("indexId ---------->" + indexId);
					System.out.println("indexResults ---------->" + indexResults);
					//String hash = AeSimpleSHA1.SHA1(indexId + ","+ data.CheckEncryptData(indexResults));
					if(indexResults != null && !(indexResults.trim().equals(""))){
						String[] indexResultArr = indexResults.split(",");
						for(String resultStr : indexResultArr){
							System.out.println("resultStr ---------->" + resultStr);
							String hash = AeSimpleSHA1.SHA1(indexId + ","+ data.CheckEncryptData(resultStr));
							
							stmt.setLong(1, indexId);
							stmt.setString(2, hash);
							stmt.setString(3, data.CheckEncryptData(resultStr));
							stmt.executeUpdate();
							
							ResultSet rs = stmt.getGeneratedKeys();
							if (rs.next()) {
								System.out.println("index results ID created:" + rs.getLong(1));
								id = rs.getLong(1);
							}
						}
					}
					//indexIdMap.remove(indexId);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
}
