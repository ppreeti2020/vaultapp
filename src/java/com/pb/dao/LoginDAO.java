package com.pb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Properties;
import java.text.*;
import java.util.Date;
import java.sql.Timestamp;

import com.pb.common.AeSimpleSHA1;
import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;
import com.pb.common.PropertyUtils;

public class LoginDAO {

	DBConnectionDAO db = new DBConnectionDAO();
	// CommonDBConnection commonDB = new CommonDBConnection();
	DataEncryptDecrypt data = new DataEncryptDecrypt();
	AeSimpleSHA1 sha = new AeSimpleSHA1();
	Date date = new Date();

	public long saveLoginDetails(HashMap map) throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();
		long save = 0;
		Statement stmt = null;
		String groupName = "";
		String loginName = "";
		String IP = "";
		String browser = "";
		String success = "";
		String hash = "";
		String rowforHash = "";
		String loginDate = "";

		try {
			Timestamp tsReturn = null;
			System.out.println("group" + map.get("group"));
			System.out.println("name" + map.get("name") + date.getTime());
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			System.out.println("Login Date:::" +s);
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			if(encrypt.equalsIgnoreCase("YES")){
				if (map.get("group") != null) {
					groupName = data.encryptData(map.get("group").toString());
				}
				if (map.get("name") != null) {
					loginName = data.encryptData(map.get("name").toString());
				}
				if (map.get("IP") != null) {
					IP = data.encryptData(map.get("IP").toString());
				}
				if (map.get("browser") != null) {
					browser = data.encryptData(map.get("browser").toString());
				}
				if (map.get("success") != null) {
					success = data.encryptData(map.get("success").toString());
				}
				
				loginDate = data.encryptData(s);
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				if (map.get("group") != null) {
					groupName = map.get("group").toString();
				}
				if (map.get("name") != null) {
					loginName = map.get("name").toString();
				}
				if (map.get("IP") != null) {
					IP = map.get("IP").toString();
				}
				if (map.get("browser") != null) {
					browser = map.get("browser").toString();
				}
				if (map.get("success") != null) {
					success = map.get("success").toString();
				}
				
				loginDate = s;
			}
			
			System.out.println("********" + success);
			rowforHash = groupName + "," + loginName + "," + loginDate + ","
					+ IP + "," + browser + "," + success;
			hash = sha.SHA1(rowforHash);
			String query = "INSERT INTO LOGIN_DETAILS(Group_Name,Login_Name,Login_Date,IP,Browser,Login_Time,SUCCESS_FLAG,HASH_ID) VALUES(?, ?, ?, ?, ?,DEFAULT, ?, ?);";
			ps = conn.prepareStatement(query,
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, groupName);
			ps.setString(2, loginName);
			ps.setString(3, loginDate);
			ps.setString(4, IP);
			ps.setString(5, browser);
			ps.setString(6, success);
			ps.setString(7, hash);
			// stmt.execute(query,Statement.RETURN_GENERATED_KEYS);
			ps.executeUpdate();
			System.out.println("saveLoginDetails--->Saved");
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				save = rs.getLong(1);
				System.out.println("saveLoginDetails--generatedKeys: " + save);
				saveLogIntegrity(CommonConstants.LOGIN_TAB_INTEGRITY_CHECK,
						"LOG_ID", hash, hash, save, "yes");
			}

			// stmt.
		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			ps.close();
			conn.close();
		}
		return save;
	}

	public void saveLogIntegrity(String tableName, String ID, String newHash,
			String oldHash, Long logId, String flag) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Statement stmt = db.getDBConnection().createStatement();
			String query1 = "INSERT INTO "
					+ tableName
					+ "(INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG,LOG_ID) VALUES('"
					+ newHash + "','" + oldHash + "','" + s + "',DEFAULT,'"
					+ flag + "'," + logId + ");";
			stmt.execute(query1);
		} catch (Exception e) {

		}

	}

	public long saveLoggedDetails(HashMap map) throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = db.getDBConnection();

		long save = 0;
		Statement stmt = null;
		String logged = "";
		String hash = "";
		try {

			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			if (map.get("logged") != null && map.get("logId") != null) {
				if(encrypt.equalsIgnoreCase("YES")){
					logged = data.encryptData(map.get("logged").toString());
					s = data.encryptData(s);
				}
				else if(encrypt.equalsIgnoreCase("NO")){
					logged = map.get("logged").toString();
					s = s;
				}
				
				hash = sha.SHA1(logged + "," + s);
				String query = "INSERT INTO LOGGED_COUNT(LOG_ID,LOGGED,LOG_DATE,LOG_TIME,HASH_ID) VALUES(?, ?, ?,DEFAULT, ?)";
				ps = conn.prepareStatement(query,
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setLong(1, Long.parseLong(map.get("logId").toString()));
				ps.setString(2, logged);
				ps.setString(3, s);
				ps.setString(4, hash);
				ps.executeUpdate();
				System.out.println("^^^^^^^^^^^^^Saved");

				ResultSet rs = ps.getGeneratedKeys();
				while (rs.next()) {
					save = rs.getLong(1);
					System.out.println("^^^^^^^^^^^^^updated" + save);

				}
			}
			ps.close();
			conn.close();
			return save;
			// stmt.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return save;
	}

	public long saveLogoutDetails(HashMap map) {
		String logout = "";
		String rowforHash = "";
		String hash = "";
		Long save = 0L;
		String logoutDate = "";
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String encrypt = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.DB_ENCRYPT);
			if(encrypt.equalsIgnoreCase("YES")){
				if (map.get("logout") != null)
					logout = data.encryptData(map.get("logout").toString());
				logoutDate = data.encryptData(s);
			}
			else if(encrypt.equalsIgnoreCase("NO")){
				if (map.get("logout") != null)
					logout = map.get("logout").toString();
				logoutDate = s;
			}
			
			rowforHash = logout + "," + logoutDate;
			hash = sha.SHA1(rowforHash);
			String query = "INSERT INTO LOGOUT_DETAILS(Logout,Logout_Date,LOGOUT_TIME,LOG_ID,HASH_ID) values("
					+ "?"
					+ ","
					+ "?"
					+ ",DEFAULT,"
					+ "?" + "," + "?" + ")";
			PreparedStatement stmt = db.getDBConnection().prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, logout);
			stmt.setString(2, logoutDate);
			stmt.setLong(3, Long.parseLong(map.get("logId").toString()));
			stmt.setString(4, hash);
			System.err.println("Query is"+query);
			stmt.executeUpdate();
			System.out.println("^^^^^^^^^^^^^Saved");

			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()) {
				save = rs.getLong(1);
				System.out.println("^^^^^^^^^^^^^updated" + save);
				saveLogIntegrity(CommonConstants.LOGOUT_TAB_INTEGRITY_CHECK,
						"LOGOUT_ID", hash, hash, save, "yes");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	

	public long getPastLoggedInCount() {
		//logger.info("[UserManager][getAllUsers]");
		
		PreparedStatement ps=null;
		long count=-1;
		Connection conn =  db.getDBConnection();
		try {
			if(conn != null)
			{
			
			
			String query1 = "select count(*) as total from USER_MGMT";
					
			ps = conn.prepareStatement(query1);
			
			//Statement stmt = db.getDBConnection().createStatement();
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				count=rs.getLong(1);
			}

			if(ps != null)
				ps.close();
			if(conn != null)
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}  catch (Exception e) {
			e.printStackTrace();
		}
		return count;

	}

}
