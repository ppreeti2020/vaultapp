package com.pb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;

public class CommonDBConnection {
	public static final String MSSQL = "MS SQL Server";

	public static final String ORACLE = "ORACLE";

	public static final String MSSQL_NAME = "Ms. SQL Server";

	public static final String ORACLE_NAME = "Oracle 11g/10g";

	public static final String MSSQL_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

	public static final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";

	public static List<String> getDBList() {
		List<String> allDB = new ArrayList<String>();
		allDB.add(CommonDBConnection.MSSQL);
		//allDB.add(CommonDBConnection.ORACLE);

		return allDB;
	}

	public static Connection getConnection(String DB_TYPE, String url,
			String user, String pass) {
		Connection conn = null;
		try {
			Class.forName(CommonDBConnection.getDriver(DB_TYPE));

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			return DriverManager.getConnection(url, user, pass);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}

	public static Connection getDBConnection(String path) {
		Connection conn = null;
		try {
			String filePath = CommonConstants.DB_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(filePath);
			String url = getURL(
					properties.getProperty(CommonConstants.DB_SERVER_TYPE),
					properties.getProperty(CommonConstants.DB_SERVER_HOST),
					properties.getProperty(CommonConstants.DB_SERVER_PORT));
			conn = getConnection(
					properties.getProperty(CommonConstants.DB_SERVER_TYPE),
					url, properties.getProperty(CommonConstants.DB_USER_NAME),
					properties.getProperty(CommonConstants.DB_PASSWORD));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}

	public static String testConnection(String DB_TYPE, String url,
			String user, String pass) {
		String str = null;
		try {
			Class.forName(CommonDBConnection.getDriver(DB_TYPE));

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "notconnected";
		}
		try {
			Connection conn = DriverManager.getConnection(url, user, pass);
			str = "connected";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "notconnected";
		}
		return str;
	}

	public static String getName(String DB_TYPE) {
		if (DB_TYPE.equalsIgnoreCase(MSSQL)) {
			return MSSQL_NAME;
		} else if (DB_TYPE.equalsIgnoreCase(ORACLE)) {
			return ORACLE_NAME;
		} else {
			return "";
		}
	}

	public static String getDriver(String DB_TYPE) {
		if (DB_TYPE.equalsIgnoreCase(MSSQL)) {
			return MSSQL_DRIVER;
		} else if (DB_TYPE.equalsIgnoreCase(ORACLE)) {
			return ORACLE_DRIVER;
		} else {
			return "";
		}
	}

	public static String getURL(String DB_TYPE, String host, String port) {
		if (DB_TYPE.equalsIgnoreCase(MSSQL)) {
			return "jdbc:sqlserver://" + host + ":" + port + ";";
		} else if (DB_TYPE.equalsIgnoreCase(ORACLE)) {
			return "jdbc:oracle:thin:@" + host + ":" + port + ";";
		} else {
			return "";
		}
	}

	public static String getURL(String DB_TYPE, String host, String port,
			String dbName) {
		if (DB_TYPE.equalsIgnoreCase(MSSQL)) {
			return "jdbc:sqlserver://" + host + ":" + port + ";databaseName="
					+ dbName + ";";
		} else if (DB_TYPE.equalsIgnoreCase(ORACLE)) {
			return "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName
					+ ";";
		} else {
			return "";
		}
	}

	public static List getDBList(Connection conn, String DB_TYPE) {
		List list = new ArrayList();
		String query = "";
		try {

			if (DB_TYPE.equalsIgnoreCase(MSSQL)) {
				query = "SELECT NAME FROM sys.sysdatabases";
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					System.out.println("databases:" + rs.getString(1));
					list.add(rs.getString(1));
				}

			} else if (DB_TYPE.equalsIgnoreCase(ORACLE)) {
				query = "select * from v$database;";
			}
		} catch (Exception e) {

		}
		return list;
	}

}
