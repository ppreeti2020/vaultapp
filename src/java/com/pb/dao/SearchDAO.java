package com.pb.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.pb.common.AeSimpleSHA1;
import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;

public class SearchDAO {
	DBConnectionDAO db = new DBConnectionDAO();
	/* Nithu Alexander: 09/09/2014
	 * Added DataEncryptDecrypt and Commented CommonDAO as part of 
	 * SIT and Eclipse Code merge*/
	//CommonDAO data = new CommonDAO();	
	DataEncryptDecrypt data = new DataEncryptDecrypt();
	AeSimpleSHA1 sha = new AeSimpleSHA1();
	long save = 0;
	Statement stmt = null;

	public long saveSearchDetails(HashMap map) {
		String searchValue = "";
		String searchKeyword = "";
		String resCount = "";
		String hash = "";
		try {
			Timestamp tsReturn = null;
			Date date = new Date();
			System.out.println("LogId" + map.get("logId"));
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			tsReturn = new Timestamp(date.getTime());
			// And the default time and date-time DateFormats
			if (map.get("searchVal") != null)
				/* Nithu Alexander : 09/09/2014 Added and Commented as part of SIT and Eclipse code Merge*/
				searchValue = data.encryptData(map.get("searchVal").toString());
				//searchValue = data.CheckEncryptData(map.get("searchVal").toString());
			
			/* Nithu Alexander : 09/09/2014 Added and Commented as part of SIT and Eclipse code Merge*/
			if (map.get("resCount") != null)
				resCount = data.encryptData(map.get("resCount").toString());
				//resCount = data.CheckEncryptData(map.get("resCount").toString());
			
			/* Nithu Alexander : 09/09/2014 Added and Commented as part of SIT and Eclipse code Merge*/
			if (map.get("searchKeyword") != null)
				searchKeyword = data.encryptData(map.get("searchKeyword").toString());
				//searchKeyword = data.CheckEncryptData(map.get("searchKeyword").toString());

			/* Nithu Alexander : 09/09/2014 Added and Commented as part of SIT and Eclipse code Merge*/
			String searchDate = data.encryptData(s);
			//String searchDate = data.CheckEncryptData(s);
			
			
			hash = sha.SHA1(searchValue + "," + resCount + "," + searchDate);

			String query = "INSERT INTO SEARCH_DETAILS(LOG_ID,Search_Value,Search_Date,Search_Time,Res_Count,Search_Keyword,HASH_ID) VALUES('"
					+ map.get("logId")
					+ "','"
					+ searchValue
					+ "','"
					+ searchDate
					+ "', DEFAULT, '"
					+ resCount
					+ "','"
					+ searchKeyword
					+ "','"
					+ hash
					+ "');";
			stmt = db.getDBConnection().createStatement();
			stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
			System.out.println("search values^^^^^^^^^updated");
			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()) {
				save = rs.getLong(1);
				System.out.println("^^^^^^^^^^^^^updated" + save);
				saveSearchIntegrity(CommonConstants.SEARCH_DET_INTEGRITY_CHECK,
						"SERID", hash, hash, save, "yes");
			}
			return save;
			// stmt.
		} catch (Exception e) {
			e.printStackTrace();
		}
		return save;
	}

	public long saveSearchResults(HashMap map) {
		String Search_Results = "";
		String hash = "";
		try {
			if (map.get("results") != null)
				/* Nithu Alexander : 09/09/2014 Added and Commented as part of SIT and Eclipse code Merge*/
				Search_Results = data.encryptData(map.get("results").toString());
				//Search_Results = data.CheckEncryptData(map.get("results").toString());
			hash = sha.SHA1(Search_Results);
			String query = "INSERT INTO SEARCH_RESULTS(SER_ID,Search_Results,HASH_ID) VALUES('"
					+ map.get("SerId")
					+ "','"
					+ Search_Results
					+ "','"
					+ hash
					+ "');";
			stmt = db.getDBConnection().createStatement();

			stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
			System.out.println("search results^^^^^^^^^^updated");
			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()) {
				save = rs.getLong(1);
				System.out.println("value^^^^^^^^updated" + save);
				saveSearchIntegrity(CommonConstants.SEARCH_RES_INTEGRITY_CHECK,
						"SRES_ID", hash, hash, save, "yes");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return save;
	}

	public void saveSearchIntegrity(String tableName, String ID,
			String newHash, String oldHash, Long sresId, String flag) {
		try {
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Statement stmt = db.getDBConnection().createStatement();
			String query1 = "UPDATE " + tableName + " set INTE_NEW_HASH_ID='"
					+ newHash + "',INTE_OLD_HASH_ID='" + oldHash
					+ "',INTE_CHECK_DATE='" + s + "',CHECK_TIME,CHECK_FLAG='"
					+ flag + "' where " + ID + "=" + sresId + ";";
			stmt.execute(query1);
		} catch (Exception e) {

		}

	}
}
