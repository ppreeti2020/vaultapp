package com.pb.dao;

/*
 * created at Oct 1,2011
 * @author suhasini
 * get the data from database for reports to audit the application
 */

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import java.util.List;

import org.apache.log4j.Logger;

import com.pb.dao.CommonDAO;
import com.pb.dto.DataDTO;
import com.pb.dto.IndexDTO;
import com.pb.dto.UserDTO;
import com.pb.manager.AdminReportManager;
import com.pb.manager.DBConnectionManager;

public class AdminReportDAO {//implements AdminReportManager {

	// DBConnectionDAO dao = new DBConnectionDAO();
	static final Logger logger = Logger.getLogger(AdminReportDAO.class);

	private DBConnectionManager dbManager = new DBConnectionDAO();

	/*
	 * public void setDbManager(DBConnectionManager dbManager) { this.dbManager
	 * = dbManager; }
	 * 
	 * public DBConnectionManager getDbManager() { return dbManager; }
	 */
	/*
	 * To get Login Reports for auditing the application(who all are trying to
	 * login to application)
	 */
	public List<DataDTO> getLoginReports() {
		System.out.println("Getting Login Report...");
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();
		try {
			if (conn != null) {
				logger.info("[AdminReportDAO][getLoginReports]");
				String success = comDao.CheckEncryptData("Y");
				String query = "select LOG_ID,Group_Name,Login_Name,Login_Date,IP,Browser from LOGIN_DETAILS where SUCCESS_FLAG = ?";
				ps = conn.prepareStatement(query);
				ps.setString(1, success);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					DataDTO cDao = new DataDTO();
					cDao.setLogId(rs.getLong(1));

					cDao.setLoginGroup(comDao.CheckDecryptData(
							rs.getString(2)));

					cDao.setLoginName(comDao.CheckDecryptData(
							rs.getString(3)));

					cDao.setLoginDate(comDao.CheckDecryptData(
							rs.getString(4)));

					cDao.setIP4(comDao.CheckDecryptData( rs.getString(5)));

					cDao.setBrowser(comDao.CheckDecryptData(
							rs.getString(6)));
					list.add(cDao);
				}
				return list;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get Login Reports for auditing the application To get unsuccessful
	 * login reports
	 */
	public List<DataDTO> getUnsuccessfulLoginReports() {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();
		try {

			logger.info("[AdminReportDAO][getUnsuccessfulLoginReports]");

			String success = comDao.CheckEncryptData("N");
			String query = "select LOG_ID,Group_Name,Login_Name,Login_Date,IP,Browser from LOGIN_DETAILS where SUCCESS_FLAG = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, success);
			// stmt.executeQuery(query);
			//System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("LogId: " + rs.getString(1));

				cDao.setLogId(Long.parseLong(rs.getString(1)));

				cDao.setLoginGroup(comDao.CheckDecryptData(
						rs.getString(2)));

				cDao.setLoginName(comDao.CheckDecryptData( rs.getString(3)));

				cDao.setLoginDate(comDao.CheckDecryptData( rs.getString(4)));

				cDao.setIP4(comDao.CheckDecryptData( rs.getString(5)));

				cDao.setBrowser(comDao.CheckDecryptData( rs.getString(6)));

				list.add(cDao);
			}
		
			// cDao.setReportList(reportList)
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get Login Reports for auditing the application To get search reports
	 * login reports
	 */
	public List<DataDTO> getSearchReports(String path) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();
		try {

			logger.info("[AdminReportDAO][getSearchReports]");

			String query = "select l.LOG_ID,s.SER_ID,l.Group_Name,l.Login_Name,s.Res_Count,s.Search_Value,s.Search_Date from LOGIN_DETAILS as l,SEARCH_DETAILS as s where l.LOG_ID = s.LOG_ID";
			ps = conn.prepareStatement(query);
			//System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("selected records" + rs.getString(1));

				cDao.setLogId(Long.parseLong(rs.getString(1)));

				cDao.setSerId(Long.parseLong(rs.getString(2)));

				cDao.setLoginGroup(comDao.CheckDecryptData(
						rs.getString(3)));

				cDao.setLoginName(comDao.CheckDecryptData( rs.getString(4)));

				cDao.setResCount(comDao.CheckDecryptData( rs.getString(5)));

				cDao.setSearchValue(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setSearchDate(comDao.CheckDecryptData(
						rs.getString(7)));

				System.out.println("getSearchReports" + rs.getString(2));
				if (rs.getString(2) != null) {
					cDao = getSearchResId(cDao,
							Long.parseLong(rs.getString(2)), path);
				}
				System.out.println("getSearchReports....." + cDao.getSresId());
				if (cDao.getSresId() != 0) {
					cDao = getSearchSerResId(cDao,
							Long.parseLong(rs.getString(2)), cDao.getSresId(),
							path);
				}
				list.add(cDao);
			}
			// cDao.setReportList(reportList)
			
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get Login Reports for auditing the application To get getSearch
	 * Results for each search
	 */
	public DataDTO getSearchResId(DataDTO cDao, long SerId, String path) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		long Id = 0;
		String str = "";

		// List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getSearchResId]");

			if (SerId != 0) {
				String query1 = "select SRES_ID,Search_Results from SEARCH_RESULTS where SER_ID = ?";
				ps = conn.prepareStatement(query1);
				ps.setLong(1, SerId);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					Id = rs.getLong(1);
					if (rs.getString(2) != null) {
						str = comDao.CheckDecryptData( rs.getString(2));
					}

				}
			}
			cDao.setSresId(Id);
			cDao.setSearchResults(str);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return cDao;
	}

	/*
	 * To get Login Reports for auditing the application To get getSearch
	 * Results for each search
	 */
	public DataDTO getSearchSerResId(DataDTO cdao, long SerId, long sresId,
			String path) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		// long Id = 0;
		// List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getSearchResId]");

			String query2 = "select DB_NAME,NRIC_NO from ACCOUNT_DETAILS where SER_ID = "
					+ SerId + " and SRES_ID = " + sresId;
			ps = conn.prepareStatement(query2);
			ps.setLong(1, SerId);
			ps.setLong(2, sresId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				cdao.setDbName(comDao.CheckDecryptData( rs.getString(1)));
				System.out.println("NricNo:" + rs.getString(2));
				cdao.setNricNo(comDao.CheckDecryptData( rs.getString(2)));

			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return cdao;
	}

	/*
	 * To get Login Reports for auditing the application To get get Logout
	 * Results for each login
	 */
	public List<DataDTO> getLogoutReports() {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getSearchResId]");
			String query = "select l1.logout_id,l.Group_Name,l.Login_Name,l1.LOGOUT,l1.LOGOUT_DATE,l1.LOGOUT_TIME from LOGIN_DETAILS l,LOGOUT_DETAILS l1 where l.LOG_ID = l1.LOG_ID";
			ps = conn.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("selected records" + rs.getString(1));
				cDao.setLogoutId(Long.parseLong(rs.getString(1)));
				cDao.setLoginGroup(comDao.CheckDecryptData(
						rs.getString(2)));
				cDao.setLoginName(comDao.CheckDecryptData( rs.getString(3)));
				String checkLog = comDao
						.CheckDecryptData( rs.getString(4));
				if (checkLog != null) {
					if (checkLog.equals("logout"))
						cDao.setLogout(checkLog);
					else
						cDao.setLogout("Not Logout");
				} else {
					cDao.setLogout("Not Logout");
				}

				cDao.setLogoutDate(comDao.CheckDecryptData(
						rs.getString(5)));

				list.add(cDao);
			}
			// cDao.setReportList(reportList)
			
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get User access document(statement) details
	 */
	public List<DataDTO> getDocumentDetails() throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		// long Id = 0;
		String str = "";
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getDocumentDetails]");

			// String query2 =
			// "select d.DOC_ID,l.Login_Name,a.DB_NAME,a.NRIC_NO,d.Doc_Gen_Date,d.Content_Type from LOGIN_DETAILS as l,DOCUMENT_DETAILS as d,ACCOUNT_DETAILS as a where a.ACC_ID = d.ACC_ID and l.LOG_ID = a.LOG_ID";
			String query2 = "select d.DOC_ID,l.Login_Name,ad.DB_NAME,ad.NRIC_NO,d.DOC_Name,d.Description,d.Content_Type,d.Doc_Gen_Date,a.DOC_ACTION,a.DOC_ACCESS_DATE from LOGIN_DETAILS as l,ACCOUNT_DETAILS as ad,DOCUMENT_DETAILS as d, DOCUMENT_ACTION a where l.LOG_ID=ad.LOG_ID and ad.ACC_ID = d.ACC_ID and a.DOC_ID = d.DOC_ID";
			ps = conn.prepareStatement(query2);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cdao = new DataDTO();
				if (rs.getLong(1) != 0) {
					cdao.setDocId(rs.getLong(1));
				}
				cdao.setLoginName(comDao.CheckDecryptData( rs.getString(2)));
				cdao.setDbName(comDao.CheckDecryptData( rs.getString(3)));
				cdao.setNricNo(comDao.CheckDecryptData( rs.getString(4)));
				cdao.setFileName(comDao.CheckDecryptData( rs.getString(5)));
				cdao.setDocDesc(comDao.CheckDecryptData( rs.getString(6)));
				cdao.setContentType(comDao.CheckDecryptData(
						rs.getString(7)));
				cdao.setDocgenDate(comDao.CheckDecryptData(
						rs.getString(8)));

				if (comDao.CheckDecryptData( rs.getString(9)).equals(
						"view")) {
					str = "View";
				} else if (comDao.CheckDecryptData( rs.getString(9))
						.equals("download")) {
					str = "Download";
				}
				cdao.setDocAccessDate(comDao.CheckDecryptData(
						rs.getString(10)));
				cdao.setDocAction(str);
				list.add(cdao);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	public String getDocumentActionDetails(Long DOC_ID, String path)
			throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();

		// long Id = 0;
		String str = "";
		// List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getDocumentActionDetails]");
			String query2 = "select DOC_ACT_ID,DOC_ACTION from DOCUMENT_ACTION where DOC_ID = ?";
			ps = conn.prepareStatement(query2);
			ps.setLong(1, DOC_ID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				// DataDTO cdao = new DataDTO();
				/*
				 * if(rs.getString(1) != null) {
				 * cdao.setLoginName(data.decryptData(rs.getString(1))); }
				 */

				if (rs.getString(2) != null) {
					if (comDao.CheckDecryptData( rs.getString(2)).equals(
							"view")) {
						str += "User Viewed Document" + ",";
					} else if (comDao.CheckDecryptData( rs.getString(2))
							.equals("download")) {
						str += "User downloaded Document" + ",";
					}
				}
				// cdao.setDocAction(str);
				// list.add(cdao);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return str;
	}

	public List<DataDTO> getFrequentlySearchedUser()
			throws SQLException {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();

		// long Id = 0;

		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getFrequentlySearchedUser]");
			String query2 = "select nric_no,COUNT(*)'nric_no' from [CSR-APP].dbo.ACCOUNT_DETAILS group by NRIC_NO";
			ps = conn.prepareStatement(query2);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();

				cDao.setNricNo(comDao.CheckDecryptData( rs.getString(1)));
				cDao.setCount(Integer.parseInt(rs.getString(2)));

				list.add(cDao);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get count of login application
	 */
	public List<DataDTO> getCountLogged(String date) {
		PreparedStatement ps = null;
		// CommonDAO comDao = new CommonDAO();

		// long Id = 0L;
		DataDTO cDao = new DataDTO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getCountLogged][date]-->" + date);
			String msgQuery = "select COUNT(*),log_date from logged_count where logged = 'Y' group by log_date";
			// String query = "";
			ps = conn.prepareStatement(msgQuery);
			// cs.setString(1, date);
			// cs = dao.getDBConnection().prepareCall(msgQuery);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				cDao = new DataDTO();
				cDao.setCountLog(Integer.parseInt(rs.getString(1)));
				cDao.setLogDate(rs.getString(2));
				// Id = Long.parseLong(rs.getString(1));

				list.add(cDao);
			}
			
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get count of login application
	 */
	public List<DataDTO> getCountLoggedInfo(String path) {

		// long Id = 0;
		CallableStatement cs = null;
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getCountLoggedInfo]");
			String msgQuery = "select count(*) from Logged_Count  where logged = 'Y' and log_date";
			// List list = getLoginReports();

			cs = conn.prepareCall(msgQuery);
			ResultSet rs = cs.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();

				cDao.setCountLog(Integer.parseInt(rs.getString(1)));

				list.add(cDao);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get integrity check
	 */
	public List<DataDTO> getIntegrityCheckInfo(String path) {

		// long Id = 0;
		CallableStatement cs = null;
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getCountLoggedInfo]");
			String msgQuery = "select count(*) from Logged_Count  where logged = 'Y' and log_date";
			// List list = getLoginReports();

			cs = conn.prepareCall(msgQuery);
			ResultSet rs = cs.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();

				cDao.setCountLog(Integer.parseInt(rs.getString(1)));

				list.add(cDao);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
		
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get logout table integrity check
	 */

	public List<UserDTO> getLoginTableIntCheck(String tableName) {
		PreparedStatement ps = null;
		List<UserDTO> list = new ArrayList<UserDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getLoginTableIntCheck][tableName]-->"
					+ tableName);

			String query = "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from "
					+ tableName;
			ps = conn.prepareStatement(query);
			ResultSet rs1 = ps.executeQuery();
			if (rs1.next()) {

				UserDTO uDto = new UserDTO();
				uDto.setTableName(tableName);
				uDto.setNewHashId(rs1.getString(1));
				uDto.setOldHashId(rs1.getString(2));
				uDto.setHashCheckDate(rs1.getString(3));
				uDto.setHashCheckTime(rs1.getString(4));
				if (rs1.getString(5) != null) {
					if (rs1.getString(5).equals("yes"))
						uDto.setHashCheckFlag("Valid");
					else if (rs1.getString(5).equals("no"))
						uDto.setHashCheckFlag("Not Valid");
				}
				list.add(uDto);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get root log profiles for treeview
	 */
	public Map<Long, DataDTO> getRootLogReports() {
		Map<Long, DataDTO> list = new HashMap<Long, DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();

		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getRootLogReports]");
			String query = "select distinct l.host_name,l.HOST_ID from LOG_HOST l left join E2_LOG_FILE_MONITOR m on l.HOST_ID = m.HOST_ID  ";
			ps = conn.prepareStatement(query);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getRootLogReports:hostName:"
						+ rs.getString(1));

				cDao.setHostName(comDao.CheckDecryptData( rs.getString(1)));
				cDao.setHostId(rs.getLong(2));
				list.put(rs.getLong(2), cDao);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
e.printStackTrace();
		}
		finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get log profiles
	 */
	public Map<Long, DataDTO> getLogProfiles(String path) {
		Map<Long, DataDTO> list = new HashMap<Long, DataDTO>();
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getLogProfiles]");
			String query = "select distinct e.profile, m.host_id,e.LOG_FILE_ID from E2_LOG_FILE_MONITOR m join E2_LOG_MONITOR e on m.E2_LOG_FILE_ID = e.E2_LOG_FILE_ID right join LOG_INDEX r on e.LOG_FILE_ID = r.LOG_FILE_ID where  e.profile != '' and e.profile = (select distinct e.profile from E2_LOG_FILE_MONITOR m join E2_LOG_MONITOR e on m.E2_LOG_FILE_ID = e.E2_LOG_FILE_ID right join LOG_INDEX r on e.LOG_FILE_ID = r.LOG_FILE_ID where  e.profile != '') ";
			ps = conn.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				cDao.setProfile(comDao.CheckDecryptData( rs.getString(1)));
				cDao.setHostId(rs.getLong(2));
				cDao.setLogFileId(rs.getLong(3));
				list.put(rs.getLong(3), cDao);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {

		}
		finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	public Map<Long, DataDTO> getDatabases(String realpath) {

		Map<Long, DataDTO> dbs = new HashMap<Long, DataDTO>();
		PreparedStatement ps = null;
		// CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		logger.info("[UserManager][getDatabases] ");
		try {
			if (conn != null) {
				String query = "select DB_ID,DB_NAME,DB_DESC,HOST_ID from CSR_DATABASE";
				ps = conn.prepareStatement(query);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					DataDTO dto = new DataDTO();
					// System.out.println(rs.getLong(4)+"selecting databases" +
					// rs.getString(2));
					dto.setDbId(rs.getLong(1));
					dto.setDbName(rs.getString(2));
					dto.setProfile(rs.getString(2));
					dto.setDbDesc(rs.getString(3));
					dto.setHostId(rs.getLong(4));
					dbs.put(rs.getLong(1), dto);
				}
				
				// stmt.
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return dbs;
	}
	
	public Map<Integer, DataDTO> getDbProfiles() {

		Map<Integer, DataDTO> dbs = new HashMap<Integer, DataDTO>();
		PreparedStatement ps = null;
		// CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		logger.info("[UserManager][getDatabases] ");
		try {
			if (conn != null) {
				String query = "select distinct csr.DB_ID, csr.DB_NAME, csr.DB_DESC, csr.HOST_ID, pm.PROFILE_NAME"+ 
							" from PROFILE_METADATA pm, PROFILE_DB_DATA pd, CSR_DATABASE csr"+
							" where pm.PROFILE_ID = pd.PROFILE_ID and pd.DB_ID = csr.DB_ID";
				
				ps = conn.prepareStatement(query);
				ResultSet rs = ps.executeQuery();
				int k=1;
				while (rs.next()) {
					DataDTO dto = new DataDTO();
					// System.out.println(rs.getLong(4)+"selecting databases" +
					// rs.getString(2));
					dto.setDbId(rs.getLong(1));
					dto.setDbName(rs.getString(2));
					dto.setProfile(rs.getString(5));
					dto.setDbDesc(rs.getString(3));
					dto.setHostId(rs.getLong(4));
					dbs.put(k, dto);
					k++;
				}
				
				// stmt.
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		System.out.println("getDbProfiles()--profiles:"+dbs);
		return dbs;
	}

	public List<DataDTO> getDatabase(Long hostId) {

		List<DataDTO> dbs = new ArrayList<DataDTO>();
		PreparedStatement ps = null;
		// CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		logger.info("[UserManager][getDatabases] ");
		try {
			if (conn != null) {
				String query = "select DB_ID,DB_NAME,DB_DESC,HOST_ID from CSR_DATABASE where HOST_ID=?";
				ps = conn.prepareStatement(query);
				ps.setLong(1, hostId);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					DataDTO dto = new DataDTO();
					System.out.println("selecting databases" + rs.getString(2));
					dto.setDbId(rs.getLong(1));
					dto.setDbName(rs.getString(2));
					dto.setProfile(rs.getString(2));
					dto.setDbDesc(rs.getString(3));
					dto.setHostId(rs.getLong(4));
					dbs.add(dto);
				}
				
				// stmt.
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return dbs;
	}

	public List<DataDTO> getIndexes( Long dbId) {
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();
		PreparedStatement ps = null;
		try {
			ps = conn
					.prepareStatement("select d.INDEX_ID,d.INDEX_NAME,d.INDEX_DESC,c.DB_ID,c.DB_NAME,c.DB_DESC from DATABASE_INDEX d join CSR_DATABASE c on d.DB_ID = c.DB_ID where c.DB_ID = ?");
			ps.setLong(1, dbId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO dto = new DataDTO();
				dto.setIndexId(rs.getLong(1));
				dto.setIndexName(rs.getString(2));
				dto.setIndexDesc(rs.getString(3));
				dto.setDbId(rs.getLong(4));
				dto.setDbName(rs.getString(5));
				dto.setDbDesc(rs.getString(6));
				list.add(dto);
			}
			System.out.println("List 1:" + list);
		}

		catch (Exception e) {

		}finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;

	}

	/*
	 * To get Log File Profiles
	 */
	public List<DataDTO> getLogFileReports(Long hostId) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getLogFileReports][host]-->" + hostId);
			System.out.println("getLogFileReports::" + hostId);
			String query = "";
			// String success = comDao.CheckDecryptData(path,path,"Y");
			// ,SUCCESS_FLAG,HASH_ID
			if (hostId != 0 && hostId != null) {
				query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where l.HOST_ID = ?";
				ps = conn.prepareStatement(query);
				ps.setLong(1, hostId);
			} else {
				query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID ";
				ps = conn.prepareStatement(query);
			}
			System.out.println("selecting records" + query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLogFileReports--LogfileId:"
						+ rs.getLong(1));

				cDao.setLogFileId(rs.getLong(1));

				cDao.setDrdFile(comDao.CheckDecryptData( rs.getString(2)));

				cDao.setProfile(comDao.CheckDecryptData( rs.getString(3)));

				cDao.setFormat(comDao.CheckDecryptData( rs.getString(4)));

				cDao.setTotalRead(comDao.CheckDecryptData( rs.getString(5)));

				cDao.setTotalWrite(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setFinalRatio(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setMethod(comDao.CheckDecryptData( rs.getString(8)));

				cDao.setJournalFile(comDao.CheckDecryptData(
						rs.getString(9)));

				cDao.setDocuments(comDao.CheckDecryptData(
						rs.getString(10)));

				cDao.setDocumentPages(comDao.CheckDecryptData(
						rs.getString(11)));

				cDao.setIgnoredPages(comDao.CheckDecryptData(
						rs.getString(12)));

				cDao.setErrorQccurs(comDao.CheckDecryptData(
						rs.getString(13)));

				cDao.setLogDate(comDao.CheckDecryptData( rs.getString(14)));

				cDao.setLogTime(comDao.CheckDecryptData( rs.getString(15)));

				cDao.setLogEndDate(comDao.CheckDecryptData(
						rs.getString(16)));

				cDao.setLogEndTime(comDao.CheckDecryptData(
						rs.getString(17)));

				cDao.setDrpFile(comDao.CheckDecryptData( rs.getString(18)));

				cDao.setCompressingType(rs.getString(19));

				list.add(cDao);
			}
			System.out.println("List 2:" + list);
			
			// cDao.setReportList(reportList)
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	
	
	
	
	
	/*
	 * To get Log File Profiles
	 */
	public List<DataDTO> getLogFileReportsWithResources(Long hostId, String path) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getLogFileReports][host]-->" + hostId);
			System.out.println("getLogFileReports::" + hostId);
			String query = "";
			// String success = comDao.CheckDecryptData(path,path,"Y");
			// ,SUCCESS_FLAG,HASH_ID
			if (hostId != 0 && hostId != null) {
				query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE,lr.RESOURCE_NAME,lr.SIZE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID join LOG_RESOURCES lr on lr.LOG_FILE_ID=m.LOG_FILE_ID where l.HOST_ID = ?";
				ps = conn.prepareStatement(query);
				ps.setLong(1, hostId);
			} else {
				query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE,lr.RESOURCE_NAME,lr.SIZE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID join LOG_RESOURCES lr on lr.LOG_FILE_ID=m.LOG_FILE_ID";
				ps = conn.prepareStatement(query);
			}
			System.out.println("selecting records" + query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLogFileReports--LogfileId:"
						+ rs.getLong(1));

				cDao.setLogFileId(rs.getLong(1));

				cDao.setDrdFile(comDao.CheckDecryptData( rs.getString(2)));

				cDao.setProfile(comDao.CheckDecryptData( rs.getString(3)));

				cDao.setFormat(comDao.CheckDecryptData( rs.getString(4)));

				cDao.setTotalRead(comDao.CheckDecryptData( rs.getString(5)));

				cDao.setTotalWrite(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setFinalRatio(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setMethod(comDao.CheckDecryptData( rs.getString(8)));

				cDao.setJournalFile(comDao.CheckDecryptData(
						rs.getString(9)));

				cDao.setDocuments(comDao.CheckDecryptData(
						rs.getString(10)));

				cDao.setDocumentPages(comDao.CheckDecryptData(
						rs.getString(11)));

				cDao.setIgnoredPages(comDao.CheckDecryptData(
						rs.getString(12)));

				cDao.setErrorQccurs(comDao.CheckDecryptData(
						rs.getString(13)));

				cDao.setLogDate(comDao.CheckDecryptData( rs.getString(14)));

				cDao.setLogTime(comDao.CheckDecryptData( rs.getString(15)));

				cDao.setLogEndDate(comDao.CheckDecryptData(
						rs.getString(16)));

				cDao.setLogEndTime(comDao.CheckDecryptData(
						rs.getString(17)));

				cDao.setDrpFile(comDao.CheckDecryptData( rs.getString(18)));

				cDao.setCompressingType(rs.getString(19));
				
				cDao.setResWrote(comDao.CheckDecryptData( rs.getString(20)));
				cDao.setResSize(rs.getString(21));

				list.add(cDao);
			}
			System.out.println("List 2:" + list);
			
			// cDao.setReportList(reportList)
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	
	
	
	
	
	
	
	
	
	
	/*
	 * To get Log File Profiles
	 */
	public List<DataDTO> getLogFileReports(String profile, String date,
			Long hostId) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getLogFileReports][host]-->" + hostId);
			System.out.println("getLogFileReports::" + profile+"--" + date+"--"+hostId);
			System.out.println("getLogFileReports::" + comDao.CheckEncryptData(profile)+"--" + comDao.CheckEncryptData(date)+"--"+hostId);
			String query = "";
			// String success = comDao.CheckEncryptData("Y");
			// ,SUCCESS_FLAG,HASH_ID
			if (hostId != 0 && hostId != null) {
				if ((profile != null && !profile.equals(""))
						&& (date != null && !date.equals(""))) {
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where l.HOST_ID = ? and m.profile = ? and m.LOG_DATE=?";
					ps = conn.prepareStatement(query);
					ps.setLong(1, hostId);
					ps.setString(2, comDao.CheckEncryptData(profile.toUpperCase()));
					ps.setString(3, comDao.CheckEncryptData(date));
				}

				else if (profile != null  && !profile.equals("")) {
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where l.HOST_ID = ? and m.profile = ? ";
					ps = conn.prepareStatement(query);
					ps.setLong(1, hostId);
					ps.setString(2, comDao.CheckEncryptData(profile.toUpperCase()));
				} else if (date != null && !date.equals("")) {
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where l.HOST_ID = ? and m.LOG_DATE=?";
					ps = conn.prepareStatement(query);
					ps.setLong(1, hostId);
					ps.setString(2, comDao.CheckEncryptData(date));
				}

			} else {
				if ((profile != null && !profile.equals(""))
						&& (date != null && !date.equals(""))) {
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where m.profile = ? and m.LOG_DATE=?";
					ps = conn.prepareStatement(query);

					ps.setString(1, comDao.CheckEncryptData("+HC8Ac+bE/H8TvZF4TQX5w=="));
					ps.setString(2, comDao.CheckEncryptData(date));
					
				

				}

				else if (profile != null) {
					//String profileee=comDao.CheckEncryptData(profile);
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where m.profile = ? ";
					//System.out.println("Profileeeeeeeeeeeeeeisxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"+profileee);
					//System.out.println(comDao.CheckDecryptData( "+HC8Ac+bE/H8TvZF4TQX5w=="));
					ps = conn.prepareStatement(query);
					ps.setString(1,comDao.CheckEncryptData(profile));
					
				
					//ps.setString(1, comDao.CheckEncryptData(profile));
				} else if (date != null) {
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where  m.LOG_DATE=?";
					ps = conn.prepareStatement(query);

					ps.setString(1, comDao.CheckEncryptData(date));
				}

			}
			System.out.println("selecting records" + query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLogFileReports--LogfileId:"
						+ rs.getLong(1));

				cDao.setLogFileId(rs.getLong(1));

				cDao.setDrdFile(comDao.CheckDecryptData( rs.getString(2)));

				cDao.setProfile(comDao.CheckDecryptData( rs.getString(3)));

				cDao.setFormat(comDao.CheckDecryptData( rs.getString(4)));

				cDao.setTotalRead(comDao.CheckDecryptData( rs.getString(5)));

				cDao.setTotalWrite(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setFinalRatio(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setMethod(comDao.CheckDecryptData( rs.getString(8)));

				cDao.setJournalFile(comDao.CheckDecryptData(
						rs.getString(9)));

				cDao.setDocuments(comDao.CheckDecryptData(
						rs.getString(10)));

				cDao.setDocumentPages(comDao.CheckDecryptData(
						rs.getString(11)));

				cDao.setIgnoredPages(comDao.CheckDecryptData(
						rs.getString(12)));

				cDao.setErrorQccurs(comDao.CheckDecryptData(
						rs.getString(13)));

				cDao.setLogDate(comDao.CheckDecryptData( rs.getString(14)));

				cDao.setLogTime(comDao.CheckDecryptData( rs.getString(15)));

				cDao.setLogEndDate(comDao.CheckDecryptData(
						rs.getString(16)));

				cDao.setLogEndTime(comDao.CheckDecryptData(
						rs.getString(17)));

				cDao.setDrpFile(comDao.CheckDecryptData( rs.getString(18)));

				cDao.setCompressingType(rs.getString(19));

				list.add(cDao);
			}
			System.out.println("List 3:" + list);
		
			// cDao.setReportList(reportList)
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	public List<IndexDTO> getIndexData(Long profileid) {
		Connection conn = this.dbManager.getDBConnection();
		List<IndexDTO> idlist = new ArrayList<IndexDTO>();
		PreparedStatement ps = null;
		try {

			String query = "select db.DB_NAME,dm.account,dm.address,dm.name,dm.invlink from [csr-app].dbo.CSR_DATABASE db,[csr-app].dbo.DOC_METADATA dm where dm.REPOSITORY_ID=db.DB_ID ";
			ps = conn.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				IndexDTO id = new IndexDTO();
				String profileName = rs.getString(1);
				String account = rs.getString(2);
				String address = rs.getString(3);
				String name = rs.getString(4);
				id.setProfileName(profileName);
				id.setAccount(account);
				id.setAddress(address);
				id.setName(name);
				idlist.add(id);

			}
		}

		catch (Exception e) {

		}finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return idlist;
	}

	public List<String> getLogIndexes(Long logFileid) {
		CommonDAO comDao = new CommonDAO();
		PreparedStatement ps = null;

		List<String> list = new ArrayList<String>();
		Connection conn = this.dbManager.getDBConnection();
		String query = "SELECT distinct INDEX_NAME From Log_INDEX where LOG_FILE_ID=?";
		try {

			ps = conn.prepareStatement(query);
			ps.setLong(1, logFileid);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String indexName = rs.getString(1);
				indexName = comDao.CheckDecryptData( indexName);

				list.add(indexName);
			}

		}

		catch (Exception e) {
			System.out.println(e);
		}finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return list;
	}

	private boolean isNullOrEmpty(String s)
	{
		if(s==null )
			return true;
		if(s.equalsIgnoreCase(""))
			return true;
		
		return false;
		
			
	}
	
	
	
	/*
	 * To get Log File Profiles when user search for profile and date
	 */
	public List<DataDTO> getLogFileReportsWithIndexes(String profile, String date,
			String type, Long hostId) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getLogFileReports][host]-->" + hostId);
			System.out.println("getLogFileReports::" + hostId);
			String query = "";
	//	String encryptedProfile=comDao.CheckEncryptData(profile);
			//date=comDao.CheckEncryptData(date,profile);
			// String success = comDao.CheckEncryptData("Y");
			// ,SUCCESS_FLAG,HASH_ID
			//CommonDAO comDao = new CommonDAO();
		//	select LOG_FILE_ID,DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME,DRP_FILE,COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where m.COMPRESSION_TYPE=? and l.HOST_ID = ?
			if (hostId != 0 && hostId != null) {
				if ( !isNullOrEmpty(profile) &&  !isNullOrEmpty(date)) {
					System.out.println("hostId ::"+hostId);
					System.out.println("type ::"+type);
					System.out.println("profile ::"+comDao.CheckEncryptData(profile));
					System.out.println("date ::"+comDao.CheckEncryptData(date));
					
					query = "select distinct m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE,ln.INDEX_FILE_SIZE,ln.INDEX_DATE,ln.INDEX_NAME from E2_LOG_MONITOR m,E2_LOG_FILE_MONITOR l,LOG_INDEX ln where m.E2_log_file_id=l.e2_log_file_id and ln.log_file_id=m.log_file_id and l.HOST_ID = ? and m.COMPRESSION_TYPE = ? and m.profile = ?  and m.LOG_DATE=?";
					System.out.println("sql1");
					ps = conn.prepareStatement(query);
					ps.setLong(1, hostId);
					ps.setString(2, type);
					ps.setString(3, comDao.CheckEncryptData(profile.toUpperCase()));
					ps.setString(4, comDao.CheckEncryptData(date));
				}

				else if ( !isNullOrEmpty(profile) && isNullOrEmpty(date)) {
					
					System.out.println("sql2");
					query = "select distinct m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE,ln.INDEX_FILE_SIZE,ln.INDEX_DATE,ln.INDEX_NAME from E2_LOG_MONITOR m,E2_LOG_FILE_MONITOR l,LOG_INDEX ln where m.E2_log_file_id=l.e2_log_file_id and ln.log_file_id=m.log_file_id and l.HOST_ID = ? and m.COMPRESSION_TYPE = ? and m.profile = ? ";
					ps = conn.prepareStatement(query);
					ps.setLong(1, hostId);
					ps.setString(2, type);
					ps.setString(3,  comDao.CheckEncryptData(profile));
				} else if ( isNullOrEmpty(profile) && !isNullOrEmpty(date)) {	System.out.println("sql3");
					
					query = "select distinct m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE,ln.INDEX_FILE_SIZE,ln.INDEX_DATE,ln.INDEX_NAME from E2_LOG_MONITOR m,E2_LOG_FILE_MONITOR l,LOG_INDEX ln where m.E2_log_file_id=l.e2_log_file_id and ln.log_file_id=m.log_file_id and l.HOST_ID = ? and m.COMPRESSION_TYPE = ? and  and m.LOG_DATE=?";
					ps = conn.prepareStatement(query);
					ps.setLong(1, hostId);
					ps.setString(2, type);
					ps.setString(3, comDao.CheckEncryptData(date));
				}

			} else {
				if (profile != null && date != null) {
					query = "select distinct m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE,ln.INDEX_FILE_SIZE,ln.INDEX_DATE,ln.INDEX_NAME from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID join Log_Index ln on ln.LOG_FILE_ID=l.E2_LOG_FILE_ID where m.COMPRESSION_TYPE = ? and m.profile = ? and m.LOG_DATE=?";
					ps = conn.prepareStatement(query);
					ps.setString(1, type);
					ps.setString(2, profile);
					ps.setString(3, date);
				}

				else if (profile != null) {
					query = "select distinct m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE,ln.INDEX_FILE_SIZE,ln.INDEX_DATE,ln.INDEX_NAME from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID join Log_Index ln on ln.LOG_FILE_ID=l.E2_LOG_FILE_ID where m.COMPRESSION_TYPE = ? and m.profile = ? ";
					ps = conn.prepareStatement(query);
					ps.setString(1, type);
					ps.setString(2, profile);
				} else if (date != null) {
					query = "select distinct m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE,ln.INDEX_FILE_SIZE,ln.INDEX_DATE,ln.INDEX_NAME from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID join Log_Index ln on ln.LOG_FILE_ID=l.E2_LOG_FILE_ID where  m.COMPRESSION_TYPE = ? and m.LOG_DATE=?";
					ps = conn.prepareStatement(query);
					ps.setString(1, type);
					ps.setString(2, date);
				}

			}
			System.out.println("selecting records" + query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLogFileReports--LogfileId:"
						+ rs.getLong(1));

				cDao.setLogFileId(rs.getLong(1));

				cDao.setDrdFile(comDao.CheckDecryptData( rs.getString(2)));

				cDao.setProfile(comDao.CheckDecryptData( rs.getString(3)));

				cDao.setFormat(comDao.CheckDecryptData( rs.getString(4)));

				cDao.setTotalRead(comDao.CheckDecryptData( rs.getString(5)));

				cDao.setTotalWrite(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setFinalRatio(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setMethod(comDao.CheckDecryptData( rs.getString(8)));

				cDao.setJournalFile(comDao.CheckDecryptData(
						rs.getString(9)));

				cDao.setDocuments(comDao.CheckDecryptData(
						rs.getString(10)));

				cDao.setDocumentPages(comDao.CheckDecryptData(
						rs.getString(11)));

				cDao.setIgnoredPages(comDao.CheckDecryptData(
						rs.getString(12)));

				cDao.setErrorQccurs(comDao.CheckDecryptData(
						rs.getString(13)));

				cDao.setLogDate(comDao.CheckDecryptData( rs.getString(14)));

				cDao.setLogTime(comDao.CheckDecryptData( rs.getString(15)));

				cDao.setLogEndDate(comDao.CheckDecryptData(
						rs.getString(16)));

				cDao.setLogEndTime(comDao.CheckDecryptData(
						rs.getString(17)));

				cDao.setDrpFile(comDao.CheckDecryptData( rs.getString(18)));

				cDao.setCompressingType(rs.getString(19));

				cDao.setIndexFileSize(comDao.CheckDecryptData( rs.getString(20)));
				cDao.setIndexingStartTime(comDao.CheckDecryptData( rs.getString(21)));
				cDao.setIndexName(comDao.CheckDecryptData( rs.getString(22)));
				
			//	cDao.setIndexes(getLogIndexes(rs.getLong(1), path));
				list.add(cDao);
			}
			System.out.println("List 4:" + list);
			
			// cDao.setReportList(reportList)
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	
	
	
	
	/*
	 * To get Log File Profiles when user search for profile and date
	 */
	public List<DataDTO> getLogFileReports(String profile, String date,
			String type, Long hostId) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();

		try {

			logger.info("[AdminReportDAO][getLogFileReports][host]-->" + hostId);
			System.out.println("getLogFileReports::" + hostId);
			System.out.println("getLogFileReports::" + type+">>"+profile+">>"+comDao.CheckEncryptData(date));
			
			System.out.println("getLogFileReports::" + type+">>"+comDao.CheckDecryptData("u7jeCxmlJbE36Sb2Yphx0w=="));
			String query = "";
	//	String encryptedProfile=comDao.CheckEncryptData(profile);
			//date=comDao.CheckEncryptData(date,profile);
			// String success = comDao.CheckEncryptData("Y");
			// ,SUCCESS_FLAG,HASH_ID
			//CommonDAO comDao = new CommonDAO();
		//	select LOG_FILE_ID,DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME,DRP_FILE,COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where m.COMPRESSION_TYPE=? and l.HOST_ID = ?
			if (hostId != 0 && hostId != null) {
				if ( !isNullOrEmpty(profile) &&  !isNullOrEmpty(date)) {
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where l.HOST_ID = ? and m.COMPRESSION_TYPE = ? and m.profile = ? and m.LOG_DATE=?";
					System.out.println("sql1");
					ps = conn.prepareStatement(query);
					ps.setLong(1, hostId);
					ps.setString(2, type);
					ps.setString(3, comDao.CheckEncryptData(profile.toUpperCase()));
					ps.setString(4, comDao.CheckEncryptData(date));
				}

				else if ( !isNullOrEmpty(profile) && isNullOrEmpty(date)) {
					
					System.out.println("sql2");
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where l.HOST_ID = ? and m.COMPRESSION_TYPE = ? and m.profile = ? ";
					ps = conn.prepareStatement(query);
					ps.setLong(1, hostId);
					ps.setString(2, type);
					ps.setString(3,  comDao.CheckEncryptData(profile));
				} else if ( isNullOrEmpty(profile) && !isNullOrEmpty(date)) {	System.out.println("sql3");
					
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where l.HOST_ID = ? and m.COMPRESSION_TYPE = ? and m.LOG_DATE=?";
					ps = conn.prepareStatement(query);
					ps.setLong(1, hostId);
					ps.setString(2, type);
					ps.setString(3, comDao.CheckEncryptData(date));
				}

			} else {
				if (profile != null && date != null) {
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where m.COMPRESSION_TYPE = ? and m.profile = ? and m.LOG_DATE=?";
					ps = conn.prepareStatement(query);
					ps.setString(1, type);
					ps.setString(2, profile);
					ps.setString(3, date);
				}

				else if (profile != null) {
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where m.COMPRESSION_TYPE = ? and m.profile = ? ";
					ps = conn.prepareStatement(query);
					ps.setString(1, type);
					ps.setString(2, profile);
				} else if (date != null) {
					query = "select m.LOG_FILE_ID,m.DRD_FILE,m.PROFILE,m.FORMAT,m.TOTTAL_READ,m.TOTTAL_WRITTEN,m.FINAL_RATIO,m.METHOD,m.JRNL_FILE_NAME,m.DOCUMENTS,m.DOCUMENT_PAGES,m.IGNORED_PAGES,m.ERROR_OCCURS,m.LOG_DATE,m.LOG_TIME,m.LOG_END_DATE,m.LOG_END_TIME,m.DRP_FILE,m.COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where  m.COMPRESSION_TYPE = ? and m.LOG_DATE=?";
					ps = conn.prepareStatement(query);
					ps.setString(1, type);
					ps.setString(2, date);
				}

			}
			System.out.println("selecting records" + query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLogFileReports--LogfileId:"
						+ rs.getLong(1));

				cDao.setLogFileId(rs.getLong(1));

				cDao.setDrdFile(comDao.CheckDecryptData( rs.getString(2)));

				cDao.setProfile(comDao.CheckDecryptData( rs.getString(3)));

				cDao.setFormat(comDao.CheckDecryptData( rs.getString(4)));

				cDao.setTotalRead(comDao.CheckDecryptData( rs.getString(5)));

				cDao.setTotalWrite(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setFinalRatio(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setMethod(comDao.CheckDecryptData( rs.getString(8)));

				cDao.setJournalFile(comDao.CheckDecryptData(
						rs.getString(9)));

				cDao.setDocuments(comDao.CheckDecryptData(
						rs.getString(10)));

				cDao.setDocumentPages(comDao.CheckDecryptData(
						rs.getString(11)));

				cDao.setIgnoredPages(comDao.CheckDecryptData(
						rs.getString(12)));

				cDao.setErrorQccurs(comDao.CheckDecryptData(
						rs.getString(13)));

				cDao.setLogDate(comDao.CheckDecryptData( rs.getString(14)));

				cDao.setLogTime(comDao.CheckDecryptData( rs.getString(15)));

				cDao.setLogEndDate(comDao.CheckDecryptData(
						rs.getString(16)));

				cDao.setLogEndTime(comDao.CheckDecryptData(
						rs.getString(17)));

				cDao.setDrpFile(comDao.CheckDecryptData( rs.getString(18)));

				cDao.setCompressingType(rs.getString(19));

				cDao.setIndexes(getLogIndexes(rs.getLong(1)));
				list.add(cDao);
			}
			System.out.println("List 4:" + list);
		
			// cDao.setReportList(reportList)
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get Log File Profiles
	 */
	public List<DataDTO> getLogFileProfiles(String type, String path) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();
		// String success="";
		try {

			logger.info("[AdminReportDAO][getLogFileProfiles][type]-->" + type);

			// success = comDao.CheckEncryptData("Y");
			String query = "";
			// ,SUCCESS_FLAG,HASH_ID
			if (!type.equals("")) {
				query = "select distinct  r.RESOURCE_ID, e.profile, e.log_file_id , m.E2_LOG_FILE_ID,r.RESOURCE_NAME,r.SIZE,e.DRP_FILE,e.LOG_DATE,e.LOG_TIME from E2_LOG_MONITOR e right join LOG_RESOURCES r on e.LOG_FILE_ID = r.LOG_FILE_ID left join E2_LOG_FILE_MONITOR m on e.E2_LOG_FILE_ID = m.E2_LOG_FILE_ID where e.COMPRESSION_TYPE = ? ";
				ps = conn.prepareStatement(query);
				ps.setString(1, type);
			} else {
				query = "select e.log_file_id ,e.profile,  r.RESOURCE_ID, m.E2_LOG_FILE_ID,r.RESOURCE_NAME,r.SIZE,e.DRP_FILE,e.LOG_DATE,e.LOG_TIME from E2_LOG_MONITOR e right join LOG_RESOURCES r on e.LOG_FILE_ID = r.LOG_FILE_ID left join E2_LOG_FILE_MONITOR m on e.E2_LOG_FILE_ID = m.E2_LOG_FILE_ID";
				ps = conn.prepareStatement(query);
			}
			// ps.setString(1, success);
			// System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out
						.println("getLogFileProfiles--LogId:" + rs.getLong(1));

				cDao.setLogFileId(rs.getLong(1));

				cDao.setProfile(comDao.CheckDecryptData( rs.getString(2)));
				cDao.setResourceId(rs.getLong(3));
				cDao.setResWrote(comDao.CheckDecryptData( rs.getString(5)));
				cDao.setResSize(comDao.CheckDecryptData( rs.getString(6)));
				System.out.println("getLogFileProfiles--DrpFile:"
						+ rs.getString(7));
				cDao.setDrpFile(comDao.CheckDecryptData( rs.getString(7)));
				cDao.setLogDate(comDao.CheckDecryptData( rs.getString(8)));
				cDao.setLogTime(comDao.CheckDecryptData( rs.getString(9)));
				System.out.println("getLogFileProfiles--LogId:"
						+ comDao.CheckDecryptData( rs.getString(5)));
				list.add(cDao);
			}
			System.out.println("List 5:" + list);
			// cDao.setReportList(reportList)
			
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	public List<DataDTO> getLogFileReportsWithIndexes(String name,Long hostId)
	{
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();
		// String success="";
		try {

			logger.info("[AdminReportDAO][getLogFileReports][name]-->" + name);

			// String success = comDao.CheckEncryptData("Y");
			// ,SUCCESS_FLAG,HASH_ID
			String query = "select distinct ln.LOG_FILE_ID,DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME,DRP_FILE,COMPRESSION_TYPE,ln.INDEX_FILE_SIZE,ln.INDEX_DATE,ln.INDEX_NAME from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID join Log_Index ln on ln.LOG_FILE_ID=m.LOG_FILE_ID where m.COMPRESSION_TYPE=? and l.HOST_ID = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, name);
			ps.setLong(2, hostId);
			System.out.println("selecting records index");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLoginReports--LogId:" + rs.getLong(1));

				cDao.setLogFileId(rs.getLong(1));

				cDao.setDrdFile(comDao.CheckDecryptData( rs.getString(2)));

				cDao.setProfile(comDao.CheckDecryptData( rs.getString(3)));

				cDao.setFormat(comDao.CheckDecryptData( rs.getString(4)));

				cDao.setTotalRead(comDao.CheckDecryptData( rs.getString(5)));

				cDao.setTotalWrite(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setFinalRatio(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setMethod(comDao.CheckDecryptData( rs.getString(8)));

				cDao.setJournalFile(comDao.CheckDecryptData(
						rs.getString(9)));

				cDao.setDocuments(comDao.CheckDecryptData(
						rs.getString(10)));

				cDao.setDocumentPages(comDao.CheckDecryptData(
						rs.getString(11)));

				cDao.setIgnoredPages(comDao.CheckDecryptData(
						rs.getString(12)));

				cDao.setErrorQccurs(comDao.CheckDecryptData(
						rs.getString(13)));

				cDao.setLogDate(comDao.CheckDecryptData( rs.getString(14)));

				cDao.setLogTime(comDao.CheckDecryptData( rs.getString(15)));

				cDao.setLogEndDate(comDao.CheckDecryptData(
						rs.getString(16)));

				cDao.setLogEndTime(comDao.CheckDecryptData(
						rs.getString(17)));

				cDao.setDrpFile(comDao.CheckDecryptData( rs.getString(18)));

				cDao.setCompressingType(rs.getString(19));
				
				cDao.setIndexFileSize(comDao.CheckDecryptData( rs.getString(20)));
				cDao.setIndexingStartTime(comDao.CheckDecryptData( rs.getString(21)));
				cDao.setIndexName(comDao.CheckDecryptData( rs.getString(22)));
				//cDao.setIndexes(getLogIndexes(rs.getLong(1), path));
				list.add(cDao);
			}
			System.out.println("List 1:" + list);
			// cDao.setReportList(reportList)
			
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}
	
	
	/*
	 * To get Log File Reports
	 */
	public List<DataDTO> getLogFileReports(String name, Long hostId) {
		PreparedStatement ps = null;
		System.out.println("getLogFileReports:::hostId"+hostId);
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();
		// String success="";
		try {

			logger.info("[AdminReportDAO][getLogFileReports][name]-->" + name);

			// String success = comDao.CheckEncryptData("Y");
			// ,SUCCESS_FLAG,HASH_ID
			String query = "select LOG_FILE_ID,DRD_FILE,PROFILE,FORMAT,TOTTAL_READ,TOTTAL_WRITTEN,FINAL_RATIO,METHOD,JRNL_FILE_NAME,DOCUMENTS,DOCUMENT_PAGES,IGNORED_PAGES,ERROR_OCCURS,LOG_DATE,LOG_TIME,LOG_END_DATE,LOG_END_TIME,DRP_FILE,COMPRESSION_TYPE from E2_LOG_MONITOR m left join E2_LOG_FILE_MONITOR l on m.E2_LOG_FILE_ID = l.E2_LOG_FILE_ID where m.COMPRESSION_TYPE=? and l.HOST_ID = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, name);
			ps.setLong(2, hostId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				cDao.setLogFileId(rs.getLong(1));

				cDao.setDrdFile(comDao.CheckDecryptData( rs.getString(2)));

				cDao.setProfile(comDao.CheckDecryptData( rs.getString(3)));

				cDao.setFormat(comDao.CheckDecryptData( rs.getString(4)));

				cDao.setTotalRead(comDao.CheckDecryptData( rs.getString(5)));

				cDao.setTotalWrite(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setFinalRatio(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setMethod(comDao.CheckDecryptData( rs.getString(8)));

				cDao.setJournalFile(comDao.CheckDecryptData(
						rs.getString(9)));

				cDao.setDocuments(comDao.CheckDecryptData(
						rs.getString(10)));

				cDao.setDocumentPages(comDao.CheckDecryptData(
						rs.getString(11)));

				cDao.setIgnoredPages(comDao.CheckDecryptData(
						rs.getString(12)));

				cDao.setErrorQccurs(comDao.CheckDecryptData(
						rs.getString(13)));

				cDao.setLogDate(comDao.CheckDecryptData( rs.getString(14)));

				cDao.setLogTime(comDao.CheckDecryptData( rs.getString(15)));

				cDao.setLogEndDate(comDao.CheckDecryptData(
						rs.getString(16)));

				cDao.setLogEndTime(comDao.CheckDecryptData(
						rs.getString(17)));

				cDao.setDrpFile(comDao.CheckDecryptData( rs.getString(18)));

				cDao.setCompressingType(rs.getString(19));
				//cDao.setIndexes(getLogIndexes(rs.getLong(1), path));
				list.add(cDao);
			}
			System.out.println("List 1:" + list);
			// cDao.setReportList(reportList)
			
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get Log File Resources
	 */
	public List<DataDTO> getLogFileResources(Long logFileId) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		List<DataDTO> resList = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();
		// String success="";
		try {

			logger.info("[AdminReportDAO][getLogFileReports][logFileId]-->"
					+ logFileId);

			String query = "select RESOURCE_ID,RESOURCE_NAME,SIZE,RESOURCES_DATE,RESOURCES_TIME,RESOURCES_END_DATE,RESOURCES_END_TIME,RES_STATUS from LOG_RESOURCES where LOG_FILE_ID=?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, logFileId);
			System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				cDao.setResourceId(rs.getLong(1));

				cDao.setResWrote(comDao.CheckDecryptData( rs.getString(2)));

				cDao.setResSize(comDao.CheckDecryptData( rs.getString(3)));

				cDao.setResStartDate(comDao.CheckDecryptData(
						rs.getString(4)));

				cDao.setResStartTime(comDao.CheckDecryptData(
						rs.getString(5)));

				cDao.setResFinishDate(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setResFinishTime(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setStatus(comDao.CheckDecryptData( rs.getString(8)));

				resList.add(cDao);
				
			}
			System.out.println("List 6:" + resList);
			// cDao.setReportList(reportList)

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return resList;
	}

	/*
	 * To get count of resources
	 */
	public int getLogCountResources(Long logFileId) {
		PreparedStatement ps = null;
		// CommonDAO comDao = new CommonDAO();

		int size = 0;

		Connection conn = this.dbManager.getDBConnection();
		// String success="";
		try {

			logger.info("[AdminReportDAO][getLogFileReports][logFileId]-->"
					+ logFileId);

			// String success = comDao.CheckEncryptData("Y");
			// ,SUCCESS_FLAG,HASH_ID
			String query = "select count(*) from LOG_RESOURCES where LOG_FILE_ID=?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, logFileId);
			//System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {

				size = rs.getInt(1);

			}
			// cDao.setReportList(reportList)
		
			return size;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return size;
	}

	/*
	 * To get log file indexes
	 */
	public List<DataDTO> getLogFileIndexes(Long logFileId, String path) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		List<DataDTO> list = new ArrayList<DataDTO>();
		// String success="";
		try {

			logger.info("[AdminReportDAO][getLogFileReports][logFileId]-->"
					+ logFileId);

			// String success = comDao.CheckEncryptData("Y");
			// ,SUCCESS_FLAG,HASH_ID
			String query = "select INDEX_ID,INDEX_NAME ,INDEX_DISK_READ,INDEX_DISK_WRITE,INDEX_FILE_SIZE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,INDEX_DATE,INDEX_TIME,INDEX_END_DATE,INDEX_END_TIME from LOG_INDEX where LOG_FILE_ID=?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, logFileId);
			//System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLoginReports--LogId:" + rs.getString(2));

				// cDao.setResourceId(rs.getLong(1));
				cDao.setIndexId(rs.getLong(1));

				cDao.setIndexingFile(comDao.CheckDecryptData(
						rs.getString(2)));

				cDao.setIndexDiskRead(comDao.CheckDecryptData(
						rs.getString(3)));

				cDao.setIndexDiskWrite(comDao.CheckDecryptData(
						rs.getString(4)));

				cDao.setIndexFileSize(comDao.CheckDecryptData(
						rs.getString(5)));

				cDao.setIndexStackDepth(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setIndexCacheReadHit(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setIndexCacheReadMiss(comDao.CheckDecryptData(
						rs.getString(8)));

				cDao.setIndexCacheWriteHit(comDao.CheckDecryptData(
						rs.getString(9)));

				cDao.setIndexCacheWriteMiss(comDao.CheckDecryptData(
						rs.getString(10)));

				cDao.setIndexAddDuplicate(comDao.CheckDecryptData(
						rs.getString(11)));

				cDao.setIndexAddUnique(comDao.CheckDecryptData(
						rs.getString(12)));

				cDao.setLogDate(comDao.CheckDecryptData( rs.getString(13)));

				cDao.setIndexingStartTime(comDao.CheckDecryptData(
						rs.getString(14)));

				cDao.setLogEndDate(comDao.CheckDecryptData(
						rs.getString(15)));

				cDao.setIndexingFinishingTime(comDao.CheckDecryptData(
						rs.getString(16)));
				list.add(cDao);
			}
			System.out.println("List 7:" + list);
			// cDao.setReportList(reportList)
			
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get log file indexes
	 */
	public List<DataDTO> getLogFileIndexes(String path) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		List<DataDTO> list = new ArrayList<DataDTO>();
		// String success="";
		try {

			logger.info("[AdminReportDAO][getLogFileReports][logFileId]-->");

			// String success = comDao.CheckEncryptData("Y");
			// ,SUCCESS_FLAG,HASH_ID
			String query = "select INDEX_ID,INDEX_NAME ,INDEX_DISK_READ,INDEX_DISK_WRITE,INDEX_FILE_SIZE,STATCK_DEPTH,CACHE_READ_HIT,CACHE_READ_MISS,CAHE_WRITE_HIT,CAHE_WRITE_MISS,ADD_DUPLICATE,ADD_UNIQUE,INDEX_DATE,INDEX_TIME,INDEX_END_DATE,INDEX_END_TIME from LOG_INDEX";
			ps = conn.prepareStatement(query);

			//System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLoginReports--LogId:" + rs.getString(2));

				// cDao.setResourceId(rs.getLong(1));
				cDao.setIndexId(rs.getLong(1));

				cDao.setIndexingFile(comDao.CheckDecryptData(
						rs.getString(2)));

				cDao.setIndexDiskRead(comDao.CheckDecryptData(
						rs.getString(3)));

				cDao.setIndexDiskWrite(comDao.CheckDecryptData(
						rs.getString(4)));

				cDao.setIndexFileSize(comDao.CheckDecryptData(
						rs.getString(5)));

				cDao.setIndexStackDepth(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setIndexCacheReadHit(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setIndexCacheReadMiss(comDao.CheckDecryptData(
						rs.getString(8)));

				cDao.setIndexCacheWriteHit(comDao.CheckDecryptData(
						rs.getString(9)));

				cDao.setIndexCacheWriteMiss(comDao.CheckDecryptData(
						rs.getString(10)));

				cDao.setIndexAddDuplicate(comDao.CheckDecryptData(
						rs.getString(11)));

				cDao.setIndexAddUnique(comDao.CheckDecryptData(
						rs.getString(12)));

				cDao.setLogDate(comDao.CheckDecryptData( rs.getString(13)));

				cDao.setIndexingStartTime(comDao.CheckDecryptData(
						rs.getString(14)));

				cDao.setLogEndDate(comDao.CheckDecryptData(
						rs.getString(15)));

				cDao.setIndexingFinishingTime(comDao.CheckDecryptData(
						rs.getString(16)));
				list.add(cDao);
			}
			System.out.println("List 8:" + list);
			// cDao.setReportList(reportList)
			
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	/*
	 * To get Log file Errors
	 */
	public List<DataDTO> getLogFileErrors(Long logFileId) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		List<DataDTO> list = new ArrayList<DataDTO>();
		// String success="";
		try {

			logger.info("[AdminReportDAO][getLogFileReports][logFileId]-->"
					+ logFileId);

			String query = "select ERROR_ID,ERROR_CODE,ERROR_DESC from E2_LOG_ERROR where LOG_FILE_ID=?";
			ps = conn.prepareStatement(query);
			ps.setLong(1, logFileId);
			//System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLoginReports--LogId:" + rs.getLong(1));

				cDao.setErrorId(rs.getLong(1));

				cDao.setErrCode(comDao.CheckDecryptData( rs.getString(2)));

				cDao.setErrDesc(comDao.CheckDecryptData( rs.getString(3)));

				list.add(cDao);
			}
			System.out.println("List 9:" + list);
			if (ps != null)
		
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return list;
	}

	
	private String getProfileName(Long dbId,Long hostId,String realPath)
	{
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		String profileName="";
		try {
			System.out.println("getLogFileResources::");

			String query = "select DB_NAME from CSR_DATABASE WHERE HOST_ID=? AND DB_ID=? ";
			ps = conn.prepareStatement(query);
			ps.setLong(1,hostId);
			ps.setLong(2, dbId);
			// ps.setLong(1, logFileId);
			//System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				
			
				profileName=		rs.getString(1);
				
				
			//	System.out.println("Here11");
				//resList.add(cDao);
				//System.out.println("getLogFileResources--List:" + resList);
			}
		//	System.out.println("List 10:" + resList);
			// cDao.setReportList(reportList)

			

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return profileName;
	}
	
	
	public List<DataDTO> getlogFileResourcesByDBIdAndHostId(Long hostId,Long dbId,String realPath)
	{
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		List<DataDTO> resList = new ArrayList<DataDTO>();
		String profileName=getProfileName(dbId,hostId,realPath);
		try {
			System.out.println("getLogFileResources::");

			//String query = "select RESOURCE_ID,RESOURCE_NAME,SIZE,RESOURCES_DATE,RESOURCES_TIME,RESOURCES_END_DATE,RESOURCES_END_TIME,RES_STATUS,el.LOG_FILE_ID,l.RES_STATUS,el.PROFILE,el.COMPRESSION_TYPE from LOG_RESOURCES l join E2_LOG_MONITOR el on l.LOG_FILE_ID=el.LOG_FILE_ID join E2_LOG_FILE_MONITOR efm on efm.E2_LOG_FILE_ID=el.E2_LOG_FILE_ID join CSR_DATABASE cd on cs.HOST_ID=efm.HOST_ID WHERE efm.HOST_ID=? and cs.DB_ID=?  ";
			String query="select distinct RESOURCE_ID,RESOURCE_NAME,SIZE,RESOURCES_DATE,RESOURCES_TIME,RESOURCES_END_DATE,RESOURCES_END_TIME,RES_STATUS,lm.LOG_FILE_ID,lm.PROFILE,lm.COMPRESSION_TYPE from LOG_RESOURCES lr ,E2_LOG_MONITOR lm,E2_LOG_FILE_MONITOR lfm WHERE lr.LOG_FILE_ID=lm.LOG_FILE_ID and lfm.E2_LOG_FILE_ID=lm.E2_LOG_FILE_ID AND  lfm.HOST_ID=? AND lm.PROFILE=?";
			
			ps = conn.prepareStatement(query);
			 ps.setLong(1, hostId);
			 ps.setString(2,comDao.CheckEncryptData(profileName));
			System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLogFileResources--resourceId:"
						+ rs.getLong(1));
//System.out.println("Here1");
				cDao.setResourceId(rs.getLong(1));
			//	System.out.println("Here2");
				cDao.setResWrote(comDao.CheckDecryptData(
						rs.getString(2)));
			//	System.out.println("Here3");
				cDao.setResSize(comDao.CheckDecryptData(
						rs.getString(3)));
			//	System.out.println("Here4");
				cDao.setResStartDate(comDao.CheckDecryptData(
						rs.getString(4)));
			//	System.out.println("Here5");
				cDao.setResStartTime(comDao.CheckDecryptData(
						rs.getString(5)));
				//System.out.println("Here6");
				cDao.setResFinishDate(comDao.CheckDecryptData(
						rs.getString(6)));
				//System.out.println("Here7");
				cDao.setResFinishTime(comDao.CheckDecryptData(
						rs.getString(7)));
				//System.out.println("Here8");
				cDao.setStatus(comDao.CheckDecryptData(
						rs.getString(8)));
				//System.out.println("Here9");
				cDao.setLogFileId(rs.getLong(9));
				//System.out.println("Here10");
				//System.out.println("getLogFileResources--resourceSize:"						+ cDao.getResSize());
				
				
				cDao.setProfileName(comDao.CheckDecryptData(
						rs.getString(10)));
				cDao.setCompressingType(
						rs.getString(11));
				
				
			//	System.out.println("Here11");
				resList.add(cDao);
				//System.out.println("getLogFileResources--List:" + resList);
			}
		//	System.out.println("List 10:" + resList);
			// cDao.setReportList(reportList)

			

		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return resList;
	}
	
	private String getDBName(Long dbId,Long hostId,String path)
	{
		
		Connection conn = this.dbManager.getDBConnection();
		//CommonDAO comDao = new CommonDAO();
		String dbName="";
		try {
			
			String query = "select DB_NAME from CSR_DATABASE WHERE DB_ID="+dbId+" and host_id="+hostId;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			System.out.println("successfully");
			while (rs.next()) {
				dbName=rs.getString(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return dbName;
	}
	
	private boolean getResourceFileDetailsFromLogFiles(String path,Long dbId,Long hostId,String fileName)
	{
		List<DataDTO> list = new ArrayList<DataDTO>();
		Connection conn = this.dbManager.getDBConnection();
		CommonDAO comDao = new CommonDAO();
		
		String profileName=this.getDBName(dbId, hostId, path);
	    System.out.println("getResourceFileDetailsFromLogFiles -- Profile Name is=="+profileName);
		try {
			String encryptedProfile=comDao.CheckEncryptData(profileName);
			//String query = "select DRP_FILE from E2_LOG_MONITOR lm WHERE lm.PROFILE='"+encryptedProfile+"'";
			
			String query="select lr.RESOURCE_NAME from LOG_RESOURCES lr,E2_LOG_MONITOR elm where elm.LOG_FILE_ID=lr.LOG_FILE_ID and elm.PROFILE='"+encryptedProfile+"'";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			//System.out.println("successfully");
			while (rs.next()) {
				String drdFile=rs.getString(1);
				if(drdFile!=null)
					drdFile=comDao.CheckDecryptData( drdFile);
				
				System.out.println("Drd File"+drdFile);
				int i=drdFile.lastIndexOf("\\");
				
				String sub=drdFile.substring(i+1,drdFile.length());
				System.out.println("Sub  is"+sub);
				System.out.println("file Name is"+fileName);
				if(sub.equalsIgnoreCase(fileName))
					return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try
			{
			
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return false;
	}
	
	
	public List<DataDTO> getResourcesIntegrityDetailsByProfile(String profile,Long hostId) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		List<DataDTO> resList = new ArrayList<DataDTO>();
		try {
			System.out.println("getLogFileResources::");
			String profileName=profile.toUpperCase();
			String encryptedProfile=comDao.CheckEncryptData(profileName);
		//	String query = "select INTE_ID,FILE_NAME,FILE_SIZE,ACTION,FILE_EXISTS,CHECK_VALID from RESOURCE_DATA_INTEGRITY";
		String query=	"select distinct rdi.INTE_ID,rdi.FILE_NAME,rdi.FILE_SIZE,rdi.ACTION,rdi.FILE_EXISTS,rdi.CHECK_VALID from log_resources lr,e2_log_monitor lm,resource_data_integrity rdi	where lr.LOG_FILE_ID=lm.log_file_id and rdi.FILE_NAME=lr.RESOURCE_NAME and lm.profile='"+encryptedProfile+"'";
			ps = conn.prepareStatement(query);
			// ps.setLong(1, logFileId);
			//System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLogFileResources--resourceId:"
						+ rs.getLong(1));
//System.out.println("Here1");
				cDao.setResourceId(rs.getLong(1));
			//	System.out.println("Here2");
				cDao.setResWrote(comDao.CheckDecryptData(rs.getString(2)));
				//System.out.println("Here3");
				cDao.setResSize(
						rs.getString(3));
			//	System.out.println("Here4");
				
			
				//System.out.println("Here9");
				cDao.setAction(comDao.CheckDecryptData(
						rs.getString(4)));
				
					cDao.setStatus(comDao.CheckDecryptData(
						rs.getString(5)));
					cDao.setCheckValid(comDao.CheckDecryptData(
						rs.getString(6)));
				//System.out.println("Here13");
				
				
			//	System.out.println("Here11");
				//	if(getResourceFileDetailsFromLogFiles(realPath,dbId,hostId,cDao.getResWrote()))
				resList.add(cDao);
				//System.out.println("getLogFileResources--List:" + resList);
			}
		//	System.out.println("List 10:" + resList);
			// cDao.setReportList(reportList)

			

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return resList;
	}
	
	
	
	
	
	
	public List<DataDTO> getResourcesIntegrityDetails() {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		List<DataDTO> resList = new ArrayList<DataDTO>();
		try {
			System.out.println("getLogFileResources::");

			String query = "select INTE_ID,FILE_NAME,FILE_SIZE,ACTION,FILE_EXISTS,CHECK_VALID from RESOURCE_DATA_INTEGRITY";
			ps = conn.prepareStatement(query);
			// ps.setLong(1, logFileId);
			System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLogFileResources--resourceId:"
						+ rs.getLong(1));
//System.out.println("Here1");
				cDao.setResourceId(rs.getLong(1));
			//	System.out.println("Here2");
				cDao.setResWrote(comDao.CheckDecryptData(rs.getString(2)));
				//System.out.println("Here3");
				cDao.setResSize(
						rs.getString(3));
			//	System.out.println("Here4");
				
			
				//System.out.println("Here9");
				cDao.setAction(comDao.CheckDecryptData(
						rs.getString(4)));
				
					cDao.setStatus(comDao.CheckDecryptData(
						rs.getString(5)));
					cDao.setCheckValid(comDao.CheckDecryptData(
						rs.getString(6)));
				//System.out.println("Here13");
				
				
			//	System.out.println("Here11");
				resList.add(cDao);
				//System.out.println("getLogFileResources--List:" + resList);
			}
		//	System.out.println("List 10:" + resList);
			// cDao.setReportList(reportList)

			

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return resList;
	}
	
	
	
	public List<DataDTO> getLogFileResources() {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		List<DataDTO> resList = new ArrayList<DataDTO>();
		try {
			System.out.println("getLogFileResources::");

			String query = "select RESOURCE_ID,RESOURCE_NAME,SIZE,RESOURCES_DATE,RESOURCES_TIME,RESOURCES_END_DATE,RESOURCES_END_TIME,RES_STATUS,el.LOG_FILE_ID,l.RES_STATUS,el.PROFILE,el.COMPRESSION_TYPE from LOG_RESOURCES l join E2_LOG_MONITOR el on l.LOG_FILE_ID=el.LOG_FILE_ID";
			ps = conn.prepareStatement(query);
			//System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				cDao.setResourceId(rs.getLong(1));
				cDao.setResWrote(comDao.CheckDecryptData(
						rs.getString(2)));
				cDao.setResSize(comDao.CheckDecryptData(
						rs.getString(3)));
				cDao.setResStartDate(comDao.CheckDecryptData(
						rs.getString(4)));
				cDao.setResStartTime(comDao.CheckDecryptData(
						rs.getString(5)));
				cDao.setResFinishDate(comDao.CheckDecryptData(
						rs.getString(6)));
			//	System.out.println("Here7");
				cDao.setResFinishTime(comDao.CheckDecryptData(
						rs.getString(7)));
				//System.out.println("Here8");
				cDao.setStatus(comDao.CheckDecryptData(
						rs.getString(8)));
				//System.out.println("Here9");
				cDao.setLogFileId(rs.getLong(9));
				//System.out.println("Here10");
				//System.out.println("getLogFileResources--resourceSize:"						+ cDao.getResSize());
				cDao.setStatus(comDao.CheckDecryptData(
						rs.getString(10)));
				//System.out.println("Here11");
				cDao.setProfileName(comDao.CheckDecryptData(
						rs.getString(11)));
				//System.out.println("Here12");
				cDao.setCompressingType(
						rs.getString(12));
				//System.out.println("Here13");
				
				
			//	System.out.println("Here11");
				resList.add(cDao);
				//System.out.println("getLogFileResources--List:" + resList);
			}
		//	System.out.println("List 10:" + resList);
			// cDao.setReportList(reportList)

			

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return resList;
	}

	public List<DataDTO> getLogFileResources(String type) {
		PreparedStatement ps = null;
		CommonDAO comDao = new CommonDAO();
		Connection conn = this.dbManager.getDBConnection();
		List<DataDTO> resList = new ArrayList<DataDTO>();
		try {
			System.out.println("getLogFileResources::");

			String success = comDao.CheckEncryptData("Y");
			// ,SUCCESS_FLAG,HASH_ID
			String query = "select RESOURCE_ID,RESOURCE_NAME,SIZE,RESOURCES_DATE,RESOURCES_TIME,RESOURCES_END_DATE,RESOURCES_END_TIME,RES_STATUS,el.LOG_FILE_ID from LOG_RESOURCES l join E2_LOG_MONITOR el on l.LOG_FILE_ID=el.LOG_FILE_ID where el.COMPRESSION_TYPE=?";
			ps = conn.prepareStatement(query);
			ps.setString(1, type);
			//System.out.println("selecting records");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DataDTO cDao = new DataDTO();
				System.out.println("getLogFileResources--resourceId:"
						+ rs.getLong(1));

				cDao.setResourceId(rs.getLong(1));

				cDao.setResWrote(comDao.CheckDecryptData(
						rs.getString(2)));

				cDao.setResSize(comDao.CheckDecryptData(
						rs.getString(3)));

				cDao.setResStartDate(comDao.CheckDecryptData(
						rs.getString(4)));

				cDao.setResStartTime(comDao.CheckDecryptData(
						rs.getString(5)));

				cDao.setResFinishDate(comDao.CheckDecryptData(
						rs.getString(6)));

				cDao.setResFinishTime(comDao.CheckDecryptData(
						rs.getString(7)));

				cDao.setStatus(comDao.CheckDecryptData(
						rs.getString(8)));

				cDao.setLogFileId(rs.getLong(9));
				System.out.println("getLogFileResources--resourceId:"
						+ cDao.getResSize());

				resList.add(cDao);
				System.out
						.println("getLogFileResources--resourceId:" + resList);
			}
			System.out.println("List 11:" + resList);
			// cDao.setReportList(reportList)
			

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try
			{
			if (ps != null)
				ps.close();
			if (conn != null)
				conn.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return resList;
	}
	

}
