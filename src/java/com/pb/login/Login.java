package com.pb.login;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.Timer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.dispatcher.SessionMap;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.HTMLEntityCodec;

import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.pb.admin.LoadDataTask;
import com.pb.common.CommonConstants;
import com.pb.common.CompareDate;
import com.pb.common.GetDay;
import com.pb.common.PerformanceLogger;
import com.pb.common.PropertyUtils;
import com.pb.dao.LoginDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.UserDTO;
import com.pb.evault.VaultUtils;
import com.pb.ldap.LDAPUtils;

import java.util.Date;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;


/**
 * 
 * @author maruthi
 */

public class Login extends ActionSupport implements SessionAware,
ServletRequestAware, ServletResponseAware {

	static final Logger logger = Logger.getLogger(Login.class);
	private static final Object Date = null;
	LoginDAO loginDao = new LoginDAO();
	private String message;
	private String vaultDatabase;
	private String userName;
	private String userPwd;
	private List<String> prePopulateList;
	private String searchIndexValues;
	private List<String> csrGroups;
	private List<String> csrUsers;
	private List<String> csrRoles;
	PerformanceLogger pl = new PerformanceLogger();
	private String tabName;
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private Map<String, Object> session;
	private Map<String, Object> parameter;
	//<result name="REPOSITORY" type="redirect">/RepAccount.action</result>
	public String getTabName() {
		return tabName;
	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
	}

	Set<SearchIndex> searchIndexes;
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 * the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getVaultDatabase() {
		return vaultDatabase;
	}

	public void setVaultDatabase(String vaultDatabase) {
		this.vaultDatabase = vaultDatabase;
	}

	public List<String> getCsrGroups() {
		return csrGroups;
	}

	public void setCsrGroups(List<String> csrGroups) {
		this.csrGroups = csrGroups;
	}

	public List<String> getCsrUsers() {
		return csrUsers;
	}

	public void setCsrUsers(List<String> csrUsers) {
		this.csrUsers = csrUsers;
	}

	public List<String> getCsrRoles() {
		return csrRoles;
	}

	public void setCsrRoles(List<String> csrRoles) {
		this.csrRoles = csrRoles;
	}

	public String getSearchIndexValues() {
		return searchIndexValues;
	}

	public void setSearchIndexValues(String searchIndexValues) {
		this.searchIndexValues = searchIndexValues;
	}

	public List<String> getPrePopulateList() {
		return prePopulateList;
	}

	public void setPrePopulateList(List<String> prePopulateList) {
		this.prePopulateList = prePopulateList;
	}

	public Set<SearchIndex> getSearchIndexes() {
		return searchIndexes;
	}

	public void setSearchIndexes(Set<SearchIndex> searchIndexes) {
		this.searchIndexes = searchIndexes;
	}

	public Map<String, Object> getParameter() {
		return parameter;
	}

	public void setParameter(Map<String, Object> parameter) {
		this.parameter = parameter;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public HttpServletResponse getServletResponse() {
		return this.servletResponse;
	}

	public void setServletResponse(HttpServletResponse paramHttpServletResponse) {
		this.servletResponse = paramHttpServletResponse;
	}

	public HttpServletRequest getServletRequest() {
		return this.servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public Map<String, Object> getSession() {
		return this.session;
	}

	public void loadStrutsSession() {
		this.session = ActionContext.getContext().getSession();
	}

	public void loadStrutsParameter() {
		this.parameter = ActionContext.getContext().getParameters();
	}

	public void loadDefault() {
		loadStrutsParameter();
		loadStrutsSession();
	}

	public Login() {
	}

	public void validate() {
		logger.info("[Login][validate]");

		if (getUserName() == null || getUserName().length() == 0) {
			logger.info("[Login][validate] user name is empty");
			addFieldError("userName", "User Name is required");
		}

		else if (getUserPwd() == null || getUserPwd().length() == 0) {
			logger.info("[Login][validate] password is empty");
			addFieldError("userPwd", "Password is required");
		}

	}

	public String ReturnBrowser(HttpServletRequest request) {
		try {
			String s = this.getServletRequest().getHeader("user-agent");
			System.out.println("using Browser" + s);

			if (s.indexOf("MSIE") > -1) {
				System.out.println("using Internet Explorer");
				return "MSIE";

			} else if (s.indexOf("Netscape") > -1) {
				System.out.println("using Netscape");
				return "Netscape";
			} else if (s.indexOf("Chrome") > -1) {
				System.out.println("using Chrome");
				return "Chrome";
			} else if (s.indexOf("Opera") > -1) {
				System.out.println("using Opera");
				return "Opera";
			} else if (s.indexOf("Safari") > -1) {
				System.out.println("using Safari");
				return "Safari";
			} 
			else if (s.indexOf("Mozilla") > -1) {
				System.out.println("using Mozilla");
				return "Mozilla";
			}
			else if (s.indexOf("Firefox") > -1) {
				System.out.println("using Firefox");
				return "Firefox";
			}else {
				System.out.println("Other");
				return "";
			}
		} catch (Exception e) {
			System.out.println("Error is Here: ReturnBrowser" + e);
		}
		return "";
	}


	public String execute() {
		
		long startTime;
		long endTime;
		long difference;
		long startTimeInitial;
		startTimeInitial =  System.currentTimeMillis();
		
		/*HttpSession sessionku = getServletRequest().getSession(false);
		System.out.println("&&&&&&&&&&&&&& session id : " + sessionku.getId());*/
		
//		Timer timer=(Timer)ServletActionContext.getContext().getApplication().get("timer");
		
		
	/*	if(timer==null)
		{
		String filePath = CommonConstants.CSR_PROPERTY_FILE_NAME;
		System.out.println("filepath"+filePath);
		Properties properties1 = PropertyUtils.getProperties(filePath);
		System.out.println("properties1");
		String schtime=properties1.getProperty("SCHEDULER_TIME");
		System.out.println("schtime"+schtime);
		if(schtime!=null)
		{int i=Integer.parseInt(schtime);
		System.out.println("i is"+i);
			timer=new Timer();System.out.println("timer");
			timer.schedule(new LoadDataTask(),i*60*1000,i*60*1000);
			System.out.println("after schedule");
					ServletActionContext.getContext().getApplication().put("timer",timer);
					System.out.println("end Context Initialized vault");
		}
		
		}
		*/
		
		
		BasicConfigurator.configure();
		logger.info("[Login][execute]");
		HTMLEntityCodec codec = new HTMLEntityCodec();
		HashMap map = new HashMap();
		UserDAO uDao = new UserDAO();
		String userGroup = "";
		String authenticate = "";
		boolean b = false;
		int ip1 = this.getServletRequest().getRemotePort();
		map.put("IP", this.getServletRequest().getRemoteAddr());
		String browser = ReturnBrowser(this.getServletRequest());
		map.put("browser", browser);
		String ip = this.getServletRequest().getRemoteAddr();
		long logID = 0L;
		
		HttpSession hs = servletRequest.getSession();
		
		/*hs.setAttribute("useraccesscontrol", "false");
		hs.setAttribute("sysaccesscontrol", "false");
		hs.setAttribute("search", "false");
		hs.setAttribute("audit", "false");
		hs.setAttribute("userindexcontrol", "false");
		hs.setAttribute("pradmincreation", "false");
		hs.setAttribute("profileuserassign", "false");
		hs.setAttribute("repository", "false");
		hs.setAttribute("reports", "false");*/
		String propertyFile = CommonConstants.CSR_PROPERTY_FILE_NAME;
		try {
			String iChars = CommonConstants.I_CHARS;// "!@#$%^*()+=[]\\\';,/{}|\":<>?-";
			
			//Map session = ActionContext.getContext().getSession();
			
			WindowsLoginModule wlm;
			if (getUserName() != null || getUserName().length() != 0) {
				for (int i = 0; i < getUserName().length(); i++) {
					if ((iChars.indexOf(getUserName().charAt(i))) != -1) {
						b = true;
						// getUserName().replaceAll(regex, replacement)
					}
				}
				if (b) {
					System.out.println("Error is here in charCheck of UserName::"
							+ getUserName());
					map.put("name", codec.encode(getUserName()));
					map.put("success", "N");
					logID = loginDao.saveLoginDetails( map);
					setUserName("");
					setMessage("Invalid Username");
					endTime =  System.currentTimeMillis();
					difference = endTime-startTimeInitial;
					System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
					
					return "AUTH_FAILED";
				}


				//String ldapuserid = LDAPUtils.getUserIDByDistinguishedName(getUserName(),"");
				//System.out.println("ldapuserId::"+ldapuserid);	
				
				userGroup = LDAPUtils.LDAPUserGroup(getUserName(),getUserPwd());
				System.out.println("userGroup>>>"+userGroup);

				pl.writeTime("***********************Start of User Transaction ***********************");
				pl.writeTime("The UserName is" + getUserName());
				pl.writeTime("The usergroup is : " + userGroup);
				
				/* Added as part of AVA-12.1, 12.2, 12.3, 12.4 and SCT M-02 fixes for VaultApp application
				 * The below change is to reset the JSESSION ID after Login*/
				
				((SessionMap)this.session).invalidate();
				this.session = ActionContext.getContext().getSession();
				hs = servletRequest.getSession(true);
				
				hs.setAttribute("useraccesscontrol", "false");
				hs.setAttribute("sysaccesscontrol", "false");
				hs.setAttribute("search", "false");
				hs.setAttribute("audit", "false");
				hs.setAttribute("userindexcontrol", "false");
				hs.setAttribute("pradmincreation", "false");
				hs.setAttribute("profileuserassign", "false");
				hs.setAttribute("repository", "false");
				hs.setAttribute("reports", "false");

				hs.setAttribute("UserLogin", getUserName());
				hs.setAttribute("UserName", getUserName());
				map.put("name", getUserName());
				session.put("LOGGED-IN", "true");
				session.put("userName", getUserName());

				Properties properties = PropertyUtils.getProperties(propertyFile);
				String roleDesc = "";
				String roleDesc1 = "";
				String roleDesc2 = "";
				String roleDesc3 = "";
				String roleDesc4 = "";
				String roleDesc5 = "";
				String roleDesc6 = "";

				if (properties.getProperty(CommonConstants.CSR_ADMIN_USER_ID) != null && !properties.getProperty(CommonConstants.CSR_ADMIN_USER_ID).equals(" ")
						&& loginDao.getPastLoggedInCount()==0)	 {
					//System.out.println("***********************Start of first login block ***********************");
					String admin = properties
					.getProperty(CommonConstants.CSR_ADMIN_USER_ID);
					String pwd = properties
					.getProperty(CommonConstants.CSR_ADMIN_PWD);
					String group = properties
					.getProperty(CommonConstants.CSR_ADMIN_GROUP_NAME);
					System.out.println("PropertyFile admin username::"+admin+"---Login username::" + getUserName());
					//System.out.println("PropertyFile admin password::"+pwd+"---Login password::" + getUserPwd());

					if (getUserName().equalsIgnoreCase(admin)

							&& getUserPwd().equalsIgnoreCase(pwd)) {
						logger.info("In Administrator block:");
						//hs.setAttribute("useraccesscontrol", "true");
						hs.setAttribute("sysaccesscontrol", "true");
						map.put("success", "Y");
						logID = loginDao.saveLoginDetails(map);
						map.put("logged", "Y");
						map.put("logId", logID);
						session.put("logId", logID);
						hs.setAttribute("logId", logID);
						loginDao.saveLoggedDetails(map);
						UserDTO uDto = new UserDTO();
						uDto.setUserGrp(group);
						String saveFlag = "N";
						uDto.setUserName(admin);
						Long newuserId = 0L;
						Long groupId = uDao.getGroupIdbelongstoGroup(group);
						if (groupId == null || groupId == 0) {
							System.out.println("groupId" + groupId);
							groupId = uDao.saveTempUsergrpDetails1(uDto,
									"create", saveFlag);
						}
						if (groupId != 0) {
							uDto.setGroupId(groupId);
							boolean userExist = uDao.getUserAccessWithGroupId(admin, groupId);
							if(!userExist)
							{
								newuserId = uDao.createTempuserMgmt1(uDto, "create",
										saveFlag, groupId);
								uDto.setUserId(newuserId);
								uDao.saveUserPrevDetails(uDto, "update", "Y");
							}

						}
						tabName = "useradmin";
						endTime =  System.currentTimeMillis();
						difference = endTime-startTimeInitial;
						System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
						
						return "TEST";
					}
				} else if (properties.getProperty(CommonConstants.CSR_ADMIN_USER_ID) != null && !properties.getProperty(CommonConstants.CSR_ADMIN_USER_ID).equals(" ")
						&& loginDao.getPastLoggedInCount()==1 ) {
					//System.out.println("***********************Start of second login block ***********************");
					String admin = properties
					.getProperty(CommonConstants.CSR_ADMIN_USER_ID);
					String pwd = properties
					.getProperty(CommonConstants.CSR_ADMIN_PWD);
					String group = properties
					.getProperty(CommonConstants.CSR_ADMIN_GROUP_NAME);
					System.out.println(admin+"&&&&&&Group:i am here&&&&&&&&" + getUserName()+""+getUserName().equalsIgnoreCase(admin));
					System.out.println(pwd+"&&&&&&Group:i am here&&&&&&&&" + getUserPwd()+""+getUserPwd().equalsIgnoreCase(pwd));

					if (getUserName().equalsIgnoreCase(admin)

							&& getUserPwd().equalsIgnoreCase(pwd)) {
						logger.info("In Administrator blcok:");
						System.out.println("In Administrator blcok:");
						//uDto.setFlag("Y");
						map.put("success", "Y");
						map.put("logged", "Y");
						map.put("logId", logID);
						session.put("logId", logID);
						hs.setAttribute("logId", logID);
						logID = this.loginDao.saveLoginDetails(map);

						session.put("logId", logID);



						this.loginDao.saveLoggedDetails(map);
						if(userGroup==null || userGroup.equalsIgnoreCase("error"))
						{
							setMessage("Please create admin to proceed");
							endTime =  System.currentTimeMillis();
							difference = endTime-startTimeInitial;
							System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
							
							return "CREATEADMIN";
						}
						else
						{


							properties.setProperty(CommonConstants.CSR_ADMIN_USER_ID, " ");
							//.remove(CommonConstants.CSR_ADMIN_USER_ID);
							properties.setProperty(CommonConstants.CSR_ADMIN_PWD," ");
							properties.setProperty(CommonConstants.CSR_ADMIN_GROUP_NAME," ");
							//properties.store(new FileOutputStream(CommonConstants.CSR_PROPERTY_FILE_NAME), null);
							endTime =  System.currentTimeMillis();
							difference = endTime-startTimeInitial;
							System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
							
							
							return "ADMIN";
						}


						// session.put("UserID", "51");
						//tabName = "useradmin";

					}



				} else if (userGroup!=null && !userGroup.equalsIgnoreCase("error")){
					//System.out.println("***********************Start of user login block ***********************");
					/*String displayuserId = LDAPUtils.getDisplayNameOfUser(getUserName());
					  System.out.println("displayuserId::"+displayuserId);
					  if(displayuserId == null){
						authenticate = LDAPUtils.authenticateUser(getUserName(),getUserPwd());
					  }
					  else{
						authenticate = LDAPUtils.authenticateUser(displayuserId,getUserPwd());
					  }*/
						authenticate = LDAPUtils.authenticateUser(getUserName(),getUserPwd());
					if(authenticate == null || authenticate.equalsIgnoreCase("error")){
						map.put("success", "N");
						// map.put("success", "N");
						logID = loginDao.saveLoginDetails( map);
						map.put("logged", "Y");
						map.put("logId", logID);
						session.put("logId", logID);
						loginDao.saveLoggedDetails(map);
						setMessage("Access Denied.");
						endTime =  System.currentTimeMillis();
						difference = endTime-startTimeInitial;
						System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
						
						return "AUTH_FAILED";
					}
					logger.info("After LDAP user authentication");

					Long UserID = 0L;
					boolean isAccess = false;

					List list = uDao.getUserAccess(getUserName(), userGroup, propertyFile);
					
					String propertyFile1 = CommonConstants.CSR_PROPERTY_FILE_NAME;
					Properties properties1 = PropertyUtils.getProperties(propertyFile1);
					String adminId=	properties1.getProperty(CommonConstants.ADMINISTRATOR);
					
					if(adminId==null) adminId="";
					if(adminId.equalsIgnoreCase(getUserName())){
					
						map.put("success", "Y");
						map.put("name", getUserName());
						logID = loginDao.saveLoginDetails( map);
						map.put("logged", "Y");
						map.put("logId", logID);
						session.put("logId", logID);
						hs.setAttribute("pradmincreation", "true");
						hs.setAttribute("sysaccesscontrol", "true");
						hs.setAttribute("reports", "true");
						if(properties.getProperty(CommonConstants.CSR_ADMIN_AUDIT).equalsIgnoreCase("yes")){
							hs.setAttribute("audit", "true");
						}
						loginDao.saveLoggedDetails(map);
						UserDAO u=new UserDAO();
						u.deleteAdminFromDB();
						tabName = "useradmin";
						endTime =  System.currentTimeMillis();
						difference = endTime-startTimeInitial;
						System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
						
						return "ADMIN";
					}
					
					
					System.out.println("getUserAccess:" + list);
					Iterator iter = list.iterator();
					String accessDate = "";
					String accFromDate = "";
					String accToDate = "";
					String accFromDays = "";
					String accToDays = "";
					String active = "";
					if (iter.hasNext()) {
						while (iter.hasNext()) {

							UserDTO dto = (UserDTO) iter.next();
							System.out.println(userGroup+"userGrp:"+dto.getUserGrp());
							System.out.println(userGroup
									.equalsIgnoreCase(dto.getUserGrp()));
							if (dto.getUserGrp() != null) {
								System.out.println(userGroup
										+ "checking Groups" + dto.getUserGrp());
								active = dto.getActive();
								accFromDate = dto.getAccessdateFrom();
								accToDate = dto.getAccessdateTo();
								System.out.println("active Flag:"+active+"accFromDate:"
										+ accFromDate + "accToDate:"
										+ accToDate);
								if(!active.equalsIgnoreCase("Active")){
									map.put("success", "N");
									logID = loginDao.saveLoginDetails( map);
									map.put("logged", "N");
									map.put("logId", logID);
									hs.setAttribute("logId", logID);
									setMessage("Cannot Login. User is "+active);
									endTime =  System.currentTimeMillis();
									difference = endTime-startTimeInitial;
									System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
									
									return "AUTH_FAILED";
								}
								boolean checkDate = true;

								checkDate = CompareDate.checkDiffDate(
										accFromDate, accToDate);
								int year = Calendar.getInstance().get(
										Calendar.YEAR);
								boolean getDay = GetDay.getDay(
										dto.getAccessfromDays(),
										dto.getAccesstoDays());
								System.out.println("checkDate::"+checkDate+"year::"+year+"getDay::"+getDay);

								session.put("UserID", dto.getUserId());
								UserID = dto.getUserId();
								System.out.println(checkDate
										+ ":checkDate:" + getDay
										+ "checking Year" + year);
								if(checkDate == false){
									map.put("success", "N");
									logID = loginDao.saveLoginDetails( map);
									map.put("logged", "N");
									map.put("logId", logID);
									hs.setAttribute("logId", logID);
									setMessage("User does not have access to the system at this time.");
									endTime =  System.currentTimeMillis();
									difference = endTime-startTimeInitial;
									System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
									
									return "AUTH_FAILED";
								}
								if(getDay == false){
									map.put("success", "N");
									logID = loginDao.saveLoginDetails( map);
									map.put("logged", "N");
									map.put("logId", logID);
									hs.setAttribute("logId", logID);
									setMessage("User does not have access to the system for today's day of the week.");
									endTime =  System.currentTimeMillis();
									difference = endTime-startTimeInitial;
									System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
									
									return "AUTH_FAILED";
								}
								hs.setAttribute("UserGroup", userGroup);
								map.put("group", userGroup);
								session.put("LDAP_GROUP", userGroup);

								List<UserDTO> roleDtos = uDao.getRole(dto
										.getUserId());
								for (UserDTO roleDto : roleDtos) {
									System.out.println("checking Roles"
											+ roleDto.getRoleDesc());

									if (roleDto.getRoleDesc().equalsIgnoreCase(CommonConstants.ROLE_DESC_ADMIN)) {
										roleDesc = roleDto.getRoleDesc();
										System.out.println("*****roleDesc***"+ roleDesc);
									}
									if (roleDto.getRoleDesc().equalsIgnoreCase(CommonConstants.ROLE_DESC_PROJECTADMIN)) {
										roleDesc1 = roleDto.getRoleDesc();
										Long gid=uDao.getGroupIdbelongstoGroup(userGroup);
										List<Long> profileIdList =uDao.getProfileIdForGroups(UserID);
										System.out.println("profileIDList Size:::"+profileIdList.size());
										hs.setAttribute("profileIdList", profileIdList);
										System.out.println("*****roleDesc1***"+ roleDesc1);
									}
									if (roleDto.getRoleDesc().equalsIgnoreCase(CommonConstants.ROLE_DESC_SYSADMIN)) {
										roleDesc2 = roleDto.getRoleDesc();
										System.out.println("*****roleDesc2***"+ roleDesc2);
									}
									if (roleDto.getRoleDesc().equalsIgnoreCase(CommonConstants.ROLE_DESC_CSR)) {
										roleDesc3 = roleDto.getRoleDesc();
										System.out.println("*****roleDesc3***"+ roleDesc3);
									}
									if (roleDto.getRoleDesc().equalsIgnoreCase(CommonConstants.ROLE_DESC_REPOSITORY)) {
										roleDesc4 = roleDto.getRoleDesc();
										System.out.println("*****roleDesc4***"+ roleDesc4);
									}
									if (roleDto.getRoleDesc().equalsIgnoreCase(CommonConstants.ROLE_DESC_REP)) {
										roleDesc5 = roleDto.getRoleDesc();
										System.out.println("*****roleDesc5***"+ roleDesc5);
									}
									if (roleDto.getRoleDesc().equalsIgnoreCase(CommonConstants.ROLE_DESC_AUDIT)) {
										roleDesc6 = roleDto.getRoleDesc();
										System.out.println("*****roleDesc6***"+ roleDesc6);
									}

									System.out.println("userId.." + UserID);
									hs.setAttribute("UserId", UserID);

									session.put("UserID", UserID);
								}



							}

						}
					} else {
						map.put("success", "N");
						logID = loginDao.saveLoginDetails( map);
						map.put("logged", "Y");
						map.put("logId", logID);
						hs.setAttribute("logId", logID);
						setMessage("Invalid User.");
						endTime =  System.currentTimeMillis();
						difference = endTime-startTimeInitial;
						System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
						
						return "AUTH_FAILED";
					}
				}
				if ( roleDesc.equalsIgnoreCase(CommonConstants.ROLE_DESC_ADMIN)) {
					System.out.println("coming here: admin*******");
					map.put("success", "Y");
					map.put("name", getUserName());
					logID = loginDao.saveLoginDetails( map);
					map.put("logged", "Y");
					map.put("logId", logID);
					session.put("logId", logID);
					hs.setAttribute("pradmincreation", "true");
					hs.setAttribute("sysaccesscontrol", "true");
					hs.setAttribute("reports", "true");
					if(properties.getProperty(CommonConstants.CSR_ADMIN_AUDIT).equalsIgnoreCase("yes")){
						hs.setAttribute("audit", "true");
					}
					
					String propertyFile1 = CommonConstants.CSR_PROPERTY_FILE_NAME;
					Properties properties1 = PropertyUtils.getProperties(propertyFile1);
				String adminId=	properties1.getProperty(CommonConstants.ADMINISTRATOR);
					if(adminId==null || adminId.equalsIgnoreCase(""))
					{
						
					}
					else
					{
						UserDAO u=new UserDAO();
						u.deleteAdminFromDB();
					}
					
					loginDao.saveLoggedDetails(map);
					tabName = "useradmin";
					endTime =  System.currentTimeMillis();
					difference = endTime-startTimeInitial;
					System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
					
					return "ADMIN";
				} if (roleDesc1.equalsIgnoreCase(CommonConstants.ROLE_DESC_PROJECTADMIN)) {
					System.out.println("coming here:Project admin*******");
					map.put("success", "Y");
					map.put("name", getUserName());
					logID = loginDao.saveLoginDetails( map);
					map.put("logged", "Y");
					map.put("logId", logID);
					session.put("logId", logID);
					hs.setAttribute("userindexcontrol", "true");
					hs.setAttribute("profileuserassign", "true");
				//	hs.setAttribute("useraccesscontrol", "true");
					
					if (roleDesc3.equalsIgnoreCase(CommonConstants.ROLE_DESC_CSR)) {
						hs.setAttribute("search", "true");
					}
					if (roleDesc4.equalsIgnoreCase(CommonConstants.ROLE_DESC_REPOSITORY)) {
						hs.setAttribute("repository", "true");
					}
					if (roleDesc5.equalsIgnoreCase(CommonConstants.ROLE_DESC_REP)) {
						hs.setAttribute("reports", "true");
					}

					loginDao.saveLoggedDetails(map);
					endTime =  System.currentTimeMillis();
					difference = endTime-startTimeInitial;
					System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
					
					return "PADMIN";
				} 
				if (roleDesc3.equalsIgnoreCase(CommonConstants.ROLE_DESC_CSR)) {
					System.out.println("coming here: csr*******");
					map.put("success", "Y");
					map.put("name", getUserName());
					logID = loginDao.saveLoginDetails( map);
					map.put("logged", "Y");
					map.put("logId", logID);
					session.put("logId", logID);
					hs.setAttribute("search", "true");
					if (roleDesc4.equalsIgnoreCase(CommonConstants.ROLE_DESC_REPOSITORY)){
						hs.setAttribute("repository", "true");
					}
					if (roleDesc5.equalsIgnoreCase(CommonConstants.ROLE_DESC_REP)) {
						hs.setAttribute("reports", "true");
					}
					if (roleDesc6.equalsIgnoreCase(CommonConstants.ROLE_DESC_AUDIT)) {
						hs.setAttribute("audit", "true");
					}
					loginDao.saveLoggedDetails(map);
					endTime =  System.currentTimeMillis();
					difference = endTime-startTimeInitial;
					System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
					
					return "CSR";
				}
				if(roleDesc==""&&roleDesc1==""&&roleDesc2==""&&roleDesc3==""&&roleDesc4==""&&roleDesc5==""&&roleDesc6==""){
					map.put("success", "N");
					// map.put("success", "N");
					logID = loginDao.saveLoginDetails( map);
					map.put("logged", "Y");
					map.put("logId", logID);
					session.put("logId", logID);
					loginDao.saveLoggedDetails(map);
					setMessage("Access Denied.");
					endTime =  System.currentTimeMillis();
					difference = endTime-startTimeInitial;
					System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
					
					return "AUTH_FAILED";
				} 
			}
			
			endTime =  System.currentTimeMillis();
			difference = endTime-startTimeInitial;
			System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
			
			// map.put("logged", "Y");
			// map.put("logId", logID);
		}

		catch (Exception e) {
			map.put("success", "N");
			try {
				logID = loginDao.saveLoginDetails( map);
				session.put("logId", logID);
				System.out.println("Error is here:catch-->" + getUserName());
				addActionError("User Authentication Failed.Please Contact Administrator!!");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			endTime =  System.currentTimeMillis();
			difference = endTime-startTimeInitial;
			System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
			
			return "AUTH_FAILED";
		}
		map.put("success", "N");
		try {
			logID = loginDao.saveLoginDetails( map);
			session.put("logId", logID);

			setMessage("Invalid User.");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		endTime =  System.currentTimeMillis();
		difference = endTime-startTimeInitial;
		System.out.println("********* Elapsed milli seconds - Total time taken for login method : " + difference);
		
		return "AUTH_FAILED";
	}

	

}