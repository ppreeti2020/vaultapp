package com.pb.login;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.dispatcher.SessionMap;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.pb.dao.LoginDAO;

public class Logout extends ActionSupport implements SessionAware,
		ServletRequestAware, ServletResponseAware {
	
	static final Logger logger = Logger.getLogger(Logout.class);
	LoginDAO dao = new LoginDAO();
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private Map<String, Object> session;
	private Map<String, Object> parameter;

	public Map<String, Object> getParameter() {
		return parameter;
	}

	public void setParameter(Map<String, Object> parameter) {
		this.parameter = parameter;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public HttpServletResponse getServletResponse() {
		return this.servletResponse;
	}

	public void setServletResponse(HttpServletResponse paramHttpServletResponse) {
		this.servletResponse = paramHttpServletResponse;
	}

	public HttpServletRequest getServletRequest() {
		return this.servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public Map<String, Object> getSession() {
		return this.session;
	}

	public void loadStrutsSession() {
		this.session = ActionContext.getContext().getSession();
	}

	public void loadStrutsParameter() {
		this.parameter = ActionContext.getContext().getParameters();
	}

	public void loadDefault() {
		loadStrutsParameter();
		loadStrutsSession();
	}

	public String execute() {
		try {
			//Map session = ActionContext.getContext().getSession();
			this.session = ActionContext.getContext().getSession();

			session.remove("LOGGED-IN");
			session.remove("LDAP_GROUP");
			session.remove("UserID");
						
			Long logId;
			if (getServletRequest().getSession().getAttribute("UserId") != null)
				getServletRequest().getSession().removeAttribute("UserId");
			if (getServletRequest().getSession().getAttribute("UserGroup") != null)
				getServletRequest().getSession().removeAttribute("UserGroup");
			if (getServletRequest().getSession().getAttribute("textSearch") != null)
				getServletRequest().getSession().removeAttribute("textSearch");
			
			/* Below condition commented as part of AVA-12.1, 12.2, 12.3, 12.4 and SCT M-02 fixes for VaultApp application
			 * Reset the JSESSION ID after Logout*/
			//if (session instanceof org.apache.struts2.dispatcher.SessionMap) {
				try {
					if (session.get("logId") != null) {
						logId = Long.parseLong(session.get("logId").toString());
						HashMap map = new HashMap();
						map.put("logId", logId);
						map.put("logout", "logout");
						session.remove("logId");
						dao.saveLogoutDetails(map);
					}

					((SessionMap)this.session).invalidate();
					System.out.println("TESTING INVALIDATE() IN LOGOUT 1");

				} catch (IllegalStateException e) {
					logger.error("[Logout][execute] IllegalStateException "
							+ e.getMessage());
				}
			//}

			logger.info("[Logout][execute] Session invalidated");
		} catch (Exception e) {
			addActionError("Server Down, Sorry for Inconvenience");
			e.printStackTrace();
			return "FAILURE";
		}
		return "SUCCESS";
	}

	
}
