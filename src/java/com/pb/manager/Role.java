package com.pb.manager;

import java.io.Serializable;

/**
 * Represents the Roles that can be assign to the users of the application.
 */
public interface Role extends Serializable {

    /** */
    String VAULT_ADMINISTRATOR = "administrator";

    /**
     * Returns the name of the role.
     * @return non-null String
     */
    String getName();

    /**
     * Returns the description of the role. TODO: Check if we can remove this.
     * @return non-null String
     */
    String getDescription();
    
    void setName(String name);
    
    void setDescription(String description);

}
