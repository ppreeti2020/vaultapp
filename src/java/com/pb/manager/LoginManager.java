package com.pb.manager;

import com.pb.dto.LoginDTO;

public interface LoginManager {

	/*
	 * To save login details
	 */
	public long saveLoginDetails(String path, LoginDTO logDto);
	/*
	 * to get logout details
	 */
	public long saveLogoutDetails(LoginDTO dto,String realpath);
	/*
	 * To save frequently logged details
	 */
	public long saveLoggedDetails(LoginDTO loginDto,String realpath);
	/*
	 * To save integrity check of table
	 */
	public void saveLogIntegrity(String tableName, String ID, String newHash,
			String oldHash, Long logId, String flag,String realpath) ;
	public long getPastLoggedInCount(String realPath) ;
}
