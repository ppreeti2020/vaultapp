package com.pb.manager;

import java.util.List;

import com.pb.dto.DataDTO;
import com.pb.dto.UserDTO;

public interface UserManager {

	/*
	 * return groupId belongs to group
	 */
	public Long getGroupIdbelongstoGroup(String groupName,String realpath);
	/*
	 * Save group details temporarily in database
	 */
	public long saveTempUsergrpDetails(UserDTO dto, String action,
			String saveFlag,String realpath);
	/*
	 *  To get user details based on groupId
	 */
	public boolean getUserAccessWithGroupId(String userName, Long groupId,String realpath);
	/*
	 * Create User Temporarily until user claick submit
	 */
	public Long createTempuser(UserDTO dto, String action, String flag,
			Long groupId,String realpath);
	/*
	 * To save all details of user info
	 */
	public long saveUserPrevDetails(UserDTO dto, String action, String flag,String realpath);
	/*
	 * To get all roles
	 */
	public List<UserDTO> getRoles(String realpath);
	/*
	 * To insert Role
	 */
	public void insertRole(Long userId,Long roleId,String realpath);
	/*
	 * get UserAccess
	 */
	public List<UserDTO> getUserAccess(String userName, String userGroup,String realpath);
	/*
	 * 
	 */
	public List<UserDTO> getRole(Long userId,String realpath);
	/*
	 * get integrity check of table
	 */
	
	public String getIntegrity(String tableName, String ID, String oldHash,
			Long tabId,String realpath);
	
	/*
	 * get integrity check of table
	 */
	public void saveUserIntegrity(String tableName, String ID, String newHash,
			String oldHash, Long logId, String flag,String realpath);
	
	/*
	 * To get vault Database Name
	 */
	
	public Long getVaultDBName(String dbName,Long hostId,String realpath);
	/*
	 * save vault dbname into database
	 */
	
	public Long saveDBName(String dbName, String dbDesc,Long hostId,String realpath);
	
	public boolean checkDBIndex(Long dbId,String realpath);
	
	public boolean checkDBIndex(Long dbId,String indexName,String realpath);
	
	public Long saveDBIndex(Long dbId,String indexName, String indexDesc,String realpath);
	
	public List<UserDTO> getAllUsers(String realpath);
	
	public boolean deleteUser(Long userId,String realpath);
	
	public List<UserDTO> getUser(Long userId,String realpath);
	
	public void updateUSERRole(Long userId, Long roleId,String realpath);
	
	public boolean getCsrUsers(Long userId,String realpath);
	
	public UserDTO getUserName(Long userId,String realpath);
	
	public void DeleteRole(Long userId,String realpath);
	
	public String getRoleName(Long userID,String realpath);
	
	public List getCsrActions(String realpath);
	
	public List<UserDTO> getUserAccessUsingGroups(String groupName,String realpath);
	
	public List<UserDTO> getDatabases(String realpath);
	
	public List<DataDTO> getIndexByProfile(String path,String profile);
	public boolean isUserExists(String uid,String realpath);
	public List<DataDTO> getIndexByDBID(String path,Long dbId,Long hostId);
}
