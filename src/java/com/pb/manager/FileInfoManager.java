package com.pb.manager;

import java.util.List;

import com.pb.dto.DataDTO;
import com.pb.dto.FileDTO;

public interface FileInfoManager {

	public void saveAFPFileinfo(FileDTO dto,String realpath);
	
	public List<FileDTO> getAFPFileinfos(String fileName,String realpath);
	
	public List<FileDTO> getAFPFileinfo(String realpath);
	
	public void saveFileIntegrity(String tableName, String ID, String newHash,
			String oldHash, Long logId, String flag,String realpath);
	
	public DataDTO getFileIntegrity(String fileName,String realpath);
	
	public void saveProfileDetails(DataDTO dao,String path);
	
	public void saveIndexDetails(DataDTO dao,String path);
	
	public List<DataDTO> getIndexInte(String fileName,Long dbId,Long fileSize,String path);
	
	public void updateIndexDetails(DataDTO dao,String path);
	
	public void updateProfileDetails(DataDTO dao,String path);
	
	public List<DataDTO> getProfileDetails(String realPath);
	
	public List<DataDTO> getProfileDetails(Long hostId,String realPath);
	
	public List<DataDTO> getProfileDetails(Long hostId,Long profile,String realPath);
	
	public List<DataDTO> getIndexDetails(Long hostId,Long dbId,String path);
	
	public List<DataDTO> getIndexDetails(Long hostId,String path);
	
	public List<DataDTO> getIndexDetails(String path);
	public List<DataDTO> getIndexInteFromDB(String fileName,Long dbId,String path);
	public Long getProfileInteIdExists(String profileName,String realPath);
}
