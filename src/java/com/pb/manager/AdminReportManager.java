package com.pb.manager;


import java.util.List;
import java.util.Map;

import com.pb.dto.DataDTO;
import com.pb.dto.UserDTO;

public interface AdminReportManager {
	/*
	 * To get Login Reports for auditing the application(who all are trying to
	 * login to application)
	 */
	public List<DataDTO> getLoginReports(String path);
	/*
	 * To get Login Reports for auditing the application
	 * To get unsuccessful login reports
	 */
	public List<DataDTO> getUnsuccessfulLoginReports(String path);
	
	/*
	 * To get Login Reports for auditing the application
	 * To get get Logout Results for each login
	 */
	public List<DataDTO> getLogoutReports(String path);
	
	/*
	 * To get count of login application
	 */
	public List<DataDTO> getCountLogged(String date,String path);
	
	/*
	 * To get count of login application
	 */
	public List<DataDTO> getCountLoggedInfo(String path);
	/*
	 * To get integrity check 
	 */
	public List<DataDTO> getIntegrityCheckInfo(String path);
	
	/*
	 * To get logout table integrity check 
	 */

	public List<UserDTO> getLoginTableIntCheck(String tableName,String path);
	
	/*
	 * To get root log profiles for treeview
	 */
	public Map<Long,DataDTO> getRootLogReports(String path);
	
	/*
	 * To get log profiles 
	 */
	public Map<Long,DataDTO> getLogProfiles(String path);
	
	/*
	 * To get Log File Profiles
	 */
	public List<DataDTO> getLogFileReports(Long hostId,String path);
	
	/*
	 * To get Log File Profiles
	 */
	public List<DataDTO> getLogFileProfiles(String type,String path);
	
	/*
	 * To get Log File Reports
	 */
	public List<DataDTO> getLogFileReports(String name,Long hostId,String path) ;
	
	/*
	 * To get Log File Resources
	 */
	public List<DataDTO> getLogFileResources(Long logFileId,String path);
	
	/*
	 * To get count of resources
	 */
	public int getLogCountResources(Long logFileId,String path);
	
	/*
	 * To get Log file Errors
	 */
	public List<DataDTO> getLogFileErrors(Long logFileId,String path);
	/*
	 * To get Log file Indexes
	 */
	public List<DataDTO> getLogFileIndexes(Long logFileId,String path);
	
	/*
	 * To get Log File Resources
	 */
	public List<DataDTO> getLogFileResources(String realPath);
	
	/*
	 * To get Log File Resources based on type (Indexing,Reindexing etc)
	 */
	public List<DataDTO> getLogFileResources(String type,String realPath);
	
	public List<DataDTO> getLogFileReports(String profile,String date,String type,Long hostId,String path);
	
	public List<DataDTO> getLogFileReports(String profile,String date,Long hostId,String path);
	
	public Map<Long,DataDTO> getDatabases(String realpath);
	
	public List<DataDTO> getDatabase(Long hostId,String realpath);
	
	public List<DataDTO> getIndexes(String path,Long dbId);
	
	public List<DataDTO> getLogFileReportsWithIndexes(String name,Long hostId,String path);
	
		public List<DataDTO> getLogFileReportsWithIndexes(String profile, String date,
				String type, Long hostId, String path) ;
		
		public List<DataDTO> getlogFileResourcesByDBIdAndHostId(Long hostId,Long dbId,String realPath);
		public List<DataDTO> getResourcesIntegrityDetails(String realPath);
		public List<DataDTO> getResourcesIntegrityDetailsByProfile(String realPath,Long dbId,Long hostId);
		
		public List<DataDTO> getLogFileReportsWithResources(Long hostId, String path) ;
}
