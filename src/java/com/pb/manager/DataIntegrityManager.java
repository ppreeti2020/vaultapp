package com.pb.manager;

import java.io.InputStream;
import java.util.List;

import com.pb.dto.DataDTO;

public interface DataIntegrityManager {

	public void saveFileDetails(DataDTO dao,String realpath);
	
	public Long saveLogHost(DataDTO dao,String realpath);
	
	public Long getLogHost(DataDTO dao,String realpath);
	
	public void saveDocFileDetails(DataDTO dao,String realpath);
	
	public void saveIntegrityCheckDetails(DataDTO dao,String realpath);
	
	public void saveDocIntegrityCheckDetails(DataDTO dao,String realpath);
	
	public void updateFileDetails(DataDTO dao,String realpath);
	
	public void updateDocFileDetails(DataDTO dao,String realpath);
	
	public List<DataDTO> getFileDetails(String realpath) ;
	
	public List<DataDTO> getSingleFileDetails(String fileName, String newFileName,String path,String realpath);
	
	public List<DataDTO> getDocSingleFileDetails(String fileName, String newFileName,String path,String realpath);
	
	public void saveLogIntegrityCheckDetails(DataDTO dao,String realpath);
	
	public String getLogProfile(String drpFile, String realpath);
	
	public Long saveLogFileDetails(DataDTO dao, InputStream in,String realpath);
	
	public void updateLogFileDetails(DataDTO dao, InputStream in, String status,String realpath);
	
	public Long CheckLogFileDetails(String fileName, String path, Long size, String desc,String realpath);
	
	public List<DataDTO> getLogFileDetails(Long e2LogFileId, Long size,String realpath);
	
	public Long saveLogFileData(DataDTO dao, Long e2logFileId,
			boolean errorOccurs,String realpath);
	
	public Long updateLogFileData(DataDTO dao, Long logFileId,
			String compressionType, boolean errorOccurs,String realpath);
	
	public Long updateLogFileStatus(DataDTO dao, Long logFileId, String status,
			boolean errorOccurs,String realpath);
	
	public Long updateLogFileType( Long logFileId, String type,String realpath);
	
	public Long getLogFileData(String comressingFile,Long e2logFileId, String desc,String[] errorCodes,String realpath);
	
	public Long getLogFileErrorData(String comressingFile, String desc,String realpath);
	
	public Long saveLogErrorData(String errorCode, String errors,
			Long logFileId, int errorCount,String realpath);
	
	public Long saveLogResources(String[] wrote, String[] size, String date,
			String resStartTime, String resFinishingTime, Long logFileId,
			int count,String realpath);
	
	public Long getLogResources(String wrote, String size, String date,
			String resStartTime, String resFinishingTime, Long logFileId,String realpath);
	
	public Long saveLogIndexing(DataDTO dao, Long logFileId,String realpath);
	
	public Long updateLogIndexing(DataDTO dao, Long logFileId,String realpath);
	
	public Long saveLogIndexDetails(DataDTO dao, Long indexId, int count,String realpath);
	
	public Long getLogIndexDetails(DataDTO dao, Long indexId,String realpath);
	
	public List<DataDTO> getLogProfiles(String realpath);
	
	public List<DataDTO> getLogResources(Long logFileId,String realpath);
	
	public void saveLogResourceStatus(Long logFileId,Long resourceId, String resourceName, String status, String newFileName,String realpath,String action);

	public Long getLogIndexFile(DataDTO dao,boolean errorOccurs,Long e2LogFileId, String[] errorCodes,String type,String realpath);
	
	public List<DataDTO> getDOCFileDetails(String path);
	
	public List<DataDTO> getLogIndex(Long logFileId,String realpath);
	
	public List<DataDTO> getSingleResourceFileDetails(String fileName,String newFileName, String action,String realpath);
	public void saveResourceFileDetails(DataDTO dao,String path);
	public void updateResourceFileDetails(DataDTO dao,String path);
	public List<DataDTO> getFileDetailsWithProfile(String path,Long dbId,Long hostId);
	public List<DataDTO> getDOCFileDetailsWithProfile(String path,Long dbId,Long hostId) ;
}
