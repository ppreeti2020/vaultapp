package com.pb.manager;

import java.sql.Connection;

public interface DBConnectionManager {

	Connection getDBConnection();
}
