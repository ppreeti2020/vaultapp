package com.pb.ldap;

//.sun.jndi.ldap.LdapCtxFactory;
//import DataEncryptDecrypt2;

//import DataEncryptDecrypt2;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.naming.AuthenticationException;
import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;

import org.apache.log4j.Logger;
//import org.apache.poi.hssf.record.formula.MemErrPtg;

import com.pb.common.CommonConstants;
import com.pb.common.DataEncryptDecrypt;
import com.pb.common.PropertyUtils;

public class LDAPUtils {

	public static final Logger logger = Logger.getLogger(LDAPUtils.class);
	public static final String DISTINGUISHED_NAME = "distinguishedName";
	public static final String CN = "cn";
	public static final String MEMBER = "member";
	public static final String MEMBER_OF = "memberOf";
	public static final String SEARCH_BY_SAM_ACCOUNT_NAME = "(SAMAccountName={0})";
	public static final String SEARCH_GROUP_BY_GROUP_CN = "(&(objectCategory=group)(cn={0}))";

	private static final String[] attrIdsToSearch = new String[] { MEMBER_OF }; 
	private static final String GROUPS_OU = "ou=Bu1,o=sgpbsdemo.com";
	public static final String SEARCH_BY_SAM_ACCOUNT_NAME2 = "(sAMAccountName=%s)"; 
	//public static final String SEARCH_GROUP_BY_GROUP_CN = "(&(objectCategory=group)(cn={0}))"; 

	//objectCategory=Person)(anr={0}) 
	public static final String SEARCH_BY_SAM_USERID="(userPrincipalName={0})";

	private static LdapContext getInitialContext(String hostName, String port,String username, String password,String domain)
			throws NamingException {
	//	System.out.println("UserName::"+username);
		String providerURL = new StringBuffer("ldap://")
		.append(hostName)
		.append(":")
		.append( port)
		.toString();
		
		String ldapPropert =CommonConstants.LDAP_CONFIG_FILE_NAME; 

	/*	Properties ldapProperties = PropertyUtils.getProperties(ldapPropert);
	String userdn=	ldapProperties.getProperty(CommonConstants.LDAP_User_DN);
		*/
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				CommonConstants.LDAP_INITIAL_CTX_FACTORY);
		env.put(Context.PROVIDER_URL,providerURL);
		env.put(Context.SECURITY_AUTHENTICATION,
				CommonConstants.SIMPLE_AUTHENTICATION);
	//	String sp = "cn="+username+"";
		String sp = username+"@"+ domain;
		System.out.println("sp::"+sp);
	//	System.out.println("password"+password);
		env.put(Context.SECURITY_PRINCIPAL,username);
		env.put(Context.SECURITY_CREDENTIALS, password);
System.out.println("User Name is"+username);
//System.out.println("passworrd is"+password);
		env.put("java.naming.referral", "follow");
		System.out.println("No SSL");
		
		//System.out.println("@@@@@@@@@@ before ldap initial context create");
		
		System.out.println("################### host name : " + hostName);
		System.out.println("################### port : " + port);
		System.out.println("################### username : " + username);
		System.out.println("################### ldap domain : " + domain);
		
		InitialLdapContext i=new InitialLdapContext(env,null);
		
		//System.out.println("@@@@@@@@@@ after ldap initial context create");
		
System.out.println("i is"+i);
		return i;
	}	
	
	
	
	/*private static LdapContext getInitialContextForAuth(String hostName, String port,String username, String password,String domain)
			throws NamingException {
		System.out.println("UserName::"+username);
		String providerURL = new StringBuffer("ldap://")
		.append(hostName)
		.append(":")
		.append( port)
		.toString();
		
		String ldapPropert =CommonConstants.LDAP_CONFIG_FILE_NAME; 

		//Properties ldapProperties = PropertyUtils.getProperties(ldapPropert);
	//String userdn=	ldapProperties.getProperty(CommonConstants.LDAP_User_DN);
		
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				CommonConstants.LDAP_INITIAL_CTX_FACTORY);
		env.put(Context.PROVIDER_URL,providerURL);
		env.put(Context.SECURITY_AUTHENTICATION,
				CommonConstants.SIMPLE_AUTHENTICATION);
		//String sp = "cn="+username+"";
		String sp = username+"@"+ domain;
		System.out.println("sp::"+sp);
		env.put(Context.SECURITY_PRINCIPAL,sp);
		env.put(Context.SECURITY_CREDENTIALS, password);

		env.put("java.naming.referral", "follow");
		System.out.println("No SSL");
		InitialLdapContext i=new InitialLdapContext(env,null);
System.out.println("i is"+i);
		return i;
	}	
	
	*/
	
	
	
	
	
	private static LdapContext getSecureInitialContext(String hostName, String port,String username, String password,String sslPath,String sslPassword,String context)
			throws NamingException {

		String providerURL = new StringBuffer("ldaps://")
		.append(hostName)
		.append(":")
		.append( port)
		.toString();
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				CommonConstants.LDAP_INITIAL_CTX_FACTORY);
		System.setProperty("javax.net.ssl.trustStore",sslPath);
		System.out.println("certificate path :" + sslPath);

		System.setProperty("javax.net.ssl.trustStorePassword",sslPassword);
		env.put(Context.PROVIDER_URL,providerURL);
		env.put(Context.SECURITY_AUTHENTICATION,
				CommonConstants.SIMPLE_AUTHENTICATION);
		env.put(Context.SECURITY_PRINCIPAL,
				"cn="+username
				+ ","
				+ context);
		env.put(Context.SECURITY_CREDENTIALS, password);


		return new InitialLdapContext(env,null);
	}	
	
	



	public static String getUserDNBySamAccountName(String samAccoutnName ) {  

		System.out.println("Start getUserDNBysamAccoutnName="+samAccoutnName);
		
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);

		String samaccntName="";

		// String domain = "TestAD.com";
		// Properties properties =
		// PropertyUtils.getProperties(propertiesFileNameWithPath);
		// String principalName = userId +"@"+domain;
		//System.out.println(propertiesFileNameWithPath + "--userId:path" );
		// userId = properties.getProperty(CommonConstants.ADMINISTRATOR);

		//String principalName = userId + "@"
		//	+ properties.getProperty(CommonConstants.DOMAIN);
		// password = properties.getProperty(CommonConstants.CSR_ADMIN_PWD);
		String distinguishedName=null;
		if (properties.getProperty(CommonConstants.LDAP_Host) != null
				&& properties.getProperty(CommonConstants.LDAP_Port) != null) {
			System.out.println(properties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			DirContext ctx = null;
			
			try {
				String hostName = properties.getProperty(CommonConstants.LDAP_Host);
				String port = properties.getProperty(CommonConstants.LDAP_Port);
				String userId = properties
						.getProperty(CommonConstants.LDAP_User_DN);
				String password = properties
						.getProperty(CommonConstants.Password);
				System.out.println(userId+"");
				
				try {
					password=DataEncryptDecrypt.decryptData(password);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					password=properties
							.getProperty(CommonConstants.Password);
							
				}
				
				
				if(password==null || password.equalsIgnoreCase(""))
				{
					password=properties
							.getProperty(CommonConstants.Password);
				}
				
				
				if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{
					if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
								.getProperty(CommonConstants.LDAP_User_DN));
					}
					else if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! host name : " + hostName);
						System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! port : " + port);
						System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! userId : " + userId);
						System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ldap domain : " + properties
								.getProperty(CommonConstants.LDAP_Domain));
						
						ctx = getInitialContext(hostName,port,userId,password,properties
								.getProperty(CommonConstants.LDAP_Domain));
					} }   





				SearchControls constraints = new SearchControls();  
				constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);  
				String[] attrIDs = { "distinguishedName",  
						"sn",  
						"givenname",  
						"mail",  
						"telephonenumber","sAMAccountName"};  
				constraints.setReturningAttributes(attrIDs);  
				//First input parameter is search bas, it can be "CN=Users,DC=YourDomain,DC=com"  
				//Second Attribute can be uid=username  
				/*NamingEnumeration<?> answer = ctx.search(properties
						.getProperty(CommonConstants.LDAP_Naming_Context), "(&(objectClass=userProxy)(samAccountName="+ samAccoutnName + "*))"
						, constraints);  */
				
				String ldapUserFilter = properties
						.getProperty(CommonConstants.LDAP_User_Filter);
				NamingEnumeration<?> answer = ctx.search(properties
						.getProperty(CommonConstants.LDAP_Naming_Context), "(&" +ldapUserFilter	+ "(samAccountName="+ samAccoutnName + "*))"
						, constraints);  
				if (answer.hasMore()) {  
					Attributes attrs = ((SearchResult) answer.next()).getAttributes(); 
				 distinguishedName=	attrs.get("distinguishedName").toString();
					
					System.out.println("distinguishedName "+distinguishedName);  
					System.out.println("givenname "+ attrs.get("givenname"));  
					System.out.println("sn "+ attrs.get("sn"));  
					System.out.println("mail "+ attrs.get("mail"));  
					System.out.println("telephonenumber "+ attrs.get("telephonenumber")); 
					System.out.println("sAMAccountName before split::"+attrs.get("sAMAccountName").toString());
					String samname = attrs.get("sAMAccountName").toString();
					samaccntName= samname.substring(samname.lastIndexOf(':')+1,samname.length()).trim();
					System.out.println(samaccntName);  
				}else{  
					throw new Exception("Invalid User");  
				}  

			} catch (Exception ex) {  
				ex.printStackTrace();  
			}  
			//  return user;  
		}  
		
		System.out.println("End getUserDNBySamactName="+samaccntName);
		return distinguishedName;
	}  


/*	public static String authenticateUser(String userId, String password) {
		
		getUserDNBySamAccountName(user)
		
		return null;
	}
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static String authenticateUser(String userId, String password) {
		//System.out.println(password + "userId:" + userId);
		String authenticate = null;
		DirContext ctx = null;
		String ldapPropert =CommonConstants.LDAP_CONFIG_FILE_NAME; 

		Properties ldapProperties = PropertyUtils.getProperties(ldapPropert);
		if (ldapProperties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out.println(ldapProperties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ ldapProperties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			try {
				String hostName = ldapProperties
						.getProperty(CommonConstants.LDAP_Host);
				
				String port = ldapProperties
						.getProperty(CommonConstants.LDAP_Port);
				System.out.println("ENABLE_SSL:"+ldapProperties.getProperty(CommonConstants.ENABLE_SSL));
				if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{

					if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,ldapProperties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),ldapProperties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),ldapProperties
								.getProperty(CommonConstants.LDAP_User_DN));
						System.out.println("context returned :"+ ctx);
					}
					else if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{//LDAP_User_DN
					//String dn=	getUserDNBySamAccountName(userId);
						System.out.println("User Id before "+userId);
						
						userId=getUserDNBySamAccountName(userId);
						System.out.println("Disctunguised id is"+userId);
						userId=userId.split(":")[1].trim();
						System.out.println("Trimmed user id is"+userId);
						ctx = getInitialContext(hostName,port,userId,password,ldapProperties
								.getProperty(CommonConstants.LDAP_Domain ));
						System.out.println("context returned :"+ ctx);
						if(ctx != null){
							authenticate = "success";
						}
						else if(ctx == null || ctx.equals(""))
						{
							authenticate = "error";
						}
					}

					ctx.close();
				}
				else
				{
					//userGroup = "No authentication Found";
					System.out.println("No authentication Found");
				}
			} catch (NamingException ne) {
				authenticate = "error";
				logger.error("[LDAPUtils][authenticateUser] Error authenticating user:"
						+ userId);
				logger.error(ne.getMessage());
				logger.error(ne.getStackTrace());
				logger.error(ne.getRootCause());
			}

		}

		// if no exception, the user is already authenticated.
		logger.info("[LDAPUtils][authenticateUser] OK, successfully authenticating user");
		return authenticate;
	}
	
	
	
	
	
	
	
	
	
	
	public static String authenticateUserForLdapConfig(String userId, String password) {
		//System.out.println(password + "userId:" + userId);
		String authenticate = null;
		DirContext ctx = null;
		String ldapPropert =CommonConstants.LDAP_CONFIG_FILE_NAME; 

		Properties ldapProperties = PropertyUtils.getProperties(ldapPropert);
		if (ldapProperties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out.println(ldapProperties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ ldapProperties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			try {
				String hostName = ldapProperties
						.getProperty(CommonConstants.LDAP_Host);
				
				String port = ldapProperties
						.getProperty(CommonConstants.LDAP_Port);
				System.out.println("ENABLE_SSL:"+ldapProperties.getProperty(CommonConstants.ENABLE_SSL));
				if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{

					if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,ldapProperties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),ldapProperties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),ldapProperties
								.getProperty(CommonConstants.LDAP_User_DN));
						System.out.println("context returned :"+ ctx);
					}
					else if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{//LDAP_User_DN
					//String dn=	getUserDNBySamAccountName(userId);
						System.out.println("User Id before "+userId);
						
						/*userId=getUserDNBySamAccountName(userId);
						System.out.println("Disctunguised id is"+userId);
						userId=userId.split(":")[1].trim();
						System.out.println("Trimmed user id is"+userId);*/
						ctx = getInitialContext(hostName,port,userId,password,ldapProperties
								.getProperty(CommonConstants.LDAP_Domain ));
						System.out.println("context returned :"+ ctx);
						if(ctx != null){
							authenticate = "success";
						}
						else if(ctx == null || ctx.equals(""))
						{
							authenticate = "error";
						}
					}

					ctx.close();
				}
				else
				{
					//userGroup = "No authentication Found";
					System.out.println("No authentication Found");
				}
			} catch (NamingException ne) {
				authenticate = "error";
				logger.error("[LDAPUtils][authenticateUser] Error authenticating user:"
						+ userId);
				logger.error(ne.getMessage());
				logger.error(ne.getStackTrace());
				logger.error(ne.getRootCause());
			}

		}

		// if no exception, the user is already authenticated.
		logger.info("[LDAPUtils][authenticateUser] OK, successfully authenticating user");
		return authenticate;
	}
	
	
	
	
	
	

	public static String LDAPUserGroup(String userId, String password1) {
		System.out.println("userId:" + userId);
		String userGroup = null;
		DirContext ctx = null;
		String ldapPropert =CommonConstants.LDAP_CONFIG_FILE_NAME; 

		Properties ldapProperties = PropertyUtils.getProperties(ldapPropert);
		if (ldapProperties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out.println(ldapProperties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ ldapProperties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			try {
				String hostName = ldapProperties
						.getProperty(CommonConstants.LDAP_Host);
				//String ldapuserid = LDAPUtils.getUserIDByDistinguishedName(ldapProperties.getProperty(CommonConstants.USER_NAME),"");
				//System.out.println("ldapuserId::"+ldapuserid);
				String adminuser = ldapProperties.getProperty(CommonConstants.USER_NAME);
				String adminpassword = ldapProperties
						.getProperty(CommonConstants.Password);
				try {
					adminpassword=DataEncryptDecrypt.decryptData(adminpassword);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					adminpassword=ldapProperties
							.getProperty(CommonConstants.Password);
				}
				String port = ldapProperties
						.getProperty(CommonConstants.LDAP_Port);
				System.out.println("ENABLE_SSL:"+ldapProperties.getProperty(CommonConstants.ENABLE_SSL));
				if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{

					if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,adminuser,adminpassword,ldapProperties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),ldapProperties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),ldapProperties
								.getProperty(CommonConstants.LDAP_Domain));
						System.out.println("context returned :"+ ctx);
					}
					else if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						ctx = getInitialContext(hostName,port,adminuser,adminpassword,ldapProperties
								.getProperty(CommonConstants.LDAP_Domain));
						System.out.println("context returned :"+ ctx);
					}
					String returnedAtts[] = { "memberOf" };
					/* enable code below if using ADAM */
					/*String searchFilter = "(&(objectClass=userProxy)(sAMAccountName="
							+ userId + "))";*/
					
					/* enable code below if using LDAP */
					String ldapUserFilter = ldapProperties
							.getProperty(CommonConstants.LDAP_User_Filter);
					
					String searchFilter = "(&" + ldapUserFilter
							+ "(sAMAccountName="
					+ userId + "))";

					SearchControls searchCtls = new SearchControls();
					searchCtls.setReturningAttributes(returnedAtts);

					searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

					/*	NamingEnumeration<?> answer = ctx.search(ldapProperties
						.getProperty(CommonConstants.LDAP_Naming_Context),
						searchFilter, searchCtls);*/
					NamingEnumeration<?> answer = ctx.search(ldapProperties
							.getProperty(CommonConstants.LDAP_Naming_Context),
							searchFilter, searchCtls);

					int i=0;
					while (answer.hasMoreElements()) {
						SearchResult sr = (SearchResult) answer.next();
						Attributes attrs = sr.getAttributes();
						i++;
						System.out.print("no of elements::"+i);
						if (attrs != null) {
							NamingEnumeration<?> ne = attrs.getAll();

							while (ne.hasMore()) {

								Attribute attr = (Attribute) ne.next();
								System.out.println("LDAPUTILS authentication>>"+attr.get().toString());
								String tempSplitArr[] = attr.get().toString()
										.split(",");

								// We just need group, now we get CN=CSR UG1 for
								// example.
								String grpTemp = tempSplitArr[0];



								String tempArr[] = grpTemp.split("=");
								userGroup = tempArr[1];


								System.out.println("user belongs to " + grpTemp);
							}

						}
						if(userGroup != null || !userGroup.equals(""))
						{
							break;
						}
					}
					System.out.println("Out of outer while loop");
					if(userGroup == null || userGroup.equals(""))
					{
						userGroup = "error";
					}
					ctx.close();
				}
				else
				{
					//userGroup = "No authentication Found";
					System.out.println("No authentication Found");
				}
			} catch (NamingException ne) {
				userGroup = "error";
				logger.error("[LDAPUtils][authenticateUser] Error authenticating user:"
						+ userId);
				logger.error(ne.getMessage());
				logger.error(ne.getStackTrace());
				logger.error(ne.getRootCause());
			}

		}

		// if no exception, the user is already authenticated.
		logger.info("[LDAPUtils][authenticateUser] OK, successfully authenticating user");
		//System.out.println("above return stmt");
		return userGroup;
	}


	public static String authenticateGroup(String Group,
			String propertiesFileNameWithPath) {
		// System.out.println(password+"userId:"+userId);
		String userGroup = null;
		// String domain = "TestAD.com";

		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME; 
		Properties properties = PropertyUtils.getProperties(propert);

		// String principalName = userId +"@"+domain;
		// System.out.println("1.userId:"+principalName);
		if (properties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out.println(properties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));

			System.out.println(CommonConstants.LDAP_INITIAL_CTX_FACTORY
					+ "--Fact:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			DirContext ctx= null;
			try {
				String hostName = properties.getProperty(CommonConstants.LDAP_Host);
				String port = properties.getProperty(CommonConstants.LDAP_Port);
				String userId = properties
						.getProperty(CommonConstants.USER_NAME);
				String password = properties
						.getProperty(CommonConstants.Password);
				
				try {
					password=DataEncryptDecrypt.decryptData(password);
				} catch (Exception e) {
					// TODO Auto-generated catch block
			password=properties
					.getProperty(CommonConstants.Password);
					
					
				}
				
				if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{
					if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
								.getProperty(CommonConstants.LDAP_User_DN));
						System.out.println("Secure LDAP");
					}
					else if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						ctx = getInitialContext(hostName,port,userId,password,properties
								.getProperty(CommonConstants.LDAP_Domain));
						System.out.println("Secure LDAP");
					}
					String returnedAtts[] = { "memberOf" };
					String searchFilter = "(&(objectClass=group)(sAMAccountName="
							+ Group + "))";

					SearchControls searchCtls = new SearchControls();
					searchCtls.setReturningAttributes(returnedAtts);

					searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

					NamingEnumeration<?> answer = ctx.search(properties
							.getProperty(CommonConstants.LDAP_Naming_Context),
							searchFilter, searchCtls);

					while (answer.hasMoreElements()) {
						SearchResult sr = (SearchResult) answer.next();
						Attributes attrs = sr.getAttributes();

						if (attrs != null) {
							NamingEnumeration<?> ne = attrs.getAll();

							while (ne.hasMore()) {
								Attribute attr = (Attribute) ne.next();
								String tempSplitArr[] = attr.get().toString()
										.split(",");

								// We just need group, now we get CN=CSR UG1 for
								// example.
								String grpTemp = tempSplitArr[0];

								userGroup = grpTemp;

							}
						}
					}
					System.out
					.println("[LDAPUtils][authenticateUser] OK, successfully authenticating user");
					ctx.close();
				}
			} catch (NamingException ne) {
				logger.error("[LDAPUtils][authenticateUser] Error authenticating user:");
				logger.error(ne.getMessage());
			}

			// if no exception, the user is already authenticated.
			logger.info("[LDAPUtils][authenticateUser] OK, successfully authenticating user");
		}
		return userGroup;
	}
	// private static final String USERS_OU =	        "ou=People,o=forethought.com";


	private static String toDC(String domainName) {
		StringBuilder buf = new StringBuilder();
		for (String token : domainName.split("\\.")) {
			if (token.length() == 0)
				continue; // defensive check
			if (buf.length() > 0)
				buf.append(",");
			buf.append("DC=").append(token);
		}
		return buf.toString();
	}

	/* private String getUserDN(String username) {
	        return new StringBuffer()
	                .append("uid=")
	                .append(username)
	                .append(",")
	                .append(USERS_OU)
	                .toString();
	    }
	 */
	public static List getLDAPCSRGroups(String propertiesFileNameWithPath)
	{
		List groups = null;
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		try {
			List ldapGroups = getGroups(propertiesFileNameWithPath);
			if (ldapGroups != null)
			{
				groups = new ArrayList();
				for (int i = 0; i < ldapGroups.size(); i++) 
				{
					SearchResult si = (SearchResult)ldapGroups.get(i); 
					Attributes attribs = si.getAttributes();
					BasicAttribute attr = (BasicAttribute)attribs.get("cn");
					if (attr != null)
					{
					String ss=	(String)attr.get(0);
						if(ss.contains("CN="))
						{
						ss=	ss.replaceAll("CN=", "");
						}
						groups.add(ss);

				}}
			}
			if (groups != null)
				Collections.sort(groups);
				
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return groups;
	}




	public static List getGroups (String propertiesFileNameWithPath) {


		List groupsList = new ArrayList<String>();
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);

		System.out.println(propertiesFileNameWithPath + "--userId:path" );
		// userId = properties.getProperty(CommonConstants.ADMINISTRATOR);

		//String principalName = userId + "@"
		//	+ properties.getProperty(CommonConstants.DOMAIN);
		// password = properties.getProperty(CommonConstants.CSR_ADMIN_PWD);
		if (properties.getProperty(CommonConstants.LDAP_Host) != null
				&& properties.getProperty(CommonConstants.LDAP_Port) != null) {
			LdapContext ctx = null;

			try {
				String hostName = properties.getProperty(CommonConstants.LDAP_Host);
				String port = properties.getProperty(CommonConstants.LDAP_Port);
				String userId = properties
						.getProperty(CommonConstants.USER_NAME);
				String password = properties
						.getProperty(CommonConstants.Password);
				
				try {
					password=DataEncryptDecrypt.decryptData(password);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					password=properties
							.getProperty(CommonConstants.Password);
							
				}
				
				
				System.out.println(userId+"");
				if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{
					if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
								.getProperty(CommonConstants.LDAP_Domain));
						/*	ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
							.getProperty(CommonConstants.LDAP_Naming_Context));*/
					}
					else if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						ctx = getInitialContext(hostName,port,userId,password,properties
								.getProperty(CommonConstants.LDAP_Domain));
						/*	ctx = getInitialContext(hostName,port,userId,password,properties
							.getProperty(CommonConstants.LDAP_Naming_Context));*/
					}
					String returnedAtts[] = { "cn" };
					String searchFilter = "(&(objectClass=group))";

					SearchControls searchCtls = new SearchControls();
					searchCtls.setReturningAttributes(returnedAtts);
					searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
					searchCtls.setCountLimit(0);
					searchCtls.setTimeLimit(0);

					int pageSize = 500;
					byte[] cookie = null;

					//Request the paged results control
					Control[] ctls = new Control[]{new PagedResultsControl(pageSize, Control.NONCRITICAL)};
					System.out.println("ctls"+ctls);
					ctx.setRequestControls(ctls);
					do 
					{
						NamingEnumeration results = ctx.search(properties
								.getProperty(CommonConstants.LDAP_Naming_Context),
								searchFilter, searchCtls);
						System.out.println("results"+results);
						while (results != null && results.hasMoreElements()) {
							SearchResult sr = (SearchResult)results.next();
							if (groupsList == null)
								groupsList = new LinkedList();
							groupsList.add(sr);
							
						}
						// examine the response controls
						cookie = parseControls(ctx.getResponseControls());

						// pass the cookie back to the server for the next page
						ctx.setRequestControls(new Control[]{new PagedResultsControl(pageSize, cookie, Control.NONCRITICAL) });

					} while ((cookie != null) && (cookie.length != 0));

					ctx.close();
				}
			} catch (AuthenticationException a) {
				System.out.println("Authentication failed: " + a);

			} catch (javax.naming.CommunicationException commException)
			{
				System.out.println("javax.naming.CommunicationException in ldap serarch......");

			}catch (NamingException ne) {

				ne.printStackTrace();
				logger.error("[LDAPUtils][getLDAPCSRGroups] Error authenticating user:Error is in gettting Group");
				logger.error(ne.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		System.out.println("Number of LDAP Groups returned is::"+groupsList.size());
		return groupsList;

	}

	public static ArrayList<String> getLDAPCSRUsers(String userId, String password,
			String propertiesFileNameWithPath) {
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		DirContext ctx = null;
		ArrayList<String> groupsList = new ArrayList<String>();
		if (properties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out.println(properties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			try {
				String hostName = properties.getProperty(CommonConstants.LDAP_Host);
				String port = properties.getProperty(CommonConstants.LDAP_Port);
				userId = properties
						.getProperty(CommonConstants.USER_NAME);
				password = properties
						.getProperty(CommonConstants.Password);
				
				try {
					password=DataEncryptDecrypt.decryptData(password);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					password=properties
							.getProperty(CommonConstants.Password);
							
				}
				if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{
					if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
								.getProperty(CommonConstants.LDAP_Domain));
					}
					else if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						ctx = getInitialContext(hostName,port,userId,password,properties
								.getProperty(CommonConstants.LDAP_Domain));
					}

					String returnedAtts[] = { "cn" };
					String ldapUserFilter = properties
							.getProperty(CommonConstants.LDAP_User_Filter);
					String searchFilter = "(&"+ ldapUserFilter
							+ "))";

					SearchControls searchCtls = new SearchControls();
					searchCtls.setReturningAttributes(returnedAtts);

					searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

					NamingEnumeration<?> answer = ctx.search(properties
							.getProperty(CommonConstants.LDAP_Naming_Context),
							searchFilter, searchCtls);

					while (answer.hasMoreElements()) {
						SearchResult sr = (SearchResult) answer.next();
						Attributes attrs = sr.getAttributes();

						if (attrs != null) {
							NamingEnumeration<?> ne = attrs.getAll();

							while (ne.hasMore()) {
								Attribute attr = (Attribute) ne.next();
								// Example: CN=CSR UG1,CN=Users,DC=poc,DC=com
								logger.info("[LDAPUtils][authenticateUsers]  memberOf value:  "
										+ attr.get());
								//String tempSplitArr[] = attr.get().toString()
								//.split(",");

								// We just need group, now we get CN=CSR UG1 for
								// example.

								groupsList.add(attr.get().toString());
							}
						}
					}

					ctx.close();
				}
			} catch (NamingException ne) {
				ne.printStackTrace();
				logger.error("[LDAPUtils][getLDAPCSRGroups] Error authenticating user:");
				logger.error(ne.getMessage());
			}
		}
		return groupsList;

	}

	public static List<String> getGroupsOfUser2(String userId)
	{
		List<String> outList=new ArrayList<String>();
		
		String userGroup = null;
		DirContext ctx = null;
		String ldapPropert =CommonConstants.LDAP_CONFIG_FILE_NAME; 

		Properties ldapProperties = PropertyUtils.getProperties(ldapPropert);
		if (ldapProperties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out.println(ldapProperties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ ldapProperties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			try {
				String hostName = ldapProperties
						.getProperty(CommonConstants.LDAP_Host);
				//String ldapuserid = LDAPUtils.getUserIDByDistinguishedName(ldapProperties.getProperty(CommonConstants.USER_NAME),"");
				//System.out.println("ldapuserId::"+ldapuserid);
				String adminuser = ldapProperties.getProperty(CommonConstants.USER_NAME);
				String adminpassword = ldapProperties
						.getProperty(CommonConstants.Password);
				try {
					adminpassword=DataEncryptDecrypt.decryptData(adminpassword);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					adminpassword=ldapProperties
							.getProperty(CommonConstants.Password);
				}
				String port = ldapProperties
						.getProperty(CommonConstants.LDAP_Port);
				System.out.println("ENABLE_SSL:"+ldapProperties.getProperty(CommonConstants.ENABLE_SSL));
				if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{

					if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,adminuser,adminpassword,ldapProperties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),ldapProperties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),ldapProperties
								.getProperty(CommonConstants.LDAP_Domain));
						System.out.println("context returned :"+ ctx);
					}
					else if(ldapProperties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						ctx = getInitialContext(hostName,port,adminuser,adminpassword,ldapProperties
								.getProperty(CommonConstants.LDAP_Domain));
						System.out.println("context returned :"+ ctx);
					}
					
					String returnedAtts[] = { "memberOf" };
					String ldapUserFilter = ldapProperties
							.getProperty(CommonConstants.LDAP_User_Filter);
					
					String searchFilter = "(&" + ldapUserFilter
							+ "(sAMAccountName="
							+ userId + "))";

					SearchControls searchCtls = new SearchControls();
					searchCtls.setReturningAttributes(returnedAtts);

					searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

					/*	NamingEnumeration<?> answer = ctx.search(ldapProperties
						.getProperty(CommonConstants.LDAP_Naming_Context),
						searchFilter, searchCtls);*/
					NamingEnumeration<?> answer = ctx.search(ldapProperties
							.getProperty(CommonConstants.LDAP_Naming_Context),
							searchFilter, searchCtls);

					int i=0;
					while (answer.hasMoreElements()) {
						SearchResult sr = (SearchResult) answer.next();
						Attributes attrs = sr.getAttributes();
						i++;
						System.out.print("no of elements::"+i);
						if (attrs != null) {
							NamingEnumeration<?> ne = attrs.getAll();

							while (ne.hasMore()) {

								Attribute attr = (Attribute) ne.next();
								System.out.println("LDAPUTILS authentication>>"+attr.get().toString());
								String tempSplitArr[] = attr.get().toString()
										.split(",");

								// We just need group, now we get CN=CSR UG1 for
								// example.
								String grpTemp = tempSplitArr[0];



								String tempArr[] = grpTemp.split("=");
								userGroup = tempArr[1];

outList.add(userGroup);
								System.out.println("user belongs to " + grpTemp);
							}

						}
						/*if(userGroup != null || !userGroup.equals(""))
						{
							break;
						}*/
					}
					System.out.println("Out of outer while loop");
					if(userGroup == null || userGroup.equals(""))
					{
						userGroup = "error";
					}
					ctx.close();
				}
				else
				{
					//userGroup = "No authentication Found";
					System.out.println("No authentication Found");
				}
			} catch (NamingException ne) {
				userGroup = "error";
				logger.error("[LDAPUtils][authenticateUser] Error authenticating user:"
						+ userId);
				logger.error(ne.getMessage());
				logger.error(ne.getStackTrace());
				logger.error(ne.getRootCause());
			}

		}

		// if no exception, the user is already authenticated.
		logger.info("[LDAPUtils][authenticateUser] OK, successfully authenticating user");
		System.out.println("above return stmt");
		return outList;
	}
	
	
	

	public static List<String> getGroupsOfUser(String propertiesFileNameWithPath1,String userName)
	{
		List<String> groups=new ArrayList<String>();
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);

		//System.out.println("getUsersBelongstoGroup" + Group);
		System.out.println("userId?????????"+userName);

		DirContext ctx = null;
		if (properties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out.println(properties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			try {
				String hostName = properties.getProperty(CommonConstants.LDAP_Host);
				String port = properties.getProperty(CommonConstants.LDAP_Port);
				String userId = properties
						.getProperty(CommonConstants.USER_NAME);
				String password = properties
						.getProperty(CommonConstants.Password);
				
				try {
					password=DataEncryptDecrypt.decryptData(password);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					password=properties
							.getProperty(CommonConstants.Password);
							
				}
				if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{
					if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
								.getProperty(CommonConstants.LDAP_User_DN));
					}
					else if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						ctx = getInitialContext(hostName,port,userId,password,properties
								.getProperty(CommonConstants.LDAP_Domain));
					}

				}


				String defaultSearchBase = properties
						.getProperty(CommonConstants.LDAP_Naming_Context);



				String              filter  = String.format(SEARCH_BY_SAM_ACCOUNT_NAME2, userName); 
				String ldapUserFilter = properties
						.getProperty(CommonConstants.LDAP_User_Filter);
				filter="(&" + ldapUserFilter
						+ filter+")";
				SearchControls      constraints = new SearchControls(); 
				constraints.setSearchScope(SearchControls.SUBTREE_SCOPE); 
				constraints.setReturningAttributes(attrIdsToSearch); 
				NamingEnumeration results = ctx.search(defaultSearchBase, filter,constraints); 
				// Fail if no entries found 
				if (results == null || !results.hasMore()) { 
					System.out.println("No result found"); 
					return groups; 
				} 

				// Get result for the first entry found 
				SearchResult result = (SearchResult) results.next(); 

				// Get the entry's distinguished name 
				NameParser parser = ctx.getNameParser(""); 
				Name contextName = parser.parse(ctx.getNameInNamespace()); 
				Name baseName = parser.parse(defaultSearchBase); 

				Name entryName = parser.parse(new CompositeName(result.getName()) 
				.get(0)); 
				
				

				// Get the entry's attributes 
				Attributes attrs = result.getAttributes(); 
				Attribute attr = attrs.get(attrIdsToSearch[0]); 

				NamingEnumeration e = attr.getAll(); 
				System.out.println("Member of"); 
				while (e.hasMore()) { 
					String value = (String) e.next(); 
					
					System.out.println("value is"+value);
					String groupcom[]=value.split(",");

					String tmp[]=groupcom[0].split("=");
					System.out.println(tmp[1]);
					groups.add(tmp[1]);
				} 
			}


			catch(Exception e)
			{
				e.printStackTrace();
			}

		}	
		return groups;
	}



	public static List<String> getUsersOfGroup2(String propertiesFileNameWithPath,String Group)
	{
		List<String> Users = new ArrayList<String>();
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);

		System.out.println("getUsersBelongstoGroup" + Group);
		// System.out.println("userId"+userId);
		//String domain = "TestAD.com";
		// String principalName = username + "@" + domain;
		// String password = "s1ng@pore";
		DirContext ctx = null;
		if (properties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out.println(properties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
		}
		try {
			String hostName = properties.getProperty(CommonConstants.LDAP_Host);
			String port = properties.getProperty(CommonConstants.LDAP_Port);
			String userId = properties
					.getProperty(CommonConstants.USER_NAME);
			String password = properties
					.getProperty(CommonConstants.Password);
			try {
				password=DataEncryptDecrypt.decryptData(password);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				password=properties
						.getProperty(CommonConstants.Password);
						
			}
			if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
			{
				if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
				{

					ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
							.getProperty(CommonConstants.LDAP_User_DN));
				}
				else if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
				{
					ctx = getInitialContext(hostName,port,userId,password,properties
							.getProperty(CommonConstants.LDAP_Domain));

					//ctx=getInitialContext(hostName,port,userId,password,"cn=Groups,dc=PBPOC,dc=COM");
				}
			}


			String defaultSearchBase = properties
					.getProperty(CommonConstants.LDAP_Naming_Context);

			SearchControls constraints = new SearchControls();  
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);  
			String[] attrIDs = { "member"};  
			constraints.setReturningAttributes(attrIDs);  
			//First input parameter is search bas, it can be "CN=Users,DC=YourDomain,DC=com"  
			//Second Attribute can be uid=username  
			NamingEnumeration<?> answer = ctx.search(properties
					.getProperty(CommonConstants.LDAP_Naming_Context), "(&(objectCategory=Group)(name="+Group+"))", constraints);  
			if (answer.hasMoreElements()) {  
				Attributes attrs = ((SearchResult) answer.next()).getAttributes();  
				//      System.out.println("distinguishedName "+ attrs.get("member"));  

				String str=attrs.get("member")==null?null:attrs.get("member").toString();
				if(str!=null)
				{
					String parsed[] =  str.split(":")[1].split(",");
					for(int i=0;i<parsed.length;i++)
					{
						String tmp=parsed[i].trim();
						String req[]=tmp.split("=");
						if(req[0].equalsIgnoreCase("CN") && !req[1].equalsIgnoreCase("Users"))
						{
							Users.add(req[1].trim());
						}

					}
				}
				// Users.add(attrs.get("member").toString());
				/*  System.out.println("givenname "+ attrs.get("givenname"));  
	                System.out.println("sn "+ attrs.get("sn"));  
	                System.out.println("mail "+ attrs.get("mail"));  
	                System.out.println("telephonenumber "+ attrs.get("telephonenumber")); 
	       String samaccntName= attrs.get("sAMAccountName").toString().split(":")[1].trim();
	                System.out.println(samaccntName);  */
			}else{  
				throw new Exception("Invalid User");  
			}  

		} catch (Exception ex) {  
			ex.printStackTrace();  

		}
		return Users;	

	}
	public static List<String> getUsersBelongstoGroup2(String Group)
	{
		List<String> Users = new ArrayList<String>();
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);

		System.out.println("getUsersBelongstoGroup" + Group);
		
		DirContext ctx = null;
		if (properties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out.println(properties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			try {
				String hostName = properties.getProperty(CommonConstants.LDAP_Host);
				String port = properties.getProperty(CommonConstants.LDAP_Port);
				String userId = properties
						.getProperty(CommonConstants.USER_NAME);
				String password = properties
						.getProperty(CommonConstants.Password);
				
				try {
					password=DataEncryptDecrypt.decryptData(password);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					password=properties
							.getProperty(CommonConstants.Password);
							
				}
				
				
				
				if(password==null || password.equalsIgnoreCase(""))
				{
					password=properties
							.getProperty(CommonConstants.Password);
				}
				if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{
					if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
								.getProperty(CommonConstants.LDAP_User_DN));
					}
					else if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						ctx = getInitialContext(hostName,port,userId,password,properties
								.getProperty(CommonConstants.LDAP_Domain));
					}
					String defaultSearchBase = properties
							.getProperty(CommonConstants.LDAP_Naming_Context);
					
					
					
					
					SearchControls searchCtrls = new SearchControls();
					searchCtrls.setSearchScope(SearchControls.SUBTREE_SCOPE);
					String[] attributes = {"member","memberof"};
					searchCtrls.setReturningAttributes(attributes);
		 
		                        //Change the NameOfGroup for the group name you would like to retrieve the members of.
					String filter = "(&(objectCategory=group)(name="+Group+"))";
		 
		                       //use the context we created above and the filter to return all members of a group.
					NamingEnumeration values = ctx.search( defaultSearchBase, filter, searchCtrls);
		 
					//Loop through the search results
					while (values.hasMoreElements()) {
						SearchResult sr = (SearchResult)values.next();
						System.out.println(">>>" + sr.getName());
						Attributes attrs = sr.getAttributes();
		 
						if (null != attrs)
						{
							for (NamingEnumeration ae = attrs.getAll(); ae.hasMoreElements();)
							{
								Attribute atr = (Attribute) ae.next();
								String attributeID = atr.getID();
										
								for(Enumeration vals=atr.getAll();vals.hasMoreElements();)
								{
									
									String username=((String)	vals.nextElement()).split("=")[1].split(",")[0];
									System.out.println("username in getUsersBelongstoGroup2 ------------->"+username);
									Users.add(username);
								}
									
							}
						}
						else {
							
						}
					}
					
					
				} 
				
				
				
			}
				catch (AuthenticationException e) {
					System.out.println(Group + " is NOT authenticated");
					return Users;
				} catch (NamingException e) {
					e.printStackTrace();
				}  catch (Exception e) {
					e.printStackTrace();
				}finally {

				}
		}
		
					
			return Users;		
	}
	

	public static List<String> getUsersBelongstoGroup(String propertiesFileNameWithPath,String Group) {
		// ,String userId, String password, String propertiesFileNameWithPath) {
		List<String> Users = new ArrayList<String>();
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);

		System.out.println("getUsersBelongstoGroup" + Group);
		// System.out.println("userId"+userId);
		//String domain = "TestAD.com";
		// String principalName = username + "@" + domain;
		// String password = "s1ng@pore";
		DirContext ctx = null;
		if (properties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out.println(properties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			try {
				String hostName = properties.getProperty(CommonConstants.LDAP_Host);
				String port = properties.getProperty(CommonConstants.LDAP_Port);
				String userId = properties
						.getProperty(CommonConstants.USER_NAME);
				String password = properties
						.getProperty(CommonConstants.Password);
				
				try {
					password=DataEncryptDecrypt.decryptData(password);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					password=properties
							.getProperty(CommonConstants.Password);
							
				}
				if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{
					if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
								.getProperty(CommonConstants.LDAP_User_DN));
					}
					else if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						ctx = getInitialContext(hostName,port,userId,password,properties
								.getProperty(CommonConstants.LDAP_Domain));
					}
					String defaultSearchBase = properties
							.getProperty(CommonConstants.LDAP_Naming_Context);
					// String groupDistinguishedName =
					// "DN=CN=DLS-APP-MyAdmin-C,OU=DLS File Permissions,DC=TestAD,DC=com";

					//ctx = new InitialDirContext(env);

					// userName is SAMAccountName
					SearchResult sr = executeSearchSingleResult(ctx,
							SearchControls.SUBTREE_SCOPE, defaultSearchBase,
							MessageFormat.format(SEARCH_BY_SAM_ACCOUNT_NAME,
									new Object[] { Group }), new String[] { CN,
						MEMBER });

					// String groupCN = getCN(groupDistinguishedName);
					// HashMap processedUserGroups = new HashMap();
					// HashMap unProcessedUserGroups = new HashMap();





					// Look for and process memberOf
					Attribute memberOf = sr.getAttributes().get(MEMBER);
					if (memberOf != null) {
						// System.out.println("all groups."+memberOf.getAll());
						for (Enumeration<?> e1 = memberOf.getAll(); e1
								.hasMoreElements();) {

							String unprocessedGroupDN = e1.nextElement().toString();
							// System.out.println(unprocessedGroupDN + " groups1.");
							String tempSplitArr[] = unprocessedGroupDN.toString()
									.split(",");





							// We just need group, now we get CN=CSR UG1 for
							// example.
							String grpTemp = tempSplitArr[0];

							String tempArr[] = grpTemp.split("=");
							//	System.out.println("xxx"+tempArr[1]+"ddddddddddd"+tempArr[3]);
							Users.add(tempArr[1]);
							/*
							 * String unprocessedGroupCN =
							 * getCN(unprocessedGroupDN); //
							 * System.out.println(unprocessedGroupDN + " groups2.");
							 * // Quick check for direct membership if (isSame
							 * (groupCN, unprocessedGroupCN) && isSame
							 * (groupDistinguishedName, unprocessedGroupDN)) {
							 * System.out.println(username + " is authorized.");
							 * return true; } else {
							 * unProcessedUserGroups.put(unprocessedGroupDN,
							 * unprocessedGroupCN); }
							 */
						}
						/*
						 * if (userMemberOf(ctx, defaultSearchBase,
						 * processedUserGroups, unProcessedUserGroups, groupCN,
						 * groupDistinguishedName)) { System.out.println(username +
						 * " is authorized."); return true; }
						 */
					}

					// System.out.println(username + " is NOT authorized.");
					if (ctx != null) {
						try {
							ctx.close();
						} catch (NamingException e) {

						}
					}
				}
				return Users;
			} catch (AuthenticationException e) {
				System.out.println(Group + " is NOT authenticated");
				return Users;
			} catch (NamingException e) {
				e.printStackTrace();
			}  catch (Exception e) {
				e.printStackTrace();
			}finally {

			}
		}
		return Users;
	}


	public static String getUserIDByDistinguishedName(String username,String propertiesFileNameWithPath ) {  

		System.out.println("username ----------> "+ username);  
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);

		String samAccountName="";

		if (properties.getProperty(CommonConstants.LDAP_Host) != null
				&& properties.getProperty(CommonConstants.LDAP_Port) != null) {
			System.out.println(properties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			DirContext ctx = null;
			try {
				String hostName = properties.getProperty(CommonConstants.LDAP_Host);
				String port = properties.getProperty(CommonConstants.LDAP_Port);
				String userId = properties
						.getProperty(CommonConstants.USER_NAME);
				String password = properties
						.getProperty(CommonConstants.Password);
				System.out.println(userId+"");
				
				try {
					password=DataEncryptDecrypt.decryptData(password);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					password=properties
							.getProperty(CommonConstants.Password);
							
				}
				if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{
					if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
								.getProperty(CommonConstants.LDAP_User_DN));
					}
					else if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						ctx = getInitialContext(hostName,port,userId,password,properties
								.getProperty(CommonConstants.LDAP_Domain));
					} }   

				SearchControls constraints = new SearchControls();  
				constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);  
				String[] attrIDs = { "distinguishedName",  
						"sn",  
						"givenname",  
						"mail",  
						"telephonenumber","sAMAccountName"};  
				constraints.setReturningAttributes(attrIDs);  
				//First input parameter is search bas, it can be "CN=Users,DC=YourDomain,DC=com"  
				//Second Attribute can be uid=username  
				String ldapUserFilter = properties
						.getProperty(CommonConstants.LDAP_User_Filter);
				System.out.println("Second Attribute in Search ------------> +(&" + ldapUserFilter
						+ "(cn=" + username + "))");  
				NamingEnumeration<?> answer = ctx.search(properties
						.getProperty(CommonConstants.LDAP_Naming_Context), "(&" + ldapUserFilter
								+ "(cn=" + username + "))"
						, constraints);  
				
				if (answer.hasMore()) {  
					Attributes attrs = ((SearchResult) answer.next()).getAttributes();  
					System.out.println("distinguishedName "+ attrs.get("distinguishedName"));  
					System.out.println("givenname "+ attrs.get("givenname"));  
					System.out.println("sn "+ attrs.get("sn"));  
					System.out.println("mail "+ attrs.get("mail"));  
					System.out.println("telephonenumber "+ attrs.get("telephonenumber")); 
					System.out.println("sAMAccountName before split::"+attrs.get("sAMAccountName").toString());
					String samName = attrs.get("sAMAccountName").toString();
					samAccountName= samName.substring(samName.lastIndexOf(':')+1,samName.length()).trim();
					System.out.println(samAccountName);  
				}else{  
					throw new Exception("Invalid User");  
				}
				
			} catch (Exception ex) {  
				System.out.println(" Error while querying ADAM");  
				ex.printStackTrace();  
			}  
			
		}  
		return samAccountName;
	}  



	private static SearchResult executeSearchSingleResult(DirContext ctx,
			int searchScope, String searchBase, String searchFilter,
			String[] attributes) throws NamingException {
		NamingEnumeration<?> result = executeSearch(ctx, searchScope, searchBase,
				searchFilter, attributes);

		SearchResult sr = null;
		// Loop through the search results
		while (result.hasMoreElements()) {

			sr = (SearchResult) result.next();

			//System.out.println("cccccccccccccccccccccccccc"+sr.);
			break;
		}
		return sr;
	}

	private static NamingEnumeration<?> executeSearch(DirContext ctx,
			int searchScope, String searchBase, String searchFilter,
			String[] attributes) throws NamingException {
		// Create the search controls
		SearchControls searchCtls = new SearchControls();

		// Specify the attributes to return
		if (attributes != null) {
			searchCtls.setReturningAttributes(attributes);
		}

		// Specify the search scope
		searchCtls.setSearchScope(searchScope);

		// Search for objects using the filter
		NamingEnumeration<?> result = ctx.search(searchBase, searchFilter,
				searchCtls);
		return result;
	}

	private static String getGroupDN(String name) {
		return new StringBuffer()
		.append("cn=")
		.append(name)
		.append(",")
		.append(GROUPS_OU)
		.toString();
	}

	private static String getUserUID(String userDN) 
	{
		int start = userDN.indexOf("=");
		int end = userDN.indexOf(",");

		if (end == -1) {
			end = userDN.length();
		}

		return userDN.substring(start+1, end);
	}	

	public static List<String> getMembers(String groupName,String propertiesFileNameWithPath) throws NamingException {
		List<String> members = new LinkedList<String>();

		// Set up attributes to search for
		String[] searchAttributes = new String[1];
		searchAttributes[0] = "uniqueMember";
		String propert = CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);

		String samaccntName="";

		// String domain = "TestAD.com";
		// Properties properties =
		// PropertyUtils.getProperties(propertiesFileNameWithPath);
		// String principalName = userId +"@"+domain;
		System.out.println(propertiesFileNameWithPath + "--userId:path" );
		// userId = properties.getProperty(CommonConstants.ADMINISTRATOR);

		//String principalName = userId + "@"
		//	+ properties.getProperty(CommonConstants.DOMAIN);
		// password = properties.getProperty(CommonConstants.CSR_ADMIN_PWD);
		if (properties.getProperty(CommonConstants.LDAP_Host) != null
				&& properties.getProperty(CommonConstants.LDAP_Port) != null) {
			System.out.println(properties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			DirContext ctx = null;
			try {
				String hostName = properties.getProperty(CommonConstants.LDAP_Host);
				String port = properties.getProperty(CommonConstants.LDAP_Port);
				String userId = properties
						.getProperty(CommonConstants.USER_NAME);
				String password = properties
						.getProperty(CommonConstants.Password);
				
				try {
					password=DataEncryptDecrypt.decryptData(password);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					password=properties
							.getProperty(CommonConstants.Password);
							
				}
				System.out.println(userId+"");
				if(properties.getProperty(CommonConstants.ENABLE_SSL) != null)
				{
					if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("T"))
					{

						ctx = getSecureInitialContext(hostName,port,userId,password,properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),properties.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),properties
								.getProperty(CommonConstants.LDAP_User_DN));
					}
					else if(properties.getProperty(CommonConstants.ENABLE_SSL).equalsIgnoreCase("F"))
					{
						ctx = getInitialContext(hostName,port,userId,password,properties
								.getProperty(CommonConstants.LDAP_Domain));
					} }   








				Attributes attributes =
						ctx.getAttributes(getGroupDN(groupName),
								searchAttributes);
				if (attributes != null) {
					Attribute memberAtts = attributes.get("uniqueMember");
					if (memberAtts != null) {
						for (NamingEnumeration vals = memberAtts.getAll();
								vals.hasMoreElements();
								members.add(
										getUserUID((String)vals.nextElement()))) ;
					}
				}


			}
			catch(Exception e)
			{

			}
		}
		return members;
	}



	


	/*public String getUserDnByUserId(String paramString)
		    throws LdapException
		  {
			String ldapFileName =  CommonConstants.LDAP_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(ldapFileName);
			String UserFilter = properties.getProperty(CommonConstants.LDAP_User_Filter);
			String User_Name_Attribute = properties.getProperty(CommonConstants.LDAP_User_Name_Attribute);
			String defaultSearchBase = properties.getProperty(CommonConstants.LDAP_Naming_Context);
		    String str = "(&" + UserFilter + "(" + User_Name_Attribute + "=" + paramString + "))";
		    try
		    {
		      NamingEnumeration localNamingEnumeration = search(defaultSearchBase, str, null, 2);

		      if (!localNamingEnumeration.hasMoreElements())
		      {
		        localObject = Localization.getMessage("error.noUserContextFound");
		        LdapException localLdapException2 = new LdapException((String)localObject);
		        localLdapException2.setErrorCode(85);
		        EventMonitoring.getInstance().logErrorEvent(paramString, (String)localObject, localLdapException2);
		        throw localLdapException2;
		      }

		      if ((localNamingEnumeration != null) && (localNamingEnumeration.hasMoreElements())) {
		    	  Object localObject = (SearchResult)localNamingEnumeration.nextElement();
		    	  return ((SearchResult)localObject).getNameInNamespace();
		      }
		    }
		    catch (LdapException localLdapException1)
		    {
		      throw localLdapException1;
		    }
		    catch (NamingException localNamingException)
		    {
		      Object localObject;
		      if (this.isEventLogging)
		      {
		        localObject = getNamingExceptionMessage("getUserDnByUserId", this.parameters.getNamingContext(), str, null, null);
		        EventMonitoring.getInstance().logErrorEvent("LDAPNamingException", (String)localObject, localNamingException);
		        throw new LdapException(Localization.getMessage("fail.call.ldap"));
		      }

		      throw new LdapException(localNamingException.getLocalizedMessage());
		    }

		    return null;
		  }*/


	static byte[] parseControls(Control[] controls) throws NamingException {
		byte[] cookie = null;
		if (controls != null) {
			for (int i = 0; i < controls.length; i++) {
				if (controls[i] instanceof PagedResultsResponseControl) {
					PagedResultsResponseControl prrc = (PagedResultsResponseControl)controls[i];
					cookie = prrc.getCookie();
				}
			}
		}

		return (cookie == null) ? new byte[0] : cookie;
	}

	public static String getDisplayNameOfUser(String userName) {
		System.out.println("Get displayName OF user" + userName);
	
		String displayName = "";
	
		String ldapFileName =  CommonConstants.LDAP_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(ldapFileName);
	
		// System.out.println("getUsersBelongstoGroup" + Group);
		// System.out.println("userId"+userId);
		// String domain = "TestAD.com";
		// String principalName = username + "@" + domain;
		// String password = "s1ng@pore";
		DirContext ctx = null;
		if (properties.getProperty(CommonConstants.LDAP_Host) != null) {
			System.out
			.println(properties
					.getProperty(CommonConstants.LDAP_Port)
					+ "--host:"
					+ properties
					.getProperty(CommonConstants.LDAP_Naming_Context));
			try {
				String hostName = properties
						.getProperty(CommonConstants.LDAP_Host);
				String port = properties
						.getProperty(CommonConstants.LDAP_Port);
				String userId = properties
						.getProperty(CommonConstants.USER_NAME);
				String password = properties
						.getProperty(CommonConstants.Password);
				
				try {
					password=DataEncryptDecrypt.decryptData(password);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					password=properties
							.getProperty(CommonConstants.Password);
							
				}
				if (properties.getProperty(CommonConstants.ENABLE_SSL) != null) {
					if (properties.getProperty(CommonConstants.ENABLE_SSL)
							.equalsIgnoreCase("T")) {
	
						ctx = getSecureInitialContext(
								hostName,
								port,
								userId,
								password,
								properties
								.getProperty(CommonConstants.LDAP_TRUST_STORE_PASSWORD),
								properties
								.getProperty(CommonConstants.LDAP_TRUST_STORE_PATH),
								properties
								.getProperty(CommonConstants.LDAP_User_DN));
					} else if (properties.getProperty(
							CommonConstants.ENABLE_SSL)
							.equalsIgnoreCase("F")) {
						ctx = getInitialContext(
								hostName,
								port,
								userId,
								password,
								properties
								.getProperty(CommonConstants.LDAP_Domain));
					}
	
				}
	
				String defaultSearchBase = properties
						.getProperty(CommonConstants.LDAP_Naming_Context);
				String[] attrIdsToSearch = new String[] { "displayName" };
	
				String filter = String.format(SEARCH_BY_SAM_ACCOUNT_NAME2,
						userName);
				SearchControls constraints = new SearchControls();
				constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
				constraints.setReturningAttributes(attrIdsToSearch);
				NamingEnumeration results = ctx.search(defaultSearchBase,
						filter, constraints);
				//System.out.println("Here1");
				// Fail if no entries found
				if (results == null || !results.hasMore()) {
					System.out.println("No result found");
					return displayName;
				}
	
				// Get result for the first entry found
				SearchResult result = (SearchResult) results.next();
				//System.out.println("Here2");
				// Get the entry's distinguished name
				/*
				 * NameParser parser = ctx.getNameParser(""); Name contextName =
				 * parser.parse(ctx.getNameInNamespace()); Name baseName =
				 * parser.parse(defaultSearchBase);
				 * 
				 * Name entryName = parser.parse(new
				 * CompositeName(result.getName()) .get(0));
				 */
				// Get the entry's attributes
				Attributes attrs = result.getAttributes();
				//System.out.println("Here3");
				Attribute attr = attrs.get(attrIdsToSearch[0]);
				//System.out.println("Here4");
				if (attr == null)
					return null;
				NamingEnumeration e = attr.getAll();
	
				//System.out.println("Here5");
				while (e.hasMore()) {
					Object o = e.next();
					if (o != null) {
						displayName = (String) o;
						System.out.println("Value is1" + displayName);
					}
				}
			}
	
			catch (Exception e) {
				e.printStackTrace();
			}
	
		}
		return displayName;
	}
	



}
