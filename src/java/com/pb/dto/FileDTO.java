package com.pb.dto;

public class FileDTO {

	private String fileName;
	private String fileDate;
	private String profile;
	private String resourseSet;
	private String unCompress;
	private String compress;
	private String pages;
	private float ratio;
	private float bytesPerpage;
	private String hashId;
	private String tableName;
	private String hostName;
	private String ipAddr;
	private String fileCreatedDate;

	public String getFileCreatedDate() {
		return this.fileCreatedDate;
	}

	public void setFileCreatedDate(String fileCreatedDate) {
		this.fileCreatedDate = fileCreatedDate;
	}

	public String getIpAddr() {
		return this.ipAddr;
	}

	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	public String getHostName() {
		return this.hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getHashId() {
		return this.hashId;
	}

	public void setHashId(String hashId) {
		this.hashId = hashId;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileDate() {
		return this.fileDate;
	}

	public void setFileDate(String fileDate) {
		this.fileDate = fileDate;
	}

	public String getProfile() {
		return this.profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getResourceSet() {
		return this.resourseSet;
	}

	public void setResourceSet(String resourceSet) {
		this.resourseSet = resourceSet;
	}

	public String getUnCompress() {
		return this.unCompress;
	}

	public void setUnCompress(String unCompress) {
		this.unCompress = unCompress;
	}

	public String getCompress() {
		return this.compress;
	}

	public void setCompress(String compress) {
		this.compress = compress;
	}

	public String getPages() {
		return this.pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	public float getRatio() {
		return this.ratio;
	}

	public void setRatio(float ratio) {
		this.ratio = ratio;
	}

	public float getBytesPerpage() {
		return this.bytesPerpage;
	}

	public void setBytesPerpage(float bytesPerpage) {
		this.bytesPerpage = bytesPerpage;
	}
}
