package com.pb.dto;

public class UserDTO {
	private Long dbId;
	private String dbName;
	private String dbProfileName;
	private String dbDesc;
	private Long roleId;
	private String active;
	private String validdateFrom;
	private String validdateTo;
	private String validfromDays;
	private String validtoDays;
	private int currYear;
	private String accessdateFrom;
	private String accessdateTo;
	private int accessfromDays;
	private int accesstoDays;
	private String weekend;
	private String userGrp;
	private Long userId;
	private String tableName;
	private String roleName;
	private String roleDesc;
	private String newHashId;
	private String oldHashId;
	private String hashCheckDate;
	private String hashCheckTime;
	private String hashCheckFlag;
	private String accessfromDay;
	private String accesstoDay;
	private String actionName;
	private Long actionId;
	private String csrUsers;
	private String groupDesc;
	private String userName;
	private Long groupId;
	private Long[] seluserId;
	private Long indexId;
	private String indexName;
	private Long userIndexId;
	private String indexResults;
	private String indexDesc;
	private Long serResIndexId;
    private String userProfileName;
    private Long userProfileId;
    private String dataType;
    private String width;
    private String is_nullable;
    private boolean isProfileAdmin;
    
    private Long profileId;
    private String profileName;
    private String profileDb;
    private String profileAdmin;
    private String profileGroup;
    private String profileUsers;
    
    /*Nithu Alexander: 22/09/2014
     * Adding parameter for UserDisplayName.
     * This is part of the requirement to display the FullName of every UserId
     * in VaultApp Admin Section*/
    private String userDisplayName;
    
    
    public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}
	
	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	
	public String getProfileDb() {
		return profileDb;
	}

	public void setProfileDb(String profileDb) {
		this.profileDb = profileDb;
	}
	
	public String getProfileAdmin() {
		return profileAdmin;
	}

	public void setProfileAdmin(String profileAdmin) {
		this.profileAdmin = profileAdmin;
	}
	
	public String getProfileUsers() {
		return profileUsers;
	}

	public void setProfileUsers(String profileUsers) {
		this.profileUsers = profileUsers;
	}    
    
    public boolean getIsProfileAdmin() {
		return isProfileAdmin;
	}

	public void setIsProfileAdmin(boolean isProfileAdmin) {
		this.isProfileAdmin = isProfileAdmin;
	}
    
    public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
    
    public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}
    
    public String getIs_nullable() {
		return is_nullable;
	}

	public void setIs_nullable(String is_nullable) {
		this.is_nullable = is_nullable;
	}
    
	public String getProfileGroup() {
		return profileGroup;
	}

	public void setProfileGroup(String profileGroup) {
		this.profileGroup = profileGroup;
	}
	
	public Long getUserProfileId() {
		return userProfileId;
	}

	public void setUserProfileId(Long userProfileId) {
		this.userProfileId = userProfileId;
	}

	public String getUserProfileName() {
		return userProfileName;
	}

	public void setUserProfileName(String userProfileName) {
		this.userProfileName = userProfileName;
	}

	public Long getSerResIndexId() {
		return serResIndexId;
	}

	public void setSerResIndexId(Long serResIndexId) {
		this.serResIndexId = serResIndexId;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public Long getIndexId() {
		return indexId;
	}

	public void setIndexId(Long indexId) {
		this.indexId = indexId;
	}

	public Long[] getSeluserId() {
		return this.seluserId;
	}

	public void setSeluserId(Long[] seluserId) {
		this.seluserId = seluserId;
	}

	public Long getGroupId() {
		return this.groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGroupDesc() {
		return this.groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	public String getCsrUsers() {
		return this.csrUsers;
	}

	public void setCsrUsers(String csrUsers) {
		this.csrUsers = csrUsers;
	}

	public Long getActionId() {
		return this.actionId;
	}

	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}

	public String getActionName() {
		return this.actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getAccessfromDay() {
		return this.accessfromDay;
	}

	public void setAccessfromDay(String accessfromDay) {
		this.accessfromDay = accessfromDay;
	}

	public String getAccesstoDay() {
		return this.accesstoDay;
	}

	public void setAccesstoDay(String accesstoDay) {
		this.accesstoDay = accesstoDay;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getNewHashId() {
		return this.newHashId;
	}

	public void setNewHashId(String newHashId) {
		this.newHashId = newHashId;
	}

	public String getOldHashId() {
		return this.oldHashId;
	}

	public void setOldHashId(String oldHashId) {
		this.oldHashId = oldHashId;
	}

	public String getHashCheckDate() {
		return this.hashCheckDate;
	}

	public void setHashCheckDate(String hashCheckDate) {
		this.hashCheckDate = hashCheckDate;
	}

	public String getHashCheckTime() {
		return this.hashCheckTime;
	}

	public void setHashCheckTime(String hashCheckTime) {
		this.hashCheckTime = hashCheckTime;
	}

	public String getHashCheckFlag() {
		return this.hashCheckFlag;
	}

	public void setHashCheckFlag(String hashCheckFlag) {
		this.hashCheckFlag = hashCheckFlag;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return this.roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserGrp() {
		return this.userGrp;
	}

	public void setUserGrp(String userGrp) {
		this.userGrp = userGrp;
	}

	public String getWeekend() {
		return this.weekend;
	}

	public void setWeekend(String weekend) {
		this.weekend = weekend;
	}

	public String getAccessdateFrom() {
		return this.accessdateFrom;
	}

	public void setAccessdateFrom(String accessdateFrom) {
		this.accessdateFrom = accessdateFrom;
	}

	public String getAccessdateTo() {
		return accessdateTo;
	}

	public void setAccessdateTo(String accessdateTo) {
		this.accessdateTo = accessdateTo;
	}

	public int getAccesstoDays() {
		return this.accesstoDays;
	}

	public void setAccesstoDays(int accesstoDays) {
		this.accesstoDays = accesstoDays;
	}

	public int getAccessfromDays() {
		return this.accessfromDays;
	}

	public void setAccessfromDays(int accessfromDays) {
		this.accessfromDays = accessfromDays;
	}

	public int getCurrYear() {
		return this.currYear;
	}

	public void setCurrYear(int currYear) {
		this.currYear = currYear;
	}

	public String getValidfromDays() {
		return validfromDays;
	}

	public void setValidfromDays(String validfromDays) {
		this.validfromDays = validfromDays;
	}

	public String getValidtoDays() {
		return validtoDays;
	}

	public void setValidtoDays(String validtoDays) {
		this.validtoDays = validtoDays;
	}

	public String getValiddateFrom() {
		return validdateFrom;
	}

	public void setValiddateFrom(String validdateFrom) {
		this.validdateFrom = validdateFrom;
	}

	public String getValiddateTo() {
		return validdateTo;
	}

	public void setValiddateTo(String validdateTo) {
		this.validdateTo = validdateTo;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getDbId() {
		return this.dbId;
	}

	public void setDbId(Long dbId) {
		this.dbId = dbId;
	}

	public String getDbName() {
		return this.dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	
	public String getDbProfileName() {
		return this.dbProfileName;
	}

	public void setDbProfileName(String dbProfileName) {
		this.dbProfileName = dbProfileName;
	}

	public String getDbDesc() {
		return this.dbDesc;
	}

	public void setDbDesc(String dbDesc) {
		this.dbDesc = dbDesc;
	}

	public void setUserIndexId(Long userIndexId) {
		this.userIndexId = userIndexId;
	}

	public Long getUserIndexId() {
		return userIndexId;
	}

	public void setIndexResults(String indexResults) {
		this.indexResults = indexResults;
	}

	public String getIndexResults() {
		return indexResults;
	}

	public void setIndexDesc(String indexDesc) {
		this.indexDesc = indexDesc;
	}

	public String getIndexDesc() {
		return indexDesc;
	}
	/* Nithu Alexander: 22/09/2014
	 * Getter and Setter methods for userDisplayName
	 * This is part of the requirement to display fullName of a 
	 * user along with userId in VaultApp Admin section */
	
	public void setUserDisplayName(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}

	public String getUserDisplayName() {
		return userDisplayName;
	}
}
