package com.pb.dto;

public class LoginDTO {

	private String IPAddr;
	private String hostName;
	private String browserName;
	private String groupName;
	private String userName;
	private String flag;
	private String logged;
	private Long logId;
	private String logout;
	
	public void setIPAddr(String iPAddr) {
		IPAddr = iPAddr;
	}
	public String getIPAddr() {
		return IPAddr;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getHostName() {
		return hostName;
	}
	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}
	public String getBrowserName() {
		return browserName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserName() {
		return userName;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getFlag() {
		return flag;
	}
	public void setLogged(String logged) {
		this.logged = logged;
	}
	public String getLogged() {
		return logged;
	}
	public void setLogId(Long logId) {
		this.logId = logId;
	}
	public Long getLogId() {
		return logId;
	}
	public void setLogout(String logout) {
		this.logout = logout;
	}
	public String getLogout() {
		return logout;
	}
	
	
}
