package com.pb.dto;

import java.sql.Date;

public class IndexDTO {
	
	private String profileName;
private String account;
private String address;
private String name;
private Date invLink;


public String getProfileName() {
	return profileName;
}
public void setProfileName(String profileName) {
	this.profileName = profileName;
}
public String getAccount() {
	return account;
}
public void setAccount(String account) {
	this.account = account;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Date getInvLink() {
	return invLink;
}
public void setInvLink(Date invLink) {
	this.invLink = invLink;
}

}
