package com.pb.error;

//Import required java libraries
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;

import java.util.*;

//Extend HttpServlet class
public class ErrorHandler extends HttpServlet {

// Method to handle GET method request.
public void doGet(HttpServletRequest request,
                 HttpServletResponse response)
         throws ServletException, IOException
{
	//System.out.println("############### ERROR HANDLER IS CALLING ");
   // Analyze the servlet exception       
   Throwable throwable = (Throwable)
   request.getAttribute("javax.servlet.error.exception");
   Integer statusCode = (Integer)
   request.getAttribute("javax.servlet.error.status_code");
   String servletName = (String)
   request.getAttribute("javax.servlet.error.servlet_name");
   if (servletName == null){
      servletName = "Unknown";
   }
   String requestUri = (String)
   request.getAttribute("javax.servlet.error.request_uri");
   if (requestUri == null){
      requestUri = "Unknown";
   }

   // Set response content type
  

   if (throwable == null && statusCode == null){
	  // System.out.println("########### COMING TO IF");
       
   }else{
	   	String filePath = CommonConstants.CSR_PROPERTY_FILE_NAME;
		System.out.println("filepath"+filePath);
		Properties properties1 = PropertyUtils.getProperties(filePath);
		System.out.println("properties1");
		String host=properties1.getProperty("LOAD_BALANCER_HOST");
		String port=properties1.getProperty("LOAD_BALANCER_PORT");
		
		System.out.println("host : " +host);
		System.out.println("port : " +port);
	   
	   //System.out.println("########### COMING TO ELSE 2");
	   System.out.println("#####"+request.getServerName());
	   System.out.println("#####"+request.getServerPort());
	   System.out.println("#####"+request.getServletPath());
	   System.out.println("#########" + request.getContextPath());
	   System.out.println("@@@@@@@@@@@@@@@@@@@@"+request.getScheme());
	   System.out.println("@@@@@@@@@@@@@@@@@@ " + "https://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/ErrorHandlerAction.action");
	   response.sendRedirect(request.getScheme()+"://"+host+":"+port+request.getContextPath()+"/ErrorHandlerAction.action");
   }

   
}
// Method to handle POST method request.
public void doPost(HttpServletRequest request,
                  HttpServletResponse response)
   throws ServletException, IOException {
  doGet(request, response);
}
}