/*
 * Copyright:  Copyright (C) 1993-2010 Pitney Bowes Software Inc.
 *
 * $Id: AppServletContextListener.java 6184 2010-09-09 20:42:03Z ssahmed $
 *
 * AppServletContextListener.java
 */

package com.pb.evault;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Properties;
import java.util.Timer;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.g1.e2.vault.OutputFormat;
import com.pb.admin.LoadDataTask;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;

/**
 * Listener class for Servlet context lifecycle events. This class is used to:
 * 
 * <p>
 * (1) Initialize the Log4j logging system
 * </p>
 * <p>
 * (2) Get all configuration parameters and make them accessible to all other
 * components in this web application
 * </p>
 * 
 * New configuration (i.e. context) parameters should be read by this class.
 * 
 * @author ssahmed
 */
public class AppServletContextListener implements ServletContextListener {

	private static final boolean DEFAULT_FORCE_PDF = false;
	private static final int DEFAULT_MAX_RESULTS = 20;
	private static final OutputFormat DEFAULT_OUTPUT_FORMAT = OutputFormat.PNG;
	private static AppServletContextListener _instance;
	private static ServletContext context = null;

	public void contextInitialized(ServletContextEvent sce) {

		System.out.println("Context Initialized vault");
		// TODO Auto-generated method stub
		/*
		 * String filePath = CommonConstants.CSR_PROPERTY_FILE_NAME;
		 * System.out.println("filepath"+filePath); Properties properties1 =
		 * PropertyUtils.getProperties(filePath);
		 * System.out.println("properties1"); String
		 * schtime=properties1.getProperty("SCHEDULER_TIME");
		 * System.out.println("schtime"+schtime); if(schtime!=null) {int
		 * i=Integer.parseInt(schtime); System.out.println("i is"+i); Timer
		 * timer=new Timer();System.out.println("timer"); timer.schedule(new
		 * LoadDataTask(),i*60*1000,i*60*1000);
		 * System.out.println("after schedule");
		 * ServletActionContext.getContext(
		 * ).getApplication().put("timer",timer);
		 * System.out.println("end Context Initialized vault"); }
		 */

		ServletContext ctxt = sce.getServletContext();
		this.context = ctxt;
		_instance = this;
		Logger logger = Logger.getLogger(AppServletContextListener.class);
		logger.info("**** [AppServletContextListener] Context has been initialized");
		ServletContext context = sce.getServletContext();
		System.out.println("rootPath" + context.getRealPath("/"));
		String hostname = null;
		int port = (-1);
		String sslTruststorePath = null;
		char[] sslTruststorePassword = null;
		boolean forcePDF = false;
		int maxResults = (-1);
		OutputFormat outputFormat = null;
		String propert = CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);

		if (properties.getProperty(CommonConstants.RENDERING_SERVER_HOST) != null
				|| properties
						.getProperty(CommonConstants.RENDERING_SERVER_HOST) != ""){
			hostname = properties
					.getProperty(CommonConstants.RENDERING_SERVER_HOST);
		}
		else if (properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != null
				|| properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != ""){
			hostname = properties
					.getProperty(CommonConstants.RENDERING_SERVER_IP);
			}
		else if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null
				|| properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != ""){
			hostname = properties
					.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);
		}
		else if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_IP) != null
				|| properties.getProperty(CommonConstants.ARCHIVAL_SERVER_IP) != ""){
			hostname = properties
					.getProperty(CommonConstants.ARCHIVAL_SERVER_IP);
		}
		System.out.println(properties
				.getProperty(CommonConstants.RENDERING_SERVER_PORT));
		
		/*Changes done in order to check hostname shouldn't be empty.*/
		if(hostname == null)
		{
			System.out.println("ErrorInformation: Request you to please add Machine name otherwise PDF render will not work!!!");
			hostname = properties
					.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);
		}
		if (properties.getProperty(CommonConstants.RENDERING_SERVER_PORT) != null) {
			if (properties.getProperty(CommonConstants.RENDERING_SERVER_PORT) != "")
				port = Integer
						.valueOf(
								properties
										.getProperty(CommonConstants.RENDERING_SERVER_PORT))
						.intValue();
		} else {
			if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT) != null)
				if (properties
						.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT) != "")
					port = Integer
							.valueOf(
									properties
											.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT))
							.intValue();
		}
		// hostname =
		// PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.EVAULT_HOST);
		//vaibhav 
		//start scheduler on startup
		
		boolean startScheduler = false;
		String startSchedulerString = properties.getProperty(CommonConstants.ENABLE_VAULTAPP_SCHEDULER_STARTUP);
		startScheduler = "true".equalsIgnoreCase(startSchedulerString);
		if(startScheduler){
			LoadDataTask.startMonitor();
		}
		
		//start scheduler on startup
		// the ssltruststorepath parameter
		if (ctxt.getInitParameter("ssltruststorepath") != null) {
			sslTruststorePath = ctxt.getInitParameter("ssltruststorepath");
		} else {
			sslTruststorePath = null;
		}

		// the ssltruststorepassword parameter
		if (ctxt.getInitParameter("ssltruststorepassword") != null) {
			sslTruststorePassword = ctxt.getInitParameter(
					"ssltruststorepassword").toCharArray();
		} else {
			sslTruststorePassword = null;
		}

		// the forcepdf parameter
		if (ctxt.getInitParameter("forcepdf") != null) {
			forcePDF = Boolean.parseBoolean(ctxt.getInitParameter("forcepdf"));
		} else {
			forcePDF = DEFAULT_FORCE_PDF;
		}

		// the maxresults parameter
		if (ctxt.getInitParameter("maxresults") != null) {
			final String maxResultsParam = ctxt.getInitParameter("maxresults");
			try {
				maxResults = Integer.parseInt(maxResultsParam);
			} catch (NumberFormatException e) {
				maxResults = DEFAULT_MAX_RESULTS;
				logger.warn(String.format(
						"Invalid maxresults configuration parameter: [%s]",
						maxResultsParam));
			}
		} else {
			maxResults = DEFAULT_MAX_RESULTS;
		}

		// the (image) outputformat parameter
		if (ctxt.getInitParameter("outputformat") != null) {
			final String outputFormatParam = ctxt
					.getInitParameter("outputformat");
			outputFormat = OutputFormat.getOutputFormat(outputFormatParam);
			if (outputFormat == null) {

				outputFormat = DEFAULT_OUTPUT_FORMAT;
				logger.warn(String.format(
						"Invalid output format configuration parameter: [%s]",
						outputFormatParam));
			}
		} else {
			outputFormat = DEFAULT_OUTPUT_FORMAT;
		}

		final AppConfig appConfig = new AppConfig(hostname, port,
				sslTruststorePath, sslTruststorePassword, forcePDF, maxResults,
				outputFormat);

		// store this "bean" in a context attribute so that it's
		// accessible to all other components in this web app
		ctxt.setAttribute("appconfig", appConfig);

		// read the build+version info properties file
		Properties buildProps = new Properties();
		try {

			final AppVersion appVersion = new AppVersion("1.0", "poc",
					"everyday");

			// store this "bean" in a context attribute so that it's
			// accessible to all other components in this web app
			ctxt.setAttribute("appversion", appVersion);
		} catch (Exception e) {
			logger.error("Unable to load build/version info properties file", e);
		}

		logger.info("**** [AppServletContextListener] Exiting AppServletContextListener ");
	}

	public void contextDestroyed(ServletContextEvent sce) {
		ServletContext sc = sce.getServletContext();
		System.out.println("ServletContext Destroyed::" + sc);
		String host = "";
		try {
			LoadDataTask.stopMonitoring();
		}catch (Exception t) {
			System.out.println("excepion occured");
			t.printStackTrace();
		}

	}

	public static AppServletContextListener getInstance() {
		return _instance;
	}

	public static ServletContext getContext() {
		return context;
	}

}
