/*
 * Copyright:  Copyright (C) 1993-2010 Pitney Bowes Software Inc.
 *
 * $Id: RenderDocument.java 6100 2010-08-24 18:01:02Z ssahmed $
 *
 * RenderDocument.java
 */

package com.pb.evault;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.servlet.ServletException;
//Nithu testing download
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.g1.e2.vault.OutputFormat;
import com.g1.e2.vault.RenderOptions;
import com.g1.e2.vault.ResolutionWidth;
import com.g1.e2.vault.Rotation;
import com.g1.e2.vault.ServerErrorException;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultException;
import com.lowagie.tools.Executable;
import com.opensymphony.xwork2.ActionContext;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.ocbc.SearchAccount;
import com.pb.ocbc.SearchAccountNew;

/**
 * Servlet whose only purpose is to render a document (based on request
 * parameters and attributes) and write the resulting binary data to its
 * response/output stream.
 * 
 * @author ssahmed
 */
public class RenderDocument extends HttpServlet {

	SearchAccount sa = new SearchAccount();

	@Override
	public void init() throws ServletException {
		/*
		 * get the app configuration (parameters) once so that they do not have
		 * to be looked up on each invocation of service().
		 */
		appConfig = (AppConfig) getServletContext().getAttribute("appconfig");

		// setup the Mime Type mapping db
		mimeTypeMap = new HashMap<String, String>();
		mimeTypeMap.put(".pdf", "application/pdf");
		mimeTypeMap.put(".mht", "message/rfc822");
		mimeTypeMap.put(".mhtml", "message/rfc822");
		mimeTypeMap.put(".htm", "text/html");
		mimeTypeMap.put(".html", "text/html");
		mimeTypeMap.put(".bmp", "image/bmp");
		mimeTypeMap.put(".gif", "image/gif");
		mimeTypeMap.put(".jpg", "image/jpeg");
		mimeTypeMap.put(".jpeg", "image/jpeg");
		mimeTypeMap.put(".tif", "image/tiff");
		mimeTypeMap.put(".tiff", "image/tiff");
		mimeTypeMap.put(".txt", "text/plain");
		mimeTypeMap.put(".xml", "text/xml");
		System.out.println("Initing render.do");

	}

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		logger.debug(String.format("processRequest(): query string [%s]",request.getQueryString()));
		System.out.println("coming to RenderDocument::+processRequest");
		// the database containing the document
		HttpSession sessionku = request.getSession();
		
		final String dbName = sessionku.getValue("SELECT_DB").toString();
		if ((dbName == null) || (dbName.trim().length() == 0)) {
			throw new ServletException("Missing or invalid dbName request parameter");
		}
		// the account number corresponding to the document
		final String acctNumber = request.getParameter("acct");
		if ((acctNumber == null) || (acctNumber.trim().length() == 0)) {
			throw new ServletException("Missing or invalid acctNumber request parameter");
		}
		// the DocHandle "encapsulates" certain required attributes
		// of the document
		final String docHandleStr = request.getParameter("dh");

		System.out.println("Doc handle string at render is"+docHandleStr);
		if ((docHandleStr == null) || (docHandleStr.trim().length() == 0)) {
			throw new ServletException("Missing or invalid acctNumber request parameter");
		}

		// the page # in the document to render
		final int page = Integer.parseInt(request.getParameter("pg"));
		// the document's page count
		final int pageCount = Integer.parseInt(request.getParameter("pagecnt"));

		final DocumentHandle docHandle = new DocumentHandle(docHandleStr);
		logger.debug(String
					.format("Rendering Document with date [%s], file [%s], pointer [%s], page count [%d]",
						docHandle.getDate(), docHandle.getFile(),
						docHandle.getPointer(), pageCount));

		final String docFormat = request.getParameter("docformat");
		final String docType = request.getParameter("doctype");
		// request parameter indicating whether the document being rendered
		// supports rendering to an image output format (GIF or PNG) that can
		// be displayed inline in the browser.
		final boolean supportsImageOutput = Boolean.parseBoolean(request.getParameter("imageoutput"));
		// request parameter indicating whether the document being rendered
		// supports rendering to the PDF output format.
		final boolean supportsPdfOutput = Boolean.parseBoolean(request.getParameter("pdfoutput"));

		OutputFormat of = null;
		/*
		 * force rendering to PDF for documents that are to be displayed inline
		 * in the browser interface - but only when they support rendering to
		 * PDF format.
		 */
		if (appConfig.forcePDF() && supportsPdfOutput) {
			//System.out.println("format1");
			of = OutputFormat.PDF;
		} else if (supportsImageOutput) {
			//System.out.println("format2");
			of = appConfig.getOutputFormat();
		} else {
			if (docFormat.equals("collection")) {
				System.out.println("output format is ---> "+docFormat);

				of = OutputFormat.COLLECT;
			} else if (docFormat.equals("pdf")) {
				/*
				 * Not sure if native PDF format documents should be rendered to
				 * PDF (as supportsPdfOutput is set for them) or RAW. For now,
				 * let's just try and render them to PDF which seems to work.
				 */
				System.out.println("out put format is"+"pdf");
				of = OutputFormat.PDF;
			} else if (docFormat.equals("djdeline")) {
				/*
				 * Not sure if native PDF format documents should be rendered to
				 * PDF (as supportsPdfOutput is set for them) or RAW. For now,
				 * let's just try and render them to PDF which seems to work.
				 */
				of = OutputFormat.TEXT;
				System.out.println("out put format is"+"text");
			}
			else{
				System.out.println("out put format is"+"raw");
				of = OutputFormat.RAW;
			}
		}
		HashMap map = new HashMap();
		if (appConfig.forcePDF()) {
			logger.trace(String
					.format("docFormat [%s], docType [%s], forcePDF [%s], supportsPdfOutput [%s], of [%s]",
							docFormat, docType, appConfig.forcePDF(),
							supportsPdfOutput, of));

		} else {
			logger.trace(String
					.format("docFormat [%s], docType [%s], supportsImageOutput [%s], supportsPdfOutput [%s], of [%s]",
							docFormat, docType, supportsImageOutput,
							supportsPdfOutput, of));

		}

		Rotation rot = Rotation.NONE;
		if (of.supportsRotation() && (request.getParameter("rot") != null)) {
			final String rotValue = request.getParameter("rot");
			rot = Rotation.getByName(rotValue);
			if (rot == null) {
				rot = Rotation.NONE;
			}
		}

		int fromPage = page;
		
		int numPagesToRender = 1;
		boolean forceSaveAs = false;

		if (request.getParameter("of") != null) {
			final String outputFormat = request.getParameter("of");
			logger.trace(String.format("requested output format is [%s]",
					outputFormat));
			of = OutputFormat.getOutputFormat(outputFormat);
			map.put("docType", of);
			map.put("pageNo", page);

			if (of.supportsMultiplePages()) {
				// render pages 1-pageCount
				fromPage = 1;
				numPagesToRender = pageCount;
			}

			/*
			 * If rendering to any one of PDF, RAW, Text, or TIFF force the
			 * rendered output file to be "Saved As" a file on the client-side.
			 */
			if (of.equals(OutputFormat.PDF) || of.equals(OutputFormat.RAW)
					|| of.equals(OutputFormat.TEXT)
					|| of.equals(OutputFormat.TIFF)) {
				forceSaveAs = true;
			}
		}

		ResolutionWidth rw = ResolutionWidth.NONE;
		if (of.supportsResolution() && (request.getParameter("res") != null)) {
			final String resValue = request.getParameter("res");
			rw = ResolutionWidth.getByName(resValue);
			if (rw == null) {
				rw = ResolutionWidth.WIDTH_640;
			}
		}

		RenderOptions renderOptions = new RenderOptions();
		if (request.getParameter("par").equals("NoBG")) {
			renderOptions.setEnableBackground(false);
		}

		logger.trace(String
				.format("Rendering to output format [%s] with preferred mime type [%s]",
						of.toString(), of.getMimeType()));

		String contentType = null;
		if (of.equals(OutputFormat.COLLECT)
				&& ((docType != null) && (docType.trim().length() > 0))
				&& (mimeTypeMap.get(docType) != null)) {
			// special "hack" to detect MHT & PDF documents stored as
			// Collections
			logger.trace(String.format("Overriding mime type for output format [%s] to [%s] (docType is %s)",of.getName(), mimeTypeMap.get(docType), docType));
			contentType = mimeTypeMap.get(docType);

			System.out.println("Content type is"+contentType);

		} else {
			if (forceSaveAs) {
				
				// Set the "Content-Disposition" header correctly
				String outputFilename = getOutputFilename(dbName, acctNumber,docHandle.getDate(), of.getFileExtension());
				System.out.println("Download output fileName ------------>"+outputFilename);
				response.setHeader("Content-Disposition","attachment; filename=\"" + outputFilename + "\"");
				// this particular content type header value is used to force
				// browsers to pop up a "Save As ..." dialog on the client side.
				contentType = "application/x-download";
				System.out.println("Download logic");
			} else {
				// for all other types of documents, get the Mime type
				// value from the output format being rendered to
				contentType = of.getMimeType();
			}
		}

		/*
		 * TODO: Use the Content Disposition header as follows so that when the
		 * PDF (export/render) button is pressed on the toolbar a
		 * "File Save As..." dialog appears with a more meaningful PDF filename
		 * displayed in it other than "Render.do":
		 * 
		 * Content-Disposition: attachment; filename=<file name.ext>
		 */

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		VaultClient vaultClient = new VaultClient();
		try {
			String propert = CommonConstants.VAULT_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(propert);
			String host = "";
			int port = 0;
			if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null
					|| properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != "")
				host = properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);

			if (properties.getProperty(CommonConstants.RENDERING_SERVER_PORT) != null
					|| properties.getProperty(CommonConstants.RENDERING_SERVER_PORT) != "")
				port = Integer.parseInt(properties.getProperty(CommonConstants.RENDERING_SERVER_PORT));

			boolean isFailed = true;

			try {
				
			if (host != null)
				vaultClient.connect(host, port);
				isFailed = false;
			} catch (Exception e) {
				// TODO: handle exception

			}
			System.out.println("Pages to rotate::"+numPagesToRender);
			System.out.println("Content type"+contentType);
			System.out.println("rw::"+rw);
			System.out.println("rot::"+rot);
			System.out.println("Render Options::"+renderOptions);
			System.out.println("baos::"+baos);
			System.out.println("dbname"+dbName);
			System.out.println("acctnumber"+acctNumber);
			System.out.println("output format"+of.getName()+"=="+of.getValue());
			System.out.println("baos"+baos.size());
			System.out.println("date is"+docHandle.getDate());
			System.out.println("doc file is"+docHandle.getFile());


			vaultClient.renderDirect(dbName, acctNumber, docHandle.getDate(),
					docHandle.getFile(), docHandle.getPointer(), of, fromPage,
					numPagesToRender, rw, rot, renderOptions, baos);

			System.out.println("No errors");
			// if no errors occurred, write the rendering to the
			// response/output stream
			OutputStream output = null;
			try {
				String aksi = request.getParameter("akhmad") != null ? request.getParameter("akhmad") : "";
				if (!aksi.equals("")|| (of.equals(OutputFormat.PDF) && !aksi.equals(""))) {
					response.setContentType(contentType);
					String pathfile = "";
					if (docType.equals(".pdf") || of.equals(OutputFormat.PDF)) {
						pathfile = "C:\\document.pdf";
					} else {
						pathfile = "C:\\document" + docType;
					}

					File file = new File(pathfile);
					output = new FileOutputStream(file);
					output.write(baos.toByteArray());
					if (output != null) {
						output.close();
					}
					if (docType.equals(".pdf") || of.equals(OutputFormat.PDF)) {
						System.out.println("path file is"+pathfile);
						Executable ex = new Executable();
						ex.openDocument(pathfile);
						ex.printDocument(pathfile);
					} else if (docType.equals(".htm")) {
						String content = "", s;
						FileReader fr = new FileReader(pathfile);
						BufferedReader br = new BufferedReader(fr);
						while ((s = br.readLine()) != null) {
							content += s;
						}
						content = content.replace("</body>","<script>window.print();</script></body>");
						output = response.getOutputStream();
						output.write(content.getBytes());
						if (output != null) {
							output.close();
						}
					} else {
						System.out.println("Viewing document as other");
						response.setContentType(contentType);
						output = response.getOutputStream();
						output.write(baos.toByteArray());
						if (output != null) {
							output.close();
						}
					}
				} else {
					System.out.println("Content Type -----------> "+contentType);
					System.out.println("Output Stream size-----------> "+baos.size());
					response.setContentType(contentType);
					output = response.getOutputStream();
					output.write(baos.toByteArray());
					if (output != null) {
						output.close();
					}
					
				}
				if (aksi != null) {
					map.put("action", aksi);
				}
				if (request.getParameter("act") != null) {
					map.put("action", request.getParameter("act").toString());
				}
				map.put("acctNumber", acctNumber);
				if (aksi != null || request.getParameter("act") != null) {
					sa.saveDocRenderDetails(map, sessionku);
				}
			}

			finally {
				System.out.println("Rendered finallly");
			}
		}
		/*
		 * catch (GeneralSecurityException e) { throw new
		 * ServletException(GSE_ERROR_MSG, e); }
		 */
		catch (VaultException e) {
			e.printStackTrace();
			logger.error("Caught VaultException", e);
			throw new ServletException("Caught VaultException", e);
		} catch (ServerErrorException e) {
			e.printStackTrace();
			logger.error(
					String.format(
							"Caught ServerErrorException: errorCode [%s], errorMessage [%s]",
							e.getErrorCode(), e.getErrorMessage()), e);
			throw new ServletException("Caught ServerErrorException", e);
		}  catch (Exception e){
			e.printStackTrace();
		}finally {
			vaultClient.shutdown();
			baos.close();
		}
	}

	/**
	 * Construct a filename suitable for saving a Document's rendering output to
	 * 
	 * @param dbName
	 * @param acctNumber
	 * @param date
	 * @param fileExtension
	 * @return
	 */
	private String getOutputFilename(String dbName, String acctNumber,
			String date, String fileExtension) {
		date = date.replaceAll("/+", "_");
		date = date.replaceAll("\\s+", "_");
		date = date.replaceAll(":+", "_");
		return String.format("%s-%s-%s%s", dbName, acctNumber, date,
				fileExtension);
	}

	// <editor-fold defaultstate="collapsed"
	// desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	private static final Logger logger = Logger.getLogger(RenderDocument.class);
	private static final String GSE_ERROR_MSG = "Security error occurred while trying to connect to e2 Vault server via SSL";
	private static AppConfig appConfig = null;
	private static Map<String, String> mimeTypeMap;

}
