/*
 * Copyright:  Copyright (C) 1993-2010 Pitney Bowes Software Inc.
 *
 * $Id: DocumentHandle.java 5626 2010-06-01 19:34:01Z ssahmed $
 *
 * DocumentHandle.java
 */

package com.pb.evault;

import java.io.Serializable;

import com.g1.e2.vault.VaultClient.Document;

/**
 * A JavaBean that is used to encapsulate the minimum amount of information
 * about a Document that is required and necessary in order to render a page
 * from it.
 * 
 * @author ssahmed
 */
public class DocumentHandle implements Serializable {

	public DocumentHandle(String data) {
		String[] fields = data.split(SEPARATOR_REGEX);
		if (fields.length != 4) {
			throw new IllegalArgumentException("Invalid flattened doc handle: "
					+ data);
		}

		update(fields[0], fields[1], fields[2], fields[3]);
	}

	public DocumentHandle(Document document) {
		// for non-Collection format documents, set the type to the document's
		// format;
		// but for Collection format documents, set the type to the document's
		// type which
		// is the file extension for the underlying document.
		// This is for behavioural compatibility with the Perl web client!
		this(document.getDate(), (document.getFormat().equalsIgnoreCase(
				"Collection") ? document.getType() : document.getFormat()),
				document.getFile(), document.getPointer());
	}

	DocumentHandle(String date, String type, String file, String pointer) {
		update(date, type, file, pointer);
	}

	public DocumentHandle() {
	}

	public String getDate() {
		return date;
	}

	public void setDate(String newDate) {
		this.date = newDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String newType) {
		this.type = newType;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String newFile) {
		this.file = newFile;
	}

	public String getPointer() {
		return pointer;
	}

	public void setPointer(String newPointer) {
		this.pointer = newPointer;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DocumentHandle)) {
			return false;
		}

		if (obj.getClass() != this.getClass()) {
			return false;
		}

		final DocumentHandle other = (DocumentHandle) obj;

		final boolean result = this.getDate().equals(other.getDate())
				&& this.getType().equals(other.getType())
				&& this.getFile().equals(other.getFile())
				&& this.getPointer().equals(other.getPointer());

		return result;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 37 * hash + (this.date != null ? this.date.hashCode() : 0);
		hash = 37 * hash + (this.type != null ? this.type.hashCode() : 0);
		hash = 37 * hash + (this.file != null ? this.file.hashCode() : 0);
		hash = 37 * hash + (this.pointer != null ? this.pointer.hashCode() : 0);
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(getDate());
		sb.append(SEPARATOR);
		sb.append(getType());
		sb.append(SEPARATOR);
		sb.append(getFile());
		sb.append(SEPARATOR);
		sb.append(getPointer());

		return sb.toString();
	}

	private void update(String date, String type, String file, String pointer) {
		if (date == null) {
			throw new IllegalArgumentException("date is null");
		}
		if (date.trim().length() == 0) {
			throw new IllegalArgumentException("date is empty");
		}
		if (type == null) {
			throw new IllegalArgumentException("type is null");
		}
		/* note that the type can be empty */
		if (file == null) {
			throw new IllegalArgumentException("file is null");
		}
		if (file.trim().length() == 0) {
			throw new IllegalArgumentException("file is empty");
		}
		if (pointer == null) {
			throw new IllegalArgumentException("pointer is null");
		}
		if (pointer.trim().length() == 0) {
			throw new IllegalArgumentException("pointer is empty");
		}

		if (date.indexOf(SEPARATOR) >= 0) {
			throw new IllegalArgumentException(
					"date contains illegal character: " + date);
		}
		if (type.indexOf(SEPARATOR) >= 0) {
			throw new IllegalArgumentException(
					"type contains illegal character: " + type);
		}
		if (file.indexOf(SEPARATOR) >= 0) {
			throw new IllegalArgumentException(
					"file contains illegal character: " + file);
		}
		if (pointer.indexOf(SEPARATOR) >= 0) {
			throw new IllegalArgumentException(
					"pointer contains illegal character: " + pointer);
		}

		// if (date.indexOf("/") >= 0) {
		// hack to support rendering of MHT (collection) documents
		// remove the "/" character as certain Windows OSes don't
		// like it when a webapp is serving data from a URL
		// containing "%2F" chars
		// this.date = date.replaceAll("/", "");
		// }
		// else {
		this.date = date;
		// }
		this.type = type;
		this.file = file;
		this.pointer = pointer;
	}

	private String date;
	private String type;
	private String file;
	private String pointer;

	private static final String SEPARATOR = "|";
	private static final String SEPARATOR_REGEX = "\\" + SEPARATOR;

}
