package com.pb.evault;

import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.ServerErrorException;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Database;
import com.g1.e2.vault.VaultException;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.login.Login;

public class VaultUtils {

	static final Logger logger = Logger.getLogger(VaultUtils.class);

	public static Set<SearchIndex> getSearchIndexes(String databaseStr) {

		logger.info("The value of the databaseStr is " + databaseStr);

		Set<SearchIndex> searchIndexes = new HashSet<SearchIndex>();
		Set<SearchIndex> tempSearchIndexes = null;
		VaultClient client = new VaultClient();
		Properties properties = PropertyUtils
				.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);
		String[] ignoreSearchIndexes = properties.getProperty(
				CommonConstants.IGNORE_SEARCHINDEX).split(",");

		logger.info("The value of the ignoreSearchIndexes is "
				+ ignoreSearchIndexes.toString());

		try {
			Properties propStatus = PropertyUtils.getProperties(PropertyUtils
					.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME)
					.getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
			int totalHost = Integer.parseInt(properties
					.getProperty(CommonConstants.TOTAL_EVAULT_HOST));
			logger.info("The value of the totalHost is " + totalHost);
			int hostNow = 0;
			boolean[] hosts = new boolean[totalHost];
			for (int i = 0; i < hosts.length; i++)
				hosts[i] = false;
			boolean isFailed = true;
			do {
				try {
					Random random = new Random();
					hostNow = random.nextInt(totalHost);
					if (!hosts[hostNow]) {
						logger.info("Before connecting to vault");
						client.connect(
								properties
										.getProperty(CommonConstants.EVAULT_HOST
												+ hostNow),
								Integer.valueOf(
										properties
												.getProperty(CommonConstants.EVAULT_PORT
														+ hostNow)).intValue());
						logger.info("after connecting to vault");
					}
					isFailed = false;
				} catch (Exception e) {
					// TODO: handle exception
					hosts[hostNow] = true;
					logger.info("inside the excption block **********");
					System.out
							.println("In the generic exception blcok while establishing connection.");
					e.printStackTrace();
				}
			} while (isFailed);

			logger.info("isFailed is" + isFailed);
			Database database = client.getDatabase(databaseStr);
			logger.info("after getting the database");
			tempSearchIndexes = database.getSearchIndexes();
			Iterator<SearchIndex> iterator = tempSearchIndexes.iterator();
			while (iterator.hasNext()) {
				logger.info("Insid the while loop to iterate");
				SearchIndex index = iterator.next();
				System.out
						.println("name:" + index.getName() + ",field:"
								+ index.getFields() + ",desc:"
								+ index.getDescription());
				boolean isIgnoreFound = false;
				for (String ignore : ignoreSearchIndexes) {
					if (index.getDescription().toUpperCase()
							.contains(ignore.trim().toUpperCase())) {
						isIgnoreFound = true;
					}
				}
				if (!isIgnoreFound) {
					searchIndexes.add(index);
				}
			}
		} catch (VaultException e) {
			System.err.println("ERROR: unknown host specified.");
			e.printStackTrace(System.err);
			System.exit(1);
		} catch (ServerErrorException e) {
			System.err.println("ERROR: unknown host specified.");
			e.printStackTrace(System.err);
			System.exit(1);
		} catch (Exception e) {
			logger.info("Inside the generic catch block");
			e.printStackTrace();
			logger.info("The exception message is " + e.getMessage());
			System.out
					.println("In the generic exception blcok while getting the indexes.");
		}

		/*
		 * catch (UnknownHostException e) {
		 * System.err.println("ERROR: unknown host specified.");
		 * e.printStackTrace(System.err); System.exit(1); } catch
		 * (ConnectException e) { System.err .println(
		 * "ERROR: Connection refused; server doesn't appear to be listening on the remote address/port."
		 * ); e.printStackTrace(System.err); System.exit(1); } catch
		 * (IOException e) { System.err.println("ERROR: I/O error occurred.");
		 * e.printStackTrace(System.err); System.exit(1); }
		 */

		return searchIndexes;
	}

}
