/*
 * Copyright:  Copyright (C) 1993-2010 Pitney Bowes Software Inc.
 *
 * $Id: AppVersion.java 6199 2010-09-13 18:30:37Z ssahmed $
 *
 * AppVersion.java
 */

package com.pb.evault;

/**
 * Bean class to encapsulate the application's build/version info.
 * 
 * @author ssahmed
 */
public final class AppVersion {

	AppVersion(String version, String builtBy, String builtOn) {
		if (version == null)
			throw new IllegalArgumentException("version is null");
		if (version.length() == 0)
			throw new IllegalArgumentException("version is empty");
		if (builtBy == null)
			throw new IllegalArgumentException("builtBy is null");
		if (builtBy.length() == 0)
			throw new IllegalArgumentException("builtBy is empty");
		if (builtOn == null)
			throw new IllegalArgumentException("builtOn is null");
		if (builtOn.length() == 0)
			throw new IllegalArgumentException("builtOn is empty");

		this.version = version;
		this.builtBy = builtBy;
		this.builtOn = builtOn;
	}

	/**
	 * Get the version of this app.
	 * 
	 * @return the version of this app
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Get the name of the person who built this app.
	 * 
	 * @return the name of the person who built this app.
	 */
	public String getBuiltBy() {
		return builtBy;
	}

	/**
	 * Get the date & time when this app was built.
	 * 
	 * @return the date & time when this app was built.
	 */
	public String getBuiltOn() {
		return builtOn;
	}

	private final String version;
	private final String builtBy;
	private final String builtOn;

}
