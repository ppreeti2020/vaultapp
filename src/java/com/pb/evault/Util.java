/*
 * Copyright:  Copyright (C) 1993-2010 Pitney Bowes Software Inc.
 *
 * $Id: Util.java 6100 2010-08-24 18:01:02Z ssahmed $
 *
 * Util.java
 */

package com.pb.evault;

import java.io.IOException;
import java.security.GeneralSecurityException;

import com.g1.e2.vault.VaultClient;

/**
 * 
 * @author ssahmed
 */
public final class Util {

	/**
	 * Connect to the e2 Vault server using the supplied configuration
	 * parameters
	 * 
	 * @param vc
	 * @param appConfig
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	static void connectToVault(VaultClient vc, AppConfig appConfig)
			throws IOException, GeneralSecurityException {
		// If the SSL truststore path and password are specified, then
		// connect to the e2 Vault server using SSL...
		if ((appConfig.getSSLTruststorePath() != null)
				&& (appConfig.getSSLTruststorePassword() != null)) {
			vc.connectSSL(appConfig.getHostname(), appConfig.getPort(),
					appConfig.getSSLTruststorePath(),
					appConfig.getSSLTruststorePassword());
			
		} else {
			// otherwise connect normally (i.e. sans SSL)
			vc.connect(appConfig.getHostname(), appConfig.getPort());
		}
	}

}
