/*
 * Copyright:  Copyright (C) 1993-2010 Pitney Bowes Software Inc.
 *
 * $Id: AppConfig.java 6100 2010-08-24 18:01:02Z ssahmed $
 *
 * AppConfig.java
 */

package com.pb.evault;

import java.util.Arrays;

import com.g1.e2.vault.OutputFormat;

/**
 * An immutable bean-like class that holds all the configuration parameters for
 * this web application (WAR):
 * 
 * <p>
 * <b>hostname</b>: the hostname for the e2 Rendering engine to connect to
 * </p>
 * <p>
 * <b>port</b>: the port # for the e2 Rendering engine to connect to
 * </p>
 * <p>
 * <b>forcePDF</b>: if set, render all documents to PDF format
 * </p>
 * <p>
 * <b>maxResults</b>: the maximum number of search results to display
 * </p>
 * <p>
 * <b>outputFormat</b>: the output format to which Documents are rendered to for
 * display in the browser interface
 * </p>
 * 
 * Add new configuration parameters to this class so that they are accessible by
 * other components in this web app.
 * 
 * @author ssahmed
 */
public final class AppConfig {

	AppConfig(String hostname, int port, String sslTruststorePath,
			char[] sslTruststorePassword, boolean forcePDF, int maxResults,
			OutputFormat outputFormat) {
		this.hostname = hostname;
		this.port = port;
		this.sslTruststorePath = sslTruststorePath;
		this.sslTruststorePassword = sslTruststorePassword;
		this.forcePDF = forcePDF;
		this.maxResults = maxResults;
		this.outputFormat = outputFormat;
	}

	/**
	 * Get the hostname parameter.
	 * 
	 * @return the hostname parameter
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * Get the port parameter.
	 * 
	 * @return the port parameter
	 */
	public int getPort() {
		return port;
	}

	/*
	 * Get the path to the SSL truststore to use when securely connecting (i.e.
	 * using SSL) to the e2 Vault server.
	 */
	public String getSSLTruststorePath() {
		return sslTruststorePath;
	}

	/*
	 * Get the password for the SSL truststore to use when securely connecting
	 * (i.e. using SSL) to the e2 Vault server.
	 */
	public char[] getSSLTruststorePassword() {
		return sslTruststorePassword;
	}

	/**
	 * Get the boolean valued forcePDF parameter.
	 * 
	 * @return the boolean valued forcePDF parameter
	 */
	public boolean forcePDF() {
		return forcePDF;
	}

	/**
	 * Get the maxResults parameter.
	 * 
	 * @return the maxResults parameter
	 */
	public int getMaxResults() {
		return maxResults;
	}

	/**
	 * Get the outputFormat parameter.
	 * 
	 * @return the outputFormat parameter.
	 */
	public OutputFormat getOutputFormat() {
		return outputFormat;
	}

	@Override
	public String toString() {
		if ((getSSLTruststorePath() != null)
				&& (getSSLTruststorePassword() != null)) {
			char[] maskedPasswd = new char[getSSLTruststorePassword().length];
			Arrays.fill(maskedPasswd, '*');
			final String maskedPasswdStr = new String(maskedPasswd);
			return String
					.format("AppConfig [hostname=%s, port=%d, sslTruststorePath=%s, sslTruststorePassword=%s, forcePDF=%s, maxResults=%d, outputFormat=%s]",
							getHostname(), getPort(), getSSLTruststorePath(),
							maskedPasswdStr, forcePDF(), getMaxResults(),
							getOutputFormat().getName());
		} else {
			return String
					.format("AppConfig [hostname=%s, port=%d, forcePDF=%s, maxResults=%d, outputFormat=%s]",
							getHostname(), getPort(), forcePDF(),
							getMaxResults(), getOutputFormat().getName());
		}
	}

	private final String hostname;
	private final int port;
	private final String sslTruststorePath;
	private final char[] sslTruststorePassword;
	private final boolean forcePDF;
	private final int maxResults;
	private OutputFormat outputFormat;

}
