/*
 * Copyright:  Copyright (C) 1993-2010 Pitney Bowes Software Inc.
 *
 * $Id: Interface.java 6184 2010-09-09 20:42:03Z ssahmed $
 *
 * Interface.java
 */

package com.pb.evault;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

import com.pb.common.CommonConstants;
import com.pb.common.PerformanceLogger;
import com.pb.common.PropertyUtils;
import com.pb.dao.UserDAO;
import com.pb.ocbc.SearchAccount;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.g1.e2.vault.DocumentInfo;
import com.g1.e2.vault.DocumentSection;
import com.g1.e2.vault.OutputFormat;
import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.SearchMatches;
import com.g1.e2.vault.SearchMatchesIterator;
import com.g1.e2.vault.ServerErrorException;
import com.g1.e2.vault.ServerLoopingException;
import com.g1.e2.vault.TooManyResultsException;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Account;
import com.g1.e2.vault.VaultClient.Database;
import com.g1.e2.vault.VaultClient.Document;
import com.g1.e2.vault.VaultException;
import com.google.common.collect.Sets;
import com.opensymphony.xwork2.ActionContext;

/**
 * Primary / front controller Servlet for this web application.
 * 
 * @author ssahmed
 */
public class Interface extends HttpServlet {

	PerformanceLogger pl = new PerformanceLogger();
	SearchAccount sa = new SearchAccount();

	@Override
	public void init() throws ServletException {
		/*
		 * get the app configuration (parameters) once so that they do not have
		 * to be looked up on each invocation of service().
		 */
		System.out.println("Inside Interface init1::1"+appConfig);
		appConfig = (AppConfig) getServletContext().getAttribute("appconfig");
		/* and the app version info */
		//System.out.println("Inside Interface init::2");
		appVersion = (AppVersion) getServletContext()
				.getAttribute("appversion");
		//System.out.println("Inside Interface init::3");
		logger.info(String.format("e2 ServiceWeb version [%s], built-on [%s]",
				appVersion.getVersion(), appVersion.getBuiltOn()));
		logger.info("Using " + appConfig);

		final String service = appConfig.getHostname() + ":"
				+ appConfig.getPort();
		//System.out.println("Inside Interface init::4");
		VaultClient vc = new VaultClient();
		try {
			//System.out.println("Inside Interface init::5");
			Util.connectToVault(vc, appConfig);
			//System.out.println("Inside Interface init::6");
			if ((appConfig.getSSLTruststorePath() != null)
					&& (appConfig.getSSLTruststorePassword() != null)) {
				//System.out.println("Inside Interface init::7");
				logger.info(String.format(
						"SSL connection to e2 Vault using truststore [%s]",
						new File(appConfig.getSSLTruststorePath())
						.getAbsolutePath()));
			}
			logger.info(String.format("e2 Vault RenderAPI version is [%s]",
					vc.getVersion()));
			logger.info(String.format(
					"Using e2 Vault Rendering engine at [%s]", service));
		} catch (UnknownHostException e) {
			throw new ServletException(
					"Unknown hostname for e2 Vault Rendering engine: "
							+ appConfig.getHostname(), e);
		} catch (IOException e) {
			throw new ServletException(
					"Failed to connect to e2 Vault Rendering engine ("
							+ service + ")", e);
		} catch (GeneralSecurityException e) {
			throw new ServletException(GSE_ERROR_MSG, e);
		}finally {
			try {
				vc.shutdown();
			} catch (IOException e) {
				throw new ServletException(
						"Caught IOException while shutting down connection to e2 Vault: "
								+ e.getMessage());
			}
		}
	}

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		final String httpMethod = request.getMethod();
		UserDAO userDao = new UserDAO();
		System.out.println("coming to interface::+processRequest");
		logger.debug(String.format("processRequest(): HTTP method [%s], path info [%s], query string [%s]",httpMethod, request.getPathInfo(),request.getQueryString()));
		// Map sessionku = ActionContext.getContext().getSession();
		HttpSession sessionku = request.getSession();
		final String db = request.getParameter("db");//sessionku.getValue("SELECT_DB").toString();// PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.CSR_DATABASE);
		System.out.println("db is"+db);
		final String index = request.getParameter("index");
		System.out.println("index is"+index);
		final String acct = request.getParameter("acct");
		System.out.println("acct is"+acct);
		final String op = request.getParameter("op");
		System.out.println("op is"+op);
		
		final Long userId = Long.parseLong(sessionku.getValue("UserID").toString());
		System.out.println("user id is"+userId);
		List actionList = userDao.getUserActions(userId);
		request.setAttribute("actionList", actionList);
		logger.debug(String.format("processRequest(): db [%s], index [%s], acct [%s], op [%s]",db, index, acct, op));

		try {
			if ("POST".equals(httpMethod) && ((db != null) && (index != null))) {
				doSearch(db, index, request, response);
			} else if ((request.getParameter("cont") != null) && (db != null)
					&& (index != null)) {
				doSearch(db, index, request, response);
			} else {
				if ((acct != null) && (acct.trim().length() > 0)
						&& (db != null) && (db.trim().length() > 0)
						&& "view".equals(op)) {
					// Validate the page # (to render) parameter, if specified.
					// If not specified, then render the first page.
					//System.out.println("coming to this page+++++++++++");
					sa.updateActionDetails(op, sessionku);
					int pageNum = 1;
					final String page = request.getParameter("pg");
					if (page != null && !(page.trim().equals(""))) {
						try {
							pageNum = Integer.parseInt(page);

							// scrub page #
							if (pageNum <= 0) {
								pageNum = 1;
							}
						} catch (NumberFormatException e) {
							throw new ServletException("Invalid non-numeric page number: " + page,e);
						}
					}

					// If a page # to render has been specified, we cannot
					// validate here that it's <= the Document's page
					// count. That'll happen in the viewDocument() method.
					//viewDocument(db, acct, pageNum, request, response);
					viewDocument(db, acct,pageNum, request, response);
				//Nithu : Added below condition as part of testing for download function
				} else if((acct != null) && (acct.trim().length() > 0) && (db != null) && (db.trim().length() > 0) && "download".equals(op)){
					RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/test.jsp");
				}else {
					updateInterface(db, index, request, response);
				}
			}
		} catch (GeneralSecurityException e) {
			throw new ServletException(GSE_ERROR_MSG, e);
		}
	}

	protected void doSearch(String dbName, String indexName,
			HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException, GeneralSecurityException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		System.out.println("coming to interface::+doSearch");
		final String searchKey = request.getParameter("searchKey");
		final String cursorKey = request.getParameter("cont");

		
		if (cursorKey != null) {
			
			logger.info(String
					.format("Searching database [%s] using index [%s] for key [%s] continuing from [%s]",
							dbName, indexName, searchKey, cursorKey));
		} else {
			//System.out.println("2");
			logger.info(String.format(
					"Searching database [%s] using index [%s] for key [%s]",
					dbName, indexName, searchKey));
		}

		// the "selected" account number - really only useful when
		// searching over a Document index that requires an account
		// (e.g. InvLink)
		final String acctNumber = request.getParameter("acct");

		VaultClient vaultClient = new VaultClient();
		// vaultClient.connect(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.EVAULT_HOST),
		// Integer.valueOf(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.EVAULT_PORT)).intValue());
		String propert = CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		String host = "";
		int port = 0;
		if (properties.getProperty(CommonConstants.RENDERING_SERVER_HOST) != null
				|| properties
				.getProperty(CommonConstants.RENDERING_SERVER_HOST) != "")
			host = properties
			.getProperty(CommonConstants.RENDERING_SERVER_HOST);
		else if (properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != null
				|| properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != "")
			host = properties.getProperty(CommonConstants.RENDERING_SERVER_IP);
		else if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null
				|| properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != "")
			host = properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);
		else if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_IP) != null
				|| properties.getProperty(CommonConstants.ARCHIVAL_SERVER_IP) != "")
			host = properties.getProperty(CommonConstants.ARCHIVAL_SERVER_IP);

		if (properties.getProperty(CommonConstants.RENDERING_SERVER_PORT) != null
				|| properties
				.getProperty(CommonConstants.RENDERING_SERVER_PORT) != "")
			port = Integer.parseInt(properties
					.getProperty(CommonConstants.RENDERING_SERVER_PORT));
		else if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT) != null
				|| properties.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT) != "")
			port = Integer.parseInt(properties
					.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT));
		// Properties propStatus =
		// PropertyUtils.getProperties(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
		// int totalHost =
		// Integer.parseInt(properties.getProperty(CommonConstants.TOTAL_EVAULT_HOST));
		// int hostNow = 0;
		// boolean[] hosts = new boolean[totalHost];
		// for(int i=0;i<hosts.length;i++) hosts[i]=false;
		boolean isFailed = true;
		// do{
		try {
			// Random random = new Random();
			// hostNow = random.nextInt(totalHost);
			if (host != null)
				vaultClient.connect(host, port);
			isFailed = false;
		} catch (Exception e) {
			// TODO: handle exception
			// hosts[hostNow]=true;
		}
		// }while(isFailed);
		try {
			RequestDispatcher rd = null;

			Database db = vaultClient.getDatabase(dbName);

			// SearchIndex searchIndex =
			// db.findSearchIndexByName(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.SEARCH_INDEX_NAME));
			SearchIndex searchIndex = db.findSearchIndexByName(indexName);

			logger.info("****************** searchIndex isAccountIndex: "
					+ searchIndex.isAccountIndex());

			HttpSession session = request.getSession(false);
			String userGroup = session.getAttribute("LDAP_GROUP").toString();

			String permissionsFilePath = PropertyUtils.getProperties(
					CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
							CommonConstants.PERMISSION_FILE_PATH);
			Properties permissions = PropertyUtils
					.getProperties(permissionsFilePath);
			String permissionString = permissions.getProperty(userGroup);

			/*
			 * SearchMatches<Document> sm = null; List<Document> documents =
			 * null; sm = db.searchForDocumentsWithoutAccount(searchIndex,
			 * searchKey, cursorKey, 1000);
			 * System.out.println("max result:"+appConfig.getMaxResults());
			 * documents = sm.getMatches(); if (!documents.isEmpty()) { Document
			 * doc = documents.get(0); // XXXI: need to deal with the situation
			 * when no // custom columns have been configured for this //
			 * document search Set<String> columnNames =
			 * doc.getCustomFields().keySet();
			 * request.setAttribute("columnNames", columnNames); }
			 * request.setAttribute("documents", documents);
			 * request.setAttribute("hasMore", Boolean.valueOf(sm.hasMore()));
			 * 
			 * rd =
			 * request.getRequestDispatcher("/WEB-INF/DocumentSearchResults.jsp"
			 * );
			 */
			if (searchIndex.isAccountIndex()) {
				if (permissionString != null && !permissionString.equals("")) {

					List<Account> accounts = new ArrayList<VaultClient.Account>();
					SearchMatches<Account> sm = null;

					if (permissionString.indexOf(",") != -1) {
						String catArr[] = permissionString.split(",");

						/* for each category, get data and append to the list. */
						for (int i = 0; i < catArr.length; i++) {
							String category = catArr[i];
							/* firsr time, just get it */
							if (i == 0) {
								//System.out.println("t1");
								// sm = db.searchForAccounts(searchIndex,
								// category, cursorKey,
								// appConfig.getMaxResults());
								SearchMatchesIterator<Account> iterator = db
										.searchForAccounts(searchIndex, "");
								while (iterator.hasNext()) {

									//System.out.println("x1");
									List<Account> tempAct = iterator.next();
									//System.out.println("x2");
									for (Account acc : tempAct) {
										if (acc.getAccountNumber()
												.trim()
												.toUpperCase()
												.contains(
														acctNumber
														.toUpperCase()
														.trim()))
											accounts.add(acc);
										// System.out.println("x3"+acc.getAccountNumber());
									}
									// accounts.addAll(tempAct);
								}
								// accounts = sm.getMatches();
							} else {
								//System.out.println("t2");
								sm = db.searchForAccounts(searchIndex,
										category, cursorKey,
										appConfig.getMaxResults());
								accounts.addAll(sm.getMatches());
							}
							System.out.println("total:" + accounts.size());
						}

						/* Single category specified. Eg. GRP1=1 */
					} else {/*
					 * searching for documents without an account
					 * directly pass permissionString since its a single
					 * value.
					 */
						sm = db.searchForAccounts(searchIndex,
								permissionString, cursorKey,
								appConfig.getMaxResults());
						accounts = sm.getMatches();
						//System.out.println("t3");
					}
					String dict = indexName;
					List<Account> tempAcct = new ArrayList<VaultClient.Account>();
					if (!accounts.isEmpty()) {
						//System.out.println("t4");
						Account acct = accounts.get(0);

						/*
						 * XXXI: need to deal with the situation when no custom
						 * columns have been configured for this account search
						 */
						Set<String> columnNames = acct.getCustomFields()
								.keySet();
						int idx = 0;
						for (String col : columnNames) {
							if (idx == 0)
								dict = col;
							System.out.println("col:" + col);
							idx++;
						}
						for (Account d : accounts) {
							columnNames = d.getCustomFields().keySet();
							idx = 0;
							for (String col : columnNames) {

								if (idx == 0
										&& d.getCustomFields()
										.get(col)
										.toUpperCase()
										.contains(
												searchKey.toUpperCase())) {
									tempAcct.add(d);
								}
								idx++;
							}

						}
						accounts = tempAcct;
						request.setAttribute("columnNames", columnNames);
					}
					request.setAttribute("accNo", acctNumber);
					request.setAttribute("dict", dict);
					request.setAttribute("accounts", accounts);
					request.setAttribute("hasMore",
							Boolean.valueOf(sm.hasMore()));

					rd = request
							.getRequestDispatcher("/WEB-INF/AccountSearchResults.jsp");

				} else {
					rd = request
							.getRequestDispatcher("/WEB-INF/AccountSearchResults.jsp");
					request.setAttribute("NoPermissions", "NoPermissions");
				}
			} else {
				/*
				 * Searching without account number. searching using a document
				 * index
				 */
				logger.debug(String.format(
						"Searching using a Document index: %s", searchIndex));
				SearchMatches<Document> sm = null;
				List<Document> documents = null;
				if ((acctNumber != null) && (acctNumber.trim().length() > 0)
						&& searchIndex.isAccountRequired()) {
					/* searching for documents with (i.e. "under") an account */
					logger.info(String
							.format("Searching for Documents with Account # [%s], search key [%s], cursor key [%s]",
									acctNumber, searchKey, cursorKey));
					sm = db.searchForDocumentsWithAccount(searchIndex,
							acctNumber, searchKey, cursorKey,
							appConfig.getMaxResults());
				} else {
					/* searching for documents without an account */
					logger.info(String
							.format("Searching for Documents without Account: search key [%s], cursor key [%s]",
									searchKey, cursorKey));
					sm = db.searchForDocumentsWithoutAccount(searchIndex,
							searchKey, cursorKey, appConfig.getMaxResults());
				}

				documents = sm.getMatches();
				List<Document> tempDocs = new ArrayList<VaultClient.Document>();
				if (!documents.isEmpty()) {
					Document doc = documents.get(0);
					/*
					 * XXXI: need to deal with the situation when no custom
					 * columns have been configured for this document search
					 */
					Set<String> columnNames = doc.getCustomFields().keySet();
					for (String col : columnNames) {
						System.out.println("col:" + col);
					}
					request.setAttribute("columnNames", columnNames);
					for (Document d : documents) {
						boolean isFound = false;
						columnNames = d.getCustomFields().keySet();
						for (String col : columnNames) {

							if (doc.getCustomFields().get(col).toUpperCase()
									.contains(searchKey.toUpperCase())) {
								tempDocs.add(d);
							}
						}

					}
				}
				documents = tempDocs;
				request.setAttribute("documents", documents);
				request.setAttribute("hasMore", Boolean.valueOf(sm.hasMore()));

				rd = request
						.getRequestDispatcher("/WEB-INF/DocumentSearchResults.jsp");

			}

			/**
			 * if (searchIndex.isAccountIndex()) { // searching using an account
			 * index logger.info(String.format(
			 * "********* Searching using an Account index: %s", searchIndex));
			 * SearchMatches<Account> sm = db.searchForAccounts(searchIndex,
			 * searchKey, cursorKey, appConfig.getMaxResults());
			 * 
			 * List<Account> accounts = sm.getMatches();
			 * 
			 * if (!accounts.isEmpty()) { Account acct = accounts.get(0);
			 * 
			 * // XXXI: need to deal with the situation when no // custom
			 * columns have been configured for this // account search
			 * Set<String> columnNames = acct.getCustomFields().keySet();
			 * 
			 * request.setAttribute("columnNames", columnNames); }
			 * 
			 * logger.info("******************** accounts: " + accounts);
			 * request.setAttribute("accounts", accounts);
			 * request.setAttribute("hasMore", Boolean.valueOf(sm.hasMore()));
			 * 
			 * rd = request.getRequestDispatcher("/AccountSearchResults.jsp"); }
			 * else { // searching using a document index
			 * logger.debug(String.format
			 * ("Searching using a Document index: %s", searchIndex));
			 * SearchMatches<Document> sm = null; List<Document> documents =
			 * null;
			 * 
			 * if ((acctNumber != null) && (acctNumber.trim().length() > 0) &&
			 * searchIndex.isAccountRequired()) { // searching for documents
			 * with (i.e. "under") an account logger.info(String.format(
			 * "Searching for Documents with Account # [%s], search key [%s], cursor key [%s]"
			 * , acctNumber, searchKey, cursorKey)); sm =
			 * db.searchForDocumentsWithAccount(searchIndex, acctNumber,
			 * searchKey, cursorKey, appConfig.getMaxResults()); } else { //
			 * searching for documents without an account
			 * logger.info(String.format(
			 * "Searching for Documents without Account: search key [%s], cursor key [%s]"
			 * , searchKey, cursorKey)); sm =
			 * db.searchForDocumentsWithoutAccount(searchIndex, searchKey,
			 * cursorKey, appConfig.getMaxResults()); }
			 * 
			 * documents = sm.getMatches();
			 * 
			 * if (!documents.isEmpty()) { Document doc = documents.get(0);
			 * 
			 * // XXXI: need to deal with the situation when no // custom
			 * columns have been configured for this // document search
			 * Set<String> columnNames = doc.getCustomFields().keySet();
			 * 
			 * request.setAttribute("columnNames", columnNames); }
			 * 
			 * request.setAttribute("documents", documents);
			 * request.setAttribute("hasMore", Boolean.valueOf(sm.hasMore()));
			 * 
			 * rd = request.getRequestDispatcher("/DocumentSearchResults.jsp");
			 * }
			 **/

			rd.forward(request, response);
		} catch (VaultException e) {
			logger.error("Caught VaultException", e);
			throw new ServletException("Caught VaultException", e);
		} catch (ServerErrorException e) {
			logger.error(
					String.format(
							"Caught ServerErrorException: errorCode [%s], errorMessage [%s]",
							e.getErrorCode(), e.getErrorMessage()), e);
			throw new ServletException("Caught ServerErrorException", e);
		} finally {
			vaultClient.shutdown();
			out.close();
		}
	}

	protected void updateInterface(String dbName, String indexName,
			HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException, GeneralSecurityException {
		final String acctNum = request.getParameter("acct");
		System.out.println("coming to interface::+updateInterface");
		logger.trace(String.format(
				"updateInterface(): db [%s], index [%s], acct [%s]", dbName,
				indexName, acctNum));

		/* connect to the vault (Rendering) server and get the list of databases */
		VaultClient vaultClient = new VaultClient();
		// vaultClient.connect(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.EVAULT_HOST),
		// Integer.valueOf(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.EVAULT_PORT)).intValue());
		String propert = CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(propert);
		String host = "";
		int port = 0;
		if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null
				|| properties
				.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != "")
			host = properties
			.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);
		/*else if (properties.getProperty(CommonConstants.RENDERING_SERVER_IP) != null
				|| properties.getPoperty(CommonConstants.RENDERING_SERVER_IP) != "")
			host = properties.getProperty(CommonConstants.RENDERING_SERVER_IP);
		else if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null
				|| properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != "")
			host = properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);
		else if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_IP) != null
				|| properties.getProperty(CommonConstants.ARCHIVAL_SERVER_IP) != "")
			host = properties.getProperty(CommonConstants.ARCHIVAL_SERVER_IP);*/

		/*if (properties.getProperty(CommonConstants.RENDERING_SERVER_PORT) != null
				|| properties
						.getProperty(CommonConstants.RENDERING_SERVER_PORT) != "")
			port = Integer.parseInt(properties
					.getProperty(CommonConstants.RENDERING_SERVER_PORT));*/
		else if (properties.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT) != null
				|| properties.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT) != "")
			port = Integer.parseInt(properties
					.getProperty(CommonConstants.ARCHIVAL_SERVER_PORT));

		//host="SWECMAPOS0A";
		//port=16001;
		// Properties propStatus =
		// PropertyUtils.getProperties(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.STATUS_PROPERTIES_FILE_PATH));
		// int totalHost =
		// Integer.parseInt(properties.getProperty(CommonConstants.TOTAL_EVAULT_HOST));
		// int hostNow = 0;
		// boolean[] hosts = new boolean[totalHost];
		// for(int i=0;i<hosts.length;i++) hosts[i]=false;
		boolean isFailed = true;
		// do{
		try {
			// Random random = new Random();
			// hostNow = random.nextInt(totalHost);
			if (host != null)
				vaultClient.connect(host, port);
			isFailed = false;
		} catch (Exception e) {
			// TODO: handle exception
			// hosts[hostNow]=true;
		}
		// }while(isFailed);
		try {
			Set<Database> databases = vaultClient.getDatabases();
			Set<SearchIndex> searchIndexes = null;

			request.setAttribute("databases", databases);

			if ((dbName != null) && (indexName != null)) {
				// XXXI: remove this branch/block as this doesn't make any
				// sense!
				logger.debug(String.format(
						"***** Searching database [%s] using index [%s]",
						dbName, indexName));
				throw new ServletException("How the !@#$ did we get here??!!");
			} else {
				if (dbName == null) {
					Database[] dbsAsArray = new Database[databases.size()];
					dbsAsArray = databases.toArray(dbsAsArray);

					dbName = dbsAsArray[0].getName();
				}
				request.setAttribute("selectedDB", dbName);

				Set<SearchIndex> tempSearchIndexes = null;
				// get the search indexes for this selected database
				Database db = vaultClient.getDatabase(dbName);
				// searchIndexes = db.getSearchIndexes();
				tempSearchIndexes = db.getSearchIndexes();

				searchIndexes = new HashSet<SearchIndex>();
				String[] ignoreSearchIndexes = PropertyUtils
						.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME)
						.getProperty(CommonConstants.IGNORE_SEARCHINDEX)
						.split(",");
				Iterator<SearchIndex> iterator = tempSearchIndexes.iterator();
				while (iterator.hasNext()) {
					SearchIndex index = iterator.next();
					boolean isIgnoreFound = false;
					for (String ignore : ignoreSearchIndexes) {
						if (index.getDescription().toUpperCase()
								.contains(ignore.trim().toUpperCase())) {
							isIgnoreFound = true;
						}
					}
					if (!isIgnoreFound) {
						searchIndexes.add(index);
					}
				}
				// filter search indexes removing ones that are not
				// meant to be displayed
				Iterator<SearchIndex> iter = searchIndexes.iterator();
				while (iter.hasNext()) {
					SearchIndex si = iter.next();
					if (si.isInvisible()) {
						iter.remove();
					} else if (((acctNum == null) || (acctNum.trim().length() == 0))
							&& (si.isDocumentIndex() && si.isAccountRequired())) {
						// if an account hasn't been selected, remove
						// any document index that requires an account
						// to be selected
						iter.remove();
					}
				}

				request.setAttribute("searchIndexes", searchIndexes);

				RequestDispatcher rd = request
						.getRequestDispatcher("/WEB-INF/interface.jsp");
				rd.forward(request, response);
			}
		} catch (VaultException e) {
			logger.error("Caught VaultException", e);
			throw new ServletException("Caught VaultException", e);
		} catch (ServerErrorException e) {
			logger.error(
					String.format(
							"Caught ServerErrorException: errorCode [%s], errorMessage [%s]",
							e.getErrorCode(), e.getErrorMessage()), e);
			throw new ServletException("Caught ServerErrorException", e);
		} finally {
			vaultClient.shutdown();
		}
	}

	protected void viewDocument(String dbName, String acctNumber, int pageNum,
			HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException, GeneralSecurityException {
		
		/* Nithu Alexander: 01 Sep 2015
		 * Adding new request parameters (docPageCount,docFormat,outputFormats,imageOutput,pdfOutput,
		 * textOutput,tiffOutput,docInfoType) to Interface Servlet as part of Document View logic Re-structuring
		 * */
		final String selDocHandle = request.getParameter("selDocHandle");
		final String selDocFormat = request.getParameter("selDocFormat");
		final String selDocPageCount = request.getParameter("selDocPageCount");
		final String selDocOutputFrmts = request.getParameter("selDocOutputFrmts");
		final String selDocInfoType = request.getParameter("selDocInfoType");
		final String selImageOpFlag = request.getParameter("selImageOpFlag");
		final String selPdfOpFlag = request.getParameter("selPdfOpFlag");
		final String selTextOpFlag = request.getParameter("selTextOpFlag");
		final String selTiffOpFlag = request.getParameter("selTiffOpFlag");
		
		int selPageCount = 0;
		boolean isFailed = true;
		boolean multiplePages = false;
		
		System.out.println("coming to interface::"+selDocHandle);
		System.out.println("AccountNumber::"+acctNumber);
		System.out.println("database name::"+dbName);
		System.out.println("coming to interface::+viewDocument");
		pl.writeTime("document viewed by csr : " + dbName);
		DocumentHandle docHandle = null;
		
		Boolean b=(Boolean)	request.getSession().getAttribute("documentfound");
		System.out.println("Interface documentfound flag value is"+b);
		
		
		if (selDocHandle != null) {
			if (selDocHandle.trim().length() == 0) {
				throw new ServletException(String.format("Invalid doc(Handle) request parameter: [%s]", selDocHandle));
			}

			docHandle = new DocumentHandle(selDocHandle);
			logger.debug(String.format("Viewing specific document [%s] in database [%s] under account [%s] (page # %d)",selDocHandle, dbName, acctNumber, pageNum));

		} else {
			logger.debug(String.format("Viewing auto-selected document in database [%s] under account [%s] (page # %d)",dbName, acctNumber, pageNum));
		}
		
		try {
			
			DocumentHandle selectedDocHandle = docHandle;
			
			request.setAttribute("imageoutput", Boolean.valueOf(selImageOpFlag));
			request.setAttribute("pdfoutput", Boolean.valueOf(selPdfOpFlag));
			request.setAttribute("textoutput", Boolean.valueOf(selTextOpFlag));
			request.setAttribute("tiffoutput", Boolean.valueOf(selTiffOpFlag));
			request.setAttribute("outputformats", selDocOutputFrmts);
			logger.debug(String.format("viewDocument(): supported output formats for document are [%s]",selDocOutputFrmts));
			
			request.setAttribute("doc", selectedDocHandle);
			request.setAttribute("docformat", selDocFormat.toLowerCase());
			if (selDocFormat != null && selDocFormat.equalsIgnoreCase("collection")) {
				String type = selDocInfoType.toLowerCase();
				type = type.equals("afp") ? "pdf" : type;
				pl.writeTime("document type is : " + type);
				request.setAttribute("doctype", type);
				logger.debug(String.format(
						"viewDocument(): document format [%s], type [%s]",
						selDocFormat.toLowerCase(), selDocInfoType.toLowerCase()));
			} else if (selDocFormat.equalsIgnoreCase("djdeline")) {
				String type = selDocInfoType.toLowerCase();
				type = type.equals("afp") ? "text" : type;
				pl.writeTime("document type is : " + type);
				request.setAttribute("doctype", type);
				logger.debug(String.format(
						"viewDocument(): document format [%s], type [%s]",
						selDocFormat.toLowerCase(), selDocInfoType.toLowerCase()));
			}
			pl.writeTime("Page count is : " + selDocPageCount);
			
			if(selDocPageCount != null && !(selDocPageCount.trim().equals(""))){
				selPageCount = Integer.parseInt(selDocPageCount.toString());
			}
			if (pageNum > selPageCount) {
				pageNum = 1;
			}

			// set the Document's page number to be rendered and the
			// total page count
			if (request.getParameter("par") != null) {
				if (request.getParameter("par").equals("m1")) {
					if (pageNum > 1)
						pageNum = pageNum - 1;
					request.setAttribute("rot", request.getParameter("rot"));
					request.setAttribute("res", request.getParameter("res"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
				} else if (request.getParameter("par").equals("m5")) {
					if (pageNum > 5) {
						pageNum = pageNum - 5;
					} else {
						pageNum = 1;
					}
					request.setAttribute("rot", request.getParameter("rot"));
					request.setAttribute("res", request.getParameter("res"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
				} else if (request.getParameter("par").equals("first")) {
					pageNum = 1;
					request.setAttribute("rot", request.getParameter("rot"));
					request.setAttribute("res", request.getParameter("res"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
				} else if (request.getParameter("par").equals("last")) {
					
					pageNum = selPageCount;
					request.setAttribute("rot", request.getParameter("rot"));
					request.setAttribute("res", request.getParameter("res"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
				} else if (request.getParameter("par").equals("rotleft")) {
					request.setAttribute("par", "rotleft");
					if ((request.getParameter("rot")) != ""
							&& (request.getParameter("rot") != null)) {
						if (request.getParameter("rot").equals("CCW_90")) {
							request.setAttribute("rot", "CCW_180");
						} else if (request.getParameter("rot")
								.equals("CCW_180")) {
							request.setAttribute("rot", "CCW_270");
						} else if (request.getParameter("rot").equals(
								("CCW_270"))) {
							request.setAttribute("rot", "NONE");
						} else {
							request.setAttribute("rot", "CCW_90");
						}
					} else {
						request.setAttribute("rot", "CCW_90");
					}
					request.setAttribute("res", request.getParameter("res"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
				} else if (request.getParameter("par").equals("rotnorm")) {
					request.setAttribute("par", "rotnorm");
					request.setAttribute("res", request.getParameter("res"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
				} else if (request.getParameter("par").equals("rotright")) {
					request.setAttribute("par", "rotright");
					if ((request.getParameter("rot")) != ""
							&& (request.getParameter("rot") != null)) {
						if (request.getParameter("rot").equals("CCW_270")) {
							request.setAttribute("rot", "CCW_180");
						} else if (request.getParameter("rot")
								.equals("CCW_180")) {
							request.setAttribute("rot", "CCW_90");
						} else if (request.getParameter("rot").equals(
								("CCW_90"))) {
							request.setAttribute("rot", "NONE");
						} else {
							request.setAttribute("rot", "CCW_270");
						}
					} else {
						request.setAttribute("rot", "CCW_270");
					}
					request.setAttribute("res", request.getParameter("res"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
				} else if (request.getParameter("par").equals("zoomin")) {
					request.setAttribute("par", "zoomin");
					/* Harjinder Singh: 6th Nov 2014. Change in the NULL check and Empty String check of below condition.
					 * Code fix for zoomin issue in Document View
					 * */
					/*if ((request.getParameter("res")) != ""
							&& (request.getParameter("res") != null)) {*/
					if(request.getParameter("res") != null && !(request.getParameter("res").trim().equals(""))){
						if (request.getParameter("res").equals("WIDTH_512")) {
							request.setAttribute("res", "WIDTH_640");
						} else if (request.getParameter("res").equals(
								"WIDTH_640")) {
							request.setAttribute("res", "WIDTH_800");
						} else if (request.getParameter("res").equals(
								("WIDTH_800"))) {
							request.setAttribute("res", "WIDTH_1024");
						} else if (request.getParameter("res").equals(
								("WIDTH_1024"))) {
							request.setAttribute("res", "WIDTH_1280");
						} else if (request.getParameter("res").equals(
								("WIDTH_1280"))) {
							request.setAttribute("res", "WIDTH_1600");
						} else {
							request.setAttribute("res", "WIDTH_1600");
						}
					} else {
						request.setAttribute("res", "WIDTH_800");
					}
					request.setAttribute("rot", request.getParameter("rot"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
				} else if (request.getParameter("par").equals("zoomnorm")) {
					request.setAttribute("par", "zoomnorm");
					request.setAttribute("rot", request.getParameter("rot"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);

				} else if (request.getParameter("par").equals("zoomout")) {
					request.setAttribute("par", "zoomout");

					/* Harjinder Singh: 6th Nov 2014. Change in the NULL check and Empty String check of below condition.
					 * Code fix for zoomin issue in Document View
					 * */
					/*if ((request.getParameter("res")) != ""
							&& (request.getParameter("res") != null)) {*/
					if(request.getParameter("res") != null && !(request.getParameter("res").trim().equals(""))){
						if (request.getParameter("res").equals("WIDTH_1600")) {
							request.setAttribute("res", "WIDTH_1280");
						} else if (request.getParameter("res").equals(
								"WIDTH_1280")) {
							request.setAttribute("res", "WIDTH_1024");
						} else if (request.getParameter("res").equals(
								("WIDTH_1024"))) {
							request.setAttribute("res", "WIDTH_800");
						} else if (request.getParameter("res").equals(
								("WIDTH_800"))) {
							request.setAttribute("res", "WIDTH_640");
						} else if (request.getParameter("res").equals(
								("WIDTH_640"))) {
							request.setAttribute("res", "WIDTH_512");
						} else {
							request.setAttribute("res", "WIDTH_512");
						}
					} else {
						request.setAttribute("res", "WIDTH_512");
					}
					request.setAttribute("rot", request.getParameter("rot"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
				} else if (request.getParameter("par").equals("multiplepages")) {
					request.setAttribute("par", "multiplepages");
					if ((request.getParameter("mult1")) != ""
							&& (request.getParameter("mult1") != null)) {
						if (request.getParameter("mult1").equals("2")) {
							request.setAttribute("mult1", "4");
						} else if (request.getParameter("mult1").equals("4")) {
							request.setAttribute("mult1", "1");
						} else {
							request.setAttribute("mult1", "2");
						}
					} else {
						request.setAttribute("mult1", "2");
					}
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
					request.setAttribute("rot", request.getParameter("rot"));
					request.setAttribute("res", request.getParameter("res"));
					multiplePages = true;
				} else {
					pageNum = pageNum
							+ Integer.valueOf(request.getParameter("par"))
							.intValue();
					
					if (pageNum > selPageCount)
						pageNum = selPageCount;
					
					request.setAttribute("rot", request.getParameter("rot"));
					request.setAttribute("res", request.getParameter("res"));
					request.setAttribute("mult1", request.getParameter("mult1"));
					request.setAttribute("mult2", pageNum + 1);
					request.setAttribute("mult3", pageNum + 2);
					request.setAttribute("mult4", pageNum + 3);
				}
			}

			request.setAttribute("pagenum", pageNum);
			request.setAttribute("pagecnt", selPageCount);
			request.setAttribute("singlepagedoc",Boolean.valueOf(selPageCount == 1));
			System.out.println("Interface request dispatcher:"+request.getParameter("par"));
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/ViewDocument.jsp");
			rd.include(request, response);
			System.out.println("Included inside");
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Generic Exception ----------> "+e);
			logger.error("Caught Generic Exception", e);
			throw new ServletException("Caught Exception", e);
		} finally {
			System.out.println("Requesting RenderDocument");
		}
	}

	// <editor-fold defaultstate="collapsed"
	// desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("**** doGet");
		System.out.println("coming to interface::+doGet");
		HttpSession session = request.getSession(false);

		if (session != null) {
			System.out.println("**** session: " + session);
			processRequest(request, response);
		} else {
			System.out.println("session is null ");
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/ShowLogin.action");
			if (dispatcher != null) {
				dispatcher.forward(request, response);
			}
		}
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("**** doPost");
		System.out.println("coming to interface::+doPost");
		HttpSession session = request.getSession(false);
		System.out.println("**** session: " + session);
		if (session != null) {
			processRequest(request, response);
		} else {
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/ShowLogin.action");
			if (dispatcher != null) {
				dispatcher.forward(request, response);
			}
		}
	}

	/**
	 * Returns a short description of the servlet.
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	private static final Logger logger = Logger.getLogger(Interface.class);
	private static final String GSE_ERROR_MSG = "Security error occurred while trying to connect to e2 Vault server via SSL";
	private static AppConfig appConfig = null;
	private static AppVersion appVersion = null;

}
