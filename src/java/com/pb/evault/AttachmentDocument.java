/*
 * Copyright:  Copyright (C) 1993-2010 Pitney Bowes Software Inc.
 *
 * $Id: RenderDocument.java 6100 2010-08-24 18:01:02Z ssahmed $
 *
 * RenderDocument.java
 */

package com.pb.evault;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.lowagie.tools.Executable;
import com.pb.ocbc.SearchAccountNew;

/**
 * Servlet whose only purpose is to render an attachment document 
 */
public class AttachmentDocument extends HttpServlet {

	private static final long serialVersionUID = 1L;
	SearchAccountNew sa = new SearchAccountNew();

	@Override
	public void init() throws ServletException {
		appConfig = (AppConfig) getServletContext().getAttribute("appconfig");

		// setup the Mime Type mapping db
		mimeTypeMap = new HashMap<String, String>();
		mimeTypeMap.put(".pdf", "application/pdf");
		mimeTypeMap.put(".mht", "message/rfc822");
		mimeTypeMap.put(".mhtml", "message/rfc822");
		mimeTypeMap.put(".htm", "text/html");
		mimeTypeMap.put(".html", "text/html");
		mimeTypeMap.put(".bmp", "image/bmp");
		mimeTypeMap.put(".gif", "image/gif");
		mimeTypeMap.put(".jpg", "image/jpeg");
		mimeTypeMap.put(".jpeg", "image/jpeg");
		mimeTypeMap.put(".tif", "image/tiff");
		mimeTypeMap.put(".tiff", "image/tiff");
		mimeTypeMap.put(".txt", "text/plain");
		mimeTypeMap.put(".xml", "text/xml");
	}

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		logger.debug(String.format("processRequest(): query string [%s]",
				request.getQueryString()));
		System.out.println("processRequest inside AttachmentDocument");
		request.getSession();
		final String host = request.getParameter("host");
		final String attachmentPath = request.getParameter("attachmentPath");
		final String attachment = request.getParameter("attachment");
		final String pathfile = "\\\\"+host+attachmentPath+"\\"+attachment;
		System.out.println("pathfile::"+pathfile);
		File file = new File(pathfile);
		response.setContentType("application/pdf");
		response.setContentLength((int) file.length());
		InputStream input = new FileInputStream(file);
		
		OutputStream output = response.getOutputStream();
		int length = 0;
        byte[] buffer = new byte[1024];   
         
        while((input != null) && ((length = input.read(buffer)) != - 1))   
        {   
        	output.write(buffer, 0, length);   
        }   
        input.close();   
        output.flush();   
        output.close();  
			
	}
	

	/**
	 * Construct a filename suitable for saving a Document's rendering output to
	 * 
	 * @param dbName
	 * @param acctNumber
	 * @param date
	 * @param fileExtension
	 * @return
	 */
	private String getOutputFilename(String dbName, String acctNumber,
			String date, String fileExtension) {
		date = date.replaceAll("/+", "_");
		date = date.replaceAll("\\s+", "_");
		date = date.replaceAll(":+", "_");
		return String.format("%s-%s-%s%s", dbName, acctNumber, date,
				fileExtension);
	}

	// <editor-fold defaultstate="collapsed"
	// desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	private static final Logger logger = Logger.getLogger(AttachmentDocument.class);
	private static AppConfig appConfig = null;
	private static Map<String, String> mimeTypeMap;

}
