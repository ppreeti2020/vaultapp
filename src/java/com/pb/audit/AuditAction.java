package com.pb.audit;

import java.io.File;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.print.attribute.standard.Compression;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.ServerErrorException;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Database;
import com.g1.e2.vault.VaultException;
import com.pb.action.BaseAction;
import com.pb.admin.Common;
import com.pb.common.CommonConstants;
import com.pb.common.INIFile;
import com.pb.common.ProfileIniParser;
import com.pb.common.PropertyUtils;
import com.pb.dao.AdminReportDAO;
import com.pb.dao.DataIntegrityDAO;
import com.pb.dao.FileInfoDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.DataDTO;
import com.pb.dto.IndexDTO;
import com.pb.dto.UserDTO;
import com.pb.manager.AdminReportManager;
import com.pb.manager.DataIntegrityManager;
import com.pb.manager.FileInfoManager;
import com.pb.manager.UserManager;

public class AuditAction extends BaseAction {

	private static final long serialVersionUID = 1L;
	static final Logger logger = Logger.getLogger(AuditAction.class);

	public static final String XML_PROLOG = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	public static final String STORE_FRONT = "<Audit>";

	public static final String STORE_FRONT_CLOSE = "</Audit>";

	// private List<DataDTO> sampleList;
	private List<DataDTO> populateLogList, populateFreqList;
	private List<UserDTO> populateDBList;
	private Map<Long, DataDTO> treeMapList;
	private Map<Integer, DataDTO> treeProfile;
	private String name;
	private HttpServletRequest servletRequest;
	private HttpServletResponse servletResponse;
	private int pageSize;

	private String hostName;
	private String profile;
	private String searchProfile;
	// private UserManager userManager = new UserDAO();
	private FileInfoDAO fileManager = new FileInfoDAO();
	private AdminReportDAO reportManager = new AdminReportDAO();
	private DataIntegrityDAO dataManager = new DataIntegrityDAO();
	private String ip;
	private String logDate;
	private String tabId;
	private String auditSubTabId;
	private String serverIP;
	private String softwarePath;
	private String server;
	private String serverHost;
	// private String serverSoftwarePath;
	private int archivalServerPort;
	private int renderServerPort;
	private int dataRouterPort;
	private String auditLogTabId;
	private String vaultPath;
	private boolean archivalTabs;
	private List<IndexDTO> indexData;

	public List<IndexDTO> getIndexData() {
		return indexData;
	}

	public void setIndexData(List<IndexDTO> indexData) {
		this.indexData = indexData;
	}

	public boolean isArchivalTabs() {
		return archivalTabs;
	}

	public void setArchivalTabs(boolean archivalTabs) {
		this.archivalTabs = archivalTabs;
	}

	public void setPopulateDBList(List<UserDTO> populateDBList) {
		this.populateDBList = populateDBList;
	}

	public List<UserDTO> getPopulateDBList() {
		return populateDBList;
	}

	public void setAuditLogTabId(String auditLogTabId) {
		this.auditLogTabId = auditLogTabId;
	}

	public String getAuditLogTabId() {
		return auditLogTabId;
	}

	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}

	public String getLogDate() {
		return logDate;
	}

	public void setSearchProfile(String searchProfile) {
		this.searchProfile = searchProfile;
	}

	public String getSearchProfile() {
		return searchProfile;
	}

	public void setAuditSubTabId(String auditSubTabId) {
		this.auditSubTabId = auditSubTabId;
	}

	public String getAuditSubTabId() {
		return auditSubTabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public String getTabId() {
		return tabId;
	}

	public void setDataRouterPort(int dataRouterPort) {
		this.dataRouterPort = dataRouterPort;
	}

	public int getDataRouterPort() {
		return dataRouterPort;
	}

	public void setRenderServerPort(int renderServerPort) {
		this.renderServerPort = renderServerPort;
	}

	public int getRenderServerPort() {
		return renderServerPort;
	}

	public void setArchivalServerPort(int archivalServerPort) {
		this.archivalServerPort = archivalServerPort;
	}

	public int getArchivalServerPort() {
		return archivalServerPort;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getServer() {
		return server;
	}

	public String getServerHost() {
		return serverHost;
	}

	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	public String getServerIP() {
		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public String getSoftwarePath() {
		return softwarePath;
	}

	public void setSoftwarePath(String softwarePath) {
		this.softwarePath = softwarePath;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getIp() {
		return ip;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getProfile() {
		return profile;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostName() {
		return hostName;
	}

	public void setTreeProfile(Map<Integer, DataDTO> treeProfile) {
		this.treeProfile = treeProfile;
	}

	public Map<Integer, DataDTO> getTreeProfile() {
		return treeProfile;
	}

	public void setTreeMapList(Map<Long, DataDTO> treeMapList) {
		this.treeMapList = treeMapList;
	}

	public Map<Long, DataDTO> getTreeMapList() {
		return treeMapList;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setPopulateFreqList(ArrayList populateFreqList) {
		this.populateFreqList = populateFreqList;
	}

	public List getPopulateFreqList() {
		return populateFreqList;
	}

	public List<DataDTO> getPopulateLogList() {
		return populateLogList;

	}

	public void setPopulateLogList(ArrayList<DataDTO> populateLogList) {
		this.populateLogList = populateLogList;
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest paramHttpServletRequest) {
		this.servletRequest = paramHttpServletRequest;
	}

	public HttpServletResponse getServletResponse() {
		return servletResponse;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.servletResponse = response;
	}

	private boolean isNullOrEmpty(String s)
	{
		if(s==null )
			return true;
		if(s.equalsIgnoreCase(""))
			return true;
		
		return false;
		
			
	}
	
	public String executeServerDetails() {
		//System.out.println("executeServerDetails");
		setTabId("Audit");
		HttpSession sessionku = getServletRequest().getSession();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		if (getServletRequest().getParameter("pg") != null) {
			setTabId("AuditCheck");
		} else {
			setTabId("Audit");
		}
		try {
			System.out.println("executeServerDetails"
					+ getServletRequest().getParameter("server"));
			String filePath = CommonConstants.VAULT_AUDIT_CONFIG_FILE_NAME;
			System.out.println("filePath" + filePath);
			treeMapList = this.reportManager.getRootLogReports();

			treeProfile = this.reportManager.getDbProfiles();

			File file = new File(filePath);
			if (file.exists()) {
				if (getServletRequest().getParameter("server") != null) {

					// file.createNewFile();
					// String filePath =
					// CommonConstants.CSR_PROPERTY_FILE_NAME;
					// String propert = PropertyUtils.getProperties(filePath)
					// .getProperty(CommonConstants.VAULT_CONFIG_FILE_PATH)
					// + "/" + CommonConstants.VAULT_CONFIG_FILE_NAME;
					Properties properties = PropertyUtils
							.getProperties(CommonConstants.VAULT_AUDIT_CONFIG_FILE_NAME);
					// String prop = properties
					// .getProperty(CommonConstants.ARCHIVAL_SERVER_HOST1);
					System.out.println("displayprop"
							+ getServletRequest().getParameter("server"));

					if (getServletRequest().getParameter("server").equals(
							"server1")) {
						//System.out.println("Inside server1");
						if (properties
								.getProperty(CommonConstants.SERVER1_HOST) != null)
							setServerHost(properties
									.getProperty(CommonConstants.SERVER1_HOST));

						if (properties
								.getProperty(CommonConstants.ARCHIVAL_SERVER1_PORT) != null)
							setArchivalServerPort(Integer
									.parseInt(properties
											.getProperty(CommonConstants.ARCHIVAL_SERVER1_PORT)));
						if (properties
								.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != null)
							setRenderServerPort(Integer
									.parseInt(properties
											.getProperty(CommonConstants.RENDERING_SERVER1_PORT)));
						if (properties
								.getProperty(CommonConstants.DATA_ROUTER_SERVER1_PORT) != null
								&& !properties
										.getProperty(
												CommonConstants.DATA_ROUTER_SERVER1_PORT)
										.equals("")
								&& !properties
										.getProperty(
												CommonConstants.DATA_ROUTER_SERVER1_PORT)
										.equals(0))
							setDataRouterPort(Integer
									.parseInt(properties
											.getProperty(CommonConstants.DATA_ROUTER_SERVER1_PORT)));

						if (properties
								.getProperty(CommonConstants.SERVER1_SOFTWARE_PATH) != null)
							setSoftwarePath(properties
									.getProperty(CommonConstants.SERVER1_SOFTWARE_PATH));
						if (properties
								.getProperty(CommonConstants.SERVER1_VAULT_SERVER_PATH) != null)
							setVaultPath(properties
									.getProperty(CommonConstants.SERVER1_VAULT_SERVER_PATH));

					} else if (getServletRequest().getParameter("server")
							.equals("server2")) {
						//System.out.println("Inside server2");
						if (properties
								.getProperty(CommonConstants.SERVER2_HOST) != null)
							setServerHost(properties
									.getProperty(CommonConstants.SERVER2_HOST));

						if (properties
								.getProperty(CommonConstants.ARCHIVAL_SERVER2_PORT) != null)
							setArchivalServerPort(Integer
									.parseInt(properties
											.getProperty(CommonConstants.ARCHIVAL_SERVER2_PORT)));
						if (properties
								.getProperty(CommonConstants.RENDERING_SERVER2_PORT) != null
								&& !properties.getProperty(
										CommonConstants.RENDERING_SERVER2_PORT)
										.equals("")
								&& !properties.getProperty(
										CommonConstants.RENDERING_SERVER2_PORT)
										.equals(0))
							setRenderServerPort(Integer
									.parseInt(properties
											.getProperty(CommonConstants.RENDERING_SERVER2_PORT)));
						if (properties
								.getProperty(CommonConstants.DATA_ROUTER_SERVER2_PORT) != null
								&& !properties
										.getProperty(
												CommonConstants.DATA_ROUTER_SERVER2_PORT)
										.equals("")
								&& !properties
										.getProperty(
												CommonConstants.DATA_ROUTER_SERVER2_PORT)
										.equals(0))
							setDataRouterPort(Integer
									.parseInt(properties
											.getProperty(CommonConstants.DATA_ROUTER_SERVER2_PORT)));

						if (properties
								.getProperty(CommonConstants.SERVER2_SOFTWARE_PATH) != null)
							setSoftwarePath(properties
									.getProperty(CommonConstants.SERVER2_SOFTWARE_PATH));
						if (properties
								.getProperty(CommonConstants.SERVER2_VAULT_SERVER_PATH) != null)
							setVaultPath(properties
									.getProperty(CommonConstants.SERVER2_VAULT_SERVER_PATH));

					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}

	private boolean isProfileNullOrEmpty(String name) {
		if (name == null || name.equalsIgnoreCase("")) {
			return true;
		}
		return false;
	}
	
	public String showLogErrors() throws NumberFormatException, SQLException
	{
		String logFileId=getServletRequest().getParameter("logFileId");
		
		System.out.println("Log Errors Tracking"+logFileId);
		populateFreqList = this.reportManager.getLogFileErrors(	Long.parseLong(logFileId));
		//getServletRequest().getSession().setAttribute("populateFreqList",populateFreqList);
		/*for (DataDTO d : populateFreqList) {
			System.out.println(d.getErrCode());
			System.out.println(d.getErrDesc());
		}*/
		
		return "SUCCESS";
	}

	public String execute() {
		Long UserId = 0L;
		HttpSession sessionku = getServletRequest().getSession();

		String logDate = (String) getServletRequest().getParameter("logDate");
		String profileName = (String) getServletRequest().getParameter(
				"searchProfile");
		
	

		String name1=(String)getServletRequest().getParameter(
		"name");
		System.out.println("Log Date===================" + logDate);
		System.out.println("Profile Name===================" + profileName);
		System.out.println("Name===================" + name1);

		populateLogList = new ArrayList<DataDTO>();
		populateFreqList = new ArrayList();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		String tab = (String) getServletRequest().getParameter("tab");
		if (getSession().get("UserID") != null) {
			UserId = Long.parseLong(getSession().get("UserID").toString());

			System.out.println("UserID:search1" + UserId);

		}
		List<DataDTO> logReport = new ArrayList<DataDTO>();
		try {
			treeMapList = this.reportManager.getRootLogReports();

			treeProfile = this.reportManager.getDbProfiles();

			Long hostId = 0l;
			if (getServletRequest().getParameter("hostId") != null)
				hostId = Long.parseLong(getServletRequest().getParameter(
						"hostId"));
			if (isNullOrEmpty((String)getServletRequest().getParameter("name"))) {
				//System.out.println("coming here" + UserId);
				if ((!isNullOrEmpty((String)getServletRequest().getParameter("searchProfile"))
						|| !isNullOrEmpty((String)getServletRequest().getParameter("logDate")))) {

					System.out.println("logDate"
							+ getServletRequest().getParameter("logDate"));
					System.out
							.println("searchProfile"
									+ getServletRequest().getParameter(
											"searchProfile"));
					logReport = this.reportManager.getLogFileReports(
							getServletRequest().getParameter("searchProfile"),
							getServletRequest().getParameter("logDate"),
							hostId);
					setSearchProfile(getServletRequest().getParameter(
							"searchProfile"));
					setLogDate(getServletRequest().getParameter("logDate"));
				} else {
					logReport = this.reportManager.getLogFileReports(hostId);
				}
				int fileSize = 0;
				for (DataDTO dataDto : logReport) {
					// DataDTO DataDTO = (DataDTO) iter.next();
					// DataDTO dDao = new DataDTO();

					// List logErrors = dao.getLogFileErrors(logfileid);
					// fileSize = logErrors.size();
					// DataDTO.setErrCount(fileSize);
					System.out.println("Profile is" + dataDto.getProfile()
							+ "ddddd==" + dataDto.getProfileName());
					int logResources = this.reportManager.getLogCountResources(
							dataDto.getLogFileId());
					dataDto.setResourceCount(logResources);
					// List logIndexes = dao.getLogFileIndexes(logfileid);
					// DataDTO.setIndexCount(logIndexes.size());

					populateLogList.add(dataDto);

				}
				if (getServletRequest().getParameter("logFileId") != null
						&& getServletRequest().getParameter("type") != null) {
					String xmlData = "";
					String name = "";
					if (getServletRequest().getParameter("type")
							.equalsIgnoreCase("resources")) {
						List list = this.reportManager.getLogFileResources(Long
								.parseLong(getServletRequest().getParameter(
										"logFileId")));
						xmlData = "Resource Name | Size\n";
						for (Iterator iter = list.iterator(); iter.hasNext();) {
							DataDTO dDao = (DataDTO) iter.next();
							System.out
									.println("getResourceId::"
											+ dDao.getResourceId()
											+ dDao.getResWrote());
							/*if (dDao.getResourceId() != 0) {
								System.out.println("xmlData::" + xmlData);
							}*/
						}
						name = "Resources";
					}
					if (getServletRequest().getParameter("type")
							.equalsIgnoreCase("errors")) {
						populateFreqList = this.reportManager.getLogFileErrors(
								Long.parseLong(getServletRequest()
										.getParameter("logFileId")));
						for (Iterator iter = populateFreqList.iterator(); iter
								.hasNext();) {
							DataDTO DataDTO = (DataDTO) iter.next();
							if (DataDTO.getErrorId() != 0) {

							}
						}
						name = "Errors";
					}

				}

				setAuditLogTabId("Log");
			} else if (getServletRequest().getParameter("name") != null
					&& getServletRequest().getParameter("hostId") != null) {
				String compressingType = getServletRequest().getParameter(
						"name");
				System.out.println("logFileId: "
						+ servletRequest.getAttribute("logFileId"));
				// System.out.println("linkType in java: " + linkType);
				// populateFreqList = new ArrayList();
				if ((!isNullOrEmpty((String)(getServletRequest().getParameter("searchProfile"))))
						|| !isNullOrEmpty((String)getServletRequest().getParameter("logDate") )) {
					
					System.out.println("HEreeeeee"+getServletRequest().getParameter("searchProfile"));
					System.out.println("Here"+ getServletRequest().getParameter("logDate"));
					
					if(tab.equalsIgnoreCase("index") ||tab.equalsIgnoreCase("reIndex") || tab.equalsIgnoreCase("purge") || tab.equalsIgnoreCase("remove"))
					{
						logReport = this.reportManager.getLogFileReportsWithIndexes(
								getServletRequest().getParameter("searchProfile"),
								getServletRequest().getParameter("logDate"),
								compressingType, hostId);
					}
					else
					{
					logReport = this.reportManager.getLogFileReports(
							getServletRequest().getParameter("searchProfile"),
							getServletRequest().getParameter("logDate"),
							compressingType, hostId);
				
					}
					} else {
					System.out.println("Here2=========================================="+tab);
					if(tab.equalsIgnoreCase("index") ||tab.equalsIgnoreCase("reIndex") || tab.equalsIgnoreCase("purge") || tab.equalsIgnoreCase("remove"))
					{
						//System.out.println("inside or=======================");
						logReport= this.reportManager.getLogFileReportsWithIndexes(
									compressingType,
									Long.parseLong(getServletRequest().getParameter(
											"hostId")));
					}
					else
					{
					logReport = this.reportManager.getLogFileReports(
							compressingType,
							Long.parseLong(getServletRequest().getParameter(
									"hostId")));
					}
				}
			
				int fileSize = 0;
				for (DataDTO dataDto : logReport) {
					
					int logResources = this.reportManager.getLogCountResources(
							dataDto.getLogFileId());
					dataDto.setResourceCount(logResources);
					populateLogList.add(dataDto);
				}

				if (getServletRequest().getParameter("logFileId") != null
						&& getServletRequest().getParameter("type") != null) {
					String xmlData = "";
					String name = "";
					if (getServletRequest().getParameter("type")
							.equalsIgnoreCase("resources")) {
						populateFreqList = this.reportManager
								.getLogFileResources(Long
										.parseLong(getServletRequest()
												.getParameter("logFileId")));
						name = "Resources";
						// setAuditLogTabId("ShowResource");
					}
					if (getServletRequest().getParameter("type")
							.equalsIgnoreCase("errors")) {
						populateFreqList = this.reportManager.getLogFileErrors(
								Long.parseLong(getServletRequest()
										.getParameter("logFileId")));
						name = "Errors";
					}

				}
			}
			if (tab.equalsIgnoreCase("indexLog")) {
				setAuditLogTabId("indexLog");
			}
			if (tab.equalsIgnoreCase("index")) {
				setAuditLogTabId("index");
			}

			if (tab.equalsIgnoreCase("indexResource")) {
				setAuditLogTabId("indexResource");
			}

			if (tab.equalsIgnoreCase("reIndexLog")) {
				setAuditLogTabId("reIndexLog");
			}
			if (tab.equalsIgnoreCase("reIndex")) {
				setAuditLogTabId("reIndex");
			}

			if (tab.equalsIgnoreCase("reIndexResource")) {
				setAuditLogTabId("reIndexResource");
			}

			if (tab.equalsIgnoreCase("purgingLog")) {
				setAuditLogTabId("purgingLog");
			}
			if (tab.equalsIgnoreCase("purge")) {
				setAuditLogTabId("purge");
			}

			if (tab.equalsIgnoreCase("purgeResource")) {
				setAuditLogTabId("purgeResource");
			}

			if (tab.equalsIgnoreCase("removeLog")) {
				setAuditLogTabId("removeLog");
			}
			if (tab.equalsIgnoreCase("remove")) {
				setAuditLogTabId("remove");
			}

			if (tab.equalsIgnoreCase("removeResource")) {
				setAuditLogTabId("removeResource");
			}

			setTabId("Audit");

		} catch (Exception e) {

		}
		return "SUCCESS";
	}

	public String executeResources() {
		Long UserId = 0L;
		HttpSession sessionku = getServletRequest().getSession();

		populateLogList = new ArrayList<DataDTO>();
		populateFreqList = new ArrayList();
		setTabId("Audit");
		setAuditLogTabId("Resources");
		String res = "";
		String size = "";
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		if (getSession().get("UserID") != null) {
			UserId = Long.parseLong(getSession().get("UserID").toString());

			//System.out.println("UserID:search1" + UserId);

		}
		try {
			treeMapList = this.reportManager.getRootLogReports();
			treeProfile = this.reportManager.getDbProfiles();
			Long hostId = 0l;
			
			if (getServletRequest().getParameter("hostId") != null)
				hostId = Long.parseLong(getServletRequest().getParameter(
						"hostId"));
							
				if (isNullOrEmpty((String)getServletRequest().getParameter("name"))) {
					//System.out.println("coming here" + UserId);
					List logReports;
					if ((!isNullOrEmpty((String)getServletRequest().getParameter("searchProfile"))
							|| !isNullOrEmpty((String)getServletRequest().getParameter("logDate")))) {

						System.out.println("logDate"
								+ getServletRequest().getParameter("logDate"));
						System.out
								.println("searchProfile"
										+ getServletRequest().getParameter(
												"searchProfile"));
						logReports = this.reportManager.getLogFileReports(
								getServletRequest().getParameter("searchProfile"),
								getServletRequest().getParameter("logDate"),
								hostId);
						setSearchProfile(getServletRequest().getParameter(
								"searchProfile"));
						setLogDate(getServletRequest().getParameter("logDate"));
					} else {			
				
				logReports = (ArrayList) this.reportManager
						.getLogFileReports(hostId);
					}
				int fileSize = 0;
				System.out.println("logReports size::"+logReports.size());
				for (Iterator iter = logReports.iterator(); iter.hasNext();) {
					DataDTO DataDtO = (DataDTO) iter.next();
					List logResources = this.reportManager.getLogFileResources(
							DataDtO.getLogFileId());
					for (Iterator iter1 = logResources.iterator(); iter1
							.hasNext();) {
						DataDTO dDao = (DataDTO) iter1.next();
						res += dDao.getResWrote() + " \n";
						DataDtO.setResWrote(res);
						size += dDao.getResSize() + " \n";
						DataDtO.setResSize(size);
					}
					
					populateLogList.add(DataDtO);
				}
				populateFreqList = this.reportManager
						.getLogFileResources();

			} 
			else if (getServletRequest().getParameter("name") != null) {
				String compressingType = getServletRequest().getParameter(
						"name");
				if (compressingType.equalsIgnoreCase("Indexing")) {
					setAuditLogTabId("indexResource");
				}
				if (compressingType.equalsIgnoreCase("Reindexing")) {
					setAuditLogTabId("reIndexResource");
				}
				if (compressingType.equalsIgnoreCase("Purging")) {
					setAuditLogTabId("purgeResource");
				}
				if (compressingType.equalsIgnoreCase("Remove")) {
					setAuditLogTabId("removeResource");
				}
				System.out.println("logFileId: "
						+ servletRequest.getAttribute("logFileId"));
							
				
				List logReports=null;
				if ((!isNullOrEmpty((String)(getServletRequest().getParameter("searchProfile"))))
						|| !isNullOrEmpty((String)getServletRequest().getParameter("logDate") )) {
					
					System.out.println("HEreeeeee"+getServletRequest().getParameter("searchProfile"));
					System.out.println("Here"+ getServletRequest().getParameter("logDate"));
					logReports = this.reportManager.getLogFileReports(
							getServletRequest().getParameter("searchProfile"),
							getServletRequest().getParameter("logDate"),
							compressingType, hostId);
				}
							
				else
				{
				 logReports = (ArrayList) this.reportManager
						.getLogFileReports(compressingType, hostId);
				}
				int fileSize = 0;
				for (Iterator iter = logReports.iterator(); iter.hasNext();) {
					DataDTO DataDTO = (DataDTO) iter.next();
					List logResources = this.reportManager.getLogFileResources(
							DataDTO.getLogFileId());
					for (Iterator iter1 = logResources.iterator(); iter1
							.hasNext();) {
						DataDTO dDao = (DataDTO) iter1.next();
						res += dDao.getResWrote() + "\n";
						DataDTO.setResWrote(res);
						size += dDao.getResSize() + "\n";
						DataDTO.setResSize(size);
					}
					// List logIndexes = dao.getLogFileIndexes(logfileid);
					// DataDTO.setIndexCount(logIndexes.size());
					populateLogList.add(DataDTO);
				}
				populateFreqList = this.reportManager.getLogFileResources(
						compressingType);
			}
		} catch (Exception e) {

		}
		setSearchProfile((String)getServletRequest().getParameter(
		"searchProfile"));
		setLogDate((String)getServletRequest().getParameter("logDate"));
		System.out.println("Populating resources finsihed");
		return "SUCCESS";
	}

	public String executeIndexes() {
		Long UserId = 0L;
		setTabId("Audit");
		setAuditLogTabId("Index");
		HttpSession sessionku = getServletRequest().getSession();

		populateLogList = new ArrayList<DataDTO>();
		populateFreqList = new ArrayList();
		String res = "";
		String size = "";
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		if (getSession().get("UserID") != null) {
			UserId = Long.parseLong(getSession().get("UserID").toString());

			//System.out.println("UserID:search1" + UserId);

		}
		String host = "";
		int port = 0;
		String ip = "";
		try {
			treeMapList = this.reportManager.getRootLogReports();
			// Map<Long, DataDTO> map =
			// this.reportManager.getLogProfiles(realPath);
			// if(!map.isEmpty())
			// {
			treeProfile = this.reportManager.getDbProfiles();
			// }

			if (servletRequest.getParameter("profile") != null) {
				profile = servletRequest.getParameter("profile");
				if (profile.equalsIgnoreCase("profiles")) {

				} else {
					// hostName = servletRequest.getParameter("hostName");

					System.out
							.println("executeIndexes-vault:profile" + profile);
					// List<DataDTO> indexList =
					// this.reportManager.getLogFileIndexes(Long.parseLong(servletRequest.getParameter("logFileId")),
					// realPath);
					String index = "";
					// DataDTO dto = new DataDTO();

					String propert = CommonConstants.VAULT_AUDIT_CONFIG_FILE_NAME;
					Properties properties = PropertyUtils
							.getProperties(propert);
					if (properties.getProperty(CommonConstants.SERVER1_HOST) != null
							|| properties
									.getProperty(CommonConstants.SERVER1_HOST) != "")
						host = properties
								.getProperty(CommonConstants.SERVER1_HOST);

					if (properties
							.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != null
							|| properties
									.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != "")
						port = Integer
								.parseInt(properties
										.getProperty(CommonConstants.RENDERING_SERVER1_PORT));
					
					
					String hosts=getServletRequest().getParameter("hostId");
					String dbId=getServletRequest().getParameter("dbId");
				//	ProfileIniParser pp=new ProfileIniParser(profileFile, 10);
					
					UserDAO umg=new UserDAO();
					populateLogList=	umg.getIndexByDBID(new Long(dbId), new Long(hosts));
					System.out.println("size is"+populateLogList.size());
					
				/*	// {
					for (SearchIndex in : Indexs) {

						// if(dto.getIndexingFile()!=null)
						// {
						// System.out.println("dto.getIndexingFile()"+dto.getIndexingFile());
						// index =
						// dto.getIndexingFile().split("\\\\")[1].split(".dri")[0];
						// if(index.equalsIgnoreCase(in.getName()))
						// {
						DataDTO dto = new DataDTO();
						// dto.setIndexId(in.getIndex());
						dto.setIndexName(in.getName());
						dto.setIndexDesc(in.getDescription());
						populateLogList.add(dto);
						// }
						// }
						// }
					}*/

					/*
					 * System.out.println("connected"); Set<Database> Databases
					 * = vc.getDatabases(); String dbName = ""; String tempdb =
					 * ""; for (Database db1 : Databases) {
					 * System.out.println("Vault DatabaseName" + db1.getName());
					 * for (int i = 0; i < objINI.getTotalSections(); i++) { //
					 * System
					 * .out.println("INIDatabases"+objINI.getStringProperty
					 * (objINI.getAllSectionNames()[i], // "Database"));
					 * 
					 * if (db1.getName() .equalsIgnoreCase(
					 * objINI.getStringProperty( objINI.getAllSectionNames()[i],
					 * "Database"))) { if
					 * (!db1.getName().equalsIgnoreCase(tempdb)) {
					 * System.out.println("INIDatabases" +
					 * objINI.getStringProperty( objINI.getAllSectionNames()[i],
					 * "Database")); //
					 * System.out.println("Index1"+objINI.getStringProperty
					 * (objINI.getAllSectionNames()[i], // "Index1")); UserDTO
					 * dto = new UserDTO(); dto.setDbDesc(db1.getDescription());
					 * dto.setDbName(db1.getName()); dbList.add(dto); } tempdb =
					 * db1.getName(); } } }
					 * 
					 * Long dbId = 0L;
					 * 
					 * dbName =
					 * servletRequest.getParameter("profile").toString();
					 * System.out.println("dbName:" + dbName);
					 * 
					 * 
					 * for (int i = 0; i < objINI.getTotalSections(); i++) { if
					 * (dbName.equalsIgnoreCase(objINI.getStringProperty(
					 * objINI.getAllSectionNames()[i], "Database"))) { Database
					 * db = vc.getDatabase(dbName); Set<SearchIndex> Indexs =
					 * db.getSearchIndexes(); //
					 * System.out.println("Index | Fields | Name | Desc"); int k
					 * = 0; //indexValues = new String[Indexs.size()];
					 * //indexName = new String[Indexs.size()]; for (SearchIndex
					 * in : Indexs) { if (objINI.getStringProperty(
					 * objINI.getAllSectionNames()[i], "Index" + in.getIndex())
					 * != null) { System.out.println(in.getIndex() + "|" +
					 * in.getFields() + "|" + in.getName() + "|" +
					 * in.getDescription()); System.out.println("index" +
					 * in.getIndex() + ": " + objINI.getStringProperty(
					 * objINI.getAllSectionNames()[i], "Index" +
					 * in.getIndex())); System.out.println("Name"+in.getName());
					 * System.out.println("Desc"+in.getDescription());
					 * 
					 * UserDTO DataDTO = new UserDTO(); Integer ind =
					 * in.getIndex(); DataDTO.setIndexId(ind.longValue());
					 * DataDTO.setIndexDesc(in.getDescription());
					 * DataDTO.setIndexName(in.getName());
					 * populateLogList.add(DataDTO); //indexValues[k] =
					 * in.getDescription(); //indexName[k] = in.getName(); k++;
					 * 
					 * 
					 * } }
					 * 
					 * // System.out.println("boolean:"+b);
					 * 
					 * 
					 * } }
					 */
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}

	public String getindexData() {
		AdminReportDAO ad = new AdminReportDAO();
		setTabId("Audit");
		setAuditLogTabId("ShowIndexData");
		treeMapList = this.reportManager.getRootLogReports();
		treeProfile = this.reportManager.getDbProfiles();

		List<IndexDTO> indexData = ad.getIndexData(1l);
		this.indexData = indexData;
		return "SUCCESS";

	}

	public String executeProfiles() {
		System.out.println("connected");
		//
		setTabId("AuditCheck");
		setAuditSubTabId("Profile");
		String dbName = "";
		String tempdb = "";
		populateLogList = new ArrayList<DataDTO>();
		populateDBList = new ArrayList<UserDTO>();
		HttpSession sessionku = getServletRequest().getSession();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		try {
			treeMapList = this.reportManager.getRootLogReports();
			treeProfile = this.reportManager.getDbProfiles();
			System.out.println("hostId:"
					+ getServletRequest().getParameter("hostId"));
			System.out.println("profile:"
					+ getServletRequest().getParameter("profile"));
			if (getServletRequest().getParameter("hostId") != null) {
				if (getServletRequest().getParameter("dbId") != null) {
					populateLogList = this.fileManager.getProfileDetails(
							Long.parseLong(getServletRequest().getParameter(
									"hostId")),
							getServletRequest().getParameter("profile"));
				} else {
					populateLogList = this.fileManager.getProfileDetails(
							Long.parseLong(getServletRequest().getParameter(
									"hostId")));
				}

			} else {
				populateLogList = this.fileManager.getProfileDetails();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}

	public String executeAuditIndexes() {
		//System.out.println("executeAuditIndexes");
		setTabId("AuditCheck");
		setAuditSubTabId("Index");
		String dbName = "";
		String tempdb = "";
		HttpSession sessionku = getServletRequest().getSession();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		populateLogList = new ArrayList<DataDTO>();
		populateDBList = new ArrayList<UserDTO>();
		try {
			treeMapList = this.reportManager.getRootLogReports();
			treeProfile = this.reportManager.getDbProfiles();
			
			if (getServletRequest().getParameter("hostId") != null) {
				//System.out.println("Place 1");
				if (getServletRequest().getParameter("profile") != null
						|| getServletRequest().getParameter("dbId") != null) {
					//System.out.println("Place 2");
					populateLogList = this.fileManager.getIndexDetails(
							Long.parseLong(getServletRequest().getParameter(
									"hostId")),
							Long.parseLong(getServletRequest().getParameter(
									"dbId")));
				} else {
					System.out.println("Place 3"
							+ getServletRequest().getParameter("hostId") );
					populateLogList = this.fileManager.getIndexDetails(
							Long.parseLong(getServletRequest().getParameter(
									"hostId")));
					// System.out.println("Place 4"+populateLogList);
				}

			} else {
				//System.out.println("Place 4");
				populateLogList = this.fileManager.getIndexDetails();
			}

		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}

	public String executeDRP() {
		HttpSession sessionku = getServletRequest().getSession();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		try {
			treeMapList = this.reportManager.getRootLogReports();
			treeProfile = this.reportManager.getDbProfiles();
			populateLogList = new ArrayList<DataDTO>();
			
			String profile=getServletRequest().getParameter("profile");
			String hostId=getServletRequest().getParameter("hostId");
			
			if (!isNullOrEmpty(profile) && !isNullOrEmpty(hostId)) {
				populateLogList = this.dataManager.getFileDetailsWithProfile(profile,new Long(hostId));
			} else {
				populateLogList = this.dataManager.getFileDetails();
			}
			setTabId("AuditCheck");
			setAuditSubTabId("DRP");
		} catch (Exception e) {

		}
		return "SUCCESS";
	}

	public String executeDRD() {
		HttpSession sessionku = getServletRequest().getSession();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		try {
			treeMapList = this.reportManager.getRootLogReports();
			// Map<Long, DataDTO> map =
			// this.reportManager.getLogProfiles(realPath);
			// if(!map.isEmpty())
			// {
			treeProfile = this.reportManager.getDbProfiles();
			// }
			populateLogList = new ArrayList<DataDTO>();
			
			String profile=getServletRequest().getParameter("profile");
			String hostId=getServletRequest().getParameter("hostId");
			
			
			if (!isNullOrEmpty(profile)&&!isNullOrEmpty(hostId)) {
				populateLogList = this.dataManager.getDOCFileDetailsWithProfile(profile,new Long(hostId));
			} else {
				populateLogList = this.dataManager.getDOCFileDetails();
			}
			setTabId("AuditCheck");
			setAuditSubTabId("DRD");
		} catch (Exception e) {

		}
		return "SUCCESS";
	}

	public String AuditResources() {
		HttpSession sessionku = getServletRequest().getSession();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		String profile=(String)getServletRequest().getParameter("profile");
		String hostId=(String)getServletRequest().getParameter("hostId");
		System.out.println("profile="+profile);
		System.out.println("Host id="+hostId);
		try {
			treeMapList = this.reportManager.getRootLogReports();
			// Map<Long, DataDTO> map =
			// this.reportManager.getLogProfiles(realPath);
			// if(!map.isEmpty())
			// {
			treeProfile = this.reportManager.getDbProfiles();
			// }
			populateLogList = new ArrayList<DataDTO>();
			// if(getServletRequest().getParameter("dbId")!=null)
			// populateLogList =
			// this.reportManager.getLogFileResources(Long.parseLong(getServletRequest().getParameter("logFileId")),realPath);
			// else
			
			if(!isNullOrEmpty(profile)&&!isNullOrEmpty(hostId))
			{//System.out.println("inside");
				Long hostIdl=Long.parseLong(hostId);
				populateLogList = this.reportManager.getResourcesIntegrityDetailsByProfile(profile, hostIdl);
			//	public List<DataDTO> getlogFileResourcesByDBIdAndHostId(Long hostId,Long dbId,String realPath)
				
			}else
			{
				//System.out.println("outside");
		//	populateLogList = this.reportManager.getLogFileResources(realPath);
				populateLogList=this.reportManager.getResourcesIntegrityDetails();
			}
			
			setTabId("AuditCheck");
			setAuditSubTabId("Resources");
		} catch (Exception e) {

		}
		return "SUCCESS";
	}

	public String execute1() {
		Long UserId = 0L;
		HttpSession sessionku = getServletRequest().getSession();

		populateLogList = new ArrayList();
		populateFreqList = new ArrayList();
		if (sessionku.getAttribute("UserName") == null) {
			return "TIMEOUT";
		}
		if (getSession().get("UserID") != null) {
			UserId = Long.parseLong(getSession().get("UserID").toString());

			//System.out.println("UserID:search1" + UserId);

		}
		try {
			treeMapList = this.reportManager.getRootLogReports();
			treeProfile = this.reportManager.getDbProfiles();
			if (getServletRequest().getParameter("name") == null
					&& getServletRequest().getParameter("hostId") != null) {
				List logReports = (ArrayList) this.reportManager
						.getLogFileReports(Long.parseLong(getServletRequest()
								.getParameter("hostId")));
				int fileSize = 0;
				for (Iterator iter = logReports.iterator(); iter.hasNext();) {
					DataDTO DataDTO = (DataDTO) iter.next();
					// DataDTO dDao = new DataDTO();

					// List logErrors = dao.getLogFileErrors(logfileid);
					// fileSize = logErrors.size();
					// DataDTO.setErrCount(fileSize);

					int logResources = this.reportManager.getLogCountResources(
							DataDTO.getLogFileId());
					DataDTO.setResourceCount(logResources);
					// List logIndexes = dao.getLogFileIndexes(logfileid);
					// DataDTO.setIndexCount(logIndexes.size());
					populateLogList.add(DataDTO);
				}
				if (getServletRequest().getParameter("logFileId") != null
						&& getServletRequest().getParameter("type") != null) {
					String xmlData = "";
					String name = "";
					if (getServletRequest().getParameter("type")
							.equalsIgnoreCase("resources")) {
						List list = this.reportManager.getLogFileResources(Long
								.parseLong(getServletRequest().getParameter(
										"logFileId")));
						xmlData = "Resource Name | Size\n";
						for (Iterator iter = list.iterator(); iter.hasNext();) {
							DataDTO dDao = (DataDTO) iter.next();
							System.out
									.println("getResourceId::"
											+ dDao.getResourceId()
											+ dDao.getResWrote());
							if (dDao.getResourceId() != 0) {
								xmlData += dDao.getResWrote() + "   "
										+ dDao.getResSize() + "\n";
								//System.out.println("xmlData::" + xmlData);
							}
						}
						name = "Resources";
					}
					if (getServletRequest().getParameter("type")
							.equalsIgnoreCase("errors")) {
						populateFreqList = this.reportManager.getLogFileErrors(
								Long.parseLong(getServletRequest()
										.getParameter("logFileId")));
						for (Iterator iter = populateFreqList.iterator(); iter
								.hasNext();) {
							DataDTO DataDTO = (DataDTO) iter.next();
							if (DataDTO.getErrorId() != 0) {
								xmlData += DataDTO.getErrCode() + " "
										+ DataDTO.getErrDesc() + "\n";
							}
						}
						name = "Errors";
					}
					String xml = addXMLProlog(xmlData, name);
					servletResponse.setContentType("text/xml");
					PrintWriter out = servletResponse.getWriter();
					//System.out.println("xmlData--->" + xml);
					out.println(xml);
					return null;
				}
			} else if (getServletRequest().getParameter("name") != null) {
				String compressingType = getServletRequest().getParameter(
						"name");
				System.out.println("logFileId: "
						+ servletRequest.getAttribute("logFileId"));
				// System.out.println("linkType in java: " + linkType);
				// populateFreqList = new ArrayList();

				List logReports = (ArrayList) this.reportManager
						.getLogFileReports(compressingType, Long
								.parseLong(getServletRequest().getParameter(
										"hostId")));
				int fileSize = 0;
				for (Iterator iter = logReports.iterator(); iter.hasNext();) {
					DataDTO DataDTO = (DataDTO) iter.next();
					// DataDTO dDao = new DataDTO();

					// List logErrors = dao.getLogFileErrors(logfileid);
					// fileSize = logErrors.size();
					// DataDTO.setErrCount(fileSize);

					int logResources = this.reportManager.getLogCountResources(
							DataDTO.getLogFileId());
					DataDTO.setResourceCount(logResources);
					// List logIndexes = dao.getLogFileIndexes(logfileid);
					// DataDTO.setIndexCount(logIndexes.size());
					populateLogList.add(DataDTO);
				}
				if (getServletRequest().getParameter("logFileId") != null
						&& getServletRequest().getParameter("type") != null) {
					String xmlData = "";
					String name = "";
					if (getServletRequest().getParameter("type")
							.equalsIgnoreCase("resources")) {
						populateFreqList = this.reportManager
								.getLogFileResources(Long
										.parseLong(getServletRequest()
												.getParameter("logFileId")));
						name = "Resources";
					}
					if (getServletRequest().getParameter("type")
							.equalsIgnoreCase("errors")) {
						populateFreqList = this.reportManager.getLogFileErrors(
								Long.parseLong(getServletRequest()
										.getParameter("logFileId")));
						name = "Errors";
					}
					String xml = addXMLProlog(xmlData, name);
					servletResponse.setContentType("text/xml");
					PrintWriter out = servletResponse.getWriter();
					//System.out.println("xmlData--->" + xml);
					out.println(xml);
					return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}

	public static String addXMLProlog(String xmlDATA, String name) {
		// xmlDATA = XML_PROLOG + STORE_FRONT + xmlDATA + STORE_FRONT_CLOSE;
		StringBuffer xmlBuff = new StringBuffer();
		xmlBuff.append(XML_PROLOG);
		xmlBuff.append(STORE_FRONT);
		xmlBuff.append("<" + name + ">");
		xmlBuff.append(xmlDATA);
		xmlBuff.append("</" + name + ">");
		xmlBuff.append(STORE_FRONT_CLOSE);
		return xmlBuff.toString();
	}

	
	public void setVaultPath(String vaultPath) {
		this.vaultPath = vaultPath;
	}

	public String getVaultPath() {
		return vaultPath;
	}
}
