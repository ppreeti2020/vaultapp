package com.pb.filter;

import java.text.Normalizer;
import java.util.*;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class XSSRequestWrapperNew extends HttpServletRequestWrapper {
	

	private Map<String, String[]> sanitizedQueryString;
	
	public XSSRequestWrapperNew(HttpServletRequest request) {
		super(request);
	}
	@Override
	public String getHeader(String name){
		if(super.getHeader(name)!=null)
			return stripXSS(super.getHeader(name));
		return null;
	}
	@Override
	public String getContextPath(){
		if(super.getContextPath()!=null)
			return stripXSS(super.getContextPath());
		return null;
	}
	@Override
	public String getQueryString(){
		if(super.getQueryString()!=null)
			return stripXSS(super.getQueryString());
		return null;
	}
	@Override
	public String getRequestURI(){
		if(super.getRequestURI()!=null)
			return stripXSS(super.getRequestURI());
		return null;
	}
	@Override
	public StringBuffer getRequestURL(){
		if(super.getRequestURL()!=null)
			return new StringBuffer(stripXSS(super.getRequestURL().toString()));
		return null;
	}
	//QueryString overrides
	@Override
	public String getParameter(String name) {
		String parameter = null;
		String[] vals = getParameterMap().get(name); 
		
		if (vals != null && vals.length > 0) {
			parameter = vals[0];
		}
		
		return parameter;
	}

	@Override
	public String[] getParameterValues(String name) {
		return getParameterMap().get(name);
	}
	
	@Override
	public Enumeration<String> getParameterNames() {
		return Collections.enumeration(getParameterMap().keySet());
	}

	//@SuppressWarnings("unchecked")
	@Override
	public Map<String,String[]> getParameterMap() {
		
		//System.out.println("Sanitized query string"+sanitizedQueryString);
		
		if(sanitizedQueryString == null) {
			Map<String, String[]> res = new HashMap<String, String[]>();
			Map<String, String[]> originalQueryString = super.getParameterMap();
			if(originalQueryString!=null) {
				for (String key : (Set<String>) originalQueryString.keySet()) {
					
					String[] rawVals = originalQueryString.get(key);
					String[] snzVals = new String[rawVals.length];
					for (int i=0; i < rawVals.length; i++) {
						snzVals[i] = stripXSS(rawVals[i]);
					}
					
					
					String skey=stripXSS(key);
					res.put(skey, snzVals);
				}
			}
			sanitizedQueryString = res;
		}
		//System.out.println("after "+sanitizedQueryString);
		return sanitizedQueryString;
	}

	//TODO: Implement support for headers and cookies (override getHeaders and getCookies)
	
	/**
	 * Removes all the potentially malicious characters from a string
	 * @param value the raw string
	 * @return the sanitized string
	 */
	private String stripXSS(String value) {
		String cleanValue = null;
		if (value != null) {
			cleanValue = Normalizer.normalize(value, Normalizer.Form.NFD);

			// Avoid null characters
			cleanValue = cleanValue.replaceAll("\0", "");
			
			// Avoid anything between script tags
			Pattern scriptPattern = Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	 
			// Avoid anything in a src='...' type of expression
			scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");

			scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
			
			// Remove any lonesome </script> tag
			scriptPattern = Pattern.compile("</script>", Pattern.CASE_INSENSITIVE);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");

			// Remove any lonesome <script ...> tag
			scriptPattern = Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");

			// Avoid eval(...) expressions
			scriptPattern = Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
			
			// Avoid expression(...) expressions
			scriptPattern = Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
			
			// Avoid javascript:... expressions
			scriptPattern = Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
			
			// Avoid vbscript:... expressions
			scriptPattern = Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
			
			// Avoid onload= expressions
			scriptPattern = Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
			
			// Remove any lonesome > char
			scriptPattern = Pattern.compile(">", Pattern.CASE_INSENSITIVE);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
			
			// Remove any lonesome < char
			scriptPattern = Pattern.compile("<", Pattern.CASE_INSENSITIVE);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
						
			/*// Remove any lonesome " char
			scriptPattern = Pattern.compile("\"", Pattern.CASE_INSENSITIVE);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
						
			// Remove any lonesome ' char
			scriptPattern = Pattern.compile("'", Pattern.CASE_INSENSITIVE);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");*/
			
			// Remove any lonesome % char
			/*scriptPattern = Pattern.compile("%", Pattern.CASE_INSENSITIVE);
			cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");*/
		}
		
		return cleanValue;
	}

	
	
}
