package com.pb.filter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pb.filter.XSSRequestWrapper;

public class XSSFilter implements Filter {
	 
  //  @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
 
   // @Override
    public void destroy() {
    }
 
  //  @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
    	
    	/* Added as part of AVA-12.1, 12.2, 12.3, 12.4 and SCT M-02 fixes for VaultApp application*/
	    if((((HttpServletRequest) request).getSession()!=null || ((HttpServletRequest) request).getRequestURI().contains("Login")) && !(((HttpServletRequest) request).getRequestURI().contains("Render"))){
				String sessionid=((HttpServletRequest) request).getSession().getId();
				SimpleDateFormat COOKIE_EXPIRES_HEADER_FORMAT=new SimpleDateFormat("EEE, dd-MM-yyyy HH:mm:ss zzz");
				COOKIE_EXPIRES_HEADER_FORMAT.setTimeZone(new SimpleTimeZone(0,"GMT"));
				Date d=new Date();
				d.setTime(d.getTime() + 900 * 1000); //15 mins
				String cookieLifeTime=COOKIE_EXPIRES_HEADER_FORMAT.format(d);
				((HttpServletResponse) response).setHeader("SET-COOKIE", "Token="+sessionid+";Expires="+cookieLifeTime+"Secure;HttpOnly");
				
				/* Nithu Alexander: 18th Jun 2015.
				 * Below changes added as part of AVA-11.2 Security Issue fix
				 * */
				((HttpServletResponse) response).setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
				((HttpServletResponse) response).setHeader("Pragma" , "no-cache");
				((HttpServletResponse) response).setDateHeader("Expires",0);
			}
    	
    	
    	chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), (HttpServletResponse)response);
    }
 
}

