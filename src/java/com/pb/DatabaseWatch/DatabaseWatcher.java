package com.pb.DatabaseWatch;

import java.io.File;
import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import com.g1.e2.vault.SearchIndex;
import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Database;
import com.pb.admin.Common;
import com.pb.common.AeSimpleSHA1;
import com.pb.common.CommonConstants;
import com.pb.common.INIFile;
import com.pb.common.ProfileIniParser;
import com.pb.common.PropertyUtils;
import com.pb.dao.AdminReportDAO;
import com.pb.dao.DBConnectionDAO;
import com.pb.dao.DataDAO;
import com.pb.dao.DataIntegrityDAO;
import com.pb.dao.FileInfoDAO;
import com.pb.dao.LoginDAO;
import com.pb.dao.SearchDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.DataDTO;
import com.pb.dto.FileDTO;
import com.pb.dto.UserDTO;
import com.pb.manager.AdminReportManager;
import com.pb.manager.DataIntegrityManager;
import com.pb.manager.FileInfoManager;
import com.pb.manager.UserManager;
import com.pb.ocbc.SearchAccount;


public class DatabaseWatcher {
	static final Logger logger = Logger.getLogger(DatabaseWatcher.class);
	private static final int ArrayList = 0;
	DBConnectionDAO db = new DBConnectionDAO();
	AeSimpleSHA1 sha = new AeSimpleSHA1();
	String oldHash = "";
	String newHash = "";
	LoginDAO logDAO = new LoginDAO();
	AdminReportDAO adminDAO = new AdminReportDAO();
	SearchDAO serDAO = new SearchDAO();
	UserDAO userDAO = new UserDAO();
	private DataIntegrityDAO dataManager = new DataIntegrityDAO();
	private UserDAO userManager = new UserDAO();
	private AdminReportDAO reportManager = new AdminReportDAO();
	private FileInfoDAO fileManager = new FileInfoDAO();

	public void LoginTableIntegrityCheck() {

		Long logId = 0L;
		String groupName = "";
		String loginName = "";
		String loginDate = "";
		String IP = "";
		String browser = "";
		String success = "";

		try {
			System.out.println("LoginTableIntegrityCheck");
			logger.info("LoginTableIntegrityCheck");
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			/*
			 * List loginDet = adminDAO.getLoginReports(); Iterator logIter =
			 * loginDet.iterator(); while(logIter.hasNext()) { CommonDAO cDAO =
			 * (CommonDAO)logIter.next(); oldHash = cDAO.get; newHash =
			 * sha.SHA1(
			 * groupName+","+loginName+","+loginDate+","+IP+","+browser+
			 * ","+success);
			 * 
			 * 
			 * }
			 */

			Connection conn = db.getDBConnection();
			String query = "select LOG_ID,Group_Name,Login_Name,Login_Date,IP,Browser,SUCCESS_FLAG,HASH_ID from LOGIN_DETAILS";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				logId = rs.getLong(1);
				groupName = rs.getString(2);
				loginName = rs.getString(3);
				loginDate = rs.getString(4);
				IP = rs.getString(5);
				browser = rs.getString(6);
				success = rs.getString(7);
				oldHash = rs.getString(8);
				newHash = sha.SHA1(groupName + "," + loginName + ","
						+ loginDate + "," + IP + "," + browser + "," + success);

				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from LOGIN_TAB_INTEGRITY_CHECK where LOG_ID = "+logId;
				String hash = userDAO.getIntegrity(
						CommonConstants.LOGIN_TAB_INTEGRITY_CHECK, "LOG_ID",
						oldHash, logId);

				if (!oldHash.equals(newHash)) {
					logger.info("LoginTableIntegrityCheck:checking hash --> not match");
					logDAO.saveLogIntegrity(
							CommonConstants.LOGIN_TAB_INTEGRITY_CHECK,
							"LOG_ID", newHash, oldHash, logId, "no");

				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void LogoutTableIntegrityCheck() {
		Long logId = 0L;
		Long logoutId = 0L;
		String groupName = "";
		String loginName = "";
		String logoutDate = "";
		String logout = "";
		String browser = "";
		String success = "";

		try {
			System.out.println("LogoutTableIntegrityCheck");
			logger.info("LogoutTableIntegrityCheck");
			Connection conn = db.getDBConnection();
			String query = "select l.Log_Id,l1.logout_id,l.Group_Name,l.Login_Name,l1.LOGOUT,l1.LOGOUT_DATE,l1.LOGOUT_TIME,l1.HASH_ID from LOGIN_DETAILS l,LOGOUT_DETAILS l1 where l.LOG_ID = l1.LOG_ID";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			while (rs.next()) {
				logId = rs.getLong(1);
				logoutId = rs.getLong(2);
				groupName = rs.getString(3);
				loginName = rs.getString(4);

				logout = rs.getString(5);
				logoutDate = rs.getString(6);

				oldHash = rs.getString(8);
				newHash = sha.SHA1(logout + "," + logoutDate);

				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from LOGOUT_TAB_INTEGRITY_CHECK where LOGOUT_ID = "+logoutId;
				String hash = userDAO.getIntegrity(
						CommonConstants.LOGOUT_TAB_INTEGRITY_CHECK,
						"LOGOUT_ID", oldHash, logoutId);

				if (!oldHash.equals(hash)) {
					logger.info("LogoutTableIntegrityCheck checking hash-->not match");
					logDAO.saveLogIntegrity(
							CommonConstants.LOGOUT_TAB_INTEGRITY_CHECK,
							"LOGOUT_ID", newHash, oldHash, logoutId, "no");

				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void SearcDetTableIntegrityCheck() {
		Long logId = 0L;
		Long serId = 0L;
		String groupName = "";
		String loginName = "";
		String loginDate = "";
		String searchValue = "";
		String resCount = "";
		String searchDate = "";

		try {
			logger.info("SearcDetTableIntegrityCheck checking hash");
			System.out.println("SearcDetTableIntegrityCheck");
			Connection conn = db.getDBConnection();
			String query = "select l.LOG_ID,s.SER_ID,l.Group_Name,l.Login_Name,s.Res_Count,s.Search_Value,s.Search_Date,s.HASH_ID from LOGIN_DETAILS as l,SEARCH_DETAILS as s where l.LOG_ID = s.LOG_ID";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			while (rs.next()) {
				logId = rs.getLong(1);
				serId = rs.getLong(2);
				groupName = rs.getString(3);
				loginName = rs.getString(4);
				resCount = rs.getString(5);
				searchValue = rs.getString(6);
				searchDate = rs.getString(7);

				oldHash = rs.getString(8);
				newHash = sha.SHA1(searchValue + "," + resCount + ","
						+ searchDate);
				// newHash =
				// sha.SHA1(groupName+","+loginName+","+loginDate+","+IP+","+browser+","+success);

				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from SEARCH_RES_INTEGRITY_CHECK where SER_ID ="+serId;
				String hash = userDAO.getIntegrity(
						CommonConstants.SEARCH_DET_INTEGRITY_CHECK, "SER_ID",
						oldHash, serId);

				if (!oldHash.equals(newHash)) {
					logger.info("SearcDetTableIntegrityCheck checking hash-->not match");
					serDAO.saveSearchIntegrity(
							CommonConstants.SEARCH_DET_INTEGRITY_CHECK,
							"SER_ID", newHash, oldHash, serId, "no");

				}

				// rs1.getShort(columnIndex)

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void AccountTableIntegrityCheck() {
		Long accId = 0L;
		Long serId = 0L;

		String dbName = "";
		String nricNo = "";
		String docCount = "";
		String custName = "";
		String accDate = "";
		try {
			System.out.println("AccountTableIntegrityCheck");
			logger.info("AccountTableIntegrityCheck checking hash");
			Connection conn = db.getDBConnection();
			String query = "select a.ACC_ID,s.SER_ID,a.DB_NAME,a.NRIC_NO,a.Doc_Count,a.Cust_Name,a.ACC_Date,a.HASH_ID from ACCOUNT_DETAILS a, SEARCH_DETAILS s where s.SER_ID = a.SER_ID";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			while (rs.next()) {

				accId = rs.getLong(1);
				serId = rs.getLong(2);
				dbName = rs.getString(3);
				nricNo = rs.getString(4);
				docCount = rs.getString(5);
				custName = rs.getString(6);
				accDate = rs.getString(7);
				oldHash = rs.getString(8);
				// newHash = sha.SHA1(searchResults);
				newHash = sha.SHA1(dbName + "," + nricNo + "," + docCount + ","
						+ custName + "," + accDate);
				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from ACCOUNT_INTEGRITY_CHECK where ACC_ID ="+accId;
				// String hash =
				// userDAO.getIntegrity(CommonConstants.ACCOUNT_INTEGRITY_CHECK,
				// "ACC_ID", oldHash, accId);

				if (!oldHash.equals(newHash)) {
					logger.info("AccountTableIntegrityCheck checking hash-->not match");
					serDAO.saveSearchIntegrity(
							CommonConstants.ACCOUNT_INTEGRITY_CHECK, "ACC_ID",
							newHash, oldHash, accId, "no");

				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void SearcResTableIntegrityCheck() {
		Long sresId = 0L;
		Long serId = 0L;

		String searchResults = "";
		String resCount = "";
		String searchDate = "";

		try {
			logger.info("SearcResTableIntegrityCheck checking hash");
			Connection conn = db.getDBConnection();
			String query = "select s.SER_ID,r.SRES_ID,r.Search_Results,r.HASH_ID from SEARCH_RESULTS r,SEARCH_DETAILS s where s.SER_ID = r.SER_ID";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			while (rs.next()) {

				serId = rs.getLong(1);
				sresId = rs.getLong(2);
				searchResults = rs.getString(3);

				oldHash = rs.getString(4);
				newHash = sha.SHA1(searchResults);
				// newHash =
				// sha.SHA1(groupName+","+loginName+","+loginDate+","+IP+","+browser+","+success);

				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from SEARCH_RES_INTEGRITY_CHECK where SRES_ID ="+serId;
				// String hash =
				// userDAO.getIntegrity(CommonConstants.SEARCH_RES_INTEGRITY_CHECK,
				// "SRES_ID", oldHash, sresId);

				if (!oldHash.equals(newHash)) {
					logger.info("SearcResTableIntegrityCheck checking hash-->not match");
					serDAO.saveSearchIntegrity(
							CommonConstants.SEARCH_RES_INTEGRITY_CHECK,
							"SRES_ID", newHash, oldHash, sresId, "no");

				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void DocumentTableIntegrityCheck() {
		Long accId = 0L;
		Long docId = 0L;

		String docName = "";
		String description = "";
		String contType = "";
		String docDate = "";
		// String accDate = "";
		try {
			logger.info("DocumentTableIntegrityCheck checking hash");
			Connection conn = db.getDBConnection();
			String query = "select a.ACC_ID,d.DOC_ID,d.DOC_Name,d.Description,d.Doc_Gen_Date,d.Content_Type,d.HASH_ID from DOCUMENT_DETAILS d ,ACCOUNT_DETAILS a where a.ACC_ID = d.ACC_ID";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			while (rs.next()) {

				accId = rs.getLong(1);
				docId = rs.getLong(2);
				docName = rs.getString(3);
				description = rs.getString(4);
				contType = rs.getString(5);
				docDate = rs.getString(6);

				oldHash = rs.getString(7);
				newHash = sha.SHA1(docName + "," + description + "," + contType
						+ "," + docDate);
				// newHash =
				// sha.SHA1(dbName+","+nricNo+","+docCount+","+custName+","+accDate);
				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from DOCUMENT_INTE_DETAILS_CHECK where DOC_ID ="+docId;
				String hash = userDAO.getIntegrity(
						CommonConstants.DOCUMENT_INTE_DETAILS_CHECK, "DOC_ID",
						oldHash, docId);

				if (!oldHash.equals(hash)) {
					logger.info("DocumentTableIntegrityCheck checking hash-->not match");
					serDAO.saveSearchIntegrity(
							CommonConstants.DOCUMENT_INTE_DETAILS_CHECK,
							"DOC_ID", newHash, oldHash, docId, "no");

				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void DocumentActTableIntegrityCheck() {
		Long docactId = 0L;
		Long docId = 0L;

		String action = "";

		// String accDate = "";
		try {
			logger.info("DocumentActTableIntegrityCheck checking hash");
			Connection conn = db.getDBConnection();
			String query = "select d.DOC_ID,a.DOC_ACT_ID,a.DOC_ACTION,a.HASH_ID from DOCUMENT_DETAILS d,DOCUMENT_ACTION a where d.DOC_ID = a.DOC_ID";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			while (rs.next()) {

				docId = rs.getLong(1);
				docactId = rs.getLong(2);
				action = rs.getString(3);

				oldHash = rs.getString(4);
				newHash = sha.SHA1(action);
				// newHash =
				// sha.SHA1(dbName+","+nricNo+","+docCount+","+custName+","+accDate);
				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from DOC_INTEGRITY_CHECK where DOC_ACT_ID ="+docactId;
				String hash = userDAO.getIntegrity(
						CommonConstants.DOC_INTEGRITY_CHECK, "DOC_ACT_ID",
						oldHash, docactId);

				if (!oldHash.equals(hash)) {
					logger.info("DocumentActTableIntegrityCheck checking hash-->not match");
					serDAO.saveSearchIntegrity(
							CommonConstants.DOC_INTEGRITY_CHECK, "DOC_ACT_ID",
							newHash, oldHash, docactId, "no");

				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void DocumentRenderTableIntegrityCheck() {
		Long actId = 0L;
		Long docId = 0L;

		String action = "";
		String nric = "";
		String docType = "";
		String pageNo = "";
		String ActDate = "";
		try {
			logger.info("DocumentRenderTableIntegrityCheck checking hash");
			Connection conn = db.getDBConnection();
			String query = "select d.DOC_ID,dr.ACT_ID,dr.ACTION,dr.NRIC_NO,dr.Doc_Type,dr.Page_NO,dr.ACT_DATE,dr.HASH_ID from DOCUMENT_RENDER_DETAILS dr, DOCUMENT_DETAILS d where d.DOC_ID = dr.DOC_ID";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			while (rs.next()) {

				docId = rs.getLong(1);
				actId = rs.getLong(2);
				action = rs.getString(3);
				nric = rs.getString(4);
				docType = rs.getString(5);
				pageNo = rs.getString(6);
				ActDate = rs.getString(7);

				oldHash = rs.getString(8);
				// newHash = sha.SHA1(action);
				newHash = sha.SHA1(action + "," + nric + "," + docType + ","
						+ pageNo + "," + ActDate);
				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from DOCUMENT_RENDER_INTE_DETAILS where ACT_ID ="+actId;
				String hash = userDAO.getIntegrity(
						CommonConstants.DOCUMENT_RENDER_INTE_DETAILS, "ACT_ID",
						oldHash, actId);

				if (!oldHash.equals(hash)) {
					logger.info("DocumentRenderTableIntegrityCheck checking hash-->not match");
					serDAO.saveSearchIntegrity(
							CommonConstants.DOCUMENT_RENDER_INTE_DETAILS,
							"ACT_ID", newHash, oldHash, actId, "no");

				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void UserMgmtTableIntegrityCheck() {
		Long userId = 0L;
		Long usergrpId = 0L;
		String userName = "";
		String userGrp = "";
		String active = "";
		String currYear = "";
		String accessdateFrom = "";
		String accessdateTo = "";
		String accessFromDays = "";
		String accessToDays = "";
		String crDate = "";

		try {
			System.out.println("UserMgmtTableIntegrityCheck checking hash");
			logger.info("UserMgmtTableIntegrityCheck checking hash");
			Connection conn = db.getDBConnection();
			String query = "select u.USER_ID,ug.USER_GRP_ID,u.USER_NAME,u.CURRENT_YEAR,u.ACCESS_DAYS_FROM, u.ACCESS_DAYS_TO, u.ACCESS_Date_FROM, u.ACCESS_DATE_TO, ug.USER_GROUP,u.ACTIVE_FLAG,u.CREATE_DATE,u.HASH_ID from  USER_MGMT u, USER_GROUPS ug where u.USER_GRP_ID = ug.USER_GRP_ID";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			while (rs.next()) {

				userId = rs.getLong(1);

				usergrpId = rs.getLong(2);
				userName = rs.getString(3);
				currYear = rs.getString(4);
				accessdateFrom = rs.getString(5);
				accessdateTo = rs.getString(6);
				accessFromDays = rs.getString(7);
				accessToDays = rs.getString(8);

				userGrp = rs.getString(9);
				active = rs.getString(10);
				crDate = rs.getString(11);
				oldHash = rs.getString(12);
				// newHash = sha.SHA1(action);
				newHash = sha.SHA1(userGrp + "," + active + "," + currYear
						+ "," + accessdateFrom + "," + accessdateTo + ","
						+ accessFromDays + "," + accessToDays + "," + crDate);
				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from USER_INTEGRITY_CHECK where USER_ID ="+userId;
				String hash = userDAO.getIntegrity(
						CommonConstants.USER_INTEGRITY_CHECK, "USER_ID",
						oldHash, userId);
				if (oldHash != null) {
					if (!oldHash.equals(hash)) {
						logger.info("UserMgmtTableIntegrityCheck checking hash--> not match");
						userDAO.saveUserIntegrity(
								CommonConstants.USER_INTEGRITY_CHECK,
								"USER_ID", newHash, oldHash, userId, "no");

					}
				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void UserRoleTableIntegrityCheck() {
		Long userId = 0L;
		Long user_role_Id = 0L;
		Long roleId = 0L;
		String flag = "";
		try {
			System.out.println("UserRoleTableIntegrityCheck checking hash");
			logger.info("UserRoleTableIntegrityCheck checking hash");
			Connection conn = db.getDBConnection();
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String query = "select u.USER_ID,ur.USER_ROLE_ID,ur.ROLE_ID,ur.HASH_ID,ur.DELETE_FLAG from USER_MGMT u, User_ROLES ur where u.USER_ID = ur.USER_ID";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {

				userId = rs.getLong(1);
				user_role_Id = rs.getLong(2);
				roleId = rs.getLong(3);

				oldHash = rs.getString(4);
				flag = rs.getString(5);
				// newHash = sha.SHA1(action);
				newHash = sha.SHA1(userId + "," + roleId + "," + flag);
				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from USER_ROLE_INTEGRITY_CHECK where USER_ROLE_ID ="+user_role_Id;
				String hash = userDAO.getIntegrity(
						CommonConstants.USER_ROLE_INTEGRITY_CHECK,
						"USER_ROLE_ID", oldHash, user_role_Id);
				if (oldHash != null) {
					if (!oldHash.equals(hash)) {
						logger.info("UserRoleTableIntegrityCheck checking hash-->not match");
						userDAO.saveUserIntegrity(
								CommonConstants.USER_ROLE_INTEGRITY_CHECK,
								"USER_ROLE_ID", newHash, oldHash, user_role_Id,
						"no");

					}
				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void UserDBTableIntegrityCheck() {
		Long userId = 0L;
		Long user_db_Id = 0L;
		Long dbId = 0L;
		String flag = "";
		try {
			System.out.println("UserDBTableIntegrityCheck checking hash");
			logger.info("UserDBTableIntegrityCheck checking hash");
			Connection conn = db.getDBConnection();
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String query = "select u.USER_ID,ud.USER_DB_ID,ud.DB_id,ud.Hash_Id,ud.DELETE_FLAG from USER_MGMT u, User_DATABASE ud where u.USER_ID = ud.user_Id";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {

				userId = rs.getLong(1);
				user_db_Id = rs.getLong(2);
				dbId = rs.getLong(3);
				oldHash = rs.getString(4);
				flag = rs.getString(5);
				// newHash = sha.SHA1(action);
				newHash = sha.SHA1(userId + "," + dbId + "," + flag);
				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from USER_ROLE_INTEGRITY_CHECK where USER_DB_ID ="+user_db_Id;
				String hash = userDAO.getIntegrity(
						CommonConstants.USER_DB_INTEGRITY_CHECK, "USER_DB_ID",
						oldHash, user_db_Id);
				if (oldHash != null) {
					if (!oldHash.equals(hash)) {
						logger.info("UserDBTableIntegrityCheck checking hash-->not match");
						userDAO.saveUserIntegrity(
								CommonConstants.USER_DB_INTEGRITY_CHECK,
								"USER_DB_ID", newHash, oldHash, user_db_Id,
						"no");

					}
				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void CompressTableIntegrityCheck() {
		Long CompId = 0L;

		String fileName = "";
		String fileDate = "";
		String profile = "";
		String resource = "";
		String uncompress = "";
		String compress = "";
		String pages = "";
		FileInfoDAO fdao = new FileInfoDAO();
		try {
			System.out.println("UserDBTableIntegrityCheck checking hash");
			logger.info("UserDBTableIntegrityCheck checking hash");
			Connection conn = db.getDBConnection();
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			String query = "Select COM_FILE_ID,FILE_NAME,NO_OF_PAGES,PROFILE,RESOURCE_SET,UNCOMP_SIZE,COMP_SIZE,FILE_DATE,CHECK_DATE,BYTES_PER_PAGE,COMP_RATIO,HASH_ID from COMPRESSION_CHECK";// "INSERT INTO COMPRESSION_CHECK(FILE_NAME,NO_OF_PAGES,PROFILE,RESOURCE_SET,UNCOMP_SIZE,COMP_SIZE,FILE_DATE,CHECK_DATE,BYTES_PER_PAGE,COMP_RATIO,HASH_ID) VALUES('"+dto.getFileName()+"','"+dto.getPages()+"','"+dto.getProfile()+"','"+dto.getResourceSet()+"','"+dto.getUnCompress()+"','"+dto.getCompress()+"','"+dto.getFileDate()+"','"+s+"','"+dto.getBytesPerpage()+"','"+dto.getRatio()+"','"+dto.getHashId()+"')";
			Statement stmt = conn.createStatement();
			stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()) {
				FileDTO dto = new FileDTO();
				CompId = rs.getLong(1);
				fileName = rs.getString(2);
				pages = rs.getString(3);

				profile = rs.getString(4);
				resource = rs.getString(5);
				uncompress = rs.getString(6);
				compress = rs.getString(7);
				fileDate = rs.getString(8);

				oldHash = rs.getString(12);
				// newHash = sha.SHA1(action);
				newHash = AeSimpleSHA1.SHA1(fileName + "," + fileDate + ","
						+ profile + "," + resource + "," + uncompress + ","
						+ compress + "," + pages);

				// String query1 =
				// "select INTE_NEW_HASH_ID,INTE_OLD_HASH_ID,INTE_CHECK_DATE,CHECK_TIME,CHECK_FLAG from USER_ROLE_INTEGRITY_CHECK where USER_DB_ID ="+user_db_Id;
				String hash = userDAO.getIntegrity(
						CommonConstants.COMPRESSION_INTEGRITY_CHECK,
						"COM_FILE_ID", oldHash, CompId);
				if (oldHash != null) {
					if (!oldHash.equals(hash)) {
						logger.info("UserDBTableIntegrityCheck checking hash-->not match");
						fdao.saveFileIntegrity(
								CommonConstants.COMPRESSION_INTEGRITY_CHECK,
								"COM_FILE_ID", dto.getHashId(),
								dto.getHashId(), CompId, "no");

					}
				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void TablesIntegrityCheck() {
		try {
			System.out.println("TablesIntegrityCheck checking hash");
			logger.info("TablesIntegrityCheck checking hash");
			Connection conn = db.getDBConnection();
			Statement stmt = conn.createStatement();
			String query = "select TABLE_ID,TABLE_NAME,HASH_ID,DATE,TIME,RES_COUNT from TABLES_DETAILS";
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) {

				while (rs.next()) {
					Long comId = rs.getLong(1);
					String tableName = rs.getString(2);
					String oldHash = rs.getString(3);
					String newHash = sha.SHA1(tableName);
					String hash = userDAO.getIntegrity(
							CommonConstants.TABLES_INTEGRITY_CHECK, "TABLE_ID",
							oldHash, comId);
					if (oldHash != null) {
						if (!oldHash.equals(hash)) {
							logger.info("TablesIntegrityCheck checking hash-->not match");
							userDAO.saveUserIntegrity(
									CommonConstants.TABLES_INTEGRITY_CHECK,
									"TABLE_ID", newHash, oldHash, comId, "no");

						}
					}
				}
			}

			else {
				CreateTableIntegrity();
			}
			System.out.println("completed scheduler");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void CreateTableIntegrity() {
		try {
			System.out.println("CreateTableIntegrity checking hash");
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			String s = formatter.format(date);
			Connection conn = db.getDBConnection();

			CallableStatement proc = conn
			.prepareCall("{ call allTables_SpaceUsed() }");
			ResultSet rs = proc.executeQuery();

			while (rs.next()) {

				String tableName = rs.getString("tablename");
				String rows = rs.getString("nrows");
				String size = rs.getString("DataSize");
				System.out.println("tableName" + tableName);
				String hash = sha.SHA1(tableName);
				insertTableDetails(tableName, rows, size, s, hash);
				// System.out.println(supplier + ": " + coffee);
			}
			proc.execute();

		} catch (Exception e) {

		}
	}

	public void insertTableDetails(String tableName, String rows, String size,
			String s, String hash) {
		try {
			System.out.println(":insertTableDetails");
			Connection conn = db.getDBConnection();
			Statement stmt = conn.createStatement();
			String query = "INSERT INTO TABLES_DETAILS(TABLE_NAME,RES_COUNT,SIZE,DATE,TIME,HASH_ID) values('"
				+ tableName
				+ "','"
				+ rows
				+ "','"
				+ size
				+ "','"
				+ s
				+ "',DEFAULT,'" + hash + "');";
			stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				Long id = rs.getLong(1);
				userDAO.saveUserIntegrity(
						CommonConstants.TABLES_INTEGRITY_CHECK, "TABLE_ID",
						hash, hash, id, "no");

			}
			System.out.println("insertTableDetails: saved details");
		} catch (Exception e) {

		}
	}

	public void saveVaultDatabases() {
		boolean b = false;
		Long dbId = 0l;
		String host = "";
		int port = 0;
		String instance = "";
		String realPath = CommonConstants.CSR_PROPERTY_FILE_NAME;
		Long hostId = 0l;
		UserDAO uDao = new UserDAO();
		DataDTO dataDto = new DataDTO();
		try {

			String propert = CommonConstants.VAULT_AUDIT_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(propert);
			System.out.println("saveVaultDatabases" + properties);
			if (properties.getProperty(CommonConstants.SERVER1_HOST) != null
					|| properties.getProperty(CommonConstants.SERVER1_HOST) != "") {
				host = properties.getProperty(CommonConstants.SERVER1_HOST);
				instance = properties.getProperty(CommonConstants.SERVER1_INSTANCE_NAME);
				dataDto.setHostName(host);
				dataDto.setServerInstanceName(instance);
			}

			if (properties
					.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != null
					|| properties
					.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != "")
				port = Integer
				.parseInt(properties
						.getProperty(CommonConstants.RENDERING_SERVER1_PORT));

			hostId = this.dataManager.getLogHost(dataDto);
			if (hostId != 0) {

				System.out.println("saveVaultDatabases::hostID:"+hostId);
				VaultClient vc = Common.getVaultConnection();
				System.out.println("saveVaultDatabases::vaultConnection::"+vc);
				Set<Database> Databases = vc.getDatabases();
				System.out.println("saveVaultDatabases::Databases size::"+Databases.size());
				for (Database db1 : Databases) {
					if(!db1.getName().equalsIgnoreCase("Default Database")){
						System.out.println("saveVaultDatabases:databases"
								+ db1.getName());
						dbId = uDao.getVaultDBName(db1.getName(),hostId);
						if (dbId == 0) {
							dbId = uDao.saveDBName(db1.getName(), db1.getDescription(), hostId);
						}

						if(dbId !=0)
						{
							System.out.println("coming here:"+dbId);
							Set<SearchIndex> set = db1.getSearchIndexes();
							System.out.println("set::"+set);
							for(SearchIndex si:set)
							{
								System.out.println(si.getIndex()+"|"+si.getFields()+"|"+si.getName()+"|"+si.getDescription());

								b = uDao.checkDBIndex(dbId,si.getName());
								if(!b){
									if (!si.getName().equalsIgnoreCase(
									"guid")) {
										Long indexId = uDao.saveDBIndex(dbId, si.getName(), si.getDescription());
									}
								}
							}
							System.out.println("save vault database & saveDBIndex completed");
						}
					}
				}


			}

		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveVaultDatabases(String profileFile) {

		boolean b = false;
		Long dbId = 0l;
		String host = "";
		int port = 0;
		String instance = "";
		String realPath = CommonConstants.CSR_PROPERTY_FILE_NAME;
		Long hostId = 0l;
		DataDTO dataDto = new DataDTO();
		try {

			String propert = CommonConstants.VAULT_AUDIT_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(propert);
			System.out.println("saveVaultDatabases" + properties);
			for (int i = 1; i <= 1; i++) {
				if (i == 1) {
					if (properties.getProperty(CommonConstants.SERVER1_HOST) != null
							|| properties
							.getProperty(CommonConstants.SERVER1_HOST) != "") {
						host = properties
						.getProperty(CommonConstants.SERVER1_HOST);

						instance = properties
						.getProperty(CommonConstants.SERVER1_INSTANCE_NAME);
						dataDto.setHostName(host);
						dataDto.setServerInstanceName(instance);
					}

					if (properties
							.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != null
							|| properties
							.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != "")
						port = Integer
						.parseInt(properties
								.getProperty(CommonConstants.RENDERING_SERVER1_PORT));

				}

				hostId = this.dataManager.getLogHost(dataDto);
				if (hostId != 0) {
					// VaultClient vc =
					// Common.getVaultConnection(host,port,"server"+1);
					// Set<Database> Databases = vc.getDatabases();
					ProfileIniParser pp = new ProfileIniParser(profileFile,
							realPath);

					Set<String> Databases = pp.getDatabases();

					// vc.
					for (String db1 : Databases) {

						dbId = this.userManager.getVaultDBName(
								db1.toUpperCase(), hostId);
						if (dbId == 0) {
							dbId = this.userManager.saveDBName(
									db1.toUpperCase(), db1.toUpperCase(),
									hostId);
						}

						if (dbId != 0) {
							System.out.println("coming here:" + dbId);
							List<UserDTO> set = pp.getSearchIndexes(db1);
							System.out.println("set::" + set);
							for (UserDTO si : set) {
								// System.out.println(si.getIndex()+"|"+si.getFields()+"|"+si.getName()+"|"+si.getDescription());

								b = this.userManager.checkDBIndex(dbId,
										si.getIndexName());
								if (!b) {

									// Long indexId =
									this.userManager.saveDBIndex(dbId,
											si.getIndexName(),
											si.getIndexDesc());

								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void saveVaultProfiles() {

		UserDAO userDao = new UserDAO();
		INIFile objINI = null;
		String strFile = null;
		Long dbId = 0L;
		Long profileId = 0L;
		Long pdID = 0L;

		List<UserDTO> listDb = new ArrayList<UserDTO>();
		String host = "";
		int port = 0;
		String ip = "";
		DataDTO dataDto = new DataDTO();
		Long hostId = 0l;
		String instance = "";
		try {
			strFile = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.EVAULT_CONFIG_FILE_PATH)+"/profiles.ini";
			System.out.println("profiles.ini file path::"+strFile);
			String propert = CommonConstants.VAULT_AUDIT_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(propert);
			for (int j = 1; j <= 2; j++) {
				if (j == 1) {
					if (properties.getProperty(CommonConstants.SERVER1_HOST) != null
							|| properties
							.getProperty(CommonConstants.SERVER1_HOST) != "")
						host = properties
						.getProperty(CommonConstants.SERVER1_HOST);

					if (properties
							.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != null
							|| properties
							.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != "")
						port = Integer
						.parseInt(properties
								.getProperty(CommonConstants.RENDERING_SERVER1_PORT));
					instance = properties
					.getProperty(CommonConstants.SERVER1_INSTANCE_NAME);
				}

				else if(j==2) { if
					(properties.getProperty(CommonConstants.SERVER2_HOST) != null
							|| properties .getProperty(CommonConstants.SERVER2_HOST) !=
					"") host = properties
					.getProperty(CommonConstants.SERVER2_HOST);

				if
				(properties.getProperty(CommonConstants.RENDERING_SERVER1_PORT
				) != null || properties
				.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != "")
					port = Integer.parseInt(properties
							.getProperty(CommonConstants.RENDERING_SERVER1_PORT)); }

				dataDto.setHostName(host);
				dataDto.setServerInstanceName(instance);
				dataDto.setIpAddr(ip);

				// dataDto.setSoftwarePath(swPath);
				hostId = this.dataManager.getLogHost(dataDto);

				objINI = new INIFile(strFile);

				System.out.println("vault");
				int total = objINI.getTotalSections();
				System.out.println("Total:::"+total);
				for (int i = 0; i < total; i++) {
					System.out.println("INISections"+objINI.getAllSectionNames()[i]);
					System.out.println("INISectionsDB"+objINI.getStringProperty(objINI.getAllSectionNames()[i],"Database"));
					String db = objINI.getStringProperty(objINI.getAllSectionNames()[i],"Database");
					String profile = objINI.getAllSectionNames()[i];
					if(db != null){
						dbId = userDao.getDatabaseId(db);
						if(dbId != 0L){
							profileId = userDao.getprofileId(profile);
							if(profileId == 0){

								profileId = userDao.saveProfile(profile);
								DataDTO dao = new DataDTO();
								dao.setProfileName(profile.toUpperCase());
								dao.setDbId(dbId);
								dao.setAction("F");
								dao.setHostId(hostId);
								dao.setFileStatus("T");
								dao.setDataFileSize(0l);
								this.fileManager.saveProfileDetails(dao);
							}
							pdID = userDao.getprofileDbData(profileId, dbId);
							if(pdID == 0){
								userDao.saveProfileDbData(profileId, dbId);
							}
						}
					}
				}

			}	


		} catch (Exception e) {

		}
		objINI = null;

	}

	/*public void checkVaultDatabases(String profileFile) {

		boolean b = false;
		List<UserDTO> listDb = new ArrayList<UserDTO>();
		String host = "";
		int port = 0;
		String ip = "";
		DataDTO dataDto = new DataDTO();
		Long hostId = 0l;
		String instance = "";
		try {
			System.out.println("checkVaultDatabases");
			String realPath = CommonConstants.CSR_PROPERTY_FILE_NAME;
			String propert = realPath  + "/"
					+ CommonConstants.VAULT_AUDIT_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(propert);
			for (int i = 1; i <= 1; i++) {
				if (i == 1) {
					if (properties.getProperty(CommonConstants.SERVER1_HOST) != null
							|| properties
									.getProperty(CommonConstants.SERVER1_HOST) != "")
						host = properties
								.getProperty(CommonConstants.SERVER1_HOST);

					if (properties
							.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != null
							|| properties
									.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != "")
						port = Integer
								.parseInt(properties
										.getProperty(CommonConstants.RENDERING_SERVER1_PORT));
					instance = properties
							.getProperty(CommonConstants.SERVER1_INSTANCE_NAME);
				}
				/*
	 * else if(i==2) { if
	 * (properties.getProperty(CommonConstants.SERVER2_HOST) != null
	 * || properties .getProperty(CommonConstants.SERVER2_HOST) !=
	 * "") host = properties
	 * .getProperty(CommonConstants.SERVER2_HOST);
	 * 
	 * if
	 * (properties.getProperty(CommonConstants.RENDERING_SERVER1_PORT
	 * ) != null || properties
	 * .getProperty(CommonConstants.RENDERING_SERVER1_PORT) != "")
	 * port = Integer.parseInt(properties
	 * .getProperty(CommonConstants.RENDERING_SERVER1_PORT)); }
	 */
	/*	dataDto.setHostName(host);
				dataDto.setServerInstanceName(instance);
				dataDto.setIpAddr(ip);

				// dataDto.setSoftwarePath(swPath);
				hostId = this.dataManager.getLogHost(dataDto);
				ProfileIniParser pi = new ProfileIniParser(profileFile,
						realPath);
				// VaultClient vc =
				// Common.getVaultConnection(host,port,"server"+i);
				List<String> Databases = pi.getProfiles();
				listDb = this.userManager.getDatabases();

				for (UserDTO dto : listDb) {
					b = true;
					for (String db1 : Databases) {

						if (dto.getDbName().equalsIgnoreCase(db1)) {
							b = false;

						}
					}
					System.out.println("BBBBBBBB" + b);
					if (b) {
						DataDTO dao = new DataDTO();
						dao.setProfileName(dto.getDbName().toUpperCase());
						dao.setDbId(dto.getDbId());
						dao.setAction("F");
						dao.setHostId(hostId);
						dao.setFileStatus("T");
						dao.setDataFileSize(0l);
						List<DataDTO> list1 = this.fileManager
								.getProfileDetails(hostId, dto.getDbId());
						if (!list1.iterator().hasNext()) {
							System.out.println("NEWPROFILEADD2");
							this.fileManager.saveProfileDetails(dao);
						} else {
							DataDTO dt = list1.iterator().next();
							dao.setProfileInteId(dt.getProfileInteId());
							dao.setCreatedDate(dt.getCreatedDate());
							dao.setDataFileSize(0l);
							this.fileManager
									.updateProfileDetails(dao);
						}

					} else {
						DataDTO dao = new DataDTO();
						dao.setProfileName(dto.getDbName());
						dao.setDbId(dto.getDbId());
						dao.setAction("T");
						dao.setHostId(hostId);
						dao.setFileStatus("T");
						List<DataDTO> list1 = this.fileManager
								.getProfileDetails(hostId, dto.getDbId());
						if (!list1.iterator().hasNext()) {
							System.out.println("EXISTINGPROFILEADD2");
							this.fileManager.saveProfileDetails(dao);
						} else {
							System.out.println("EXISTINGPROFILEUPDATE");
							DataDTO dt = list1.iterator().next();
							System.out.println("ProfileId"
									+ dt.getProfileInteId()
									+ dt.getInteCheckDate());
							dao.setProfileInteId(dt.getProfileInteId());
							dao.setCreatedDate(dt.getCreatedDate());
							dao.setDataFileSize(0l);
							this.fileManager
									.updateProfileDetails(dao);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public void checkVaultIndexes() {

		boolean b = false;
		Long dbId = 0l;
		String host = "";
		int port = 0;
		String ip = "";
		// String host = "";
		int renderPort = 0;
		Long hostId = 0l;
		// String ip = "";
		int archivalPort = 0;
		int dataPort = 0;
		String path = "";
		DataDTO dataDto = new DataDTO();
		String instance = "";
		try {

			System.out.println("checkVaultIndexes");
			String propert = CommonConstants.VAULT_AUDIT_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(propert);
			InetAddress addr = InetAddress.getLocalHost();
			String hostname = addr.getHostName();
			String Ip = addr.getHostAddress();
			for (int i = 1; i <= 1; i++) {
				if (i == 1) {
					if (properties.getProperty(CommonConstants.SERVER1_HOST) != null
							&& properties
							.getProperty(CommonConstants.SERVER1_HOST) != "") {
						host = properties
						.getProperty(CommonConstants.SERVER1_HOST);
						// dataDto.setHostName(host);
					}

					if (properties
							.getProperty(CommonConstants.ARCHIVAL_SERVER1_PORT) != null
							&& properties
							.getProperty(CommonConstants.ARCHIVAL_SERVER1_PORT) != "") {
						archivalPort = Integer
						.parseInt(properties
								.getProperty(CommonConstants.ARCHIVAL_SERVER1_PORT));
					}

					if (properties
							.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != null
							&& properties
							.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != "") {
						renderPort = Integer
						.parseInt(properties
								.getProperty(CommonConstants.RENDERING_SERVER1_PORT));
					}

					if (properties
							.getProperty(CommonConstants.DATA_ROUTER_SERVER1_PORT) != null
							&& properties
							.getProperty(CommonConstants.DATA_ROUTER_SERVER1_PORT) != "") {
						dataPort = Integer
						.parseInt(properties
								.getProperty(CommonConstants.DATA_ROUTER_SERVER1_PORT));
					}

					if (properties
							.getProperty(CommonConstants.SERVER1_SOFTWARE_PATH) != null
							&& properties
							.getProperty(CommonConstants.SERVER1_SOFTWARE_PATH) != "") {
						path = properties
						.getProperty(CommonConstants.SERVER1_SOFTWARE_PATH);
					}
					instance = properties
					.getProperty(CommonConstants.SERVER1_INSTANCE_NAME);
				}
				/*
				 * else if(i==2) { if
				 * (properties.getProperty(CommonConstants.SERVER2_HOST) != null
				 * && properties.getProperty(CommonConstants.SERVER2_HOST) !=
				 * "") { host =
				 * properties.getProperty(CommonConstants.SERVER2_HOST); }
				 * 
				 * if
				 * (properties.getProperty(CommonConstants.ARCHIVAL_SERVER2_PORT
				 * ) != null &&
				 * properties.getProperty(CommonConstants.ARCHIVAL_SERVER2_PORT)
				 * != "") { archivalPort =
				 * Integer.parseInt(properties.getProperty
				 * (CommonConstants.ARCHIVAL_SERVER2_PORT)); }
				 * 
				 * if
				 * (properties.getProperty(CommonConstants.RENDERING_SERVER2_PORT
				 * ) != null &&
				 * properties.getProperty(CommonConstants.RENDERING_SERVER2_PORT
				 * ) != "") { renderPort =
				 * Integer.parseInt(properties.getProperty
				 * (CommonConstants.RENDERING_SERVER2_PORT)); } if
				 * (properties.getProperty
				 * (CommonConstants.DATA_ROUTER_SERVER2_PORT) != null &&
				 * properties
				 * .getProperty(CommonConstants.DATA_ROUTER_SERVER2_PORT) != "")
				 * { dataPort =
				 * Integer.parseInt(properties.getProperty(CommonConstants
				 * .DATA_ROUTER_SERVER2_PORT)); } if
				 * (properties.getProperty(CommonConstants
				 * .SERVER2_SOFTWARE_PATH) != null &&
				 * properties.getProperty(CommonConstants.SERVER2_SOFTWARE_PATH)
				 * != "") { path =
				 * properties.getProperty(CommonConstants.SERVER2_SOFTWARE_PATH
				 * ); } }
				 */
				dataDto.setHostName(host);
				dataDto.setServerInstanceName(instance);
				dataDto.setIpAddr(ip);
				dataDto.setSoftwarePath(path);
				dataDto.setArchivalServerPort(archivalPort);
				dataDto.setRenderServerPort(renderPort);
				dataDto.setDataRouterPort(dataPort);
				hostId = this.dataManager.getLogHost(dataDto);
				List<DataDTO> dbList = this.reportManager.getDatabase(hostId);

				/*
				 * ===========================================================
				 */
				/*
				List<DataDTO> tempList=new ArrayList<DataDTO>();

				for(DataDTO dto:dbList)
				{
					if(dto.getDbId()==1)
						tempList.add(dto);
				}
				dbList=tempList;*/


				/**
				 * ========================================================
				 */

				SimpleDateFormat formatter = new SimpleDateFormat(
						CommonConstants.DATE_FORMAT);
				Date crDate = new Date();
				DataDTO dataDao = new DataDTO();
				String fName = "";
				String finalPath = "";
				for (DataDTO dDto : dbList) {
					List<DataDTO> indexList = this.reportManager.getIndexes(
							dDto.getDbId());
					for (DataDTO dto : indexList) {
						// final Path for local
						// finalPath =
						// path+"\\index\\"+dDto.getDbName()+"\\"+dto.getIndexName()+".dri";
						finalPath = path+"\\index"+"\\"+dto.getDbName()+"\\"+dto.getIndexName()+".dri";

						System.out.println("Path is="+finalPath);

						// finalPath =
						// path+"\\index\\"+dDto.getDbName()+"\\"+dto.getIndexName()+".dri";
						dataDao.setHostId(hostId);
						dataDao.setFilePath(finalPath);
						dataDao.setIndexName(dto.getIndexName());
						dataDao.setDbId(dDto.getDbId());
						dataDao.setIndexId(dto.getIndexId());
						dataDto.setInteCheckDate(formatter.format(new Date()));
						File file = new File(finalPath);
						System.out.println("Path is" + finalPath);
						String s = formatter.format(crDate);
						if (file.exists()) {
							Long LM = file.lastModified();
							System.out.println("file:LM::" + LM);
							crDate = new Date(LM);

							System.out.println("files------>" + path);
							fName = file.getName();
							// shaRes = AeSimpleSHA1.SHA1(fName+","+s);

							// System.out.println("Sha" + shaRes);
							dataDao.setNewFileName(fName);
							dataDao.setCreatedDate(s);
							// dataD
							// dataDao.setFileAction("n");
							dataDao.setFileName(fName);
							// dataDao.setHashId(shaRes1);

							dataDao.setFilePath(finalPath);
							dataDao.setStatus("T");
							dataDto.setFileStatus("T");
							dataDao.setDataFileSize(file.length());
							System.out.println("FILESIZEIS=" + file.length());

							List<DataDTO> indexInteList = this.fileManager
							.getIndexInte(finalPath, dDto.getDbId(),
									file.length());

							List<DataDTO> indexInteList2 = this.fileManager
							.getIndexInteFromDB(finalPath,
									dDto.getDbId());

							System.out.println("IndexList"
									+ indexInteList.size() + "==" + finalPath);
							System.out.println("IndexList2"
									+ indexInteList2.size() + "==" + finalPath);
							if (indexInteList.size() != 0) {
								for (DataDTO dto1 : indexInteList) {
									dataDao.setAction("c");
									dataDao.setFileStatus("T");
									dataDao.setUpdated_dt(s);
									dataDao.setIndexInteId(dto1
											.getIndexInteId());
									// System.out.println();
									this.fileManager.updateIndexDetails(
											dataDao);
								}
							}
							/*
							 * else { dataDao.setFilePath(path);
							 * dataDao.setFileStatus("T");
							 * dataDao.setAction("T"); dataDao.setUpdated_dt(s);
							 * dataDao.setCreatedDate(s);
							 * this.fileManager.saveIndexDetails(dataDao,
							 * realPath); }
							 */
							else if (indexInteList2.size() != 0) {
								dataDao.setAction("T");
								dataDao.setFileStatus("T");
								dataDao.setUpdated_dt(s);
								dataDao.setIndexInteId(indexInteList2.get(0)
										.getIndexInteId());
								System.out.println("===========IndexList2");
								this.fileManager.updateIndexDetails(dataDao);
							} else {
								System.out.println("===========IndexList2New");
								// dataDao.setFilePath(SBggPcgaTzI=);
								dataDao.setFileStatus("T");
								dataDao.setAction("T");
								dataDao.setUpdated_dt(s);
								dataDao.setCreatedDate(s);
								this.fileManager.saveIndexDetails(dataDao);
							}

						} else {
							List<DataDTO> indexInteList2 = this.fileManager
							.getIndexInteFromDB(finalPath,
									dDto.getDbId());
							System.out.println("FILEDOESNOTEXISTINPATH"+finalPath+dDto.getDbId());
							if (indexInteList2.size() != 0) {
								dataDao.setAction("F");
								dataDao.setFileStatus("F");
								dataDao.setUpdated_dt(s);
								dataDao.setDataFileSize((long)(indexInteList2.get(0).getFileSize()));
								System.out.println(s);
								dataDao.setIndexInteId(indexInteList2.get(0)
										.getIndexInteId());
								System.out.println("=============="+indexInteList2.get(0)
										.getIndexInteId());
								System.out.println("===========IndexList2");
								this.fileManager.updateIndexDetails(dataDao);
							} else {
								System.out.println("FILE DOESNOR EXIST"+indexInteList2.size());
								dataDao.setFileStatus("F");
								dataDao.setAction("F");
								dataDao.setUpdated_dt(formatter
										.format(new Date()));
								dataDao.setCreatedDate(formatter
										.format(new Date()));
								this.fileManager.saveIndexDetails(dataDao);
							}

						}

					}
				}

			}
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

