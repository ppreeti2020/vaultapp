package com.pb.scheduler;

import java.util.Map;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class SchedulerJob implements Job {
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		try {
			Map dataMap = context.getJobDetail().getJobDataMap();
			SchedulerTask task = (SchedulerTask) dataMap.get("schedulerTask");
			task.printSchedulerMessage();
		} catch (Exception e) {
			System.out.println("scheduler problem" + e);
		}
	}
}