/*
 * Date modified: 22/05/2012
 * modified by Suhashini
 */
package com.pb.scheduler;

import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;

import org.apache.commons.collections.map.HashedMap;

import com.pb.DirectoryWatch.AbstractResourceWatcher;
import com.pb.DirectoryWatch.DirectoryWatcher;
import com.pb.DirectoryWatch.FileListener;
import com.pb.DirectoryWatch.IntervalThread;
import com.pb.common.CommonConstants;
import com.pb.common.ProfileIniParser;
import com.pb.common.PropertyUtils;
import com.pb.dao.AdminReportDAO;
import com.pb.dao.DataIntegrityDAO;
import com.pb.dto.DataDTO;
import com.pb.dto.UserDTO;
import com.pb.evault.AppServletContextListener;
import com.pb.manager.AdminReportManager;
import com.pb.manager.DataIntegrityManager;

//import com.poc.DirectoryWatch.ResourceWatcher;

/*
 * This class is for to run threads for checking e2 log files, e2 pagedata, docdata and resources
 */
public class DirectoryWatchListener //implements ServletContextListener
{
	/*public DirectoryWatcher jrndw;
	public DirectoryWatcher dw;
	public DirectoryWatcher docdw1;
	public DirectoryWatcher dw1;
	public DirectoryWatcher resource;*/

	/*public void stop() {
		System.out.println("dw before stop");
		System.out.println("jrndw::"+jrndw);
		jrndw.removeAllListeners();
	/*	dw.stop();
		docdw1.stop();
		dw1.stop();
		resource.stop();*/
	/*	System.out.println("dw after stop");
	}*/

	DataIntegrityDAO dataManager = new DataIntegrityDAO();

	AdminReportDAO adrm = new AdminReportDAO();

	private void parseLogFiles(String directory, Long hostId)throws Exception {
		System.out.println("Inside parseLogFiles> directory:"+directory+"hostId:"+hostId);
		FileMonitor fm = new FileMonitor();
		File theDirectory = new File(directory);
		File[] children = theDirectory.listFiles(new GenericExtFilter(".log"));

		// Store all the current files and their timestamps
		for (int i = 0; i < children.length; i++) {

			File file = children[i];
			/*
			 * currentFiles.put(file.getAbsolutePath(), new
			 * Long(file.lastModified()));
			 */
			if (file.getName().indexOf("e2loaderd") != -1) {
				fm.LogFileIntegrityCheck("", file.getAbsolutePath(),
						file.lastModified(), "new file", hostId);
			}
		}
	}

	public void contextInitialized() {
		ServletContext sc = AppServletContextListener.getContext();
		String host = "";
		String swPath = "";
		Long hostId = 0l;
		DataDTO dataDto = new DataDTO();
		try {
			/* Nithu Alexander: 09/09/2014
			 * Property File VAULT_AUDIT_CONFIG_FILE_NAME changed to VAULT_CONFIG_FILE_NAME 
			 * as part of code merge between SIT and Eclipse*/
			String filePath = CommonConstants.VAULT_CONFIG_FILE_NAME;
			String schedulerPath = CommonConstants.CSR_PROPERTY_FILE_NAME;
			InetAddress addr = InetAddress.getLocalHost();
			String hostname = addr.getHostName();
			String Ip = addr.getHostAddress();
			int schedulerTime = 0;
			String finalPath = "";
			String finalPath1 = "";
			String instance = "";
			
			Properties prop = PropertyUtils.getProperties(schedulerPath);
			if (prop.getProperty(CommonConstants.SCHEDULER_TIME) != null){
				System.out.println("SCHEDULER_TIME:"+ prop.getProperty(CommonConstants.SCHEDULER_TIME));
				schedulerTime = Integer.parseInt(prop.getProperty(CommonConstants.SCHEDULER_TIME));
			}
				
			Properties properties = PropertyUtils.getProperties(filePath);
			System.out.println("Vault Storage Path:::"+ properties.getProperty(CommonConstants.SERVER1_SOFTWARE_PATH));
			/*	Nithu Alexander : 09/09/2014
			 * Below Condition changed as part of SIT and Eclipse code merge*/
			if(properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null && properties.getProperty(CommonConstants.SOFTWARE_PATH) != null && properties.getProperty(CommonConstants.SERVER_PATH) != null){
			//if (properties.getProperty(CommonConstants.SERVER1_HOST) != null && properties.getProperty(CommonConstants.SERVER1_SOFTWARE_PATH) != null && properties.getProperty(CommonConstants.SERVER1_VAULT_SERVER_PATH) != null) {
				/* Nithu Alexander: 09/09/2014
				 * Below getProperty Constants changed as part of SIT and Eclipse code merge*/
				host = properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);
				swPath = properties.getProperty(CommonConstants.SOFTWARE_PATH);
				dataDto.setHostName(host);
				dataDto.setIpAddr(host);
				/* Nithu Alexander: 09/09/2014
				 * Below getProperty Constants changed as part of SIT and Eclipse code merge*/
				instance = properties.getProperty(CommonConstants.SERVER_INSTANCE_NAME);
				dataDto.setServerInstanceName(instance);
				dataDto.setSoftwarePath(swPath);
				hostId = this.dataManager.getLogHost(dataDto);
				/* Nithu Alexander: 09/09/2014
				 * Below getProperty Constants changed as part of SIT and Eclipse code merge*/
				finalPath = properties.getProperty(CommonConstants.SOFTWARE_PATH);
				String finalSoftwarePath = swPath;
				
				/***** adding listeners *********/
				DirectoryWatcher jrndw = (DirectoryWatcher)sc.getAttribute("jrndw");
				if(jrndw==null){
					jrndw = new DirectoryWatcher(
							finalSoftwarePath + "\\jrn", schedulerTime,
							hostId,".jrn");
					jrndw.addListener(new FileListener());
					sc.setAttribute("jrndw", jrndw);
					jrndw.start();
				}
								
				

			}

			

		} catch (Exception t) {
			t.printStackTrace();

		}

	}

	public void contextDestroyed(){
		ServletContext sc = AppServletContextListener.getContext();
		System.out.println("ServletContext::"+sc);
		try {
				System.out.println("Inside contextDestroyed of DirectoryWatcher");
				DirectoryWatcher jrndw = (DirectoryWatcher)sc.getAttribute("jrndw");
				if(jrndw!=null){
					jrndw.stop();
					sc.setAttribute("jrndw",null);
				}
		} catch (Exception t) {
			t.printStackTrace();

		}


	}
}
