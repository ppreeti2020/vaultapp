package com.pb.scheduler;

import java.text.ParseException;
import java.util.Map;
import java.util.Properties;


import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
/*
 import com.poc.DirectoryWatch.DirectoryWatcher;
 import com.poc.DirectoryWatch.FileListener;*/

import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.scheduler.SchedulerJob;
import com.pb.scheduler.SchedulerTask;


public class QuartzSchedulerListener //implements ServletContextListener
{
	public Scheduler scheduler;
	public JobDetail job = new JobDetail();

	public void stop() {
		//
		try {
			System.out.println("qa before shutdown");
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			if(scheduler!=null){
				System.out.println("scheduler>>"+scheduler);
			}
			scheduler.shutdown(false);
			System.out.println("qa after scheduler shutdown");
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void contextInitialized() {
		
		
		String schedulerPath = CommonConstants.CSR_PROPERTY_FILE_NAME;
		 Properties prop = PropertyUtils.getProperties(schedulerPath);
		 System.out.println("Quartz SCHEDULER_TIME:"+prop.getProperty(CommonConstants.SCHEDULER_TIME));
		 int SchedulerTime = Integer.parseInt(prop.getProperty(CommonConstants.SCHEDULER_TIME));
		 SchedulerTask task = new SchedulerTask();
		
		job.setName("jobName");
		job.setGroup("jobGroup");
		job.setJobClass(SchedulerJob.class);

		Map dataMap = job.getJobDataMap();
		dataMap.put("schedulerTask", task);

		try {
			// configure the scheduler time, run it every 5 seconds
			CronTrigger trigger = new CronTrigger();
			trigger.setName("runMeJobTesting");
			//String time = "0/5 0 * * * ?";
			
			String time="0 0/"+SchedulerTime+" * * * ?";
			System.out.println("timestring"+ time);			
			trigger.setCronExpression(time);// 1 mins
			// Seconds Minutes Hours Day-of-Month Month Day-of-Week Year
			// (optional field)
			// schedule it
			
			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
		
			scheduler.scheduleJob(job, trigger);

		} catch (ParseException e) {
			e.printStackTrace();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}

	}
}