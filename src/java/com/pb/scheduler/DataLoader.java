package com.pb.scheduler;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;


import com.pb.common.CommonConstants;

import com.pb.common.INIFile;
import com.pb.common.PropertyUtils;
import com.pb.dao.DBConnectionDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.UserDTO;

public class DataLoader {
	static final Logger logger = Logger.getLogger(DataLoader.class);
	
	public static void main(String... aArgs) throws IOException{
		DataLoader jcn = new DataLoader();
		jcn.processJrn("C:\\prasanna\\jrnfile\\PPLUS_AFP_2012-12-131.jrn");

	}
	public List<String> getColumnNames(String tableName)
	{	
		DBConnectionDAO db = new DBConnectionDAO();
		System.out.println("getColumnNames!!!!!!!" + tableName);
		List<String> columns = new ArrayList<String>();
		PreparedStatement ps = null;
		try {
			if (tableName != null) {
				String query = "SELECT COLUMN_NAME FROM [CSR-APP].INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ? and COLUMN_NAME NOT IN('RID','Flag')";
				ps = db.getDBConnection().prepareStatement(query);
				ps.setString(1, tableName);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					columns.add(rs.getString(1));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return columns;

	}

		
	public String getprofileDbName(String dbProfileName)
	{
		String profileDbName = "";
		DBConnectionDAO db = new DBConnectionDAO();
		System.out.println("getprofileDbName!!!!!!!" + dbProfileName);
		PreparedStatement ps = null;
		try {
			if (dbProfileName != null) {
				String query = "select csrdb.DB_NAME from CSR_DATABASE csrdb, PROFILE_METADATA pmd, PROFILE_DB_DATA pdbd where pmd.PROFILE_ID = pdbd.PROFILE_ID" +
						" and pdbd.DB_ID = csrdb.DB_ID and pmd.PROFILE_NAME = ?";
				ps = db.getDBConnection().prepareStatement(query);
				ps.setString(1, dbProfileName);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					profileDbName = rs.getString(1);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return profileDbName;
	}

	/* Nithu Alexander :22-Jul-2015
	 * Changing arguments to processLineRow() method
	 * Previous Arguements: String[] linewords,List<String> columns
	 * Current Arguements: String[] linewords*/
	public List<String> processLineRow(String[] linewords){
		List<String> fileIndexColumns = new ArrayList<String>();
		for(int i=0; i<linewords.length;i++){	
			fileIndexColumns.add(linewords[i]);
		}
		System.out.println("columns size::"+fileIndexColumns.size());
		return fileIndexColumns;
	}

	public String processJrn(String file){
	//	DataLoader jcn = new DataLoader();
		int recCount = 0;
		UserDAO u = new UserDAO();
		String message = "";
		FileInputStream fis = null;
		BufferedReader reader = null;
		PreparedStatement ps = null;
		Connection conn = null;
		DBConnectionDAO db = new DBConnectionDAO();
		String ProfileName = "";
		String[] filepath = file.split("\\\\");
		String fileName = filepath[filepath.length-1];
		System.out.println("fileName::"+fileName);
		INIFile objINI = null;
		String strFile = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.EVAULT_CONFIG_FILE_PATH)+"/profiles.ini";
		
	//	String indextoolpath=PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.INDEXCHECKTOOLPATH);
		//String indexfilepath=PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.INDEXFILEPATH);
		
		
		objINI = new INIFile(strFile);
		int total = objINI.getTotalSections();
		System.out.println("Total sections in Profile INI file:::"+total);
		ProfileName ="";
		for (int i = 0; i < total; i++) {
			if(objINI.getAllSectionNames()[i].equalsIgnoreCase("FileMap")){
				System.out.println("Inside FileMap Section");
				String[] profileMap = objINI.getPropertyNames(objINI.getAllSectionNames()[i]);
				for(String pmap:profileMap){
					if(fileName.indexOf(pmap)!=-1){
						ProfileName = objINI.getStringProperty(objINI.getAllSectionNames()[i],pmap);
						System.out.println("ProfileName::"+ProfileName);
					}
				}

			}
		}
		String tableName = "";
		boolean tableExists = false;
		if(ProfileName!=""){
			tableName = getprofileDbName(ProfileName);
			System.out.println("Data Table Name::"+tableName);
			tableExists = u.checkTableName(tableName);

			if(tableExists)	{
				List<String> columns1 = getColumnNames(tableName);
				int n = columns1.size();

				StringBuilder cols = new StringBuilder();
				String delim = "";
				for(String coln:columns1){
					cols.append(delim).append(coln);
					delim = ",";
				}
				String columnStr = cols.toString();// comma seperated column names

				StringBuilder colVals = new StringBuilder();
				String delim2 = "";
				for(int i=0;i<n;i++){
					colVals.append(delim2).append("?");
					delim2 = ",";
				}
				try{
					System.out.println("Connecting to database...");
					conn = db.getDBConnection();
					System.out.println("Inserting values into Master_Table table");
					String sql = "INSERT INTO "+tableName+" ("+columnStr+")VALUES("+colVals+")";
					ps = conn.prepareStatement(sql);
					String[] colVal = columnStr.split(",");
					Map<String,String> columnsMap = new HashMap<String,String>();
					for(String column: colVal){
						List<UserDTO> dtypes = u.getDataType(column, tableName);
						String dtype ="";
						for (Iterator<UserDTO> iter = dtypes.iterator(); iter.hasNext();) {
							UserDTO udto = (UserDTO) iter.next();
							dtype = udto.getDataType();
						}
						columnsMap.put(column, dtype);
					}
					try{
						fis = new FileInputStream(file);
						boolean docExists=false;
						reader = new BufferedReader(new InputStreamReader(fis));
						recCount =0;
						System.out.println("Started reading File line by line");
						String line = null;
						/*Nithu Alexander: 22-Jul-2015
						 *Changing arguments of processLineRow() method.
						 *Declaring List<String> columns inside processLineRow()
						 **/
						List<String> columns = new ArrayList<String>();
						outer:while ((line = reader.readLine()) != null) {
							String []linewords = line.split("\\|",-1);
							columns = processLineRow(linewords);
							System.out.println(colVal.length +"=="+ columns.size());
							if(colVal.length == columns.size()){
								int j =1;
								for(String val:columns){
									System.out.println("here");
									if(j==3){
										System.out.println("DRN is vault"+val);
										docExists=u.isDrnExists(tableName, val);
										if(docExists) continue outer;
										}
									
									
									if(val.equalsIgnoreCase("nullValue")){
										ps.setString(j,"");
									}
									else{
										String datatype = columnsMap.get(colVal[j-1]);
										if(datatype.equalsIgnoreCase("date")){
											DateFormat date = new SimpleDateFormat("yyyy-MM-dd");
											java.sql.Date convertedDate=null;
											
											// Start - Modified by Shivank to fix date discrepancy issue from text files
											//convertedDate = new java.sql.Date(date.parse(val).getTime());
											convertedDate = new java.sql.Date(parseDateToGetTime(val));
											//End
											ps.setDate(j,convertedDate);
										}
										else if(datatype.equalsIgnoreCase("bigint")){
											ps.setLong(j,Long.parseLong(val));
										}
										else if(datatype.equalsIgnoreCase("int")){
											ps.setLong(j,Integer.parseInt(val));
										}
										else if(datatype.equalsIgnoreCase("float")||datatype.equalsIgnoreCase("decimal")){
											ps.setFloat(j, Float.parseFloat(val));
										}
										else if(datatype.equalsIgnoreCase("boolean")){
											ps.setBoolean(j, Boolean.parseBoolean(val));
										}
										else
											ps.setString(j,val);
									}
									j++;
								}
								//if(true)
								//{
								
								ps.addBatch();
								System.out.println("Added record to batch"+docExists);
								//}
								/*else
								{
									System.out.println("Record not added to batch");
								}*/
								
								recCount = recCount+1;
								if(recCount == 1000){
									ps.executeBatch();
									recCount = 0;
								}
								columns.clear();
							}else{
								logger.error("FileName:"+fileName+">>"+"Column number from JRN file and the Table are not same!!!");
								message = "FileName:"+fileName+">>"+"Column number from JRN file and the Table are not same!!!";
								return message;
							}
						}
						if(recCount !=0){
							ps.executeBatch();
							System.out.println("FileName:"+fileName+">>"+"Records inserted successfully into the data table:::");
						}
						ps.close();
						conn.close();
						message = "FileName:"+fileName+">>"+"Completed";
						System.out.println("FileName:"+fileName+">>"+"VaultApp JRN loader completed");

					}catch (FileNotFoundException ex) {
						logger.error("FileNotFoundException in DataLoader " + ex.getMessage());
						ex.printStackTrace();
					} catch (IOException ex) {
						logger.error("IOException1 in DataLoader " + ex.getMessage());
						ex.printStackTrace();
					} finally {
						try {
							reader.close();
							fis.close();
						}catch (IOException ex) {
							logger.error("IOException2 in DataLoader " + ex.getMessage());
							ex.printStackTrace();
						}
					}
					ps.close();
					conn.close();
				}
				catch(SQLException se){
					se.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			else{
				logger.error("FileName:"+fileName+">>"+"Data Table for database "+tableName+" is not created!!!");
			}
		}
		else{
			logger.error("FileName:"+fileName+">>"+"No Profile is matching with the JRN file!!!");
		}
		return message;
	}
	public  long parseDateToGetTime(String inputDate) throws ParseException

	{
		DateFormat date1 = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat date2 = new SimpleDateFormat("MM/dd/yyyy");
		long time = 0;

		if (inputDate.contains("/")) {
			time = date2.parse(inputDate).getTime();
		} else if (inputDate.contains("-")) {
			time = date1.parse(inputDate).getTime();
		}

		return time;

	} 
}
