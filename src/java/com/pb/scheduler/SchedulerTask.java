package com.pb.scheduler;


import java.io.File;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.pb.DatabaseWatch.DatabaseWatcher;
import com.pb.DirectoryWatch.DirectoryWatcher;
import com.pb.DirectoryWatch.FileListener;
import com.pb.common.CommonConstants;
import com.pb.common.ProfileIniParser;
import com.pb.common.PropertyUtils;
import com.pb.dao.AdminReportDAO;
import com.pb.dao.DataIntegrityDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.DataDTO;
import com.pb.manager.AdminReportManager;
import com.pb.manager.DataIntegrityManager;


public class SchedulerTask {

	public SchedulerTask() {

	}
	private DataIntegrityDAO dataManager = new DataIntegrityDAO();
	private AdminReportDAO adrm=new AdminReportDAO() ;
	public void printSchedulerMessage() throws Exception {

		DatabaseWatcher watcher = new DatabaseWatcher();
		FileMonitor fm = new FileMonitor();
		String host = "";
		String swPath = "";
		String vaultPath = "";
		String profileFile=null;
		Long hostId=0l;
		DataDTO dataDto = new DataDTO();
		UserDAO udao = new UserDAO();
		String filePath =	CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils
		.getProperties(filePath);
		System.out.println("host:"+properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST));
		System.out.println("host:"+properties.getProperty(CommonConstants.SOFTWARE_PATH));
		String instance="";
		if ((properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST) != null ) && properties.getProperty(CommonConstants.SOFTWARE_PATH) != null)
		{
			host = properties.getProperty(CommonConstants.ARCHIVAL_SERVER_HOST);

			swPath = properties.getProperty(CommonConstants.SOFTWARE_PATH);
			dataDto.setHostName(host);
			dataDto.setIpAddr(host);
			dataDto.setSoftwarePath(swPath);
			instance=properties
			.getProperty(CommonConstants.SERVER_INSTANCE_NAME);
			dataDto.setServerInstanceName(instance);
			//hostId = this.dataManager.getLogHost(dataDto);

		//	watcher.saveVaultDatabases();
		//	watcher.saveVaultProfiles();
			//watcher.checkVaultIndexes();
			
			System.out.println("After vault database,indexes and profiles save");

	fm.JrnFileIntegrity(properties.getProperty(CommonConstants.SOFTWARE_PATH)+"\\jrn");
		
			
		}

	}
}