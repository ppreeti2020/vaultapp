package com.pb.scheduler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import java.net.InetAddress;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;


import com.g1.e2.vault.VaultClient;
import com.g1.e2.vault.VaultClient.Database;
import com.pb.LogParse.LogStrTok;
import com.pb.admin.Common;
import com.pb.common.AeSimpleSHA1;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.dao.CommonDAO;
import com.pb.dao.DataDAO;
import com.pb.dao.DataIntegrityDAO;
import com.pb.dao.FileInfoDAO;
import com.pb.dao.UserDAO;
import com.pb.dto.DataDTO;
import com.pb.dto.UserDTO;
import com.pb.manager.DataIntegrityManager;
import com.pb.manager.FileInfoManager;
import com.pb.manager.UserManager;
import com.sun.accessibility.internal.resources.accessibility;


public class FileMonitor {

	String oldfName = "";
	String fName = "";
	String fNewName = "";
	String path = "";
	Date crDate = new Date();

	String shaRes = "";
	String shaRes1 = "";

	private DataIntegrityDAO dataManager = new DataIntegrityDAO();
	private UserDAO userManager = new UserDAO();
	private FileInfoDAO fileManager = new FileInfoDAO();

	Properties properties = PropertyUtils
	.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);
	AeSimpleSHA1 sha = new AeSimpleSHA1();

	List list = new ArrayList();
	DataIntegrityDAO dao = new DataIntegrityDAO();
	public void FileIntegrity(String dataPath,Long hostId) {
		List<DataDTO> list = new ArrayList<DataDTO>();
		System.out.println("FileIntegrity:" + dataPath);
		path = dataPath;
		try
		{
			File file = new File(path);
			File[] allFiles = file.listFiles(new GenericExtFilter(".drp"));
			//InetAddress addr = InetAddress.getLocalHost();
			//System.out.println(allFiles+"FileIntegrity-Address------>" + hostId);
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			DataDTO dataDao = new DataDTO();
			System.out.println(allFiles+"allfiles:FileIntegrity-Address------>" + hostId);
			if(allFiles != null)
			{
				for (File f : allFiles) {
					System.out.println("file:"+f);

					if(f.exists())
					{
						Long LM = f.lastModified();
						System.out.println("file:LM::"+LM);
						crDate = new Date(LM);
						dataDao.setHostId(hostId);
						String s = formatter.format(crDate);
						System.out.println("files------>" + path);
						fName = f.getName();
						shaRes = AeSimpleSHA1.SHA1(fName+","+s);
						dataDao.setFilePath(path);
						System.out.println("Sha" + shaRes);
						dataDao.setNewFileName(fName);
						dataDao.setCreatedDate(s);
						dataDao.setFileAction("new file");
						dataDao.setFileName(fName);
						dataDao.setHashId(shaRes1);
						dataDao.setFilePath(path);
						dataDao.setStatus("T");
						dataDao.setFileExists("T");
						dataDao.setDataFileSize(f.length());
						list = this.dataManager.getSingleFileDetails(fName,fName, path);
						Iterator<DataDTO> iter = list.iterator();
						//boolean b = false;

						if (!iter.hasNext()) {
							System.out.println("FileIntegrity:creating as new File");
							this.dataManager.saveFileDetails(dataDao);
						}
						else
						{
							dataDao.setFileAction("FileIntegrity:update file");
							this.dataManager.updateFileDetails(dataDao);
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}



	public void Resourcentegrity(String dataPath,Long hostId) {
		List<DataDTO> list = new ArrayList<DataDTO>();
		System.out.println("FileIntegrity:" + dataPath);
		path = dataPath;
		try
		{
			File file = new File(path);
			File[] allFiles = file.listFiles();
			//InetAddress addr = InetAddress.getLocalHost();
			//System.out.println(allFiles+"FileIntegrity-Address------>" + hostId);
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			DataDTO dataDao = new DataDTO();
			System.out.println(allFiles+"allfiles:FileIntegrity-Address------>" + hostId);
			if(allFiles != null)
			{
				for (File f : allFiles) {
					System.out.println("file:"+f);
					if(f.exists())
					{
						Long LM = f.lastModified();
						System.out.println("file:LM::"+LM);
						crDate = new Date(LM);
						dataDao.setHostId(hostId);
						String s = formatter.format(crDate);
						System.out.println("files------>" + path);
						fName = f.getName();
						shaRes = AeSimpleSHA1.SHA1(fName+","+s);
						dataDao.setFilePath(path);
						System.out.println("Sha" + shaRes);
						dataDao.setNewFileName(fName);
						dataDao.setCreatedDate(s);
						dataDao.setFileAction("new file");
						dataDao.setFileName(fName);
						dataDao.setHashId(shaRes1);
						dataDao.setFilePath(path);
						dataDao.setStatus("T");
						dataDao.setDataFileSize(f.length());
						list = this.dataManager.getSingleResourceFileDetails(fName,fName, path);
						Iterator<DataDTO> iter = list.iterator();
						//boolean b = false;

						if (!iter.hasNext()) {
							System.out.println("FileIntegrity:creating as new File");
							this.dataManager.saveResourceFileDetails(dataDao);
						}
						else
						{
							dataDao.setFileAction("FileIntegrity:update file");
							this.dataManager.updateResourceFileDetails(dataDao);
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
	
	public void IndexIntegrity(String dataPath,String realPath,Long hostId) {
		List<DataDTO> list = new ArrayList<DataDTO>();
		System.out.println("IndexIntegrity:" + dataPath);
		path = dataPath;
		try
		{
			File file = new File(path);
			File[] allFiles = file.listFiles();
			InetAddress addr = InetAddress.getLocalHost();
			//System.out.println(allFiles+"FileIntegrity-Address------>" + hostId);
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			DataDTO dataDao = new DataDTO();
			System.out.println(allFiles+"allfiles:FileIntegrity-Address------>" + hostId);
			if(allFiles != null)
			{
				for (File f : allFiles) {
					System.out.println("file:"+f);
					if(f.exists())
					{
						Long LM = f.lastModified();
						System.out.println("file:LM::"+LM);
						crDate = new Date(LM);
						dataDao.setHostId(hostId);
						String s = formatter.format(crDate);
						System.out.println("files------>" + path);
						fName = f.getName();
						shaRes = AeSimpleSHA1.SHA1(fName+","+s);
						dataDao.setFilePath(path);
						System.out.println("Sha" + shaRes);
						dataDao.setNewFileName(fName);
						dataDao.setCreatedDate(s);
						dataDao.setFileAction("new file");
						dataDao.setFileName(fName);
						dataDao.setHashId(shaRes1);
						dataDao.setFilePath(path);
						dataDao.setStatus("T");
						dataDao.setDataFileSize(f.length());
						list = this.dataManager.getSingleIndexFileDetails(fName,fName, path);
						Iterator<DataDTO> iter = list.iterator();
						if (!iter.hasNext()) {
							System.out.println("FileIntegrity:creating as new File");
							this.dataManager.saveResourceFileDetails(dataDao);
						}
						else
						{
							dataDao.setFileAction("FileIntegrity:update file");
							this.dataManager.updateResourceFileDetails(dataDao);
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}


	public void DocFileIntegrity(String dataPath,Long hostId) throws Exception {
		System.out.println("DocFileIntegrity:path" + dataPath);
		path = dataPath;
		try
		{
			List<DataDTO> list = new ArrayList<DataDTO>();
			File file = new File(path);
			File[] allFiles = file.listFiles(new GenericExtFilter(".drd"));
			InetAddress addr = InetAddress.getLocalHost();
			String hostname = addr.getHostName();
			String Ip = addr.getHostAddress();
			System.out.println(dataPath + "Address------>" + hostname);
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			DataDTO dataDao = new DataDTO();
			String action = "n";
			if(allFiles != null)
			{
				for (File f : allFiles) {

					Long LM = f.lastModified();
					crDate = new Date(LM);
					dataDao.setHostId(hostId);
					String s = formatter.format(crDate);
					System.out.println("files------>" + path);
					fName = f.getName();
					shaRes = AeSimpleSHA1.SHA1(fName+","+s);
					System.out.println("Sha" + shaRes);
					dataDao.setFilePath(path);
					System.out.println("Sha" + shaRes);
					dataDao.setNewFileName(fName);
					dataDao.setCreatedDate(s);
					dataDao.setFileAction("new file");
					dataDao.setFileName(fName);
					dataDao.setHashId(shaRes1);
					dataDao.setFilePath(path);
					dataDao.setStatus("T");
					dataDao.setFileExists("T");
					dataDao.setDataFileSize(f.length());
					list = this.dataManager.getDocSingleFileDetails(fName, path);
					Iterator<DataDTO> iter = list.iterator();
					//boolean b = false;
					System.out.println("DocFileIntegrity--creating as new File");
					if (!iter.hasNext()) {
						this.dataManager.saveDocFileDetails(dataDao);
					}

				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/*public void ProfileileIntegrity(String realPath) throws Exception {
		System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		path = properties.getProperty(CommonConstants.PBBC_FILE_PATH
				)+ "//profile.ini";// f.getAbsolutePath();
		List<DataDTO> list = new ArrayList<DataDTO>();
		File file = new File(path);

		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		String Ip = addr.getHostAddress();
		System.out.println(path + "Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		DataDTO dataDao = new DataDTO();

			Long LM = file.lastModified();
			crDate = new Date(LM);
			String s = formatter.format(crDate);
			System.out.println("files------>" + path);
			fName = file.getName();
			shaRes = AeSimpleSHA1.SHA1(fName);
			System.out.println("Sha" + shaRes);
			dataDao.setNewFileName(fNewName);
			dataDao.setCreatedDate(s);
			dataDao.setFileAction("new file");
			dataDao.setFileName(fName);
			dataDao.setHashId(shaRes1);
			dataDao.setHostName(hostname);
			dataDao.setIpAddr(Ip);
			dataDao.setFilePath(path);
			dataDao.setDataFileSize(LM);
			list = this.dataManager.getDocSingleFileDetails(fName, path,realPath);
			Iterator<DataDTO> iter = list.iterator();
			//boolean b = false;
			System.out.println("creating as new File");
			if (!iter.hasNext()) {
				this.dataManager.saveDocFileDetails(dataDao,realPath);
			}



	}*/
	/*public void ResourceIntegrity(String dataPath,String realPath,Long hostId) throws Exception {
		System.out.println("ResourceIntegrity-path:" +dataPath);String action="";
		try
		{
			int index = dataPath.lastIndexOf("server");
			String path1 = dataPath.substring(0,
					index);
		List<DataDTO> profiles = this.dataManager.getLogProfiles(realPath);
		for (Iterator<DataDTO> profIter = profiles.iterator(); profIter.hasNext();) {
			DataDTO dtDao = (DataDTO) profIter.next();

			if (dtDao.getLogFileId() != 0
					&& !dtDao.getProfile().equalsIgnoreCase("")) {
				File file = new File(path1+"server\\distrib\\"+dtDao.getProfile());
				System.out.println(file.getPath());
				List<DataDTO> resList = this.dataManager.getLogResources(dtDao.getLogFileId(),realPath);
				for (Iterator<DataDTO> reIter = resList.iterator(); reIter.hasNext();) {

					DataDTO dataDao = (DataDTO) reIter.next();
					dataDao.setHostId(hostId);
					boolean b=false;
					boolean b1=false;
					String s = "";
					if (!dataDao.getResWrote().equals("")) {
						File[] allFiles = file.listFiles();
						String newFileName = "";
						for (File f : allFiles) {
							System.out.println("coming here1: ResourceIntegrity");
							if (f.getName().equalsIgnoreCase(
									dataDao.getResWrote())) {
								System.out.println("not equal: deleted");
								b = true;
								s = "d";
								//dao.saveLogResourceStatus(dtDao.getLogFileId(), dataDao.getResourceId(),dataDao.getResWrote(), "d", newFileName );
							}
							else if(!b){
								newFileName = f.getName();
								b1 = true;
								s="c";
								System.out.println("not equal: changed");

							}

						}
						if(!b || !b1)
							this.dataManager.saveLogResourceStatus(dtDao.getLogFileId(), dataDao.getResourceId(),dataDao.getResWrote(), s, newFileName, realPath ,action);
					}

				}
			}
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}*/
	public void LogFileIntegrity(String dataPath,String realPath,Long hostId,String dirPath, String host, String ip) throws Exception {
		System.out.println("LogFileIntegrity:path" + dataPath);
		System.out.println("LogFileIntegrity:hostId" + hostId);
		path = dataPath;// f.getAbsolutePath();

		File file = new File(path);
		File[] allFiles = file.listFiles();
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		String Ip = addr.getHostAddress();
		System.out.println(path + "Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		DataDTO dataDao = new DataDTO();
		String logBlob = "";
		InputStream in = null;
		//Long hostId = 0L;
		if(allFiles != null)
		{
			for (File f : allFiles) {

				Long LM = f.lastModified();
				crDate = new Date(LM);
				String s = formatter.format(crDate);
				System.out.println("files------>" + path);
				fName = f.getName();
				shaRes = AeSimpleSHA1.SHA1(fName);
				System.out.println("Sha" + shaRes);
				dataDao.setNewFileName(fNewName);
				dataDao.setCreatedDate(s);
				dataDao.setFileAction("new file");
				dataDao.setFileName(fName);
				dataDao.setHashId(shaRes1);
				dataDao.setHostName(hostname);
				dataDao.setIpAddr(Ip);

				dataDao.setFilePath(path);
				/*hostId = this.dataManager.getLogHost(dataDao,realPath);
			System.out.println("LogFileIntegrity-hostId:"+hostId);
			if(hostId == 0)
			{
				hostId = this.dataManager.saveLogHost(dataDao,realPath);
			}*/
				dataDao.setHostId(hostId);
				if (f.exists()) {
					logBlob = readFile(f);
					in = new FileInputStream(f);
					dataDao.setFileSize((int) (f.length()));
				}
				/*
				 * map.put("fName", fName); map.put("path", path); map.put("crDate",
				 * s); map.put("shaRes", shaRes); map.put("hostname", hostname);
				 * map.put("action", "new file"); map.put("Ip", Ip);
				 */
				System.out.println("LogFileIntegrity:before CheckLogFileDetails");
				Long l = this.dataManager.CheckLogFileDetails(fName, path,f.length(),"logcheck");
				//Iterator iter = list.iterator();
				//boolean b = false;
				System.out.println("LogFileIntegrity:creating as new File");
				if (l != 0) {
					System.out.println("LogFileIntegrity:before saveLogFileDetails");
					dataDao.setFileAction("update file");
					this.dataManager.saveLogFileDetails(dataDao, in);
				}

			}
		}

	}
	/*
	 * File Integrity Check for PageData folder(DRP)
	 */
	public void FileIntegrityCheck(Long hostId,String oldfilePath, String newfilePath,
			Long LM, String action,String realPath) throws Exception {
		List<DataDTO> list = new ArrayList<DataDTO>();
		System.out.println("FileIntegrityCheck:oldfilePath:"+oldfilePath);
		System.out.println("FileIntegrityCheck:newfilePath:"+newfilePath);
		System.out.println("FileIntegrityCheck:action:"+action);
		//System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		//InetAddress addr = InetAddress.getLocalHost();
		//String hostname = addr.getHostName();
		//String Ip = addr.getHostAddress();
		//System.out.println(Ip + "Address------>" + hostname);
		if(oldfilePath != null || newfilePath != null)
		{
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			File fNew = new File(newfilePath);
			File fOld = new File(oldfilePath);
			String fileName = "";
			DataDTO dataDao = new DataDTO();
			dataDao.setHostId(hostId);
			crDate = new Date(LM);
			//path = properties.getProperty(CommonConstants.PBBC_FILE_PATH)+ "//pagedata";// f.getAbsolutePath();
			fName = fOld.getName();
			fNewName = fNew.getName();
			System.out.println(fNewName + "files:changes------>" + fName);
			//shaRes = AeSimpleSHA1.SHA1(fName);
			//shaRes1 = AeSimpleSHA1.SHA1(fNewName);
			System.out.println("Sha" + shaRes);
			String s = formatter.format(crDate);
			dataDao.setNewFileName(fNewName);
			dataDao.setHostId(hostId);
			dataDao.setCreatedDate(s);
			dataDao.setFileAction(action);
			dataDao.setFileName(fName);
			dataDao.setFilePath(path);
			Long fileSize = 0l;
			if(action.equalsIgnoreCase("changed file"))
			{
				if(fOld.exists())
				{
					fileName = fName;
					fileSize = fOld.length();
				}
			}
			if(fNew.exists())
			{
				fileName = fNewName;
				fileSize = fNew.length();
			}
			list = this.dataManager.getSingleFileDetails(fName,fNewName, path);
			Iterator<DataDTO> iter = list.iterator();
			//boolean b = false;

			if (!iter.hasNext()) {
				/*
				 * map.put("shaRes", shaRes1); map.put("hostname", hostname);
				 * map.put("Ip", Ip); map.put("path", path); map.put("crDate",
				 * crDate); map.put("fName", fNewName);
				 */
				System.out.println("creating as new File"+newfilePath);
				System.out.println("update Date--->" + s);
				System.out.println("action$$$$$$$$$$"+action);
				dataDao.setHashId(shaRes1);
				dataDao.setFilePath(newfilePath);
				//dataDao.setHostName(hostname);
				//dataDao.setIpAddr(Ip);
				dataDao.setFilePath(path);
				dataDao.setFileName(fNewName);
				System.out.println("fNew.length():"+fNew.length());
				dataDao.setDataFileSize(fileSize);
				dataDao.setStatus("T");
				this.dataManager.saveFileDetails(dataDao);

			}
			while (iter.hasNext()) {
				DataDTO cDAO = (DataDTO) iter.next();
				System.out.println(cDAO.getFileName() + "Existed fName--->" + fName);
				System.out.println(cDAO.getFileName() + "path--->" + path);
				System.out.println("update Date--->" + s);
				System.out.println("action$$$$$$$$$$"+action);
				dataDao.setInteId(cDAO.getInteId());
				// map.put("valid","N");
				// map.put("SHA2", shaRes1);
				dataDao.setHashId(shaRes1);
				dataDao.setInteId(cDAO.getInteId());
				dataDao.setUpdated_dt(s);
				dataDao.setDataFileSize(fileSize);
				if(action.equalsIgnoreCase("deleted file"))
					dataDao.setStatus("F");

				//this.dataManager.saveIntegrityCheckDetails(dataDao,realPath);
				this.dataManager.updateFileDetails(dataDao);
			}
		}
	}
	public void FileDocIntegrityCheck(Long hostId,String oldfilePath, String newfilePath,
			Long LM, String action,String realPath) throws Exception {
		List<DataDTO> list = new ArrayList<DataDTO>();
		//	System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		//InetAddress addr = InetAddress.getLocalHost();
		//String hostname = addr.getHostName();
		//String Ip = addr.getHostAddress();
		//System.out.println(Ip + "Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		File fNew = new File(newfilePath);
		File fOld = new File(oldfilePath);
		DataDTO dataDao = new DataDTO();
		Long fileSize = 0l;
		String fileName = "";
		crDate = new Date(LM);
		//path = properties.getProperty(CommonConstants.PBBC_FILE_PATH)+ "//docdata";// f.getAbsolutePath();
		fName = fOld.getName();
		fNewName = fNew.getName();
		System.out.println(fNewName + "files:changes------>" + fName);
		shaRes = AeSimpleSHA1.SHA1(fName);
		shaRes1 = AeSimpleSHA1.SHA1(fNewName);
		System.out.println("Sha" + shaRes);
		String s = formatter.format(crDate);
		dataDao.setNewFileName(fNewName);
		dataDao.setCreatedDate(s);
		dataDao.setFileAction(action);
		dataDao.setFileName(fName);
		dataDao.setHostId(hostId);

		dataDao.setFilePath(path);
		if(action.equalsIgnoreCase("changed file"))
		{
			if(fOld.exists())
			{
				fileName = fName;
				fileSize = fOld.length();
			}
		}

		if(fNew.exists())
		{
			fileName = fNewName;
			fileSize = fNew.length();
		}
		list = this.dataManager.getDocSingleFileDetails(fNewName, path);
		Iterator<DataDTO> iter = list.iterator();
		//boolean b = false;


		if (!iter.hasNext()) {
			/*
			 * map.put("shaRes", shaRes1); map.put("hostname", hostname);
			 * map.put("Ip", Ip); map.put("path", path); map.put("crDate",
			 * crDate); map.put("fName", fNewName);
			 */
			System.out.println("creating as new File");
			dataDao.setHashId(shaRes1);
			dataDao.setFilePath(newfilePath);
			//dataDao.setHostName(hostname);
			//dataDao.setIpAddr(Ip);
			dataDao.setFileName(fNewName);
			dataDao.setFilePath(path);
			System.out.println("fNew.length():"+fNew.length());
			dataDao.setDataFileSize(fileSize);
			//dataDao.setFilePath(path);
			dataDao.setStatus("T");
			this.dataManager.saveDocFileDetails(dataDao);
		}
		while (iter.hasNext()) {
			DataDTO cDAO = (DataDTO) iter.next();
			System.out.println(cDAO.getFileName() + "fName--->" + fName);
			System.out.println(cDAO.getFileName() + "path--->" + path);

			// map.put("inteID", cDAO.getInteId());
			// map.put("valid","N");
			// map.put("SHA2", shaRes1);
			dataDao.setHashId(shaRes1);
			dataDao.setInteId(cDAO.getInteId());
			dataDao.setUpdated_dt(s);

			dataDao.setDataFileSize(fileSize);
			dataDao.setFilePath(path);
			System.out.println("fNew.length():"+fNew.length());
			//dataDao.setDataFileSize(fileSize);
			if(action.equalsIgnoreCase("deleted file"))
				dataDao.setStatus("F");
			dataDao.setInteId(cDAO.getInteId());
			//this.dataManager.saveDocIntegrityCheckDetails(dataDao,realPath);
			this.dataManager.updateDocFileDetails(dataDao);
		}
	}
	public void LogFileIntegrityCheck(String oldfilePath, String newfilePath,
			Long LM, String action, Long hostId) throws Exception {
		//System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		System.out.println("LogFileIntegrityCheck-oldfilePath::"+oldfilePath+"newfilePath::"+newfilePath+"LM::"+LM+"action::"+action);

		List<DataDTO> list = new ArrayList<DataDTO>();
		String fileName="";
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		LogStrTok log = new LogStrTok();
		String Ip = addr.getHostAddress();
		System.out.println(hostId + ":hostId==Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		File fNew = new File(newfilePath);
		File fOld = new File(oldfilePath);
		DataDTO dataDao = new DataDTO();
		crDate = new Date(LM);

		//path = properties.getProperty(CommonConstants.PBBC_FILE_PATH)+ "//log";// f.getAbsolutePath();
		fName = fOld.getName();
		fNewName = fNew.getName();
		System.out.println(fNewName + "files:changes------>" + fName);
		shaRes = AeSimpleSHA1.SHA1(fName);
		shaRes1 = AeSimpleSHA1.SHA1(fNewName);
		System.out.println("Sha" + shaRes);
		String s = formatter.format(crDate);
		String logBlob = "";
		long fileSize = 0;
		InputStream in = null;
		if (fOld.exists()) {
			logBlob = readFile(fOld);
			in = new FileInputStream(fOld);
			dataDao.setFileSize((int) fOld.length());
			fileName = fOld.getName();
			fileSize = fOld.length();
			path = oldfilePath;
		}
		if (fNew.exists()) {
			logBlob = readFile(fNew);
			in = new FileInputStream(fNew);
			dataDao.setFileSize((int) (fNew.length()));
			fileName = fNew.getName();
			fileSize = fNew.length();
			path = newfilePath;
		}
		dataDao.setHostId(hostId);
		dataDao.setNewFileName(fNewName);
		dataDao.setCreatedDate(s);
		dataDao.setFileAction(action);
		dataDao.setFileName(fName);
		dataDao.setLogBlob(logBlob);
		// System.out.println("logBlob:"+logBlob);
		System.out.println("fName:" + fName);
		if (fNew.exists() && !fOld.exists()) {
			if (action.equalsIgnoreCase("deleted file")) {
				dataDao.setFileAction("deleted");
				this.dataManager.updateLogFileDetails(dataDao, in, "delete");
			}
		}
		System.out.println("LogFileIntegrityCheck: before CheckLogFileDetails" + fName);
		Long l = this.dataManager.CheckLogFileDetails(fileName, path,fileSize,"logread");
		System.out.println("LogFileIntegrityCheck: afer CheckLogFileDetails::" + l);
		if( l!=0)
		{
			System.out.println("LogFileIntegrityCheck: going inside::" + l);
			list = this.dataManager.getLogFileDetails(l, fileSize);
			Iterator<DataDTO> iter = list.iterator();
			//boolean b = false;

			while (iter.hasNext()) {
				DataDTO cDAO = (DataDTO) iter.next();
				//System.out.println(cDAO.getFileName() + "fName--->" + fName);
				//System.out.println(cDAO.getFileName() + "path--->" + path);
				System.out.println(cDAO.getE2LogId() + "Id--->" + path);
				// map.put("valid","N");
				// map.put("SHA2", shaRes1);
				dataDao.setHashId(shaRes1);
				dataDao.setE2LogId(cDAO.getE2LogId());
				if (fNew.exists()) {
					this.dataManager.saveLogIntegrityCheckDetails(dataDao);
				}
				dataDao.setFileAction("update");
				dataDao.setFileSize((int)fileSize);
				this.dataManager.updateLogFileDetails(dataDao, in, "update");

				if (cDAO.getE2LogId() != 0) {
					System.out.println("FileMonitor::logBlob::"+logBlob);
					log.ReadLogData(cDAO.getE2LogId(), logBlob);
				}
			}
		}
		else
		{
			System.out.println("creating as new File"+fileName);
			dataDao.setHashId(shaRes1);
			dataDao.setHostName(hostname);
			dataDao.setIpAddr(Ip);
			dataDao.setFilePath(path);
			dataDao.setCreatedDate(s);
			dataDao.setNewFileName(fNewName);
			dataDao.setFileName(fileName);

			/*
			 * map.put("shaRes", shaRes1); map.put("hostname", hostname);
			 * map.put("Ip", Ip); map.put("path", path); map.put("crDate",
			 * crDate); map.put("fName", fNewName);
			 */

			//Long hostId = this.dataManager.getLogHost(dataDao,realPath);
			System.out.println("LogFileIntegrityCheck-hostId:"+hostId);
			//if(hostId == 0)
			//{
			//hostId = this.dataManager.saveLogHost(dataDao,realPath);
			//}
			dataDao.setHostId(hostId);
			Long e2LogFileId = this.dataManager.saveLogFileDetails(dataDao, in);
			System.out.println("e2LogFileId:" + e2LogFileId);
			if (e2LogFileId != 0) {
				log.ReadLogData(e2LogFileId, logBlob);
			}
		}
	}



	/*public void ResourceIntegrity(Long hostId,String oldfilePath, String newfilePath,
			Long LM, String action, String realPath) throws Exception {



	}*/


	/*public void ResourceIntegrity(Long hostId,String oldfilePath, String newfilePath,
			Long LM, String action, String realPath) throws Exception {
		//System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		path = newfilePath;// f.getAbsolutePath();
	//	String action="T";
		try
		{
		File fNew = new File(newfilePath);
		File fOld = new File(oldfilePath);
		//DataDAO dataDao = new DataDAO();
		crDate = new Date(LM);
		fName = fOld.getName();
		fNewName = fNew.getName();
		String newFileName = "";
		if(fOld.exists())
		{
			//oldfileName = fName;
			newFileName = fName;

		}
		if(fNew.exists())
		{
			newFileName = fNewName;

		}
		System.out.println(newfilePath+"coming here: ResourceIntegrity"+newFileName);
		List<DataDTO> profiles = this.dataManager.getLogProfiles(realPath);
		Long logfileId = 0l;
		for (Iterator<DataDTO> profIter = profiles.iterator(); profIter.hasNext();) {
			DataDTO dtDao = (DataDTO) profIter.next();
			logfileId = dtDao.getLogFileId();
			if (logfileId!= null && !logfileId.equals(0) && dtDao.getProfile() != null
					&& !dtDao.getProfile().equalsIgnoreCase("")) {
				//File file = new File(path+"\\distrib\\"+dtDao.getProfile());
				//System.out.println(file.getPath());
				System.out.println("getLogFileId"+dtDao.getLogFileId());
				boolean b=false;
				boolean b1=false;
				String oldfileName = "";
				List<DataDTO> resList = this.dataManager.getLogResources(dtDao.getLogFileId(),realPath);
				for (Iterator<DataDTO> reIter = resList.iterator(); reIter.hasNext();) {

					DataDTO dataDao = (DataDTO) reIter.next();
					if (!dataDao.getResWrote().equals("")) {
					//	File[] allFiles = file.listFiles();
						String s = "";
						//String newFileName = "";
						//for (File f : allFiles) {
							//System.out.println("coming here1: ResourceIntegrity");
							if(fName
									.equals(dataDao.getResWrote()))
							{
								if(action.equalsIgnoreCase("deleted file"))
								{
									b=true;
									s = "d";
								System.out.println("not equal: deleted");

									}
							else if(action.equalsIgnoreCase("changed file")) {
								//newFileName = f.getName();
								b1=true;
								s = "c";
								System.out.println("not equal: changed");
								}
							}
						//}
						if(b || b1)
							this.dataManager.saveLogResourceStatus(dtDao.getLogFileId(), dataDao.getResourceId(),dataDao.getResWrote(), s, fNewName,realPath,action );

					}

				}
			}
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}*/




	public void ProfileIntegrity(String oldfilePath, String newfilePath,
			Long LM, String action, String realPath,Long hostId) throws Exception {
		//System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		path = newfilePath;// f.getAbsolutePath();

		boolean b = false;
		List<UserDTO> listDb = new ArrayList<UserDTO>();
		String host = "";
		int port = 0;
		String ip = "";
		DataDTO dataDto = new DataDTO();
		boolean fExist = true;
		//Long hostId=0l;
		try {
			System.out.println("checkVaultDatabases");
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			File fNew = new File(newfilePath);
			File fOld = new File(oldfilePath);
			String fileName = "";
			DataDTO dataDao = new DataDTO();
			crDate = new Date(LM);
			//path = properties.getProperty(CommonConstants.PBBC_FILE_PATH)+ "//pagedata";// f.getAbsolutePath();
			fName = fOld.getName();
			fNewName = fNew.getName();
			System.out.println(fNewName + "files:changes------>" + fName);
			//shaRes = AeSimpleSHA1.SHA1(fName);
			//shaRes1 = AeSimpleSHA1.SHA1(fNewName);
			System.out.println("Sha" + shaRes);
			String s = formatter.format(crDate);
			dataDao.setNewFileName(fNewName);
			dataDao.setCreatedDate(s);
			dataDao.setFileAction(action);
			dataDao.setFileName(fName);
			Long fileSize = 0l;
			if(action.equalsIgnoreCase("changed file"))
			{
				if(fOld.exists())
				{
					fileName = fName;
					fileSize = fOld.length();
					fExist = false;
				}
			}
			if(fNew.exists())
			{
				fileName = fNewName;
				fileSize = fNew.length();
				//	fExist = false;
			}
			fExist=true;
			String propert = realPath
			+ "/" + CommonConstants.VAULT_AUDIT_CONFIG_FILE_NAME;
			Properties properties = PropertyUtils.getProperties(propert);
			String instance="";
			if(fExist)
			{
				for(int i=1;i<=1;i++)
				{
					if(i==1)
					{
						if (properties.getProperty(CommonConstants.SERVER1_HOST) != null
								|| properties
								.getProperty(CommonConstants.SERVER1_HOST) != "")
							host = properties
							.getProperty(CommonConstants.SERVER1_HOST);

						if (properties.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != null
								|| properties
								.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != "")
							port = Integer.parseInt(properties
									.getProperty(CommonConstants.RENDERING_SERVER1_PORT));
						instance=properties
						.getProperty(CommonConstants.SERVER1_INSTANCE_NAME);
						dataDto.setHostName(host);
						dataDto.setServerInstanceName(instance);

					}
					/*	else
						if(i==2)
						{
							if (properties.getProperty(CommonConstants.SERVER2_HOST) != null
									|| properties
											.getProperty(CommonConstants.SERVER2_HOST) != "")
								host = properties
										.getProperty(CommonConstants.SERVER2_HOST);

							if (properties.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != null
									|| properties
											.getProperty(CommonConstants.RENDERING_SERVER1_PORT) != "")
								port = Integer.parseInt(properties
										.getProperty(CommonConstants.RENDERING_SERVER1_PORT));
						}*/

					//dataDto.setSoftwarePath(swPath);
					hostId = this.dataManager.getLogHost(dataDto);
					VaultClient vc = Common.getVaultConnection(host,port,"server"+i);
					Set<Database> Databases = vc.getDatabases();
					listDb = this.userManager.getDatabases();

					for(UserDTO dto:listDb)
					{b=false;
					for (Database db1 : Databases) {
						System.out.println("checkVaultDatabases:databases"
								+ db1.getName());

						if(!dto.getDbName().equalsIgnoreCase(db1.getName()))
						{
							b = true;
						}
					}
					Long  pid=	fileManager.getProfileInteIdExists(dto.getDbName());
					System.out.println("Pid isxxxxxxxxxxxxxxxxxxxxxx"+pid);	

					if(b)
					{

						dataDao.setProfileName(dto.getDbName());
						dataDao.setDbId(dto.getDbId());
						dataDao.setAction("F");
						dataDao.setHostId(hostId);
						dataDao.setFileStatus("T");

						if(pid==0)
						{
							this.fileManager.saveProfileDetails(dataDao);
						}
						else
						{
							dataDao.setProfileID(pid);
							this.fileManager.updateProfileDetails(dataDao);
						}
					}

					else
					{

						dataDao.setProfileName(dto.getDbName());
						dataDao.setDbId(dto.getDbId());
						dataDao.setAction("T");
						dataDao.setHostId(hostId);
						dataDao.setFileStatus("T");
						if(pid==0)
						{
							this.fileManager.saveProfileDetails(dataDao);
						}
						else
						{
							dataDao.setProfileID(pid);
							this.fileManager.updateProfileDetails(dataDao);
						}

					}


					}
				}
			}
			else
			{
				//dataDao.setProfileName(dt);
				//dataDao.setDbId(dto.getDbId());
				dataDao.setAction("F");
				dataDao.setHostId(hostId);
				dataDao.setFileStatus("F");
				List<DataDTO> list = this.fileManager.getProfileDetails();
				for(DataDTO dto: list)
				{
					dataDao.setDbId(dto.getDbId());
					dataDao.setProfileInteId(dto.getProfileInteId());
					this.fileManager.updateProfileDetails(dataDao);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}


	}
	public void ResourceIntegrityCheck1(String oldfilePath, String newfilePath,
			Long LM, String action, String realPath,Long hostId) throws Exception {
		//System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		List<DataDTO> list = new ArrayList<DataDTO>();
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		String Ip = addr.getHostAddress();
		System.out.println(Ip + "Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		File fNew = new File(newfilePath);
		File fOld = new File(oldfilePath);
		DataDTO dataDao = new DataDTO();
		crDate = new Date(LM);
		//path = properties.getProperty(CommonConstants.PBBC_FILE_PATH);// f.getAbsolutePath();
		fName = fOld.getName();
		fNewName = fNew.getName();
		System.out.println(fNewName + "files:changes------>" + fName);
		shaRes = AeSimpleSHA1.SHA1(fName);
		shaRes1 = AeSimpleSHA1.SHA1(fNewName);
		System.out.println("Sha" + shaRes);
		String s = formatter.format(crDate);
		dataDao.setNewFileName(fNewName);
		dataDao.setCreatedDate(s);
		dataDao.setFileAction(action);
		dataDao.setFileName(fName);
		dataDao.setHostId(hostId);
		list = this.dataManager.getSingleFileDetails(fName, fName,path);
		Iterator<DataDTO> iter = list.iterator();
		//boolean b = false;
		System.out.println("creating as new File");
		if (!iter.hasNext()) {
			/*
			 * map.put("shaRes", shaRes1); map.put("hostname", hostname);
			 * map.put("Ip", Ip); map.put("path", path); map.put("crDate",
			 * crDate); map.put("fName", fNewName);
			 */
			dataDao.setHashId(shaRes1);
			dataDao.setHostName(hostname);
			dataDao.setIpAddr(Ip);
			dataDao.setFilePath(path);
			dataDao.setHostId(hostId);
			//	dao.saveLogResourceStatus(dataDao);
		}
		while (iter.hasNext()) {
			DataDTO cDAO = (DataDTO) iter.next();
			System.out.println(cDAO.getFileName() + "fName--->" + fName);
			System.out.println(cDAO.getFileName() + "path--->" + path);

			// map.put("inteID", cDAO.getInteId());
			// map.put("valid","N");
			// map.put("SHA2", shaRes1);
			dataDao.setHashId(shaRes1);
			dataDao.setHostId(hostId);
			dataDao.setInteId(cDAO.getInteId());
			//dao.saveIntegrityCheckDetails(dataDao);
			//dao.saveLogResourceStatus(dataDao);
		}
	}











	public void ResourceIntegrityCheck(Long hostId,String oldfilePath, String newfilePath,
			Long LM, String action,String realPath) throws Exception {




		List<DataDTO> list = new ArrayList<DataDTO>();
		//	System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		//InetAddress addr = InetAddress.getLocalHost();
		//String hostname = addr.getHostName();
		//String Ip = addr.getHostAddress();
		//System.out.println(Ip + "Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		File fNew = new File(newfilePath);
		File fOld = new File(oldfilePath);
		DataDTO dataDao = new DataDTO();
		Long fileSize = 0l;
		String fileName = "";
		crDate = new Date(LM);
		//path = properties.getProperty(CommonConstants.PBBC_FILE_PATH)+ "//docdata";// f.getAbsolutePath();
		fName = fOld.getName();
		fNewName = fNew.getName();
		System.out.println(fNewName + "files:changes------>" + fName);
		shaRes = AeSimpleSHA1.SHA1(fName);
		shaRes1 = AeSimpleSHA1.SHA1(fNewName);
		System.out.println("Sha" + shaRes);
		String s = formatter.format(crDate);
		dataDao.setNewFileName(fNewName);
		dataDao.setCreatedDate(s);
		dataDao.setFileAction(action);
		dataDao.setFileName(fName);
		dataDao.setHostId(hostId);

		dataDao.setFilePath(path);
		if(action.equalsIgnoreCase("changed file"))
		{
			if(fOld.exists())
			{
				fileName = fName;
				fileSize = fOld.length();
			}
		}

		if(fNew.exists())
		{
			fileName = fNewName;
			fileSize = fNew.length();
		}
		list = this.dataManager.getSingleResourceFileDetails(fName,fNewName, path);
		Iterator<DataDTO> iter = list.iterator();
		//boolean b = false;


		if (!iter.hasNext()) {
			/*
			 * map.put("shaRes", shaRes1); map.put("hostname", hostname);
			 * map.put("Ip", Ip); map.put("path", path); map.put("crDate",
			 * crDate); map.put("fName", fNewName);
			 */
			System.out.println("creating as new File");
			dataDao.setHashId(shaRes1);
			dataDao.setFilePath(newfilePath);
			//dataDao.setHostName(hostname);
			//dataDao.setIpAddr(Ip);
			dataDao.setFileName(fNewName);
			dataDao.setFilePath(path);
			System.out.println("fNew.length():"+fNew.length());
			dataDao.setDataFileSize(fileSize);
			//dataDao.setFilePath(path);
			dataDao.setStatus("T");
			this.dataManager.saveResourceFileDetails(dataDao);
		}
		while (iter.hasNext()) {
			DataDTO cDAO = (DataDTO) iter.next();
			System.out.println(cDAO.getFileName() + "fName--->" + fName);
			System.out.println(cDAO.getFileName() + "path--->" + path);

			// map.put("inteID", cDAO.getInteId());
			// map.put("valid","N");
			// map.put("SHA2", shaRes1);
			dataDao.setHashId(shaRes1);
			dataDao.setInteId(cDAO.getInteId());
			dataDao.setUpdated_dt(s);

			dataDao.setDataFileSize(fileSize);
			dataDao.setFilePath(path);
			System.out.println("fNew.length():"+fNew.length());
			//dataDao.setDataFileSize(fileSize);
			if(action.equalsIgnoreCase("deleted file"))
				dataDao.setStatus("F");
			dataDao.setInteId(cDAO.getInteId());
			//this.dataManager.saveDocIntegrityCheckDetails(dataDao,realPath);
			this.dataManager.updateResourceFileDetails(dataDao);
		}


	}
	public void FileIntegrity() throws Exception {
		System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		path = properties.getProperty(CommonConstants.PBBC_FILE_PATH
		)+ "//pagedata";// f.getAbsolutePath();

		File file = new File(path);
		File[] allFiles = file.listFiles();
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		String Ip = addr.getHostAddress();
		System.out.println(path + "Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		DataDTO dataDao = new DataDTO();

		for (File f : allFiles) {

			Long LM = f.lastModified();
			crDate = new Date(LM);
			String s = formatter.format(crDate);
			System.out.println("files------>" + path);
			fName = f.getName();
			shaRes = sha.SHA1(fName);
			System.out.println("Sha" + shaRes);
			dataDao.setNewFileName(fNewName);
			dataDao.setCreatedDate(s);
			dataDao.setFileAction("new file");
			dataDao.setFileName(fName);
			dataDao.setHashId(shaRes1);
			dataDao.setHostName(hostname);
			dataDao.setIpAddr(Ip);
			dataDao.setFilePath(path);
			list = dao.getSingleFileDetails(fName, path);
			Iterator iter = list.iterator();
			boolean b = false;
			System.out.println("creating as new File");
			if (!iter.hasNext()) {
				dao.saveFileDetails(dataDao);
			}

		}

	}

	public void LogFileIntegrity() throws Exception {
		System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		path = properties.getProperty(CommonConstants.PBBC_FILE_PATH);// f.getAbsolutePath();

		File file = new File(path+"//log");
		File[] allFiles = file.listFiles();
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		String Ip = addr.getHostAddress();
		System.out.println(path + "Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		DataDTO dataDao = new DataDTO();
		String logBlob = "";
		InputStream in = null;
		for (File f : allFiles) {

			Long LM = f.lastModified();
			crDate = new Date(LM);
			String s = formatter.format(crDate);
			System.out.println("files------>" + path);
			fName = f.getName();
			shaRes = sha.SHA1(fName);
			System.out.println("Sha" + shaRes);
			dataDao.setNewFileName(fNewName);
			dataDao.setCreatedDate(s);
			dataDao.setFileAction("new file");
			dataDao.setFileName(fName);
			dataDao.setHashId(shaRes1);
			dataDao.setHostName(hostname);
			dataDao.setIpAddr(Ip);
			dataDao.setFilePath(path);
			if (f.exists()) {
				logBlob = readFile(f);
				in = new FileInputStream(f);
				dataDao.setFileSize((int) (f.length()));
			}
			/*
			 * map.put("fName", fName); map.put("path", path); map.put("crDate",
			 * s); map.put("shaRes", shaRes); map.put("hostname", hostname);
			 * map.put("action", "new file"); map.put("Ip", Ip);
			 */
			list = dao.getSingleFileDetails(fName, path);
			Iterator iter = list.iterator();
			boolean b = false;
			System.out.println("creating as new File");
			if (!iter.hasNext()) {
				dao.saveLogFileDetails(dataDao, in);
			}

		}

	}
	
	public void FileDocIntegrityCheck(String oldfile, String newfile,
			Long LM, String action) throws Exception {
		CommonDAO cdao = new CommonDAO();
		System.out.println("FileIntegrityCheck-oldfile::"+oldfile+"newfile::"+newfile+"LM::"+LM+"action::"+action);
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		String Ip = addr.getHostAddress();
		System.out.println(Ip + "Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		File fNew = new File(newfile);
		File fOld = new File(oldfile);
		DataDTO dataDao = new DataDTO();
		crDate = new Date(LM);
		fName = fOld.getName();
		fNewName = fNew.getName();
		shaRes = sha.SHA1(fName);
		shaRes1 = sha.SHA1(fNewName);
		System.out.println("Sha" + shaRes);
		String s = formatter.format(crDate);
		dataDao.setNewFileName(fNewName);
		dataDao.setCreatedDate(s);
		dataDao.setFileAction(action);
		dataDao.setFileName(fName);
		list = dao.getDocSingleFileDetails(fNewName, path);
		Iterator iter = list.iterator();
		boolean b = false;
		System.out.println("creating as new File");
		if (!iter.hasNext()) {
			
			dataDao.setHashId(shaRes1);
			dataDao.setHostName(hostname);
			dataDao.setIpAddr(Ip);
			dataDao.setFilePath(path);
			dataDao.setFileExists("T");
			dataDao.setCheckValid("T");
			dao.saveDocFileDetails(dataDao);
		}
		while (iter.hasNext()) {
			DataDAO cDAO = (DataDAO) iter.next();
			
			if (fNew.exists()) {
				dataDao.setDataFileSize((Long) (fNew.length()));
			}
			dataDao.setUpdated_dt(s);
			dataDao.setHashId(shaRes1);
			dataDao.setInteId(cDAO.getInteId());
			dataDao.setStatus("F");
			if(action.equalsIgnoreCase("deleted file")){
				dataDao.setFileExists("F");
			}
			else{
				dataDao.setFileExists("T");
			}
			dao.updateDocFileDetails(dataDao);
			
		}
	}

	public void FileIntegrityCheck(String oldfile, String newfile,
			Long LM, String action) throws Exception {
		CommonDAO cdao = new CommonDAO();
		System.out.println("FileIntegrityCheck-oldfile::"+oldfile+"newfile::"+newfile+"LM::"+LM+"action::"+action);
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		String Ip = addr.getHostAddress();
		System.out.println(Ip + "Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		File fNew = new File(newfile);
		File fOld = new File(oldfile);
		DataDTO dataDao = new DataDTO();
		crDate = new Date(LM);
		path = properties.getProperty(CommonConstants.PBBC_FILE_PATH)+ "//pagedata";// f.getAbsolutePath();
		fName = fOld.getName();
		fNewName = fNew.getName();
		shaRes = sha.SHA1(fName);
		shaRes1 = sha.SHA1(fNewName);
		System.out.println("Sha" + shaRes);
		String s = formatter.format(crDate);
		dataDao.setNewFileName(fNewName);
		dataDao.setCreatedDate(s);
		dataDao.setFileAction(action);
		dataDao.setFileName(fName);
		list = dao.getSingleFileDetails(fNewName, path);
		Iterator iter = list.iterator();
		boolean b = false;
		System.out.println("creating as new File");
		if (!iter.hasNext()) {
			
			dataDao.setHashId(shaRes1);
			dataDao.setHostName(hostname);
			dataDao.setIpAddr(Ip);
			dataDao.setFilePath(path);
			dataDao.setFileExists("T");
			dataDao.setCheckValid("T");
			dao.saveFileDetails(dataDao);
		}
		while (iter.hasNext()) {
			DataDAO cDAO = (DataDAO) iter.next();
			
			if (fNew.exists()) {
				dataDao.setDataFileSize((Long) (fNew.length()));
			}
			dataDao.setUpdated_dt(s);
			dataDao.setHashId(shaRes1);
			dataDao.setInteId(cDAO.getInteId());
			dataDao.setStatus("F");
			if(action.equalsIgnoreCase("deleted file")){
				dataDao.setFileExists("F");
			}
			else{
				dataDao.setFileExists("T");
			}
			dao.updateFileDetails(dataDao);
			
		}
	}

	public void LogFileIntegrityCheck(String oldfile, String newfile,
			Long LM, String action) throws Exception {
		System.out.println("LogFileIntegrityCheck-oldfilePath::"+oldfile+"newfilePath::"+newfile+"LM::"+LM+"action::"+action);
		CommonDAO cdao = new CommonDAO();
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		LogStrTok log = new LogStrTok();
		String Ip = addr.getHostAddress();
		System.out.println(Ip + "::LogFileIntegrityCheck-Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
		File fNew = new File(newfile);
		File fOld = new File(oldfile);
		DataDTO dataDao = new DataDTO();
		crDate = new Date(LM);
		fName = fOld.getName();
		fNewName = fNew.getName();
		shaRes = sha.SHA1(fName);
		shaRes1 = sha.SHA1(fNewName);
		String s = formatter.format(crDate);
		String logBlob = "";
		InputStream in = null;
		if (fOld.exists()) {
			logBlob = readFile(fOld);
			in = new FileInputStream(fOld);
			dataDao.setFileSize((int) fOld.length());
		}
		if (fNew.exists()) {
			logBlob = readFile(fNew);
			in = new FileInputStream(fNew);
			dataDao.setFileSize((int) (fNew.length()));
		}

		dataDao.setNewFileName(fNewName);
		dataDao.setCreatedDate(s);
		dataDao.setFileAction(action);
		dataDao.setFileName(fName);
		dataDao.setLogBlob(logBlob);
		list = dao.getLogFileDetails(fNewName, path, fOld.length());
		Iterator iter = list.iterator();
		boolean b = false;

		if (!iter.hasNext()) {
			System.out.println("creating as new File");
			dataDao.setHashId(shaRes1);
			dataDao.setHostName(hostname);
			dataDao.setIpAddr(Ip);
			dataDao.setFilePath(path);
			dataDao.setCreatedDate(s);
			dataDao.setNewFileName(fNewName);
			Long e2LogFileId = dao.saveLogFileDetails(dataDao, in);
			System.out.println("e2LogFileId:" + e2LogFileId);
			if (e2LogFileId != 0) {
				log.ReadLogData(e2LogFileId, logBlob);
			}
		}
		while (iter.hasNext()) {
			DataDAO cDAO = (DataDAO) iter.next();
			dataDao.setHashId(shaRes1);
			dataDao.setE2LogId(cDAO.getE2LogId());
			dataDao.setStatus("F");
			
			if(action.equalsIgnoreCase("deleted file")){
				dataDao.setFileExists("F");
				dao.updateLogFileDetails(dataDao, in, "delete");
			}
			else{
				dataDao.setFileExists("T");
				dao.updateLogFileDetails(dataDao, in, "update");
			}
			if (cDAO.getE2LogId() != 0) {
				log.ReadLogData(cDAO.getE2LogId(), logBlob);
			}

		}
	}

	public static String readFile(File file) {

		StringBuffer contents = new StringBuffer();

		BufferedReader reader = null;

		try {

			reader = new BufferedReader(new FileReader(file));

			String text = null;

			// repeat until all lines is read

			while ((text = reader.readLine()) != null) {

				contents.append(text).append(
						System.getProperty("line.separator"));

			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (reader != null) {

					reader.close();

				}

			} catch (IOException e) {

				e.printStackTrace();

			}

		}
		return contents.toString();
	}

	public void ResourceIntegrity() throws Exception {
		//System.out.println("path" + CommonConstants.PBBC_FILE_PATH);
		path = properties.getProperty(CommonConstants.PBBC_FILE_PATH);// f.getAbsolutePath();
		System.out.println("coming here: ResourceIntegrity");
		List profiles = dao.getLogProfiles();
		for (Iterator profIter = profiles.iterator(); profIter.hasNext();) {
			DataDAO dtDao = (DataDAO) profIter.next();
			if (dtDao.getLogFileId() != 0
					&& !dtDao.getProfile().equalsIgnoreCase("")) {
				File file = new File(path+"\\distrib\\"+dtDao.getProfile());
				System.out.println(file.getPath());
				List resList = dao.getLogResources(dtDao.getLogFileId());
				for (Iterator reIter = resList.iterator(); reIter.hasNext();) {

					DataDAO dataDao = (DataDAO) reIter.next();
					if (!dataDao.getResWrote().equals("")) {
						File[] allFiles = file.listFiles();
						String newFileName = "";
						for (File f : allFiles) {
							System.out.println("coming here1: ResourceIntegrity");
							if (!f.getName().equalsIgnoreCase(
									dataDao.getResWrote())) {
								System.out.println("not equal: deleted");
								dao.saveLogResourceStatus(dtDao.getLogId(), dataDao.getResourceId(),dataDao.getResWrote(), "d", newFileName );
							}
							else {
								newFileName = f.getName();
								System.out.println("not equal: changed");
								dao.saveLogResourceStatus(dtDao.getLogId(), dataDao.getResourceId(),dataDao.getResWrote(), "c", newFileName );
							}

						}
					}

				}
			}
		}
	}
	public void ResourceIntegrityCheck(String oldfile, String newfile,
			Long LM, String action) throws Exception {
		System.out.println("ResourceIntegrityCheck-oldfile::"+oldfile+"newfile::"+newfile+"LM::"+LM+"action::"+action);
		CommonDAO cdao = new CommonDAO();
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		LogStrTok log = new LogStrTok();
		String Ip = addr.getHostAddress();
		System.out.println(Ip + "::ResourceIntegrityCheck-Address------>" + hostname);
		SimpleDateFormat formatter = new SimpleDateFormat(CommonConstants.DATE_FORMAT);
		File fNew = new File(newfile);
		File fOld = new File(oldfile);
		DataDTO dataDao = new DataDTO();
		crDate = new Date(LM);
		fName = fOld.getName();
		fNewName = fNew.getName();
		shaRes = sha.SHA1(fName);
		shaRes1 = sha.SHA1(fNewName);
		String s = formatter.format(crDate);
		String logBlob = "";
		InputStream in = null;
		if (fOld.exists()) {
			logBlob = readFile(fOld);
			in = new FileInputStream(fOld);
			dataDao.setFileSize((int) fOld.length());
		}
		if (fNew.exists()) {
			logBlob = readFile(fNew);
			in = new FileInputStream(fNew);
			dataDao.setFileSize((int) (fNew.length()));
		}

		dataDao.setNewFileName(fNewName);
		dataDao.setCreatedDate(s);
		dataDao.setFileAction(action);
		dataDao.setFileName(fName);
		dataDao.setLogBlob(logBlob);
		list = dao.getSingleResourceFileDetails(fNewName, path);
		Iterator iter = list.iterator();
		boolean b = false;

		if (!iter.hasNext()) {
			System.out.println("creating as new File");
			dataDao.setHashId(shaRes1);
			dataDao.setHostName(hostname);
			dataDao.setIpAddr(Ip);
			dataDao.setFilePath(path);
			dataDao.setCreatedDate(s);
			dataDao.setNewFileName(fNewName);
			dao.saveResourceFileDetails(dataDao);
			
		}
		while (iter.hasNext()) {
			DataDAO cDAO = (DataDAO) iter.next();
			dataDao.setHashId(shaRes1);
			dataDao.setE2LogId(cDAO.getE2LogId());
			dataDao.setStatus("F");
			
			if(action.equalsIgnoreCase("deleted file")){
				dataDao.setFileExists("F");
				dao.updateLogFileDetails(dataDao, in, "delete");
			}
			else{
				dataDao.setFileExists("T");
				dao.updateLogFileDetails(dataDao, in, "update");
			}
			if (cDAO.getE2LogId() != 0) {
				log.ReadLogData(cDAO.getE2LogId(), logBlob);
			}

		}
	}
	
	public void JrnFileIntegrity(String dataPath) {
		
		synchronized(this){
			
		 UserDAO userManager1 = new UserDAO();
		List<DataDTO> list = new ArrayList<DataDTO>();
		System.out.println("JrnFileIntegrity:" + dataPath);
		path = dataPath;
		try
		{
			File file = new File(path);
			File[] allFiles = file.listFiles(new GenericExtFilter(".TXT"));
			
			System.out.println("Listed allfiles"+allFiles.length);
			SimpleDateFormat formatter = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			DataDTO dataDao = new DataDTO();
			System.out.println(allFiles+"allfiles:JrnFileIntegrity-Address------>" + dataPath);
			if(allFiles != null)
			{
				for (File f : allFiles) {
					System.out.println("file:"+f+"length is"+f.length()+"ame"+"=="+f.getName());

					if(f.exists()&& f.length()>0)
					{
						DataLoader jcn = new DataLoader();
						String fileName = f.getName();
						int count = userManager1.getJrnStatus(fileName);
						if( count==0){
							boolean b = userManager1.saveJrnDetails(fileName);
							System.out.println("saved::"+b);
							if(b!=false){
								System.out.println("inside to process");
								String message = jcn.processJrn(f.toString());
								if(message.contains("Completed")){
									userManager1.updateJrnDetails(fileName);
									moveFile(f);
								}
								/*else if(message.contains("DOCUMENTNOTFOUND"))
								{
									System.out.println("Document not found");
									moveFile(f);
								}*/
								
								else
									System.out.println(message);
							}
						}
					}
					
					else
					{
						if(f.exists())
						{
							moveFile(f);
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		}
	}

private void moveFile(File afile)
{
	String strFile = PropertyUtils.getProperties(CommonConstants.VAULT_CONFIG_FILE_NAME).getProperty(CommonConstants.SERVER1_PROCESSED_JRN_PATH);
	 if(afile.renameTo(new File(strFile+"\\" + afile.getName()))){
 		System.out.println("File is moved successful!");
 	   }else{
 		System.out.println("File is failed to move!");
 	   }


	
	//	/SERVER1_PROCESSED_JRN_PATH
}

}
