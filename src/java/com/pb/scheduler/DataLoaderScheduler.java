package com.pb.scheduler;

import java.util.Properties;
import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.struts2.ServletActionContext;

//import com.pb.admin.VaultConfigAction.LoadDataTask;
import com.pb.admin.LoadDataTask;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;

public class DataLoaderScheduler {
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		LoadDataTask.stopMonitoring();
	}

	public void contextInitialized(ServletContextEvent arg0) {
		
		System.out.println("Context Initialized vault");
		LoadDataTask.startMonitor();
		System.out.println("end Context Initialized vault");
	}

}
