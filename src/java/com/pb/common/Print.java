package com.pb.common;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.Attribute;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;


public class Print {
	
	 public static void main(String[] args) throws IOException {  
	      File file = new File("C:\\test.txt");  
	      String testData = "Hello World !";
	     // byte[] b = new byte[(int) file.length()];
	      FileInputStream fis = new FileInputStream(file);
	      //fileInputStream.read(b);
	           
	      
	    //  InputStream is = new BufferedInputStream(new FileInputStream(file));  
	      PrintService service = PrintServiceLookup.lookupDefaultPrintService();
	      
	      System.out.println("service default::"+service.getAttributes().size());
	      DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
	      PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
	  	  aset.add(new Copies(2));
	      System.out.println("DocFlavour::::"+flavor.getMimeType());
	              
	      DocPrintJob job = service.createPrintJob();  
	      Doc doc = new SimpleDoc(fis, flavor, null);  
	      job.addPrintJobListener(new PrintJobAdapter() {
	    	    public void printDataTransferCompleted(PrintJobEvent e) {
	    	     System.out.println("Data transfer completed!");
	    	    }

	    	    public void printJobNoMoreEvents(PrintJobEvent e) {
	    	     System.out.println("No more events!");
	    	    }

	    	    public void printJobRequiresAttention(PrintJobEvent e) {
	    	     System.out.println("Requires Attention!");
	    	    }

	    	    public void printJobFailed(PrintJobEvent e) {
	    	     System.out.println("Print Job Failed!");
	    	    }

	    	    public void printJobCompleted(PrintJobEvent e) {
	    	     System.out.println("Print Job Completed!");
	    	    }

	    	    public void printJobCanceled(PrintJobEvent e) {
	    	     System.out.println("Print Job Cancelled!");
	    	    }
	    	   });

	      try {  
	       job.print(doc, aset);  
	      } catch (PrintException e) {  
	       e.printStackTrace();  
	      }    
	      catch(Exception e){
	    	  e.printStackTrace();
	      }
	      fis.close();  
	      System.out.println("Printing done....");  
	     }  

}
