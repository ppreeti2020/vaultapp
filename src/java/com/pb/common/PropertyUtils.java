package com.pb.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.pb.ldap.LDAPUtils;

public class PropertyUtils {

	public static final Logger logger = Logger.getLogger(PropertyUtils.class);

	public static Properties getProperties(String fileNameWithPath) {
		Properties properties = new Properties();
		try {
			InputStream s = PropertyUtils.class.getClassLoader()
			.getResourceAsStream(fileNameWithPath);
			properties.load(s);
		} catch (IOException e) {
			logger.error("Exception in PropertyUtils.getProperties "
					+ e.getMessage());
		}

		return properties;
	}
	
	
	public static void save(Properties props,String fileName)
	{
		try {
			URL u = PropertyUtils.class.getClassLoader().getResource(fileName);
			File f=new File(u.toURI().getPath());
			 props.store(new FileOutputStream(f), "");
			
		} catch (Exception e) {
			logger.error("Exception in PropertyUtils.getProperties "
					+ e.getMessage());
		}
			
	}
	
		
}

