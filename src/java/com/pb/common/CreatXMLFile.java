/*
 * created by Suhashini
 * to generate XML file
 */
package com.pb.common;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.SimpleTimeZone;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.w3c.dom.*;

import com.pb.dao.DataDAO;

public class CreatXMLFile {
	public void SetXMLElements(List list) throws Exception {
		// BufferedReader bf = new BufferedReader(
		// new InputStreamReader(System.in));
		//System.out.print("Enter number to add elements in your XML file: ");
		// String str = bf.readLine();
		// int no = Integer.parseInt(str);

		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory
				.newDocumentBuilder();
		Document document = documentBuilder.newDocument();
		// document.createElementNS(, );

		// System.out.print("Enter root: ");
		// String root = bf.readLine();
		Element rootElement = document.createElement("data");
		rootElement.setAttribute("wiki-url", "http://simile.mit.edu/shelf/");
		rootElement.setAttribute("wiki-section", "Simile JFK Timeline");
		// rootElement.setAttributeNS("", "xmlns:xsi",
		// "16c62a3e46b34eb7a935c8389ce6e2ab.xsd");
		document.appendChild(rootElement);
		Iterator iter = list.iterator();
		String info = "";
		ClientSideEncryptDecrypt encdec = new ClientSideEncryptDecrypt();
		while (iter.hasNext()) {
			DataDAO cDAO = (DataDAO) iter.next();
			java.util.Calendar cal = Calendar.getInstance(new SimpleTimeZone(6,
					"GMT"));

			// Get the default MEDIUM/SHORT DateFormat
			SimpleDateFormat formatter = new SimpleDateFormat(
					"dd-MM-yyyy HH:mm:ss");
			SimpleDateFormat formatter1 = new SimpleDateFormat(
					"dd-MM-yyyy HH:mm:ss");

			SimpleDateFormat format = new SimpleDateFormat(
					"EEE MMM dd yyyy HH:mm:ss zzz-0800");
			SimpleDateFormat format1 = new SimpleDateFormat(
					"EEE MMM dd yyyy HH:mm:ss zzz-0800");
			format.setCalendar(cal);
			format1.setCalendar(cal);
			Date dateStr = new Date();
			String str = "";
			if (cDAO.getLoginDate() != null) {
				dateStr = formatter.parse(encdec.decryptData(cDAO
						.getLoginDate()));
				str = format1.format(dateStr);
			}
			Date dateStr1 = new Date();
			String str1 = "";
			if (cDAO.getLogoutDate() != null) {
				dateStr1 = formatter.parse(encdec.decryptData(cDAO
						.getLogoutDate()));
				str1 = format1.format(dateStr);
			}
			//System.out.print("Enter the element: ");
			// String element = bf.readLine();
			// System.out.print("Enter the Attribute: ");
			// String atr = bf.readLine();
			//System.out.print("Enter the data: ");
			// String data = bf.readLine();
			// Attr attr =
			// document.createAttributeNS("xmlns:xsi=http://www.w3.org/2001/XMLSchema-instance",
			// atr);
			if (cDAO.getIP4() != null && cDAO.getBrowser() != null) {
				info = encdec.decryptData(cDAO.getIP4())
						+ " Through IP address via "
						+ encdec.decryptData(cDAO.getBrowser());
			}
			Element em = document.createElement("event");
			if (cDAO.getLogout() != null && cDAO.getLogoutDate() != null) {
				em.setAttribute("start", str);
				em.setAttribute("end", str1);
				em.setAttribute(
						"title",
						"Logged in at "
								+ encdec.decryptData(cDAO.getLoginName())
								+ " and Logged out at "
								+ encdec.decryptData(cDAO.getLogoutDate()));
			} else if (cDAO.getLoginDate() != null) {
				em.setAttribute("start", str);
				em.setAttribute(
						"title",
						"Logged in at "
								+ encdec.decryptData(cDAO.getLoginName()));
			}

			em.appendChild(document.createTextNode(info));

			rootElement.appendChild(em);
		}
		createXMLFile(document);

	}

	public static void createXMLFile(Document document)
			throws TransformerException {
		try {
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			// StreamResult result = new StreamResult(System.out);
			// transformer.transform(source, result);
			transformer.transform(source, result);
			String xmlString = sw.toString();

			File file = new File("c:/viewxml.xml");
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file)));
			bw.write(xmlString);
			bw.flush();
			bw.close();
		} catch (Exception e) {

		}
	}
}