/*
 * Created on Oct 12, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.pb.common;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.IvParameterSpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * @author Suhasini
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class ClientSideEncryptDecrypt {
	/**
	 * Return encrypted value for a given String argument.
	 * 
	 * @param str
	 * @return encrypted string value.
	 * @throws Exception
	 */
	public static String encryptData(String str) throws Exception {
		// obtaining byte array from string argument
		byte[] valuesBytes = str.getBytes();

		// Encrypting the data
		byte[] encrypted;
		try {
			encrypted = getCipher("ENCRYPT_MODE").doFinal(valuesBytes);
		} catch (Exception e) {
			throw new Exception(e);
		}

		// return encrypted value
		return new String(new BASE64Encoder().encode(encrypted));
	}

	/**
	 * Return decrypted value for a given encrypted String argument.
	 * 
	 * @param str
	 * @return decrypted string value.
	 * @throws Exception
	 */
	public static String decryptData(String str) throws Exception {
		try {
			// decrypting the data
			byte[] actualValue = getCipher("DECRYPT_MODE").doFinal(
					new BASE64Decoder().decodeBuffer(str));
			// return decrypted value
			return new String(actualValue, "UTF-8");
		} catch (Exception ex) {
			throw new Exception("Error decrypting " + str);
		}
	}

	/*
	 * Return the Cipher object.
	 */
	private static Cipher getCipher(String mode) throws Exception {
		// encryption Key in byte array
		/*
		 * byte[] encryptKey = { (byte) 0x91, (byte) 0x02, (byte) 0x9, (byte)
		 * 0x44, (byte) 0x20, (byte) 0x43, (byte) 0x23, (byte) 0x08}; String
		 * secretKey = PropertyUtils.getProperties(
		 * CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
		 * CommonConstants.SECRET_KEY);
		 */
		String saltKey = "CSR APPS VAULT APPLICATION WITH PITNEY BOWES SOFTWARE INSIGHT CO";
		byte[] encryptKey = saltKey.getBytes();
		// initialization of vector (necessary for CBC mode)
		IvParameterSpec IvParameters = new IvParameterSpec(new byte[] { 92, 48,
				36, 78, 91, 80, 05, 43 });
		// creating a DES key spec from the byte array key
		DESedeKeySpec spec = new DESedeKeySpec(encryptKey);

		// initializing secret key factory for generating DES keys
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");

		// creating a DES SecretKey object
		SecretKey theKey = keyFactory.generateSecret(spec);

		// obtaining a DES Cipher object
		Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");

		if (mode.equalsIgnoreCase("ENCRYPT_MODE")) {
			// Initializing the cipher and put it into encrypt mode
			cipher.init(Cipher.ENCRYPT_MODE, theKey, IvParameters);
		} else if (mode.equalsIgnoreCase("DECRYPT_MODE")) {
			// Initializing the cipher and put it into decrypt mode
			cipher.init(Cipher.DECRYPT_MODE, theKey, IvParameters);
		}
		return cipher;
	}

}
