package com.pb.common;

import java.io.*;

import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;

public class PrintTest {

	public static String printtext() throws  IOException,
	ClassNotFoundException {
		String message = "";
		try {
			File file = new File("C:\\printtest.txt");  
			InputStream psStream = new BufferedInputStream(new FileInputStream(file)); 
			DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;    

			Doc myDoc = new SimpleDoc(psStream, flavor, null);
			PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet(); 
			aset.add(new Copies(1));
			aset.add(Sides.ONE_SIDED); 

			PrintService service = PrintServiceLookup.lookupDefaultPrintService();
			System.out.println("Print Service::"+service);
			DocPrintJob job = service.createPrintJob();
			System.out.println("Print Job::"+job);
			try {
				job.print(myDoc, aset);
				message = "Document has been printed to default printer "+service+" !!";
				System.out.println("Document has been printed!!");
			} catch (Exception pe) {
				pe.printStackTrace();
			}

		} catch (FileNotFoundException ffne) {
			ffne.printStackTrace();
		}
		return message;

	}
	
	public static void main(String args[]) throws  IOException,
	ClassNotFoundException {
		String message = "";
		try {
			File file = new File("C:\\printtest.txt");  
			InputStream psStream = new BufferedInputStream(new FileInputStream(file)); 
			DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;    

			Doc myDoc = new SimpleDoc(psStream, flavor, null);
			PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet(); 
			aset.add(new Copies(1));
			aset.add(Sides.ONE_SIDED); 

			PrintService service = PrintServiceLookup.lookupDefaultPrintService();
			System.out.println("Print Service::"+service);
			DocPrintJob job = service.createPrintJob();
			System.out.println("Print Job::"+job);
			try {
				job.print(myDoc, aset);
				message = "Document has been printed!!";
				System.out.println("Document has been printed!!");
			} catch (Exception pe) {
				pe.printStackTrace();
			}

		} catch (FileNotFoundException ffne) {
			ffne.printStackTrace();
		}
		

	}
	
	public static String print(ByteArrayOutputStream baos) throws IOException,
	ClassNotFoundException {
		String message = "";
		byte[] buf = baos.toByteArray();
		DocFlavor psInFormat = DocFlavor.BYTE_ARRAY.AUTOSENSE;
		Doc myDoc = new SimpleDoc(baos.toByteArray(), psInFormat, null);
		PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
		PrintService myPrinter = PrintServiceLookup.lookupDefaultPrintService();
		System.out.println("Print Service::"+myPrinter);
		PrintService[] services = PrintServiceLookup.lookupPrintServices(
				psInFormat, aset);

		// this step is necessary because I have several printers configured
		/*PrintService myPrinter = null;
		for (int i = 0; i < services.length; i++) {

			String svcName = services[i].toString();
			System.out.println("service found: " + svcName);
			if (svcName.contains("FX AP-II C3300 PCL")) {
				myPrinter = services[i];
				System.out.println("my printer found: " + svcName);

				break;

			}
		}*/

		if (myPrinter != null) {
			DocPrintJob job = myPrinter.createPrintJob();

			try {
				job.print(myDoc, aset);
				message = "Document has been printed to default printer "+myPrinter+" !!";
			} catch (Exception pe) {
				pe.printStackTrace();
			}
		} else {
			System.out.println("no printer services found");
			message = "No printer services found!!";
		}
		return message;
	}

}
