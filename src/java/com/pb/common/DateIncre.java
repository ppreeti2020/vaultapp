package com.pb.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * 
 * @author www.javadb.com
 */
public class DateIncre {

	public static String incrementDate(String fromDate, int inc)
			throws ParseException {

		int daysToIncrement = 1;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		cal.setTime(format.parse(fromDate));
		cal.add(cal.DATE, inc);
		//System.out.println("Date after increment: " + cal.getTime());
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String date = format1.format(cal.getTime());
		//System.out.println("new date format:" + date);
		return date;
	}

	public void decrementDate(Calendar cal) {

		int monthsToDecrement = -1;
		cal.add(Calendar.MONTH, monthsToDecrement);
		System.out.println("Date after decrement: " + cal.getTime());

	}
	/**
	 * @param args
	 *            the command line arguments
	 * @throws ParseException
	 */
	/*
	 * public static void main(String[] args) throws ParseException {
	 * 
	 * Calendar cal = Calendar.getInstance(); SimpleDateFormat format1 = new
	 * SimpleDateFormat("dd-MM-yyyy"); cal.setTime(format1.parse("02-12-2012"));
	 * System.out.println("Now : " + cal.getTime()); DateIncre main = new
	 * DateIncre(); for(int i=0;i<7;i++) { main.incrementDate("18-02-2011"); }
	 * main.decrementDate(cal); }
	 */
}
