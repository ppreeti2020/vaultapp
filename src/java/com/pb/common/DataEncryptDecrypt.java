/*
 * Created on Oct 14, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.pb.common;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.*;

import javax.crypto.*;
import javax.crypto.spec.*;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;
/**
 * 
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class DataEncryptDecrypt {
	static private Cipher cipher;
	private static Key key;
	static String keystring="e8ffc7e56311679fe8ffc7e56311679f";
						   //e8ffc7e56311679fe8ffc7e56311679f
	
static {
		
		try {
			cipher = Cipher.getInstance("AES");
			byte[] raw = { (byte) 0xA5, (byte) 0x01, (byte) 0x7B, (byte) 0xE5,
					(byte) 0x23, (byte) 0xCA, (byte) 0xD4, (byte) 0xD2,
					(byte) 0xC6, (byte) 0x5F, (byte) 0x7D, (byte) 0x8B,
					(byte) 0x0B, (byte) 0x9A, (byte) 0x3C, (byte) 0xF1 };
			
			try {
				
				keystring=
						 PropertyUtils.getProperties(
								CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
								CommonConstants.SECRET_KEY);
				
				raw=keystring.getBytes("UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			key = new SecretKeySpec(raw, "AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
	}
	
	public static String encryptData(String aData) throws Exception {
		String result = "";
		try {
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] utf8 = aData.getBytes("UTF8");
			byte[] encryptedData = cipher.doFinal(utf8);
			result = Base64.encode(encryptedData);//this.b64Encoder.encode(encryptedData);
		} 
		catch (InvalidKeyException oException) { 			oException.printStackTrace(); } 
		catch (IllegalBlockSizeException oException) { 		oException.printStackTrace(); } 
		catch (BadPaddingException oException) { 			oException.printStackTrace(); } 
		catch (IOException oException) { 					oException.printStackTrace(); }
		return result;
	
	}
	
	
	public static String decryptData(String aData) throws Exception {
		
		String result = "";
		try {
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] decodedData = Base64.decode(aData);//this.b64Decoder.decodeBuffer(aData);
			byte[] utf8 = cipher.doFinal(decodedData);
			result = new String(utf8, "UTF8");
		} 
		catch (InvalidKeyException oException) { result=aData;			oException.printStackTrace(); } 
		catch (Base64DecodingException oException) { 	result=aData;	oException.printStackTrace(); } 
		catch (IllegalBlockSizeException oException) { 	result=aData;	oException.printStackTrace(); } 
		catch (BadPaddingException oException) { 	result=aData;		oException.printStackTrace(); } 
		catch (UnsupportedEncodingException oException) { result=aData;	oException.printStackTrace(); }
		
	//	System.out.println("decypting data"+aData);
		
	return result;
	}
	
	/**
	 * Return encrypted value for a given String argument.
	 * 
	 * @param str
	 * @return encrypted string value.
	 * @throws Exception
	 */
	
/*	static byte[] ivBytes={ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	static String key="e8ffc7e56311679fe8ffc7e56311679f";
	
	static
	{
		key = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.SECRET_KEY);
	}
	
	public static String encryptData(String text) throws Exception {
		byte[] mes=text.getBytes("UTF-8");
		byte[]	keyBytes = key.getBytes("UTF-8");
	    AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
	    SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
	    Cipher cipher = null;
	    cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	    cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
	    return  new String(cipher.doFinal(mes));
	}

	*//**
	 * Return decrypted value for a given encrypted String argument.
	 * 
	 * @param str
	 * @return decrypted string value.
	 * @throws Exception
	 *//*
	public static String decryptData(String text) throws Exception {
		byte[] mes=text.getBytes();
		byte[]	keyBytes = key.getBytes("UTF-8");
	    AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
	    SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
	    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	    cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
	    return  new String(cipher.doFinal(mes));
	}

	
	 * Return the Cipher object.
	 
	private static Cipher getCipher(String mode) throws Exception {
		// encryption Key in byte array
		
		 * byte[] encryptKey = { (byte) 0x91, (byte) 0x02, (byte) 0x9, (byte)
		 * 0x44, (byte) 0x85, (byte) 0x66, (byte) 0x23, (byte) 0x08};CSR_PROPERTY_FILE_NAME
		 
		String saltKey = PropertyUtils.getProperties(
				CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(
				CommonConstants.SECRET_KEY);
		// String saltKey =
		// "CSR APPS VAULT APPLICATION WITH PITNEY BOWES BUSINESS INSIGHT CO";
		// System.out.println("lenght"+saltKey.length());
		byte[] encryptKey = saltKey.getBytes();
		
		 * KeyGenerator gen = KeyGenerator.getInstance("AES/CTR/PKCS5PADDING");
		 * gen.init(256); SecretKey k = gen.generateKey(); Cipher ciph =
		 * Cipher.getInstance("AES"); ciph.init(Cipher.ENCRYPT_MODE, k);
		 
		// initialization of vector (necessary for CBC mode)
		IvParameterSpec IvParameters = new IvParameterSpec(new byte[] { 92, 48,
				36, 78, 91, 80, 05, 43 });
		// creating a DES key spec from the byte array key
		DESedeKeySpec spec = new DESedeKeySpec(encryptKey);

		// initializing secret key factory for generating DES keys
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");

		// creating a DES SecretKey object
		SecretKey theKey = keyFactory.generateSecret(spec);

		// obtaining a DES Cipher object
		// aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");

		if (mode.equalsIgnoreCase("ENCRYPT_MODE")) {
			// Initializing the cipher and put it into encrypt mode
			cipher.init(Cipher.ENCRYPT_MODE, theKey, IvParameters);
		} else if (mode.equalsIgnoreCase("DECRYPT_MODE")) {
			// Initializing the cipher and put it into decrypt mode
			cipher.init(Cipher.DECRYPT_MODE, theKey, IvParameters);
		}
		return cipher;
	}
*/
	public static void main(String[] args) throws Exception {
		// String str = "h40Mvqy1kRzxCO6kIYv51Q==";
		
		String encrypt=	DataEncryptDecrypt.encryptData( "Isurusam");


		System.out.println(encrypt);



			
			
			String decrypt=DataEncryptDecrypt.decryptData(  encrypt);
			
			System.out.println("Decrypted string"+decrypt);
			
	}
}
