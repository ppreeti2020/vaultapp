package com.pb.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.pb.dao.DBConnectionDAO;

/**
 * Parse an Apache log file with StringTokenizer
 */
public class LogStrTokold implements LogExample {
	DBConnectionDAO dao = new DBConnectionDAO();

	public static boolean contains(String full, String searched) {
		// System.out.println("searched" +searched);
		if (full.indexOf(searched) != -1) {

			return true;
		} else {
			return false;
		}

	}

	public static int StringAccurance(String str, String findMe) {
		// String findMe = "add unique";

		int len = findMe.length();
		int result = 0;
		if (len > 0) {
			int start = str.indexOf(findMe);
			while (start != -1) {
				result++;
				start = str.indexOf(findMe, start + len);
				// System.out.println(start);
			}
		}
		return result;
	}

	public static String returnnoBrackets(String str) {
		String extracted = Messages.getString("LogStrTok.0"); //$NON-NLS-1$
		if (contains(str, Messages.getString("LogStrTok.1")) && contains(str, Messages.getString("LogStrTok.2"))) //$NON-NLS-1$ //$NON-NLS-2$
			extracted = str.substring(str.indexOf('[') + 1, str.indexOf(']'));
		else if (contains(str, Messages.getString("LogStrTok.3")) && !contains(str, Messages.getString("LogStrTok.4"))) //$NON-NLS-1$ //$NON-NLS-2$
			extracted = str.substring(str.indexOf('[') + 1);
		else if (!contains(str, Messages.getString("LogStrTok.5")) && contains(str, Messages.getString("LogStrTok.6"))) //$NON-NLS-1$ //$NON-NLS-2$
			extracted = str.substring(str.indexOf(']'));
		return extracted;
	}

	public void parseLogFile() throws Exception {
		DataEncryptDecrypt data = new DataEncryptDecrypt();
		String str = readFile();
		Scanner scanner = new Scanner(str);

		scanner.findInLine(Messages.getString("LogStrTok.7")); //$NON-NLS-1$

		if (scanner.hasNext())
			System.out.println(Messages.getString("LogStrTok.8")); //$NON-NLS-1$
		else
			System.out.println(Messages.getString("LogStrTok.9")); //$NON-NLS-1$
		String startingTime = Messages.getString("LogStrTok.10"); //$NON-NLS-1$
		String date = Messages.getString("LogStrTok.11"); //$NON-NLS-1$
		String jrnlFile = Messages.getString("LogStrTok.12"); //$NON-NLS-1$
		String compressingFile = Messages.getString("LogStrTok.13"); //$NON-NLS-1$
		String profile = Messages.getString("LogStrTok.14"); //$NON-NLS-1$
		String format = Messages.getString("LogStrTok.15"); //$NON-NLS-1$
		String pages = Messages.getString("LogStrTok.16"); //$NON-NLS-1$
		String totalRead = Messages.getString("LogStrTok.17"); //$NON-NLS-1$
		String totalWritten = Messages.getString("LogStrTok.18"); //$NON-NLS-1$
		String finalRatio = Messages.getString("LogStrTok.19"); //$NON-NLS-1$
		String methodName = Messages.getString("LogStrTok.20"); //$NON-NLS-1$
		String documents = Messages.getString("LogStrTok.21"); //$NON-NLS-1$
		String documentPages = Messages.getString("LogStrTok.22"); //$NON-NLS-1$
		String ignorePages = Messages.getString("LogStrTok.23"); //$NON-NLS-1$
		String errors[] = null;
		String totalErrors = Messages.getString("LogStrTok.24"); //$NON-NLS-1$
		String wrote[] = null;
		String sizes[] = null;
		String indexingFile = Messages.getString("LogStrTok.25"); //$NON-NLS-1$
		String indexFile[] = null;
		String indexFileSize[] = null;
		String indexDiskRead[] = null;
		String indexDiskWrite[] = null;
		String indexStackDepth[] = null;
		String indexCacheReadHit[] = null;
		String indexCacheReadMiss[] = null;
		String indexCacheWriteHit[] = null;
		String indexCacheWriteMiss[] = null;
		String indexAddDuplicate[] = null;
		String indexAddUnique[] = null;
		int compCount = 0;
		int errorcount = 0;
		int formatcount = 0;
		int filesizecount = 0;
		String indexstartTime = Messages.getString("LogStrTok.26"); //$NON-NLS-1$
		String indexFinishingTime = Messages.getString("LogStrTok.27"); //$NON-NLS-1$
		String resstartTime = Messages.getString("LogStrTok.28"); //$NON-NLS-1$
		String resFinishingTime = Messages.getString("LogStrTok.29"); //$NON-NLS-1$
		if (str.contains(Messages.getString("LogStrTok.30"))) { //$NON-NLS-1$
			// System.out.println("date:"+str.split("date")[1].split(",")[0]+"time:"+str.split("open log")[0].split(" ")[0]);
			date = returnnoBrackets(str.split(Messages.getString("LogStrTok.31"))[1].split(Messages.getString("LogStrTok.32"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
			startingTime = str.split(Messages.getString("LogStrTok.33"))[0].split(Messages.getString("LogStrTok.34"))[0]; //$NON-NLS-1$ //$NON-NLS-2$
			System.out.println(Messages.getString("LogStrTok.35") + date + Messages.getString("LogStrTok.36") + startingTime); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (str.contains(Messages.getString("LogStrTok.37"))) { //$NON-NLS-1$
			System.out.println(Messages.getString("LogStrTok.38") //$NON-NLS-1$
					+ str.split(Messages.getString("LogStrTok.39"))[1].split(Messages.getString("LogStrTok.40"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
			jrnlFile = returnnoBrackets(str.split(Messages.getString("LogStrTok.41"))[1].split(Messages.getString("LogStrTok.42"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
			System.out.println(Messages.getString("LogStrTok.43") + jrnlFile); //$NON-NLS-1$
		}
		if (str.contains(Messages.getString("LogStrTok.44"))) { //$NON-NLS-1$
			compCount = StringAccurance(str, Messages.getString("LogStrTok.45")); //$NON-NLS-1$
			for (int i = 1; i <= compCount; i++) {
				// System.out.println(compCount+"compressing file:"+str.split("compressing")[i].split("profile")[0]);
				compressingFile = returnnoBrackets(str.split(Messages.getString("LogStrTok.46"))[i] //$NON-NLS-1$
						.split(Messages.getString("LogStrTok.47"))[0]); //$NON-NLS-1$
				System.out.println(Messages.getString("LogStrTok.48") + compressingFile); //$NON-NLS-1$
				String comp = str.split(Messages.getString("LogStrTok.49"))[i]; //$NON-NLS-1$
				if (comp.contains(Messages.getString("LogStrTok.50"))) { //$NON-NLS-1$
					// System.out.println("profile"+str.split("compressing")[i].split("profile")[1].split("format")[0]);
					profile = returnnoBrackets(str.split(Messages.getString("LogStrTok.51"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.52"))[1].split(Messages.getString("LogStrTok.53"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(Messages.getString("LogStrTok.54") + profile); //$NON-NLS-1$
				}
				if (comp.contains(Messages.getString("LogStrTok.55"))) { //$NON-NLS-1$
					// System.out.println("format:"+str.split("compressing")[i].split("profile")[1].split("format")[1].split("\\n")[0]);
					format = returnnoBrackets(str.split(Messages.getString("LogStrTok.56"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.57"))[1].split(Messages.getString("LogStrTok.58"))[1] //$NON-NLS-1$ //$NON-NLS-2$
							.split(Messages.getString("LogStrTok.59"))[0]); //$NON-NLS-1$
					System.out.println(Messages.getString("LogStrTok.60") + format); //$NON-NLS-1$
				}
				if (comp.contains(Messages.getString("LogStrTok.61"))) { //$NON-NLS-1$
					// System.out.println("pages:"+str.split("compressing")[i].split("pages")[1].split(" t")[0]);
					pages = returnnoBrackets(str.split(Messages.getString("LogStrTok.62"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.63"))[1].split(Messages.getString("LogStrTok.64"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(Messages.getString("LogStrTok.65") + pages); //$NON-NLS-1$
				}
				if (comp.contains(Messages.getString("LogStrTok.66"))) { //$NON-NLS-1$
					// System.out.println("total read:"+str.split("compressing")[i].split("total read:")[1].split(" t")[0]);
					totalRead = returnnoBrackets(str.split(Messages.getString("LogStrTok.67"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.68"))[1].split(Messages.getString("LogStrTok.69"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(Messages.getString("LogStrTok.70") + totalRead); //$NON-NLS-1$
				}
				if (comp.contains(Messages.getString("LogStrTok.71"))) { //$NON-NLS-1$
					// System.out.println("total written:"+str.split("compressing")[i].split("total written:")[1].split(" f")[0]);
					totalWritten = returnnoBrackets(str.split(Messages.getString("LogStrTok.72"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.73"))[1].split(Messages.getString("LogStrTok.74"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(Messages.getString("LogStrTok.75") + totalWritten); //$NON-NLS-1$
				}
				if (comp.contains(Messages.getString("LogStrTok.76"))) { //$NON-NLS-1$
					// System.out.println("final ratio:"+str.split("compressing")[i].split("final ratio:")[1].split("\n")[0]);
					finalRatio = returnnoBrackets(str.split(Messages.getString("LogStrTok.77"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.78"))[1].split(Messages.getString("LogStrTok.79"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(Messages.getString("LogStrTok.80") + finalRatio); //$NON-NLS-1$
				}
				if (comp.contains(Messages.getString("LogStrTok.81"))) { //$NON-NLS-1$
					// System.out.println("method"+str.split("compressing")[i].split("method")[1].split("]")[0]);
					methodName = returnnoBrackets(str.split(Messages.getString("LogStrTok.82"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.83"))[1].split(Messages.getString("LogStrTok.84"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(Messages.getString("LogStrTok.85") + methodName); //$NON-NLS-1$
				}
				if (comp.contains(Messages.getString("LogStrTok.86"))) { //$NON-NLS-1$
					// System.out.println("documents :"+str.split("compressing")[i].split("method")[1].split("]")[1].split("\\[")[1]);
					documents = returnnoBrackets(str.split(Messages.getString("LogStrTok.87"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.88"))[1].split(Messages.getString("LogStrTok.89"))[1] //$NON-NLS-1$ //$NON-NLS-2$
							.split(Messages.getString("LogStrTok.90"))[0]); //$NON-NLS-1$
					System.out.println(Messages.getString("LogStrTok.91") + documents); //$NON-NLS-1$
				}
				if (comp.contains(Messages.getString("LogStrTok.92"))) { //$NON-NLS-1$
					// System.out.println("documents :"+str.split("compressing")[i].split("method")[1].split("]")[1].split("\\[")[1]);
					documentPages = returnnoBrackets(str.split(Messages.getString("LogStrTok.93"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.94"))[0].split(Messages.getString("LogStrTok.95"))[1]); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(Messages.getString("LogStrTok.96") + documentPages); //$NON-NLS-1$
				}
				if (comp.contains(Messages.getString("LogStrTok.97"))) { //$NON-NLS-1$
					// System.out.println("documents :"+str.split("compressing")[i].split("method")[1].split("]")[1].split("\\[")[1]);
					ignorePages = returnnoBrackets(str.split(Messages.getString("LogStrTok.98"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.99"))[1].split(Messages.getString("LogStrTok.100"))[1]); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(Messages.getString("LogStrTok.101") + ignorePages); //$NON-NLS-1$
				}
				if (comp.contains(Messages.getString("LogStrTok.102"))) { //$NON-NLS-1$
					errorcount = StringAccurance(comp, Messages.getString("LogStrTok.103")); //$NON-NLS-1$
					System.out.println(Messages.getString("LogStrTok.104") + errorcount); //$NON-NLS-1$
					errors = new String[errorcount];
					for (int j = 1; j <= errorcount; j++) {
						System.out.println(Messages.getString("LogStrTok.105") + comp.split(Messages.getString("LogStrTok.106"))[j]); //$NON-NLS-1$ //$NON-NLS-2$
						errors[j - 1] = comp.split(Messages.getString("LogStrTok.107"))[j]; //$NON-NLS-1$
					}
				}
				if (comp.contains(Messages.getString("LogStrTok.108"))) { //$NON-NLS-1$
					formatcount = StringAccurance(str.split(Messages.getString("LogStrTok.109"))[1], //$NON-NLS-1$
							Messages.getString("LogStrTok.110")); //$NON-NLS-1$
					System.out.println(Messages.getString("LogStrTok.111") + formatcount); //$NON-NLS-1$
					int x = 0;
					wrote = new String[formatcount];
					sizes = new String[formatcount];
					System.out.println(Messages.getString("LogStrTok.112") //$NON-NLS-1$
							+ comp.split(Messages.getString("LogStrTok.113"))[1].split(Messages.getString("LogStrTok.114"))[0] //$NON-NLS-1$ //$NON-NLS-2$
									.split(Messages.getString("LogStrTok.115"))[1].split(Messages.getString("LogStrTok.116"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
					resstartTime = comp.split(Messages.getString("LogStrTok.117"))[1].split(Messages.getString("LogStrTok.118"))[0] //$NON-NLS-1$ //$NON-NLS-2$
							.split(Messages.getString("LogStrTok.119"))[1].split(Messages.getString("LogStrTok.120"))[0]; //$NON-NLS-1$ //$NON-NLS-2$
					for (int j = 1; j <= formatcount; j++) {
						// System.out.println("wrote :"+comp.split("format")[1].split("wrote")[j].split("\n")[0].split("size")[0]);
						// System.out.println("size :"+comp.split("format")[1].split("wrote")[j].split("\n")[0].split("size")[1]);

						wrote[x] = returnnoBrackets(comp.split(Messages.getString("LogStrTok.121"))[1] //$NON-NLS-1$
								.split(Messages.getString("LogStrTok.122"))[j].split(Messages.getString("LogStrTok.123"))[0].split(Messages.getString("LogStrTok.124"))[0]); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						System.out.println(Messages.getString("LogStrTok.125") + wrote[x]); //$NON-NLS-1$
						sizes[x] = returnnoBrackets(comp.split(Messages.getString("LogStrTok.126"))[1] //$NON-NLS-1$
								.split(Messages.getString("LogStrTok.127"))[j].split(Messages.getString("LogStrTok.128"))[0].split(Messages.getString("LogStrTok.129"))[1]); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						System.out.println(Messages.getString("LogStrTok.130") + sizes[x]); //$NON-NLS-1$
						x++;
					}
					System.out
							.println(Messages.getString("LogStrTok.131") //$NON-NLS-1$
									+ comp.split(Messages.getString("LogStrTok.132"))[1].split(Messages.getString("LogStrTok.133"))[formatcount - 1] //$NON-NLS-1$ //$NON-NLS-2$
											.split(Messages.getString("LogStrTok.134"))[1].split(Messages.getString("LogStrTok.135"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
					resFinishingTime = comp.split(Messages.getString("LogStrTok.136"))[1].split(Messages.getString("LogStrTok.137"))[formatcount - 1] //$NON-NLS-1$ //$NON-NLS-2$
							.split(Messages.getString("LogStrTok.138"))[1].split(Messages.getString("LogStrTok.139"))[0]; //$NON-NLS-1$ //$NON-NLS-2$

				}
				if (comp.contains(Messages.getString("LogStrTok.140"))) { //$NON-NLS-1$
					// System.out.println("start indexing document file :"+str.split("compressing")[i].split("start indexing document file")[1].split("]")[0]);
					indexingFile = returnnoBrackets(str.split(Messages.getString("LogStrTok.141"))[i] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.142"))[1] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.143"))[0]); //$NON-NLS-1$
					System.out.println(Messages.getString("LogStrTok.144") + indexingFile); //$NON-NLS-1$

					filesizecount = StringAccurance(str, Messages.getString("LogStrTok.145")); //$NON-NLS-1$
					System.out.println(Messages.getString("LogStrTok.146") + filesizecount); //$NON-NLS-1$
					indexFile = new String[filesizecount];
					indexFileSize = new String[filesizecount];
					indexDiskRead = new String[filesizecount];
					indexDiskWrite = new String[filesizecount];
					indexStackDepth = new String[filesizecount];
					indexCacheReadHit = new String[filesizecount];
					indexCacheReadMiss = new String[filesizecount];
					indexCacheWriteHit = new String[filesizecount];
					indexAddDuplicate = new String[filesizecount];
					indexAddUnique = new String[filesizecount];
					indexCacheWriteMiss = new String[filesizecount];
					if (comp.contains(Messages.getString("LogStrTok.147"))) { //$NON-NLS-1$
						System.out.println(Messages.getString("LogStrTok.148") //$NON-NLS-1$
								+ str.split(Messages.getString("LogStrTok.149"))[1] //$NON-NLS-1$
										.split(Messages.getString("LogStrTok.150"))[1] //$NON-NLS-1$
										.split(Messages.getString("LogStrTok.151"))[0] //$NON-NLS-1$
										.split(Messages.getString("LogStrTok.152"))[0].split(Messages.getString("LogStrTok.153"))[1] //$NON-NLS-1$ //$NON-NLS-2$
										.split(Messages.getString("LogStrTok.154"))[0]); //$NON-NLS-1$
						indexstartTime = str
								.split(Messages.getString("LogStrTok.155"))[1] //$NON-NLS-1$
								.split(Messages.getString("LogStrTok.156"))[1] //$NON-NLS-1$
								.split(Messages.getString("LogStrTok.157"))[0].split(Messages.getString("LogStrTok.158"))[0] //$NON-NLS-1$ //$NON-NLS-2$
								.split(Messages.getString("LogStrTok.159"))[1].split(Messages.getString("LogStrTok.160"))[0]; //$NON-NLS-1$ //$NON-NLS-2$
						for (int k = 0; k < filesizecount; k++) {

							System.out
									.println(Messages.getString("LogStrTok.161") //$NON-NLS-1$
											+ str.split(Messages.getString("LogStrTok.162"))[1] //$NON-NLS-1$
													.split(Messages.getString("LogStrTok.163"))[1] //$NON-NLS-1$
													.split(Messages.getString("LogStrTok.164"))[k] //$NON-NLS-1$
													.split(Messages.getString("LogStrTok.165"))[0] //$NON-NLS-1$
													.split(Messages.getString("LogStrTok.166"))[1].split(Messages.getString("LogStrTok.167"))[1]); //$NON-NLS-1$ //$NON-NLS-2$
							indexFile[k] = str
									.split(Messages.getString("LogStrTok.168"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.169"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.170"))[k].split(Messages.getString("LogStrTok.171"))[0] //$NON-NLS-1$ //$NON-NLS-2$
									.split(Messages.getString("LogStrTok.172"))[1].split(Messages.getString("LogStrTok.173"))[1]; //$NON-NLS-1$ //$NON-NLS-2$
							// System.out.println("file size :"+str.split("start indexing document file")[1].split("flushing index cache")[1].split("add unique")[k].split("file size")[1].split("\n")[0]);
							indexFileSize[k] = returnnoBrackets(str
									.split(Messages.getString("LogStrTok.174"))[i] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.175"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.176"))[0]); //$NON-NLS-1$
							System.out.println(Messages.getString("LogStrTok.177") //$NON-NLS-1$
									+ indexFileSize[k]);
							// System.out.println("disk read :"+str.split("start indexing document file")[1].split("flushing index cache")[1].split("add unique")[k].split("disk read")[1].split("\n")[0]);
							indexDiskRead[k] = returnnoBrackets(str
									.split(Messages.getString("LogStrTok.178"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.179"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.180"))[k].split(Messages.getString("LogStrTok.181"))[1] //$NON-NLS-1$ //$NON-NLS-2$
									.split(Messages.getString("LogStrTok.182"))[0]); //$NON-NLS-1$
							System.out.println(Messages.getString("LogStrTok.183") //$NON-NLS-1$
									+ indexDiskRead[k]);
							// System.out.println("disk write :"+str.split("start indexing document file")[1].split("flushing index cache")[1].split("add unique")[k].split("disk write")[1].split("\n")[0]);
							indexDiskWrite[k] = returnnoBrackets(str
									.split(Messages.getString("LogStrTok.184"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.185"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.186"))[k].split(Messages.getString("LogStrTok.187"))[1] //$NON-NLS-1$ //$NON-NLS-2$
									.split(Messages.getString("LogStrTok.188"))[0]); //$NON-NLS-1$
							System.out.println(Messages.getString("LogStrTok.189") //$NON-NLS-1$
									+ indexDiskWrite[k]);
							// System.out.println("stack depth :"+str.split("start indexing document file")[1].split("flushing index cache")[1].split("add unique")[k].split("stack depth")[1].split("\n")[0]);
							indexStackDepth[k] = returnnoBrackets(str
									.split(Messages.getString("LogStrTok.190"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.191"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.192"))[k] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.193"))[1].split(Messages.getString("LogStrTok.194"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
							System.out.println(Messages.getString("LogStrTok.195") //$NON-NLS-1$
									+ indexStackDepth[k]);
							// System.out.println("cache read hit :"+str.split("start indexing document file")[1].split("flushing index cache")[1].split("add unique")[k].split("cache read hit")[1].split("\n")[0]);
							indexCacheReadHit[k] = returnnoBrackets(str
									.split(Messages.getString("LogStrTok.196"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.197"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.198"))[k] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.199"))[1].split(Messages.getString("LogStrTok.200"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
							System.out.println(Messages.getString("LogStrTok.201") //$NON-NLS-1$
									+ indexCacheReadHit[k]);
							// System.out.println("cache read miss :"+str.split("start indexing document file")[1].split("flushing index cache")[1].split("add unique")[k].split("cache read miss")[1].split("\n")[0]);
							indexCacheReadMiss[k] = returnnoBrackets(str
									.split(Messages.getString("LogStrTok.202"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.203"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.204"))[k] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.205"))[1].split(Messages.getString("LogStrTok.206"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
							System.out.println(Messages.getString("LogStrTok.207") //$NON-NLS-1$
									+ indexCacheReadMiss[k]);
							// System.out.println("cache write hit :"+str.split("start indexing document file")[1].split("flushing index cache")[1].split("add unique")[k].split("cache write hit")[1].split("\n")[0]);
							indexCacheWriteHit[k] = returnnoBrackets(str
									.split(Messages.getString("LogStrTok.208"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.209"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.210"))[k] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.211"))[1].split(Messages.getString("LogStrTok.212"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
							System.out.println(Messages.getString("LogStrTok.213") //$NON-NLS-1$
									+ indexCacheWriteHit[k]);

							indexCacheWriteMiss[k] = returnnoBrackets(str
									.split(Messages.getString("LogStrTok.214"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.215"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.216"))[k] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.217"))[1].split(Messages.getString("LogStrTok.218"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
							System.out.println(Messages.getString("LogStrTok.219") //$NON-NLS-1$
									+ indexCacheWriteMiss[k]);
							if (str.split(Messages.getString("LogStrTok.220"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.221"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.222"))[k] //$NON-NLS-1$
									.contains(Messages.getString("LogStrTok.223"))) { //$NON-NLS-1$
								indexAddDuplicate[k] = returnnoBrackets(str
										.split(Messages.getString("LogStrTok.224"))[1] //$NON-NLS-1$
										.split(Messages.getString("LogStrTok.225"))[1] //$NON-NLS-1$
										.split(Messages.getString("LogStrTok.226"))[k] //$NON-NLS-1$
										.split(Messages.getString("LogStrTok.227"))[1].split(Messages.getString("LogStrTok.228"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
								System.out.println(Messages.getString("LogStrTok.229") //$NON-NLS-1$
										+ indexAddDuplicate[k]);
							} else
								indexAddDuplicate[k] = Messages.getString("LogStrTok.230"); //$NON-NLS-1$
							// System.out.println("add unique :"+str.split("start indexing document file")[1].split("flushing index cache")[1].split("add unique")[k+1].split("\n")[0]);
							indexAddUnique[k] = returnnoBrackets(str
									.split(Messages.getString("LogStrTok.231"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.232"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.233"))[k + 1].split(Messages.getString("LogStrTok.234"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
							System.out.println(Messages.getString("LogStrTok.235") //$NON-NLS-1$
									+ indexAddUnique[k]);

						}
						if (comp.contains(Messages.getString("LogStrTok.236"))) { //$NON-NLS-1$
							System.out
									.println(Messages.getString("LogStrTok.237") //$NON-NLS-1$
											+ str.split(Messages.getString("LogStrTok.238"))[1] //$NON-NLS-1$
													.split(Messages.getString("LogStrTok.239"))[1] //$NON-NLS-1$
													.split(Messages.getString("LogStrTok.240"))[filesizecount] //$NON-NLS-1$
													.split(Messages.getString("LogStrTok.241"))[0] //$NON-NLS-1$
													.split(Messages.getString("LogStrTok.242"))[1].split(Messages.getString("LogStrTok.243"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
							indexFinishingTime = str
									.split(Messages.getString("LogStrTok.244"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.245"))[1] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.246"))[filesizecount] //$NON-NLS-1$
									.split(Messages.getString("LogStrTok.247"))[0].split(Messages.getString("LogStrTok.248"))[1] //$NON-NLS-1$ //$NON-NLS-2$
									.split(Messages.getString("LogStrTok.249"))[0]; //$NON-NLS-1$
						}
					}
				}
				if (str.contains(Messages.getString("LogStrTok.250"))) { //$NON-NLS-1$
					// System.out.println("errors:"+str.split("finished indexing document file")[1].split(",")[1].split("errors")[0]);
					totalErrors = returnnoBrackets(str
							.split(Messages.getString("LogStrTok.251"))[1] //$NON-NLS-1$
							.split(Messages.getString("LogStrTok.252"))[1].split(Messages.getString("LogStrTok.253"))[0]); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(Messages.getString("LogStrTok.254") + totalErrors); //$NON-NLS-1$
				}

			}
			try {
				Statement stmt = dao.getDBConnection().createStatement();
				String errorOccur = Messages.getString("LogStrTok.255"); //$NON-NLS-1$
				if (errors != null) {
					errorOccur = Messages.getString("LogStrTok.256"); //$NON-NLS-1$
				}
				Long logId = 0l;
				System.out.println(Messages.getString("LogStrTok.257") + compressingFile); //$NON-NLS-1$
				String hash = AeSimpleSHA1.SHA1(data
						.encryptData(compressingFile)
						+ Messages.getString("LogStrTok.258") //$NON-NLS-1$
						+ data.encryptData(profile)
						+ Messages.getString("LogStrTok.259") //$NON-NLS-1$
						+ data.encryptData(format)
						+ Messages.getString("LogStrTok.260") //$NON-NLS-1$
						+ data.encryptData(totalRead)
						+ Messages.getString("LogStrTok.261") //$NON-NLS-1$
						+ data.encryptData(totalWritten)
						+ Messages.getString("LogStrTok.262") //$NON-NLS-1$
						+ data.encryptData(finalRatio)
						+ Messages.getString("LogStrTok.263") //$NON-NLS-1$
						+ data.encryptData(methodName)
						+ Messages.getString("LogStrTok.264") //$NON-NLS-1$
						+ data.encryptData(jrnlFile)
						+ Messages.getString("LogStrTok.265") //$NON-NLS-1$
						+ data.encryptData(documents)
						+ Messages.getString("LogStrTok.266") //$NON-NLS-1$
						+ data.encryptData(documentPages)
						+ Messages.getString("LogStrTok.267") //$NON-NLS-1$
						+ data.encryptData(ignorePages)
						+ Messages.getString("LogStrTok.268") //$NON-NLS-1$
						+ data.encryptData(errorOccur)
						+ Messages.getString("LogStrTok.269") //$NON-NLS-1$
						+ data.encryptData(date)
						+ Messages.getString("LogStrTok.270") //$NON-NLS-1$
						+ data.encryptData(startingTime));
				String query = Messages.getString("LogStrTok.271") //$NON-NLS-1$
						+ data.encryptData(compressingFile)
						+ Messages.getString("LogStrTok.272") //$NON-NLS-1$
						+ data.encryptData(profile)
						+ Messages.getString("LogStrTok.273") //$NON-NLS-1$
						+ data.encryptData(format)
						+ Messages.getString("LogStrTok.274") //$NON-NLS-1$
						+ data.encryptData(totalRead)
						+ Messages.getString("LogStrTok.275") //$NON-NLS-1$
						+ data.encryptData(totalWritten)
						+ Messages.getString("LogStrTok.276") //$NON-NLS-1$
						+ data.encryptData(finalRatio)
						+ Messages.getString("LogStrTok.277") //$NON-NLS-1$
						+ data.encryptData(methodName)
						+ Messages.getString("LogStrTok.278") //$NON-NLS-1$
						+ data.encryptData(jrnlFile)
						+ Messages.getString("LogStrTok.279") //$NON-NLS-1$
						+ data.encryptData(documents)
						+ Messages.getString("LogStrTok.280") //$NON-NLS-1$
						+ data.encryptData(documentPages)
						+ Messages.getString("LogStrTok.281") //$NON-NLS-1$
						+ data.encryptData(ignorePages)
						+ Messages.getString("LogStrTok.282") //$NON-NLS-1$
						+ data.encryptData(errorOccur)
						+ Messages.getString("LogStrTok.283") //$NON-NLS-1$
						+ hash
						+ Messages.getString("LogStrTok.284") //$NON-NLS-1$
						+ data.encryptData(date)
						+ Messages.getString("LogStrTok.285") //$NON-NLS-1$
						+ data.encryptData(startingTime)
						+ Messages.getString("LogStrTok.286") //$NON-NLS-1$
						+ data.encryptData(date)
						+ Messages.getString("LogStrTok.287") //$NON-NLS-1$
						+ data.encryptData(indexFinishingTime) + Messages.getString("LogStrTok.288"); //$NON-NLS-1$

				stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
				ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					logId = rs.getLong(1);
				}
				if (logId != 0) {
					for (int k = 0; k < filesizecount; k++) {
						System.out.println(Messages.getString("LogStrTok.289") + logId); //$NON-NLS-1$
						createIndexing(indexFile[k], indexFileSize[k],
								indexDiskRead[k], indexDiskWrite[k],
								indexStackDepth[k], indexCacheReadHit[k],
								indexCacheReadMiss[k], indexCacheWriteHit[k],
								indexCacheWriteMiss[k], indexAddDuplicate[k],
								indexAddUnique[k], date, indexstartTime,
								indexFinishingTime, logId);
						System.out.println(Messages.getString("LogStrTok.290") + logId); //$NON-NLS-1$
					}
					for (int j = 0; j < formatcount; j++) {
						System.out.println(Messages.getString("LogStrTok.291") + logId); //$NON-NLS-1$
						createResources(wrote[j], sizes[j], date, resstartTime,
								resFinishingTime, logId);
						System.out.println(Messages.getString("LogStrTok.292") + logId); //$NON-NLS-1$
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void createIndexing(String indexFile, String indexFileSize,
			String indexDiskRead, String indexDiskWrite,
			String indexStackDepth, String indexCacheReadHit,
			String indexCacheReadMiss, String indexCacheWriteHit,
			String indexCacheWriteMiss, String indexAddDuplicate,
			String indexAddUnique, String date, String startTime,
			String endTime, Long logId) throws NoSuchAlgorithmException,
			UnsupportedEncodingException, Exception {
		System.out.println(Messages.getString("LogStrTok.293") + indexFile); //$NON-NLS-1$
		DataEncryptDecrypt data = new DataEncryptDecrypt();
		Statement stmt1 = dao.getDBConnection().createStatement();
		String hash = AeSimpleSHA1.SHA1(data.encryptData(indexFile) + Messages.getString("LogStrTok.294") //$NON-NLS-1$
				+ data.encryptData(indexFileSize) + Messages.getString("LogStrTok.295") //$NON-NLS-1$
				+ data.encryptData(indexDiskRead) + Messages.getString("LogStrTok.296") //$NON-NLS-1$
				+ data.encryptData(indexDiskWrite) + Messages.getString("LogStrTok.297") //$NON-NLS-1$
				+ data.encryptData(indexStackDepth) + Messages.getString("LogStrTok.298") //$NON-NLS-1$
				+ data.encryptData(indexCacheReadHit) + Messages.getString("LogStrTok.299") //$NON-NLS-1$
				+ data.encryptData(indexCacheReadMiss) + Messages.getString("LogStrTok.300") //$NON-NLS-1$
				+ data.encryptData(indexCacheWriteHit) + Messages.getString("LogStrTok.301") //$NON-NLS-1$
				+ data.encryptData(indexCacheWriteMiss) + Messages.getString("LogStrTok.302") //$NON-NLS-1$
				+ data.encryptData(indexAddDuplicate) + Messages.getString("LogStrTok.303") //$NON-NLS-1$
				+ data.encryptData(indexAddUnique) + Messages.getString("LogStrTok.304") //$NON-NLS-1$
				+ data.encryptData(date) + Messages.getString("LogStrTok.305") + data.encryptData(startTime) //$NON-NLS-1$
				+ Messages.getString("LogStrTok.306") + logId); //$NON-NLS-1$
		System.out.println(Messages.getString("LogStrTok.307") + hash); //$NON-NLS-1$
		String query1 = Messages.getString("LogStrTok.308") //$NON-NLS-1$
				+ data.encryptData(indexFile)
				+ Messages.getString("LogStrTok.309") //$NON-NLS-1$
				+ data.encryptData(indexFileSize)
				+ Messages.getString("LogStrTok.310") //$NON-NLS-1$
				+ data.encryptData(indexDiskRead)
				+ Messages.getString("LogStrTok.311") //$NON-NLS-1$
				+ data.encryptData(indexDiskWrite)
				+ Messages.getString("LogStrTok.312") //$NON-NLS-1$
				+ data.encryptData(indexStackDepth)
				+ Messages.getString("LogStrTok.313") //$NON-NLS-1$
				+ data.encryptData(indexCacheReadHit)
				+ Messages.getString("LogStrTok.314") //$NON-NLS-1$
				+ data.encryptData(indexCacheReadMiss)
				+ Messages.getString("LogStrTok.315") //$NON-NLS-1$
				+ data.encryptData(indexCacheWriteHit)
				+ Messages.getString("LogStrTok.316") //$NON-NLS-1$
				+ data.encryptData(indexCacheWriteMiss)
				+ Messages.getString("LogStrTok.317") //$NON-NLS-1$
				+ data.encryptData(indexAddDuplicate)
				+ Messages.getString("LogStrTok.318") //$NON-NLS-1$
				+ data.encryptData(indexAddUnique)
				+ Messages.getString("LogStrTok.319") //$NON-NLS-1$
				+ hash
				+ Messages.getString("LogStrTok.320") //$NON-NLS-1$
				+ data.encryptData(date)
				+ Messages.getString("LogStrTok.321") //$NON-NLS-1$
				+ data.encryptData(startTime)
				+ Messages.getString("LogStrTok.322") //$NON-NLS-1$
				+ data.encryptData(date)
				+ Messages.getString("LogStrTok.323") //$NON-NLS-1$
				+ data.encryptData(endTime) + Messages.getString("LogStrTok.324") + logId + Messages.getString("LogStrTok.325"); //$NON-NLS-1$ //$NON-NLS-2$

		stmt1.execute(query1);
	}

	public void createResources(String resourceName, String resourceSize,
			String date, String startTime, String endTime, Long logId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException,
			Exception {
		System.out.println(Messages.getString("LogStrTok.326") + resourceName); //$NON-NLS-1$
		DataEncryptDecrypt data = new DataEncryptDecrypt();
		Statement stmt1 = dao.getDBConnection().createStatement();
		String hash = AeSimpleSHA1.SHA1(data.encryptData(resourceName) + Messages.getString("LogStrTok.327") //$NON-NLS-1$
				+ data.encryptData(resourceSize) + Messages.getString("LogStrTok.328") + data.encryptData(date) //$NON-NLS-1$
				+ Messages.getString("LogStrTok.329") + data.encryptData(startTime) + Messages.getString("LogStrTok.330") + logId); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println(Messages.getString("LogStrTok.331") + hash); //$NON-NLS-1$
		String query1 = Messages.getString("LogStrTok.332") //$NON-NLS-1$
				+ data.encryptData(resourceName)
				+ Messages.getString("LogStrTok.333") //$NON-NLS-1$
				+ data.encryptData(resourceSize)
				+ Messages.getString("LogStrTok.334") //$NON-NLS-1$
				+ hash
				+ Messages.getString("LogStrTok.335") //$NON-NLS-1$
				+ data.encryptData(date)
				+ Messages.getString("LogStrTok.336") //$NON-NLS-1$
				+ data.encryptData(startTime)
				+ Messages.getString("LogStrTok.337") //$NON-NLS-1$
				+ data.encryptData(date)
				+ Messages.getString("LogStrTok.338") //$NON-NLS-1$
				+ data.encryptData(endTime) + Messages.getString("LogStrTok.339") + logId + Messages.getString("LogStrTok.340"); //$NON-NLS-1$ //$NON-NLS-2$

		stmt1.execute(query1);
	}

	public static String readFile() {

		// e2loaderd.20110802.115132.4584
		File file = new File(Messages.getString("LogStrTok.341")); //$NON-NLS-1$

		StringBuffer contents = new StringBuffer();

		BufferedReader reader = null;

		try {

			reader = new BufferedReader(new FileReader(file));

			String text = null;

			// repeat until all lines is read

			while ((text = reader.readLine()) != null) {

				contents.append(text).append(
						System.getProperty(Messages.getString("LogStrTok.342"))); //$NON-NLS-1$

			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (reader != null) {

					reader.close();

				}

			} catch (IOException e) {

				e.printStackTrace();

			}

		}
		return contents.toString();
	}

}

/**
 * Common fields for Apache Log demo.
 */
interface LogExample {
	/** The number of fields that must be found. */
	public static final int NUM_FIELDS = 9;

	/** The sample log entry to be parsed. */
	public static final String logEntryLine = Messages.getString("LogStrTok.343"); //$NON-NLS-1$

}