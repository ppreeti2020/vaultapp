package com.pb.common;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.DateFormatSymbols;

public class GetDay {

	public static boolean getDay(int day1, int day2) {
		boolean b = false;
		String dayNames[] = new DateFormatSymbols().getWeekdays();
		Calendar date2 = Calendar.getInstance();
		for (int i = day1; i <= day2; i++) {
			String day = returnday(i);
			if (day.equalsIgnoreCase(dayNames[date2.get(Calendar.DAY_OF_WEEK)])) {
				System.out.println("Today is a "
						+ dayNames[date2.get(Calendar.DAY_OF_WEEK)]);
				b = true;
			}
		}
		return b;
	}

	public static String returnday(int first) {
		Calendar now = Calendar.getInstance();

		String[] strDays = new String[] { "Monday", "Tuesday", "Wednesday",
				"Thursday", "Friday", "Saturday", "Sunday" };
		// Day_OF_WEEK starts from 1 while array index starts from 0
		System.out.println("Current day is : " + strDays[first - 1]);
		return strDays[first - 1];
	}
}
