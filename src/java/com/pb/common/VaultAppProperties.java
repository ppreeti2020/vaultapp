package com.pb.common;

import java.util.Properties;

public class VaultAppProperties {
	private static VaultAppProperties mailMergeProperties;
	Properties properties = null;

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	private VaultAppProperties() {
		// TODO Auto-generated constructor stub

	}

	static {
		mailMergeProperties = new VaultAppProperties();

		Properties properties = PropertyUtils
				.getProperties("mailmerge.properties");

		mailMergeProperties.setProperties(properties);

	}

	public synchronized static VaultAppProperties getInstance() {

		return mailMergeProperties;
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
