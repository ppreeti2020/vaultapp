package com.pb.common;

import com.google.common.cache.Cache;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class ValidateSalt implements Filter  {
 
   // @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
 
        HttpServletRequest httpReq = (HttpServletRequest) request;
 
        // Get the salt sent with the request
        String salt = (String) httpReq.getParameter("csrfPreventionSalt");
        
        System.out.println("!!!!!!!!!!!!! ValidateSalt salt : " + salt);
 
        // Validate that the salt is in the cache
        Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>)httpReq.getSession().getAttribute("csrfPreventionSaltCache");
        System.out.println("!!!!!!!!!!!!! ValidateSalt before if");
        if (csrfPreventionSaltCache != null &&
                salt != null &&
                csrfPreventionSaltCache.getIfPresent(salt) != null){
        		System.out.println("!!!!!!!!!!!!! ValidateSalt if1 ");
 
            // If the salt is in the cache, we move on
            chain.doFilter(request, response);
            System.out.println("!!!!!!!!!!!!! ValidateSalt if2 ");
        } else {
        	
        	String filePath = CommonConstants.CSR_PROPERTY_FILE_NAME;
    		System.out.println("filepath"+filePath);
    		Properties properties1 = PropertyUtils.getProperties(filePath);
    		System.out.println("properties1");
    		String host=properties1.getProperty("LOAD_BALANCER_HOST");
    		String port=properties1.getProperty("LOAD_BALANCER_PORT");
    		
    		System.out.println("host : " +host);
    		System.out.println("port : " +port);
        	
        	System.out.println("!!!!!!!!!!!!! ValidateSalt else1 ");
        	HttpServletRequest req = (HttpServletRequest)request;
        	HttpServletResponse res = (HttpServletResponse)response;
        	System.out.println("!!!!!!!!!!!!! ValidateSalt error page url :  " + req.getScheme()+"://"+host+":"+port+req.getContextPath()+"/ErrorHandlerAction.action");
        	res.sendRedirect(req.getScheme()+"://"+host+":"+port+req.getContextPath()+"/ErrorHandlerAction.action");
        	System.out.println("!!!!!!!!!!!!! ValidateSalt else2 ");
            // Otherwise we throw an exception aborting the request flow
            throw new ServletException("Potential CSRF detected!! Inform a scary sysadmin ASAP.");
        }
    }
 
    // @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
 
   // @Override
    public void destroy() {
    }
}