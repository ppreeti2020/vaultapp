package com.pb.common;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class PerformanceLogger {

	private static Logger logger = Logger.getLogger(PerformanceLogger.class);
	String methodName;
	long startTime;
	long endTime;

	public PerformanceLogger() {
		super();
		logger.setLevel(Level.INFO);
	}

	public PerformanceLogger(Level level) {
		super();

		logger.setLevel(level);
	}

	public static boolean debug() {
		return logger.getLevel() == Level.DEBUG ? true : false;
	}

	public void writeTime(String operation) {
		logger.info(operation);
	}

	public void startMethod(String methodName) {
		this.methodName = methodName;
		startTime = System.currentTimeMillis();
		writeTime(methodName + ":START: ," + startTime + ", milliseconds");
	}

	public void endMethod(String methodName) {
		this.methodName = methodName;
		endTime = System.currentTimeMillis();
		writeTime(methodName + ":END: ," + endTime + ", milliseconds");
		writeTime(methodName + ":TIME TAKEN: ,"
				+ ((endTime - startTime) / 1000) + ", seconds");
	}

}