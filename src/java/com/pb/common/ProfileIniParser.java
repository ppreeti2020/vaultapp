package com.pb.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.g1.e2.vault.VaultClient;
import com.pb.admin.Common;
import com.pb.dto.UserDTO;

public class ProfileIniParser {

	private int profileIndexCount = 10;
	Map<String, List<UserDTO>> map = null;
	String fileName;
	Set<String> databases=new HashSet<String>();

	public ProfileIniParser(String fileName, String realPath) {
		// TODO Auto-generated constructor stub
		this.fileName = fileName;
		String filePath =	realPath+"\\"+CommonConstants.VAULT_CONFIG_FILE_NAME;
		Properties properties = PropertyUtils.getProperties(filePath);
		/*
		String count=properties.getProperty(CommonConstants.PROFILE_INDEX_COUNT);
		System.out.println("Count is====="+count+filePath);
	
		this.profileIndexCount = Integer.parseInt(count);*/
	}
	
	
	

	public Map<String, List<UserDTO>> getFileContent() {
		File file = new File(fileName);

		// StringBuffer contents = new StringBuffer();

		map = new HashMap<String, List<UserDTO>>();

		BufferedReader reader = null;

		try {

			reader = new BufferedReader(new FileReader(file));

			String text = null;

			// repeat until all lines is read
			int i = 0;
			List<UserDTO> indexList = new ArrayList<UserDTO>();
			int indexCount = 1;
			String profile = "";
			// int indexCount=0;
			while ((text = reader.readLine()) != null) {

				if (text.startsWith("[")) {
					if (i != 0) {
					//	System.out								.println(text.substring(1, text.indexOf("]")));

						profile = text.substring(1, text.indexOf("]"));

					}
					i++;
				}
if(text.startsWith("Database="))
{
	String dbText[]=text.split("=");
	databases.add(dbText[1]);
	
}
				if (text.startsWith("Index" + indexCount)) {
					if (indexCount == 1)
						indexList = new ArrayList<UserDTO>();

					UserDTO udto = new UserDTO();
					String indexVals = text.split("=")[1];
					String indexVal2[] = indexVals.split(",");
					String indexName = indexVal2[0];
					String indexDesc = indexVal2[3];
					udto.setIndexName(indexName);
					udto.setIndexDesc(indexDesc);
					indexList.add(udto);

					indexCount++;

				}
				if (indexCount == profileIndexCount + 1) {
					map.put(profile, indexList);
					indexCount = 1;
				}
			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (reader != null) {

					reader.close();

				}

			} catch (IOException e) {

				e.printStackTrace();

			}

		}
		return map;
	}

	public List<String> getProfiles() throws Exception {
		// System.out.println("here");
		map = getFileContent();

		Set<String> keys = map.keySet();
		List<String> keyList = new ArrayList<String>();
		keyList.addAll(keys);

		/*
		 * for (String string : keys) { List<UserDTO> list=map.get(string); for
		 * (UserDTO userDTO : list) {
		 * System.out.print(string+"-"+userDTO.getIndexName
		 * ()+"-"+userDTO.getIndexDesc()+"\n"); }
		 * 
		 * 
		 * }
		 */

		return keyList;

		// String contents=null;

	}

	public Set<String> getDatabases() {
		File file = new File(fileName);

		// StringBuffer contents = new StringBuffer();
Set<String> dataBaseset=new HashSet<String>();
		map = new HashMap<String, List<UserDTO>>();

		BufferedReader reader = null;

		try {

			reader = new BufferedReader(new FileReader(file));

			String text = null;

			// int indexCount=0;
			while ((text = reader.readLine()) != null) {
				if (text.startsWith("Database=")) {

					
				String str[]=	text.split("=");
				dataBaseset.add(str[1]);
				
					
				}

			}
		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (reader != null) {

					reader.close();

				}

			} catch (IOException e) {

				e.printStackTrace();

			}

		}
		
		return dataBaseset;
	}

	public List<UserDTO> getSearchIndexes(String profile) {

		return map.get(profile);

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		/*String fileName = "C:\\CIMB_profiles.ini";

		// String
		// fileName="C:\\Program Files\\Group1\\e2\\Vault\\Server\\profiles.ini";
		ProfileIniParser pi = new ProfileIniParser(fileName,"C:\\Jboss\\jboss-5.0.1.GA\\bin");
		pi.getFileContent();
		Set<String> dbs=pi.getDatabases();
		for (String string : dbs) {
			System.out.println(string);
		}
		
	/*Set<String>	 dbs=pi.getDatabases();
	for (String string : dbs) {
		System.out.println(string);
	}*/

	}

}
