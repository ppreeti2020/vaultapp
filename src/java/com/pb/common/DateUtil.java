/*
 * created at oct 21, 2011
 */
package com.pb.common;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static String getCurrentDate()
	{
		String currentDate = "";
		try
		{
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(
				CommonConstants.DATE_FORMAT);
		currentDate = formatter.format(date);
		}
		catch(Exception e)
		{
			
		}
		return currentDate;
	}
	
	public static boolean checkDiffDate(String date1, String date2) {
		boolean b = false;
		try {
			SimpleDateFormat format = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			Date startDate = format.parse(date1);
			Date endDate = format.parse(date2);			
			Date testDate=new Date();
			
			System.out.println("Start Date is"+startDate);
			System.out.println("End Date is"+endDate);
			System.out.println("Test Date is"+testDate);
			
			
			 return testDate.getTime() >= startDate.getTime() && 
             testDate.getTime() <= endDate.getTime(); 

			
			/*Calendar cal = Calendar.getInstance();
			Calendar currentcal = Calendar.getInstance();

			cal.setTime(fromDate);
			// cal.set(fromDate.getYear(), fromDate.getMonth(),
			// fromDate.getDate(),fromDate.getHours(),fromDate.getMinutes(),fromDate.getSeconds());
			currentcal.set(currentcal.get(Calendar.YEAR),
					currentcal.get(Calendar.MONTH),
					currentcal.get(Calendar.DAY_OF_MONTH),
					currentcal.get(Calendar.HOUR),
					currentcal.get(Calendar.MINUTE),
					currentcal.get(Calendar.SECOND));
			currentcal.setTime(new Date());
			Calendar cal1 = Calendar.getInstance();
			
			cal1.setTime(toDate);
			
		//	System.out.println("From Date"+cal);
			//System.out.println("To Date"+cal1);
			
			System.out.println("Cal1"+cal1);
			System.out.println("Currentcal"+currentcal);
			
			
			System.out.println(cal1.after(currentcal));
			//System.out.println(cal1.after(currentcal));
			// cal.set(toDate.getYear(), toDate.getMonth(),
			// toDate.getDate(),toDate.getHours(),toDate.getMinutes(),toDate.getSeconds());

			if ((cal.before(currentcal) || cal.equals(currentcal))
					&& cal1.after(currentcal)) {
				System.out.print("Current date("
						+ new SimpleDateFormat("dd/MM/yyyy").format(cal
								.getTime()) + ") is between the given dates "
						+ date1 + "and " + date2);
				b = true;
			}*/
		} catch (Exception e) {

		}
		return b;
	}

	public static Long diffDays(String date1, String date2) {
		// Long b = 0L;
		try {
			System.out.println("fromDate" + date1);
			if ((!date1.equals("")) && (!date2.equals(""))) {
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
				Date fromDate = format.parse(date1);
				Date toDate = format.parse(date2);
				System.out.println(fromDate);
				Calendar fromCal = Calendar.getInstance();
				// Calendar fromCal = Calendar.getInstance();

				fromCal.setTime(fromDate);

				// cal.set(fromDate.getYear(), fromDate.getMonth(),
				// fromDate.getDate(),fromDate.getHours(),fromDate.getMinutes(),fromDate.getSeconds());
				// fromCal.set(fromCal.get(Calendar.YEAR),
				// fromCal.get(Calendar.MONTH),
				// fromCal.get(Calendar.DAY_OF_MONTH),fromCal.get(Calendar.HOUR),fromCal.get(Calendar.MINUTE),fromCal.get(Calendar.SECOND));
				Calendar toCal = Calendar.getInstance();
				toCal.setTime(toDate);
				// cal.set(toDate.getYear(), toDate.getMonth(),
				// toDate.getDate(),toDate.getHours(),toDate.getMinutes(),toDate.getSeconds());
				long milliseconds1 = fromCal.getTimeInMillis();
				long milliseconds2 = toCal.getTimeInMillis();
				long diff = milliseconds2 - milliseconds1;
				long diffDays = diff / (24 * 60 * 60 * 1000);
				System.out.println("days Diff" + diffDays);
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				if (fromCal.equals(toCal)) {

				}
				/*
				 * if(cal.equals(fromCal)) { System.out.print("Current date(" +
				 * new SimpleDateFormat("dd/MM/yyyy").format(cal.getTime()) +
				 * ") is between the given dates " +date1+"and "+date2 ); b =
				 * true; }
				 */

				return diffDays;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0L;
	}
	
	public static boolean getDay(int day1, int day2) {
		boolean b = false;
		String dayNames[] = new DateFormatSymbols().getWeekdays();
		Calendar date2 = Calendar.getInstance();
		for (int i = day1; i < day2; i++) {
			String day = returnday(i);
			if (day.equalsIgnoreCase(dayNames[date2.get(Calendar.DAY_OF_WEEK)])) {
				System.out.println("Today is a "
						+ dayNames[date2.get(Calendar.DAY_OF_WEEK)]);
				b = true;
			}
		}
		return true;
	}

	public static String returnday(int first) {
		Calendar now = Calendar.getInstance();

		String[] strDays = new String[] { "Monday", "Tuesday", "Wednesday",
				"Thursday", "Friday", "Saturday", "Sunday" };
		// Day_OF_WEEK starts from 1 while array index starts from 0
		System.out.println("Current day is : " + strDays[first - 1]);
		return strDays[first - 1];
	}

			public static String incrementDate(String fromDate, int inc)
			throws ParseException {
		
				int daysToIncrement = 1;
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
				cal.setTime(format.parse(fromDate));
				cal.add(cal.DATE, inc);
				System.out.println("Date after increment: " + cal.getTime());
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				String date = format1.format(cal.getTime());
				System.out.println("new date format:" + date);
				return date;
		}
		
		public void decrementDate(Calendar cal) {
		
				int monthsToDecrement = -1;
				cal.add(Calendar.MONTH, monthsToDecrement);
				System.out.println("Date after decrement: " + cal.getTime());
		
		}
}
