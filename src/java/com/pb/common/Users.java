package com.pb.common;

import java.io.Serializable;

public class Users implements Serializable {
	public static final Long serialVersionUID = 1L;

	private Long userId;
	private String sessionId;
	private String userName;

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Users(Long userId, String sessionId) {
		this.userId = userId;
		this.sessionId = sessionId;
	}

	public Users(Long userId, String sessionId, String userName) {
		this.userId = userId;
		this.sessionId = sessionId;
		this.userName = userName;
	}
}
