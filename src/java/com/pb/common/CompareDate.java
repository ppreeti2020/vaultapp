package com.pb.common;

import java.util.*;
import java.text.*;

import com.pb.common.CommonConstants;

;
public class CompareDate {
	public static boolean checkDiffDate(String date1, String date2) {
		boolean b = false;
		try {
			SimpleDateFormat format = new SimpleDateFormat(
					CommonConstants.DATE_FORMAT);
			Date fromDate = format.parse(date1);
			Date toDate = format.parse(date2);
			Calendar cal = Calendar.getInstance();
			Calendar currentcal = Calendar.getInstance();

			cal.setTime(fromDate);
			// cal.set(fromDate.getYear(), fromDate.getMonth(),
			// fromDate.getDate(),fromDate.getHours(),fromDate.getMinutes(),fromDate.getSeconds());
			currentcal.set(currentcal.get(Calendar.YEAR),
					currentcal.get(Calendar.MONTH),
					currentcal.get(Calendar.DAY_OF_MONTH),
					currentcal.get(Calendar.HOUR_OF_DAY),
					currentcal.get(Calendar.MINUTE),
					currentcal.get(Calendar.SECOND));
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(toDate);
			System.out.println("fromDate::"+fromDate+">>toDate::"+toDate+">>currentDate::"+currentcal);
			// cal.set(toDate.getYear(), toDate.getMonth(),
			// toDate.getDate(),toDate.getHours(),toDate.getMinutes(),toDate.getSeconds());

			if ((cal.before(currentcal) || cal.equals(currentcal))
					&& cal1.after(currentcal)) {
				System.out.print("Current date("
						+ new SimpleDateFormat("dd/MM/yyyy").format(cal
								.getTime()) + ") is between the given dates "
						+ date1 + "and " + date2);
				b = true;
			}
		} catch (Exception e) {

		}
		return b;
	}

	public static Long diffDays(String date1, String date2) {
		// Long b = 0L;
		try {
			System.out.println("fromDate" + date1);
			if ((!date1.equals("")) && (!date2.equals(""))) {
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
				Date fromDate = format.parse(date1);
				Date toDate = format.parse(date2);
				System.out.println(fromDate);
				Calendar fromCal = Calendar.getInstance();
				// Calendar fromCal = Calendar.getInstance();

				fromCal.setTime(fromDate);

				// cal.set(fromDate.getYear(), fromDate.getMonth(),
				// fromDate.getDate(),fromDate.getHours(),fromDate.getMinutes(),fromDate.getSeconds());
				// fromCal.set(fromCal.get(Calendar.YEAR),
				// fromCal.get(Calendar.MONTH),
				// fromCal.get(Calendar.DAY_OF_MONTH),fromCal.get(Calendar.HOUR),fromCal.get(Calendar.MINUTE),fromCal.get(Calendar.SECOND));
				Calendar toCal = Calendar.getInstance();
				toCal.setTime(toDate);
				// cal.set(toDate.getYear(), toDate.getMonth(),
				// toDate.getDate(),toDate.getHours(),toDate.getMinutes(),toDate.getSeconds());
				long milliseconds1 = fromCal.getTimeInMillis();
				long milliseconds2 = toCal.getTimeInMillis();
				long diff = milliseconds2 - milliseconds1;
				long diffDays = diff / (24 * 60 * 60 * 1000);
				System.out.println("days Diff" + diffDays);
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				if (fromCal.equals(toCal)) {

				}
				/*
				 * if(cal.equals(fromCal)) { System.out.print("Current date(" +
				 * new SimpleDateFormat("dd/MM/yyyy").format(cal.getTime()) +
				 * ") is between the given dates " +date1+"and "+date2 ); b =
				 * true; }
				 */

				return diffDays;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0L;
	}

	public static void main(String args[]) throws ParseException {
		Long i = diffDays("01-02-2012", "01-01-2012");

	}
}
