package com.pb.common;

public class CommonConstants {
	public static final String INITIAL_CONTEXT_FACTORY = "INITIAL_CONTEXT_FACTORY";
	public static final String PROVIDER_URL = "PROVIDER_URL";
	public static final String SERVER1_PROCESSED_JRN_PATH="SERVER1_PROCESSED_JRN_PATH";
	public static final String AUTHENTICATION = "AUTHENTICATION";
	public static final String SIMPLE_AUTHENTICATION = "simple";
	public static final String DOMAIN = "DOMAIN";
	public static final String ALL_DB = "ALL_DB";
	public static final String BASE = "BASE";
	public static final String CSR_ADMIN_GROUP_NAME = "CSR_ADMIN_GROUP_NAME";
	public static final String HOST_NAME = "HOST_NAME";
	public static final String FILTER_RESULT = "FILTER_RESULT";
	public static final String VAULT_IDX = "VAULT_IDX";
	public static final String HOST_PORT = "HOST_PORT";
	public static final String CSR_PROPERTY_FILE_NAME = "csrapp.properties";
	public static final String PAGE_DATA = "pagedata";
	public static final String DOC_DATA = "docdata";
	public static final String CSR_GROUP_STARTS_WITH_STRING = "CSR_GROUP_STARTS_WITH_STRING";
	public static final String EVAULT_HOST = "EVAULT_HOST";
	public static final String EVAULT_PORT = "EVAULT_PORT";
	public static final String SEARCH_INDEX_NAME = "SEARCH_INDEX_NAME";
	public static final String AVAILABLE_PBIDX_VALUES = "AVAILABLE_PBIDX_VALUES";
	public static final String AVAILABLE_PBIDX_VALUES_REPORT = "AVAILABLE_PBIDX_VALUES_REPORT";
	public static final String CSR_ADMIN_USER_ID = "CSR_ADMIN_USER_ID";
	public static final String CSR_ADMIN_PWD = "CSR_ADMIN_PWD";
	public static final String PERMISSION_FILE_PATH = "PERMISSION_FILE_PATH";
	public static final String APPROVAL_PROPERTIES_FILE_PATH = "APPROVAL_PERMISSION_FILE_PATH";
	public static final String STATUS_PROPERTIES_FILE_PATH = "STATUS_PERMISSION_FILE_PATH";
	public static final String CSR_DATABASE = "CSR_DATABASE";
	public static final String IGNORE_SEARCHINDEX = "IGNORE_SEARCHINDEX";
	public static final String STATUS_APPROVAL = "STATUS";
	public static final String CSR_DBA_MAKER_GROUP = "CSR_DBA_MAKER_GROUP";
	public static final String CSR_DBA_CHECKER_GROUP = "CSR_DBA_CHECKER_GROUP";
	public static final String TOTAL_EVAULT_HOST = "TOTAL_EVAULT_HOST";
	public static final String DATABASE_PATH = "DATABASE_PATH";
	public static final String DATABASE_USER = "DATABASE_USER";
	public static final String DATABASE_PWD = "DATABASE_PWD";
	public static final String SECRET_KEY = "SECRET_KEY";
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT1 = "yyyy-MM-dd";
	public static final String I_CHARS = "!@#$%^*()+=[]\\\';,/{}|\":<>?-&";
	public static final String PBBC_FILE_PATH = "PBBC_FILE_PATH";
	
	public static final String ROLE_DESC_ADMIN = "Administrator";
	public static final String ROLE_DESC_PROJECTADMIN = "Profile Admin";
	public static final String ROLE_DESC_SYSADMIN = "System Admin";
	public static final String ROLE_DESC_CSR = "CSR";
	public static final String ROLE_DESC_REPOSITORY = "Repository";
	public static final String ROLE_DESC_REP = "Reports";
	public static final String ROLE_DESC_AUDIT = "AUDIT";

	
	public static final String ROLE_DESC_4 = "Admin";
	public static final String ADMINISTRATOR = "ADMINISTRATOR";
	public static final String GROUP_POLICY_CREATOR = "Group Policy Creator Owners";
	public static final String LOGIN_TAB_INTEGRITY_CHECK = "LOGIN_TAB_INTEGRITY_CHECK";
	public static final String LOGOUT_TAB_INTEGRITY_CHECK = "LOGOUT_TAB_INTEGRITY_CHECK";
	public static final String SEARCH_DET_INTEGRITY_CHECK = "SEARCH_DET_INTEGRITY_CHECK";
	public static final String SEARCH_RES_INTEGRITY_CHECK = "SEARCH_RES_INTEGRITY_CHECK";
	public static final String ACCOUNT_INTEGRITY_CHECK = "ACCOUNT_INTEGRITY_CHECK";
	public static final String DOCUMENT_INTE_DETAILS_CHECK = "DOCUMENT_INTE_DETAILS_CHECK";
	public static final String DOC_INTEGRITY_CHECK = "DOC_INTEGRITY_CHECK";
	public static final String DOCUMENT_RENDER_INTE_DETAILS = "DOCUMENT_RENDER_INTE_DETAILS";
	public static final String USER_INTEGRITY_CHECK = "USER_INTEGRITY_CHECK";
	public static final String USER_ROLE_INTEGRITY_CHECK = "USER_ROLE_INTEGRITY_CHECK";
	public static final String CSR_ROLES_INTEGRITY_CHECK = "CSR_ROLES_INTEGRITY_CHECK";

	public static final String USER_DB_INTEGRITY_CHECK = "USER_DB_INTEGRITY_CHECK";
	public static final String TABLES_DETAILS = "TABLES_DETAILS";
	public static final String TABLES_INTEGRITY_CHECK = "TABLES_INTEGRITY_CHECK";
	public static final String COMPRESSION_INTEGRITY_CHECK = "COMPRESSION_INTEGRITY_CHECK";
	public static final String USER_GROUPS_INTEGRITY_CHECK = "USER_GROUPS_INTEGRITY_CHECK";
	public static final String AllTables_SpaceUsed = "allTables_SpaceUsed";

	public static final String CSV_FILE_PATH = "CSV_FILE_PATH";// ";//properties
	public static final String CSV_FILE_NAME = "CSV_FILE_NAME";
	/* Ldap config Details */
	//public static final String LDAP_CONFIG_FILE_PATH = "LDAP_CONFIG_FILE_PATH";
	public static final String LDAP_CONFIG_FILE_NAME = "csrldapconfig.properties";
	public static final String LDAP_INITIAL_CTX_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	public static final String LDAP_INITIAL_CONTEXT_FACTORY = "LDAP_INITIAL_CONTEXT_FACTORY";
	public static final String LDAP_Host = "LDAP_Host";
	public static final String LDAP_Port = "LDAP_Port";
	public static final String LDAP_Naming_Context = "LDAP_Naming_Context";
	public static final String LDAP_Domain = "LDAP_User_DN";
	public static final String LDAP_AUTH_DOMAIN = "LDAP_AUTH_DOMAIN";
	public static final String LDAP_User_DN = "LDAP_User_DN";
	public static final String LDAP_User_Filter = "LDAP_User_Filter";
	public static final String LDAP_User_Name_Attribute = "LDAP_User_Name_Attribute";
	public static final String LDAP_Group_Member_Attribute = "LDAP_Group_Member_Attribute";
	public static final String LDAP_Group_Filter = "LDAP_Group_Filter";
	public static final String LDAP_Group_Name_Attribute = "LDAP_Group_Name_Attribute";
	public static final String USER_NAME = "USER_NAME";
	public static final String Password = "Password";
	public static final String EVAULT_CONFIG_FILE_PATH = "EVAULT_CONFIG_FILE_PATH";
	//public static final String VAULT_CONFIG_FILE_PATH = "VAULT_CONFIG_FILE_PATH";

	public static final String INDEXCHECKTOOLPATH="INDEXCHECKTOOLPATH";
	public static final String INDEXFILEPATH="INDEXFILEPATH";
	
	
	
	
	
	public static final String VAULT_CONFIG_FILE_NAME = "csrvaultconfig.properties";
	public static final String VAULT_AUDIT_CONFIG_FILE_NAME = "VaultAuditServerConfig.properties";

	public static final String ARCHIVAL_SERVER_HOST = "ARCHIVAL_SERVER_HOST";
	public static final String ARCHIVAL_SERVER_PORT = "ARCHIVAL_SERVER_PORT";
	public static final String ARCHIVAL_SERVER_IP = "ARCHIVAL_SERVER_IP";
	public static final String SOFTWARE_PATH = "SOFTWARE_PATH";
	public static final String RENDERING_SERVER_HOST = "RENDERING_SERVER_HOST";
	public static final String RENDERING_SERVER_PORT = "RENDERING_SERVER_PORT";
	public static final String RENDERING_SERVER_IP = "RENDERING_SERVER_IP";
	public static final String DATA_ROUTER_SERVER_HOST = "DATA_ROUTER_SERVER_HOST";
	public static final String DATA_ROUTER_ARCHIVAL_SERVER_PORT = "DATA_ROUTER_ARCHIVAL_SERVER_PORT";
	public static final String DATA_ROUTER_ARCHIVAL_SERVER_IP = "DATA_ROUTER_ARCHIVAL_SERVER_IP";
	public static String DB_CONFIG_FILE_PATH = "DB_CONFIG_FILE_PATH";
	public static final String DB_CONFIG_FILE_NAME = "csrdatabaseconfig.properties";

	public static final String ENABLE_SSL = "ENABLE_SSL";
	
	public static final String LDAP_TRUST_STORE_PATH = "LDAP_TRUST_STORE_PATH";
	public static final String LDAP_TRUST_STORE_PASSWORD = "LDAP_TRUST_STORE_PASSWORD";
	public static final String DB_SERVER_TYPE = "DB_SERVER_TYPE";
	public static final String DB_SERVER_HOST = "DB_SERVER_HOST";
	public static final String DB_SERVER_PORT = "DB_SERVER_PORT";
	public static final String DB_SERVER_IP = "DB_SERVER_IP";
	public static final String DATABASE_NAME = "DATABASE_NAME";
	public static final String DB_USER_NAME = "DB_USER_NAME";
	public static final String DB_PASSWORD = "DB_PASSWORD";
	public static final String MSSQL = "MS SQL Server";
	public static final String DatabaseName = "DatabaseName";
	public static final String PROFILE_INDEX_COUNT="PROFILE_INDEX_COUNT";
	public static final String SERVER1_HOST = "SERVER1_HOST";
	//public static final String SERVER1_IP = "SERVER1_IP";
	public static final String SERVER1_SOFTWARE_PATH = "SERVER1_SOFTWARE_PATH";
	public static final String SERVER1_VAULT_SERVER_PATH = "SERVER1_VAULT_SERVER_PATH";
	
	public static final String SERVER1_INSTANCE_NAME="SERVER1_INSTANCE_NAME";
	public static final String SERVER2_INSTANCE_NAME="SERVER2_INSTANCE_NAME";
	
	
	public static final String SERVER1_VAULT_SERVER_AUDITADM_PATH = "SERVER1_VAULT_SERVER_AUDITADM_PATH";
	public static final String SERVER1_VAULT_SERVER_AUDITCSV_PATH = "SERVER1_VAULT_SERVER_AUDITCSV_PATH";
	
	public static final String ARCHIVAL_SERVER1_PORT = "ARCHIVAL_SERVER1_PORT";
	public static final String RENDERING_SERVER1_PORT = "RENDERING_SERVER1_PORT";
	public static final String DATA_ROUTER_SERVER1_PORT = "DATA_ROUTER_SERVER1_PORT";
	
	public static final String SERVER2_HOST = "SERVER2_HOST";
	//public static final String SERVER2_IP = "SERVER2_IP";
	
	public static final String SERVER2_SOFTWARE_PATH = "SERVER2_SOFTWARE_PATH";
	public static final String SERVER2_VAULT_SERVER_PATH = "SERVER2_VAULT_SERVER_PATH";
	public static final String ARCHIVAL_SERVER2_PORT = "ARCHIVAL_SERVER2_PORT";
	public static final String RENDERING_SERVER2_PORT = "RENDERING_SERVER2_PORT";
	public static final String DATA_ROUTER_SERVER2_PORT = "DATA_ROUTER_SERVER2_PORT";
	
	public static final String ARCHIVAL_SERVER_HOST1 = "ARCHIVAL_SERVER_HOST1";
	public static final String ARCHIVAL_SERVER_IP1 = "ARCHIVAL_SERVER_IP1";
	public static final String ARCHIVAL_SERVER_HOST2 = "ARCHIVAL_SERVER_HOST2";
	public static final String ARCHIVAL_SERVER_PORT2 = "ARCHIVAL_SERVER_PORT2";
	public static final String ARCHIVAL_SERVER_IP2 = "ARCHIVAL_SERVER_IP2";
	public static final String SOFTWARE_PATH1 = "SOFTWARE_PATH1";
	public static final String SOFTWARE_PATH2 = "SOFTWARE_PATH2";
	public static final String ARCHIVAL_SERVER_PORT1 = "ARCHIVAL_SERVER_PORT1";
	public static final String RENDERING_SERVER_PORT1 = "RENDERING_SERVER_PORT1";
	public static final String DATA_ROUTER_SERVER_PORT1 = "DATA_ROUTER_ARCHIVAL_SERVER_PORT1";
	
	public static final String DATA_ROUTER_SERVER_PORT = "DATA_ROUTER_SERVER_PORT";
	public static final String SERVER_PATH = "SERVER_PATH";
	
	public static final String SERVER_INSTANCE_NAME = "SERVER_INSTANCE_NAME";
	public static final String RENDERING_SERVER_HOST1 = "RENDERING_SERVER_HOST1";
	public static final String RENDERING_SERVER_IP1 = "RENDERING_SERVER_IP1";
	public static final String RENDERING_SERVER_HOST2 = "RENDERING_SERVER_HOST2";
	public static final String RENDERING_SERVER_PORT2 = "RENDERING_SERVER_PORT2";
	public static final String RENDERING_SERVER_IP2 = "RENDERING_SERVER_IP2";
	public static final String DATA_ROUTER_SERVER_HOST2 = "DATA_ROUTER_SERVER_HOST2";
	public static final String DATA_ROUTER_ARCHIVAL_SERVER_PORT2 = "DATA_ROUTER_ARCHIVAL_SERVER_PORT2";
	public static final String DATA_ROUTER_ARCHIVAL_SERVER_IP2 = "DATA_ROUTER_ARCHIVAL_SERVER_IP2";
	
	public static final String DATA_ROUTER_SERVER_HOST1 = "DATA_ROUTER_SERVER_HOST1";
	public static final String DATA_ROUTER_ARCHIVAL_SERVER_PORT1 = "DATA_ROUTER_ARCHIVAL_SERVER_PORT1";
	public static final String DATA_ROUTER_ARCHIVAL_SERVER_IP1 = "DATA_ROUTER_ARCHIVAL_SERVER_IP1";
	public static final String ORACLE = "ORACLE";
	public static final String SCHEDULER_TIME = "SCHEDULER_TIME";
	public static final String AUDIT_LDAP_CONFIG_FILE_NAME = "AuditVaultLdapConfig.properties";
	
	public static final String DB_ENCRYPT = "DB_ENCRYPT";
	public static final String CROSS_REF = "CROSS_REF";
	public static final String CSR_ADMIN_AUDIT = "CSR_ADMIN_AUDIT";
	public static final String VAULT_SERVER2 = "VAULT_SERVER2";
	
	public static final String MAX_RECORDS = "MAX_RECORDS";
	public static final String EXPECTED_MAX_RESULT = "EXPECTED_MAX_RESULT";
	/*Praveen Kumar: 2 Nov 2015 Added Controlled Character in the Configuration properties.*/
	public static final String CHARACTER_NOT_TO_DISPLAY="CHARACTER_NOT_DISPLAY";
	public static final String ORDER_BY_SQL_QUERY="SQL_ORDER_BY_CLAUSE";
	
	public static final String ENABLE_VAULTAPP_SCHEDULER_STARTUP="ENABLE_VAULTAPP_SCHEDULER_STARTUP";
}
