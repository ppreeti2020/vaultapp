package com.pb.common;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class ServerSideValidationUtil {
/*	
	public static void main(String args[]){
		String s="Rasi_s";
		System.out.println("isAlphaNumericWithUnderscore :"+isAlphaNumericWithUnderscore(s));
	}*/

	public static Boolean isAlpha(String s){
		if(s.matches("^[A-Za-z]+$"))
			return true;
		return false;
	}
	public static Boolean isAlphaWithSpace(String s){
		if(s.matches("^[ A-Za-z]+$"))
			return true;
		return false;
	}
	public static Boolean isNumeric(String s){
		if(s.matches("^-?[0-9]+$"))
			return true;
		return false;
	}
	public static Boolean isBoolean(String s){
		if(s.equalsIgnoreCase("true") || s.equalsIgnoreCase("false"))
			return true;
		return false;
	}
	public static Boolean isAlphaNumeric(String s){
		if(s.matches("^[A-Za-z0-9]+$"))
			return true;
		return false;
	}
	public static Boolean isAlphaNumericWithSpaceAndUnderscore(String s){
		if(s.matches("^[A-Za-z0-9\\s_]+$"))
			return true;
		return false;
	}
	public static Boolean isAlphaNumericWithUnderscore(String s){
		if(s.matches("^[A-Za-z0-9_]+$"))
			return true;
		return false;
	}
	public static Boolean isAlphaNumericWithSpaceAndUnderscoreDOT(String s){
		if(s.matches("^[A-Za-z0-9.\\s_]+$"))
			return true;
		return false;
	}
	public static Boolean isAlphaNumericWithURLPattern(String s){
		if(s.matches("^[ A-Za-z0-9_:.,=?&\"\'/\\\\]+$"))
			return true;
		return false;
	}
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean isAlphaNumericWithHyphenSpaceAndForwardSlash(String s){
		if(s.matches("^[A-Za-z0-9-/\\s]+$"))
			return true;
		return false;
	}
	
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean isAlphaNumericWithHyphenAndUnderscore(String s){
		if(s.matches("^[A-Za-z0-9-_]+$"))
			return true;
		return false;
	}
	
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean isAlphaNumericWithCommaAndUnderscore(String s){
		if(s.matches("^[A-Za-z0-9_,]+$"))
			return true;
		return false;
	}
	
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean isAlphaNumericWithSpaceAndComma(String s){
		if(s.matches("^[A-Za-z0-9,\\s]+$"))
			return true;
		return false;
	}
	
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean isNumericWithComma(String s){
		if(s.matches("^-?[0-9,]+$"))
			return true;
		return false;
	}
	
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean isNumericWithDOT(String s){
		if(s.matches("^-?[0-9\\.]+$"))
			return true;
		return false;
	}
	
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean isAlphaNumericWithSpace(String s){
		if(s.matches("^[A-Za-z0-9\\s]+$"))
			return true;
		return false;
	}
	
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean isValidPasswordPattern(String s){
		if(s.matches("^[A-Za-z0-9@#$%^&+]+$"))
			return true;
		return false;
	}
	
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean isValidFolderPathPattern(String s){
		if(s.matches("^[A-Za-z0-9_:\\\\]+$"))
			return true;
		return false;
	}
	
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean isAlphaNumericWithDOT(String s){
		if(s.matches("^[A-Za-z0-9.]+$"))
			return true;
		return false;
	}
	
	/* Nithu Alexander: Added for "XSS" and AVA-1,AVA-2 fixes*/
	public static Boolean checkValidLDAPCharacters(String s){
		if(s.contains(",") || s.contains("(") || s.contains(")") || s.contains("=")){
			return true;
		}else{
			return false;
		}
	}
	
	
	/* Nithu Alexander: Added for "XSS" and AVA-1 fixes for "DATE" validation*/
	public static Boolean isValidDatePattern(String s){
		s=s.replaceAll("\\s", "");
		String datePattern = "^((19|20)\\d\\d)[-/\\S]?(0?[1-9]|1[012])[-/\\S]?(0?[1-9]|[12][0-9]|3[01])$";
		Pattern pattern = Pattern.compile(datePattern);
		Matcher matcher  = pattern.matcher(s);
		if(matcher.matches()){
			
			matcher.reset();
			
			if(matcher.find()){
				int year = Integer.parseInt(matcher.group(3));
				String month = matcher.group(2);
				String day = matcher.group(1);
				
				if(day.equals("31") && (month.equals("4")||month.equals("6")||month.equals("9") || month.equals("11")||
						month.equals("04")||month.equals("06")||month.equals("09"))){
							return false; //only 1,3,5,7,8,10,12 have 31 days
					
				}else if(month.equals("2") || month.equals("02")){
					//leap year
					if(year % 4==0){
						if(day.equals("30") || day.equals("31")){
							return false;
						}else{
							return true;
						}
					}else{
						if(day.equals("29") || day.equals("30") || day.equals("31")){
							return false;
							
						}else{
							return true;
						}
					}
				}else{
					return true;
				}
				
			}else{
				return false;
			}
		}else{
			return false;
		}
		
	}
	public static Boolean checkInvalidChars(String s){
		if(s.contains("<") 
				|| s.contains(">") 
				|| s.contains("\"") 
				|| s.contains("'") 
				|| s.contains("#")
				|| s.contains("&")
				|| s.contains(";")
				|| s.contains("(")
				|| s.contains(")")
				|| s.contains("/")
				|| s.contains("\\s")
				|| s.contains("\\r")
				|| s.contains("\\n"))
			return true;
		return false;
	}
	public static Boolean isValidJSONARRAY(String s){
		if(s.matches("^[A-Za-z0-9\\s_{}\":,]+$"))
			return true;
		return false;
	}
	
}
