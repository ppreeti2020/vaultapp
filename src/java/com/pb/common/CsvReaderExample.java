package com.pb.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.io.IOException;

import com.csvreader.CsvReader;
import com.pb.common.CommonConstants;
import com.pb.common.PropertyUtils;
import com.pb.dao.FileInfoDAO;
import com.pb.dto.FileDTO;

public class CsvReaderExample {
	DataEncryptDecrypt data = new DataEncryptDecrypt();
	FileInfoDAO dao = new FileInfoDAO();

	public boolean createADMFile() throws IOException {
		boolean b = false;
		InputStream in = null;
		OutputStream out = null;
		try {
			File file = new File("C:\\audit.adm");
			FileOutputStream output = new FileOutputStream(file);
			String str = "1";
			output.write(str.getBytes());
			if (file.exists()) {

			//	File f2 = new File("C:\\PBBI_CCM\\e2Vault\\server\\process\\audit.adm");
				File f2 = new File("(PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME)).getProperty(CommonConstants.EVAULT_CONFIG_FILE_PATH)");
				in = new FileInputStream(file);

				// For Append the file.
				// OutputStream out = new FileOutputStream(f2,true);

				// For Overwrite the file.
				out = new FileOutputStream(f2);

				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}

				b = true;
				System.out.println("File copied.");
			}
		} catch (IOException e) {

		} finally {
			in.close();
			out.close();
		}
		return b;
	}

	public void checkComp() throws Exception {
		Properties properties = PropertyUtils
				.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);

		try {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
			String date = format1.format(cal.getTime());
			System.out.println("new date format:" + date);
			CsvReader products = new CsvReader(
					properties.getProperty(CommonConstants.CSV_FILE_PATH)
							+ "\\" + "list." + date + ".csv");

			products.readHeaders();

			while (products.readRecord()) {
				String fileName = products.get("File Name");
				String fileDate = products.get("File Date");
				String profile = products.get("Profile");
				String resource = products.get("Resource Set");
				String uncompress = products.get("Uncompressed");
				String compress = products.get("Compressed");
				String pages = products.get("Pages");
				List list = dao.getAFPFileinfos(data.encryptData(fileName));
				Iterator iter = list.iterator();
				if (!iter.hasNext()) {

					createCompFromCSV(fileName, fileDate, profile, resource,
							uncompress, compress, pages);

				}

				// perform program logic here
				System.out.println(fileName + ":" + fileDate);
			}

			products.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void createCompFromCSV(String fileName, String fileDate,
			String profile, String resource, String uncompress,
			String compress, String pages) throws Exception {
		Properties properties = PropertyUtils
				.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME);

		FileDTO dto = new FileDTO();
		float bytesPerpage = 0f;// Integer.parseInt(uncompress.toString())/Integer.parseInt(compress.toString());
		float ratio = 0f;// Integer.parseInt(compress.toString())/Integer.parseInt(pages.toString());
		fileName = data.encryptData(fileName);
		fileDate = data.encryptData(fileDate);
		profile = data.encryptData(profile);
		resource = data.encryptData(resource);
		uncompress = data.encryptData(uncompress);
		compress = data.encryptData(compress);
		pages = data.encryptData(pages);
		dto.setFileName(fileName);
		dto.setFileDate(fileDate);
		dto.setProfile(profile);
		dto.setPages(pages);
		dto.setResourceSet(resource);
		dto.setUnCompress(uncompress);
		dto.setCompress(compress);

		dto.setBytesPerpage(bytesPerpage);

		dto.setRatio(ratio);
		String hashId = AeSimpleSHA1.SHA1(fileName + "," + fileDate + ","
				+ profile + "," + resource + "," + uncompress + "," + compress
				+ "," + pages);
		dto.setHashId(hashId);
		dao.saveAFPFileinfo(dto);
	}

}
