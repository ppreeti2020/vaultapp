<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dto.DataDTO" %>
<html>
<head>
<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	
	<script src="js/jquery_treeview.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>
<script type="text/javascript" src="js/demo.js"></script>
</head>
<%
Map<Long,DataDTO>  treeMapList = request.getAttribute("treeMapList")!=null?(HashMap<Long,DataDTO>)request.getAttribute("treeMapList"):new HashMap<Long,DataDTO>();
System.out.println("treeMapList:"+treeMapList);
Map<Integer,DataDTO>  treeProfile = request.getAttribute("treeProfile")!=null?(HashMap<Integer,DataDTO>)request.getAttribute("treeProfile"):new HashMap<Integer,DataDTO>();
System.out.println("treeProfile:"+treeProfile);
%>
<body>
<ul id="browser" class="filetree treeview">
	<% int r=1;for(Long rootId: treeMapList.keySet()){
		System.out.println("rootId in auditLeftPanel::"+rootId);
		DataDTO ddao = (DataDTO)treeMapList.get(rootId);
		%> <li><span class="folder"><a href="AuditServerAction.action?server=server<%=r%>"><%= ddao.getHostName()%></a></span>
		<ul>
			<li><span class="folder"><a href="AuditAction.action?hostId=<%=rootId%>&server=server<%=r%>">Archival</a></span>
				<ul>
					<li><span class="file">
					<a href="AuditAction.action?tab=indexLog&name=Indexing&hostId=<%=rootId%>&server=server<%=r%>">Indexing</a>
					</span></li>
					<li><span class="file">
					<a href="AuditAction.action?tab=reIndexLog&name=Reindexing&hostId=<%=rootId%>&server=server<%=r%>">Reindexing</a>
					</span></li>
					<li><span class="file">
					<a href="AuditAction.action?tab=purgingLog&name=Purging&hostId=<%=rootId%>&server=server<%=r%>">Purging</a>
					</span></li>
					<li><span class="file">
					<a href="AuditAction.action?tab=removeLog&name=Remove&hostId=<%=rootId%>&server=server<%=r%>">Remove</a>
					</span></li>
					
				</ul>
			</li>
		</ul>
		<ul>
			<li><span class="folder">Renderer</span>
				<ul>
					<li><span class="file">
					<a href="ResourceReportAction.action?hostId=<%=rootId%>&server=server"+<%=r%>>Resources</a>
					</span></li>
					
				</ul>
			</li>
		</ul>
		<ul>
			<li><span class="folder"><a href="AuditIndexAction.action?hostId=<%= rootId%>&server=server<%=r%>">Profiles</a></span>
				<ul><% for(Integer rootId1: treeProfile.keySet()){
		
		
			DataDTO ddao1 = (DataDTO)treeProfile.get(rootId1);
			System.out.println(rootId1+"host"+ddao1.getProfile());
			if(ddao1.getHostId() == rootId){
			
		%> 
					<li><span class="file">
					<a href="AuditIndexAction.action?dbId=<%=ddao1.getDbId()%>&type=index&hostId=<%= rootId %>&profile=<%= ddao1.getProfile()%>&server=server<%=r%>"><%= ddao1.getProfile()%> </a>
					</span></li>
					<%}}%>
				</ul>
			</li>
		</ul></li>
		<%r++;}%>
	
</ul>
</body>
</html>