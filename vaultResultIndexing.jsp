<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.pb.dto.UserDTO"%>

<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="styles/common/messages.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="styles/common/main.css" rel="stylesheet" type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css" type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="styles/jquery.treeview.css" />
<link rel="stylesheet" href="styles/screen.css" />

<script src="js/jquery_treeview.js" type="text/javascript"></script>
<script src="js/jquery.cookie.js" type="text/javascript"></script>
<script src="js/jquery.treeview.js" type="text/javascript"></script>
<script type="text/javascript" src="js/demo.js"></script>
<title>e2 Vault Administration</title>
<s:include value="adminHeader.jsp" />
<script>
  
 	</script>
<title>tables</title>
<style type="text/css">
#playlist tr.header {
	background-color: #0072a8;
}

#playlist tr.even td {
	background-color: #eee;
}

#playlist tr.odd  td {
	background-color: #fff;
}
</style>
</head>
<%
	String host = (String) request.getAttribute("host");
	List<String> dbList = (ArrayList<String>) request
			.getAttribute("dbList");
	List<String> userProfileList = (ArrayList<String>) request
			.getAttribute("userProfileList");
	String dbName = request.getAttribute("dbName") != null ? request
			.getAttribute("dbName").toString() : "";
	System.out.println("database Name::" + dbName);
	String userProfileName = request.getAttribute("userProfileName")!= null?request.getAttribute("userProfileName").toString():"";
	  String changeValue = request.getAttribute("changeValue")!= null?request.getAttribute("changeValue").toString():"";
	  Long userProfileId = request.getAttribute("userProfileId")!= null?(Long)request.getAttribute("userProfileId"):0L;
	  System.out.println("changeValue::"+changeValue);
	  System.out.println("User Profile Name::"+userProfileName);
	  System.out.println("database Name::"+dbName);
%>
<script>
function cancelForm(button)
{
	//action="ActiveDirectoryAction"
	//alert(document.pressed);
	
		
	document.getElementById("indexsearchform").action = "UsersListAction.action";
	document.getElementById("indexsearchform").submit;
	return true;
			
	}


function ValidateForm(){
	if(document.getElementById("changeValue").value=="ProfileIndexes"){
	var r=confirm("Changes will update all the profile Users \n Do you want to continue!!");
	if(r==true){
		return true;
	}
	else
		return false;
	}
	else
		return true;
	
}
</script>
<body>
	<jsp:include page="CommonTabs.jsp" />
	<%--Nithu Alexander: 17 Aug 2015, Changing width from 1230px to 100%, height from 610px to 100% --%>
	<div id="countrydivcontainer" style="border: 1px solid gray; width: 100%; height: 100%; margin-bottom: 1em; padding: 10px; overflow:auto">


		<jsp:include page="userprofadminCommonTabs.jsp" />
		<div id="countrydivsubcontainer" style="border: 1px solid gray; width: 1180px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">

			<table width="100%" border="1" valign="top" height="450">
				<tr>
					<td width="20%" valign="top">
						<%
							if (host != null)

							{
						%>
						<ul id="browser" class="filetree treeview">
							<li><%=host%>
								<ul>
								<%if(changeValue != ""){
											if(changeValue != "" && changeValue.equalsIgnoreCase("ProfileIndexes") )
										  {%>
											<li>
											<a href="SerResIndexAction.action?changeValue=ProfileIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">ProfileIndexes</font></a>
											<ul>
											<%for(Iterator iter = userProfileList.iterator();iter.hasNext();)
										  	{   UserDTO uDTO1 = (UserDTO)iter.next();
											    System.out.println("userProfileName...."+uDTO1.getUserProfileName());%>
											 	<%if(userProfileName != "" && userProfileName.equals(uDTO1.getUserProfileName())) 
											  { %>
												<li>
													<a href="SerResIndexAction.action?userProfileName=<%=uDTO1.getUserProfileName()%>&changeValue=ProfileIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300"><%=uDTO1.getUserProfileName()%></font></a>
													<ul>
													<% for(Iterator iter1 = dbList.iterator();iter1.hasNext();)
														{ UserDTO uDTO = (UserDTO)iter1.next();
														  System.out.println("dbname...."+uDTO.getDbName());%>
						                                  <li><% if(dbName != "" && dbName.equals(uDTO.getDbName()))
						                                      	 {%><a href="SerResIndexAction.action?dbName=<%=uDTO.getDbName()%>&changeValue=ProfileIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>&userProfileName=<%=uDTO1.getUserProfileName()%>" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300"><%=uDTO.getDbDesc()%></font></a>
																<%}else 
																  {%> <a href="SerResIndexAction.action?dbName=<%=uDTO.getDbName()%>&changeValue=ProfileIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>&userProfileName=<%=uDTO1.getUserProfileName()%>"><%=uDTO.getDbDesc()%></a>
																<%}%>
														  </li>
				    							  	  <%}%>
				   									</ul>
											<%}else 
											  {%>
												<li>
													<a href="SerResIndexAction.action?userProfileName=<%=uDTO1.getUserProfileName()%>&changeValue=ProfileIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>"><%=uDTO1.getUserProfileName()%></a>
			  								<%}%>
			  									</li>
			  									</ul>
			  									</li>
										<% } 
										} else{%>
											<li>
											<a href="SerResIndexAction.action?changeValue=ProfileIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">ProfileIndexes</font></a>
											</li>
										<%} if(changeValue != "" && changeValue.equalsIgnoreCase("UserIndexes"))
											{ %><li>
											<a href="SerResIndexAction.action?changeValue=UserIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">UserIndexes</font></a>
											<ul>
											<%for(Iterator iter = userProfileList.iterator();iter.hasNext();)
										  	{   UserDTO uDTO1 = (UserDTO)iter.next();
											    System.out.println("userProfileName...."+uDTO1.getUserProfileName());%>
											 	<%if(userProfileName != "" && userProfileName.equals(uDTO1.getUserProfileName())) 
											  { %>
												<li>
													<a href="SerResIndexAction.action?userProfileName=<%=uDTO1.getUserProfileName()%>&changeValue=UserIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300"><%=uDTO1.getUserProfileName()%></font></a>
													<ul>
													<% for(Iterator iter1 = dbList.iterator();iter1.hasNext();)
														{ UserDTO uDTO = (UserDTO)iter1.next();
														  System.out.println("dbname...."+uDTO.getDbName());%>
						                                  <li><% if(dbName != "" && dbName.equals(uDTO.getDbName()))
						                                      	 {%><a href="SerResIndexAction.action?dbName=<%=uDTO.getDbName()%>&changeValue=UserIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>&userProfileName=<%=uDTO1.getUserProfileName()%>" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300"><%=uDTO.getDbDesc()%></font></a>
																<%}else 
																  {%> <a href="SerResIndexAction.action?dbName=<%=uDTO.getDbName()%>&changeValue=UserIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>&userProfileName=<%=uDTO1.getUserProfileName()%>"><%=uDTO.getDbDesc()%></a>
																<%}%>
														  </li>
				    							  	  <%}%>
				   									</ul>
											<%}else 
											  {%>
												<li>
													<a href="SerResIndexAction.action?userProfileName=<%=uDTO1.getUserProfileName()%>&changeValue=UserIndexes&userProfileId=<%=uDTO1.getUserProfileId()%>"><%=uDTO1.getUserProfileName()%></a>
			  								<%}%>
			  									</li>
			  									</ul></li>
										<% } 
										}else{%>
											<li>
											<a href="SerResIndexAction.action?changeValue=UserIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">UserIndexes</font></a>
											</li>
										<%}%>
										
									<%}else 
										{ %>
										<li>
											<a href="SerResIndexAction.action?changeValue=ProfileIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">ProfileIndexes</font></a>
										</li>
										<li>
											<a href="SerResIndexAction.action?changeValue=UserIndexes" onMouseOver="this.style.color='#ff3300'" onMouseOut="this.style.color='black'"><font color="#ff3300">UserIndexes</font></a>
										</li>
										<%}%>
										</ul></li></ul>
									<%}%>
					</td>
					<td width="80%" valign="top">
					<s:form id="indexsearchform" name="indexsearchform" action="SaveColumnIndexAction">
<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt" value="<%=request.getAttribute("csrfPreventionSalt")%>"/>
						<table border="0" align="center" width="100%">
						<tr><td>
								<input type="hidden" name="dbName" id="dbName"	value="<%=dbName%>" />
								<input type="hidden" name="userProfileName"	id="userProfileName" value="<%=userProfileName%>" />
								<input type="hidden" name="dbName" id="dbName" value="<%=dbName%>" />
								<input type="hidden" name="userProfileId" id="userProfileId" value="<%=userProfileId%>" />
								<input type="hidden" name="changeValue" id="changeValue" value="<%=changeValue%>"/></td></tr>
							<tr>
								<td align="left"><b>Search Results Index:</b>
								</td>
							</tr>
							<tr>
								<td>
									<%
										List<String> prePopulateList = request
												.getAttribute("prePopulateList") != null ? (ArrayList<String>) request
												.getAttribute("prePopulateList") : new ArrayList<String>();
										List indexResults = request.getAttribute("indexResults") != null ? (ArrayList) request
												.getAttribute("indexResults") : new ArrayList();
										//indexResults
										UserDTO dto = new UserDTO();
										System.out.println("pop" + request.getAttribute("prePopulateList"));
										int i = 1;
										//String[] searchIndexValues = request.getAttribute("indexValues")!=null?(String[])request.getAttribute("indexValues"): new String[0];
										Map<String, Long> detailIndex = (HashMap<String, Long>) request
												.getAttribute("detailIndex");
										List csrUsers = request.getAttribute("csrUsers") != null ? (ArrayList) request
												.getAttribute("csrUsers") : new ArrayList();
										System.out.println("csrUsers" + csrUsers);
										if (dbName != "" && csrUsers.size() != 0 && detailIndex.size()==0) {%>
											<table align="left">
											<tr>
												<td align="left"><font color="red">No Indexes configured for selected Database!!</font>
												</td>
											</tr>

										 </table>
										<%}
										else if (dbName != "" && csrUsers.size() != 0) {
											System.out.println("csrUsers:if:" + csrUsers.size());
											String noRes = "None";
											String nor = "";
									%> 
										<%--Nithu Alexander: 21-Aug-2015, Including style parameter for Datatable --%>
										<table border="0" align="center" width="100%">
											<tr>
												
											</tr>
											<tr>
												<td><display:table name="csrUsers" id="Accs" class="dataTable" style="position:static" 
														pagesize="15" requestURI="/SerResIndexAction.action">
														<display:setProperty name="paging.banner.onepage"
															value=" " />
														<display:column sortable="true" title="ID"><%=(i++)%></display:column>
														<display:column property="userName" sortable="true"
															title="Users"></display:column>
															<!-- Nithu Alexander: 22/09/2014
															Change to Display the FullName of every UserId -->
															<display:column property="userDisplayName" sortable="true"
															title="User Name"></display:column>
														<display:column sortable="true" title="Index" />
														<%
															if (pageContext.getAttribute("Accs") != null) {
																			dto = ((UserDTO) pageContext.getAttribute("Accs"));
														%>
														<%
															for (String key : detailIndex.keySet()) {

																				if (prePopulateList != null
																						&& prePopulateList.contains("Index_"
																								+ dto.getUserId() + "_"
																								+ dto.getDbId() + "_" + key)) {
																					System.out.println(dto.getUserId());
														%>
														<display:column sortable="true" title='<%=key%>'>
															<input type="checkbox"
																name="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>"
																id="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>"
																value="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>"
																checked />
															<input type="hidden"
																name="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>"
																id="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>"
																value="unchecked" />
														</display:column>
														<%
															} else {
														%>
														<display:column sortable="true" title='<%=key%>'>
															<input type="checkbox"
																name="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>"
																id="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>"
																value="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>" />
															<input type="hidden"
																name="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>"
																id="Index_<%=dto.getUserId()%>_<%=dto.getDbId()%>_<%=key%>"
																value="unchecked" />
														</display:column>
														<%
															}
																			}
																		}
														%>

													</display:table>
												</td>
											</tr>
											<tr>
												<td align="left">
													<div id="mes"></div> <%
 	if (request.getAttribute("message") != null) {
 				String message = (String) request
 						.getAttribute("message");
 %><font color="red"><%=message%></font> <%
														}
													%>
												</td>

											</tr>
											<tr>
												<td colspan="<%=i%>" align="left" valign="center"><input
													type="Submit" value="Apply" onclick="return ValidateForm()">&nbsp;&nbsp;<input
													type="Submit" value="Cancel" onclick="return cancelForm(this);"></td>
											</tr>
										</table>
									 <%
 	} else {
 %>
									<table align="left">
										<tr>
											<td align="left"><font color="red">No Users
													Found!!</font>
											</td>
										</tr>

									</table> <%
 	}
 %>
								</td>
							</tr>

						</table>
						</s:form>
					</td>
				</tr>

			</table>
		</div>
	</div>

</body>
</html>