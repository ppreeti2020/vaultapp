<%@page import="com.pb.evault.DocumentHandle"%>
<%@page import="com.g1.e2.vault.VaultClient.Document"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.g1.e2.vault.VaultClient.Account"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
    
<s:include value="adminHeader.jsp" />
<s:include value="userrepCommonTabs.jsp" />
<%
Set<String> columnNames = (Set<String>)request.getAttribute("columnNames");
List<Document> listDocument = (List<Document>)request.getAttribute("listDocument");
List actionList = (ArrayList)request.getAttribute("actionList");
String  accNum = request.getAttribute("accountNumber")!= null?request.getAttribute("accountNumber").toString():"";
String databaseName = request.getAttribute("databaseName")!=null?(String)request.getAttribute("databaseName"):"";
String  product = session.getAttribute("SELECT_DB")!= null?session.getAttribute("SELECT_DB").toString():"";
String dbDesc = session.getAttribute("SELECT_DB_DESC") != null?session.getAttribute("SELECT_DB_DESC").toString():"";
System.out.println(accNum+"product"+product);
String rep = session.getAttribute("rep")!=null?session.getAttribute("rep").toString():"";
String search = session.getAttribute("search")!=null?session.getAttribute("search").toString():"";
String dbId = session.getAttribute("dbId")!=null?session.getAttribute("dbId").toString():"";
String acType = request.getAttribute("acType")!=null?request.getAttribute("acType").toString():"";
String acc = request.getAttribute("acc")!=null?request.getAttribute("acc").toString():"";
String docHandle = request.getAttribute("docHandle")!=null?request.getAttribute("docHandle").toString():"";
String view = "";
String print = "";
String reprint = "";
String download = "";
String viewed = "";

%>
<script language="javascript" type="text/javascript">
function submitForm()
{
	//action="ActiveDirectoryAction"
	//alert(document.pressed);
	<%
if(rep.equals("true") )
	{

%>
	document.getElementById("accForm").action = "RepAccount.action";
<%}
else if(search.equals("true") )
	{%>
	document.getElementById("accForm").action = "SearchAccount.action";
<%}%>
	document.getElementById("acc").value = '<%=accNum%>';
	
	document.getElementById("accForm").submit;
	return true;
		
	}
</script>

<form name="accForm" id="accForm">
<div align="right"><input type="submit" onclick="return submitForm();" value="Back" /><input type="hidden" name="acc" id="acc" /></div>
</form>
<%
    if(acType ==""||acType.equalsIgnoreCase("print") ){ %> 
<table cellpadding="3" class="dataTable">   
<tr>
<td align="left">
<table><tr><td>
<b>Repository Name:</b>
</td><td><%=dbDesc%></td>
</tr></table>
</td>
</tr> 

<tr>
<td align="center">
<font color="#ff0000"><s:property value="message" /></font>
</td>
</tr>   
                        
                            <tr>
                                <% 
                                	for(String colName:columnNames){
                                %>
                                    <th class="sortable wi_ctrl selector"><b><%=colName%></b></th>
                                <% 
                                	}
                                %>
                                <th class="sortable wi_ctrl selector">Action</th>
                            </tr>
                            <% 
                            int no=0;
                          
                            	
                                for(Document doc:listDocument){
                                	String type = doc.getFormat().equals("collection")?doc.getType():doc.getFormat();
                                	DocumentHandle docHandle1 = new DocumentHandle();
                                	docHandle1.setDate(doc.getDate());
                                	docHandle1.setType(type);
									docHandle1.setFile(doc.getFile());
								    docHandle1.setPointer(doc.getPointer());
								   
								    //String RenderURL = "Interface?acct="+doc.getAccountNumber()+"&db="+product+"&doc="+docHandle.toString()+"&op=view";
								    String RenderURL = "ViewDoc.action?acType=view&docHandle="+docHandle1.toString()+"&acc="+doc.getAccountNumber();
								   	String downloadURL = "DownloadDocument.action?acct="+doc.getAccountNumber()+"&db=&doc="+docHandle1.toString()+"&op=view";
                        			//String printURL = "ListDocument.action?acc="+doc.getAccountNumber()+"&p="+product+"&doc="+docHandle1.toString()+"&op=print";
                        			String printURL = "PrintDocument.action?acType=print&acct="+doc.getAccountNumber()+"&p="+product+"&doc="+docHandle1.toString()+"&op=print";
                        			String reprintURL = "RePrintDocument.action?acType=reprint&acct="+doc.getAccountNumber()+"&db=&doc="+docHandle1.toString()+"&op=reprint";
                                
                            %>
                            <tr class="<%=(no%2==0?"odd":"even")%>">
                            	<%
                            		int idxCol = 0;
                            		for(String colName:columnNames){
                            			if(idxCol==0)
                            			{
                            		%>
                           					<td class='selector'><a href='<%=RenderURL%>'><%=doc.getCustomFields().get(colName)%></a></td>
                            		<%	
                            			}
                            			else{
                            		%>
                                   			<td class='selector'><%=doc.getCustomFields().get(colName)%></td>
                            		<%
                            			}
                            			idxCol++;
                            		}
                            		no++;%>
                            		<td  class='selector'>
                            		 <% for(Iterator iter=actionList.iterator();iter.hasNext();)
                                      	{ UserDTO dto = (UserDTO) iter.next();
                            			  if(dto.getActionName().equalsIgnoreCase("View"))
								    	     view = "true";
								    	  if(dto.getActionName().equalsIgnoreCase("Download"))
								    	  	 download = "true";
								    	  if(dto.getActionName().equalsIgnoreCase("Print")) 
								    	  	 print="true";
								     	  if(dto.getActionName().equalsIgnoreCase("RePrint")) 
								     	  	 reprint="true";
								      	} %>	
									<%if(view.equals("true"))
									  {%>	<a href='<%=RenderURL%>'><b>View</b></a>
									<%}if(download.equals("true"))
									  {%>||<a href='<%=downloadURL%>'><b>Download</b></a>
									<%}if(print.equals("true"))
									  { %>||<a href='<%=printURL%>'><b>Print</b></a>
									<%}if(reprint.equals("true"))
									  { %>||<a href='<%=reprintURL%>'><b>RePrint</b></a>
									<%}%>
								    </td>
                                </tr>
                          <%
								    
                                }
                          %>
                          
     
                        </table>
                        <%}else if(!acType.equals("reprint")){ String url = "Interface";
              System.out.println("After Click");%>
			 
		<jsp:include page="<%= url %>" flush="true" >
<jsp:param name="acct" value="<%=acc%>"/>
<jsp:param name="db" value="<%=product%>" />
<jsp:param name="doc" value="<%=docHandle%>" />

<jsp:param name="op" value="view" />
</jsp:include>
    	 
    	 
    	 
    	<% } %>
 <s:include value="footer.jsp" />