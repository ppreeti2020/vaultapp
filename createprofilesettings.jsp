<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pb.dao.DataDAO"%>
<%@page import="com.pb.dto.UserDTO"%>
<%@page import="org.owasp.esapi.codecs.HTMLEntityCodec"%>

<s:include value="adminHeader.jsp" />
<html>
<head>
<link href="styles/common/messages.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="styles/themes/purple/theme.css" rel="stylesheet"
	type="text/css" media="all" />
<meta name="helpHref" content="GettingStarted" />
<link rel="stylesheet" href="struts/css_xhtml/styles.css"
	type="text/css" />
<link rel="stylesheet" href="styles/login.css" />
<link rel="stylesheet" href="styles/common/messages.css" />
<link href="styles/common/header.css" rel="stylesheet" type="text/css"
	media="all" />
<title>Vault Administration</title>
<!-- calendar stylesheet -->
<link rel="stylesheet" type="text/css" media="all"
	href="styles/calendar-win2k-cold-1.css" title="win2k-cold-1" />

<!-- main calendar program -->
<script type="text/javascript" src="js/calendar.js"></script>

<!-- language for the calendar -->
<script type="text/javascript" src="js/calendar-en.js"></script>

<!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
<script type="text/javascript" src="js/calendar-setup.js"></script>
<%List csrRoles = request.getAttribute("csrRoles") != null? (ArrayList) request.getAttribute("csrRoles"): new ArrayList();
int roleLen = csrRoles.size();
List csrDatabases = request.getAttribute("csrDatabases") != null? (ArrayList) request.getAttribute("csrDatabases"): new ArrayList();
int dbLen = csrDatabases.size();
int i=0;
%>
<script type="text/javascript">
	
function CancelForm(button)
{
	document.getElementById("createProfile").action = "ShowCreateProfileAction.action";
	document.getElementById("createProfile").submit;
	return true;
			
}
		
	function ValidateForm(){
		
		var roleid = document.getElementsByName("roleId");
		var rib = false;
		for(var v=0;v<roleid.length;v++)
		{ <% i =0; 
			while(i<roleLen)
			{%>
				var ri = roleid[v].checked;
				if(ri != false)
				{ 	rib = true;
				}
				<%i++;
			}%>
		 }
		if(rib != true){
			alert("Please select any one of the Role!!!");
			return false;
		}
		
		var dbsel = false;
		var aInputs = document.getElementsByTagName('input');
		var count = 0;
		for (var i=0;i<aInputs.length;i++) {
			var type = aInputs[i].type;
			var name = aInputs[i].name;
			if(type != "hidden"){
				if (name.match("dbId")){
					if(aInputs[i].checked){
						dbsel= true;
					}
				}
				
			}
			
		}
		if(dbsel != true){
			alert("Please select any one of the Repositories!!!");
			return false;
		}
		
		var active = document.getElementsByName("active");
		var acib = false;
		for(var v=0;v<active.length;v++)
		{ 	 if(active[v].checked){
			 acib = true;
		 	}
		 }
		if(acib != true){
			alert("Please select any one of the User Actions!!!");
			return false;
		}
				
		var accessdateFrom = document.getElementById("accessdateFrom").value;
		if(accessdateFrom.length==0){
			alert("Please select the Access Date From!!!");
			return false;
		}
		var accessdateTo = document.getElementById("accessdateTo").value;
		if(accessdateTo.length==0){
			alert("Please select the Access Date To!!!");
			return false;
		}
		
		var accessfromDays = document.getElementById("accessfromDays");
		var fromday = accessfromDays.options[accessfromDays.selectedIndex].value;
		if(fromday<=0){
			alert("Please select the Access Days From!!!");
			return false;
		}
		var accesstoDays = document.getElementById("accesstoDays");
		var today = accesstoDays.options[accesstoDays.selectedIndex].value;
		if(today<=0){
			alert("Please select the Access Days To!!!");
			return false;
		}
			
		var datefromDate = accessdateFrom.split(' ')[0];
		var timefromDate = accessdateFrom.split(' ')[1];
		
		var fromDate = new Date(datefromDate.split('-')[0],datefromDate.split('-')[1]-1,datefromDate.split('-')[2]);
		fromDate.setHours(timefromDate.split(':')[0],timefromDate.split(':')[1],timefromDate.split(':')[2]);
		
		var datetoDate = accessdateTo.split(' ')[0];
		var timetoDate = accessdateTo.split(' ')[1];
		
		var toDate = new Date(datetoDate.split('-')[0],datetoDate.split('-')[1]-1,datetoDate.split('-')[2]);
		toDate.setHours(timetoDate.split(':')[0],timetoDate.split(':')[1],timetoDate.split(':')[2]);
		
		var currDate = new Date();
		
		if(fromDate<currDate){
			alert("Access From Date cannot be less than today's date!!!");
			return false;
		}
		if(toDate<currDate){
			alert("Access To Date cannot be less than today's date!!!");
			return false;
		}
		
		if(fromDate>toDate)
		{
		alert("Access From Date is greater than Access To Date");
		return false;
		}
		
		if(fromday>today){
			alert("Access from Day should be before Access to Day");
			return false;
		}
		
		return true;
		
	}
</script>
</head>
<body>
	<jsp:include page="CommonTabs.jsp" />

	<div id="countrydivcontainer"
		style="border: 1px solid gray; width: 1550px; height: 610px; margin-bottom: 1em; padding: 10px; overflow: auto">


		<jsp:include page="userpadminCommonTabs.jsp" />

		<div id="countrydivsubcontainer"
			style="border: 1px solid gray; width: 1500px; height: 565px; margin-bottom: 1em; padding: 10px; overflow: auto">


			<table width="100%" border="1px solid #778" height="500" valign="top">

				<tr>
					<td valign="top" align="left"><s:form id="createProfile"
							action="SaveProfileSettingsAction">
							<table align="left">
								<tr>
									<td><b>Roles</b></td>
									<td><b>Repositories</b></td>
									<td><b>User Actions</b></td>
								</tr>

								<tr>
									<td></td>
								</tr>

								<tr>
									<td>
										<%	Iterator itrer = csrRoles.iterator();
										%>

										<div id="div1"
											style="overflow: auto; width: 200px; border-top: solid 1px #C1C1C1; border-bottom: solid 1px #C1C1C1; border-left: solid 2px #C1C1C1; border-right: solid 2px #C1C1C1; height: 90px;">
											<table>
												<%
													while (itrer.hasNext()) {
															UserDTO cDao = (UserDTO) itrer.next();
															System.out.println("role" + cDao.getRoleId());
												%>


												<tr>
													<td><input type="checkbox" name="roleId"
														value="<%=cDao.getRoleId()%>"
														><%=cDao.getRoleName()%></input>
													</td>
												</tr>
												<%
													}
												%>

											</table>
										</div></td>
									<td>
										<%
											
												Iterator dbItrer = csrDatabases.iterator();
										%>

										<div id="div1"
											style="overflow: auto; width: 450px; border-top: solid 1px #C1C1C1; border-bottom: solid 1px #C1C1C1; border-left: solid 2px #C1C1C1; border-right: solid 2px #C1C1C1; height: 90px;">
											<table>
												<%
													while (dbItrer.hasNext()) {
															UserDTO uDto = (UserDTO) dbItrer.next();
												%>
												<tr>
													<td><input type="checkbox"
														name="dbId_<%=uDto.getDbId()%>"
														value="<%=uDto.getDbId()%>" /><%=uDto.getDbDesc()%>
														<input type="hidden" name="dbId_<%=uDto.getDbId()%>"
														value="unchecked" /></td>
												</tr>
												<%
													}
												%>

											</table>
										</div></td>
									<td>
										<div id="div2"
											style="overflow: auto; width: 200px; border-top: solid 1px #C1C1C1; border-bottom: solid 1px #C1C1C1; border-left: solid 2px #C1C1C1; border-right: solid 2px #C1C1C1; height: 90px;">
											<table>



												<tr>
													<td><input type="radio" name="active" value="Active"
														 />Active</td>
												</tr>
												<tr>
													<td><input type="radio" name="active" value="Inactive"
														 />Inactive</td>
												</tr>
												<tr>
													<td><input type="radio" name="active" value="Disabled"
														 />Disabled</td>
												</tr>
												<tr>
													<td><input type="radio" name="active" value="Auto Disabled"
														 />Auto Disabled</td>
												</tr>
												<tr>
													<td><input type="radio" name="active" value="Mark for Deletion"
														 />Mark for Deletion</td>
												</tr>
											</table>
										</div></td>
								</tr>
								<tr>
									<td colspan="5">
										<table align="center">
											<tr>
												<td>Access Date From:</td>
												<td><input type="text" name="accessdateFrom"
													id="accessdateFrom" /></td>
												<td>To:</td>
												<td><input type="text" name="accessdateTo"
													id="accessdateTo" /></td>
											</tr>

											<tr>
												<td>Access Days From:</td>
												<td><select name="accessfromDays" id="accessfromDays"
													>
														<option value="1">Monday</option>
														<option value="2">Tuesday</option>
														<option value="3">Wednesday</option>
														<option value="4">Thursday</option>
														<option value="5">Friday</option>
														<option value="6">Saturday</option>
														<option value="7">Sunday</option>
												</select></td>
												<td>to:</td>
												<td><select name="accesstoDays" id="accesstoDays"
													>
														<option value="1">Monday</option>
														<option value="2">Tuesday</option>
														<option value="3">Wednesday</option>
														<option value="4">Thursday</option>
														<option value="5">Friday</option>
														<option value="6">Saturday</option>
														<option value="7">Sunday</option>
												</select></td>
											</tr>
										</table></td>
								</tr>
								<tr>
									<td colspan="5"><input type="Submit" value="Next" onclick="return ValidateForm()"/>&nbsp;&nbsp;<input type="Submit" value="Cancel" onclick="return CancelForm()"></td>
								</tr>

							</table>

						</s:form></td>
				</tr>
			</table>
			</div></div>
			
			
			
			
		<script type="text/javascript">
		
		Calendar.setup({
			inputField : "accessdateFrom", // id of the input field
			ifFormat : "%Y-%m-%d %H:%M:%S", // format of the input field
			showsTime : true, // will display a time selector
			button : "f_trigger_b", // trigger for the calendar (button ID)
			singleClick : false, // double-click mode
			step : 1
		// show all years in drop-down boxes (instead of every other year as default)
		});
		Calendar.setup({
			inputField : "accessdateTo", // id of the input field
			ifFormat : "%Y-%m-%d %H:%M:%S", // format of the input field
			showsTime : true, // will display a time selector
			button : "f_trigger_b", // trigger for the calendar (button ID)
			singleClick : false, // double-click mode
			step : 1
		// show all years in drop-down boxes (instead of every other year as default)
		});
		
	</script>
</body>
</html>