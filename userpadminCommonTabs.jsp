<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
String subTabId = request.getAttribute("subTabId") != null?request.getAttribute("subTabId").toString():"";
System.out.println("subTabId:"+subTabId);
%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="styles/ajaxtabs.css" />

<script type="text/javascript" src="js/ajaxsubtabs.js">
/***********************************************
* Ajax Tabs Content script v2.2- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<title>Insert title here</title>
</head>
<body >
<ul id="countrysubtabs" class="shadetabs">

<%if(subTabId.equals("View Profiles"))
		{%>
			<li><a href="ProfilesListAction.action" rel="#default" class="selected">Profiles List</a></li>
			<li><a href="ShowCreateProfileAction.action" rel="countrysubcontainer1">Create Profile</a></li>
		<%}
 else if(subTabId.equals("Create Profiles"))
		{%>
			<li><a href="ProfilesListAction.action" rel="countrysubcontainer1">Profiles List</a></li>
			<li><a href="ShowCreateProfileAction.action" rel="#default" class="selected">Create Profile</a></li>
		<%}%>
	
</ul>
<script type="text/javascript">

var countries=new ddajaxtabs("countrysubtabs", "countrydivsubcontainer");
countries.setpersist(true);
countries.setselectedClassTarget("link"); //"link" or "linkparent"
countries.init();

</script>
</body>
</html>
