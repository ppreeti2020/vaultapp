<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="/struts-tags" prefix="s" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="com.pb.dto.DataDTO" %>
<%@page import="com.pb.common.CommonConstants" %>
<%@page import="com.pb.common.PropertyUtils" %>

  <%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
  <s:include value="adminHeader.jsp" />
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

  <link rel="stylesheet" type="text/css" media="all" href="styles/calendar-win2k-cold-1.css" title="win2k-cold-1" />

  <!-- main calendar program -->
  <script type="text/javascript" src="js/calendar.js"></script>

  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar-setup.js"></script>
	<link rel="stylesheet" href="styles/jquery.treeview.css" />
	<link rel="stylesheet" href="styles/screen.css" />
	
	<script src="js/jquery_treeview.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js" type="text/javascript"></script>
	<script src="js/jquery.treeview.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="js/demo.js"></script>
<title>e2 Vault Administration</title>
<%
String host = PropertyUtils.getProperties(CommonConstants.CSR_PROPERTY_FILE_NAME).getProperty(CommonConstants.HOST_NAME);
String name = request.getAttribute("name")!=null?(String)request.getAttribute("name"):"";
int pageSize = request.getAttribute("pageSize")!=null?Integer.parseInt(request.getAttribute("pageSize").toString()):10;
String hostId=(String)request.getParameter("hostId");
System.out.println("pageSize::"+pageSize);

Map<Integer,DataDTO>  treeProfile = request.getAttribute("treeProfile")!=null?(Map<Integer,DataDTO>)request.getAttribute("treeProfile"):new HashMap<Integer,DataDTO>();

System.out.println("treeProfile::"+treeProfile);
List populateLogList = request.getAttribute("populateLogList")!=null?(ArrayList)request.getAttribute("populateLogList"):new ArrayList();
if(pageSize == 0 && populateLogList.size() != 0  )
{
	if( populateLogList.size() < 10)
		pageSize = populateLogList.size();
	else
pageSize = 10;
}
String hostName = request.getAttribute("hostName")!=null?(String)request.getAttribute("hostName"):"";
String auditLogTabId=request.getAttribute("auditLogTabId")!=null?(String)request.getAttribute("auditLogTabId"):"";
String profile = request.getAttribute("profile")!=null?(String)request.getAttribute("profile"):"";
String searchProfile = request.getAttribute("searchProfile")!=null?(String)request.getAttribute("searchProfile"):"";
String logDate = request.getAttribute("logDate")!=null?(String)request.getAttribute("logDate"):"";

//String name = request.getAttribute("name")!=null?(String)request.getAttribute("name"):"";
System.out.println("compName"+name);
String tab="";
if(auditLogTabId.equalsIgnoreCase("indexLog"))
{
	tab="indexLog";
}

if (auditLogTabId.equalsIgnoreCase("index")) {
	tab="index";
}

if (auditLogTabId.equalsIgnoreCase("indexResource")) {
	tab="indexResource";
}

if (auditLogTabId.equalsIgnoreCase("reIndexLog")) {
	tab="reIndexLog";
}

if (auditLogTabId.equalsIgnoreCase("reIndex")) {
	tab="reIndex";
}


if (auditLogTabId.equalsIgnoreCase("reIndexResource")) {
	tab="reIndexResource";
}




if (auditLogTabId.equalsIgnoreCase("purgingLog")) {
	tab="purgingLog";
}


if (auditLogTabId.equalsIgnoreCase("purge")) {
	tab="purge";
}

if (auditLogTabId.equalsIgnoreCase("purgeResource")) {
	tab="purgeResource";
}



if (auditLogTabId.equalsIgnoreCase("removeLog")) {
	tab="removeLog";
}


if (auditLogTabId.equalsIgnoreCase("remove")) {
	tab="remove";
}

if (auditLogTabId.equalsIgnoreCase("removeResource")) {
	tab="removeResource";
}





%>
<script>
  // this function is needed to work around 
  // a bug in IE related to element attributes
  function submitPage(button)
  {
	  
			document.getElementById("auditPage").action="ResourceReportAction.action";
		
            document.getElementById("auditPage").submit();
			return true;  
  }
onLoad=function()
{
	alert("hi");
 var x = document.getElementById("pageSize");
 alert(x);
 var len = x.length;

 for(var i=0;i<len;i++)
	 {
	 
	 if(x[i].value == '<%= pageSize %>')
		 {
	 x[i].selected = true;
	 x[i].value = '<%= pageSize %>';
		 }
	 }

}


  </script>
  <title>tables</title>
  <style type="text/css">
  #playlist tr.header {
      background-color:#0072a8;
    }
    #playlist tr.even td {
      background-color: #eee;
    }
    #playlist tr.odd  td {
      background-color: #fff;
    }
  </style>
</head>
<%
List populateFreqList = (ArrayList)request.getAttribute("populateFreqList");
System.out.println("populateFreqList"+populateFreqList);
%>
<body>

<jsp:include page="CommonTabs.jsp" />

	<div id="countrydivcontainer" style="border:1px solid gray; width:1550px; height:610px; margin-bottom: 1em; padding: 10px; overflow:auto">
		<form name="auditPage" id="auditPage">
			<table width="100%" border="1" height="600">
			<tr><td width="20%"  valign="top">
				<jsp:include page="AuditLeftPanel.jsp" />
				</td>
				<td width="80%"  valign="top" >
					<jsp:include page="AuditCommonTabs.jsp" />
					<div id="countrydivcontainer1" style="border:1px solid gray; width:1230px; height:550px; margin-bottom: 1em; padding: 10px; overflow:auto">
						<table border="0" width="100%">	
						<tr ><td align="left"><b>Audit Log: <%= name %></b></td></tr>
						<tr><td>
							<table>
							<tr><td><b>Filter By:&nbsp;&nbsp;</b></td>
								<td>Profile Name:&nbsp;</td>
								<td><select name="searchProfile" id="searchProfile">
									<option value=""></option>
									<% for(Integer rootId1: treeProfile.keySet()){
										DataDTO ddao1 = (DataDTO)treeProfile.get(rootId1);
									%> 
									<option value="<%= ddao1.getProfile()%>"><%= ddao1.getProfile()%></option>
									 <%} %>
									</select>
								</td>
								<td>&nbsp;&nbsp;&nbsp;Date:</td>
								<td><input type="text" name="logDate" id="logDate" value="<%=logDate%>" onclick="checkGroup(this);"></td>
								<td><input type="hidden" name="name" value="<%= name %>"/>
									<input type="hidden" name="hostId" value="<%=hostId%>"/>
									<input type="hidden" name="server" value="server1"/>
									<input type="hidden" name="tab" value="<%= tab %>"/></td>	

<td>&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="search"  onclick="submitPage(this);"></td>
</tr>
</table>
</td>
</tr>
<tr><td align="left"><table><tr>
<% if( populateLogList.size() !=0)
	{if( populateLogList.size()>10){%>
<td>

<select name="pageSize" id="pageSize" style="width:50px;" onchange="return submitPage(this);">
<option value="10">10</option>
<%if( populateLogList.size()>20){%>
<option value="20">20</option>
<%}if( populateLogList.size()>30){%>
<option value="30">30</option>

<%}if( populateLogList.size()>40){%>
<option value="50">50</option>
<%}%>
</select>
</td>
<%} %>
<td align="right"><%=pageSize%> records of <%= populateLogList.size()%></td>
</tr>
</table>
</td>
</tr>
<tr><td >
<% DataDTO cd = new DataDTO();
int i=1;
int num = 1;
%>

<%if(name==null || name.equalsIgnoreCase("Indexing")  || name.equals("") )
		{
%>
<display:table name="populateLogList"  id="Accs" export="true" pagesize="<%=pageSize%>" requestURI="/ResourceReportAction.action">
<display:setProperty name="export.excel.filename" value="AuditLog.xls"/>
            <display:setProperty name="export.pdf.filename" value="AuditLog.pdf"/>
			 <display:setProperty name="export.xml.filename" value="AuditLog.xml"/>
			  <display:setProperty name="export.csv.filename" value="AuditLog.csv"/>
            <display:setProperty name="export.pdf" value="true" />
	<display:column sortable="true" title="ID"><%=(i++)%><%  cd = ((DataDTO) pageContext.getAttribute("Accs")); %></display:column>
			<display:column property="logDate" sortable="true" title="Date Of Archival/Injestion"></display:column>
			<display:column property="drpFile"  style="width:650px" sortable="true" title="Drp File Name"></display:column>
			<display:column title="Resource Name | Size" sortable="true" >
			<table><%Long id=0l;for(Iterator iter = populateFreqList.iterator();iter.hasNext();){
	DataDTO cdao = (DataDTO) iter.next();
	System.out.println("logId:"+ cdao.getLogFileId());
	if(cd.getLogFileId() == cdao.getLogFileId() )
	{
		id=cd.getLogFileId();
	%>
			
			<tr><td><%= cdao.getResWrote()%></td><td>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <%= cdao.getResSize ()%></td></tr>
			<%}}%></table>
			</display:column>
		
	
		</display:table>
		<%}else if(name.equals("Reindexing") || name.equals("Purging") || name.equals("Remove")) { %>
<display:table name="populateLogList"  id="Accs" export="true"  pagesize="<%=pageSize%>"  requestURI="/ResourceReportAction.action">
<display:setProperty name="export.excel.filename" value="AuditLog.xls"/>
            <display:setProperty name="export.pdf.filename" value="AuditLog.pdf"/>
			 <display:setProperty name="export.xml.filename" value="AuditLog.xml"/>
			  <display:setProperty name="export.csv.filename" value="AuditLog.csv"/>
            <display:setProperty name="export.pdf" value="true" />
	<display:column sortable="true" title="ID"><%=(i++)%><%  cd = ((DataDTO) pageContext.getAttribute("Accs")); %></display:column>
			<display:column property="logDate" sortable="true" title="Date Of Archival/Injestion"></display:column>
			<display:column property="drpFile"  style="width:650px" sortable="true" title="Drp File Name"></display:column>
			<display:column title="Resource Name | Size" sortable="true">
			<table><%Long id=0l;for(Iterator iter = populateFreqList.iterator();iter.hasNext();){
	DataDTO cdao = (DataDTO) iter.next();
	System.out.println("logId:"+ cdao.getLogFileId());
	if(cd.getLogFileId() == cdao.getLogFileId() )
	{
		id=cd.getLogFileId();
	%>
			
			<tr><td><%= cdao.getResWrote()%></td><td>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <%= cdao.getResSize ()%></td></tr>
			<%}}%></table>
			</display:column>
		
		
	
	
	
		</display:table>
<%}%>
	
	</td>
</tr>
<%}else{%>
<tr><td>No Records Found </td></tr>
<%}%>
</table>
</div>
</td>
</tr>
<script type="text/javascript">
 
	 Calendar.setup({
        inputField     :    "logDate",      // id of the input field
        ifFormat       :    "%Y/%m/%d",       // format of the input field
        showsTime      :    false,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    false,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
var profile = document.getElementById("searchProfile");
for(var p = 0;p<profile.length;p++)
{
	
	if(profile[p].value  == '<%= searchProfile%>')
	{
		profile[p].selected = true;
	}
}


</script>
</table>
</form>
</div>



</body>
</html>